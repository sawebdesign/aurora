Add the following commands (or similar) to the Post-build events of the Nettiers entity project:
	$(SolutionDir)\ScriptGeneration\JavaScriptEntities.exe /cfg "$(SolutionDir)\ScriptGeneration\config.xml"

The code above will generate the javascript enities file based on the settings in the config file.


Add the following 2 lines to the post-build event of the JuiceWCF project. You need to add subsequent entries for new Webservices.
The first param is the URL to the WCF service, the second param is the filename that will be created (excl. ".js") and the final param is the path to save the file to:

$(SolutionDir)ScriptGeneration\GenerateJScriptFromWCF.exe "http://localhost/JuiceWCF/Securityservice.svc" "JuiceSecurityWCF" "$(SolutionDir)JuiceWeb\scripts"
$(SolutionDir)ScriptGeneration\GenerateJScriptFromWCF.exe "http://localhost/JuiceWCF/Betservice.svc" "JuiceBettingWCF" "$(SolutionDir)JuiceWeb\scripts"
