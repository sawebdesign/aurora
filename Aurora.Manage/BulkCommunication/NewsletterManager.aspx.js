﻿/// <reference path="Scripts/References.js" />
$(
    function () {
        $(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        InitNewsletterEditor("Details", "editorSpan", false);
        DisplayMessage("Messages", "info", "Please fill in the form below", 7000);
        
    }
);