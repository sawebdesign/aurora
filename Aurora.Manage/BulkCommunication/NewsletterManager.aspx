﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsletterManager.aspx.cs"
    Inherits="Aurora.Manage.NewsletterManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Scripts/custom_black.js" type="text/javascript"></script>
    <script type="text/javascript" src="NewsletterManager.aspx.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="onecolumn">
            <div class="header">
                <span>Edit</span>
                <div style="width: 440px;" class="switch">
                    <table width="440px" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="button" style="width: 110px;" value="Details" class="left_switch active"
                                        name="tab1" id="tab1" />
                                </td>
                                <td>
                                    <input type="button" style="width: 110px;" value="Editor" class="middle_switch" name="tab2"
                                        id="tab2" />
                                </td>
                                <td>
                                    <input type="button" style="width: 110px;" value="Send Bulk Email" class="middle_switch"
                                        name="tab3" id="tab3" />
                                </td>
                                <td>
                                    <input type="button" style="width: 110px;" value="View history" class="right_switch"
                                        name="tab4" id="tab4" onclick="getNewsletterReport();" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="content">
                <div id="tab1_content" class="tab_content">
                    <input type="hidden" runat="server" id="ID" />
                    <input type="hidden" runat="server" id="ClientSiteID" />
                    <input type="hidden" runat="server" id="CustomXML" />
                    <input type="hidden" runat="server" id="InsertedOn" />
                    <input type="hidden" runat="server" id="InsertedBy" />
                    <table width="100%">
                        <tr>
                            <td>Name:
                            </td>
                            <td>
                                <input type="text" id="Name" runat="server" mytitle="The name of this news letter"
                                    valtype="required" class="help" />
                            </td>
                        </tr>
                        <tr>
                            <td>Description:
                            </td>
                            <td>
                                <input type="text" id="Description" runat="server" class="help" mytitle="This text describes what this newsletter is going to be used for"
                                    valtype="required" />
                            </td>
                        </tr>
                        <tr>
                            <td>Show Newsletter in Subscription Forms:</td>
                            <td>
                                <input type="checkbox" runat="server" id="isVisible" /></td>
                        </tr>
                    </table>
                </div>
                <div id="tab2_content" class="tab_content hide">
                    <br class="clear" />
                    <textarea id="Details" class="blaablaa" clientidmode="Static" runat="server" rows="4"
                        cols="30"></textarea>
                    <span id="editorSpan"></span>
                </div>
                <div id="tab3_content" class="tab_content hide">
                    <h2>Bulk Emails</h2>
                    <div>
                        To send a bulk email you first need to have a mailing list, the mailing list will
                    contain all the people you wish to receive this newsletter.
                    </div>
                    <br class="clear" />
                    <div class="onecolumn">
                        <div class="header">
                            <span>Mailing Lists</span>
                            <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                                <a href="javascript:void(0);" class="help" mytitle="Create News Item" onclick="editMailingList(0);"
                                    dataid="-1">
                                    <img src="/Styles/images/add.png" alt="Create Mailing List" title="Create Mailing List" /></a>
                            </div>
                        </div>
                        <div class="content">
                            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th align="left">
                                            <input type="checkbox" onclick=" $(this).is(':checked') ? $('#MailingListGrid input').attr('checked', 'checked') : $('#MailingListGrid input:checked').removeAttr('checked');" />&nbsp;Select
                                        </th>
                                        <th align="left">Group Name
                                        </th>
                                        <th align="left">Subject
                                        </th>
                                        <th align="right">Function
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="MailingListGrid" class="sys-template">
                                    <tr>
                                        <td align="left">
                                            <input onclick="$('#EditMailList').hide();" type="checkbox" value="{{ID}}" />
                                        </td>
                                        <td>{{Name}}
                                        </td>
                                        <td>{{Description}}
                                        </td>
                                        <td align="right">
                                            <a href="javascript:void(0);" onclick="editMailingList($(this).attr('dataid'));return false;"
                                                dataid="{{ID}}">
                                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" /></a>
                                            <a href="javascript:void(0);" onclick="editMailingList($(this).attr('dataid'));return false;"
                                                dataid="{{ID}}">
                                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" /></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="onecolumn" id="EditMailList" style="display: none;">
                        <div class="header">
                            <span>Editing Mailing List</span>
                        </div>
                        <div class="content">
                            <input type="hidden" id="MailingList_ID" value="0" />
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="members">
                                <tr>
                                    <td>Name:
                                    </td>
                                    <td>
                                        <input type="text" id="MailingList_Name" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject:
                                    </td>
                                    <td>
                                        <input type="text" id="MailingList_Description" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 30px;"></td>
                                </tr>
                                <tr>
                                    <td valign="top">Members:
                                    </td>
                                    <td align="left">
                                        <table width="100%">
                                            <tr>
                                                <td width="210" valign="top">
                                                    <b>Select A Company:</b><select id="Companies" onchange="getMembers($(this).attr('value'));"
                                                        class="sys-template">
                                                        <option value="{{company.ID}}">{{company.Name}}</option>
                                                    </select><br />
                                                    <br />
                                                    <select id="Members" class="sys-template" multiple="true" style="width: 200px; height: 150px;">
                                                        <option value="{{ID}}" departmentid="{{DepartmentID}}">{{FirstName}}&nbsp{{LastName}}</option>
                                                    </select>
                                                </td>
                                                <td width="210" valign="top">
                                                    <br />
                                                    <br />
                                                    <b>Selected Members:</b><br />
                                                    <div id="selectplaceholder">
                                                    </div>
                                                    <select id="SelectedMembers" class="sys-template" multiple="true" style="width: 200px; height: 150px;">
                                                        <option value="{{ID}}">{{FirstName+' '+LastName}}</option>
                                                    </select>
                                                    <div id="actions">
                                                        <a href="javascript:void(0);" onclick="removeMember();return false;" dataid="{{ID}}">
                                                            <img src="/styles/images/icon_delete.png" class="help" mytitle="Delete" /></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <input type="button" value="Add To Mailing List" onclick="addMembersToMailingList();" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br class="clear" />
                        <input style="float: right" type="button" onclick="saveMailingList();" id="btnMailingList"
                            value="Save Mailing List" />
                    </div>
                    <br class="clear" />
                    <input type="button" value="Send Bulk Email" onclick="sendBulkMail();" />
                </div>

                <div id="tab4_content" class="tab_content hide">
                    <br class="clear" />
                    <b>Here you can view all interactions for this news letter.</b>
                    <div class="onecolumn">
                        <div class="header">
                            <span>Newsletter History</span>
                        </div>
                        <div class="content">
                            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th align="left">Date Sent
                                        </th>
                                        <th align="left">Mailing List Name
                                        </th>
                                        <th align="left">Report
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="sys-template" id="Report">
                                    <tr>
                                        <td align="left">{{formatDateTime(newsletterReport.InsertedOn,"dd MMM yyyy HH:mm:ss")}}
                                        </td>
                                        <td align="left">{{mailingList.Name}}
                                        </td>
                                        <td align="left">
                                            <a href="#DetailReport" class="viewreport" dataid="{{newsletterReport.CommEngineMessageID}}">View Report</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br class="clear" />
                </div>
            </div>
        </div>
        <input type="button" value="Save" id="btnSave" onclick="saveNewsLetter();" style="display: none; float: right;" />
        <div style="display: none;">
            <div id="DetailReport" style="background: #fff; width: 99.5%; height: 96%;">
                <div class="onecolumn">
                    <div class="header">
                        <span>Bulk Email Summary</span>
                       
                        <div style="float: right; margin-top: 10px;" class="filter">
                            <a id="lnkFilter" href="javascript:void(0);" onclick="$('#Criteria').slideToggle();">Filter</a>
                            <div id="Criteria" style="min-width: 200px; margin-top: 15px; margin-right: 30px; display: none;">
                                <div style="float: right">
                                    &nbsp; <b>Filter By:</b>
                                    <select id="FilterBy" onchange="ddlChange();">
                                        <option value="null">None</option>
                                        <option value="EMAIL:">Email Address</option>
                                        <option value="STATUS:">Status</option>
                                    </select>
                                    <input id="emailfilter" value="" style="display:none;" />
                                    <select id="statusfilter" style="display:none;">
                                        <option value="STATUS:SUCCESS">Sent</option>
                                        <option value="STATUS">Failed</option>
                                    </select>
                                </div>
                                <div style="float: right; line-height: 19px; padding: 5px">
                                    <a href="javascript:void(0);" id="btnSearch" onclick="$('#DetailReport').block({ message: null });viewDetailedReport(detailReportID)">Search</a>
                                </div>
                                <div style="float: right">
                                    <input type="text" id="nameSearch" value="" style="width: 100px; display: none;" />
                                </div>
                            </div>
                            &nbsp;
                        </div>
                         <div style="width: auto;float:right;" class="switch">
                            <table cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="button" onclick="mailReportHideTabs();" value="Main Summary" class="button"
                                                id="tab5" />
                                        </td>
                                        <td>
                                            <input type="button" value="View Detail Summary" class="button"
                                                id="tab6" onclick="mailReportHideTabs('Detailed')" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="content">
                        <div class="tab5_content">
                            <table width="100%" class="data" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <th align="left">Messages Pending
                                        </th>
                                        <th align="left">Messages Sent
                                        </th>
                                        <th align="left">Messages Failed
                                        </th>
                                        <th align="left">Total Emails Sent
                                        </th>
                                        <th align="left">Average Email Size (kB)
                                        </th>
                                        <th align="left">Total  Bulk Email Size (kB)
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="sys-template" id="DetailReportGrid">
                                    <tr>
                                        <td>{{Pending}}
                                        </td>
                                        <td>{{Sent}}
                                        </td>
                                        <td>{{Errors}}
                                        </td>
                                        <td>{{Total}}
                                        </td>
                                        <td>{{Math.round(MessageSize / 1024,4)}}
                                        </td>
                                        <td>{{Math.round((MessageSize * Total) / 1024,4)}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab6_content hide">
                            <table width="100%" class="data" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <th align="left">Email</th>
                                        <th align="left">Name</th>
                                        <th align="left">Send Try Count</th>
                                        <th align="left">Email Sent Date</th>
                                        <th align="left">Opened Date</th>
                                    </tr>
                                </thead>
                                <tbody id="detailEmailTable">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
