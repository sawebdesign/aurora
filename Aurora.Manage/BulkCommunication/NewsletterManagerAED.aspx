﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="NewsletterManagerAED.aspx.cs" Inherits="Aurora.Manage.NewsletterManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script src="NewsletterManagerAED.aspx.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.tmpl.min.js"></script>
    <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <script
      <%-- Templates --%>
    <script id="detailEmailReport" type="text/x-jquery-tmpl">
        <tr>
            <td>${ToEmail}</td>
            <td>${ToName}</td>
            <td>${EmailTryCount}</td>
            <td>${EmailSent != null ? EmailSent.format("dd/MM/yyyy HH:ss") : 'Never Sent'}</td>
            <td>${EmailOpened != null ? EmailOpened.format("dd/MM/yyyy HH:ss") : 'Not Opened' }</td>
        </tr>
    </script>
    <div class="onecolumn">
        <div class="header">
            <span>Newsletters</span>
            <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                <a href="javascript:void(0);" class="help" mytitle="Create News Item" onclick="editNewsletter(0);return false;"
                    dataid="-1">
                    <img src="/Styles/images/add.png" alt="Create User" title="Create Newsletter" /></a>
            </div>
        </div>
        <div class="content">
            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th align="left" width="40%">
                            Name
                        </th>
                        <th align="left" width="40%">
                            Description
                        </th>
                        <th align="right" width="20%">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" id="Newsletters">
                    <tr>
                        <td>{{Name}}</td>
                        <td>{{Description}}</td>
                        <td align="right">
                            <a href="javascript:void;" onclick="editNewsletter($(this).attr('dataid'));return false;"
                                dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" /></a>
                            <a href="javascript:void;" onclick="editNewsletter($(this).attr('dataid'));return false;" dataid="{{ID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br class="clear" />
    <div id="Messages"></div>
    <div id="contentAddEdit"></div>
   <br class="clear"/>

   
</asp:Content>
