﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage
{
    public partial class NewsletterManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Custom.Data.AuroraEntities entity = new Custom.Data.AuroraEntities();

            if (Request["NewsID"] != null) {
                long newsLetterID;
                long.TryParse(Request["NewsID"], out newsLetterID);
                
                if (newsLetterID != 0)
                {
                    var newsLetter = (from newsletters in entity.Newsletter
                                     where newsletters.ClientSiteID == SessionManager.ClientSiteID
                                           && newsletters.ID == newsLetterID
                                     select newsletters).SingleOrDefault();
                    Name.Value = newsLetter.Name;
                    ID.Value = newsLetter.ID.ToString();
                    Description.Value = newsLetter.Description;
                    ClientSiteID.Value = newsLetter.ClientSiteID.ToString();
                    InsertedBy.Value = newsLetter.InsertedBy.ToString();
                    InsertedOn.Value = newsLetter.InsertedOn.ToString();
                    CustomXML.Value = newsLetter.CustomXML;
                    Details.Value = newsLetter.HTML;
                    isVisible.Checked = newsLetter.isVisible;

                }
                else {
                    ID.Value = "0";
                    ClientSiteID.Value = SessionManager.ClientSiteID.ToString();
                    InsertedBy.Value = SessionManager.UserID.ToString();
                    InsertedOn.Value = DateTime.Now.ToString();
                } 
            }
            
        }
    }
}