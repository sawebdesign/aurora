﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />

//#region Vars
var ds = null;
var companyDS = null;
var departmentDS = null;
var membersDS = null;
var memberBox = null;
var selectedMembers = null;
var selectedMemberDS = null;
var newMembers = [];
var mailingListDS = null;
var currentMailingList = null;
var bulkEmailMailList = [];
var reportDS = null;
var detailReport = null;
var detailReportID;
//#endregion

//#region Init
$(function () {
    getNewsletter();

});
//#endregion

//#region Get Methods 
function getNewsletter() {
    $(".data").block({ message: null });
    var response = function (result) {
        if (result.Result) {
            if (result.Count === 0) {
                DisplayMessage("Messages", "warn", "There are no news letters, please create one by clicking on the green button on the top right", 10000);
            }
            else {
                if (!ds) {
                    ds = $create(Sys.UI.DataView, {}, {}, {}, $get("Newsletters"));
                }

                ds.set_data(result.Data);
            }
        }
        else {
            DisplayMessage("Messages", "error", "There was an error whilst acquiring your data, please try again if the problem persists please contact support");
        }
        $(".data").unblock();
    };

    Aurora.Manage.Service.AuroraWS.GetNewsletters(response, onError);
}
function editNewsletter(id, isDelete) {
    $("#contentAddEdit").block({ message: null });
    var loadSuccess = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>Runtime Error</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#contentAddEdit").show();
            $("#btnSave").show();
            if (mailingListDS) {
                mailingListDS.dispose();
                mailingListDS = null;
            }
            getMailingLists();
        }

        $("#contentAddEdit").unblock();
    };
    if (reportDS) {
        reportDS.dispose();
        reportDS = null;
        detailReport.dispose();
        detailReport = null;
    }

    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='/Styles/images/message-loader.gif'/>", 0);
    $("#contentAddEdit").load(String.format("NewsLetterManager.aspx?NewsID={0}&var={1}", id, new Date().getMilliseconds()), loadSuccess);
}
function getMailingLists() {
    $("#contentAddEdit").block({ message: null });
    var response = function (result) {
        if (!mailingListDS) {
            mailingListDS = $create(Sys.UI.DataView, {}, {}, {}, $get("MailingListGrid"));
        }

        mailingListDS.set_data(result.Data);

        $("#contentAddEdit").unblock();
    };

    Aurora.Manage.Service.AuroraWS.GetMailingLists(response, onError);
}
function editMailingList(id) {
    $("#contentAddEdit").block({ message: null });
    $("#EditMailList").show();
    currentMailingList = findByInArray(mailingListDS._data, "ID", id);
    if (currentMailingList) {
        var mailList = new AuroraEntities.MailingList(currentMailingList);

        for (var property in mailList) {
            var $item = $("#MailingList_" + property);
            if ($item.length > 0) {
                $item.val(mailList[property]);
            }
        }

        newMembers = [];
        bulkEmailMailList = [];
    }
    else {
        currentMailingList = null;
        newMembers = [];
        bulkEmailMailList = [];
        $("#MailingList_ID").val(0);
        $("#MailingList_Name").val("");
        $("#MailingList_Description").val("");
    }

    getSelectedMembers(Number($("#MailingList_ID").val()));
    getCompanies(0);
    $("#contentAddEdit").unblock();
}
function getCompanies(id) {
    var response = function (result) {
        if (result.Result) {
            if (!companyDS) {
                companyDS = $create(Sys.UI.DataView, {}, {}, {}, $get("Companies"));
            }

            companyDS.set_data(result.Data);
            getMembers(companyDS._data[0].company.ID);
        }
    };

    Aurora.Manage.Service.AuroraWS.GetOrganisations(id, response, onError);
}
function getDepartments(id) {
    var departments = null;
    for (var com = 0; com < companyDS._data.length; com++) {
        if (companyDS._data[com].company.ID == id) {
            departments = companyDS._data[com].departments;
            break;
        }
    }
    var departmentControls = "";

    if (departments) {
        if (!departments["length"]) {
            departments.length = 1;
        }

        for (var dep = 0; dep < departments.length; dep++) {
            var currentDepartment = departments.length == 1 ? departments : departments[dep];
            var firstMember = $(String.format("#Members option[departmentid='{0}']", currentDepartment.ID)).first();
            if (firstMember.length > 0) {
                $(String.format("<optgroup id='{0}' label='{1}'></optgroup>", currentDepartment.ID, currentDepartment.Name)).insertBefore(firstMember);
            }

        }



    }
}
function getMembers(id) {
    var response = function (result, organisationID) {
        if (result.Result) {

            if (!memberBox) {
                memberBox = "<select id='Members' style='width:200px;height:150px;' class='sys-template' multiple='true'>" + $('#Members').html() + "</select>";
            }
            else {

                $("#Members").remove();
                $(memberBox).insertAfter($("#Companies"));
            }

            if (!membersDS) {
                membersDS = $create(Sys.UI.DataView, {}, {}, {}, $get("Members"));
            }
            else {
                membersDS.dispose();
                membersDS = null;
                membersDS = $create(Sys.UI.DataView, {}, {}, {}, $get("Members"));
            }


            membersDS.set_data(result.Data);
            getDepartments(organisationID);


            $('#Members').toChecklist({

                /**** Available settings, listed with default values. ****/
                addScrollBar: true,
                addSearchBox: true,
                searchBoxText: 'Type here to search list...',
                showCheckboxes: true,
                showSelectedItems: false,
                submitDataAsArray: true // This one allows compatibility with languages that use arrays

            });


        }
    };
    if (membersDS) {
        $("#Members option,#Members optgroup").remove();
        $("#Members").append("<option value='{{ID}}'>{{FirstName}}&nbsp;{{LastName}}</option>");

    }
    Aurora.Manage.Service.AuroraWS.GetAllMembers(id, response, onError, id);
}
function getSelectedMembers(id) {
    if (!selectedMembers) {
        selectedMembers = "<select id='SelectedMembers' style='width:200px;height:150px;' class='sys-template' multiple='true'>" + $("#SelectedMembers").html() + "</select>";
    }
    else {
        $('#SelectedMembers').remove();
        $(selectedMembers).insertAfter("#selectplaceholder");
    }
    var response = function (result) {
        if (result.Result && result.Data.length > 0) {
            if (!selectedMemberDS) {
                selectedMemberDS = $create(Sys.UI.DataView, {}, {}, {}, $get("SelectedMembers"));
            }
            else {
                selectedMemberDS.dispose();
                selectedMemberDS = null;

                selectedMemberDS = $create(Sys.UI.DataView, {}, {}, {}, $get("SelectedMembers"));

            }
            if (result.Data.length === 0) {
                $("#actions").hide();
            }
            selectedMemberDS.set_data(result.Data);
            $('#SelectedMembers').toChecklist({

                /**** Available settings, listed with default values. ****/
                addScrollBar: true,
                addSearchBox: true,
                searchBoxText: 'Type here to search list...',
                showCheckboxes: true,
                showSelectedItems: false,
                submitDataAsArray: true // This one allows compatibility with languages that use arrays

            });

            for (var member = 0; member < result.Data.length; member++) {
                newMembers.push({ id: result.Data[member].ID, name: result.Data[member].FirstName + " " + result.Data[member].LastName });
            }

        }



    };

    Aurora.Manage.Service.AuroraWS.GetMailingListMembers(id, response);
}
//#endregion

//#region Put Methods 
function saveNewsLetter() {
    $("#contentAddEdit").block({ message: null });
    if (validateForm("tab1_content", "Highlighted items need to be filled in", "Messages")) {
        $("#contentAddEdit").unblock();
        return;
    }
    var response = function (result) {
        if (result.Result) {
            DisplayMessage("Messages", "success", "Newsletter successfully saved", 7000);
            getNewsletter();
        }
        else {
            DisplayMessage("Messages", "success", "Newsletter successfully saved");
        }

        $("#contentAddEdit").unblock();
    };
    var entity = new AuroraEntities.Newsletter();

    for (var property in entity) {
        var $control = $("#" + property);
        if ($control.length !== 0) {
            if ($control.is("input[type='checkbox']")) {
                entity[property] = $control.is(":checked");
            }
            else {
                entity[property] = $control.val();
            }
        }
    }
    entity.HTML = oEdit1.getHTMLBody();
    Aurora.Manage.Service.AuroraWS.UpdateNewsLetter(entity, response, onError);
}
function addMembersToMailingList() {

    var duplicates = 0;
    $("#Members .checked input").each(
        function (idx, elem) {
            var $current = $(elem);
            if (!findByInArray(newMembers, "id", $current.val())) {
                newMembers.push({ id: $current.val(), name: $current.next().html() });
            }
            else {
                duplicates++;
            }

        }
    );


    $("#SelectedMembers").remove();
    var memberList = $(selectedMembers);

    var listToWrite = "";


    memberList.insertAfter("#selectplaceholder");
    memberList.html("");
    for (var member = 0; member < newMembers.length; member++) {
        memberList.append(String.format("<option value='{0}'>{1}</option>", newMembers[member].id, newMembers[member].name));
    }

    $('#SelectedMembers').toChecklist({
        addScrollBar: true,
        addSearchBox: true,
        searchBoxText: 'Type here to search list...',
        showCheckboxes: true,
        showSelectedItems: false,
        submitDataAsArray: true // This one allows compatibility with languages that use arrays

    });

    var $item = $('#SelectedMembers').find("input[value='ID']").parent();
    $item.remove();


    if (duplicates > 0) {
        alert(String.format("There were {0} duplicate(s) found and were therefore not added", duplicates));
    }

}
function saveMailingList() {
    $("#contentAddEdit").block({ message: null });
    if (validateForm("members", "Please populate the field(s) that have been marked", "Messages")) {
        $("#contentAddEdit").unblock();
        return;

    }

    if (newMembers.length == 0) {
        DisplayMessage("Messages", "warn", "You must select members to create a member list");
        $("#contentAddEdit").unblock();
        return;
    }

    var response = function (result) {
        if (result.Result) {
            DisplayMessage("Messages", "success", "Mailing list saved", 7000);
            currentMailingList = result.Data;
            $("#EditMailList").hide();
            getMailingLists();
        }

        $("#contentAddEdit").unblock();
    };
    var memberList = [];

    for (var member = 0; member < newMembers.length; member++) {
        memberList.push(newMembers[member].id);
    }
    var mailList = new AuroraEntities.MailingList();

    //create mailing list object
    if (!currentMailingList) {
        mailList = new AuroraEntities.MailingList();
    }
    else {
        mailList.ClientSiteID = 0;
        mailList.InsertedBy = 0;
        mailList.InsertedOn = new Date();
    }

    for (var property in mailList) {
        var $item = $("#MailingList_" + property);
        if ($item.length > 0) {
            mailList[property] = $item.val();
        }
    }



    Aurora.Manage.Service.AuroraWS.UpdateMailingListMembers(mailList, memberList, response, onError);
}
function sendBulkMail() {
    $("#contentAddEdit").block({ message: null });
    var response = function (result) {
        if (result.Result && result.Count > 0) {
            DisplayMessage("Messages", "success", "Bulk Email Sent, please look at the news letter's History tab to view its progress", 7000);
        }
        else if (result.Result && result.Count === 0) {
            DisplayMessage("Messages", "error", result.Data, 7000);
        }
        else {
            DisplayMessage("Messages", "error", "There was an unexpected error when sending your bulk email,please try again if the problem persists please contact support", 7000);
        }
        $("#contentAddEdit").unblock();
    };
    bulkEmailMailList = [];


    $("#MailingListGrid input:checked").each(
        function (index, elem) {
            bulkEmailMailList.push($(elem).val());
        }
    );

    if (bulkEmailMailList.length === 0) {
        DisplayMessage("Messages", "warn", "Please select a mailing list from the mailing list grid", 7000);
        $("#contentAddEdit").unblock();
        return;
    }

    var content = oEdit1.getHTMLBody();
    var getVoucher;
    if (content.indexOf("{vouchercodeplaceholder:") > -1) {
        getVoucher = content.substr(content.indexOf("{vouchercodeplaceholder:"));
        getVoucher = getVoucher.substr(0, getVoucher.indexOf("}")).replace("{", "");
        getVoucher = getVoucher.split(":");
        content = content.replace("{vouchercodeplaceholder:" + getVoucher[1], "{vouchercodeplaceholder");
        SetJSSession("VoucherID", getVoucher[1]);
    }

    Aurora.Manage.Service.AuroraWS.SendBulkEmail(Number($("#ID").val()), bulkEmailMailList, "", content, Number(GetJSSession("VoucherID") || 0), response, onError);
}

function getNewsletterReport() {
    var response = function (result) {
        if (result.Result) {
            if (!reportDS) {
                reportDS = $create(Sys.UI.DataView, {}, {}, {}, $get("Report"));

            }


            reportDS.set_data(result.Data);
            $('.viewreport').fancybox({
                padding: 0,
                transitionIn: 'elastic',
                transitionOut: 'elastic',
                titleShow: true,
                title: "Quote Preview",
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                autoDimensions: false,
                width: '90%',
                height: '90%',
                onComplete: function (e) {
                    viewReport(e);
                    mailReportHideTabs();
                }
            });
        }
    };

    Aurora.Manage.Service.AuroraWS.GetNewsLetterReports(Number($("#ID").val()), 1, 100, response, onError);
}

function removeMember() {
    var $item = $('#SelectedMembers .checked input');
    $item.each(
        function (indx, elm) {
            var $chkListLI = $(elm);
            var memberToDelete = findByInArray(newMembers, "id", $chkListLI.val());
            for (var member = 0; member < newMembers.length; member++) {
                if (newMembers[member] == memberToDelete) {
                    newMembers.splice(member, 1);
                    $chkListLI.parent().remove();
                }
            }

        }
    );
}

function viewReport(id) {
    $("#DetailReport").block({ message: null });
    var response = function (result) {
        if (result.Result) {
            if (!detailReport) {
                detailReport = $create(Sys.UI.DataView, {}, {}, {}, $get("DetailReportGrid"));
            }
            else {
                DisplayMessage("Messages", "warn", "No Reports were found for this newsletter", 8000);
            }

            detailReport.set_data(result.Data);
        }


    };

    Aurora.Manage.Service.AuroraWS.GetDetailEmailReport("{" + $(id[0]).attr("dataid") + "}", response, onError);
    detailReportID = "{" + $(id[0]).attr("dataid") + "}";
    viewDetailedReport(detailReportID);
}
function viewDetailedReport(id) {
    var response = function (result) {
        if (result.Data && result.Data.length > 0) {
            bindDataToGrid("#detailEmailTable", "#detailEmailReport", result.Data);
        }
        else {
            $("#detailEmailTable").children().remove();
        }
        $("#DetailReport").unblock();
    };

    var filterValue = "";
    if ($(".filter").is(":visible")) {
        if ($("#FilterBy").val() == "EMAIL:") {
            filterValue = "EMAIL:" + $("#emailfilter").val();
        }
        else if($("#FilterBy").val() =="null") {
            filterValue = "";
        }
        else {
            filterValue = $("#statusfilter").val();
        }
    }

    Aurora.Manage.Service.AuroraWS.GetDetailMessageReport(id, 1, 10, filterValue, response, onError);
}
function ddlChange() {
    if ($("#FilterBy").val() == "EMAIL:") {
        $("#emailfilter").show();
        $("#statusfilter").hide();
    }
    else if ($("#FilterBy").val() == "null") {
        $("#emailfilter").hide();
        $("#statusfilter").hide();
    }
    else {
        $("#statusfilter").show();
        $("#emailfilter").hide();
    }
}
function mailReportHideTabs(id) {
    if (id == "Detailed") {
        $(".tab5_content").hide();
        $(".tab6_content,.filter").show();
    }
    else {
        $(".tab5_content").show();
        $(".tab6_content,.filter").hide();
    }
}
//#endregion