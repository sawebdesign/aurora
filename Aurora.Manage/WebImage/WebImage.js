﻿/// <reference path="/scripts/jquery-1.5.2.js" />

var usermessage;
$(function () {

    $("#tbresize").hide();
    $("#resize").mouseover(function () {
        $("#tbresize").slideToggle();
    })
    $("#tbresize").mouseleave(function () {
        $("#tbresize").slideToggle();
    })

    $("#btnUpload").hide();
    $("#loader").hide();
    $('#txtUpload').change(function () {
        $("#loader").show();
        $("#txtUpload").hide();
        $('#btnUpload').click();
    });

    $("#message").hide();
    $('#message').click(function () {
        $("#message").slideToggle();
    });

    loadimage();

    if (usermessage !== undefined) {
        message(usermessage);
    } else {
        message('To Select an Image to upload click the Browse button on the top left.');
    }
});

function loadimage() {
    //load the image
    var imageUrl = getQueryVariable('ipath') + 'tmp_' + getQueryVariable('iname') + '?' + new Date();
    function loadImage(result) {
        var imageElement = document.getElementById('editimg');
        imageElement.src = imageUrl;
        $(imageElement).load(function () {
            $(this).fadeIn(500);
            setWH();
        });
    };
    function fail(result) {
        if ($("#editimg").length > 0) {
            $("#editimg").remove();
            //message('Select an Image to upload');
        }
    };

    $.ajax({ url: imageUrl, type: 'HEAD', error: fail, success: loadImage });
}

function message(lable) {
    $("#message").html(lable);
    $("#message").slideToggle();
}

function setWH() {
    $("#currenty").text($("#editimg").height());
    $("#currentx").text($("#editimg").width());
}

function getQueryVariable(variable) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="variable" type="String">
    ///		The name of the query string param that you are looking for
    ///	</param>

    var query = urldecode(window.location.search.substring(1));
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var check = pair[0];
        if (check.toLowerCase() === variable.toLowerCase()) {
            return pair[1];
        }
    }
    return null;
}

function urldecode(str) {
    str = str.replace('+', ' ');
    str = unescape(str);
    return str;
}