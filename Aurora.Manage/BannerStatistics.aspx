﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BannerStatistics.aspx.cs"
    Inherits="Aurora.Manage.BannerStatistics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Banner Stats</title>
    <script src="Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="Scripts/jquery.pagination.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script src="Service/AuroraWS.asmx/js" type="text/javascript"></script>
    <script src="Scripts/Utils.js" type="text/javascript"></script>
    <script type="text/javascript" src="BannerStatistics.aspx.js"></script>
    <link href="Styles/black/screen.css" rel="stylesheet" type="text/css" />
    <style>
    body
    {
        margin:0px 10px 10px 0px;
        padding-left:10px;
        background-image:none;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="contentList">
            <!-- Begin one column window -->
            <div class="onecolumn">
                <div class="header" id="name">
                    <span>%CampName% Campaign Details</span>
                </div>
                <br class="clear" />
                <div class="content">
                    <table class="data" width="100%" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th align="left">
                                    Start
                                </th>
                                <th align="left">
                                    End
                                </th>
                                <th align="left">
                                    Views
                                </th>
                                 <th align="left">
                                    Max Views
                                </th>
                                <th align="left">
                                    Clicks
                                </th>
                                 <th align="left">
                                    Max Clicks
                                </th>
                                <th align="left">
                                    Views/Day
                                </th>
                                <th align="left">
                                   Views/Day %
                                </th>
                                <th align="left">
                                   Ratio Views:Day
                                </th>
                            </tr>
                        </thead>
                        <tbody id="templateList" class="sys-template" sys:attach="dataview">
                            <tr>
                                <td width="10%">
                                    {{formatDateTime(ValidFromDate,"dd/MM/yyyy")}}
                                </td>
                                <td width="10%">
                                    {{formatDateTime(ValidToDate,"dd/MM/yyyy")}}
                                </td>
                                <td width="10%">
                                    {binding ShowCount}
                                </td>
                                <td width="10%">
                                    {{MaxImpressions}}
                                </td>
                                <td width="10%">
                                    {binding ClickCount}
                                </td>
                                <td width="10%">
                                    {{MaxClicks}}
                                </td>
                                <td>
                                {{calc("Exp",ValidToDate+"-"+ShowCount)}}
                                </td>
                                <td>
                                {{calc("%",ClickCount+"-"+ShowCount)}}
                                </td>
                                <td>
                                {{calc("ratio",ShowCount+"-"+ClickCount) + ":1"}}    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
            <!-- End one column window -->
            <!-- End content -->
        </div>
        
        <div class="onecolumn">
            <div class="header">
                <span>Banner Clicks Details</span>
            </div>
            <br class="clear" />
            <div class="content">
                <div>
                    <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <thead>
                            <tr>
                                <th align="left">
                                    Pages banner was clicked on
                                </th>
                                <th align="left">
                                    Date & Time
                                </th>
                                <th align="left">
                                    IP Address
                                </th>
                            </tr>
                        </thead>
                        <tbody id="BannerClickedView" class="sys-template" sys:attach="dataview">
                            <tr>
                                <td>
                                    {{Page}}
                                </td>
                                <td>
                                    {{formatDateTime(ClickDate,"dd MMM yyyy HH:mm")}}
                                </td>
                                <td>
                                    {{IP}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="html_body">
                    </div>
                    <!-- Begin pagination -->
                    <div id="Pagination">
                    </div>
                    <!-- End pagination -->
            </div>
        </div>
    </div>
    </form>
</body>
</html>
