﻿using Aurora.Custom;
using Aurora.Custom.Data;
using Aurora.Custom.UI;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;

namespace Aurora.Manage.Service {
	/// <summary>
	/// Summary description for AuroraWS
	/// </summary>
	[WebService(Namespace = "ManageWS")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService, System.Runtime.InteropServices.GuidAttribute("D122A26D-00A9-4A69-BDF5-CA1C9F72FD42")]
	public class AuroraWS : System.Web.Services.WebService {
		private AuroraEntities service = new AuroraEntities();

		#region UserAED
		[WebMethod(EnableSession = true)]
		public List<GetUsersByClientSiteIDHierarchy_Result> GetUsers(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			var results = service.GetUsersByClientSiteIDHierarchy(SessionManager.ClientSiteID, SessionManager.UserID).ToList();

			foreach (GetUsersByClientSiteIDHierarchy_Result user in results) {
				if (user != null) {
					user.Password = Aurora.Security.Encryption.Decrypt(user.Password);
				}
			}
			Aurora.Custom.SessionManager.iPageCount = results.Count();

			return results.AsEnumerable().Paginate(iPageNum, iPageSize).ToList();
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetUsersAndCount(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();


			try {
				data.Add("Result", true);
				data.Add("Data", GetUsers(iPageNum, iPageSize, iFilter));
				data.Add("Count", Aurora.Custom.SessionManager.iPageCount);
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception exception) {
				data.Clear();
				Utils.ServiceErrorResultHandler(exception, ref data);

			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetRoles(long userId) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var results = (from roles in service.Roles
							   from uroles in service.UserRole.Where(a => a.RoleID == roles.ID & a.UserID == userId).DefaultIfEmpty()
							   orderby roles.RoleName
							   select new { roles.ID, roles.RoleName, Selected = uroles.RoleID == null ? "false" : "true" });

				data.Add("Result", true);
				data.Add("Data", results.ToList());
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception exception) {
				data.Clear();


				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSites(int UserID) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				Utils.CheckSqlDBConnection();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[Clients].[proc_Client_GetByClientSiteIDHierarchy]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					cmd.Parameters.AddWithValue("@UserID", UserID);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					var result = from tb in ds.Tables[0].AsEnumerable()
								 where tb.Field<int>("ClientSiteID") > 0
								 select new {
									 ClientSiteID = tb.Field<int>("ClientSiteID"),
									 CompanyName = tb.Field<string>("CompanyName"),
									 Selected = tb.Field<string>("Selected"),
								 };

					data.Add("Result", true);
					data.Add("Data", new List<dynamic>(result));
					data.Add("ErrorMessage", string.Empty);

				}
			} catch (Exception exception) {
				data.Clear();


				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SaveUser(Users user, List<Aurora.Custom.Data.UserRole> userrole, List<Aurora.Custom.Data.UserClientSite> userclientsite, long ClientSiteID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				//this is a new user 
				if (user.ID == -1) {
					var dbUser = service.Users.Where(w => w.UserName == user.UserName);
					if (dbUser.Count() == 0) {
						user.ID = 0;
						user.Password = Aurora.Security.Encryption.Encrypt(user.Password);
						user.InsertedBy = SessionManager.UserID;
						user.InsertedOn = DateTime.Now;
						user.IsLocked = false;
						user.ClientSiteID = ClientSiteID;
						service.AddToUsers(user);
						service.SaveChanges();
					} else {
						data.Add("Result", false);
						data.Add("ErrorMessage", "A user with this username already exists");
						return data;
					}

				} else {
					var dbUser = service.Users.Where(w => w.UserName == user.UserName && w.ID != user.ID);
					if (dbUser.Count() == 0) {
						//update an existing user
						service.AttachTo(user.GetType().Name, user);
						user.Password = Security.Encryption.Encrypt(user.Password);
						user.SetAllModified(service);
					} else {
						data.Add("Result", false);
						data.Add("ErrorMessage", "A user with this username already exists");
						return data;
					}
				}

				//delete the existing UserRole &nUserClientSites
				service.UserRole.Where(w => w.UserID == user.ID).ToList().ForEach(service.DeleteObject);
				service.UserClientSite.Where(w => w.UserID == user.ID).ToList().ForEach(service.DeleteObject);

				//add new UserRole & UserClientSite
				foreach (Custom.Data.UserRole role in userrole) {
					role.UserID = user.ID;
					service.AddToUserRole(role);
				}
				foreach (Custom.Data.UserClientSite site in userclientsite) {
					site.UserID = user.ID;
					service.AddToUserClientSite(site);
				}

				service.SaveChanges();

				data.Add("Result", true);

				data.Add("ErrorMessage", string.Empty);

			} catch (Exception exception) {
				data.Clear();
				Utils.ServiceErrorResultHandler(exception, ref data);

			}

			return data;
		}

		#endregion

		#region NewsAED

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetNewsAndCount(int categoryID, int iPageNum, int iPageSize, string iFilter) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

			try {
				var results = iFilter.ToUpper() == "ARCHIVED" ?
							   (from news in service.News
								where news.ClientSiteID == SessionManager.ClientSiteID
								&& news.CategoryID == categoryID
								&& news.DeletedBy == null
								&& (news.StartDate <= currentDate || news.StartDate >= currentDate)
								&& news.EndDate <= currentDate
								select news).DefaultIfEmpty()
							   :
							   (from news in service.News
								where news.ClientSiteID == SessionManager.ClientSiteID
								&& news.CategoryID == categoryID
								&& news.DeletedBy == null
								&& (news.EndDate >= currentDate)
								select news).DefaultIfEmpty()
							   ;

				SessionManager.iPageCount = results.Count();

				data.Add("Data", results.OrderBy(r => r.ID).Paginate(iPageNum, iPageSize).ToList());
				data.Add("Result", true);
				data.Add("Count", SessionManager.iPageCount);
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception exception) {
				data.Clear();


				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetOrphanedNewsAndCount(int iPageNum, int iPageSize, string iFilter) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				var results = iFilter.ToUpper() == "ARCHIVED" ?
							   (from news in service.News
								join cats in service.Category on news.CategoryID equals cats.ID
								where news.ClientSiteID == SessionManager.ClientSiteID
								&& cats.DeletedOn != null
								&& (news.StartDate <= DateTime.Now || news.StartDate >= DateTime.Now)
								&& news.EndDate <= DateTime.Now
								select news).DefaultIfEmpty()
							   :
							   (from news in service.News
								join cats in service.Category on news.CategoryID equals cats.ID
								where news.ClientSiteID == SessionManager.ClientSiteID
								&& cats.DeletedOn != null
								&& news.DeletedBy == null
								&& (news.EndDate >= DateTime.Now)
								select news).DefaultIfEmpty()
							   ;

				SessionManager.iPageCount = results.Count();

				data.Add("Data", results.OrderBy(r => r.ID).Paginate(iPageNum, iPageSize).ToList());
				data.Add("Result", true);
				data.Add("Count", SessionManager.iPageCount);
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception exception) {
				data.Clear();


				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllNews(DateTime fromDate, DateTime toDate, long newsLetterID) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				List<Custom.Data.News> newsData = new List<Custom.Data.News>();

				if (toDate.Year == 1900) {
					var lastNewsLetterDate = (from newsletterhistory in service.NewsLetterCommEngine
											  where newsletterhistory.NewsLetterID == newsLetterID
											  orderby newsletterhistory.InsertedOn descending
											  select newsletterhistory.InsertedOn).FirstOrDefault();

					newsData = (from news in service.News
								where news.EndDate >= lastNewsLetterDate
								&& (news.StartDate <= fromDate || news.StartDate >= fromDate)
							   && news.ClientSiteID == SessionManager.ClientSiteID
							   && news.DeletedBy == null
							   && news.DeletedOn == null
								select news).ToList();
				} else {
					newsData = (from news in service.News
								where news.StartDate <= fromDate
								&& (news.StartDate <= DateTime.Now || news.StartDate >= DateTime.Now)
								&& news.ClientSiteID == SessionManager.ClientSiteID
								&& news.DeletedBy == null
								&& news.DeletedOn == null
								select news).ToList();
				}

				Utils.ServiceSuccessResultHandler(ref data, newsData, newsData.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SaveNews(Aurora.Custom.Data.News news) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				Aurora.Custom.Data.News otherFriendlyMatch = service.News.Where(w => w.Title == news.Title
																		&& w.ClientSiteID == SessionManager.ClientSiteID
																		&& w.DeletedOn == null
																		&& w.ID != news.ID).FirstOrDefault();
				if (otherFriendlyMatch != null) {
					data.Add("Result", false);

					data.Add("ErrorMessage", "A news item already exists with this SEO Friendly URL (" + otherFriendlyMatch.Title + " - " + otherFriendlyMatch.ID + ")");
				} else {
					//this is a new news 
					if (news.ID == -1) {
						news.ID = 0;
						news.InsertedBy = SessionManager.UserID;
						news.InsertedOn = DateTime.Now;
						news.StartDate = DateTime.Parse(news.StartDate.Value.Date.ToShortDateString() + " 00:00");
						news.EndDate = DateTime.Parse(news.EndDate.Value.Date.ToShortDateString() + " 00:00");
						news.ClientSiteID = SessionManager.ClientSiteID;

						service.AddToNews(news);

					} else {
						//update an existing news
						news.ClientSiteID = SessionManager.ClientSiteID;
						service.News.Attach(news);
						if (news.DeletedOn != null) {
							news.DeletedBy = SessionManager.UserID;
						}
						news.SetAllModified(service);
					}

					service.SaveChanges();

					Utils.ServiceSuccessResultHandler(ref data, news, 1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		#endregion

		#region EventsAED
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetEvents(int categoryId, int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();


			try {
				var events = iFilter.ToUpper() == "ARCHIVED"
							 ? from evnt in service.Event
							   join categories in service.Category on evnt.CategoryID equals categories.ID
							   where categories.ClientSiteID == SessionManager.ClientSiteID
							   && categories.FeatureModuleID == 4
							   && evnt.CategoryID == categoryId
							   && evnt.DeletedOn == null
							   && evnt.EndDate <= DateTime.Now
							   select evnt
							 :
							 from evnt in service.Event
							 join categories in service.Category on evnt.CategoryID equals categories.ID
							 where categories.ClientSiteID == SessionManager.ClientSiteID
							 && categories.FeatureModuleID == 4
							 && evnt.CategoryID == categoryId
							 && evnt.DeletedOn == null
							 && evnt.EndDate >= DateTime.Now
							 select evnt
							 ;

				Aurora.Custom.SessionManager.iPageCount = events.Count();

				data.Add("Data", events.OrderBy(r => r.ID).Paginate(iPageNum, iPageSize).ToList());
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
				data.Add("Count", Aurora.Custom.SessionManager.iPageCount);
			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> RemoveEvent(int eventId) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID; Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var events = (from evnt in service.Event
							  where evnt.ID == eventId
							  select evnt).SingleOrDefault();
				events.DeletedBy = 0;
				events.DeletedOn = DateTime.Now;

				service.SaveChanges();

				data.Add("Data", events);
			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateEvent(Custom.Data.Event eventData) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID; Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				Custom.Data.Event otherFriendlyMatch = (from e in service.Event
														join c in service.Category on e.CategoryID equals c.ID
														where c.ClientSiteID == SessionManager.ClientSiteID
																&& e.Title == eventData.Title
																&& e.DeletedOn == null
																&& e.ID != eventData.ID
														select e).FirstOrDefault();
				if (otherFriendlyMatch != null) {
					data.Add("Result", false);

					data.Add("ErrorMessage", "An event already exists with this SEO Friendly URL (" + otherFriendlyMatch.Title + " - " + otherFriendlyMatch.ID + ")");
				} else {
					var updateEvent = (from events in service.Event
									   where events.ID == eventData.ID
									   select events).SingleOrDefault();

					updateEvent.AllowReminders = eventData.AllowReminders;
					updateEvent.CategoryID = eventData.CategoryID;
					updateEvent.CustomXML = eventData.CustomXML;
					updateEvent.DeletedBy = eventData.DeletedBy;
					updateEvent.DisplayDate = eventData.DisplayDate;
					updateEvent.ImageCaption1 = eventData.ImageCaption1;
					updateEvent.ImageCaption2 = eventData.ImageCaption2;
					updateEvent.DeletedOn = eventData.DeletedOn;
					updateEvent.Details = eventData.Details;
					updateEvent.EndDate = eventData.EndDate.Value;
					updateEvent.Preview = eventData.Preview;
					updateEvent.GeoLatLong = eventData.GeoLatLong;
					updateEvent.ID = eventData.ID;
					updateEvent.InsertedBy = eventData.InsertedBy;
					updateEvent.InsertedOn = eventData.InsertedOn;
					updateEvent.Location = eventData.Location;
					updateEvent.Occurance = eventData.Occurance;
					//updateEvent.SchemaID = eventData.SchemaID;
					updateEvent.StartDate = eventData.StartDate.Value;
					updateEvent.Title = eventData.Title;


					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, eventData, 1);
				}

			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;

		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> AddEvent(Custom.Data.Event eventData) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID; Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Custom.Data.Event otherFriendlyMatch = (from e in service.Event
														join c in service.Category on e.CategoryID equals c.ID
														where c.ClientSiteID == SessionManager.ClientSiteID
																&& e.Title == eventData.Title
																&& e.DeletedOn == null
																&& e.ID != eventData.ID
														select e).FirstOrDefault();
				if (otherFriendlyMatch != null) {
					data.Add("Result", false);

					data.Add("ErrorMessage", "An event already exists with this SEO Friendly URL (" + otherFriendlyMatch.Title + " - " + otherFriendlyMatch.ID + ")");
				} else {
					eventData.InsertedBy = SessionManager.UserID;
					service.AddToEvent(eventData);
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, eventData, 1);
				}

			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		#endregion

		#region GalleriesAED
		/// <summary>
		/// Gets category related galleries
		/// </summary>
		/// <param name="categoryId">ID of the category</param>
		/// <param name="iPageNum">page to display</param>
		/// <param name="iPageSize">size of result set</param>
		/// <param name="iFilter"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetGallery(int categoryId, int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();


			try {
				var gallery = from gal in service.Gallery
							  join categories in service.Category on gal.CategoryID equals categories.ID
							  where categories.ClientSiteID == SessionManager.ClientSiteID && categories.FeatureModuleID == 5
							 && gal.DeletedOn == null
							 && categories.ID == categoryId
							  select gal;

				Aurora.Custom.SessionManager.iPageCount = gallery.Count();

				data.Add("Data", gallery.OrderBy(r => r.GalleryIndex).Paginate(iPageNum, iPageSize));
				data.Add("Count", Aurora.Custom.SessionManager.iPageCount);
			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}

		/// <summary>
		/// Updates a gallery entity
		/// </summary>
		/// <param name="galleryData">Gallery entity to update</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateGallery(Custom.Data.Gallery galleryData) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID; Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {

				if (galleryData.ID == -1) {
					galleryData.ID = 0;
					galleryData.InsertedBy = SessionManager.UserID;
					galleryData.InsertedOn = DateTime.Now;
					service.AddToGallery(galleryData);
				} else {
					if (galleryData.DeletedOn != null) {
						galleryData.DeletedBy = SessionManager.UserID;
					}
					service.Attach(galleryData);
					galleryData.SetAllModified(service);
				}

				service.SaveChanges();


				data.Add("Result", true);
				data.Add("Data", galleryData.ID);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {
				data.Add("Data", false);
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;

		}


		/// <summary>
		/// Resizes a gallery items image to a new thumbnail
		/// </summary>
		/// <param name="galleryItemID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ResizeGalleryImage(long galleryItemID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				bool success = false;
				Utils.CheckSession();
				var galleryConfig = Utils.GetFieldsFromCustomXML("GalleryImages");

				if (galleryConfig.Count > 0) {
					//check for thumbnail
					string fileName = String.Format("/ClientData/{0}/Uploads/sm_gallery_01_{1}.jpg", SessionManager.ClientSiteID, galleryItemID);
					if (!File.Exists(Server.MapPath(fileName))) {
						success = Utils.ResizeImageFromFile(Server.MapPath(fileName.Replace("sm_", string.Empty)), int.Parse(galleryConfig["GalleryImages_smWidth"]), Server.MapPath(fileName));
					}
				}

				Utils.ServiceSuccessResultHandler(ref data, galleryItemID, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Will execute a effect filter on the image
		/// </summary>
		/// <param name="methodDescription"></param>
		/// <param name="imagePath"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ApplyImageEffects(string methodDescription, string imagePath, object[] parmeters) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (!imagePath.Contains("\\")) {
					imagePath = Server.MapPath(imagePath);
				}
				ImageFX imgFX = new Custom.ImageFX(imagePath);
				MethodInfo[] methInfos = (MethodInfo[])imgFX.GetType().GetMethods();
				var x = methInfos.Where(m => m.GetCustomAttributes(typeof(DescriptionAttribute), true).First() == methodDescription);

				foreach (MethodInfo item in methInfos) {
					DescriptionAttribute[] attributes = (DescriptionAttribute[])item.GetCustomAttributes(typeof(DescriptionAttribute), false);
					if (attributes.Length > 0) {
						if (attributes[0].Description == methodDescription) {
							if (item.GetParameters().Count() > 0) {
								item.Invoke(imgFX, parmeters);
							} else {
								item.Invoke(imgFX, null);
							}
							imgFX.Save();
						}
					}

				}

				Utils.ServiceSuccessResultHandler(ref data, "", 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Gets All Effect Methods
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetImageEffects() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				ImageFX imgFX = new Custom.ImageFX(string.Empty);
				var methods = (from myMethods in imgFX.GetType().GetMethods()
							   where myMethods.GetCustomAttributes(typeof(DescriptionAttribute), true).Count() > 0
							   select new { Name = ((DescriptionAttribute)myMethods.GetCustomAttributes(typeof(DescriptionAttribute), true)[0]).Description }
								   ).ToList();
				Utils.ServiceSuccessResultHandler(ref data, methods, methods.Count);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region PageAED
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPages(int? pageId) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (pageId == 0) {

					//GetChldPageList
					Utils.CheckSqlDBConnection();
					using (SqlCommand cmd = new SqlCommand()) {
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = "[Features].[proc_PageContent_GetPagesWithChildStatus]";
						cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
						cmd.Parameters.AddWithValue("@IsAdmin", 1);
						DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

						var pageList = from tb in ds.Tables[0].AsEnumerable()
									   select new {
										   ID = tb.Field<int?>("ID"),
										   Category = tb.Field<long?>("CategoryID"),
										   ParentId = tb.Field<long?>("ParentID"),
										   ClientSiteID = tb.Field<long?>("ClientSiteID"),
										   ShortTitle = tb.Field<string>("ShortTitle"),
										   LongTitle = tb.Field<string>("LongTitle"),
										   Description = tb.Field<string>("Description"),
										   PageContent = tb.Field<string>("PageContent"),
										   PageIndex = tb.Field<int?>("PageIndex"),
										   isArchived = tb.Field<bool?>("isArchived"),
										   isDefault = tb.Field<bool?>("isDefault"),
										   MetaKeywords = tb.Field<string>("MetaKeywords"),
										   MetaDescription = tb.Field<string>("MetaDescription"),
										   CustomXML = tb.Field<string>("CustomXML"),
										   InsertedOn = tb.Field<DateTime?>("InsertedOn"),
										   InsertedBy = tb.Field<long?>("InsertedBy"),
										   DeletedOn = tb.Field<DateTime?>("DeletedOn"),
										   DeletedBy = tb.Field<long?>("DeletedBy"),
										   HasChildren = tb.Field<int?>("HasChildren"),
										   RedirectURL = tb.Field<string>("RedirectURL"),
										   MenuClass = tb.Field<string>("MenuClass")
									   };


						data.Add("Data", pageList.OrderBy(o => o.PageIndex != null ? Math.Abs((decimal)o.PageIndex) : 0));
						data.Add("Result", true);
						data.Add("ErrorMessage", string.Empty);
					}




				} else {
					//Single page
					var page = from pages in service.PageContent
							   where pages.ID == pageId && pages.DeletedOn == null
							   select pages;

					data.Add("Result", true);
					data.Add("Data", page.SingleOrDefault());
					data.Add("ErrorMessage", string.Empty);
				}


			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPagesBySearch(string filter) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var page = from pages in service.PageContent.Where(w => w.ClientSiteID == SessionManager.ClientSiteID).ToList()
						   where pages.isArchived == false && 
						   pages.DeletedOn == null &&
						   pages.ParentId != null &&
						   (pages.ShortTitle.Contains(filter)
						   || pages.LongTitle.Contains(filter)
						   || pages.Description.Contains(filter)
						   || pages.ID.ToString().Contains(filter)
						   || pages.Description.Replace(' ', '-').Contains(filter))
						   select new {
							   ID = pages.ID,
							   Category = pages.CategoryID,
							   ParentID = pages.ParentId,
							   ClientSiteID = pages.ClientSiteID,
							   ShortTitle = pages.ShortTitle,
							   LongTitle = pages.LongTitle,
							   Description = pages.Description,
							   PageContent = pages.PageContent1,
							   PageIndex = pages.PageIndex,
							   RedirectURL = pages.RedirectURL,
							   HasChildren = 0,
							   isDefault = 1
						   };

				data.Add("Data", page.OrderBy(o => o.PageIndex != null ? Math.Abs((decimal)o.PageIndex) : 0));
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception ex) {
				
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPagesByParent(int pageid) {
			Utils.CheckSession();
			Utils.CheckSqlDBConnection();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[Features].[proc_PageContent_GetPagesWithChildStatus]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					cmd.Parameters.AddWithValue("@ParentId", pageid);
					cmd.Parameters.AddWithValue("@IsAdmin", 1);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					var pageList = from tb in ds.Tables[0].AsEnumerable()
								   select new {
									   ID = tb.Field<int?>("ID"),
									   Category = tb.Field<long?>("CategoryID"),
									   ParentId = tb.Field<long?>("ParentID"),
									   ClientSiteID = tb.Field<long?>("ClientSiteID"),
									   ShortTitle = tb.Field<string>("ShortTitle"),
									   LongTitle = tb.Field<string>("LongTitle"),
									   Description = tb.Field<string>("Description"),
									   PageContent = tb.Field<string>("PageContent"),
									   PageIndex = tb.Field<int>("PageIndex"),
									   isArchived = tb.Field<bool?>("isArchived"),
									   isDefault = tb.Field<bool?>("isDefault"),
									   MetaKeywords = tb.Field<string>("MetaKeywords"),
									   MetaDescription = tb.Field<string>("MetaDescription"),
									   CustomXML = tb.Field<string>("CustomXML"),
									   InsertedOn = tb.Field<DateTime?>("InsertedOn"),
									   InsertedBy = tb.Field<long?>("InsertedBy"),
									   DeletedOn = tb.Field<DateTime?>("DeletedOn"),
									   DeletedBy = tb.Field<long?>("DeletedBy"),
									   HasChildren = tb.Field<int?>("HasChildren"),
									   MenuClass = tb.Field<string>("MenuClass"),
									   RedirectURL = tb.Field<string>("RedirectURL")
								   };


					data.Add("Data", pageList.OrderBy(o => o.PageIndex != null ? Math.Abs((decimal)o.PageIndex) : 0));
					data.Add("Result", true);
					data.Add("ErrorMessage", string.Empty);
				}

			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdatePage(PageContent page) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			try {
				PageContent otherFriendlyMatch = service.PageContent.Where(w => w.Description == page.Description
																		&& w.ClientSiteID == SessionManager.ClientSiteID
																		&& w.isArchived != true
																		&& w.DeletedOn == null
																		&& w.ID != page.ID).FirstOrDefault();
				if (otherFriendlyMatch != null) {
					data.Add("Result", false);

					data.Add("ErrorMessage", "A page already exists with this SEO Friendly URL (" + otherFriendlyMatch.ShortTitle + " - " + otherFriendlyMatch.ID + ")");
				} else {
					if (page.ID == -1) {
						page.ID = 0;
						page.InsertedBy = SessionManager.UserID;
						page.InsertedOn = DateTime.Now;
						page.Description = page.Description.ToLower();
						service.AddToPageContent(page);

					} else {
						page.Description = page.Description.ToLower();

						//check if page is default
						if (page.isDefault) {
							var oldDefaultPage = from defaultPage in service.PageContent
												 where defaultPage.isDefault == true
												 && defaultPage.isArchived == false
												 && defaultPage.DeletedOn == null
												 && defaultPage.ClientSiteID == SessionManager.ClientSiteID
												 select defaultPage;

							if (oldDefaultPage.Count() != 0) {
								oldDefaultPage.First().isDefault = false;
								service.SaveChanges();
							}

						}
						//update an existing page
						var oldPage = from pg in service.PageContent
									  where pg.ID == page.ID && pg.DeletedOn == null
									  select pg;

						var oldVersions = from pg in service.PageContent
										  where pg.ParentId == page.ID
										  && pg.DeletedOn == null
										  && pg.isArchived == true
										  orderby pg.InsertedOn ascending
										  select pg;

						//Versioning up to three versions
						if (oldPage.Count() >= 1 && oldVersions.Count() < 3) {
							if (page.DeletedOn != null) {
								page.DeletedBy = SessionManager.UserID;
								service.Attach(page);
								page.SetAllModified(service);
								service.SaveChanges();

								data.Add("Result", true);

								data.Add("ErrorMessage", string.Empty);

								return data;
							}
							PageContent lastUpdated = oldPage.First();
							service.Detach(oldPage.First());
							lastUpdated.EntityKey = null;
							lastUpdated.isArchived = true;
							lastUpdated.ParentId = page.ID;
							lastUpdated.InsertedOn = DateTime.Now;
							service.SaveChanges();
							service.AddToPageContent(lastUpdated);
						} else {
							if (page.DeletedOn != null) {
								page.DeletedBy = SessionManager.UserID;
								service.Attach(page);
								page.SetAllModified(service);
								service.SaveChanges();
								data.Add("Result", true);

								data.Add("ErrorMessage", string.Empty);
								return data;

							}
							PageContent lastUpdated = oldPage.First();
							service.DeleteObject(oldVersions.First());
							service.Detach(oldPage.First());
							service.SaveChanges();

							lastUpdated.isArchived = true;
							lastUpdated.ParentId = page.ID;
							lastUpdated.InsertedOn = DateTime.Now;
							lastUpdated.EntityKey = null;
							service.AddToPageContent(lastUpdated);

						}

						if (page.ClientSiteID == null)
							page.ClientSiteID = SessionManager.ClientSiteID;
						page.InsertedOn = DateTime.Now;
						service.Attach(page);
						page.SetAllModified(service);

					}

					service.SaveChanges();

					data.Add("Result", true);

					data.Add("ErrorMessage", string.Empty);
				}
			} catch (Exception exception) {
				data.Clear();


				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPagesForReorder() {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var pageList = from p in service.PageContent
							   where p.ClientSiteID == SessionManager.ClientSiteID &&
							   p.ParentId == 0 &&
							   p.isArchived == false &&
							   p.DeletedOn == null
							   select p;
				var childPageList = (from cp in service.PageContent
									 where cp.ClientSiteID == SessionManager.ClientSiteID &&
										   cp.ParentId != 0 &&
										   cp.isArchived == false &&
										   cp.DeletedOn == null

									 orderby Math.Abs((decimal)cp.PageIndex)
									 select cp).DefaultIfEmpty();
				data.Add("Children", childPageList.ToList().OrderBy(o => o != null ? Math.Abs((decimal)o.PageIndex) : 0));
				data.Add("Data", pageList.ToList().OrderBy(o => Math.Abs((decimal)o.PageIndex)));
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);

			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ReorderPages(dynamic obj) {
			Utils.CheckSession();
			SessionManager.ClientSiteID = SessionManager.ClientSiteID;
			Dictionary<string, object> data = new Dictionary<string, object>();
			service.Connection.Open();
			using (DbTransaction transaction = service.Connection.BeginTransaction()) {
				try {
					Utils.RearrangePages(ref service, obj, 0);
					service.SaveChanges();
					transaction.Commit();
				} catch (Exception ex) {
					transaction.Rollback();
					Utils.ServiceErrorResultHandler(ex, ref data);
				}
			}

			return data;
		}
		#endregion

		#region CategoriesAED
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCategories(int iPageSize, int iPageNum, string iFilter, int moduleId, int categoryID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var categs = categoryID == 0 ?
							 from cats in service.Category
							 where cats.ClientSiteID == SessionManager.ClientSiteID
							 && cats.FeatureModuleID == moduleId
							 && cats.DeletedBy == null
							 && cats.DeletedOn == null
							 && (cats.IsHidden == false || cats.IsHidden == null)
							 orderby cats.OrderIndex ascending
							 select cats
							 :
							 from cats in service.Category
							 where cats.ClientSiteID == SessionManager.ClientSiteID
							 && cats.FeatureModuleID == moduleId
							 && cats.DeletedBy == null
							 && cats.DeletedOn == null
							 && (cats.IsHidden == false || cats.IsHidden == null)
							 && cats.ID == categoryID
							 select cats
							 ;
				if (iPageNum != 0) {
					data.Add("Data", categs.Paginate(iPageNum, iPageSize).ToList());
				} else {
					data.Add("Data", categs.ToList());
				}
				data.Add("Result", true);
				data.Add("Count", categs.Count());
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetEditCategories(int iPageSize, int iPageNum, string iFilter, int moduleId, int categoryID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var categs = categoryID == 0 ?
							 from cats in service.Category
							 where cats.ClientSiteID == SessionManager.ClientSiteID
							 && cats.FeatureModuleID == moduleId
							 && cats.DeletedBy == null
							 && cats.DeletedOn == null
							 orderby cats.OrderIndex ascending
							 select cats
							 :
							 from cats in service.Category
							 where cats.ClientSiteID == SessionManager.ClientSiteID
							 && cats.FeatureModuleID == moduleId
							 && cats.DeletedBy == null
							 && cats.DeletedOn == null
							 && cats.ID == categoryID
							 select cats
							 ;

				if (!string.IsNullOrEmpty(iFilter)) {
					categs = categs.Where(t => t.Description == iFilter);
				}

				if (iPageNum != 0) {
					Utils.ServiceSuccessResultHandler(ref data, categs.Paginate(iPageNum, iPageSize).ToList(), categs.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, categs.ToList(), categs.Count());
				}

			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateCategory(Category category) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (category.ID == 0) {
					var categories = from cats in service.Category
									 where cats.Name == category.Name
										   && cats.ClientSiteID == SessionManager.ClientSiteID
										   && cats.FeatureModuleID == category.FeatureModuleID
										   && cats.ParentID == category.ParentID
										   && cats.DeletedBy == null
										   && cats.DeletedOn == null
									 select cats;

					if (categories.Count() == 0) {
						category.ClientSiteID = SessionManager.ClientSiteID;
						category.InsertedBy = int.Parse(SessionManager.UserID.ToString());
						category.InsertedOn = DateTime.Now;
						service.AddToCategory(category);
						service.SaveChanges();
					} else {
						Utils.ServiceSuccessResultHandler(ref data, categories, 1);
						return data;
					}
				} else {
					category.ClientSiteID = SessionManager.ClientSiteID;
					service.Attach(category);
					category.SetAllModified(service);
					service.SaveChanges();
				}

				Utils.ServiceSuccessResultHandler(ref data, category, 0);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> DeleteCategory(long categoryID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var category = (from categories in service.Category
								where categories.ID == categoryID && categories.ClientSiteID == SessionManager.ClientSiteID
								select categories).FirstOrDefault();
				if (category != null) {
					category.DeletedOn = DateTime.Now;
					category.DeletedBy = (int)SessionManager.UserID;
					service.SaveChanges();
				}
				Utils.ServiceSuccessResultHandler(ref data, category, 0);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}
		#endregion

		#region Testmonies
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetFeedBack(int iPageNum, int iPageSize, string iFilter) {
			SessionManager.ClientSiteID = SessionManager.ClientSiteID; Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			bool isPending = iFilter.ToUpper() == "PENDING" ? true : false;
			bool isDeclined = iFilter.ToUpper() == "DECLINED" ? true : false;

			try {
				IOrderedQueryable<CustomerFeedback> feedback;
				//Pending
				if (isPending) {
					feedback = from t in service.CustomerFeedback
							   where t.isApproved == false
									&& t.DeletedOn == null
									&& t.ClientSiteID == SessionManager.ClientSiteID
							   orderby t.InsertedOn
							   select t;
				}
					//Declined
				else if (isDeclined) {
					feedback = from t in service.CustomerFeedback
							   where t.DeletedOn != null
							   && t.ClientSiteID == SessionManager.ClientSiteID
							   orderby t.DeletedOn
							   select t;

				}
					//Active                
				else {
					feedback = from t in service.CustomerFeedback
							   where t.DeletedOn == null &&
							   t.isApproved == true
							   && t.ClientSiteID == SessionManager.ClientSiteID
							   orderby t.ApprovedOn descending
							   select t;
				}

				Utils.ServiceSuccessResultHandler(ref data, feedback.Paginate(iPageNum, iPageSize), feedback.Count());
				//if (!isDeclined)
				//{

				//    var feedback = from cusfeedback in service.CustomerFeedback
				//                   where cusfeedback.isApproved == isPending
				//                         && cusfeedback.DeletedOn == null
				//                         && cusfeedback.ClientSiteID == SessionManager.ClientSiteID
				//                   orderby cusfeedback.ApprovedOn descending
				//                   select cusfeedback;

				//    Utils.ServiceSuccessResultHandler(ref data, feedback.Paginate(iPageNum, iPageSize), feedback.Count());
				//}
				//else
				//{
				//    var feedback = from cusfeedback in service.CustomerFeedback
				//                   where cusfeedback.isApproved == false
				//                         && cusfeedback.DeletedOn == null
				//                         && cusfeedback.ClientSiteID == SessionManager.ClientSiteID
				//                   select cusfeedback;

				//    data.Add("Data", feedback.ToList());
				//    data.Add("Result", true);
				//    data.Add("ErrorMessage", string.Empty);
				//}
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateFeedback(CustomerFeedback customerFeedBack) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				CustomerFeedback dbFeed = (from x in service.CustomerFeedback
										   where x.ID == customerFeedBack.ID
										   select x).FirstOrDefault();
				if (dbFeed != null) {
					if (customerFeedBack.isApproved == true) {
						dbFeed.ApprovedOn = DateTime.Now;
						dbFeed.ApprovedBy = SessionManager.UserID;
						dbFeed.DeletedOn = null;
						dbFeed.DeletedBy = null;
						dbFeed.isApproved = true;
					} else if (customerFeedBack.isApproved == false) {
						dbFeed.ApprovedBy = null;
						dbFeed.ApprovedOn = null;
						dbFeed.DeletedOn = DateTime.Now;
						dbFeed.DeletedBy = SessionManager.UserID;
						dbFeed.isApproved = false;
					}
					dbFeed.FirstName = customerFeedBack.FirstName;
					dbFeed.LastName = customerFeedBack.LastName;
					dbFeed.Email = customerFeedBack.Email;
					dbFeed.ContactNumber = customerFeedBack.ContactNumber;
					dbFeed.Preview = customerFeedBack.Preview;
					dbFeed.Comment = customerFeedBack.Comment;

					service.SaveChanges();

					Utils.ServiceSuccessResultHandler(ref data, true, 0);

				}
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		#endregion

		#region Modules
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetModules() {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string refererURL = string.Empty;
				if (HttpContext.Current.Request.Headers["Referer"].Substring(HttpContext.Current.Request.Headers["Referer"].IndexOf("=") + 1).Length >= 5) {
					refererURL = HttpContext.Current.Request.Headers["Referer"].Substring(
						   HttpContext.Current.Request.Headers["Referer"].IndexOf("=") + 1, 5);
					if (refererURL == "http:") {
						refererURL = "0";
					}
				} else {
					refererURL = "0";
				}
				long? clientID = (long?)Utils.ConvertDBNull(refererURL, 0) != 0
								 ? (long?)Utils.ConvertDBNull(refererURL, 0)
								 :
								 SessionManager.ClientSiteID
								 ;

				var query = from modules in service.ClientModule
							join moduleFeatures in service.Module on modules.FeaturesModuleID equals moduleFeatures.ID
							where modules.ClientSiteID == clientID
						   && moduleFeatures.isDefault == false
						   //&& moduleFeatures.FriendlyName != null
						   //&& moduleFeatures.DisplayOnSite == true
						   && modules.DeletedOn == null
						   && modules.DeletedBy == null
							orderby moduleFeatures.FriendlyName ascending
							select modules;

				Utils.ServiceSuccessResultHandler(ref data, query.ToList(), query.Count());
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetModulesByClientModuleID(long clientModuleID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var query = from modules in service.ClientModule
							join moduleFeatures in service.Module on modules.FeaturesModuleID equals moduleFeatures.ID
							where modules.ClientSiteID == SessionManager.ClientSiteID
						   && moduleFeatures.isDefault == false
						   //&& moduleFeatures.DisplayOnSite == true  - removing this as I do not believe it is needed. This was included as a possible feature to not display certain modules to the client but the module is visisble where this is being called anyway
						   && modules.DeletedOn == null
						   && modules.DeletedBy == null
						   && modules.ID == clientModuleID
							orderby moduleFeatures.FriendlyName ascending
							select new { modules, moduleFeatures };

				Utils.ServiceSuccessResultHandler(ref data, query.ToList(), query.Count());
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllModules() {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var query = (from modules in service.Module
							where modules.isDefault == false
							&& modules.FriendlyName != null
                            orderby modules.FriendlyName ascending
							select new { modules.ID, modules.SchemaID, modules.Name, modules.FriendlyName, modules.Description, modules.BasePrice, modules.PriceUnit, modules.isDefault, modules.DisplayOnSite, modules.HTML, modules.StylesFilePath, modules.JScriptFilePath, modules.InsertedOn, modules.InsertedBy, modules.DeletedOn, modules.DeletedBy, modules.ConfigXML, modules.AppendTemplatesToBody, modules.IsClassBased, modules.IsVisibleToClient });


				Utils.ServiceSuccessResultHandler(ref data, query.ToList(), query.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> AddModulesToClient(string[] items, long ClientID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				//get all the clients current modules
				string refererURL = HttpContext.Current.Request.Headers["Referer"].Substring(HttpContext.Current.Request.Headers["Referer"].IndexOf("=") + 1, 5);
				long? clientID = (long?)Utils.ConvertDBNull(refererURL, 0) != 0
								 ? (long?)Utils.ConvertDBNull(refererURL, 0)
								 :
								 SessionManager.ClientSiteID
								 ;

				var query = from modules in service.ClientModule
							join moduleFeatures in service.Module on modules.FeaturesModuleID equals moduleFeatures.ID
							where modules.ClientSiteID == clientID
								  && moduleFeatures.isDefault == false
								  //&& moduleFeatures.FriendlyName != null
								  //&& moduleFeatures.DisplayOnSite == true
								  && modules.DeletedBy == null
								  && modules.DeletedOn == null
							orderby moduleFeatures.FriendlyName ascending
							select moduleFeatures;

				//Delete any items that were removed
				var index = 0;
				foreach (Aurora.Custom.Data.Module module in query) {
					if (items.Where(t => t == module.ID.ToString()).Count() == 0) {
						var clientmodule = (from clientModules in service.ClientModule
											where clientModules.FeaturesModuleID == module.ID
											&& clientModules.ClientSiteID == clientID
											select clientModules).FirstOrDefault();

						clientmodule.DeletedBy = SessionManager.UserID;
						clientmodule.DeletedOn = DateTime.Now;

						clientmodule.SetAllModified(service);
					}
					index++;
				}

				//add and check items
				foreach (string moduleID in items) {
					if (moduleID != null) {
						int modID = Utils.ConvertDBNull(int.Parse(moduleID), 0);

						if (!String.IsNullOrEmpty(moduleID) && query.Where(mod => mod.ID == modID).Count() == 0) {
							//undelete item if any
							long? moduID = long.Parse(moduleID);

							var undelete = from modules in service.ClientModule
										   where modules.ClientSiteID == clientID
											&& modules.FeaturesModuleID == moduID
										   select modules;

							if (undelete.Count() == 0) {
								ClientModule module = new ClientModule {
									ClientSiteID = ClientID,
									ConfigXML = null,
									DeletedBy = null,
									DeletedOn = null,
									FeaturesModuleID = long.Parse(moduleID),
									HTML = null,
									Inserted = DateTime.Now,
									InsertedBy = SessionManager.UserID

								};

								service.AddToClientModule(module);
							} else {
								undelete.First().DeletedBy = null;
								undelete.First().DeletedOn = null;
								undelete.First().SetAllModified(service);
							}

						}
					}

				}


				service.SaveChanges();

				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {


				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetModuleConfig(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var module = from clientModules in service.ClientModule
							 join featureModules in service.Module on clientModules.FeaturesModuleID equals featureModules.ID
							 where clientModules.ClientSiteID == SessionManager.ClientSiteID
							 && clientModules.DeletedBy == null
							 && clientModules.DeletedOn == null
							 orderby featureModules.FriendlyName ascending
							 select new { clientModules, featureModules };
				if (iFilter == "page") {
					Utils.ServiceSuccessResultHandler(ref data, new List<dynamic>(module).Paginate(iPageNum, iPageSize), module.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, new List<dynamic>(module), module.Count());
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetClientModuleConfig(int iPageNum, int iPageSize, string iFilter) {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var module = from clientModules in service.ClientModule
                             join featureModules in service.Module on clientModules.FeaturesModuleID equals featureModules.ID
                             where clientModules.ClientSiteID == SessionManager.ClientSiteID
                             && featureModules.IsVisibleToClient == true
                             && clientModules.DeletedBy == null
                             && clientModules.DeletedOn == null
                             orderby featureModules.FriendlyName ascending
                             select new { clientModules, featureModules };
                if (iFilter == "page") {
                    Utils.ServiceSuccessResultHandler(ref data, new List<dynamic>(module).Paginate(iPageNum, iPageSize), module.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, new List<dynamic>(module), module.Count());
                }

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }


		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateModuleProperties(long clientmoduleID, string xmlConfig) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				var clientModule = (from clm in service.ClientModule
									where clm.ID == clientmoduleID
									select clm).FirstOrDefault();

				if (clientModule != null) {
					clientModule.ConfigXML = xmlConfig;
					clientModule.SetAllModified(service);
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, clientModule, 1);
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateModuleTemplate(long clientmoduleID, string htmlTemplate) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				var clientModule = (from clm in service.ClientModule
									where clm.ID == clientmoduleID
									select clm).FirstOrDefault();

				if (clientModule != null) {
					clientModule.HTML = htmlTemplate;
					clientModule.SetAllModified(service);
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, clientModule, 1);
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region BannerAED
		/// <summary>
		/// Retrives all banners for a particular client
		/// </summary>
		/// <param name="iPageNum">Number of pages to display</param>
		/// <param name="iPageSize">Page size</param>
		/// <param name="iFilter">The type of Banner to retrieve</param>
		/// <param name="bannerID">ID onf the banner</param>
		/// <param name="isArchived">true if the banner is Archived</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBanners(int iPageNum, int iPageSize, string iFilter, int bannerID, bool isArchived) {
			Utils.CheckSession();

			Dictionary<string, object> data = new Dictionary<string, object>();


			try {
				if (bannerID != 0) {
					var bannersData = from banner in service.AdBanner
									  join farms in service.AdFarm on banner.FarmID equals farms.farmid
									  where banner.ClientSiteID == SessionManager.ClientSiteID
											&& banner.ValidToDate >= DateTime.Now
											&& banner.BannerID == bannerID

									  orderby banner.Name, banner.Weight ascending
									  select new {
										  banner.BannerID,
										  banner.BannerURL,
										  banner.CategoryID,
										  banner.ClickCount,
										  banner.ClientSiteID,
										  banner.FarmID,
										  banner.MaxClicks,
										  banner.MaxImpressions,
										  banner.Name,
										  banner.ShowCount,
										  banner.UnderClickCount,
										  banner.UnderURL,
										  banner.ValidFromDate,
										  banner.ValidToDate,
										  banner.Weight,
										  banner.xsize,
										  banner.ysize,
										  FarmName = farms.name,
										  BannerSizeXML = (from schema in service.ClientModule
														   where schema.ClientSiteID == SessionManager.ClientSiteID
														   && schema.FeaturesModuleID == 7
														   select schema).FirstOrDefault().ConfigXML,
										  BannerButtonSizeXML = (from schema in service.ClientModule
																 where schema.ClientSiteID == SessionManager.ClientSiteID
																 && schema.FeaturesModuleID == 8
																 select schema).FirstOrDefault().ConfigXML
									  };

					data.Add("Data", new List<dynamic>(bannersData.ToList()));
				} else {
					var bannersData = isArchived
									? from banner in service.AdBanner
									  join farms in service.AdFarm on banner.FarmID equals farms.farmid
									  where banner.ClientSiteID == SessionManager.ClientSiteID
											&& banner.ValidToDate <= DateTime.Now

									  orderby banner.Weight ascending
									  select new {
										  banner.BannerID,
										  banner.BannerURL,
										  banner.CategoryID,
										  banner.ClickCount,
										  banner.ClientSiteID,
										  banner.FarmID,
										  banner.MaxClicks,
										  banner.MaxImpressions,
										  banner.Name,
										  banner.ShowCount,
										  banner.UnderClickCount,
										  banner.UnderURL,
										  banner.ValidFromDate,
										  banner.ValidToDate,
										  banner.Weight,
										  banner.xsize,
										  banner.ysize,
										  FarmName = farms.name,
										  BannerSizeXML = (from schema in service.ClientModule
														   where schema.ClientSiteID == SessionManager.ClientSiteID
														   && schema.FeaturesModuleID == 7
														   select schema).FirstOrDefault().ConfigXML,
										  BannerButtonSizeXML = (from schema in service.ClientModule
																 where schema.ClientSiteID == SessionManager.ClientSiteID
																 && schema.FeaturesModuleID == 8
																 select schema).FirstOrDefault().ConfigXML
									  }
									  :
									  from banner in service.AdBanner
									  join farms in service.AdFarm on banner.FarmID equals farms.farmid
									  where banner.ClientSiteID == SessionManager.ClientSiteID
											&& banner.ValidToDate >= DateTime.Now


									  orderby banner.Weight ascending
									  select new {
										  banner.BannerID,
										  banner.BannerURL,
										  banner.CategoryID,
										  banner.ClickCount,
										  banner.ClientSiteID,
										  banner.FarmID,
										  banner.MaxClicks,
										  banner.MaxImpressions,
										  banner.Name,
										  banner.ShowCount,
										  banner.UnderClickCount,
										  banner.UnderURL,
										  banner.ValidFromDate,
										  banner.ValidToDate,
										  banner.Weight,
										  banner.xsize,
										  banner.ysize,
										  FarmName = farms.name,
										  BannerSizeXML = (from schema in service.ClientModule
														   where schema.ClientSiteID == SessionManager.ClientSiteID
														   && schema.FeaturesModuleID == 7
														   select schema).FirstOrDefault().ConfigXML,
										  BannerButtonSizeXML = (from schema in service.ClientModule
																 where schema.ClientSiteID == SessionManager.ClientSiteID
																 && schema.FeaturesModuleID == 8
																 select schema).FirstOrDefault().ConfigXML
									  }
									  ;

					data.Add("Data", new List<dynamic>(bannersData.Paginate(iPageNum, iPageSize).ToList().OrderBy(banner => banner.Name).ThenBy(banner => banner.Weight)));
					data.Add("Count", bannersData.Count());
				}

				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		/// <summary>
		/// Gets the banners by the type eg . Banner,Bottom,Button
		/// </summary>
		/// <param name="iPageNum">Number of pages to display</param>
		/// <param name="iPageSize">Page size</param>
		/// <param name="iFilter">The type of Banner to retrieve</param>
		/// <param name="bannerID">ID onf the banner</param>
		/// <param name="isArchived">true if the banner is Archived</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBannersByType(int iPageNum, int iPageSize, string iFilter, int bannerID, bool isArchived) {
			Utils.CheckSession();
			string ifil = String.Empty;
			Dictionary<string, object> data = new Dictionary<string, object>();
			if (Session["iFilter"] != null) {

				ifil = Session["iFilter"].ToString();

			}
			Session.Add("iFilter", iFilter);
			if (Session["iFilter"].ToString() == string.Empty || Session["iFilter"].ToString() == "") {
				iFilter = null;
			} else {
				Session.Add("iFilter", iFilter);
			}



			int farmID = Utils.ConvertDBNull(iFilter, 0);


			try {
				if (bannerID != 0) {
					var bannersData = from banner in service.AdBanner
									  join farms in service.AdFarm on banner.FarmID equals farms.farmid
									  where banner.ClientSiteID == SessionManager.ClientSiteID
											&& banner.ValidToDate >= DateTime.Now
											&& banner.BannerID == bannerID

									  orderby banner.Name, banner.Weight
									  select new {
										  banner.BannerID,
										  banner.BannerURL,
										  banner.CategoryID,
										  banner.ClickCount,
										  banner.ClientSiteID,
										  banner.FarmID,
										  banner.MaxClicks,
										  banner.MaxImpressions,
										  banner.Name,
										  banner.ShowCount,
										  banner.UnderClickCount,
										  banner.UnderURL,
										  banner.ValidFromDate,
										  banner.ValidToDate,
										  banner.Weight,
										  banner.xsize,
										  banner.ysize,
										  FarmName = farms.name,
										  BannerSizeXML = (from schema in service.ClientModule
														   where schema.ClientSiteID == SessionManager.ClientSiteID
														   && schema.FeaturesModuleID == 7
														   select schema).FirstOrDefault().ConfigXML,
										  BannerButtonSizeXML = (from schema in service.ClientModule
																 where schema.ClientSiteID == SessionManager.ClientSiteID
																 && schema.FeaturesModuleID == 8
																 select schema).FirstOrDefault().ConfigXML
									  };

					data.Add("Data", new List<dynamic>(bannersData.ToList()));
				} else {
					var bannersData = isArchived
									  ? from banner in service.AdBanner
										let FarmFilter = farmID
										join farms in service.AdFarm on banner.FarmID equals farms.farmid
										where banner.ClientSiteID == SessionManager.ClientSiteID
											  && banner.ValidToDate <= DateTime.Now
											  && banner.FarmID == farmID
										orderby banner.Weight ascending
										select new {
											banner.BannerID,
											banner.BannerURL,
											banner.CategoryID,
											banner.ClickCount,
											banner.ClientSiteID,
											banner.FarmID,
											banner.MaxClicks,
											banner.MaxImpressions,
											banner.Name,
											banner.ShowCount,
											banner.UnderClickCount,
											banner.UnderURL,
											banner.ValidFromDate,
											banner.ValidToDate,
											banner.Weight,
											banner.xsize,
											banner.ysize,
											FarmName = farms.name,
											BannerSizeXML = (from schema in service.ClientModule
															 where schema.ClientSiteID == SessionManager.ClientSiteID
															 && schema.FeaturesModuleID == 7
															 select schema).FirstOrDefault().ConfigXML,
											BannerButtonSizeXML = (from schema in service.ClientModule
																   where schema.ClientSiteID == SessionManager.ClientSiteID
																   && schema.FeaturesModuleID == 8
																   select schema).FirstOrDefault().ConfigXML
										}
										:
										from banner in service.AdBanner
										join farms in service.AdFarm on banner.FarmID equals farms.farmid
										where banner.ClientSiteID == SessionManager.ClientSiteID
											  && banner.ValidToDate >= DateTime.Now
											  && banner.FarmID == farmID

										orderby banner.Weight ascending
										select new {
											banner.BannerID,
											banner.BannerURL,
											banner.CategoryID,
											banner.ClickCount,
											banner.ClientSiteID,
											banner.FarmID,
											banner.MaxClicks,
											banner.MaxImpressions,
											banner.Name,
											banner.ShowCount,
											banner.UnderClickCount,
											banner.UnderURL,
											banner.ValidFromDate,
											banner.ValidToDate,
											banner.Weight,
											banner.xsize,
											banner.ysize,
											FarmName = farms.name,
											BannerSizeXML = (from schema in service.ClientModule
															 where schema.ClientSiteID == SessionManager.ClientSiteID
															 && schema.FeaturesModuleID == 7
															 select schema).FirstOrDefault().ConfigXML,
											BannerButtonSizeXML = (from schema in service.ClientModule
																   where schema.ClientSiteID == SessionManager.ClientSiteID
																   && schema.FeaturesModuleID == 8
																   select schema).FirstOrDefault().ConfigXML
										}
										;

					data.Add("Data", new List<dynamic>(bannersData.Paginate(iPageNum, iPageSize).ToList().OrderBy(banner => banner.Name).ThenBy(banner => banner.Weight)));
					data.Add("Count", bannersData.Count());
				}

				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateBanners(AdBanner banner, int[] zoneItems, bool isDelete) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				//Defaults
				banner.ClientSiteID = SessionManager.ClientSiteID;
				banner.AdvID = 0;
				//Create
				if (banner.BannerID == -1) {

					service.AddToAdBanner(banner);
					service.SaveChanges();
				}
					//Update
				else {
					if (!isDelete) {

						service.AdBanner.Attach(banner);
						banner.SetAllModified(service);
						service.SaveChanges();


					} else {
						var deleteBanner = (from ban in service.AdBanner
											where ban.BannerID == banner.BannerID
											select ban).SingleOrDefault();
						if (deleteBanner != null)
							service.DeleteObject(deleteBanner);
					}

				}

				if (!isDelete) {
					//get Zones and perform a clean
					var zones = from zoneBanner in service.AdBannerZone
								where zoneBanner.bannerid == banner.BannerID
								select zoneBanner;

					//clean up zones
					foreach (AdBannerZone adBannerZone in zones) {
						if (!zoneItems.Contains(adBannerZone.zoneid)) {
							service.DeleteObject(adBannerZone);
						}
					}

					//add zones
					foreach (int zoneItem in zoneItems) {
						if (zones.Where(z => z.zoneid == zoneItem).Count() == 0) {
							AdBannerZone newZone = new AdBannerZone { bannerid = banner.BannerID, zoneid = zoneItem };
							service.AddToAdBannerZone(newZone);
							service.SaveChanges();
						}

					}
				}
				//commit changes
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, banner, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		/// <summary>
		/// Gets banner zones for a given banner
		/// </summary>
		/// <param name="bannerID">Id of the banner </param>
		/// <returns>A collection of all zones were  a given banner should appear </returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBannerZones(int bannerID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var zones = from zoneBanner in service.AdBannerZone
							where zoneBanner.bannerid == bannerID
							select zoneBanner;

				data.Add("Data", zones.ToList());
				data.Add("Result", true);
				data.Add("Exception", string.Empty);
			} catch (Exception er) {


				data.Add("Exception", er.Message);
			}
			return data;
		}
		/// <summary>
		/// Gets all pages in a sites where a banner can be placed
		/// </summary>
		/// <returns>On success , returns a collection of pages or an error  </returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllPageBanners() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSqlDBConnection();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[Features].[proc_PageBanners_GetPageHierarchy]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					data.Add("Data", ds.Tables[0]);
					data.Add("Result", true);
					data.Add("Exception", string.Empty);
				}
			} catch (Exception err) {
				data.Add("Error Message ", err);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBannerClicks(int iPageNum, int iPageSize, string Filter, int bannerID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				var bannerClicks = from banner in service.AdClick
								   where banner.BannerID == bannerID
								   orderby banner.ClickDate descending
								   select banner;

				data.Add("Data", bannerClicks.Paginate(iPageNum, iPageSize).ToList());
				data.Add("Count", bannerClicks.Count());
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception er) {


				data.Add("ErrorMessage", er.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBannerConfig() {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var bannerConfig = from bannerConf in service.ClientModule
								   where bannerConf.ClientSiteID == SessionManager.ClientSiteID
										 && (bannerConf.FeaturesModuleID == 7 || bannerConf.FeaturesModuleID == 8)
										 && bannerConf.ConfigXML != null
								   select bannerConf.ConfigXML;

				data.Add("Data", bannerConfig.ToList());
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception er) {


				data.Add("ErrorMessage", er.Message);
			}

			return data;
		}
		#endregion

		#region Product
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetProducts(int iPageNum, int iPageSize, string iFilter, int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			int catID;
			int.TryParse(iFilter, out catID);

			try {
				IQueryable<Custom.Data.Product> productData = null;


				if (productID == 0 && catID == 0) {
					productData = (from products in service.Product
								   where products.ClientSiteID == SessionManager.ClientSiteID
										 && products.DeletedBy == null
										 && products.DeletedOn == null
										 && products.SizeID != null
										 && products.ParentID == 0
								   select products).DefaultIfEmpty();
				} else if (productID > 0 && String.IsNullOrEmpty(iFilter)) {
					(from products in service.Product
					 where products.ClientSiteID == SessionManager.ClientSiteID
						&& products.ID == productID
						&& products.DeletedBy == null
						&& products.DeletedOn == null
						 && products.ParentID == 0
						&& products.SizeID != null
					 select products).DefaultIfEmpty();
				} else if (catID > 0) {
					productData = (from products in service.Product
								   where products.ClientSiteID == SessionManager.ClientSiteID
									  && products.CategoryID == catID
									  && products.DeletedBy == null
									  && products.DeletedOn == null
									   && products.ParentID == 0
									  && products.SizeID != null
								   select products).DefaultIfEmpty();
				} else if (!String.IsNullOrEmpty(iFilter)) {
					productData = (from products in service.Product
								   where products.ClientSiteID == SessionManager.ClientSiteID
									  && products.DeletedBy == null
									  && products.DeletedOn == null
									  && products.ParentID == 0
									  && products.SizeID != null
									  && (products.Name.Contains(iFilter) || products.Code.Contains(iFilter))

								   select products).DefaultIfEmpty();
				}


				if (productData.Count() == 0 || productData.First() == null)
					Utils.ServiceSuccessResultHandler(ref data, productData, productData.Count());
				else
					Utils.ServiceSuccessResultHandler(ref data, productData.ToList().OrderBy(prducts => prducts.Name), productData.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetProductsWithChildren(int iPageNum, int iPageSize, string iFilter, int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			int catID;
			int.TryParse(iFilter, out catID);
			try {
				IQueryable<dynamic> productData = null;
				IQueryable<dynamic> linkedProdData = null;
				IEnumerable<dynamic> resultedData = null;

				if (productID == 0 && catID == 0 && String.IsNullOrEmpty(iFilter)) {
					productData = (from products in service.Product
								   join sizes in service.Size on products.SizeID equals sizes.ID
								   where products.ClientSiteID == SessionManager.ClientSiteID
										 && products.DeletedBy == null
										 && products.DeletedOn == null
										 && products.SizeID != null
										 && products.ParentID == 0
								   orderby products.OrderIndex ascending
								   select new {
									   products
												  ,
									   hasChildren = (from prd in service.Product
													  where prd.ParentID == products.ID
															&& prd.DeletedBy == null
															&& prd.DeletedOn == null
															&& prd.SizeID != null
													  select prd).Count()
												  ,
									   sizes.Description
								   }).DefaultIfEmpty();
				} else if (catID > 0) {
					productData = (from products in service.Product
								   join sizes in service.Size on products.SizeID equals sizes.ID
								   where products.ClientSiteID == SessionManager.ClientSiteID
									  && products.CategoryID == catID
									  && products.DeletedBy == null
									  && products.DeletedOn == null
									   && products.ParentID == 0
									  && products.SizeID != null
								   orderby products.OrderIndex ascending
								   select new { products, hasChildren = (from prd in service.Product where prd.ParentID == products.ID select prd).Count(), sizes.Description }).DefaultIfEmpty();


					linkedProdData = (from products in service.Product
									  join catLinks in service.ProductCategory on products.ID equals catLinks.ProductID
									  join sizes in service.Size on products.SizeID equals sizes.ID
									  where products.ClientSiteID == SessionManager.ClientSiteID
										 && catLinks.CategoryID == catID
										 && products.DeletedBy == null
										 && products.DeletedOn == null
										  && products.ParentID == 0
										 && products.SizeID != null
										 && products.ID != productID
									  select new { products, hasChildren = (from prd in service.Product where prd.ParentID == products.ID select prd).Count(), sizes.Description }).DefaultIfEmpty();


				} else if (!String.IsNullOrEmpty(iFilter)) {
					productData = (from products in service.Product
								   join sizes in service.Size on products.SizeID equals sizes.ID
								   where products.ClientSiteID == SessionManager.ClientSiteID
									  && products.DeletedBy == null
									  && products.DeletedOn == null
									  && products.ParentID == 0
									  && products.SizeID != null
									  && (products.Name.Contains(iFilter)
										  || products.Code.Contains(iFilter)
										  || products.Description.Contains(iFilter)
										  || products.Short_Description.Contains(iFilter))
								   orderby products.OrderIndex ascending
								   select new { products, hasChildren = (from prd in service.Product where prd.ParentID == products.ID select prd).Count(), sizes.Description }).DefaultIfEmpty();
				}



				if (linkedProdData != null) {
					resultedData = productData.ToList().Concat(linkedProdData.ToList());

					if (resultedData.Count() == 0) {
						Utils.ServiceSuccessResultHandler(ref data, resultedData, resultedData.Count());
					} else {
						Utils.ServiceSuccessResultHandler(ref data, resultedData.ToList<dynamic>().OrderBy(t => t != null ? t.products.OrderIndex : t).Paginate(iPageNum, iPageSize),
														  resultedData.Count());
					}
				} else {
					if (productData.Count() == 0) {
						Utils.ServiceSuccessResultHandler(ref data, productData, productData.Count());
					} else {
						Utils.ServiceSuccessResultHandler(ref data, productData.ToList<dynamic>().OrderBy(t => t != null ? t.products.OrderIndex : t).Paginate(iPageNum, iPageSize),
														  productData.Count());
					}
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetChildProducts(int parentID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				var productData =
									(from products in service.Product
									 join sizes in service.Size on products.SizeID equals sizes.ID
									 where products.ClientSiteID == SessionManager.ClientSiteID
										&& products.DeletedBy == null
										&& products.DeletedOn == null
										&& products.SizeID != null
										&& products.ParentID == parentID
									 select new { products, sizes.Description }).DefaultIfEmpty();

				Utils.ServiceSuccessResultHandler(ref data, productData.ToList(), productData.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetChildProductsBySize(int SizeID, int productID, int parentProductID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (parentProductID == 0) {
					var productBySize = from products in service.Product
										where products.ParentID == productID
										&& products.SizeID == SizeID
										select products;
					Utils.ServiceSuccessResultHandler(ref data, productBySize, productBySize.Count());
				} else {

					var productBySize = from products in service.Product
										where products.ParentID == parentProductID
										&& products.SizeID == SizeID
										select products;

					Utils.ServiceSuccessResultHandler(ref data, productBySize, productBySize.Count());
				}

			} catch (Exception ex) {

				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateProduct(Custom.Data.Product product, int brand, int size, int[] colour, string customXML, long[] relatedCategories) {
			Utils.CheckSession();

			Dictionary<string, object> data = new Dictionary<string, object>();
			service.Connection.Open();
			using (DbTransaction transaction = service.Connection.BeginTransaction()) {
				try {


					product.ClientSiteID = SessionManager.ClientSiteID.Value;
					if (product.ID == -1) {
						bool commitTransaction = false;
						//check if this product & size combination already exists

						//check if combination exists
						var exists = from prds in service.Product
									 where prds.ParentID > 0
									 && prds.DeletedBy == null
									 && prds.DeletedOn == null
									 && prds.SizeID == product.SizeID
									 && prds.ParentID == product.ParentID
									 && prds.CategoryID == product.CategoryID
									 select prds;
						if (exists.Count() > 0) {
							Utils.ServiceSuccessResultHandler(ref data, exists, 1);
							return data;
						}

						if (product.ParentID > 0) {
							var prdExists = from prds in service.Product
											where prds.ID == product.ParentID
											&& prds.DeletedBy == null
											&& prds.DeletedOn == null
											&& prds.CategoryID == product.CategoryID
											&& prds.SizeID == product.SizeID
											select prds;

							if (prdExists.Count() > 0) {
								Utils.ServiceSuccessResultHandler(ref data, prdExists, 1);
								return data;
							}
						}


						//Create Product
						product.InsertedBy = int.Parse(SessionManager.UserID.ToString());
						product.CustomXML = customXML;
						service.AddToProduct(product);
						commitTransaction = service.SaveChanges() == 1 ? true : false;

						foreach (int colourID in colour) {
							//Create unique combination
							ColourSizeProductGallery colourSizeProd = new ColourSizeProductGallery {
								ColourID = colourID,
								OveridePrice = 0,
								ProductID = product.ID,
								SizeID = size
							};
							service.AddToColourSizeProductGallery(colourSizeProd);
						}
						service.SaveChanges();
						//Create Brand Association
						Utils.CheckSqlDBConnection();
						using (SqlCommand cmd = new SqlCommand()) {
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = "[dbo].[proc_BrandProduct_Insert]";
							cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
							cmd.Parameters.AddWithValue("@ProductID", product.ID);
							cmd.Parameters.AddWithValue("@BrandID", brand);


							Utils.SqlDb.ExecuteNonQuery(cmd);
						}



						if (commitTransaction) {
							transaction.Commit();
						} else {
							transaction.Rollback();
						}



					} else {
						var currentColours = from cpsg in service.ColourSizeProductGallery
											 where cpsg.ProductID == product.ID
											 select cpsg;

						if (!String.IsNullOrEmpty(customXML)) {

							var currentXML = (from products in service.Product
											  where products.ID == product.ID
											  select products.CustomXML).First();
							if (string.IsNullOrEmpty(currentXML)) {
								currentXML = "<XmlData></XmlData>";
							}
							XmlDocument productDataXML = new XmlDocument();
							XmlDocument newDataXML = new XmlDocument();
							productDataXML.LoadXml(currentXML);
							newDataXML.LoadXml(customXML);

							//Removes all currently stored CustomFields Fields
							XmlNodeList nodes = productDataXML.SelectNodes("//Fields[@type='CustomFields']");
							for (int i = 0; i < nodes.Count; i++) {
								nodes[i].ParentNode.RemoveChild(nodes[i]);
							}



							XmlNode importNode = newDataXML.SelectSingleNode("//Fields[@type='CustomFields']");
							if (importNode != null) {
								XmlNode newCustomFields = productDataXML.ImportNode(importNode, true);
								productDataXML.DocumentElement.AppendChild(newCustomFields);

							}

							product.CustomXML = productDataXML.InnerXml;

						}

						service.AttachTo("Product", product);
						product.SetAllModified(service);

						service.SaveChanges();

						foreach (int colourID in colour) {
							int id = colourID;
							if (currentColours.Where(col => col.ColourID == id).Count() == 0) {
								ColourSizeProductGallery newColourProd = new ColourSizeProductGallery {
									ColourID = colourID,
									OveridePrice = 0,
									ProductID = product.ID,
									SizeID = size

								};

								service.AddToColourSizeProductGallery(newColourProd);
							}
						}


						service.SaveChanges();

						transaction.Commit();
					}


					//check links 
					var currentLinks = service.ProductCategory.Where(prCat => prCat.ProductID == product.ID);

					//purge all old data
					foreach (ProductCategory category in currentLinks) {
						service.ProductCategory.DeleteObject(category);
					}
					service.SaveChanges();
					//add new list
					foreach (long relatedCategory in relatedCategories) {
						if (relatedCategory != product.CategoryID) {
							ProductCategory newLink = new ProductCategory {
								ProductID = product.ID,
								CategoryID = relatedCategory
							};

							service.ProductCategory.AddObject(newLink);
						}
					}

					service.SaveChanges();


					service.Connection.Close();
					Utils.ServiceSuccessResultHandler(ref data, product, 0);
				} catch (Exception ex) {
					transaction.Rollback();
					service.Connection.Close();
					Utils.ServiceErrorResultHandler(ex, ref data);
				}
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetLinkedCategories(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var linkedCats = service.ProductCategory.Where(product => product.ProductID == productID);
				Utils.ServiceSuccessResultHandler(ref data, linkedCats.ToList(), linkedCats.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> RemoveProduct(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (productID > 0) {
					var product = (from products in service.Product
								   where products.ID == productID
								   select products).FirstOrDefault();

					if (product != null) {
						product.DeletedBy = (int?)SessionManager.UserID;
						product.DeletedOn = DateTime.Now;

						var children = GetAllProductDescendants(product.ID);
					}

					product.SetAllModified(service);
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, product, 1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		private List<Custom.Data.Product> GetAllProductDescendants(int ParentID) {
			List<Custom.Data.Product> AllDescendants = new List<Custom.Data.Product>();
			var productChild = from products in service.Product
							   where products.ParentID == ParentID
							   select products;

			foreach (var product in productChild) {
				AllDescendants.Concat(GetAllProductDescendants(product.ID));
			}
			if (AllDescendants.Count > 1) {
				AllDescendants.Concat(AllDescendants);
			} else if (AllDescendants.Count == 1) {
				AllDescendants.Add(productChild.First());
			}


			return AllDescendants;

		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateColour(Custom.Data.Colour colour) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (colour.ID == -1) {
					colour.ClientSiteID = SessionManager.ClientSiteID.Value;
					service.AddToColour(colour);
					service.SaveChanges();
				} else {
					service.Attach(colour);
					colour.SetAllModified(service);
					service.SaveChanges();
				}

				Utils.ServiceSuccessResultHandler(ref data, string.Empty, 0);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetColours(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var productColours = from colours in service.Colour
									 where colours.ClientSiteID == SessionManager.ClientSiteID
									 select colours;
				if (iPageSize == 0) {
					Utils.ServiceSuccessResultHandler(ref data, productColours.ToList(),
													  productColours.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, productColours.ToList().Paginate(iPageNum, iPageSize),
													  productColours.Count());
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSizes(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var sizeResults = from sizes in service.Size
								  where sizes.ClientSiteID == SessionManager.ClientSiteID
								  && sizes.DeletedOn == null
								  && sizes.Inactive == false
								  orderby sizes.Description
								  select sizes;
				if (iPageNum == 0 && iPageSize == 0) {
					Utils.ServiceSuccessResultHandler(ref data, sizeResults.ToList(),
												sizeResults.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, sizeResults.Paginate(iPageNum, iPageSize).ToList(),
													  sizeResults.Count());
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateSizes(Custom.Data.Size size) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (size.ID == -1) {
					size.ClientSiteID = SessionManager.ClientSiteID.Value;
					size.DeletedBy = null;
					size.InsertedBy = int.Parse(SessionManager.UserID.ToString());
					service.AddToSize(size);
					service.SaveChanges();
				} else {
					size.ClientSiteID = SessionManager.ClientSiteID.Value;
					if (size.DeletedOn != null) {
						size.DeletedBy = (int)SessionManager.UserID;
						size.DeletedOn = DateTime.Now;
					}

					size.InsertedBy = (int)SessionManager.UserID;
					service.Attach(size);
					size.SetAllModified(service);
					service.SaveChanges();
				}
				Utils.ServiceSuccessResultHandler(ref data, size, 1);
			} catch (Exception ex) {

				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}


		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBrands(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var brandData = from brands in service.Brand
								where brands.ClientSiteID == SessionManager.ClientSiteID
								orderby brands.Name
								select brands;
				Utils.ServiceSuccessResultHandler(ref data, brandData.Paginate(iPageNum, iPageSize), brandData.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateBrands(Brand brand) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (brand.ID == -1) {
					brand.ClientSiteID = SessionManager.ClientSiteID.Value;
					brand.DeletedBy = null;
					brand.InsertedBy = int.Parse(SessionManager.UserID.ToString());
					service.AddToBrand(brand);
					service.SaveChanges();
				} else {
					brand.ClientSiteID = SessionManager.ClientSiteID.Value;
					brand.DeletedBy = null;
					brand.InsertedBy = int.Parse(SessionManager.UserID.ToString());
					service.Attach(brand);
					brand.SetAllModified(service);
					service.SaveChanges();
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GalleryUpdate(int itemGalleryID, int productID, string itemName, string itemInfo, string type, string videoUrl, bool deleteItem) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string prodId = productID.ToString();
				var cat = (from category in service.Category
						   where category.Name == prodId
						   && category.DeletedOn == null
						   select category).FirstOrDefault();

				if (cat == null) {
					Category productCat = new Category() {
						Name = productID.ToString(),
						FeatureModuleID = 5,
						Description = type,
						OrderIndex = 1,
						ParentID = 0,
						InsertedOn = DateTime.Now,
						InsertedBy = (int?)SessionManager.UserID,
						ClientSiteID = SessionManager.ClientSiteID

					};

					service.AddToCategory(productCat);
					service.SaveChanges();
					cat = productCat;
				}


				Custom.Data.Gallery galleryItem = new Custom.Data.Gallery() {
					CategoryID = cat.ID,
					Description = itemInfo,
					GalleryIndex = 0,
					MediaType = type == "images" ? ".jpg" : null,
					VideoURL = videoUrl,
					Name = itemName,
					InsertedBy = SessionManager.UserID,
					InsertedOn = DateTime.Now


				};

				if (deleteItem && itemGalleryID != 0) {
					galleryItem = (from gal in service.Gallery
								   where gal.ID == itemGalleryID
								   select gal).FirstOrDefault();
					galleryItem.DeletedOn = DateTime.Now;
					galleryItem.DeletedBy = SessionManager.UserID;

				}

				if (itemGalleryID != 0) {
					galleryItem = (from gal in service.Gallery
								   where gal.ID == itemGalleryID
								   select gal).FirstOrDefault();
					galleryItem.Name = itemName;
					galleryItem.Description = itemInfo;

					galleryItem.SetAllModified(service);
				} else {
					service.AddToGallery(galleryItem);
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, galleryItem, 1);
				return data;
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
				throw;
			}
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetGalleryForProduct(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string prodID = productID.ToString();
				var galleryItems = from gallery in service.Gallery
								   join category in service.Category on gallery.CategoryID equals category.ID
								   where category.Name == prodID
								   && category.DeletedOn == null
								   && gallery.DeletedOn == null
								   select gallery;

				Utils.ServiceSuccessResultHandler(ref data, galleryItems, galleryItems.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
				throw;
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetGalleryItemForProduct(int galleryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var galleryData = from galleries in service.Gallery
								  where galleries.ID == galleryID
								  select galleries;

				Utils.ServiceSuccessResultHandler(ref data, galleryData, galleryData.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAssociatedProducts(int currentProductID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var currentAssociatedProducts = from assProd in service.AssociatedProduct
												where assProd.ProductID == currentProductID
												select assProd;

				Utils.ServiceSuccessResultHandler(ref data, currentAssociatedProducts, currentAssociatedProducts.Count());
			} catch (Exception ex) {

				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateAssociatedProducts(int[] productList, int currentProductID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var currentAssociatedProducts = from assProd in service.AssociatedProduct
												where assProd.ProductID == currentProductID
												select assProd;
				if (productList.Length == 0) {
					foreach (var currentAssociatedProduct in currentAssociatedProducts) {
						service.AssociatedProduct.DeleteObject(currentAssociatedProduct);

					}
				}
				foreach (int productID in productList) {
					if (currentAssociatedProducts.Where(t => t.AssociatedProductID == currentProductID).Count() == 0) {
						var associatedProduct = new AssociatedProduct {
							AssociatedProductID = productID,
							ProductID = currentProductID
						};

						service.AddToAssociatedProduct(associatedProduct);
					}

				}

				service.SaveChanges();
			} catch (Exception ex) {

				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateProductTabs(int productId, string xml) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var customXml = (from products in service.Product
								 where productId == products.ID
								 select products).SingleOrDefault();

				XmlDocument productDataXML = new XmlDocument();
				XmlDocument newDataXML = new XmlDocument();
				customXml.CustomXML = (String.IsNullOrEmpty(customXml.CustomXML)) ? "<XmlData></XmlData>" : customXml.CustomXML;
				productDataXML.LoadXml(customXml.CustomXML);
				newDataXML.LoadXml(xml);

				//Removes all currently stored CustomFields Fields
				XmlNodeList nodes = productDataXML.SelectNodes("//Fields[@type='Tabs']");
				for (int i = 0; i < nodes.Count; i++) {
					nodes[i].ParentNode.RemoveChild(nodes[i]);
				}
				XmlNode newCustomFields = productDataXML.ImportNode(newDataXML.SelectSingleNode("//Fields[@type='Tabs']"), true);

				productDataXML.DocumentElement.AppendChild(newCustomFields);

				customXml.CustomXML = productDataXML.InnerXml;

				customXml.SetAllModified(service);
				service.SaveChanges();

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetEnquiries(int iPageNum, int iPageSize, string iFilter) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var orders = bool.Parse(iFilter) ?
							 from order in service.Orders
							 where order.ClientSiteID == SessionManager.ClientSiteID
							 && order.isApproved == true
							 && order.DeletedBy == null
							 && order.DeletedOn == null
							 select order
							 :
							 from order in service.Orders
							 where order.ClientSiteID == SessionManager.ClientSiteID
							 && order.isApproved == false
							 && order.DeletedBy == null
							 && order.DeletedOn == null
							 select order
							 ;

				Utils.ServiceSuccessResultHandler(ref data, orders.ToList().Paginate(iPageNum, iPageSize), orders.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		/// <summary>
		/// retrieves the new orderLineItems saved with the order
		/// </summary>
		/// <param name="orderID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetLineItems(int orderID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				//get the addition line items if any
				var newLineItems = (from newlines in service.OrderLineItems
									join orders in service.Orders on newlines.OrderID equals orders.ID
									where newlines.OrderID == orderID
									select newlines);

				Utils.ServiceSuccessResultHandler(ref data, newLineItems.ToList(), newLineItems.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateEnquiries(int orderID, string items, string newLineItems, string reference, string additionalInfo) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				//update order information with the reference number and additional data saved
				Aurora.Custom.Data.Orders _order = service.Orders.Where(o => o.ID == orderID).FirstOrDefault();
				_order.OrderReference = reference;
				_order.AdditionalInformation = additionalInfo;

				service.SaveChanges();

				List<OrderItems> processedItems = new List<OrderItems>();
				//convert items into an array 
				string[] selectedItems = items.Split('|');
				string[] selectedNewLineItems = newLineItems.Split('|');
				int totalQuantity = 0;

				//loop through the list to finds orders
				foreach (string orderItem in selectedItems) {
					//split the item into an array

					string[] enquiryRow = orderItem.Split(',');
					if (enquiryRow.Length == 3) {
						var selectedProduct = from orders in service.OrderItems
											  join order in service.Orders on orders.OrderID equals order.ID
											  join product in service.Product on orders.ProductID equals product.ID
											  where order.ClientSiteID == SessionManager.ClientSiteID
											  && order.ID == orderID
											  select new { orders, order, product };

						if (selectedProduct.Count() != 0) {
							if (selectedProduct.First().product.Total.Value > 0 && selectedProduct.First().product.Total.Value >= int.Parse(enquiryRow[2])) {
								selectedProduct.First().product.Total = selectedProduct.First().product.Total - int.Parse(enquiryRow[2]);
								totalQuantity = totalQuantity + int.Parse(enquiryRow[2]);
							}

							if (selectedProduct.First().product.Total > 0) {
								selectedProduct.First().orders.hasSelected = true;
							}

							selectedProduct.First().order.isApproved = true;


							selectedProduct.First().orders.SetAllModified(service);
							selectedProduct.First().order.SetAllModified(service);
							selectedProduct.First().product.SetAllModified(service);

							service.SaveChanges();
							processedItems.Add(new OrderItems {

								OrderID = orderID,
								hasSelected = false,
								ProductID = selectedProduct.First().product.ID,
								Quantity = int.Parse(enquiryRow[2])
							});
						}


					}

				}

				//loop through line items and add save them to [Product].[OrderLineItems] database table
				Aurora.Custom.Data.OrderLineItems _lineItem;
				//first delete all old records, no links so this is the only way i can think of at the moment :(
				var current_items = service.OrderLineItems.Where(o => o.OrderID == orderID).ToList();

				foreach (var oldItem in current_items) {
					service.DeleteObject(oldItem);
				}
				service.SaveChanges();

				foreach (string newLineItem in selectedNewLineItems) {
					if (newLineItem.Length > 0) {
						string[] lineItemRow = newLineItem.Split(',');
						decimal price = decimal.Parse(lineItemRow[2]);
						_lineItem = new OrderLineItems() {
							OrderID = orderID,
							Name = lineItemRow[0],
							Code = lineItemRow[1],
							Quantity = int.Parse(lineItemRow[3]),
							Price = price
						};

						service.OrderLineItems.AddObject(_lineItem);
					}
				}
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, processedItems, totalQuantity);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SaveSentEnquiryData(int orderID, string items, string newLineItems, string reference, string additionalInfo) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				//update order information with the reference number and additional data saved
				Aurora.Custom.Data.Orders _order = service.Orders.Where(o => o.ID == orderID).FirstOrDefault();
				_order.OrderReference = reference;
				_order.AdditionalInformation = additionalInfo;

				service.SaveChanges();

				List<OrderItems> processedItems = new List<OrderItems>();
				//convert items into an array 
				string[] selectedItems = items.Split('|');
				string[] selectedNewLineItems = newLineItems.Split('|');
				int totalQuantity = 0;

				//loop through the list to finds orders
				foreach (string orderItem in selectedItems) {
					//split the item into an array

					string[] enquiryRow = orderItem.Split(',');
					if (enquiryRow.Length == 3) {
						var selectedProduct = from orders in service.OrderItems
											  join order in service.Orders on orders.OrderID equals order.ID
											  join product in service.Product on orders.ProductID equals product.ID
											  where order.ClientSiteID == SessionManager.ClientSiteID
											  && order.ID == orderID
											  select new { orders, order, product };

						if (selectedProduct.Count() != 0) {
							if (selectedProduct.First().product.Total.Value > 0 && selectedProduct.First().product.Total.Value >= int.Parse(enquiryRow[2])) {
								totalQuantity = totalQuantity + int.Parse(enquiryRow[2]);
							}

							selectedProduct.First().orders.SetAllModified(service);
							selectedProduct.First().order.SetAllModified(service);

							service.SaveChanges();
							processedItems.Add(new OrderItems {

								OrderID = orderID,
								hasSelected = false,
								ProductID = selectedProduct.First().product.ID,
								Quantity = int.Parse(enquiryRow[2])
							});
						}
					}

				}

				//loop through line items and add save them to [Product].[OrderLineItems] database table
				Aurora.Custom.Data.OrderLineItems _lineItem;

				//first delete all old records, no links so this is the only way i can think of at the moment :(
				var current_items = service.OrderLineItems.Where(o => o.OrderID == orderID).ToList();

				foreach (var oldItem in current_items) {
					service.DeleteObject(oldItem);
				}
				service.SaveChanges();


				foreach (string newLineItem in selectedNewLineItems) {
					if (newLineItem.Length > 0) {
						string[] lineItemRow = newLineItem.Split(',');
						NumberStyles style = NumberStyles.AllowDecimalPoint;
						decimal price = Math.Round(decimal.Parse(lineItemRow[2].Replace(".", ",").Replace(" ", ""), style), 4);
						_lineItem = new OrderLineItems() {
							OrderID = orderID,
							Name = lineItemRow[0],
							Code = lineItemRow[1],
							Quantity = int.Parse(lineItemRow[3]),
							Price = price
						};

						service.OrderLineItems.AddObject(_lineItem);
					}
				}
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, (object)true, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		/// <summary>
		/// Sends a quotation to a site visitor
		/// </summary>
		/// <param name="QuoteHtml">Details of the quotation</param>
		/// <param name="subject">subject of the email</param>
		/// <param name="orderID">Id to identify the order requested </param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SendQuotations(string QuoteHtml, string subject, string orderID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			int quotationOrderID = int.Parse(orderID);
			try {
				var userDetails = (from users in service.Orders
								   where users.ClientSiteID == SessionManager.ClientSiteID
								   &&
								   users.ID == quotationOrderID

								   select new {
									   firsName = users.ClientFirsName,
									   lastName = users.ClientLastName,
									   Email = users.ClientEmail,
									   ContactNumber = users.ClientContactNo
								   }).SingleOrDefault();
				var commEngineUsers = new List<dynamic>();
				commEngineUsers.Add(userDetails);
				Utils.SendToMessageCentre(Utils.MessageCentreEvents.SendProductQuotation.GetEnumDescriptionAttribute(), QuoteHtml, null, subject,
									Utils.GenerateCommunicationXML(commEngineUsers), CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
				Utils.ServiceSuccessResultHandler(ref data, userDetails, null);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}


			return data;


		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> RemoveEnquiry(int orderID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var deleteItem = from order in service.Orders
								 where order.ID == orderID
								 select order;

				if (deleteItem.Count() > 0) {
					deleteItem.First().DeletedOn = DateTime.Now;
					deleteItem.First().DeletedBy = int.Parse(SessionManager.UserID.ToString());

					deleteItem.First().SetAllModified(service);
					service.SaveChanges();
				}

				Utils.ServiceSuccessResultHandler(ref data, deleteItem.ToList(), deleteItem.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSpecials(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var clientSpecials = (from specials in service.Specials
									  where specials.ProductID == productID
									   && specials.DeletedBy == null
									   && specials.DeletedOn == null
									  orderby specials.InsertedOn descending
									  select specials).SingleOrDefault();

				Utils.ServiceSuccessResultHandler(ref data, clientSpecials, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateSpecials(Specials special, bool remove) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				if (special.ID == -1 && !remove) {
					Specials newSpecial = new Specials {
						ProductID = special.ProductID,
						Price = special.Price,
						DateFrom = special.DateFrom,
						DateTo = special.DateTo,
						InsertedBy = int.Parse(SessionManager.UserID.ToString()),
						InsertedOn = DateTime.Now,
						DeletedBy = null,
						DeletedOn = null
					};
					service.AddToSpecials(newSpecial);
					service.SaveChanges();
					special.ID = newSpecial.ID;
				} else if (special.ID > 0 && !remove) {
					var updateSpecial = (from specials in service.Specials
										 where specials.ID == special.ID
										 select specials).Single();

					updateSpecial.Price = special.Price;
					updateSpecial.DateFrom = special.DateFrom;
					updateSpecial.DateTo = special.DateTo;

					updateSpecial.SetAllModified(service);
					service.SaveChanges();

				} else if (remove && special.ID > 0) {
					var updateRemove = (from specials in service.Specials
										where specials.ID == special.ID
										select specials).Single();

					updateRemove.DeletedOn = DateTime.Now;
					updateRemove.DeletedBy = (int)SessionManager.UserID;

					updateRemove.SetAllModified(service);
					service.SaveChanges();

					special = updateRemove;
				}



				Utils.ServiceSuccessResultHandler(ref data, special, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetProductsCustomFields() {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				var customFieldsXML = (from moduleSchema in service.Schema
									   join modules in service.Module on moduleSchema.ModuleID equals modules.ID
									   where modules.Name == "ProductCustomField" && moduleSchema.ClientSiteID == SessionManager.ClientSiteID
									   select moduleSchema.SchemaXML).FirstOrDefault();
				if (customFieldsXML != null) {
					Utils.ServiceSuccessResultHandler(ref data, customFieldsXML, 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, "", 0);
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateProductsCustomFields(string xml) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				var customXml = (from moduleSchema in service.Schema
								 join modules in service.Module on moduleSchema.ModuleID equals modules.ID
								 where modules.Name == "ProductCustomField" && moduleSchema.ClientSiteID == SessionManager.ClientSiteID
								 select moduleSchema).SingleOrDefault();
				//If no previous record of custom XML is found, create one.
				if (customXml == null) {
					Aurora.Custom.Data.Schema customSchema = new Aurora.Custom.Data.Schema();
					customSchema.ModuleID = (from modules in service.Module
											 where modules.Name == "ProductCustomField"
											 select modules.ID).First();
					customSchema.ClientSiteID = SessionManager.ClientSiteID;
					customSchema.SchemaXML = xml;
					customSchema.InsertedOn = DateTime.Now;
					customSchema.InsertedBy = SessionManager.UserID;
					service.AddToSchema(customSchema);
				}
					//update existing XML
				else {
					customXml.SchemaXML = xml;
					customXml.SetAllModified(service);
				}
				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, "", null);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCustomProductXMLData(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				System.IO.StringWriter sw = new StringWriter();
				HtmlTextWriter htw = new HtmlTextWriter(sw);

				//get schema and data xml
				var customXMLSchema = (from modschema in service.Schema
									   join modules in service.Module on modschema.ModuleID equals modules.ID
									   where modules.Name == "ProductCustomField" && modschema.ClientSiteID == SessionManager.ClientSiteID
									   select modschema.SchemaXML).FirstOrDefault();

				if (!String.IsNullOrEmpty(customXMLSchema)) {
					string customXMLData = string.Empty;
					if (productID >= 0) {
						customXMLData = (from products in service.Product
										 where products.ID == productID &&
										 products.ClientSiteID == SessionManager.ClientSiteID
										 select products.CustomXML).FirstOrDefault();
					}

					//create in-memory object for xml panel
					XmlDocument moduleSchema = new XmlDocument();
					XmlDocument moduleData = new XmlDocument();

					//Load XML
					moduleSchema.LoadXml(customXMLSchema);
					if (moduleSchema.GetElementsByTagName("field").Count == 0) {
						Utils.ServiceSuccessResultHandler(ref data, htw.InnerWriter.ToString(), 1);
						return data;
					}
					if (!String.IsNullOrEmpty(customXMLData)) {
						moduleData.LoadXml(customXMLData);

						//Removes all "Fields" that do not have the type "CustomFields"
						XmlNodeList nodes = moduleData.SelectNodes("//Fields[not(@type='CustomFields')]");
						for (int i = 0; i < nodes.Count; i++) {
							nodes[i].ParentNode.RemoveChild(nodes[i]);
						}
					}

					ObjectPanel xmlContainer = new ObjectPanel();
					XmlPanel xmlRenderContainer = new XmlPanel();

					xmlContainer.ID = "ContentPanel";
					xmlRenderContainer.ID = "ContentXml";

					xmlContainer.Controls.Add(xmlRenderContainer);
					xmlRenderContainer.XmlPanelSchema = moduleSchema;
					xmlRenderContainer.XmlPanelData = moduleData;

					xmlRenderContainer.XmlPanelSchema = moduleSchema;
					xmlRenderContainer.XmlPanelData = moduleData;


					xmlRenderContainer.RenderXmlControls("tbl_Form");
					xmlRenderContainer.RenderControl(htw);
				}

				Utils.ServiceSuccessResultHandler(ref data, htw.InnerWriter.ToString(), 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		#endregion

		#region Utility
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> Exists(string path) {
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {

				data["Data"] = string.Format(path, SessionManager.ClientSiteID);
				data["Result"] = File.Exists(Server.MapPath(string.Format(path, SessionManager.ClientSiteID))); ;

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SendMail(string fromAddress, string fromName, string message, string subject) {
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				string retVal = fromAddress.Contains(ConfigurationManager.AppSettings["ErrorMailListFrom"]) ?
								Utils.SendErrorMail(fromAddress, fromName, message, subject)
								: Custom.Utils.SendMail(fromAddress, fromName, message, subject, true, true);

				if (retVal == "Sent") {
					data.Add("Result", true);
					data.Add("Data", string.Empty);
					data.Add("ErrorMessage", string.Empty);
				} else {
					data.Add("Result", false);
					data.Add("Data", string.Empty);
					data.Add("ErrorMessage", retVal);
				}

			} catch (Exception ex) {
				data.Add("Result", true);
				data.Add("Data", string.Empty);
				data.Add("ErrorMessage", ex.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> KeepAlive() {
			//keeps user session alive
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				Utils.ServiceSuccessResultHandler(ref data, HttpContext.Current.Request.IsAuthenticated, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		#endregion

		#region Members
		/// <summary>
		/// Gets all listed members for a category
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="iPageNum"></param>
		/// <param name="iPageSize"></param>
		/// <param name="status"></param>
		/// <param name="searchType"></param>
		/// <param name="filterCriteria"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMembers(long categoryID, int iPageNum, int iPageSize, string status, string searchType, string filterCriteria) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				IQueryable<MemberWithCategories> siteMembers = null;
				siteMembers = (from members in service.GetMembersByCategoryAndStatus(SessionManager.ClientSiteID, categoryID, status)
							   select members).AsQueryable();
				switch (searchType) {
					case "MemberName":
						siteMembers = from sm in siteMembers
									  where sm.FirstName.Contains(filterCriteria, StringComparison.OrdinalIgnoreCase)
											|| sm.LastName.Contains(filterCriteria, StringComparison.OrdinalIgnoreCase)
											|| sm.Email.Contains(filterCriteria, StringComparison.OrdinalIgnoreCase)
									  orderby sm.FirstName, sm.LastName
									  select sm;
						break;
					case "Latest":
						siteMembers = siteMembers.OrderBy(o => o.InsertedOn);
						break;
					case "Custom":
						siteMembers = from sm in siteMembers
									  where sm.CustomXML != null && XDocument.Parse(sm.CustomXML).Descendants("field").Where(field => field.Attribute("value").Value.Contains(filterCriteria, StringComparison.OrdinalIgnoreCase)).Count() > 0
									  orderby sm.FirstName, sm.LastName
									  select sm;
						break;
				}
				List<MemberWithCategories> memberList = siteMembers.ToList();
				Utils.ServiceSuccessResultHandler(ref data, memberList.Paginate(iPageNum, iPageSize),
													  memberList.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllMembers(long organisationID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var members = from member in service.Member
							  where member.OrganisationID == organisationID
							  && member.DeletedBy == null
							  && member.DeletedOn == null
							  && member.isActive == true
							  && member.isPending == false
							  select member;
				Utils.ServiceSuccessResultHandler(ref data, members.ToList(), members.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateMembers(Member memberData, List<Role> roles, string additionalXML) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			service.Connection.Open();
			using (DbTransaction transaction = service.Connection.BeginTransaction()) {
				try {
					if (memberData.ID == 0) {
						service.AddToMember(memberData);
					} else if (memberData.DeletedOn != null) {
						memberData.DeletedBy = SessionManager.UserID;
						memberData.DeletedOn = DateTime.Now;
						service.AttachTo("Member", memberData);
						memberData.SetAllModified(service);

					} else {
						var memberCurrentData = (from members in service.Member
												 where members.ID == memberData.ID
												 select members).FirstOrDefault();

						if (!memberData.isPending && memberCurrentData.isPending) {
							//send message to user that website admin has activated their account
							var commEngineUsers = new List<dynamic>();
							commEngineUsers.Add(memberCurrentData);
							Utils.SendToMessageCentre(Utils.MessageCentreEvents.MemberStatusChangedtoActive.GetEnumDescriptionAttribute(), string.Empty, null,
												String.Format("{0} Membership", CacheManager.SiteData.CompanyName),
												Utils.GenerateCommunicationXML(commEngineUsers), CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
						}

						if (memberData.isPending) {
							memberData.isActive = false;
						}


						memberCurrentData.SetAllModified<Member>(memberData, service, new[] { "InsertedOn", "InsertedBy", "DeletedBy", "DeletedOn", "Password", "isActive" }, true);

						memberCurrentData.SetAllModified(service);
						memberCurrentData.LastUpdated = DateTime.Now;
						if (memberCurrentData.CustomXML != null) {
							memberCurrentData.CustomXML = memberCurrentData.CustomXML.Replace("'", Server.HtmlEncode("'"));
						}

						service.SaveChanges();
					}

					bool commitTransaction = true;

					//get roles for user

					var userRoles = from memberRoles in service.MemberRole
									where memberRoles.MemberID == memberData.ID
									select memberRoles;

					//check if user already has roles and delete removed roles
					//foreach (MemberRole memberRole in userRoles)
					//{
					//    if (roles.Find(t => t.ID == memberRole.RoleID) == null)
					//    {
					//        service.DeleteObject(memberRole);
					//        service.SaveChanges();
					//    }
					//    else
					//    {
					//        roles.Remove((roles.Where(del => del.ID == memberRole.RoleID).SingleOrDefault()));
					//    }
					//}



					////append roles to user
					//if (memberData.DeletedOn == null || memberData.DeletedBy == 0)
					//{
					//    foreach (Role roleData in roles)
					//    {
					//        MemberRole memberRoleEntity = new MemberRole
					//        {
					//            ClientSiteID = SessionManager.ClientSiteID,
					//            MemberID = memberData.ID,
					//            RoleID = roleData.ID
					//        };

					//        service.AddToMemberRole(memberRoleEntity);
					//        service.SaveChanges();
					//    }

					//}
					//else
					//{
					//    foreach (MemberRole memberRole in userRoles)
					//    {
					//        service.MemberRole.DeleteObject(memberRole);
					//        service.SaveChanges();

					//    }
					//}

					/*
					 * It would appear this code was written to facilitate Baby2Mom's additional data. It is causing problems with the new
					 * membership system, so I am disabling it until it is needed. - Garth
					 * 

                    //add/update additional fields
                    var addtionalFields = (from customData in service.CustomModuleData
                                           where customData.SystemModuleID == memberData.ID
                                           select customData).SingleOrDefault();
                    if (addtionalFields == null)
                    {
                        var customModule = (from customModules in service.CustomModules
                                            where customModules.FeaturesModuleID == 175
                                            select customModules).SingleOrDefault();

                        service.AddToCustomModuleData(new CustomModuleData
                        {
                            ClientSiteID = SessionManager.ClientSiteID.Value,
                            Data = additionalXML,
                            DataSchemaXML = customModule.XMLSchema,
                            ModuleID = customModule.ID,
                            SystemModuleID = memberData.ID,
                            InsertedOn = DateTime.Now
                        });
                    }
                    else
                    {
                        addtionalFields.Data = additionalXML;
                        addtionalFields.SetAllModified(service);

                    } */

					service.SaveChanges();

					if (commitTransaction) {
						transaction.Commit();
						Utils.ServiceSuccessResultHandler(ref data, memberData, 1);

					} else {
						transaction.Rollback();
						Utils.ServiceSuccessResultHandler(ref data, null, 0);
					}


				} catch (Exception ex) {
					transaction.Rollback();
					Utils.ServiceErrorResultHandler(ex, ref data);
				}
			}


			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ActivateMembership(long memberCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				MemberRole memberCategory = (from mc in service.MemberRole
											 where mc.ID == memberCategoryID
											 select mc).FirstOrDefault();
					Role category = service.Role.Where(w => w.ID == memberCategory.RoleID).FirstOrDefault();
				if (memberCategory != null) {
					if (memberCategory.LapsedOn == null && (memberCategory.IsActivated == null || memberCategory.IsActivated == false) && memberCategory.CommencedOn == null) {
						memberCategory.IsActivated = true;
						memberCategory.CommencedOn = DateTime.Now;
						long duration = category.RoleDuration ?? 0;
						if (duration != 0) {
							memberCategory.ExpiresOn = memberCategory.CommencedOn.Value.AddDays(duration);
						}
						if (memberCategory.Replacing != null) {
							Utils.markCategoryAsReplaced(memberCategory.Replacing.Value);
						}
						service.SaveChanges();
						Utils.SendMemberCategoryMessageEmail(memberCategory.ID, 0, false);
						Utils.ServiceSuccessResultHandler(ref data, "Membership successfully activated.", 1);
					} else {
						Utils.ServiceSuccessResultHandler(ref data, "This Membership could not be activated. It has either already been activated, or has already lapsed.", 0);
					}
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ExtendMembership(DateTime newExpiryDate, long memberCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				MemberRole memberCategory = (from mc in service.MemberRole
											 where mc.ID == memberCategoryID && mc.ClientSiteID == SessionManager.ClientSiteID
											 select mc).FirstOrDefault();
				Role category = service.Role.Where(w => w.ID == memberCategory.RoleID).FirstOrDefault();
				if (memberCategory != null) {
					if (category.RoleDuration == 0) {
						Utils.ServiceSuccessResultHandler(ref data, "This category does not expire. Members on this category cannot be set to expire", 0);
					} else if (memberCategory.IsActivated == null || memberCategory.IsActivated == false || memberCategory.CommencedOn == null) {
						Utils.ServiceSuccessResultHandler(ref data, "This Member has not been activated.", 0);
					} else if (memberCategory.LapsedOn == null) {
						memberCategory.ExpiresOn = newExpiryDate;
						service.SaveChanges();
						Utils.ServiceSuccessResultHandler(ref data, "Category Membership updated", 1);
					}
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> CancelMembership(long memberCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				MemberRole memberCategory = (from mc in service.MemberRole
											 where mc.ID == memberCategoryID && mc.ClientSiteID == SessionManager.ClientSiteID
											 select mc).FirstOrDefault();
				Role category = service.Role.Where(w => w.ID == memberCategory.RoleID).FirstOrDefault();
				if (memberCategory != null) {
					memberCategory.LapsedOn = DateTime.Now;
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, "Category Membership Cancelled", 2);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> PayMembership(long memberCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				MemberRole memberCategory = (from mc in service.MemberRole
											 where mc.ID == memberCategoryID && mc.ClientSiteID == SessionManager.ClientSiteID
											 select mc).FirstOrDefault();
				Role category = service.Role.Where(w => w.ID == memberCategory.RoleID).FirstOrDefault();
				if (memberCategory != null) {
					memberCategory.PaidOn = DateTime.Now;
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, "Category Membership Marked as Paid", 1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ChangeMembership(long memberCategoryID, long newCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			Utils.CheckSession();
			try {
				MemberRole memberCategory = (from mc in service.MemberRole
											 where mc.ID == memberCategoryID && mc.ClientSiteID == SessionManager.ClientSiteID
											 select mc).FirstOrDefault();
				Role category = service.Role.Where(w => w.ID == newCategoryID && w.ClientSiteID == SessionManager.ClientSiteID).FirstOrDefault();
				if (memberCategory != null && category != null) {
					Utils.markCategoryAsReplaced(memberCategory.ID);
					MemberRole newCategory = new MemberRole();
					newCategory.MemberID = memberCategory.MemberID;
					newCategory.RoleID = newCategoryID;
					newCategory.ClientSiteID = SessionManager.ClientSiteID;
					newCategory.InsertedOn = DateTime.Now;
					newCategory.CommencedOn = newCategory.InsertedOn;
					newCategory.IsActivated = true;
					if (category.RoleDuration > 0) {
						newCategory.ExpiresOn = newCategory.CommencedOn.Value.AddDays((long)category.RoleDuration);
					} else {
						newCategory.ExpiresOn = null;
					}
					newCategory.CostAtPurchase = category.Cost;
					newCategory.Replacing = memberCategory.ID;
					service.AddToMemberRole(newCategory);
					service.SaveChanges();
					Utils.SendMemberCategoryMessageEmail(newCategory.ID, 0, false);
					Utils.ServiceSuccessResultHandler(ref data, "Membership successfully created", 2);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, "The Member or Category supplied does not exist", 0);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}


		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMembershipSettings() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var settings = from memberSettings in service.MembershipSettings
							   where memberSettings.ClientSiteID == SessionManager.ClientSiteID
							   select memberSettings;

				Utils.ServiceSuccessResultHandler(ref data, settings.ToList(), settings.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberSettings"></param>
		/// <param name="paymentSchemas"></param>
		/// <param name="isDelete"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateMemberSettings(MembershipSettings memberSettings, List<MemberSettingsPaymentSchema> paymentSchemas, bool isDelete) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			int success = 0;
			service.Connection.Open();
			try {
				using (DbTransaction transaction = service.Connection.BeginTransaction()) {
					//get current selected schemas
					var currentSchemas = (from memberSchema in service.MemberSettingsPaymentSchema
										  where memberSchema.ClientSiteID == SessionManager.ClientSiteID
												&& memberSchema.MemberSettingsID == memberSettings.ID
										  select memberSchema).ToList();

					foreach (MemberSettingsPaymentSchema memberSettingsPaymentSchema in paymentSchemas) {
						if (currentSchemas.Where(t => t.PaymentSchemaID == memberSettingsPaymentSchema.PaymentSchemaID).Count() == 0 && memberSettingsPaymentSchema.DeletedOn == null) {
							service.AddToMemberSettingsPaymentSchema(memberSettingsPaymentSchema);
							success = service.SaveChanges();
						} else if (memberSettingsPaymentSchema.DeletedOn != null) {


							var selected = (from memberset in service.MemberSettingsPaymentSchema
											where
												memberset.MemberSettingsID ==
												memberSettingsPaymentSchema.MemberSettingsID
												&&
												memberset.PaymentSchemaID == memberSettingsPaymentSchema.PaymentSchemaID
												&& memberset.ClientSiteID == memberSettingsPaymentSchema.ClientSiteID
											select memberset).SingleOrDefault();

							if (selected != null) {
								selected.DeletedBy = SessionManager.UserID;
								selected.DeletedOn = DateTime.Now;
								selected.SetAllModified(service);
								service.SaveChanges();
							}
						}

					}
					//save settings
					if (memberSettings.ID == 0) {
						service.AddToMembershipSettings(memberSettings);
					} else if (isDelete) {
						service.MembershipSettings.Attach(memberSettings);
						service.MembershipSettings.DeleteObject(memberSettings);
					} else {
						service.MembershipSettings.Attach(memberSettings);
						memberSettings.SetAllModified(service);

					}

					success = service.SaveChanges();

					if (success == 1) {
						transaction.Commit();
					} else {
						transaction.Rollback();
					}

				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}



		#endregion

		#region Payments
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPaymentPlans(long settingsID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (settingsID == 0) {
					var plans = from paymentPlans in service.PaymentSchema
								where paymentPlans.DeletedBy == null
								&& paymentPlans.DeletedOn == null
								select paymentPlans;

					Utils.ServiceSuccessResultHandler(ref data, plans.ToList(), plans.Count());
				} else {
					var settingPlans = from paymentPlans in service.PaymentSchema
									   join memberSettingsPaymentSchema in service.MemberSettingsPaymentSchema on paymentPlans.ID equals memberSettingsPaymentSchema.PaymentSchemaID
									   where paymentPlans.DeletedBy == null
									   && paymentPlans.DeletedOn == null
									   && memberSettingsPaymentSchema.DeletedBy == null
									   && memberSettingsPaymentSchema.DeletedOn == null
									   && memberSettingsPaymentSchema.MemberSettingsID == settingsID
									   select paymentPlans;

					Utils.ServiceSuccessResultHandler(ref data, settingPlans.ToList(), settingPlans.Count());
				}



			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region Roles
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetRolesForMembers(int iPageNum, int iPageSize, string iFilter) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var activeUsers = from mR in service.MemberRole
								  join m in service.Member on mR.MemberID equals m.ID
								  where mR.ClientSiteID == SessionManager.ClientSiteID &&
										mR.LapsedOn == null &&
										mR.Replaced == null
								  group mR by new { mR.ClientSiteID, mR.RoleID } into g
								  select new {
									  RoleID = g.Key.RoleID,
									  ActiveCount = g.Count()
								  };
				var siteRoles = from roles in service.Role
								from aU in activeUsers.Where(w => w.RoleID == roles.ID).DefaultIfEmpty()
								where roles.ClientSiteID == SessionManager.ClientSiteID
								&& roles.DeletedBy == null
								&& roles.DeletedOn == null
								orderby roles.OrderIndex
								select new {
									Role = roles,
									ActiveCount = (aU.ActiveCount == null) ? 0 : aU.ActiveCount
								};

				if (iPageSize == 0) {
					Utils.ServiceSuccessResultHandler(ref data, siteRoles.ToList(), siteRoles.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, siteRoles.ToList().Paginate(iPageNum, iPageSize), siteRoles.Count());
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateRoles(Custom.Data.Role roleEntity) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (roleEntity.ID == 0) {
					service.AddToRole(roleEntity);
				} else if (roleEntity.DeletedOn != null) {
					service.Role.Attach(roleEntity);
					roleEntity.SetAllModified(service);

				} else {
					roleEntity.DeletedOn = null;
					roleEntity.DeletedBy = null;
					service.Role.Attach(roleEntity);
					roleEntity.SetAllModified(service);
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, roleEntity, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> DeleteRole(long ID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var memberCount = (from mR in service.MemberRole
								   join m in service.Member on mR.MemberID equals m.ID
								   where mR.ClientSiteID == SessionManager.ClientSiteID &&
										 mR.LapsedOn == null &&
										 mR.Replaced == null &&
										 mR.RoleID == ID
								   select mR.ID).Count();

				if (memberCount > 0) {
					Utils.ServiceSuccessResultHandler(ref data, false, 1);
				} else {
					var role = service.Role.Where(w => w.ID == ID).FirstOrDefault();
					role.DeletedOn = DateTime.Now;
					role.DeletedBy = SessionManager.UserID;
					Utils.ServiceSuccessResultHandler(ref data, true, 1);
					service.SaveChanges();
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> AddRolesToItems(List<RoleModule> roleModules, bool removeAll) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (roleModules.Count() > 0 && !removeAll) {
					foreach (RoleModule roleModule in roleModules) {
						var roleModuleData = from roleMod in service.RoleModule
											 where roleMod.ModuleID == roleModule.ModuleID
											 && roleMod.ModuleItemID == roleModule.ModuleItemID
											 select roleMod;


						//add new items
						if (roleModuleData.Where(item => item.ModuleItemID == roleModule.ModuleItemID
							&& roleModule.RoleID == item.RoleID).Count() == 0) {
							service.AddToRoleModule(roleModule);
							service.SaveChanges();
						}



						//remove items that are not in the new list
						foreach (RoleModule current in roleModuleData) {
							if (roleModules.Where(newItems => newItems.ModuleItemID == current.ModuleItemID
								&& newItems.ModuleID == current.ModuleID
								&& newItems.RoleID == current.RoleID
								).Count() == 0) {
								service.RoleModule.DeleteObject(current);

							}
						}
					}
				} else {
					foreach (RoleModule roleModule in roleModules) {
						var deleteRole = (from rModules in service.RoleModule
										  where rModules.RoleID == roleModule.RoleID
												&& rModules.ModuleItemID == roleModule.ModuleItemID
												&& rModules.ModuleID == roleModule.ModuleID
										  select rModules).SingleOrDefault();

						if (deleteRole != null) {
							service.RoleModule.DeleteObject(deleteRole);
						}

					}
				}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, roleModules, roleModules.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> LoadRolesModules(long itemID, long moduleID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var moduleRoles = from rolesModules in service.RoleModule
								  where rolesModules.ModuleID == moduleID
										&& rolesModules.ModuleItemID == itemID
								  select rolesModules;

				Utils.ServiceSuccessResultHandler(ref data, moduleRoles, moduleRoles.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}
		#endregion

		#region Organisations


        [WebMethod(EnableSession = true)]
        public bool SetClientSite(int ClientSiteID, string CompanyName) {
            SessionManager.ClientSiteID = ClientSiteID;
            SessionManager.ClientBasePath = string.Format("~/ClientData/{0}", ClientSiteID);
            SessionManager.ClientName = CompanyName;
            SessionManager.SiteExcludedRoles = service.GetUserSiteExcludedRoles(ClientSiteID, SessionManager.UserID).ToList();
            return true;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetClientSites(int iPageNum, int iPageSize, string iFilter) {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
			List<ClientGetClientSites_Result> Clients = new List<ClientGetClientSites_Result>();

            try {
				if(iFilter == null) {
					Clients = service.ClientGetClientSites(SessionManager.LoggedInClientSiteID).ToList();
				} else {
					Clients = service.ClientGetClientSites(SessionManager.LoggedInClientSiteID).Where(cs => cs.CompanyName.ToLower().Contains(iFilter.ToLower())).ToList();
				}

                SessionManager.iPageCount = Clients.Count();
                data.Add("Data", Clients.OrderBy(r => r.CompanyName).Paginate(iPageNum, iPageSize));
                data.Add("Result", true);
                data.Add("Count", SessionManager.iPageCount);
                data.Add("ErrorMessage", string.Empty);

            } catch (Exception exception) {
                data.Clear();
                data.Add("ErrorMessage", exception.Message);
            }
            return data;
        }		

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetOrganisations(long organisationID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (organisationID == 0) {
					var companies = from company in service.Company
									join departments in service.Departments on company.ID equals departments.CompanyID
									into outer
									from departments in outer.Where(dep => dep.DeletedOn == null && dep.DeletedBy == null).DefaultIfEmpty()
									where company.ClientSiteID == SessionManager.ClientSiteID
										  && company.DeletedBy == null
										  && company.DeletedOn == null
									orderby company.Name
									select new { company, departments };



					Utils.ServiceSuccessResultHandler(ref data, companies.ToList(), companies.Count());
				} else {
					var singleCompany = from company in service.Company
										where company.ClientSiteID == SessionManager.ClientSiteID
											  && company.DeletedBy == null
											  && company.DeletedOn == null
										select company;

					Utils.ServiceSuccessResultHandler(ref data, singleCompany, singleCompany.Count());
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateOrganistaion(Custom.Data.Company companyObject) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (companyObject.ID == 0) {
					companyObject.InsertedBy = SessionManager.UserID;
					companyObject.ClientSiteID = SessionManager.ClientSiteID;

					service.AddToCompany(companyObject);
				} else if (companyObject.DeletedOn != null) {
					companyObject.DeletedOn = DateTime.Now;
					companyObject.DeletedBy = SessionManager.UserID;
					service.Company.Attach(companyObject);
					companyObject.SetAllModified(service);

				} else if (companyObject.ID > 0) {
					service.Company.Attach(companyObject);
					companyObject.SetAllModified(service);
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, companyObject, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region Departments
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetDepartments(long departmentID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var departments = from department in service.Departments
								  where department.ClientSiteID == SessionManager.ClientSiteID
										&& department.DeletedBy == null
										&& department.DeletedOn == null
										&& department.ID == departmentID
								  select department;
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateDepartment(Departments departmentEntity) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (departmentEntity.ID == 0) {
					service.AddToDepartments(departmentEntity);
				} else if (departmentEntity.DeletedOn != null) {
					service.Departments.Attach(departmentEntity);
					departmentEntity.SetAllModified(service);
				} else {
					service.Departments.Attach(departmentEntity);
					departmentEntity.SetAllModified(service);
				}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, departmentEntity, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		#endregion

		#region Newsletter Management
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetNewsletters() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var letters = from newsletter in service.Newsletter
							  where newsletter.ClientSiteID == SessionManager.ClientSiteID
							  select newsletter;

				Utils.ServiceSuccessResultHandler(ref data, letters.ToList(), letters.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateNewsLetter(Newsletter newsData) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (newsData.ID == 0) {
					service.AddToNewsletter(newsData);
				} else {
					service.Newsletter.Attach(newsData);
					newsData.SetAllModified(service);
				}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, newsData, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMailingLists() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var mailingList = from mailing in service.MailingList
								  where mailing.ClientSiteID == SessionManager.ClientSiteID
										&& mailing.DeletedBy == null
										&& mailing.DeletedOn == null
								  select mailing;

				Utils.ServiceSuccessResultHandler(ref data, mailingList.ToList(), mailingList.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMailingListMembers(long mailingList) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var memberMailingList = from memMail in service.MemberMailingList
										join member in service.Member on memMail.MemberID equals member.ID
										where memMail.MailingListID == mailingList
										select member;

				Utils.ServiceSuccessResultHandler(ref data, memberMailingList.ToList(), memberMailingList.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateMailingListMembers(MailingList mailList, List<int> memberList) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (mailList.ID == 0) {
					mailList.InsertedBy = SessionManager.UserID;
					mailList.InsertedOn = DateTime.Now;
					mailList.ClientSiteID = SessionManager.ClientSiteID;
					service.AddToMailingList(mailList);
					service.SaveChanges();
				} else if (mailList.DeletedOn != null) {
					mailList.DeletedOn = DateTime.Now;
					mailList.DeletedBy = SessionManager.UserID;

					service.MailingList.Attach(mailList);
					mailList.SetAllModified(service);
					service.SaveChanges();

					Utils.ServiceSuccessResultHandler(ref data, mailList, 1);
					return data;
				} else {
					mailList.ClientSiteID = SessionManager.ClientSiteID;
					service.MailingList.Attach(mailList);
					mailList.SetAllModified(service);
					service.SaveChanges();
				}

				var mailingList = from mail in service.MemberMailingList
								  where mail.MailingListID == mailList.ID
								  select mail;

				foreach (MemberMailingList memberMailingList in mailingList) {
					if (memberList.Where(t => t == memberMailingList.MemberID).Count() == 0) {
						service.MemberMailingList.Attach(memberMailingList);
						service.MemberMailingList.DeleteObject(memberMailingList);
					}

				}

				foreach (int memberID in memberList) {
					if (mailingList.Where(t => t.MemberID == memberID).Count() == 0) {
						MemberMailingList newMember = new MemberMailingList {
							MemberID = memberID,
							MailingListID = mailList.ID
						};
						service.AddToMemberMailingList(newMember);

					}
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, mailingList, mailingList.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SendBulkEmail(long newsletterID, long[] mailingListID, string attachments, string message, long voucherID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Guid messageIdentifier = Guid.NewGuid();

				var clientDomains = (from domain in service.Domain
									 where domain.ClientSiteID == SessionManager.ClientSiteID
									 && domain.DomainName.Contains("boltcms.com") == false
									 select domain.DomainName
									).FirstOrDefault();

				//check if we are in the live environment
				if (Server.MachineName != ConfigurationManager.AppSettings["LiveServerName"]) {
					clientDomains = "aurorafe";
				}

				if (CacheManager.SiteData.CommEngineSiteID == null) {
					Utils.ServiceSuccessResultHandler(ref data,
													  "Error: No Comm Engine ID has been setup for this client", 0);
					data["ErrorMessage"] = "Error";
					return data;
				}
				bool isVoucher = voucherID > 0;


				if (clientDomains == null) {
					Utils.ServiceSuccessResultHandler(ref data,
												   "Error: No public domain entry has been setup for this client please contact support", 0);
					data["ErrorMessage"] = "Error";
					return data;
				}

				if (String.IsNullOrEmpty(attachments)) {
					attachments = null;
				}

				foreach (long mailList in mailingListID) {
					//check the amount of members in  the mailinglist and then generate
					if (isVoucher) {
						var memberCount = (from memberMail in service.MemberMailingList
										   join members in service.Member on memberMail.MemberID equals members.ID
										   where memberMail.MailingListID == mailList
										   select members);
						GenerateVoucherCodesForMembers(voucherID, memberCount.ToList());
					}

					//Replace Unsubscribe tag
					string readReceipt = String.Format("<img src=\"http://{0}/ViewNewsletter.aspx?messageid={1}&memberid={2}&viewonline=false\">", clientDomains, messageIdentifier, "{member}");
					string unsubscribe = String.Format("<a href=\"http://{0}/UnSubscribe.aspx?mail={1}&memberid={2}\"><b><span style=\"FONT-FAMILY: Trebuchet MS\">UnSubscribe</span></b></a>", clientDomains, messageIdentifier, "{member}");
					string viewonline = String.Format("<a href=\"http://{0}/ViewNewsletter.aspx?messageid={1}&memberid={2}&viewonline=true\"><span style=\"FONT-FAMILY: Trebuchet MS\">View Online</span></a>", clientDomains, messageIdentifier, "{member}");

					message = message.Replace("{Unsubscribe}", unsubscribe);
					message = message.Replace("{ViewOnline}", viewonline);
					message += readReceipt;

					var response = service.InsertIntoCommEngine(SessionManager.ClientSiteID, message,
											 CacheManager.SiteData.CommEngineSiteID, mailList,
											 attachments, CacheManager.SiteData.Email1, voucherID, isVoucher).First();

					//Add a record for our tracking purposes
					NewsLetterCommEngine newEntry = new NewsLetterCommEngine {
						ClientSiteID = SessionManager.ClientSiteID.Value,
						CommEngineMessageID = response.Value,
						InsertedOn = DateTime.Now,
						InsertedBy = SessionManager.UserID,
						MailListID = mailList,
						NewsLetterID = newsletterID,
						SystemMessageID = messageIdentifier
					};
					service.NewsLetterCommEngine.AddObject(newEntry);
					service.SaveChanges();
				}

				Utils.ServiceSuccessResultHandler(ref data, "Success", 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetNewsLetterReports(long newsLetterID, int iPage, int iPageSize) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var report = from newsletterReport in service.NewsLetterCommEngine
							 join mailingList in service.MailingList on newsletterReport.MailListID equals
								 mailingList.ID
							 where newsletterReport.NewsLetterID == newsLetterID
								   && newsletterReport.ClientSiteID == SessionManager.ClientSiteID
							 orderby newsletterReport.InsertedOn descending
							 select new { newsletterReport, mailingList };

				Utils.ServiceSuccessResultHandler(ref data, report.ToList().Paginate(iPage, iPageSize), report.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetDetailEmailReport(Guid messageID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				List<GetNewsLetterReport_Result> reportItems;
				reportItems = new List<GetNewsLetterReport_Result>(service.GetNewsLetterReport(CacheManager.SiteData.CommEngineSiteID, messageID));

				Utils.ServiceSuccessResultHandler(ref data, reportItems, reportItems.Count);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetDetailMessageReport(Guid messageID, int iPage, int iPageSize, string iFilter) {
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				string[] filter = iFilter.Split(':');
				List<GetCommEngineMessageDetailedReport_Result> listOfEmails;

				if (filter.Count() == 2) {
					listOfEmails = service.GetCommEngineMessageDetailedReport(messageID, filter[0].ToUpper(), filter[1]).ToList();

				} else if (filter.Count() == 1) {
					listOfEmails = service.GetCommEngineMessageDetailedReport(messageID, filter[0], null).ToList();
				} else {
					listOfEmails = service.GetCommEngineMessageDetailedReport(messageID, null, null).ToList();
				}

				Utils.ServiceSuccessResultHandler(ref data, listOfEmails, listOfEmails.Count);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		#endregion

		#region Voucher
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllVouchers() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var results = service.GetAllVouchers(SessionManager.ClientSiteID);

				data.Add("Data", results);
				data.Add("Result", true);
				data.Add("ErrorMessage", string.Empty);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetVoucherTypes() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var query = from voucherTypes in service.Types
							where voucherTypes.DeletedBy == null
								  && voucherTypes.DeletedOn == null
							select voucherTypes;
				Utils.ServiceSuccessResultHandler(ref data, query.ToList(), query.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetVoucherConditions() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var query = from voucherConditions in service.Conditions
							where voucherConditions.DeletedBy == null
								  && voucherConditions.DeletedOn == null
							select voucherConditions;

				Utils.ServiceSuccessResultHandler(ref data, query.ToList(), query.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateVoucher(Custom.Data.Voucher voucher) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (voucher.ID == 0) {
					voucher.InsertedBy = SessionManager.UserID;
					voucher.InsertedOn = DateTime.Now;
					service.AddToVoucher(voucher);
				} else {
					if (voucher.DeletedOn != null) {
						voucher.DeletedBy = SessionManager.UserID;
					}

					service.Voucher.Attach(voucher);
					voucher.SetAllModified(service);
				}
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, voucher, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GenerateVouchers(long voucherID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GenerateVoucherCodes(long voucherID, int amount) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				for (int item = 0; item < amount; item++) {
					Dictionary<string, string> voucherCode = Utils.GetVoucherHash(voucherID);
					var voucherItem = new VoucherItem {
						Code = voucherCode["VoucherCode"],
						Redeemed = false,
						VoucherID = voucherID,
						VoucherIDCode = voucherCode["VoucherBatchID"],
						Status = "Free",
						InsertedOn = DateTime.Now
					};

					service.AddToVoucherItem(voucherItem);
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, amount, amount);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GenerateVoucherCodesForMembers(long voucherID, List<Member> members) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				foreach (Member member in members) {
					Dictionary<string, string> voucherCode = Utils.GetVoucherHash(voucherID);
					var voucherItem = new VoucherItem {
						Code = voucherCode["VoucherCode"],
						Redeemed = false,
						VoucherID = voucherID,
						VoucherIDCode = voucherCode["VoucherBatchID"],
						Status = "Free",
						InsertedOn = DateTime.Now,
						MemberID = member.ID
					};

					service.AddToVoucherItem(voucherItem);
				}


				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, string.Empty, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> RedeemVouchers(string voucherCode, long voucherID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var codes = from voucherItem in service.VoucherItem
							where voucherItem.Code == voucherCode
								  && voucherItem.Redeemed == false
							select voucherItem;
				codes.First().Redeemed = true;

				codes.First().SetAllModified(service);
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, codes.ToList(), codes.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> DiscardVouchers(long voucherID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var deleteObjs = from voucherItems in service.VoucherItem
								 where voucherItems.VoucherID == voucherID
								 && voucherItems.Redeemed == false
								 && voucherItems.Status == "Free"
								 select voucherItems;

				foreach (VoucherItem voucherItem in deleteObjs) {
					service.VoucherItem.DeleteObject(voucherItem);
				}

				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, true, 0);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region E-Commerce
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSiteTransactions(string type, int iPageNum, int iPageSize, string filter) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			string[] filterCriteria = filter.Split(':');
			try {
				var mySiteTransactions = from transactions in service.TransactionLog
										 join paymentSchema in service.PaymentSchema on transactions.PaymentSchemaID
											 equals paymentSchema.ID
										 join member in service.Member on transactions.MemberID equals member.ID
											 into leftJoin
										 from transactionMember in leftJoin.Where(mem => mem.ID == transactions.MemberID).DefaultIfEmpty()
										 where transactions.PaymentType == type && transactions.ClientSiteID == SessionManager.ClientSiteID
										 orderby transactions.InsertedOn descending
										 select new { transactions, paymentSchema, member = transactionMember };

				if (filterCriteria.Length > 0) {
					switch (filterCriteria[0]) {
						case "Date":
							string[] range = filterCriteria[1].Split('|');
							DateTime start = DateTime.ParseExact(range[0], "dd/MM/yyyy", null);
							DateTime end = DateTime.ParseExact(range[1], "dd/MM/yyyy", null).AddDays(1);
							mySiteTransactions = mySiteTransactions.Where(myt => myt.transactions.InsertedOn >= start && myt.transactions.InsertedOn <= end);
							break;
						case "Amount":
							if (filterCriteria[1] == "A") {
								mySiteTransactions = mySiteTransactions.OrderBy(myt => myt.transactions.TotalAmount);
							} else if (filterCriteria[1] == "D") {
								mySiteTransactions = mySiteTransactions.OrderByDescending(myt => myt.transactions.TotalAmount);
							}
							break;
						case "MemberName":
							string name = filterCriteria[1].ToUpper();
							mySiteTransactions = mySiteTransactions.Where(myt => (myt.member.FirstName.ToUpper() + " " + myt.member.LastName.ToUpper()).Contains(name) || myt.member.Email.ToUpper().Contains(name));
							break;
						case "TID":
							long id;
							long.TryParse(filterCriteria[1], out id);
							mySiteTransactions = mySiteTransactions.Where(myt => myt.transactions.ID == id);
							break;
					}
				}
				Utils.ServiceSuccessResultHandler(ref data, mySiteTransactions.Paginate(iPageNum, iPageSize), mySiteTransactions.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		
        [WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetTransactionTypesPerClient() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var types = from transactions in service.TransactionLog
							where transactions.ClientSiteID == SessionManager.ClientSiteID
							group transactions by transactions.PaymentType
								into groupedResults
								select new { Names = groupedResults.Key };

				Utils.ServiceSuccessResultHandler(ref data, types.ToList(), types.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		
        [WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetTransactionProducts(long transactionID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var transaction = (from transactions in service.TransactionLog
								   join paymentSchema in service.PaymentSchema on transactions.PaymentSchemaID equals paymentSchema.ID
								   join member in service.Member on transactions.MemberID equals member.ID into leftJoin
								   from member in leftJoin.Where(mem => mem.ID == transactions.MemberID).DefaultIfEmpty()
								   where transactions.ID == transactionID && transactions.ClientSiteID == SessionManager.ClientSiteID
								   select new { transactions, paymentSchema, member }).FirstOrDefault();

				if (transaction == null) {
					Utils.ServiceSuccessResultHandler(ref data, string.Empty, 0);
					return data;
				}

				if (transaction.transactions.PaymentType != "Donation") {
					XDocument products = XDocument.Parse(transaction.transactions.DataXML);
					var productList = from p in products.Descendants("field")
									  join dbp in service.Product on int.Parse(p.Attribute("id").Value) equals dbp.ID
									  join colour in service.Colour on int.Parse(p.Attribute("colourid").Value) equals colour.ID into leftJoin
									  from colour in leftJoin.DefaultIfEmpty()
									  join sizes in service.Size on int.Parse(p.Attribute("sizeid").Value) equals sizes.ID into leftJoinSizes
									  from sizes in leftJoinSizes.DefaultIfEmpty()
									  select new {
										  dbp.Name,
										  dbp.Price,
										  BasketPrice = Convert.ToDecimal(p.Attribute("price").Value),
										  Quantity = Convert.ToInt32(p.Attribute("quantity").Value),
										  Colour = colour != null ? colour.Name : string.Empty,
										  Size = sizes.Description
									  };
					if (productList.Count() == 0) {
						Utils.ServiceSuccessResultHandler(ref data, string.Empty, 0);
						return data;
					}


					data.Add("TransactionDate", String.Format("{0:dddd, dd MMMM yyyy HH:mm}", transaction.transactions.InsertedOn));
					data.Add("ClientName", transaction.member != null ? transaction.member.FirstName + " " + transaction.member.LastName : "N/A");

					var basketField = products.Root.Element("Fields");
					data.Add("VoucherCode", Utils.ConvertDBNull(basketField.Attribute("vouchercode"), new XAttribute("vouchercode", string.Empty)).Value);
					data.Add("VoucherAmount", Utils.ConvertDBNull(basketField.Attribute("voucheramount"), new XAttribute("voucheramount", string.Empty)).Value);
					data.Add("VATAmount", basketField.Attribute("vat").Value);
					data.Add("Total", basketField.Attribute("total").Value);
					if (XDocument.Parse(transaction.transactions.DataXML).Descendants("XmlData").First().Attributes("ShippingCost").FirstOrDefault() != null) {
						data.Add("Shipping", XDocument.Parse(transaction.transactions.DataXML).Descendants("XmlData").Select(trns => trns.Attribute("ShippingCost").Value).First());
					}


					Utils.ServiceSuccessResultHandler(ref data, productList.ToList(), productList.Count());
				}
				//else
				//{
				//    data.Add("TransactionDate", String.Format("{0:dddd, dd MMMM yyyy HH:mm}", transaction.transactions.InsertedOn));
				//    data.Add("ClientName", transaction.member != null ? transaction.member.FirstName + " " + transaction.member.LastName : "N/A");
				//}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;

		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetTransactionsForType(long transactionID, string transactionType) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				var transaction = (from trans in service.TransactionLog
								   where trans.ID == transactionID
								   select trans).SingleOrDefault();
				if (transaction != null) {
					switch (transactionType) {
						case "Donation":
							XmlDocument dataXml = new XmlDocument();
							dataXml.LoadXml(transaction.DataXML);
							var donorInfo = from xml in dataXml.GetElementsByTagName("field").Cast<XmlNode>()
											select new { name = xml.Attributes["fieldname"].Value, value = xml.Attributes["value"].Value };

							Utils.ServiceSuccessResultHandler(ref data, donorInfo.ToList(), donorInfo.Count());
							return data;
							break;
					}
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}


		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SetTransActionCompleted(long transactionID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var trans = (from transactions in service.TransactionLog
							 where transactions.ID == transactionID
							 select transactions).SingleOrDefault();

				if (trans != null) {
					trans.Completed = true;
					trans.SetAllModified(service);
					service.SaveChanges();
				}

                //send email to client & remove stock if you change this then change it on CompletePayment.aspx.cs
                StringBuilder additionalEmail = new StringBuilder();

                if (trans.DataXML != null) {
                    additionalEmail.Append("</br></br>Below is a list of products that you purchased. If any are digital products then you can click on the link and download them.</br></br>");

                    XmlDocument products = new XmlDocument();
                    products.LoadXml(trans.DataXML);
                    XmlNode rootElement = products.GetElementsByTagName("Fields").Item(0);
                    List<dynamic> receiptItems = new List<dynamic>();

                    additionalEmail.Append("<table style=\"width: 100%; border-collapse: collapse;\"><tr><th style=\"border: 1px solid #333; text-align: left; padding: 8px 8px;\">Product</th><th style=\"border: 1px solid #333; text-align: left; padding: 8px 8px;\">Quantity</th></tr>");

                    //update product totals in stock
                    foreach (XmlElement product in rootElement) {
                        long productID = long.Parse(product.GetAttribute("id"));
                        int quantity = int.Parse(product.GetAttribute("quantity"));
                        decimal basketPrice = decimal.Parse(product.GetAttribute("price"));

                        var productEntity = (from prodTable in service.Product
                                             where prodTable.ID == productID
                                             select prodTable
                                            ).SingleOrDefault();

                        if (productEntity != null && productEntity.Total > quantity) {
                            productEntity.Total = productEntity.Total - quantity;
                            productEntity.SetAllModified(service);
                            service.SaveChanges();
                        }

                        if (String.IsNullOrEmpty(productEntity.DownloadURL)) {
                            additionalEmail.AppendFormat("<tr><td style=\"border: 1px solid #333; padding: 0px 8px;\">{0}</td><td style=\"border: 1px solid #333; padding: 0px 8px;\">{1}</td></tr>", productEntity.Name, quantity);
                        } else {
                            additionalEmail.AppendFormat("<tr><td style=\"border: 1px solid #333; padding: 0px 8px;\">{0} (<a href='{2}'>Download</a>)</td><td style=\"border: 1px solid #333; padding: 0px 8px;\">{1}</td></tr>", productEntity.Name, quantity, productEntity.DownloadURL);
                        }
                        receiptItems.Add(new { productEntity.Name, quantity, productEntity.Price, basketPrice });
                    }
                    additionalEmail.Append("</table>");

                    //mark voucher as used
                    if (products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"] != null) {

                        if (products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"].Value != null) {
                            string voucherCode =
                             products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"].Value;
                            if (!String.IsNullOrEmpty(voucherCode)) {
                                var voucher =
                                    (from voucherItems in service.VoucherItem
                                     where voucherItems.Code == voucherCode
                                     select voucherItems).SingleOrDefault();

                                voucher.Redeemed = true;

                                voucher.SetAllModified(service);
                                service.SaveChanges();
                            }
                        }

                    }

                    //Send Comms to Member
                    var xmlData = new List<dynamic> { (from m in service.Member where m.ID == trans.MemberID select m).SingleOrDefault() };
                    Utils.SendToMessageCentre(Utils.MessageCentreEvents.ProductPurchaseCompleted.GetEnumDescriptionAttribute(), additionalEmail.ToString(), null, "Purchase from: " + CacheManager.SiteData.CompanyName, Utils.GenerateCommunicationXML(xmlData), CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
                }

				Utils.ServiceSuccessResultHandler(ref data, trans, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region CustomFields
		/// <summary>
		/// Gets Custom Fields for a custom module
		/// </summary>
		/// <param name="moduleName"></param>
		/// <param name="customModuleID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetModuleCustomFields(string moduleName, long customModuleID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				string customFieldsXML = string.Empty;

				if (!string.IsNullOrEmpty(moduleName)) {

					customFieldsXML = (from moduleSchema in service.Schema
									   join modules in service.Module on moduleSchema.ModuleID equals modules.ID
									   where
										   modules.Name == moduleName &&
										   moduleSchema.ClientSiteID == SessionManager.ClientSiteID
									   select moduleSchema.SchemaXML).FirstOrDefault();
				} else if (customModuleID > 0) {
					customFieldsXML = (from moduleSchema in service.CustomModules
									   where
										   moduleSchema.ID == customModuleID &&
										   moduleSchema.ClientSiteID == SessionManager.ClientSiteID
									   select moduleSchema.XMLSchema).FirstOrDefault();
				}


				if (customFieldsXML != null) {
					Utils.ServiceSuccessResultHandler(ref data, customFieldsXML, 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, "", 0);
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Updates custom module fields
		/// </summary>
		/// <param name="xml"></param>
		/// <param name="moduleName"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateModuleCustomFields(string xml, string moduleName) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				if (!string.IsNullOrEmpty(moduleName)) {

					var customXml = (from moduleSchema in service.Schema
									 join modules in service.Module on moduleSchema.ModuleID equals modules.ID
									 where modules.Name == moduleName && moduleSchema.ClientSiteID == SessionManager.ClientSiteID
									 select moduleSchema).SingleOrDefault();
					//If no previous record of custom XML is found, create one.
					if (customXml == null) {
						Aurora.Custom.Data.Schema customSchema = new Aurora.Custom.Data.Schema();
						customSchema.ModuleID = (from modules in service.Module
												 where modules.Name == moduleName
												 select modules.ID).First();
						customSchema.ClientSiteID = SessionManager.ClientSiteID;
						customSchema.SchemaXML = xml;
						customSchema.InsertedOn = DateTime.Now;
						customSchema.InsertedBy = SessionManager.UserID;
						service.AddToSchema(customSchema);
					}
						//update existing XML
					else {
						customXml.SchemaXML = xml;
						customXml.SetAllModified(service);
					}
				}


				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, "", null);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Updates the configuration fields for a custom module
		/// </summary>
		/// <param name="xml"></param>
		/// <param name="customModuleID"></param>
		/// <param name="customModule"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateCustomModuleCustomFields(string xml, long customModuleID, Custom.Data.CustomModules customModule) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				if (customModuleID != 0) {
					customModule.XMLSchema = xml;
					UpdateCustomModule(customModule);
					service.SaveChanges();
				}
				Utils.ServiceSuccessResultHandler(ref data, "", null);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetModuleCustomFieldsXMLData(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				System.IO.StringWriter sw = new StringWriter();
				HtmlTextWriter htw = new HtmlTextWriter(sw);

				//get schema and data xml
				var customXMLSchema = (from modschema in service.Schema
									   join modules in service.Module on modschema.ModuleID equals modules.ID
									   where modules.Name == "Products" && modschema.ClientSiteID == SessionManager.ClientSiteID
									   select modschema.SchemaXML).FirstOrDefault();

				if (!String.IsNullOrEmpty(customXMLSchema)) {
					string customXMLData = string.Empty;
					if (productID >= 0) {
						customXMLData = (from products in service.Product
										 where products.ID == productID &&
										 products.ClientSiteID == SessionManager.ClientSiteID
										 select products.CustomXML).FirstOrDefault();
					}

					//create in-memory object for xml panel
					XmlDocument moduleSchema = new XmlDocument();
					XmlDocument moduleData = new XmlDocument();

					//Load XML
					moduleSchema.LoadXml(customXMLSchema);
					if (!String.IsNullOrEmpty(customXMLData)) {
						moduleData.LoadXml(customXMLData);

						//Removes all "Fields" that do not have the type "CustomFields"
						XmlNodeList nodes = moduleData.SelectNodes("//Fields[not(@type='CustomFields')]");
						for (int i = 0; i < nodes.Count; i++) {
							nodes[i].ParentNode.RemoveChild(nodes[i]);
						}
					}

					ObjectPanel xmlContainer = new ObjectPanel();
					XmlPanel xmlRenderContainer = new XmlPanel();

					xmlContainer.ID = "ContentPanel";
					xmlRenderContainer.ID = "ContentXml";

					xmlContainer.Controls.Add(xmlRenderContainer);
					xmlRenderContainer.XmlPanelSchema = moduleSchema;
					xmlRenderContainer.XmlPanelData = moduleData;

					xmlRenderContainer.XmlPanelSchema = moduleSchema;
					xmlRenderContainer.XmlPanelData = moduleData;


					xmlRenderContainer.RenderXmlControls("tbl_Form");
					xmlRenderContainer.RenderControl(htw);
				}

				Utils.ServiceSuccessResultHandler(ref data, htw.InnerWriter.ToString(), 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCustomModules(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var customModules = (from custModules in service.CustomModules
									 where custModules.ClientSiteID == SessionManager.ClientSiteID
									 && custModules.DeletedBy == null
									 && custModules.DeletedOn == null
									 orderby custModules.Name
									 select custModules).DefaultIfEmpty();

				Utils.ServiceSuccessResultHandler(ref data, customModules.ToList().Paginate(iPageNum, iPageSize),
												  customModules.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		/// <summary>
		/// Gets all data for a custom module
		/// </summary>
		/// <param name="customModuleID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCustomModulesSaved(long customModuleID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSqlDBConnection();

				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[proc_GetCustomModuleColumnsByModuleID]";
					cmd.Parameters.AddWithValue("@CustomModuleID", customModuleID);

					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					XmlDocument xdoc = new XmlDocument();
					xdoc.LoadXml(ds.GetXml());
					if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) {
						Utils.ServiceSuccessResultHandler(ref data, "", 0);
					} else {
						Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc).Replace("_x0030_", string.Empty).Replace("_x0031_", string.Empty).Replace("_x0020_", string.Empty), ds.Tables[0].Rows.Count);
					}
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateCustomModule(Custom.Data.CustomModules customModule) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				if (customModule.ID == 0) {
					service.AddToCustomModules(customModule);
				} else {
					service.CustomModules.Attach(customModule);
					customModule.SetAllModified(service);
				}

				service.SaveChanges();
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		#endregion

		#region Smarter Stats
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetBandwidthPerSite(int siteID, int months) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				SmarterStats.ServerInfo serversettings = new Custom.SmarterStats.ServerInfo();
				//move to cache manager
				serversettings.UserName = "admin";
				serversettings.Password = "sawd49";
				serversettings.ServerIP = "stats.sawebdesign.com";
				serversettings.ServerPort = string.Empty;

				Custom.SmarterStats smarterstats = new Custom.SmarterStats(serversettings);

				var stats = smarterstats.GetBandwidthForSite(siteID, DateTime.Now.AddMonths(months), DateTime.Now);
				Utils.ServiceSuccessResultHandler(ref data, stats, stats.Count());


			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetAllSites() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				SmarterStats.ServerInfo serversettings = new Custom.SmarterStats.ServerInfo();
				//move to cache manager
				serversettings.UserName = "admin";
				serversettings.Password = "sawd49";
				serversettings.ServerIP = "stats.sawebdesign.com";
				serversettings.ServerPort = string.Empty;

				Custom.SmarterStats smarterstats = new Custom.SmarterStats(serversettings);

				var sites = smarterstats.GetSites();

				Utils.ServiceSuccessResultHandler(ref data, sites, sites.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region Message Centre
		/// <summary>
		/// Gets all messages for the current client
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSytemMessagesForClient() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var messages = from message in service.Message
							   join module in service.Module on message.FeatureModuleID equals module.ID
							   join eventData in service.MessageEvent on message.MessageEventID equals eventData.ID
							   where message.ClientSiteID == SessionManager.ClientSiteID
							   && message.DeletedOn == null
							   && message.DeletedBy == null
							   select new { eventData, message, module };

				Utils.ServiceSuccessResultHandler(ref data, messages.ToList(), messages.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Gets all messages for the current membership category
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMemberCategoryMessagesForClient(long MemberCategoryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var messages = (from msgs in service.MemberCategoryMessage
								where msgs.ClientSiteID == SessionManager.ClientSiteID
								&& msgs.MemberCategoryID == MemberCategoryID
								&& msgs.DeletedOn == null
								orderby msgs.Day
								select msgs);

				Utils.ServiceSuccessResultHandler(ref data, messages.ToList(), messages.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Get all system events and modules
		/// </summary>
		/// <param name="moduleID"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSystemMessageEventsForModules(long moduleID) {

			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var messageModule = moduleID > 0
										? from eventmsgs in service.MessageEvent
										  join clientModules in service.ClientModule on eventmsgs.FeaturesModuleID equals clientModules.FeaturesModuleID
										  join module in service.Module on clientModules.FeaturesModuleID equals module.ID
										  from excludeMsg in service.MessageEventExclude.Where(w => w.ClientSiteID == SessionManager.ClientSiteID && w.messageID == eventmsgs.ID).DefaultIfEmpty()
										  where eventmsgs.FeaturesModuleID == moduleID
										  && clientModules.ClientSiteID == SessionManager.ClientSiteID
										  && (eventmsgs.ClientSiteID == SessionManager.ClientSiteID || eventmsgs.ClientSiteID == null || eventmsgs.ClientSiteID == 10000)
										  && excludeMsg == null
										  select new { eventmsg = eventmsgs, module }
										  :
										  from eventmsgs in service.MessageEvent
										  join clientModules in service.ClientModule on eventmsgs.FeaturesModuleID equals clientModules.FeaturesModuleID
										  join module in service.Module on clientModules.FeaturesModuleID equals module.ID
										  from excludeMsg in service.MessageEventExclude.Where(w => w.ClientSiteID == SessionManager.ClientSiteID && w.messageID == eventmsgs.ID).DefaultIfEmpty()
										  where clientModules.ClientSiteID == SessionManager.ClientSiteID
										  && (eventmsgs.ClientSiteID == SessionManager.ClientSiteID || eventmsgs.ClientSiteID == null || eventmsgs.ClientSiteID == 10000)
										  && excludeMsg == null
										  select new { eventmsg = eventmsgs, module };
				;

				Utils.ServiceSuccessResultHandler(ref data, messageModule.ToList().OrderBy(items => items.module.FriendlyName).ThenBy(item => item.eventmsg.EventType), messageModule.Count());

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		/// <summary>
		/// Updates an existing message within the system
		/// </summary>
		/// <param name="messageData">Message Entity</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateSystemMessage(Custom.Data.Message messageData) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				if (messageData.ID == 0) {
					//check if message for this event alredy exists
					var currentMessage = from messages in service.Message
										 where messages.FeatureModuleID == messageData.FeatureModuleID
											   && messages.MessageEventID == messageData.MessageEventID
											   && messages.ClientSiteID == SessionManager.ClientSiteID
											   && messages.DeletedBy == null
											   && messages.DeletedOn == null
										 select messages;
					if (currentMessage.Count() > 0) {
						Utils.ServiceSuccessResultHandler(ref data, "Error: An Event for that module already exists", 1);
						return data;
					}
					service.AddToMessage(messageData);
				} else {
					service.AttachTo("Message", messageData);
					messageData.SetAllModified(service);
				}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, messageData, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Updates an existing message within the system
		/// </summary>
		/// <param name="messageData">Message Entity</param>
		/// /// <param name="isDelete">Delete or not</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateMemberCategoryMessage(MemberCategoryMessage messageData) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				MemberCategoryMessage updateMessage;
				if (messageData.ID == 0) {
					var alreadyExists = (from msgs in service.MemberCategoryMessage
										 where msgs.ClientSiteID == SessionManager.ClientSiteID
										 && msgs.MemberCategoryID == messageData.MemberCategoryID
										 && msgs.Day == messageData.Day
										 && msgs.IsReversed == messageData.IsReversed
										 && msgs.DeletedOn == null
										 select msgs).Count() > 0;
					if (!alreadyExists) {
						updateMessage = new MemberCategoryMessage();
						service.AddToMemberCategoryMessage(updateMessage);
						updateMessage.InsertedBy = SessionManager.UserID;
						updateMessage.InsertedOn = DateTime.Now;
						updateMessage.MemberCategoryID = messageData.MemberCategoryID;
						updateMessage.ClientSiteID = SessionManager.ClientSiteID;
					} else {
						Utils.ServiceSuccessResultHandler(ref data, "Error: A message has already been created for this day of the category", 1);
						return data;
					}
				} else {
					updateMessage = (from mcm in service.MemberCategoryMessage
									 where mcm.ID == messageData.ID &&
									 mcm.ClientSiteID == SessionManager.ClientSiteID
									 select mcm).FirstOrDefault();
					if (updateMessage == null) {
						Utils.ServiceSuccessResultHandler(ref data, "Error: Unable to perform update, message not found", 1);
						return data;
					}
				}
				updateMessage.Name = messageData.Name;
				updateMessage.Subject = messageData.Subject;
				updateMessage.HTML = messageData.HTML;
				updateMessage.Day = messageData.Day;
				updateMessage.IsReversed = messageData.IsReversed;
				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, messageData, 1);

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		#endregion

		#region File System
		/// <summary>
		/// Gets Upload Folder Size
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetFolderSize(string specificFolder) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string directoryPath = String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, specificFolder);
				if (Directory.Exists(directoryPath)) {
					DirectoryInfo uploadDirectory = new DirectoryInfo(Server.MapPath(directoryPath));
					var totalSize = uploadDirectory.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(files => files.Length);
					Utils.ServiceSuccessResultHandler(ref data, totalSize, 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, 0, 1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="specificFolder"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetFolders(string specificFolder) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string directoryPath = String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, specificFolder);



				if (Directory.Exists(directoryPath)) {
					DirectoryInfo uploadDirectory = new DirectoryInfo(Server.MapPath(directoryPath));
					var directories = uploadDirectory.EnumerateDirectories();

					Utils.ServiceSuccessResultHandler(ref data, directories.Select(dirInfo => new { dirInfo.FullName, dirInfo.Name, ParentFullName = dirInfo.Parent.FullName }), directories.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, new string[2], 0);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="specificFolder"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetFiles(string specificFolder) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string directoryPath = string.Empty;

				specificFolder = specificFolder.ToUpper().Replace(HttpContext.Current.Request["APPL_PHYSICAL_PATH"].Replace("Aurora.Manage", "Aurora.Web").ToUpper(),
																 string.Empty).Replace(@"\", "/");

				specificFolder = specificFolder.Replace(String.Format("CLIENTDATA/{0}/UPLOADS/", SessionManager.ClientSiteID), string.Empty);

				directoryPath = String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, specificFolder);



				DirectoryInfo uploadDirectory = new DirectoryInfo(Server.MapPath(directoryPath));
				if (Directory.Exists(uploadDirectory.FullName)) {
					var directories = uploadDirectory.GetFileSystemInfos("*.*", SearchOption.TopDirectoryOnly).Where(currentDir => currentDir.Attributes.ToString() != "Directory");
					Utils.ServiceSuccessResultHandler(ref data, directories.Select(files => new { files.Name, files.FullName, files.Extension, RelativePath = files.FullName.ToUpper().Replace(HttpContext.Current.Request.ServerVariables["APPL_PHYSICAL_PATH"].ToUpper(), "").Replace(@"\", "/") }), directories.Count());
				} else {
					Utils.ServiceSuccessResultHandler(ref data, null, 0);
				}


			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> SetPrimaryImage(string primaryURL, long memberID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var member = (from mem in service.Member
							  where mem.ID == memberID && mem.ClientSiteID == SessionManager.ClientSiteID
							  select mem).FirstOrDefault();
				if (member != null) {
					member.PrimaryImageURL = primaryURL;
					service.SaveChanges();
					Utils.ServiceSuccessResultHandler(ref data, true, 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, false, 0);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileFolderPath"></param>
		/// <param name="destinationPath"></param>
		/// <param name="opertationType"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> FileFolderAED(string fileFolderPath, string destinationPath, int opertationType) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				string fullPath = fileFolderPath;
				string fullDestinationPath = Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, destinationPath));
				bool isSuccessful = false;

				if (File.Exists(fullPath)) {
					switch (opertationType) {
						case 0:
							//delete
							if (File.Exists(fullPath)) {
								File.Delete(fullPath);
							}

							if (Directory.Exists(fullPath)) {
								Directory.Delete(fullPath);
							}

							isSuccessful = true;
							break;
						case 1:
							//move
							if (File.Exists(fullPath) && Directory.Exists(fullDestinationPath)) {
								File.Move(fullPath, fullDestinationPath);
							}
							isSuccessful = true;
							break;
						case 2:
							//copy
							if (File.Exists(fullPath)) {
								File.Copy(fullPath, fullDestinationPath);
							}
							isSuccessful = true;
							break;
						case 3:
							//rename
							if (File.Exists(fullPath)) {
								File.Copy(fullPath, fullDestinationPath);
								File.Delete(fullPath);
							}
							isSuccessful = true;
							break;
					}
				}

				Utils.ServiceSuccessResultHandler(ref data, isSuccessful, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		#endregion

		#region B2M Donors
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> PlaceEnquiry(long[] items) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				Utils.CheckSqlDBConnection();

				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[dbo].[B2M_GetDonors]";
					cmd.Parameters.AddWithValue("@WHERELOGIC", " N.ID IN(" + String.Join(",", items) + ")");
					cmd.Parameters.AddWithValue("@isTopTen", 0);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					string filePath = Utils.CreatePDFDocument(ds.Tables[0].AsEnumerable().ToList<dynamic>(), "Donor Wish List");

					Utils.ServiceSuccessResultHandler(ref data, filePath, 1);

				}

			} catch (Exception ex) {

			}
			return data;
		}
		#endregion

		#region DataExports
		/// <summary>
		/// 
		/// </summary>
		/// <param name="formName"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ExportDataForCustomForm(long customModuleID, string[] fieldList) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[Features].[ExportCustomModuleData]";
					cmd.Parameters.AddWithValue("@CustomModuleID", customModuleID);
					cmd.Parameters.AddWithValue("@FieldList", string.Join(",", fieldList));

					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					StringBuilder sb = new StringBuilder();
					//set seperator
					sb.AppendLine("sep=|");
					//CGCSA Registration form
					if (SessionManager.ClientSiteID == 10079 && customModuleID == 10002) {
						sb.AppendLine("Full Company Name|VAT No|Address|Main Email Address|     |Account First Name|Account Surname|Account Contact No|Account Email Address|   |First Name|Surname|Email Address|Position|Dietary Requirements|Main Contact No|Attend Cocktail Party|Special Requests");

						foreach (DataRow row in ds.Tables[0].Rows) {
							string itemToAppend = string.Empty;
							int forward = 0;
							bool skipRow = false;


							for (int column = 8; column < row.ItemArray.Length; column++) {
								skipRow = String.IsNullOrEmpty(row.ItemArray[column].ToString()) ? true : false;
								if (!skipRow) {
									skipRow = row.ItemArray[column].ToString() == "NotSelected"
												  ? true
												  : false;
								}

								if (!skipRow) {

									if (forward == 5) {

										if (!String.IsNullOrEmpty(itemToAppend)) {
											itemToAppend = row.ItemArray[0] + "|=\"" + row.ItemArray[1] + "\"|" + row.ItemArray[2] + "|" + row.ItemArray[3] + "|\"" + "   \"|" + row.ItemArray[4] + "|" + row.ItemArray[5] + "|=\"" + row.ItemArray[6] + "\"|" + row.ItemArray[7] + "|\"  \"|" + itemToAppend;
											sb.AppendLine(itemToAppend + row.ItemArray[column].ToString() + "|" + row["SpecialRequests"]);
										}

										itemToAppend = string.Empty;
										forward = 0;
									} else {
										if (forward == 4) {
											itemToAppend += row.ItemArray[column].ToString() + "|" + "=\"" + row.ItemArray[row.ItemArray.Length - 2] + "\"|";
										} else {
											itemToAppend += row.ItemArray[column].ToString() + "|";
										}

										forward++;
									}

								} else {
									itemToAppend = string.Empty;
								}
							}

						}
					} else if (SessionManager.ClientSiteID == 10095 && customModuleID == 10055) {
						sb.AppendLine("Full Company Name|VAT No|Address|Main Email Address|     |Account First Name|Account Surname|Account Contact No|Account Email Address|   |Special Requests|Independent Cash & Carry|The Buying Exchange Company|Elite Star Trading|Shield|    |First Name|Surname|Email Address|Position|Dietary Requirements");

						foreach (DataRow row in ds.Tables[0].Rows) {
							string itemToAppend = string.Empty;
							string company = string.Join("|",
								"=\"" + row["0FullCompanyName"] + "\"",
								"=\"" + row["VATNo"] + "\"",
								"=\"" + row["Postal Address"] + "\"",
								"=\"" + row["MainEmailAddress"] + "\"",
								"    ",
								"=\"" + row["AccountFirstName"] + "\"",
								"=\"" + row["AccountSurname"] + "\"",
								"=\"" + row["AccountContactNo"] + "\"",
								"=\"" + row["AccountEmailAddress"] + "\"",
								"    ",
								"=\"" + row["SpecialRequests"] + "\"",
								"=\"" + row["IndependentCashCarryGroupICC"] + "\"",
								"=\"" + row["TheBuyingExchangeCompanyBEC"] + "\"",
								"=\"" + row["EliteStarTrading"] + "\"",
								"=\"" + row["Shield"] + "\"",
								"    ");
							List<TIDelegate> delegates = new List<TIDelegate>();
							for (int i = 0; i < 10; i++) {
								string index = i.ToString();
								if (!string.IsNullOrEmpty(row["FirstName" + index].ToString())) {
									TIDelegate del = new TIDelegate {
										firstName = row["FirstName" + index].ToString(),
										surname = row["Surname" + index].ToString(),
										emailAddress = row["EmailAddress" + index].ToString(),
										position = row["Position" + index].ToString(),
										dietaryRequirements = row["DietaryRequirements" + index].ToString()
									};
									delegates.Add(del);
								}
							}
							if (delegates.Count() == 0) {
								delegates.Add(new TIDelegate());
							}
							foreach (TIDelegate item in delegates) {
								sb.AppendLine(company + "|" + string.Join("|", "=\"" + item.firstName + "\"", "=\"" + item.surname + "\"", "=\"" + item.emailAddress + "\"", "=\"" + item.position + "\"", "=\"" + item.dietaryRequirements + "\""));
							}


						}
					} else {
						if (ds.Tables.Count > 0) {
							//append header row
							sb.AppendLine(string.Join("|", ds.Tables[0].Columns.Cast<DataColumn>().Select(cols => Regex.Replace(cols.ColumnName, "[0-999]", string.Empty))));
							foreach (DataRow row in ds.Tables[0].Rows) {
								sb.AppendLine(string.Join("|", row.ItemArray));
							}
						}
					}

					if (ds.Tables.Count > 0) {

						if (!Directory.Exists(Server.MapPath(SessionManager.ClientBasePath) + "\\Uploads\\Exports")) {
							Directory.CreateDirectory(Server.MapPath(SessionManager.ClientBasePath) + "\\Uploads\\Exports");
						}

						string fileName = Guid.NewGuid().ToString().Replace("-", string.Empty);
						File.WriteAllText(String.Format("{0}\\{1}.csv", Server.MapPath(SessionManager.ClientBasePath) + "\\Uploads\\Exports", fileName), sb.ToString());

						Utils.ServiceSuccessResultHandler(ref data, String.Format("{0}/{1}.csv", SessionManager.ClientBasePath + "/Uploads/Exports", fileName), ds.Tables[0].Rows.Count);

					}

				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Exports membership data
		/// </summary>
		/// <param name="formName"></param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> ExportDataForMemberCategory(long? memberCategoryID = null, string status = "") {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				Utils.CheckSqlDBConnection();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[Members].[proc_Member_GetMemberExportData]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					cmd.Parameters.AddWithValue("@CategoryID", memberCategoryID);

					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);
					if (ds.Tables.Count == 2) {
						var headers = "ID," + Regex.Replace(ds.Tables[0].Rows[0].Field<string>("ColumnString"), "[\\[\\]]", "\"");
						headers += "\n";
						DataRowCollection rowContainer = ds.Tables[1].Rows;
						for (int i = 0; i < ds.Tables[1].Rows.Count; i++) {

							for (int j = 0; j < rowContainer[i].ItemArray.Length; j++) {
								headers += "\"" + rowContainer[i].ItemArray[j].ToString() + "\",";
							}
							headers += "\n";
						}
						//File.WriteAllText(Server.MapPath("\\") + "\\oooo.csv", headers);
					}

				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}


		#endregion

		#region Custom Forms
		/// <summary>
		/// Gets Upload Folder Size
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateCustomFormData(string xmlData, long customFormDataID, long customFormID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				//check if record exists
				var customData = (from customForms in service.CustomModuleData
								  where customForms.ID == customFormDataID
								  && customForms.ModuleID == customFormID
								  && customForms.ClientSiteID == SessionManager.ClientSiteID
								  select customForms).FirstOrDefault();

				if (customData == null) {
					var customFormEntity = (from customF in service.CustomModules
											where customF.ID == customFormID
											&& customF.ClientSiteID == SessionManager.ClientSiteID.Value
											select customF).FirstOrDefault();

					customData = new CustomModuleData {
						ModuleID = customFormID,
						Data = xmlData,
						DataSchemaXML = customFormEntity.XMLSchema,
						InsertedOn = DateTime.Now,
						ClientSiteID = SessionManager.ClientSiteID.Value
					};

					service.AddObject("CustomModuleData", customData);
				} else {
					customData.Data = xmlData;
					customData.SetAllModified(service);
				}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, customData, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
		#endregion

		#region IIS
		[WebMethod(EnableSession = true)]
		public bool CreateDomainForClient(string domainName, string currentDomain) {
			bool returnVal = true;
			try {
				ServerManager iisManager = new ServerManager();
				//Add to IIS 
				var site = (from siteCollection in iisManager.Sites
							where siteCollection.Name == currentDomain
							select siteCollection).FirstOrDefault();

				//check if it exists
				bool bindingExists = site.Bindings.Where(bind => bind.Host == domainName).Count() > 0;

				if (!bindingExists) {
					site.Bindings.Add(ConfigurationManager.AppSettings["ServerIP"] + ":80:" + domainName, "http");
					iisManager.CommitChanges();
				}

				//Add to DB
				using (var context = new AuroraEntities()) {
					var domains = from domain in context.Domain
								  where domain.DomainName == domainName
								  select domain;

					if (domains.Count() == 0 && !bindingExists) {
						context.AddToDomain(new Domain {
							ClientSiteID = SessionManager.ClientSiteID.Value,
							DomainName = domainName,
							isSubDomain = false
						});

						context.SaveChanges();
					}
				}


			} catch (Exception ex) {
				returnVal = false;
			}

			return returnVal;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSiteDomains(string currentDomain) {
			var data = new Dictionary<string, object>();
			try {
				using (ServerManager iisManager = new ServerManager()) {

					var site = (from siteCollection in iisManager.Sites
                                where siteCollection.Name.ToLower() == currentDomain.ToLower()
								select siteCollection).FirstOrDefault();
					if (site != null) {
						bool hasRoot = service.Domain.Any(c=> c.ClientSiteID == SessionManager.ClientSiteID && c.isRoot == true);
						Utils.ServiceSuccessResultHandler(ref data, new { domains = site.Bindings.Select(binding => binding.Host).ToList<string>().OrderBy(t => t), hasRoot = hasRoot }, site.Bindings.Count);
					} else {
						Utils.ServiceSuccessResultHandler(ref data, string.Empty, 0);
					}
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// returns a list of 301 redirects, for the currently logged in or impersonated ClientSite
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> Get301Redirects()
		{
			var data = new Dictionary<string, object>();
			try {
				using (ServerManager iisManager = new ServerManager()) {

					List<C301Redirect> site = service.C301Redirect.Where(c=> c.ClientSiteID == SessionManager.ClientSiteID).ToList();
					
					Utils.ServiceSuccessResultHandler(ref data, site, site.Count);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> Get301RedirectById(long ID)
		{
			var data = new Dictionary<string, object>();
			try {
				using (ServerManager iisManager = new ServerManager()) {

					C301Redirect site = service.C301Redirect.Where(c=> c.ID == ID && c.ClientSiteID == SessionManager.ClientSiteID).FirstOrDefault();
					if (site == null)
					{
						site = new C301Redirect();
					}

					Utils.ServiceSuccessResultHandler(ref data, site,  1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> Updatate301redirect(long ID, string OldURL, string NewURL)
		{
			var data = new Dictionary<string, object>();
			try {
				bool isRoot = service.Domain.Any(d=> d.isRoot == true && d.ClientSiteID == SessionManager.ClientSiteID.Value && d.DomainName == NewURL);
				var dbObject = service.C301Redirect.Where(R => R.ID == ID).FirstOrDefault();
				C301Redirect redirectObj = new C301Redirect() {  ID = ID, OldURL = OldURL, NewURL = NewURL, ClientSiteID = SessionManager.ClientSiteID.GetValueOrDefault(0)};
				if (dbObject == null)
				{
					service.C301Redirect.AddObject(redirectObj);
				}
				else
				{
					dbObject.OldURL = redirectObj.OldURL;
					dbObject.NewURL = redirectObj.NewURL;
				}
				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, new { redirectObj = redirectObj, isRoot = isRoot, success = true } , 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> Delete301redirect(long ID)
		{
			var data = new Dictionary<string, object>();
			try {
				
				var dbObject = service.C301Redirect.Where(R => R.ID == ID).FirstOrDefault();
				
				if (dbObject != null)
				{
					service.C301Redirect.DeleteObject(dbObject);
				}
				service.SaveChanges();

				Utils.ServiceSuccessResultHandler(ref data, new { success = true } , 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetSiteDomain(string domainName) {
            Utils.CheckSession();
            var data = new Dictionary<string, object>();
            try {
                using (ServerManager iisManager = new ServerManager()) {

                    var domain = (from Domain in service.Domain
                                  where Domain.DomainName.ToLower() == domainName.ToLower() && Domain.ClientSiteID == SessionManager.ClientSiteID
                                select Domain).FirstOrDefault();
                    
                    if (domain != null) {
                        Utils.ServiceSuccessResultHandler(ref data, domain, 1);
                    } else {
                        Utils.ServiceSuccessResultHandler(ref data, domain, 0);
                    }
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> RemoveDomain(string domainName, string currentDomain) {
            Utils.CheckSession();
			var data = new Dictionary<string, object>();
			try {
				using (var context = new AuroraEntities()) {
					var domain = (from domains in context.Domain
								  where domains.DomainName == domainName
								  && domains.ClientSiteID == SessionManager.ClientSiteID
								  select domains).SingleOrDefault();

					context.DeleteObject(domain);
					context.SaveChanges();
				}
				using (ServerManager iisManager = new ServerManager()) {


					var site = (from siteCollection in iisManager.Sites
								where siteCollection.Name == currentDomain
								select siteCollection).FirstOrDefault();


					var bindingToRemove = site.Bindings.Where(t => t.Host == domainName).FirstOrDefault();
					site.Bindings.Remove(bindingToRemove);
					iisManager.CommitChanges();

					Utils.ServiceSuccessResultHandler(ref data, site.Bindings.Select(binding => binding.Host).ToList<string>(), site.Bindings.Count);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod(EnableSession = true)]
		public bool SyncDomains(string currentDomain) {
            Utils.CheckSession();

			bool returnVal = true;
			try {
				var allDomains = new List<string>();
				using (var iisManager = new ServerManager()) {
					//Get to IIS website
					var site = (from siteCollection in iisManager.Sites
                                where siteCollection.Name.ToLower() == currentDomain.ToLower()
								select siteCollection).FirstOrDefault();

					if (site != null) {
						//get domains from DB
						using (var context = new AuroraEntities()) {

							var domains = from domain in context.Domain
										  where domain.ClientSiteID == SessionManager.ClientSiteID
										  select domain;

							allDomains.AddRange(site.Bindings.Select(t => t.Host));
							allDomains = allDomains.Union(domains.Select(t => t.DomainName)).ToList();

							foreach (string item in allDomains) {

								if (site.Bindings.Where(bind => bind.BindingInformation == ConfigurationManager.AppSettings["ServerIP"] + ":80:" + item.Trim()).Count() == 0) {
									site.Bindings.Add(ConfigurationManager.AppSettings["ServerIP"] + ":80:" + item.Trim(), "http");
									//commit to IIS
									iisManager.CommitChanges();
									site = (from siteCollection in iisManager.Sites
											where siteCollection.Name == currentDomain
											select siteCollection).FirstOrDefault();

									//check if site has stopped
									if (site.State == ObjectState.Stopped) {
										site.Bindings.Remove(site.Bindings.Where(bind => bind.Host == item).FirstOrDefault());
										iisManager.CommitChanges();
										site = (from siteCollection in iisManager.Sites
												where siteCollection.Name == currentDomain
												select siteCollection).FirstOrDefault();
										site.Start();
									}
								}

								if (domains.Where(dom => dom.DomainName == item.Trim()).FirstOrDefault() == null) {
									context.Domain.AddObject(new Domain {
										ClientSiteID = SessionManager.ClientSiteID.Value,
										DomainName = item,
										isSubDomain = false
									});
								}
							}

							//commit to DB
							context.SaveChanges();
						}
					}
				}


			} catch (Exception ex) {
				returnVal = false;
			}

			return returnVal;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> CheckDomains(string currentDomain, string newDomain) {
			var data = new Dictionary<string, object>();
			var resultVal = false;
			try {
				using (ServerManager iisManager = new ServerManager()) {


					var site = (from siteCollection in iisManager.Sites
								where siteCollection.Name == currentDomain
								select siteCollection).FirstOrDefault();

					var context = new AuroraEntities();

					var domains = from domain in context.Domain
								  where domain.DomainName == newDomain
								  select domain;

					resultVal = site.Bindings.Where(binding => binding.Host == newDomain).Count() > 0
								&& domains.Count() > 0;

					Utils.ServiceSuccessResultHandler(ref data, resultVal, 1);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> saveDomain(string domainName, bool isRoot) {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();

            bool returnVal = true;
            try {

                using (var context = new AuroraEntities()) {

                    //if true then remove isRoot on all domain and then add it to the one passed though
                    if (isRoot) {
                        bool Exists = false;
                        var domains = from domain in context.Domain
                                      where domain.ClientSiteID == SessionManager.ClientSiteID
                                      select domain;

                        foreach (Domain item in domains) {
                            if (item.DomainName.ToLower() == domainName.ToLower()) {
                                item.isRoot = true;
                                Exists = true;
                            } else {
                                item.isRoot = false;
                            }
                        }

                        //this is a security check to make sure that the domain Exists for the ClientSiteID
                        if (Exists) {
                            context.SaveChanges();
                        }

                        Utils.ServiceSuccessResultHandler(ref data, domains.Where(w => w.DomainName.ToLower() == domainName.ToLower()).ToList(), 0);
                    } else {
                        //if isRoot false then just set it to false on that domain.
                        var dom = (from domain in context.Domain
                                      where domain.ClientSiteID == SessionManager.ClientSiteID && domain.DomainName.ToLower() == domainName.ToLower()
                                      select domain).FirstOrDefault();
                        
                        dom.isRoot = false;
                        context.SaveChanges();

                        Utils.ServiceSuccessResultHandler(ref data, dom, 0);
                    }

                }

                
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);

			}

			return data;
		}

		#endregion

		[WebMethod]
		public Dictionary<string, object> GetCurrencies() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var currencies = service.Currency.Select(s=> s);

				Utils.ServiceSuccessResultHandler(ref data, currencies.ToList(),
												  currencies.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCurrencyConverters(int iPageNum, int iPageSize, string iFilter) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var currency = (from curr in service.CurrencyConversions
								join currencies in service.Currency on curr.CurrencyID equals currencies.CurrencyID
								where curr.DeletedOn == null && curr.ClientSiteID == SessionManager.ClientSiteID
								orderby curr.InsertedOn
								select new {
									Name = currencies.CurrencyPossessive + " " + currencies.CurrencyName + " (" + currencies.CurrencyCode + ")",
									CurrencyConversionID = curr.CurrencyConversionID,
									Value = curr.Value,
									IsDefault = curr.IsDefault
								}).DefaultIfEmpty();

				Utils.ServiceSuccessResultHandler(ref data, currency.ToList().Paginate(iPageNum, iPageSize),
												  currency.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCurrencyConverter(long currencyID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var currency = (from curr in service.CurrencyConversions
								where curr.DeletedOn == null && curr.ClientSiteID == SessionManager.ClientSiteID && curr.CurrencyConversionID == currencyID
								//orderby curr.Name
								select curr).FirstOrDefault();

				Utils.ServiceSuccessResultHandler(ref data, currency, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> UpdateCurrencyConverter(CurrencyConversions currency) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				currency.ClientSiteID = SessionManager.ClientSiteID.Value;
				var duplicateCurrency = service.CurrencyConversions.Where(w => w.CurrencyID == currency.CurrencyID
					&& w.DeletedOn == null
					&& w.ClientSiteID == SessionManager.ClientSiteID
					&& w.CurrencyConversionID != currency.CurrencyConversionID);
				if (duplicateCurrency.Count() == 0) {
					if (currency.CurrencyConversionID == 0) {
					service.AddToCurrencyConversions(currency);
				} else {
					service.AttachTo("CurrencyConversions", currency);
					currency.SetAllModified(service);
				}
					service.SaveChanges();

					// Only allow one default currency per client
					if (currency.IsDefault == true) {
						var otherCurrencies = service.CurrencyConversions.Where(w => w.ClientSiteID == SessionManager.ClientSiteID && w.CurrencyConversionID != currency.CurrencyConversionID);
						foreach (CurrencyConversions r in otherCurrencies) {
							r.IsDefault = false;
						}
					}

				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, "Success", 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, "Currency Already Exists", 0);
				}

			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> DeleteCurrencyConverter(long currencyID) {
			Utils.CheckSession();
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var currency = (from curr in service.CurrencyConversions
								where curr.ClientSiteID == SessionManager.ClientSiteID && curr.CurrencyConversionID == currencyID
								select curr).FirstOrDefault();

				if (currency != null) {
					currency.DeletedOn = DateTime.Now;
					currency.DeletedBy = SessionManager.UserID;
				}
				service.SaveChanges();
				Utils.ServiceSuccessResultHandler(ref data, "Success", 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}
	}


}
