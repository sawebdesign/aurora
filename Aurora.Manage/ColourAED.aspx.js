﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />

//#region Vars
var colourTemplate = null;
var sizeTemplate = null;
var globals = new GlobalVariables.GlobalVar();
var currentPage = 0;
var hasSaved = false;
var CurrentPage = 0;
var TotalCount = 0;
//#endregion

//#region Init
$(
    function () {
        $(".data").hide();
        DisplayMessage("Messages", "info", "Please select a product size");
        $("#Messages").appendTo($("#grid"));
        loadColorList();
    }
);
//#endregion

//#region Data Access Methods 

function loadSizes() {
    var onSuccess = function (result) {
        if (result.Result) {
            if (!sizeTemplate) {
                sizeTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("Sizes"));
            }

            sizeTemplate.set_data(result.Data);

            $("<option value='-1'>None</option>").insertBefore($("#Sizes")[0][0]);

            $("#Sizes").val(-1)
           
        }
        else {

        }
    };

    Aurora.Manage.Service.AuroraWS.GetSizes(currentPage, globals.PageSize, "", onSuccess, onError);
}
function loadColorList(sizeID) {
    $(".content").block({ message: null });
    var onSuccess = function (result) {
        $(".data").show();
        $("#Messages").hide();
        TotalCount = result.Count;
        if (result.Result) {
            if (colourTemplate == null) {
                colourTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
                initPagination();
            }
            TotalCount = result.Count;
            colourTemplate.set_data(result.Data);

            $("#template td").each(
                function () {
                    if ($(this).attr("color")) {
                        $(this).css("background", $(this).attr("color"));
                        $(this).css("borderLeft", "5px solid #eeeeee");
                    }
                    if ($(this).attr("image")) {
                        var time = new Date();
                        $(this).css("background", "url('" + $(this).attr("image") + "?" + time.getMilliseconds() + "')");
                        $(this).css("backgroundPosition", "left center");
                        $(this).css("borderLeft", "5px solid #eeeeee");
                    }
                }
            );
        }
        else {
            $(".data").hide();
            DisplayMessage("Messages", "info", "No Colours/textures availible for this size, use the green add button to start creating them.");
            $("#Messages").appendTo($("#grid"));
        }
        $(".content").unblock();
    };

   
        Aurora.Manage.Service.AuroraWS.GetColours(CurrentPage + 1,globals.PageSize,"",onSuccess, onError);
 
}

function getColourDetails(colourID) {
    scrollToElement("Messages", -20, 2);
    var onSuccess = function (responseText, status, xhr) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#Edit").unblock();
            $("#Edit").show();



        };
    }
    var x = new Date();
    $("#Edit").show().block({ message: null });
    $("#Messages").insertBefore("#Edit");
    $("#Messages").show();
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>");
    $("#Edit").load(String.format("Colour.aspx?ColourID={0}&var={1}",colourID, x.getMilliseconds()), onSuccess);
}


function saveColours() {
    var onSuccess = function (result) {
        if (result.Result) {
            hasSaved = true;
            loadColorList($("#Sizes").val());
            getColourDetails($("#ID").val());
            
        }
        else {
            DisplayMessage("Messages", "error", "There was an error while trying to save your colour, we will attend to this immediately and get back to you as soon as we have fixed it", 5000);
        }
    };

    var colourEntity = new AuroraEntities.Colour();
    colourEntity.ClientSiteID = parseInt($("#ClientSiteID").val());
    colourEntity.HexValue = $("#HexValue").val();
    colourEntity.CustomXML = $("#CustomXML").val();
    colourEntity.ImageUrl = !$("#Image1").attr("src") ? "" : $("#Image1").attr("src");
    colourEntity.ID = isNaN(parseInt($("#ID").val())) ? -1 : parseInt($("#ID").val());
    colourEntity.EntityKey = new Object();
    colourEntity.EntityKey.EntityContainerName = "AuroraEntities";
    colourEntity.EntityKey.EntitySetName = "Colour";
    colourEntity.EntityKey.IsTemporary = false;
    colourEntity.DeletedBy = null;
    colourEntity.DeletedOn = null;
    colourEntity.InsertedBy = 0;
    colourEntity.InsertedOn = new Date();
    colourEntity.Inactive = false;
    colourEntity.EntityKey.EntityKeyValues = new Array();
    colourEntity.Name = $("#Name").val();
    objKey = new Object();
    objKey.Key = "ID";
    objKey.Value = parseInt(colourEntity.ID) ? parseInt(colourEntity.ID) : -1;
    colourEntity.EntityKey.EntityKeyValues[0] = objKey;
    Aurora.Manage.Service.AuroraWS.UpdateColour(colourEntity, onSuccess, onError);

}

function reloadAfterImage(image) {
    if ($("#Image1").length == 0) {
        $("<img id='Image1' style='display:none;' src=''/>").appendTo("#Edit .content");
    }
    $("#Image1").attr("src", "");
    $("#Image1").attr("src", String.format("{0}/Uploads/colour_01_{1}.jpg", client, $("#ID").val()).replace("~",""));
    saveColours();
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadColorList();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadColorList();
    }
    $("#contentAddEdit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
//#endregion