﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomModulesData.aspx.cs"
    Inherits="Aurora.Manage.CustomModulesData" ClientIDMode="Predictable" %>

<%@ Import Namespace="Aurora.Custom" %>
<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="/Scripts/MicrosoftAjax.debug.js"></script>
    <script type="text/javascript" src="/Scripts/MicrosoftAjaxTemplates.debug.js"></script>
    <script type="text/javascript" src="/Service/AuroraWS.asmx/js"></script>
    <script type="text/javascript"src="CustomModulesData.aspx.js?<%=System.DateTime.Now %>"></script>
    <script type="text/javascript" src="/Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/Scripts/Utils.js"></script>
</head>
<body style="background: none!important;">
    <form id="form1" runat="server">
    <br class="clear" />
    <div class="onecolumn">
        <div class="header">
            <span>Edit</span>
        </div>
                <br class="clear" />
        <input type="hidden" runat="server" id="ID" />
        <input type="hidden" runat="server" id="InsertedOn" />
        <input type="hidden" runat="server" id="DeletedBy" />
        <input type="hidden" runat="server" id="DeletedOn" />
        <input type="hidden" runat="server" id="ModuleID" />
        <input type="hidden" runat="server" id="ClientSiteID" value="" />
        <div class="content">
                <cui:ObjectPanel ID="ContentPanel" Visible="true" runat="server">
                    <cui:XmlPanel ID="ContentXml" runat="server">
                    </cui:XmlPanel>
                </cui:ObjectPanel>
    
            <div>
                <a runat="server" target="_search" onclick="return false;" id="fileLink" visible="false"
                    href="">View Files</a>
            </div>
        </div>
    </div>
    <input type="button" id="btnSave" onclick="saveCustomDataXML();" value="Save" style="float: right;" />
    </form>
</body>
</html>
