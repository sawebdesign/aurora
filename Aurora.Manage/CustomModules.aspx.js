﻿//#region recipient emails control functions
var recipientEmails = {
    /// <summary>
    /// Creates the control from the supplied email addresses which are read from a hidden form element
    /// </summary>
    buildRecipientList: function () {
        if ($("#inpEmailRecipients").val() != "") {
            var emailArr = $("#inpEmailRecipients").val().split(",");

            $("#receipientList").html("");

            if (emailArr.length > 0) {
                $("#receipientList").append("<ul id='recipientUL'></ul>");
                
                for (var i = 0; i < emailArr.length; i++) {
                    //edit <img title='Edit Email' class='help' style='cursor: pointer;' alt='Edit Email' src='Styles/images/icon_edit.png'/>
                    $("#recipientUL").append("<li><div class='clearMe'><div style='float:left;'>" + emailArr[i] + "</div><div style='float:right;'><img title='Remove Email' class='help' style='cursor: pointer;' alt='Remove Email' onclick='recipientEmails.removeEmailAddress(" + i + ")' src='Styles/images/icon_delete.png'/></div></div></li>");
                }
            }

        } else {
            $("#receipientList").html("<p>Default site owner email address will be used.</p>");
        }
    },
    /// <summary>
    /// Adds the supplied email address to the list
    /// </summary>
    /// <param name="_emailAddress" type="String">The email address to be added</param>
    addEmailToList: function (_emailAddress) {
        
        var emailAddStr = _emailAddress;
        
        if ($("#inpEmailRecipients").val() != "") {
            var emailArr = $("#inpEmailRecipients").val().split(",");
        } else {
            var emailArr = [];
        }

        var testReg = emailAddStr.match(new RegExp("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", 'gi'));

        if (testReg == null)            
            return "Email address is not valid";

        //check if the email address already exists
        for (var i = 0; i < emailArr.length; i++) {
            if (emailArr[i].toLowerCase() == emailAddStr.toLowerCase()){
                return "Email address already in list.";
            }
        }

        //add the address
        emailArr.push(emailAddStr);
        recipientEmails.saveEmailArrayToForm(emailArr);
        recipientEmails.buildRecipientList();

        return "";

    },
    /// <summary>
    /// Removes the email address by using it's index in the array
    /// </summary>
    /// <param name="_index" type="Int">The array index position of the email</param>
    removeEmailAddress: function (_index) {
        var emailArr = $("#inpEmailRecipients").val().split(",");

        if (confirm("Click ok to delete the email " + emailArr[_index])) {

            emailArr.splice(_index, 1);
            recipientEmails.saveEmailArrayToForm(emailArr);
            recipientEmails.buildRecipientList();
        }
    },
    /// <summary>
    /// Takes a string array of emails and saves them to a hidden field as a list
    /// </summary>
    /// <param name="_emailArr" type="Array">A string array of email addresses</param>
    saveEmailArrayToForm: function (_emailArr) {
        if (_emailArr.length > 0) {
            $("#inpEmailRecipients").val(_emailArr.toString());
        } else {
            $("#inpEmailRecipients").val("");
        }
   }
};
//#endregion

//#region dom ready
$(
    function () {
        $("#Messages").insertBefore("#contentAddEdit");
        DisplayMessage("Messages", "info", "Please fill in the information below", 0);
        if (!$.browser.msie) {
            $("#Name")[0].innerHTML = (String.format("Editing {0} Module", $("#inpName").val()));
        }
        else {
            $("#Name").html((String.format("Editing {0} Form", $("#inpName").val())));
        }

        //tips
        $(".details").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'focus', delayIn: 0, delayOut: 0 });

        //build the control for any emails that are lready set up
        recipientEmails.buildRecipientList();
        
        //initialise dialog
        $("#dialog-form").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            buttons: {
                "Add Email": function () {
                    
                    var addEmailRet = recipientEmails.addEmailToList($("#email").val());

                    if (addEmailRet == "") {
                        $("#email").val("");
                        $(this).dialog("close");
                    } else {
                        $("#validMessage").html(addEmailRet);
                        $("#email").val("");
                    }
                   
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                
            },
            open: function () {
                //$("#dialog-form").dialog("option", "title", "Editing Email");
            }
        });

        $("#add-email")
            .click(function () {
                $("#dialog-form").dialog("open");
            });

    }
);
//#endregion