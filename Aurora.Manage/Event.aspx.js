﻿/// <reference path="/Scripts/Utils.js"/>
//#region Init
$(function () {

    $("#templateDetail .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#DisplayDate").datepicker({ dateFormat: 'dd/mm/yy' });

    $('#btn_image1').fancybox({
        padding: 0,
        titleShow: true,
        title: "Upload An Image for Caption 1",
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: '90%',
        height: '90%',
        onStart: saveData,
        onClosed: function () { reloadAfterImage(); },
        type: "iframe"
    });
    $('#btn_image2').fancybox({
        padding: 0,
        titleShow: true,
        title: "Upload An Image for Caption 2",
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: '90%',
        height: '90%',
        onStart: saveData,
        onClosed: function () { reloadAfterImage(); },
        type: "iframe"
    });
    var id = $("#ID").val();


    $('#btn_image1')[0].href = String.format("WebImage/WebImageUpload.aspx?ipath={0}/Uploads/&iName=event_01_{1}.jpg&smWidth={2}&lgWidth={3}&smPrefix=sm_", client.replace("~", ""), id, GetJSSession("Event_smWidth"), GetJSSession("Event_lgWidth"));
    //Image 1
    $("#Image1").hide();
    $('#btn_image1').mouseover(
        function () {
            $("#Image1").css("left", ($("#tab1_content").width() * 0.70) - 20);
            $("#Image1").css("border", "2px dashed grey");
            $("#Image1").css("position", "absolute");
            $("#Image1").css("top", "91px");
            $("#Image1").fadeIn(500);
            $("#Image1").height($("#Image1").height() / $("#Image1").width * ($("#tab1_content").width() - ($("#tab1_content").width() * 0.70)));
            $("#Image1").width($("#tab1_content").width() - ($("#tab1_content").width() * 0.70));

        }
    );

    $('#btn_image1').mouseout(
    function () {
        $("#Image1").fadeOut(500);
    }
    );

    //image 2
    $('#btn_image2')[0].href = String.format("WebImage/WebImageUpload.aspx?ipath={0}/Uploads/&iName=event_02_{1}.jpg&smWidth={2}&lgWidth={3}&smPrefix=sm_", client.replace("~", ""), id, GetJSSession("Event_smWidth"), GetJSSession("Event_lgWidth"));
    $("#Image2").hide();
    $('#btn_image2').mouseover(
        function () {
            $("#Image2").css("left", ($("#tab1_content").width() * 0.70) - 20);
            $("#Image2").css("border", "2px dashed grey");
            $("#Image2").css("position", "absolute");
            $("#Image2").css("top", "91px");
            $("#Image2").fadeIn(500);
            $("#Image2").height($("#Image2").height() / $("#Image2").width * ($("#tab1_content").width() - ($("#tab1_content").width() * 0.70)));
            $("#Image2").width($("#tab1_content").width() - ($("#tab1_content").width() * 0.70));

        }
    );

    $('#btn_image2').mouseout(
    function () {
        $("#Image2").fadeOut(500);
    }
    );

    DisplayMessage("Messages", "info", "Please fill in event information", 0);
    scrollToElement("Messages", -20, 2);



    //hides the page header image
    $("#Image1").hide();
    transferCustomFields("CustomData", "ContentPanel");

    //time picker
    $("#EventStartTime").timepicker({});
    $("#EventEndTime").timepicker({});

    //check if monthly occurance
    if ($("#Occurance").val() == 'Monthly') {
        //Load monthly filter from XML
        if ($("#CustomXML").val() != "") {
            $("#fldWeek").val(LoadDataFromXML($("#CustomXML").val(), "Week"));
            $("#fldDays").val(LoadDataFromXML($("#CustomXML").val(), "Days"));
        }
        else {
            $("#IgnoreFilter").val("1").attr("checked","checked");

        }

        $('#MonthlyFilter').show()
    }
    else {
        $('#MonthlyFilter').hide();
    }


    InitEditor("Details", "editorSpan",true);
});

function reloadAfterImage() {
    var anchor = {};
    anchor.dataid = $("#ID").val();

    getDetail(anchor);
}

function neverExpire(item) {
    if ($(item).is(":checked")) {
        $("#EndDate").val("01/01/2050");
        $("#EndDate").attr("disabled", "disabled");
    }
    else {
        $("#EndDate").val("");
        $("#EndDate").removeAttr("disabled");
    }
}
//#endregion