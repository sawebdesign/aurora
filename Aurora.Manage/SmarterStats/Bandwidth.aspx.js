﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx/" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />

var bandwidthDS = null;
$(function () {
    getSites();
});

function getSites() {
    $(".inner").block({ message: null });
    var response = function (result) {
        if (result.Data.length > 0) {
            var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("sites"));
            ds.set_data(result.Data);
            $("#sites option:first").attr("selected", "selected");
            $(".inner").unblock();
        }
    };

    Aurora.Manage.Service.AuroraWS.GetAllSites(response, onError);
}

function getBandwidth(siteID) {
    $(".data").block({ message: null });
    var response = function (result) {
        if (result.Data.length > 0) {
            if (!bandwidthDS) {
                bandwidthDS = $create(Sys.UI.DataView, {}, {}, {}, $get("statsGrid"));
            }

            bandwidthDS.set_data(result.Data);
            $(".data").unblock();
        }
    };
    var filter = $("#filter").val();

    Aurora.Manage.Service.AuroraWS.GetBandwidthPerSite(siteID,filter, response, onError);



}