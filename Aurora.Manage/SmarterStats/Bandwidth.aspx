﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="Bandwidth.aspx.cs" Inherits="Aurora.Manage.SmaterStats.Bandwidth" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script src="Bandwidth.aspx.js" type="text/javascript"></script>
    <div class="onecolumn">
        <div class="header">
            <span>Site Bandwidth</span>
            <select class="sys-template" id="sites" onchange="getBandwidth(this.value);" style="margin-left:10px;margin-top:3px;">
                <option value="{{SiteID}}">{{"www."+DomainName}}</option>
            </select>
        <div style="float:right">
        <select id="filter" onchange="getBandwidth($('#sites').val());">
            <option value="-1">Last 30 Days</option>
            <option value="-2">Last 60 Days</option>
            <option value="-3">Last 90 Days</option>
        </select> 
        </div>
        </div>
        <div class="content">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data">
                <thead>
                    <tr>
                        <th align="left">
                            Bandwidth (MB)
                        </th>
                        <th align="left">
                            Bandwidth (kB)
                        </th>
                        <th align="left">
                            Hits
                        </th>
                        <th align="left">
                            Views
                        </th>
                        <th align="left">
                            Visits
                        </th>
                    </tr>
                </thead>
                <tbody id="statsGrid" class="sys-template">
                    <tr>
                        <td>
                            {{((BandwidthBytes / 1024 ) / 1024).toFixed(2)}}
                        </td>
                        <td>
                            {{(BandwidthBytes / 1024).toFixed(2)}}
                        </td>
                        <td>
                            {{Hits}}
                        </td>
                        <td>
                            {{Views}}
                        </td>
                        <td>
                            {{Visits}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="Messages"></div>
    </div>
</asp:Content>
