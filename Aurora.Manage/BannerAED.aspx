﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="BannerAED.aspx.cs" Inherits="Aurora.Manage.BannerAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="BannerAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <div>
        <div class="onecolumn">
            <div class="header">
                <span>Banner List</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <img src="Styles/images/add.png" class="help" alt="Create Banner" mytitle="Create Banner"
                        style="cursor: pointer" onclick="getDetail();" />
                </div>
                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                    <div>
                        &nbsp;
                        <select id="BannerCat" class="sys-template" sys:attach="dataview" onchange="changeBannerCat();$('#contentAddEdit').hide();">
                            <option value="{binding ID}">{binding Name}</option>
                        </select>
                    </div>
                </div>
                <div style="float: left; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                    &nbsp;<select id="BannerType" onchange="loadBannerList(this.value);">
                        <option value="false">Active</option>
                        <option value="true">Archived</option>
                    </select>
                </div>
                <div style="float: right; margin-top: 10px;">
                    <a id="lnkFilter" href="javascript:void(0);" onclick="$('#Criteria').slideToggle();">
                        Filter</a>
                    <div id="Criteria" style="min-width: 200px; margin-top: 15px; margin-right: 30px;
                        display: none;">
                        <div id="Banner" style="min-width: 200px;">
                            <div id="BannerSizesContainer" style="float: right; display: none;">
                                <select id="BannerSizes" onchange=" getBannerNames();" class="help" mytitle="This list contains the types of banners that are available.">
                                </select>
                            </div>
                            <div style="float: right">
                                <b>Filter By:</b>
                                <select id="FilterBy" onchange="showFilter(this);">
                                    <option value="Page" selected="selected">Page</option>
                                    <option value="BannerType" onclick="getBannerNames();">BannerType</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    &nbsp;
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data" width="100%" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">
                                Campaign
                            </th>
                            <th align="left">
                                Statistics
                            </th>
                            <th align="left">
                                Order
                            </th>
                            <th align="left">
                                Views
                            </th>
                            <th align="left">
                                Clicks
                            </th>
                            <th align="left">
                                Text Clicks
                            </th>
                            <th align="left">
                                Type
                            </th>
                            <th align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody id="templateList" class="sys-template" sys:attach="dataview">
                        <tr>
                            <td width="30%">
                                {{Name}}
                            </td>
                            <td width="10%">
                                <a class="help Stats" mytitle="Display Banner Statistics" href="{{ 'BannerStatistics.aspx?BannerID='+BannerID}}">
                                    View</a>
                            </td>
                            <td width="10%">
                                {{Weight}}
                            </td>
                            <td width="10%">
                                {{ShowCount}}
                            </td>
                            <td width="10%">
                                {{ClickCount}}
                            </td>
                            <td width="10%">
                                {{UnderClickCount}}
                            </td>
                            <td width="10%">
                                {{FarmName}}
                            </td>
                            <td align="right" width="10%">
                                <a href="javascript:void(0);" onclick="getDetail(this);return false;" dataid="{{BannerID}}">
                                    <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" /></a>
                                <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{{BannerID}}">
                                    <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" /></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="html_body">
                </div>
                <!-- Begin pagination -->
                <div id="Pagination">
                </div>
                <!-- End pagination -->
            </div>
        </div>
        <div id="Messages">
        </div>
        <asp:Panel ID="pnlAddEdit" runat="server" ClientIDMode="Static">
            <div id="contentAddEdit">
            </div>
            <p align="right">
                <input type="button" id="btnSave" onclick="saveData(undefined);" value="Save" class="Login" />
            </p>
        </asp:Panel>
        <!-- End one column window -->
        <!-- End content -->
    </div>
    <div id="Stats">
    </div>
    </div>
</asp:Content>
