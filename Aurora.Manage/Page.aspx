﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="Aurora.Manage.Page" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>

<%@ Register src="/UserControls/MemberRoles.ascx" tagname="MemberRoles" tagprefix="uc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
<head id="Head1" runat="server">
    <title>Page Details</title>
    <script src="Scripts/custom_black.js" type="text/javascript"></script>
    <script type="text/javascript" src="Page.aspx.js?news"></script>

</head>
<body>
    <form id="form1" runat="server">
    <!-- Begin content -->
    <div id="ModulesMenu" style="visibility: hidden; position: absolute; z-index: 1000;
        background-color: #ffffff; border: 1px solid gray; padding: 3px; color: Gray;"
        onblur="showModulesMenu(this);">
    </div>
    <div class="onecolumn">
        <div class="header">
            <span>Page Design</span>
            <div style="width: 170px;" class="switch">
                <table width="100px" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td>
                                <input type="button" style="width: 90px;" value="Properties" class="left_switch"
                                    name="tab1" id="tab1" />
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Content Editor" class="middle_switch active"
                                    name="tab2" id="tab2" />
                            </td>
                          <td>
                                <input type="button"  style="width:90px;display:none;"  value="Security" class="right_switch" name="tab3"
                                    id="tab3"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <div class="tab_content" id="tab1_content">
                <div style="text-align: left;" id="PageDetails">
                    <asp:Image Visible="false" runat="server" ClientIDMode="Static" ID="PageImage" Style="position: absolute;" />
                    <table width="100%" cellpadding="2" cellspacing="4">
                        <tbody id="detailTemplate" sys:attach="dataview" style="display: table; position: relative;">
                            <input type="hidden" id="ID" name="ID" runat="server" />
                            <input type="hidden" id="ClientSiteID" name="ClientSiteID" runat="server" />
                            <input type="hidden" id="InsertedOn" name="InsertedOn" runat="server" />
                            <input type="hidden" id="InsertedBy" name="InsertedBy" runat="server" />
                            <input type="hidden" id="DeletedOn" name="DeletedOn" runat="server" />
                            <input type="hidden" id="DeletedBy" name="DeletedBy" runat="server" />
                            <input type="hidden" id="ParentId" name="ParentId" runat="server" />
                            <input type="hidden" id="CustomXML" name="CustomXML" runat="server" />
                            <input type="hidden" id="isArchived" name="isArchived" runat="server" />
                            <input type="hidden" id="ClientModuleID" name="ClientModuleID" runat="server" />
                            <tr>
                                <td colspan="2">
                                    <h2 style="margin-bottom: 0px;">
                                        Navigation</h2>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 20%">
                                    Menu Title:
                                </td>
                                <td>
                                    <input type="text" class="details" mytitle="This text will be displayed in the menu navigation to represent this page."
                                        id="ShortTitle" valtype="required" name="ShortTitle" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Page Heading:
                                </td>
                                <td>
                                    <input type="text" id="LongTitle" class="details" mytitle="This text will be displayed above the page's content to set the context for the reader"
                                        valtype="required; regex:[\w]" name="LongTitle" runat="server" />
                                </td>
                            </tr>

                            <tr>
                                <td valign="top">
                                    SEO Friendly URL:
                                </td>
                                <td>
                                    <input type="text" id="Description" onkeyup="this.value = this.value.toString().ReplaceSpecialChars().toLowerCase();" valtype="required;" name="Description" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h2 style="margin-bottom: 0px; margin-top: 10px;">
                                        SEO</h2>
                                </td>
                            </tr>
                             <tr>
                                <td valign="top">
                                    Meta Title:
                                </td>
                                <td>
                                    <input type="text" class="details" mytitle="This is the page title, it is used extensively by search engines and is usually displayed in the window/tab heading of your browser."
                                        id="PageTitle" valtype="required" name="PageTitle" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Meta Keywords:
                                </td>
                                <td>
                                    <input type="text" class="details" mytitle="These words must be seperated by a comma(',') and must describe the content within this page by the use of alternative words"
                                        id="MetaKeywords" valtype="required" name="MetaKeywords" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Meta Description:
                                </td>
                                <td>
                                    <input type="text" clientidmode="Static" id="MetaDescription" valtype="required"
                                        name="MetaDescription" runat="server" class="details" mytitle="This is a brief description of the content within this page" />
                                </td>
                            </tr>
							<tr>
                                <td colspan="2">
                                    <h2 style="margin-bottom: 0px;">
                                        Redirect</h2>
                                </td>
                            </tr>
							<tr>
                                <td valign="top">
                                   Redirect URL:
                                </td>
                                <td>
                                    <input type="text" id="RedirectURL" class="details" mytitle="This will redirect users to the URL placed in this box instead of this page."
                                        name="RedirectURL" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Permanent Redirect?
                                </td>
                                <td>
                                    <input type="checkbox" id="PermanentRedirect" name="PermanentRedirect" class="details" mytitle="By ticking this check box you will make the system do a 301 redirect." runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h2 style="margin-bottom: 0px; margin-top: 10px;">
                                        Layout</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Set As Home Page:
                                </td>
                                <td>
                                    <input type="checkbox" id="isDefault" name="isDefault" class="details" mytitle="By ticking this check box you will make this page the first page every visitor sees when they navigate to your site."
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hide Page From Menu:
                                </td>
                                <td>
                                    <input type="checkbox" runat="server" class="details" mytitle="By ticking this check box you will hide this page from your menu navigation, although it will still be accessable via its Page URL"
                                        id="showPage"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Order:
                                </td>
                                <td valign="top">
                                    <span style="padding-bottom: 5px;" name="parentindex" id="parentindex" runat="server"
                                        clientidmode="Static"></span>
                                    <input style="width: 108px;" type="text" id="PageIndex" valtype="required" name="PageIndex"
                                        runat="server" class="details" mytitle="This is a numeric only field that orders your pages like that of pages in a book, starting from 1 or 0" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Page Header Image Upload:
                                </td>
                                <td valign="middle">
                                    <a id="btn_image1" href="#" class="details" mytitle="Upload/Delete Image">
                                        <img src="Styles/images/icon_media.png"  /></a>&nbsp;<%--<a href="javascript:void(0);"
                                            id="delImage" onclick="loadDetail(this,1);"><img src="/Styles/images/icon_delete.png"
                                                style="cursor: pointer;" title="Delete image" /></a>
                                </td>--%>
                            </tr>
                            <tr style="display: none;">
                                <td>
                                    Image Width (in pixels):
                                </td>
                                <td>
                                    <input type="text" id="imgSize" value="" />
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" style="margin-bottom: -10px;">
                                    <h2>
                                        Page Security</h2>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <p>
                                        Page Security allows you to restrict public access to certain pages of the CMS.
                                        <br />
                                        Here you will enter a global user name & password that users will have to enter to access the page.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Enable Secure Login:
                                </td>
                                <td>
                                    <input type="checkbox" onclick="enableSecured(this);" id="IsSecured" name="isDefault" class="details" mytitle="By ticking this check box you will make this page unavailable to public users on your site, only users with a user name and a password will be granted access ."
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    User Name:
                                </td>
                                <td>
                                    <input type="text" class="details" mytitle="The name a user will use to login"
                                        id="UserName" disabled="disabled"  name="UserName" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Password:
                                </td>
                                <td>
                                    <input type="text" class="details" mytitle="The password a user will use to login"
                                        id="Password" disabled="disabled" name="Password" runat="server" />
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="2" style="margin-bottom: -10px;">
                                    <h2>
                                        Link a Feature to this Page</h2>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="2" valign="top">
                                    <p>
                                        This Feature has been deprecated. You will find its replacement within your content editor under the "Insert Modules" Tab, click the magic wand icon.
                                    </p>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td valign="top">
                                    Select Feature:
                                </td>
                                <td>
                                    <select class="sys-template details" sys:attach="dataview" id="FeaturesList" mytitle="Please select a feature that you wish to link and then click attach.To remove a link simply choose 'Nothing' from the drop down menu then click attach">
                                        <option value="{{ID}}">{{FriendlyName}}</option>
                                    </select>&nbsp;<a href="javascript:void(0);" onclick="CreateLink($('#FeaturesList'))">Attach</a>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="2">
                                    <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                        <cui:XmlPanel ID="ContentXml" runat="server">
                                        </cui:XmlPanel>
                                    </cui:ObjectPanel>
                                </td>
                            </tr>
                            
                        </tbody>
                        <tbody id="MenuClassBody" runat="server" Visible="false" style="display:table">
                            <tr><td colspan="2" >
                                <h2>Custom Menu Class</h2>
                                </td>
                            </tr>
                            <tr >
                                <td style="width: 20%">
                                Menu Class:    
                                </td>
                                <td>
                                <input type="text" class="details" mytitle="A class that will be applied to the menu of this page item"
                                        id="MenuClass" name="MenuClass" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab_content hide" id="tab2_content">
                <textarea id="DetailText" clientidmode="Static" runat="server" style="display:none;" name="DetailText" rows="4" cols="30"></textarea>
                <span id="editorSpan"></span>
            </div>
            <div class="tab_content hide" id="tab3_content">
                <uc1:MemberRoles ID="MemberRoles1" ModuleName="PageDetail" runat="server" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
