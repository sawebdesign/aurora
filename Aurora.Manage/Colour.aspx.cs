﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage {
    public partial class Colour : System.Web.UI.Page{
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utils.CheckSession();
            ClientSiteID.Value = SessionManager.ClientSiteID.ToString();

            if (!string.IsNullOrEmpty(Request["ColourID"]) && Request["ColourID"] != "-1")
            {
                int colourID;
                int.TryParse(Request["ColourID"],out colourID);
                var colourData = (from colours in entities.Colour
                                 where colours.ID == colourID
                                 select colours).Single();
                
                ID.Value = colourData.ID.ToString();
                CustomXML.Value = colourData.CustomXML;
                HexValue.Value = colourData.HexValue;
                ClientSiteID.Value = colourData.ClientSiteID.ToString();
                Name.Value = colourData.Name;

                if (File.Exists(Server.MapPath(colourData.ImageUrl)))
                {
                    Image1.ImageUrl = colourData.ImageUrl;
                }
                else
                {
                    Image1.Visible = false;
                }
                

            }
            else
            {
                Image1.Visible = false;
            }
        }
    }
}