﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="Currencies.aspx.cs" Inherits="Aurora.Manage.Currencies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script src="Currencies.aspx.js" type="text/javascript"></script>
	<script id="currencyConvertTmpl" type="text/x-jsrender">
        <div class="onecolumn">
			<div class="header"><span>Currency Conversion</span></div>
			<div class="clear"></div>
            <div class="content">
				<div class="message alert_warning">
					<p>
                        <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                        <span class="failureNotification">Please enter the value of the currency conversion in relation to your current primary currency.</span>
                    </p>
				</div>
				<div id="currencyConversionForm">
					<input type="hidden" value="{{attr:CurrencyConversionID}}" id="CurrencyConversionID"/>
					<table width="100%" >
						<tr>
							<td>Currency</td>
							<td><select id="CurrencyID" runat="server" clientidmode="Static"></select></td>
						</tr>
						<tr>
							<td>Value</td>
							<td><input type="text" id="Value" value="{{attr: Value}}" valtype="required"/></td>
						</tr>
						<tr>
							<td>Default Currency</td>
							<td><input type="checkbox" id="IsDefault" {{attr: IsDefault ? "checked=checked" : ""}}" /></td>
						</tr>
					</table>
				</div>
				<p style=" margin-right: -6px;" align="right">
                        <input type="button" id="btnSave" onclick="saveData();" value="Save" class="Login"
                            style="margin-right: 5px" />
                        <input type="button" id="Cancel" onclick="cancel();" value="Cancel" class="Login"
                            style="margin-right: 5px" />
                    </p>
            </div>
        </div>
	</script>
    <div class="onecolumn">
        <div class="header">
            <span>Currency Conversions</span>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img src="Styles/images/add.png" class="help" alt="Create Currency Conversion" title="Create Currency Conversion"
                    style="cursor: pointer" onclick="editCurrency(0);" />
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                    <th align="left">
                        Name
                    </th>
                    <th align="left">
                        Value
                    </th>
                    <th align="right">
                        Function
                    </th>
                </thead>
                <tbody class="sys-template" id="template">
                    <tr class="{{IsDefault ? 'boldrow' : ''}}">
                        <td align="left">
                            {{Name}}
                        </td>
                        <td align="left">
                            {{Value}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);" onclick="editCurrency($(this).attr('dataid')); return false;" dataid="{binding CurrencyConversionID}">
                                <img src="../styles/images/icon_edit.png" alt="edit" class="tipper" mytitle="Edit" /></a>
                            <a href="javascript:void(0);" onclick="deleteCurrency($(this).attr('dataid')); return false;"
                                dataid="{binding CurrencyConversionID}">
                                <img src="../styles/images/icon_delete.png" alt="delete" class="tipper" mytitle="Delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="contentAddEdit" > 
    
    </div>
    <div id="Messages"></div>

	<div style="display:none"></div>
</asp:Content>
