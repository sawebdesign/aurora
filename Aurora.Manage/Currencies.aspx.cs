﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Custom;

namespace Aurora.Manage {
	public partial class Currencies : System.Web.UI.Page {
		private AuroraEntities service = new AuroraEntities();
		protected void Page_Load(object sender, EventArgs e) {
			var currencies = service.Currency.Select(s => new {
				Name = s.CurrencyPossessive + " " + s.CurrencyName + " (" + s.CurrencyCode + ")",
				CurrencyID = s.CurrencyID,
				Important = s.Important
			}).OrderByDescending(o => o.Important).ThenBy(o => o.Name);
			CurrencyID.DataSource = currencies;
			CurrencyID.DataTextField = "Name";
			CurrencyID.DataValueField = "CurrencyID";
			CurrencyID.DataBind();
		}
	}
}