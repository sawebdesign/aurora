﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
	AutoEventWireup="true" CodeBehind="PageAED.aspx.cs" Inherits="Aurora.Manage.PageAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
	<script type="text/javascript" src="PageAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>

	<asp:Panel ID="pnlList" runat="server">
		<!-- Begin content -->
		<div id="contentList">
			<div class="inner">
				<!-- Begin one column window -->
				<div class="onecolumn">
					<div class="header">
						<span>Page List</span>
						<div style="display:inline-block; margin-top: 6px; margin-left: 6px;">
							<input placeholder="Page Search..." type="text" id="pageSearchtxt"/>
							<button type="button" id="pageSearchbtn" class="barButton" style="float:none">Search</button>
							<button type="button" id="pageSearchClearbtn" class="barButton" style="float:none">Clear</button>
						</div>
						<div style="float: right; margin-top: 5px; margin-right: 5px; cursor: pointer;">
							<a href="javascript:void(0);" onclick="parentPage=0;newPage=true;loadDetail(this);return false;"
								pageid="-1" parentid="0" pageindex="">
								<div class="barButton">
									<img src="Styles/images/add.png" alt="Create Page" title="Create Page" class="help" /></div>
							</a>
						</div>
						<div style="float: right; margin-top: 5px; margin-right: 5px; cursor: pointer;"><a id="pageReorderFancy" href="/pagereorder.aspx" title="Reorder Pages" class="help">
							<div class="barButton">
								<img src="Styles/images/arrow_switch.png" alt="Reorder Pages" />
								Reorder Pages</div>
						</a></div>
					</div>
					<br class="clear" />
					<div class="content">
						<table class="data" width="100%" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
                                    <th style="width: 30%" align="left">
                                        Menu Title
                                    </th>
                                    
                                    <th style="width: 30%" align="left">
                                        Page URL
                                    </th>
                                    <th style="width: 30%" align="left">
                                        Redirect Page URL
                                    </th>
									<th style="width: 3%" align="left">
                                        ID
                                    </th>
                                    <th style="width: 2%" align="left">
                                        Order
                                    </th>
                                    <th style="width: 8%" align="right">
                                        Function
                                    </th>
								</tr>
							</thead>
							<tbody id="listTemplate" class="sys-template" sys:attach="dataview">
								<tr id="{binding ID}" parentid="{binding ParentId}" level="0" index="{binding PageIndex}" childnodes="{binding HasChildren}" isdefault="{{isDefault}}">
									<td sys:title="{{ShortTitle}}">{{shortenString(ShortTitle,20)}}
									</td>
									<td>/page/{{Description != null ?Description.toString().replace(/ /gi,"-").toLowerCase() : ""}}
									</td>
									<td>{{RedirectURL == null ? "" : RedirectURL}}</td>
									<td>{{ID}}
									</td>
									<td>{binding PageIndex}
									</td>
									<td align="right" nowrap="nowrap">
										<a href="javascript:void(0);" onclick="showChild(this); return false;" node="show"
											pageid="{binding ID}" spacer="&nbsp;&nbsp;&nbsp;&nbsp;" class="showChildren">
											<img src="../styles/images/arrow_branch.png" alt="edit" class="help" title="Show Sub Pages" /></a>
										<a href="javascript:void(0);" ischild="true" thisid="{binding ID}" parentid="{binding ParentID}"
											onclick="newPage=true;parentPage=$(this).attr('thisid'); loadDetail(this);return false;">
											<img src="Styles/images/add.png" class="help" alt="add" onclick="" title="Add Sub Page"
												style="cursor: pointer;" /></a> <a href="javascript:void(0);" onclick="newPage=false;loadDetail(this);return false;"
													pageindex="{binding PageIndex}" pageid="{binding ID}">
													<img src="../styles/images/icon_edit.png" alt="edit" class="help" title="Edit" /></a>
										<a href="javascript:void(0);" onclick="deletePage(this);return false;" pageid="{binding ID}">
											<img src="../styles/images/icon_delete.png" alt="delete" class="help" title="Delete" /></a>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- Begin pagination -->
						<div id="Pagination">
						</div>
						<!-- End pagination -->
					</div>
				</div>
				<!-- End one column window -->

			</div>
			<!-- End content -->
		</div>
	</asp:Panel>
	<table width="100%">
		<tr>
			<td>
				<div id="Messages" style="margin-right: 20px; margin-bottom: 10px;">
				</div>
				<p style="margin-right: 15px;" align="right">
					<input type="button" id="btnSaveTop" onclick="saveData();" value="Save" class="Login"
						style="margin-right: 3px" />
				</p>
			</td>
		</tr>
	</table>
	<div id="contentAddEdit" style="margin-right: 22px;">
	</div>
	<p style="margin-right: 20px;" align="right">
		<input type="button" id="btnSave" onclick="saveData();" value="Save" class="Login"
			style="" />
	</p>

</asp:Content>
