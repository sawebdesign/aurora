﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Aurora.Custom;

namespace Aurora.Manage
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e) {
            // Get the exception object.
            Exception exc = Server.GetLastError();
            string userName = Utils.GetAppPoolUserName();
            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpUnhandledException) || exc.GetType() == typeof(HttpRuntime)) {
                // The Complete Error Handling Example generates
                // some errors using URLs with "NoCatch" in them;
                // ignore these here to simulate what would happen
                // if a global.asax handler were not implemented.
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength")) {
                    return;
                }
                Utils.SendExceptionMail(exc, "Global.asax Unhandled Exception");

                //Redirect HTTP errors to HttpError page
                Response.Redirect("~/Errors/_errors/503.html");
            }
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }
        
        void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {


                HttpApplication app = (HttpApplication)sender;
                string acceptEncoding = app.Request.Headers["Accept-Encoding"];
                acceptEncoding = acceptEncoding.ToLower();
                Stream prevUncompressedStream = app.Response.Filter;
                long size = prevUncompressedStream.CanRead ? Utils.ConvertDBNull(prevUncompressedStream.Length, 0) : 0;

                if (app.Request.ContentType != "application/json; charset=utf-8"
                    && app.Request.ContentType != "application/x-javascript"
                    && app.Request.ContentType != "text/css")
                {
                    return;
                }
                //if (app.Context.Request.FilePath.Contains(".jpg")
                //    || app.Context.Request.FilePath.Contains(".gif")
                //    || app.Context.Request.FilePath.Contains(".png")
                //    || app.Context.Request.FilePath.Contains(".js")
                //    ) {
                //    return;
                //}

              
                if ((acceptEncoding.Contains("gzip,") || acceptEncoding.Contains("gzip")))
                {
                    // gzip
                    app.Response.Filter = new GZipStream(prevUncompressedStream,
                    CompressionMode.Compress);
                    app.Response.AppendHeader("Content-Encoding", "gzip");

                }
                else
                    if (acceptEncoding.Contains("deflate"))
                    {
                        // deflate
                        app.Response.Filter = new DeflateStream(prevUncompressedStream, CompressionMode.Compress);
                        app.Response.AppendHeader("Content-Encoding", "deflate");


                    }

            }
            catch (Exception)
            {


            }
        }

    }
}
