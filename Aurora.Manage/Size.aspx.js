﻿//#region refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion

//#region Init
$(function () {
    $("#Edit").unblock();
    DisplayMessage("Messages", "info", "Please fill in the information below.", 0);
    $("#Messages").insertBefore("#Edit");
    if (!$(".data").is(":visible")) {
        $("#grid").toggle(500);
    }
});
//#endregion

//#region Data Manipulation

//#endregion