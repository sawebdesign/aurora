﻿/// <reference path="/scripts/jquery-1.4.1.js" />
/// <reference path="/Scripts/jquery.blockUI.js"></script>
/// <reference path="/Scripts/jquery.pagination.js"></script>
/// <reference path="/Scripts/Utils.js"></script>
/// <reference path="/Scripts/MicrosoftAjax.debug.js"></script>
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js"></script>
/// <reference path="/Scripts/generated/Aurora.Custom.js"></script>
/// <reference path="/Service/AuroraWS.asmx/js"></script>


var TotalCount = 0;
var PageSize = 10;
var CurrentPage = 0;
var listTemplate;
var detailTemplate;
var CurrentData = {};
var Global = new GlobalVariables.GlobalVar();

$(document).ready(function () {
    if (getQueryVariable('type') == "List" || getQueryVariable('type') == "Delete") {
        $("#contentAddEdit").hide();
        loadList();
    } else if (getQueryVariable('type') == "Add") {
        $("#contentList").hide();
    }
    $('div.content').block({ message: null });

});

function onPageError(result) {
    if (typeof result.status == "undefined") {
        onError(result);
    } else {
        DisplayMessage("Messages", "error", "status: " + result.status + " statusText: " + statusText)
    }
}

function getDate(date) {
    if (!date) {
        return "";
    } else {
        return formatDateTime(date, "dd/MM/yyyy hh:mm:ss");
    }
}

function loadList() {
    $('div.content').block({ message: null });
    if (listTemplate == null) {
        listTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("listTemplate"));
    }

    Aurora.Manage.Service.AuroraWS.GetUsersAndCount(CurrentPage + 1, PageSize, '', onloadDetailsSuccess, onPageError);

    function onloadDetailsSuccess(result) {
        if (result.Result == true) {
            if (result.Data[0] == null) {
                DisplayMessage("Messages", "warn", "No users available.")
                $("div.content").hide();
            } else {
                listTemplate.set_data(result.Data);
                if (TotalCount == 0) {
                    TotalCount = result.Count;
                    initPagination();
                }
                TotalCount = result.Count;
                CurrentData = result.Data;
                $("#listTemplate .mail").click(
                    function () {
                        this.setAttribute("href", "mailto:" + this.innerHTML);
                    }
                );
            }
            $('div.content').unblock();
        } else {
            DisplayMessage("Messages", "error", result.ErrorMessage)
        }
    }
}

function loadDetail(anchor) {
    var userid = -1;
    if (anchor.userid != undefined) {
        userid = anchor.userid;
    }

    $("#contentAddEdit").show();
    DisplayMessage("Messages", "info", "please fill in all the information below", false);
    scrollToElement("Messages", -20, 2);
    $("#btnSave").show();

    $('div.detail').block({ message: null });

    loadRoles(userid);
    loadSites(userid);

    if (detailTemplate == null) {
        detailTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("detailTemplate"));
    }

    if (userid == -1) {
        var user = new AuroraEntities.Users();
        for (var property in user) {
            user[property] = "";
        }
        user.ID = -1;
        detailTemplate.set_data(user);
    } else {
        detailTemplate.set_data(findByInArray(CurrentData, "ID", anchor.userid));
    }
    $('div.detail').unblock();
}

function loadRoles(userId) {
    Aurora.Manage.Service.AuroraWS.GetRoles(userId, onloadRolesSuccess, onPageError);

    function onloadRolesSuccess(result) {
        if (result.Result == true) {
            for (var i = 0; i < result.Data.length; i++) {
                var isSelected = "";
                if (result.Data[i].Selected == "true") {
                    isSelected = "selected";
                }
                $("#Roles").append("<option value='" + result.Data[i].ID + "' " + isSelected + ">" + result.Data[i].RoleName + "</option>");
            }
        } else {
            DisplayMessage("Messages", "error", result.ErrorMessage)
        }

        $('#Roles').toChecklist({

            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: false,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });

    }
}

function loadSites(UserId) {
    Aurora.Manage.Service.AuroraWS.GetSites(UserId, onloadSiteSuccess, onPageError);

    function onloadSiteSuccess(result) {
        if (result.Result == true) {
            for (var i = 0; i < result.Data.length; i++) {
                var isSelected = "";
                if (result.Data[i].Selected == "true") {
                    isSelected = "selected";
                }
                $("#Sites").append("<option value='" + result.Data[i].ClientSiteID + "' " + isSelected + ">" + result.Data[i].CompanyName + "</option>");
                $("#DefaultSite").append("<option value='" + result.Data[i].ClientSiteID + "' " + isSelected + ">" + result.Data[i].CompanyName + "</option>");
            }
        } else {
            DisplayMessage("Messages", "error", result.ErrorMessage)
        }

        $('#Sites').toChecklist({

            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: false,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });
    }
}

function saveData() {

    if (validateForm("UserDetails", true, "Messages", false)) {
        scrollToElement("Messages", -20, 2);
        return;
    }

    //get the form data and the user
    var formData = $('#form').serializeArray();

    var user;
    if ($("#UserID").val() == -1) {
        user = new AuroraEntities.Users();
        Global.populateEntityFromForm(user, formData);
    } else {
        user = findByInArray(CurrentData, "ID", $("#UserID").val());
    }

    //get all the roles for the user
    var userRoles = [];
    var arrayCount = 0;
    for (var i = 0; i < formData.length; i++) {
        if (formData[i].name == "Roles[]") {
            var roles = new AuroraEntities.UserRole();
            roles.UserID = user.ID;
            roles.RoleID = formData[i].value;
            userRoles[arrayCount] = roles;
            arrayCount++;
        }
    }

    //get all the sites for the user
    var userSites = [];
    arrayCount = 0;
    for (var i = 0; i < formData.length; i++) {
        if (formData[i].name == "Sites[]") {
            var sites = new AuroraEntities.UserClientSite();
            sites.UserID = user.ID;
            sites.ClientSiteID = formData[i].value;
            userSites[arrayCount] = sites;
            arrayCount++;
        }
    }
    user.__type = "Aurora.Custom.Data.Users";
    Aurora.Manage.Service.AuroraWS.SaveUser(user, userRoles, userSites, Number($("#DefaultSite").val()), onSaveDataSuccess, onPageError);

}

function onSaveDataSuccess(result) {
    if (result.Result == true) {
        DisplayMessage("Messages", "success", "User was successfully saved.", false);
        scrollToElement("Messages", -20, 2);
        loadList();
    } else {
        DisplayMessage("Messages", "error", result.ErrorMessage)
    }
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadList();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadList();
    }
    $("#contentAddEdit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
