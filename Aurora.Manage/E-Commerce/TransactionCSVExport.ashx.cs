﻿using Aurora.Custom;
using Aurora.Custom.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Reflection;


namespace Aurora.Manage.E_Commerce {
    /// <summary>
    /// Summary description for MembershipCSVExport
    /// </summary>
    public class TransactionCSVExport : IHttpHandler, IReadOnlySessionState {

        public void ProcessRequest(HttpContext context) {

            try {
                string iFilter = context.Request.QueryString["filter"];
                string type = context.Request.QueryString["type"];

                string[] filterCriteria = iFilter.Split(':');
                using (AuroraEntities service = new AuroraEntities()) {

                    var mySiteTransactions = from t in service.TransactionLog
                                             join p in service.PaymentSchema on t.PaymentSchemaID equals p.ID
                                             join m in service.Member on t.MemberID equals m.ID
                                             into leftJoin
                                             from tm in leftJoin.Where(mem => mem.ID == t.MemberID).DefaultIfEmpty()
                                             where t.PaymentType == type && t.ClientSiteID == SessionManager.ClientSiteID
                                             orderby t.InsertedOn descending
                                             select new { 
                                                 TransactionID = t.ID,
                                                 TotalAmount = t.TotalAmount,
                                                 isComplete = t.Completed,
                                                 InsertedOn =t.InsertedOn,
                                                 PaymentMethod = p.Name,
                                                 FirstName = tm.FirstName,
                                                 LastName = tm.LastName,
                                                 Email = tm.Email,
                                                 MemberDetail = tm.CustomXML,
                                             };

                    if (filterCriteria.Length > 0) {
                        switch (filterCriteria[0]) {
                            case "Date":
                                string[] range = filterCriteria[1].Split('|');
                                DateTime start = DateTime.ParseExact(range[0], "dd/MM/yyyy", null);
                                DateTime end = DateTime.ParseExact(range[1], "dd/MM/yyyy", null).AddDays(1);

                                DateTime starta = DateTime.Parse(start.ToString("MM/dd/yyyy"));
                                DateTime enda = DateTime.Parse(end.ToString("MM/dd/yyyy"));
                                mySiteTransactions = mySiteTransactions.Where(myt => myt.InsertedOn >= starta && myt.InsertedOn <= enda);
                                break;
                            case "Amount":
                                if (filterCriteria[1] == "A") {
                                    mySiteTransactions = mySiteTransactions.OrderBy(myt => myt.TotalAmount);
                                } else if (filterCriteria[1] == "D") {
                                    mySiteTransactions = mySiteTransactions.OrderByDescending(myt => myt.TotalAmount);
                                }
                                break;
                            case "MemberName":
                                string name = filterCriteria[1].ToUpper();
                                mySiteTransactions = mySiteTransactions.Where(myt => (myt.FirstName.ToUpper() + " " + myt.LastName.ToUpper()).Contains(name) || myt.Email.ToUpper().Contains(name));
                                break;
                            case "TID":
                                long id;
                                long.TryParse(filterCriteria[1], out id);
                                mySiteTransactions = mySiteTransactions.Where(myt => myt.TransactionID == id);
                                break;
                        }
                    }


                    string csv = "";
                    //get property names from the first object using reflection    
                    IEnumerable<PropertyInfo> props = mySiteTransactions.First().GetType().GetProperties();

                    //header 
                    csv += String.Join(", ", props.Select(prop => prop.Name)) + "\r\n";

                    //rows
                    foreach (var entityObject in mySiteTransactions) {
                        csv += String.Join(", ", props.Select(
                            prop => (prop.GetValue(entityObject, null) ?? "").ToString()
                        ))
                        + "\r\n";
                    }

                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + DateTime.Now.ToShortDateString() + "-" + type + ".csv");
                    context.Response.ContentType = "text/csv";
                    context.Response.Write(csv);

                }

            } catch (Exception) {
                context.Response.Redirect("~/errors/_errors/404.html");
            }
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}