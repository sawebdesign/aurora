﻿using Aurora.Custom.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Aurora.Manage {
    public partial class TransactionReport : System.Web.UI.Page {
		Custom.Data.AuroraEntities service = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e) {
			int TransactionID = Convert.ToInt32(Request.QueryString["TransactionID"]);
			TransactionLog trans = service.TransactionLog.Where(w => w.ID == TransactionID).FirstOrDefault();
			

			if (trans.MemberID != null) {
				Member member = service.Member.Where(w => w.ID == trans.MemberID).FirstOrDefault();
				XDocument doccie = XDocument.Parse(trans.DataXML);
				ClientName.InnerText = member.FirstName + " " + member.LastName;
				Email.InnerText = member.Email;

				AddressOne.InnerText = (doccie.Root.Attribute("AddressLine1") != null) ? doccie.Root.Attribute("AddressLine1").Value : "";
				AddressTwo.InnerText = (doccie.Root.Attribute("AddressLine2") != null) ? doccie.Root.Attribute("AddressLine2").Value : "";
				AddressThree.InnerText = (doccie.Root.Attribute("AddressLine3") != null) ? doccie.Root.Attribute("AddressLine3").Value : "";
				City.InnerText = (doccie.Root.Attribute("City") != null) ? doccie.Root.Attribute("City").Value : "";
				PostCode.InnerText = (doccie.Root.Attribute("PostCode") != null) ? doccie.Root.Attribute("PostCode").Value : "";

			} else {
				memberDetails.Visible = false;
			}
        }
    }
}