﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="SiteTransactionHistory.aspx.cs" Inherits="Aurora.Manage.SiteTransactionHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="SiteTransactionHistory.aspx.js"></script>
    <div class="onecolumn">
    
        <div class="header" style="position: relative">
            <span>Site Transaction List</span>
            <div style="float: right; margin-top: 5px;">
                <b>Select Type:</b>
                <select id="TransActionType" onchange="filter=''; getTransactions($(this).val());"
                    class="sys-template">
                    <option sys:value="{{Names}}">{{Names}}</option>
                </select>
                &nbsp; <a href="javascript:void(0);" onclick="$('#Criteria').slideToggle();">Filter</a>
                
                <div id="Criteria" style="min-width: 200px; margin-top: 5px; display: none;">
                    <div style="float: left">
                        &nbsp; <b>Filter By:</b>
                        <select id="FilterBy" onchange="showFilter(this)">
                            <option value="Amount" selected="selected">Amount</option>
                            <option value="Date">Date</option>
                            <option value="MemberName">Member Name/Email</option>
                            <option value="TID">Transaction Ref#</option>
                        </select>
                    </div>
                    <div style="float: right; line-height: 19px; padding: 5px">
                        <a href="javascript:void(0);" id="btnSearch" onclick="searchWithFilter()">Search</a>
                    </div>
                    <div style="float: right">
                        <select id="CAmount">
                            <option value="A">Ascending</option>
                            <option value="D">Descending</option>
                        </select>
                        <div class="dateSearch" style="display: none">
                            Start:<input type="text" id="fromDate" value="" style="width: 80px" />
                            End:<input type="text" id="toDate" value="" style="width: 80px" /></div>
                        <input type="text" id="nameSearch" value="" style="display: none; width: 100px" />
                        <input type="text" id="tSearch" value="" style="display: none; width: 100px" />
                    </div>
                    
                </div>
                
                &nbsp;
                <div style="float: right">
                        <img class="help tipsy-east exportMemberButton" style="cursor: pointer; margin-right:35px; padding-top:5px" onclick="exportData();" src="/Styles/images/report_go.png" mytitle="Export Data" alt="Export Data"/>
                </div>
            </div>
        </div>
        <div class="content">
            <table class="data" border="0" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="left">
                            Completed
                        </th>
                        <th align="left">
                            Ref#
                        </th>
                        <th align="left">
                            Transaction Date
                        </th>
                        <th align="left">
                            Payment Method
                        </th>
                        <th align="left">
                            Client
                        </th>
                        <th align="left">
                            Amount
                        </th>
                        <th align="left">
                            Reciept
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" id="TransactionList">
                    <tr>
                        <td>
                            <input type="checkbox" dataid="{{transactions.ID}}" completed="{{transactions.Completed}}"
                                onclick="updateTransaction($(this).attr('dataid'));" />
                        </td>
                        <td>
                            {{transactions.ID}}
                        </td>
                        <td>
                            {{new Date(transactions.InsertedOn).format("dd-MMM-yyyy HH:mm")}}
                        </td>
                        <td>
                            {{paymentSchema.Name}}
                        </td>
                        <td>
                            {{member ? member.FirstName +" "+member.LastName : "N/A"}}
                        </td>
                        <td>
                            {{transactions.TotalAmount}}
                        </td>
                        <td>
                            <a class="viewReport" href="{{ 'TransactionReport.aspx?TransactionID='+transactions.ID+'&type='+transactions.PaymentType}}"
                                id="{{transactions.ID}}" hasxml="{{transactions.DataXML ? 'true' : 'false'}}">View
                                Report</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="Pagination">
            </div>
            <div id="Messages">
            </div>
        </div>
    </div>
</asp:Content>
