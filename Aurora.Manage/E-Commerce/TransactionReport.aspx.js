﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx" />

var ready = 0;
var TotalCount = 0;
var CurrentPage = 0;
var Global = new GlobalVariables.GlobalVar();
var bannerID;
var dataStats = null;
$(function () {
    $(".content").block({ message: null });
    $("body").css("background", "");
    transid = getQueryVariable("TransactionID");
    if (bannerID != "" || bannerID != null) {
        //Get Banner
      
        getTransactionProducts(transid);
    }

});

function getTransactionProducts(id) {
    /// <summary>Gets a  specified banner</summary>
    var success = function (result) {
      
        if (result.Data !== undefined) {
            var dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            dataSet.set_data(result.Data);
            $("#name span").html(String.format("Transaction: {0}  Client: {1}", result.TransactionDate, result.ClientName))

            var line = "";
            if (result.VoucherCode) {
                line += String.format('<tr><td colspan="7">Voucher Code: {0}</td><td>Voucher Amount:</td><td>-{1}</td></tr>', result.VoucherCode, Number(result.VoucherAmount).toFixed(2));
            }
            if (result.VATAmount !== 0) {
                line += String.format('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>VAT:</td><td>{0}</td></tr>', Number(result.VATAmount).toFixed(2));
            }
            if (result.Shipping) {
                line += String.format('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><b>Shipping Cost:</b></td><td><b>{0}</b></td></tr>', Number(result.Shipping).toFixed(2));
            }
            if (result.Total) {
                line += String.format('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><b>Total:</b></td><td><b>{0}</b></td></tr>', Number(result.Total).toFixed(2));
            }


            if (line !== "") {
                $(line).appendTo('#templateList'); 
            }
            unblock();
        }
        else {
            //show donation data
            getTransactionsForType(id);
            // DisplayMessage("Messages", "warn", "There are no transactions to show.");
        }
    }

    Aurora.Manage.Service.AuroraWS.GetTransactionProducts(Number(id), success, onError);
}

function unblock() {
    $(".content").unblock()
}

function getTransactionsForType(id) {
    var response = function (result) {
        $(".data").hide();
        $("#name").html("<h2>Information</h2>");
        for (var item = 0; item < result.Data.length; item++) {
            $(String.format("<div><b>{0}</b>: {1}</div>", result.Data[item].name, result.Data[item].value)).appendTo(".content");
        }
        $(".content").unblock();
    };

    Aurora.Manage.Service.AuroraWS.GetTransactionsForType(id, $("#paymentType").val(),response,onError);
}