﻿/// <reference path="/scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/scripts/utils.js" />
/// <reference path="/service/auroraws.asmx/jsdebug" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />

var dsTransactions = null;
var CurrentPage = 0;
var TotalCount = 0;
var globalVars = new GlobalVariables.GlobalVar();
var filter = "";
$(function () {
    $(".tipsy").remove();
    $(".help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });

    getTransactionsTypes();
    $("#FilterBy").val("Amount");
    var dates = $("#fromDate, #toDate").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy",
        onSelect: function (selectedDate) {
            var option = this.id == "fromDate" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);

        }
    });
});

//#region Page State
function showFilter(dd) {
    var filterSelection = dd.value

    switch (filterSelection) {
        case "Amount":
            $("#CAmount").show();
            $(".dateSearch,#nameSearch,#tSearch").hide();
            break;
        case "Date":
            $(".dateSearch").show();
            $("#CAmount,#nameSearch,#tSearch").hide();
            break;
        case "MemberName":
            $("#nameSearch").show();
            $("#CAmount,.dateSearch,#tSearch").hide();
            break;
        case "TID":
            $("#tSearch").show();
            $("#CAmount,.dateSearch,#nameSearch").hide();
            break;
        default:
    }
}
//#endregion

//#region
function searchWithFilter() {
    filter = $("#FilterBy").val();
    filter += ":";
    switch (filter) {
        case "Amount:":
            filter += $("#CAmount").val();
            break;
        case "Date:":
            if (!($("#fromDate").val() && $("#toDate").val())) {
                return false;
            }
            filter += $("#fromDate").val() + '|' + $("#toDate").val();
            break;
        case "MemberName:":
            filter += $("#nameSearch").val();
            break;
        case "TID:":
            filter += $("#tSearch").val();
            break;
    }
    getTransactions($("#TransActionType").val());
    return false;
}
//#endregion

//#region Get Methods

function getTransactions(type) {
    $("#Messages").hide();
    $(".data").block({ message: null });
    var response = function (result) {
        $("#Criteria").slideUp();
        $(".data").unblock();
        if (TotalCount != result.Count) {
            TotalCount = result.Count;
            initPagination();
        }

        if (result.Data.length > 0) {
            $(".data").show();
            $(".pagination").show();
            if (!dsTransactions) {
                dsTransactions = $create(Sys.UI.DataView, {}, {}, {}, $get("TransactionList"));
                TotalCount = result.Count;
            }
            dsTransactions.set_data(result.Data);
            $("#TransactionList a[hasXML='false']").replaceWith("N/A");
            //Loads stat box
            $('.viewReport').fancybox({
                padding: 0,
                titleShow: true,
                title: '',
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                width: 850,
                height: 600,
                type: "iframe"
            });

            //chk boxes
            $("#TransactionList input:checkbox[completed='true']").attr("disabled", "disabled").attr("checked", "checked");

        } else {
            $(".data").hide();
            $(".pagination").hide();
            DisplayMessage("Messages", "warn", "There are no transactions to show.");
        }
    };

    Aurora.Manage.Service.AuroraWS.GetSiteTransactions(type, CurrentPage + 1, 10, filter, response, onError);
}

function exportData() {

    filter = $("#FilterBy").val();
    filter += ":";
    switch (filter) {
        case "Amount:":
            filter += $("#CAmount").val();
            break;
        case "Date:":
            if (!($("#fromDate").val() && $("#toDate").val())) {
                return false;
            }
            filter += $("#fromDate").val() + '|' + $("#toDate").val();
            break;
        case "MemberName:":
            filter += $("#nameSearch").val();
            break;
        case "TID:":
            filter += $("#tSearch").val();
            break;
    }

    window.open('/E-Commerce/TransactionCSVExport.ashx?type=' + $("#TransActionType").val() + '&filter=' + filter, '_blank');

}

function getTransactionsTypes() {

    var response = function (result) {
        var dsTypes = null;
        dsTypes = $create(Sys.UI.DataView, {}, {}, {}, $get("TransActionType"));
        if (result.Data !== null || result.Data !== undefined) {
            dsTypes.set_data(result.Data);
        }
        if (result.Data.length > 0) {

            getTransactions(result.Data[0].Names);
            $("#TransActionType option:first").val(result.Data[0].Names);
            $("#TransActionType option:first").attr("selected", "selected");
        }
        else {
            DisplayMessage("Messages", "warn", "There are no transactions to display");
        }

    };

    Aurora.Manage.Service.AuroraWS.GetTransactionTypesPerClient(response, onError);
}

function pageselectCallback(page_index, jq) {

    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        getTransactions($("#TransActionType option:selected").val());
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        getTransactions($("#TransActionType option:selected").val());
    }
    $("#contentAddEdit").hide();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globalVars.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

function updateTransaction(id) {
    if (confirm("Do you wish to change the state of this transaction?")) {
        var response = function (result) {
            if (result.Result) {
                getTransactions($("#TransActionType").val());
            }
        };

        Aurora.Manage.Service.AuroraWS.SetTransActionCompleted(id, response, onError);
    }
    else {
        $("input:checkbox[dataid='" + id + "']").removeAttr("checked");
    }
}
//#endregion