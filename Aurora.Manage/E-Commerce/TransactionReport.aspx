﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionReport.aspx.cs" Inherits="Aurora.Manage.TransactionReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Transaction Report</title>
    <script src="/Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.pagination.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script src="/Service/AuroraWS.asmx/js" type="text/javascript"></script>
    <script src="/Scripts/Utils.js" type="text/javascript"></script>
    <script type="text/javascript" src="TransactionReport.aspx.js"></script>
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" />
    <style>
    body
    {
        margin:0px 10px 10px 0px;
        padding-left:10px;
        background-image:none;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="Messages">
    </div>
        <div id="contentList">
            <input type="hidden" id="paymentType" value="<%=Request["type"]%>" />
            <!-- Begin one column window -->
            <div class="onecolumn">
                <div class="header" id="name">
                    <span></span>
                </div>
                <br class="clear" />
                <div class="content">
					<table id="memberDetails" runat="server" style="margin:0;">
						<tr>
							<td style="font-weight: bold;">Client Name</td>
							<td><span id="ClientName" runat="server"></span></td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Email</td>
							<td><span id="Email" runat="server"></span></td>
						</tr>
						<tr>
							<td style="font-weight: bold; vertical-align:top">Address</td>
							<td>
								<div id="AddressOne" runat="server"></div>
								<div id="AddressTwo" runat="server"></div>
								<div id="AddressThree" runat="server"></div>
								<div id="Country" runat="server"></div>
								<div id="City" runat="server"></div>
								<div id="PostCode" runat="server"></div>
							</td>
						</tr>
					</table>
                    <table class="data" width="100%" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th align="left">
                                    Name
                                </th>
                                <th align="left">
                                    Current Price
                                </th>
                                <th align="left">
                                    Purchase Price
                                </th>
                                 <th align="left">
                                    Quantity
                                </th>
                                <th align="left">
                                    Colour
                                </th>
                                 <th align="left">
                                    Size
                                </th>
                                <th align="left">
                                    
                                </th>
                                <th align="left">
                                   
                                </th>
                                <th align="left">
                                   Total
                                </th>
                            </tr>
                        </thead>
                        <tbody id="templateList" class="sys-template" sys:attach="dataview">
                            <tr>
                                <td width="10%">
                                    {{Name}}
                                </td>
                                <td width="10%">
                                    {{Price}}
                                </td>
                                <td width="10%">
                                    {{BasketPrice}}
                                </td>
                                <td width="10%">
                                    {{Quantity}}
                                </td>
                                <td width="10%">
                                    {{Colour}}
                                </td>
                                <td width="10%">
                                    {{Size}}
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    {{BasketPrice*Quantity}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                
            </div>
            <!-- End one column window -->
            <!-- End content -->
        </div>
    </div>
    </form>
</body>
</html>
