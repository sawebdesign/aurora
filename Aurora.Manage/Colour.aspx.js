﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
/// <reference path="Scripts/colorpicker.js" />

var messageTimer = null;
$(
    function () {

        scrollToElement("Messages", -20, 2);
        if (hasSaved) {
            DisplayMessage("Messages", "success", "Colour has been saved", 5000);
            messageTimer = setTimeout(function () {
                DisplayMessage("Messages", "info", "Please fill in the form below.", 5000);
                clearTimeout(messageTimer);
            }, 5500);
            hasSaved = false;
        }
        else {
            DisplayMessage("Messages", "info", "Please fill in the form below.", 5000);
        }

        $("#colour").css("backgroundColor", $("#HexValue").val());
        $('#colour').ColorPicker({
            color: "#" + $("#HexValue").val(),
            eventName: "click",
            onShow: function (colpkr) {
                $(colpkr).slideDown(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).slideUp(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $("#colour").css("backgroundColor", "#" + hex);
                $("#HexValue").val(hex);
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $("#HexValue").val(hex);
                $(el).ColorPickerHide();
            }
        });
        
        var currStyle = $("#colour").attr('style');
        currStyle = currStyle + ";BACKGROUND-COLOR : #" + $("#HexValue").val();
        $("#colour").attr('style', currStyle);

        $('#btn_image1').fancybox({
            padding: 0,
            titleShow: true,
            title: "Upload An Image for Caption 1",
            overlayColor: '#000000',
            overlayOpacity: .5,
            showActivity: true,
            width: '90%',
            height: '90%',
            onStart: saveColours,
            onClosed: function () { reloadAfterImage(); },
            type: "iframe"
        });

        $('#btn_image1')[0].href = String.format("WebImage/WebImageUpload.aspx?ipath={0}/Uploads/&iName=colour_01_{1}.jpg&smWidth={2}&lgWidth={3}&smPrefix=sm_", client.replace("~", ""), $("#ID").val(), GetJSSession("Event_smWidth"), GetJSSession("Event_lgWidth"));
        //Image 1
        $("#Image1").hide();
        $("#Edit .content").height(250);

        $('#btn_image1').mouseover(
        function () {
            $("#Image1").css("left", ($("#Edit").width() * 0.70) - 20);
            $("#Image1").css("border", "2px dashed grey");
            $("#Image1").css("position", "absolute");
            $("#Image1").css("top", "61px");
            $("#Image1").fadeIn(500);
            $("#Image1").height($("#Image1").height() / $("#Image1").width * ($("#Edit").width() - ($("#Edit").width() * 0.70)));
            $("#Image1").width($("#Edit").width() - ($("#Edit").width() * 0.70));

        }
        );

        $('#btn_image1').mouseout(
                function () {
                    $("#Image1").fadeOut(500);
                }
        );

    }
);