﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx" />

var ready = 0;
var TotalCount = 0;
var CurrentPage = 0;
var Global = new GlobalVariables.GlobalVar();
var dataPages = null;

$(function () {
    $(".content").block({ message: null });
    $("body").css("background", "");
    getPages();
    $('.alert_warning, .alert_info').click(function () {
        $(this).fadeOut();
    });
});

function getPages() {
    /// <summary>Gets a  specified banner</summary>
    $(".content").html("");
    var success = function (result) {
        
        if (result.Data.length > 0) {
            var pages = $('<ol class="sortable"></ol>');
            for (var i = 0; i < result.Data.length; i++) {
                var item = $('<li id="' + result.Data[i].ID + '"></li>')
                item.append(String.format('<div class="{2}">{0} : {1}</div>',
                                            result.Data[i].ShortTitle,
                                            result.Data[i].PageIndex,
                                            result.Data[i].PageIndex < 0 ? "hidden" : ""));
                if (result.Children[0] !== null) {
                    var childs = getChildren(result.Children, result.Data[i].ID);
                    item.append(childs);
                }
                pages.append(item);
            }
            $(".content").append(pages);
            pages.nestedSortable({
                disableNesting: 'no-nest',
                forcePlaceholderSize: true,
                handle: 'div',
                helper: 'clone',
                items: 'li',
                opacity: .6,
                placeholder: 'placeholder',
                revert: 250,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> div'
            });
        }
        unblock();
    }

    Aurora.Manage.Service.AuroraWS.GetPagesForReorder(success, onError);
}

function getChildren(childData, parentID) {
    var children = ""
    var childrenList = findSetByInArray(childData, "ParentId", parentID);
    if (childrenList.length > 0) {
        children = "<ol>"
        for (var i = 0; i < childrenList.length; i++) {
            children += String.format(String.format('<li id="{3}"><div class="{2}">{0} : {1}</div>',
                                            childrenList[i].ShortTitle,
                                            childrenList[i].PageIndex,
                                            childrenList[i].PageIndex < 0 ? "hidden" : "",
                                            childrenList[i].ID));
            children += getChildren(childData, childrenList[i].ID);
            children += '</li>';
        }
        children += "</ol>";
    }
    return children;
}

function unblock() {
        $(".content").unblock()
    }

function reorderPages() {
    var heir = $(".sortable").nestedSortable('toHierarchy');
    var success = function (result) {
        parent.$.fancybox.close();
    }
    Aurora.Manage.Service.AuroraWS.ReorderPages(heir, success, onError);
}
