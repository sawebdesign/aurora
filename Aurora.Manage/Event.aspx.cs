﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Custom.Data;


namespace Aurora.Manage
{
    public partial class Event : System.Web.UI.Page
    {
        AuroraEntities service = new AuroraEntities();
       Custom.Data.Event events = new Custom.Data.Event();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utils.CheckSession();
            Page.ViewStateMode = ViewStateMode.Disabled;

            int EventID = Custom.Utils.ConvertDBNull<int>(Request.QueryString["EventId"], 0);
            int SchemaId = 0;
            int ModuleId = 0;
            //get the module that we are loading

            events = (from evnt in service.Event
                                 where evnt.ID == EventID
                                 select evnt).SingleOrDefault();

            if (events != null)
            {
                ModuleId = Custom.Utils.ConvertDBNull<int>(events.ID, 0);
            }

            if (ModuleId > 0)
            {
               
                XmlDocument XmlData = new XmlDocument(); //The xml news data
                string schema = string.Empty; //the schema for the extension
                XmlDocument SchemaXml = new XmlDocument(); //the schema for the extension

                //get the module that we are loading
                Utils.CheckSqlDBConnection();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[Clients].[proc_Module_GetByClientSiteIDName]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    cmd.Parameters.AddWithValue("@Name", "News");
                    DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        SchemaId = Custom.Utils.ConvertDBNull<int>(ds.Tables[0].Rows[0]["SchemaId"], 0);
                        schema = Custom.Utils.ConvertDBNull<string>(ds.Tables[0].Rows[0]["SchemaXML"], "");
                    }
                   

                }

                //TODO create new Schema Record
                ContentPanel.Visible = false;
                if (EventID > 0)
                {
                    //XmlData is Bound - must comply to XmlData/Fields/field;

                    if (!string.IsNullOrEmpty(events.CustomXML))
                    {
                        XmlData.LoadXml(events.CustomXML.ToString());
                    }

                    //load the events into the form
                    ID.Value = events.ID.ToString();
                    ClientSiteID.Value = Custom.SessionManager.ClientSiteID.ToString();
                    CategoryID.Value = Custom.Utils.ConvertDBNull<string>(events.CategoryID, "");
                    //SchemaID.Value = Custom.Utils.ConvertDBNull<string>(events.SchemaID, "");
                    Title.Value = events.Title;
                    Occurance.Value = events.Occurance;
                    CustomXML.Value = events.CustomXML;
                    StartDate.Value = Custom.Utils.ConvertDBNull<DateTime>(events.StartDate, DateTime.Now).ToString("dd/MM/yyyy");
                    if (Custom.Utils.ConvertDBNull<DateTime>(events.EndDate, DateTime.Now).Year == 2050)
                    {
                        EndDate.Value = "01/01/2050";
                        NeverExpires.Checked = true;
                        EndDate.Disabled = true;
                    }
                    else
                    {
                       EndDate.Value = Custom.Utils.ConvertDBNull<DateTime>(events.EndDate, DateTime.Now).ToString("dd/MM/yyyy");
                    }
                    Details.Value = events.Details;
                    GeoLatLong.Value = events.GeoLatLong;
                    Location.Value = events.Location;
                    AllowReminders.Checked = events.AllowReminders;
                    ImageCaption1.Value = events.ImageCaption1;
                    ImageCaption2.Value = events.ImageCaption2;
                    DisplayDate.Value = Custom.Utils.ConvertDBNull<DateTime>(events.DisplayDate,DateTime.Now).ToString("dd/MM/yyyy");
                    InsertedOn.Value = Custom.Utils.ConvertDBNull<DateTime>(events.InsertedOn, DateTime.Now).ToShortDateString();
                    InsertedBy.Value = Custom.Utils.ConvertDBNull<string>(events.InsertedBy, "0");
                    Preview.Value = events.Preview;
                    EventStartTime.Value = events.StartDate.Value.TimeOfDay.ToString().Substring(0,5);
                    EventEndTime.Value = events.EndDate.Value.TimeOfDay.ToString().Substring(0, 5);

                    if (File.Exists(Server.MapPath("~/ClientData/"+SessionManager.ClientSiteID+"/Uploads/event_01_"+events.ID+".jpg")))
                    {
                        Image1.ImageUrl = "~/ClientData/" + SessionManager.ClientSiteID + "/Uploads/event_01_" +
                                          events.ID + ".jpg?" + DateTime.Now.Ticks;
                        Image1.Visible = true;
                    }

                    if (File.Exists(Server.MapPath("~/ClientData/" + SessionManager.ClientSiteID + "/Uploads/event_02_" + events.ID + ".jpg"))) {
                        Image2.ImageUrl = "~/ClientData/" + SessionManager.ClientSiteID + "/Uploads/event_02_" + events.ID + ".jpg?" + DateTime.Now.Ticks;
                        Image2.Visible = true;
                    }
                }

                //XmlSchema - must comply to XmlDataSchema/Fields/field;
                if (SchemaId > 0)
                {
                    if (!string.IsNullOrEmpty(schema))
                    {
                        SchemaXml.LoadXml(schema);

                        //Load the schema and data
                        ContentXml.XmlPanelSchema = SchemaXml;
                        ContentXml.XmlPanelData = XmlData;

                        //Render the control and add it to the Form
                        ContentXml.RenderXmlControls("tbl_Form");
                    }
                }

            }
        }

      

       
    }
}