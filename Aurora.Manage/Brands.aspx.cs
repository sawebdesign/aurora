﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage {
    public partial class Brands : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            Utils.CheckSession();
            Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
            int brandID;
            int.TryParse(Request["BrandID"],out brandID);

            var brandData = from brands in entities.Brand
                            where brands.ID == brandID
                            orderby brands.Name ascending
                            select brands;

            if (brandData.Count() > 0)
            {
                Description.Value = brandData.First().Description;
                Name.Value = brandData.First().Name;
                ID.Value = Utils.ConvertDBNull(brandData.First().ID,"");
            }
            
        }
    }
}