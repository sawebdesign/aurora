﻿//#region refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
/// <reference path="Scripts/timePicker-addOn.js" />

//#endregion

var oDescription;
var tabInit = false;

$(function () {

    getAllModules();
    loadSizes();
    loadBrands();
    getLinkedCats();

    DisplayMessage("Messages", "info", "please fill all information in the form below", 7000);
    $("#ProductTitle").text(String.format("Editing: {0}", $("#Name").val(), currentSizeName));
    $(".tipsy").remove();
    $(".help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    $("#btnSave").show();

    $('#isZeroShipping').click(function () {
        //$("#isZeroShipping").toggle(this.checked);
        toggleNoShipping();
    });
    $('#isFixedShipping').click(function () {
        //$("#isFixedShipping").toggle(this.checked);
        toggleFixedShipping();
    });
    
	//check if the product category name is set from the product or if it's blank because it's a new product
    if ($("#prodCategoryName").val() == "") {
    	$("#prodCategory").children().first()[0].nextSibling.nodeValue = $("#hierarchybreadcrumb").children().first()[0].nextSibling.nodeValue;
    } else {
    	$("#prodCategory").children().first()[0].nextSibling.nodeValue = $("#prodCategoryName").val();
    }

    if ($("#prodCategory").val() == "0") {
        $("#prodCategoryID").val($("#CatParentVal").val());
    }

    //set restrictions on text boxes
    $("#Price,#Total,#Height,#Length,#Width,#OrderIndex").ForceNumericOnly();
    $("#Price").val(formatDecimal($("#Price").val()));
    
    colour.display();
    $("#Messages").insertBefore($(".contentAddEdit"));
    //check if there is data in the grid.
    if (!prodHasData) {
        $("#prodGrid").parent().toggle(500);
    }

    var defDone = $.when(defferredColours);

    //load galleries
    gallery.loadGallery($("#ID").val(), "Images");
    $('#tab1').click();

    //Load categories into associated products

    if (assData == null) {
        assData = $create(Sys.UI.DataView, {}, {}, {}, $get("AssProdCategories"));
    }
    else {
        assData.dispose();
        assData = $create(Sys.UI.DataView, {}, {}, {}, $get("AssProdCategories"));
    }
    assData.set_data(productCategories);
    $("#AssProdCategories")[0].selectedIndex = 0;
    loadCurrentAssociatedProducts();

    $('#Modules').toChecklist({

        /**** Available settings, listed with default values. ****/
        addScrollBar: true,
        addSearchBox: true,
        searchBoxText: 'Type here to search list...',
        showCheckboxes: true,
        showSelectedItems: false,
        submitDataAsArray: true // This one allows compatibility with languages that use arrays

    });



    //create cat menu
    $('#prodCategory').menu({
        content: editAdd,
        backLink: true,
        flyOut: true,
        customClick: function (itemid) {
            editsetCat(itemid, true);
        }
    });
    //date time pickers
    $("#specialFrom").datetimepicker({ dateFormat: 'dd/mm/yy' });
    $("#specialTo").datetimepicker({ dateFormat: 'dd/mm/yy' });
    InitEditor("DetailText", "editorSpan", false);
    product.getSpecial();
    product.getCustomFieldData($("#ID").val());
    product.actionComplete();

    if ($("#ShippingCostLoc").val() != '' && $("#ShippingCostLoc").val() != undefined) {
    	$("#ShippingCostLoc").val(formatDecimal($("#ShippingCostLoc").val()));
    }
    if ($("#ShippingCostInt").val() != '' && $("#ShippingCostLoc").val() != undefined) {
    	$("#ShippingCostInt").val(formatDecimal($("#ShippingCostInt").val()));
    }
    toggleNoShipping();
    toggleFixedShipping();
});

function toggleNoShipping() {
    if ($('#isZeroShipping').is(":checked")) {
        $('#isFixedShipping').removeAttr("checked");;
        $('#ShippingCostLoc').val("");
        $('#ShippingCostInt').val("");

        $('#isFixedShipping').attr("disabled", "disabled");
        $('#ShippingCostLoc').attr("disabled", "disabled");
        $('#ShippingCostInt').attr("disabled", "disabled");
    } else {
        $('#isFixedShipping').removeAttr("disabled");
        $('#ShippingCostLoc').removeAttr("disabled");
        $('#ShippingCostInt').removeAttr("disabled");
    }
    toggleFixedShipping();
}

function toggleFixedShipping() {
    if ($('#isFixedShipping').is(":checked")) {
        $(".fixedShipping").show();
        $('#ShippingCostLoc').removeAttr("disabled");
        $('#ShippingCostInt').removeAttr("disabled");
    } else {
        $(".fixedShipping").hide();
        $('#ShippingCostLoc').val("");
        $('#ShippingCostInt').val("");
        $('#ShippingCostLoc').attr("disabled", "disabled");
        $('#ShippingCostInt').attr("disabled", "disabled");
    }
}

function loadSizes() {
    var onSuccess = function (result) {

        if (result.Data.length > 0) {
            if (!datasetSizes) {
                datasetSizes = $create(Sys.UI.DataView, {}, {}, {}, $get("Sizes"));
            }
            else {
                datasetSizes.dispose();
                datasetSizes = $create(Sys.UI.DataView, {}, {}, {}, $get("Sizes"));
            }
            datasetSizes.set_data(result.Data);
            $("#Sizes option").each(
                function (index, elem) {
                    if ($(this).val() == $("#SizeID").val()) {
                        $("#Sizes")[0].selectedIndex = index;
                        $(elem).attr("selected", "selected");
                    }
                    else {
                        $(elem).removeAttr("selected");
                    }
                }
            );

        }
        else {
            $("#contentAddEdit").hide();
            DisplayMessage("Messages", "warn", "No sizes have been created please create a size");
        }
    };
    Aurora.Manage.Service.AuroraWS.GetSizes(0, 0, "", onSuccess, onError);
}

function loadBrands() {
    var onsuccess = function (result) {
        if (result.Data.length > 0) {
            if (!datasetBrands) {
                datasetBrands = $create(Sys.UI.DataView, {}, {}, {}, $get("Brand"));
            }
            else {
                datasetBrands.dispose();
                datasetBrands = $create(Sys.UI.DataView, {}, {}, {}, $get("Brand"));
            }
            datasetBrands.set_data(result.Data);
            $("#Brand").val($("#Brand option")[0].value);
        }
    };

    Aurora.Manage.Service.AuroraWS.GetBrands(0, 10, "", onsuccess, onError);

}

function loadProducts() {
    var response = function (result) {
        assProdList = [];
        $("#AssProducts_selectedItems").children('li').each(function () {

            assProdList[assProdList.length] = $(this).html();
        });
        $('#AssProducts').remove();
        $("<select id=\"AssProducts\" sys:attach=\"dataview\" name=\"AssProducts\" title=\"Products\" style=\"width: 285px;height: 200px\" multiple=\"multiple\" class=\"sys-template\"></select>").appendTo($("#ProdSel"));

        $(result.Data).each(function (index, domEle) {
            var element = $(String.format("<option value='{0}'>{1}</option>", this.ID, this.Name));
            $(element).appendTo($("#AssProducts"));
            highlightAssociatedProducts(this.ID, element);

        });

        $('#AssProducts').toChecklist({
            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: true,
            searchBoxText: 'Type here to search list & then tab to select the item...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays
        });


        $(assProdList).each(function (index, domEle) {
            // domEle == this
            $(String.format("<li>{0}</li>", this)).appendTo($("#AssProducts_selectedItems"));

        });

        if ($("#AssProducts_checklist ul li").length === 0) {
            $('#AssProducts').hide();
        }
    };

    Aurora.Manage.Service.AuroraWS.GetProducts(0, 0, $("#AssProdCategories").val(), 0, response, onError);
}

function loadCurrentAssociatedProducts() {
    var response = function (result) {
        if (result.Data[0] != null) {
            currentAssociatedProducts = result.Data;
            loadProducts();
        }

    };

    if (currentItemID > 0) {
        Aurora.Manage.Service.AuroraWS.GetAssociatedProducts(currentItemID, response, onError);
    }

}

function highlightAssociatedProducts(id, objEle) {
    if (currentAssociatedProducts != null) {
        if (findByInArray(currentAssociatedProducts, "AssociatedProductID", id)) {
            $(objEle).attr("selected", "selected");
        }
    }


}

function editsetCat(item, addForm) {
    if (addForm) {

        $('#prodCategoryID').val(item);
        var itemName = $("#" + item).children('span').first().html();
        if (itemName == "" || itemName == undefined) {
            itemName = $("#" + item).html();
        }
        $("#prodCategory").children().first()[0].nextSibling.nodeValue = itemName;
    }
    else {
        $('#prodCategoryID').val(item);

        var itemName = $("#" + item).children('span').first().html();
        if (itemName == "" || itemName == undefined) {
            itemName = $("#" + item).html();
        }


        $("#prodCategory").children().first()[0].nextSibling.nodeValue = itemName;
    }

    return false;
}

function getLinkedCats() {
    var response = function (result) {
            for (var row = 0; row < result.Data.length; row++) {
                $catItem = $("#AllCats option[value='" + result.Data[row].CategoryID + "']");

                if ($catItem.length > 0) {
                    $catItem.attr("selected", "selected");
                }
            }

            $('#AllCats').toChecklist({
                /**** Available settings, listed with default values. ****/
                addScrollBar: true,
                addSearchBox: false,
                searchBoxText: 'Type here to search list...',
                showCheckboxes: true,
                showSelectedItems: true,
                submitDataAsArray: true // This one allows compatibility with languages that use arrays

            });
        
    };

    //bind data to all cats
    if (!linkCats) {
        linkCats = $create(Sys.UI.DataView, {}, {}, {}, $get("AllCats"));
    }
    else {
        linkCats.dispose();
        linkCats = null;
        linkCats = $create(Sys.UI.DataView, {}, {}, {}, $get("AllCats"));
    }
    linkCats.set_data(productCategories);
    $("#AllCats").val($('#prodCategoryID').val());
    $("#AllCats option[selected='selected']").remove();

    if (currentItemID > 0) {
        Aurora.Manage.Service.AuroraWS.GetLinkedCategories($("#ID").val(), response, onError);
    }
    else {
        $('#AllCats').toChecklist({
            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: false,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });
    }

}

function getAllModules() {
    var response = function (result) {
        getClientModules(result.Data);
    };

    Aurora.Manage.Service.AuroraWS.GetAllModules(response, onError);
}


function getClientModules(allModules) {
    var response = function (result, modData) {
        var associateID = findByInArray(modData, "Name", "ProductAssociated");
        var specials = findByInArray(modData, "Name", "ProductSpecial");

        if (!associateID || !findByInArray(result.Data, "FeaturesModuleID", associateID.ID)) {
            $("#NoAssociated").show();
        }

        if (!specials || !findByInArray(result.Data, "FeaturesModuleID", specials.ID)) {
            $("#NoSpecial").show();
        }

    };

    Aurora.Manage.Service.AuroraWS.GetModules(response, onError, allModules);
}

