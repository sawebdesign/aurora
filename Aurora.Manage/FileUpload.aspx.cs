﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage
{
    public partial class FileUpload : System.Web.UI.Page {

        public string fileNamePrefix;
        public int id;
        public string uploadFolder;
        public string uploadExtension;
        public string fileExtension;

        protected void Page_Load(object sender, EventArgs e) {
            fileNamePrefix = Request["prefix"] ?? null;
            id = Aurora.Custom.Utils.ConvertDBNull<int>(Request["id"], 0);
            uploadFolder = Request["path"] ?? null;
            uploadExtension = Request["extension"] ?? null;

            bool valid = (fileNamePrefix != null) && (id != 0) && (uploadFolder != null);
            if (!valid) {
                if (!Page.ClientScript.IsClientScriptBlockRegistered("InvokeMessage")) {
                    string script = "<script language='javascript'> var usermessage = 'Uploader invoked incorrectly.';</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "InvokeMessage", script);
                }
            }
            LoadFile();
        }

        protected void ButtonUploadFile_Click(object sender, EventArgs e) {

            if (FileUploadControl.HasFile && !FileUploadControl.FileName.Contains(".exe")) {
                fileExtension = FileUploadControl.FileName.Substring(FileUploadControl.FileName.LastIndexOf("."));
                
                if (!string.IsNullOrEmpty(uploadExtension)) {
                    if (uploadExtension != fileExtension) {
                        if (!Page.ClientScript.IsClientScriptBlockRegistered("FileMessage")) {
                            string script = "<script language='javascript'> var usermessage = 'Invalid file type, please ensure you have uploaded a "+ uploadExtension +" file.';</script>";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "FileMessage", script);
                        }
                        return;
                    }
                }

                //Update item media type
                AuroraEntities context = new AuroraEntities();
                var galleryItem = from galleries in context.Gallery
                                  where galleries.ID == id
                                  select galleries;
                galleryItem.First().MediaType = fileExtension;
                context.SaveChanges();

                FileUploadControl.SaveAs(Server.MapPath(uploadFolder + fileNamePrefix + id + fileExtension));

                LoadFile();

                if (!Page.ClientScript.IsClientScriptBlockRegistered("SaveMessage")) {
                    string script = "<script language='javascript'> var usermessage = 'File saved.';</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SaveMessage", script);
                }
            } else {
                if (!Page.ClientScript.IsClientScriptBlockRegistered("FileMessage")) {
                    string script = "<script language='javascript'> var usermessage = 'Invalid file entry, please ensure you have uploaded a file. Please note the executables are not allowed.';</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "FileMessage", script);
                }
            }
        }

        private void LoadFile() {
            if (uploadExtension == ".mp3") {
                HiddenAudioFileSource.Value = Security.Encryption.Encrypt(string.Format("{0}{1}", SessionManager.ClientBasePath, "/Uploads/gallery_01_" + id + uploadExtension));
                LinkDownloadFile.Visible = true;

                player.Text = string.Format("<object width=\"320\" height=\"90\" classid=\"CLSID:6BF52A52-394A-11D3-B153-00C04F79FAA6\"    codebase=\"http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/\">    <param name=\"URL\" value=\"{0}\">    <param name=\"rate\" value=\"1\">    <param name=\"balance\" value=\"0\">    <param name=\"currentPosition\" value=\"0\">    <param name=\"defaultFrame\" value=\"\">    <param name=\"playCount\" value=\"32767\">    <param name=\"autoStart\" value=\"0\">    <param name=\"currentMarker\" value=\"0\">    <param name=\"invokeURLs\" value=\"-1\">    <param name=\"baseURL\" value=\"\">    <param name=\"volume\" value=\"50\">    <param name=\"mute\" value=\"0\">    <param name=\"uiMode\" value=\"full\">    <param name=\"stretchToFit\" value=\"0\">    <param name=\"windowlessVideo\" value=\"0\">    <param name=\"enabled\" value=\"-1\">    <param name=\"enableContextMenu\" value=\"-1\">    <param name=\"fullScreen\" value=\"0\">    <param name=\"SAMIStyle\" value=\"\">    <param name=\"SAMILang\" value=\"\">    <param name=\"SAMIFilename\" value=\"\">    <param name=\"captioningID\" value=\"\">    <param name=\"enableErrorDialogs\" value=\"0\">    <param name=\"_cx\" value=\"8467\">    <param name=\"_cy\" value=\"7673\">    <param name=\"URL\" value=\"/ClientData/10001/Uploads/\">    <param name=\"rate\" value=\"1\">    <param name=\"balance\" value=\"0\">    <param name=\"currentPosition\" value=\"0\">    <param name=\"defaultFrame\" value=\"\">    <param name=\"playCount\" value=\"32767\">    <param name=\"autoStart\" value=\"-1\">    <param name=\"currentMarker\" value=\"0\">    <param name=\"invokeURLs\" value=\"-1\">    <param name=\"baseURL\" value=\"\">    <param name=\"volume\" value=\"50\">    <param name=\"mute\" value=\"0\">    <param name=\"uiMode\" value=\"full\">    <param name=\"stretchToFit\" value=\"0\">    <param name=\"windowlessVideo\" value=\"0\">    <param name=\"enabled\" value=\"-1\">    <param name=\"enableContextMenu\" value=\"-1\">    <param name=\"fullScreen\" value=\"0\">    <param name=\"SAMIStyle\" value=\"\">    <param name=\"SAMILang\" value=\"\">    <param name=\"SAMIFilename\" value=\"\">    <param name=\"captioningID\" value=\"\">    <param name=\"enableErrorDialogs\" value=\"0\">    <param name=\"_cx\" value=\"8466\">    <param name=\"_cy\" value=\"7672\">    <embed height=\"290\" type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/\" width=\"320\" src=\"{0}\" autorewind=\"1\" showdisplay=\"0\" showstatusbar=\"0\" showcontrols=\"1\" autostart=\"0\" filename=\"{0}\"> </embed></object>"
                             , "http://" + Request.ServerVariables["SERVER_NAME"] + "/ClientData/" + SessionManager.ClientSiteID + "/Uploads/gallery_01_" + id + uploadExtension);

            }
        }

        protected void LinkDownloadFile_Click(object sender, EventArgs e) {
            Utils.DownloadFile(HiddenAudioFileSource.Value);
        }

        protected void delete_ServerClick(object sender, EventArgs e) {
            string fpath = Server.MapPath(uploadFolder + fileNamePrefix + id + uploadExtension);
            if (File.Exists(fpath)) {
                File.Delete(fpath);
                if (!Page.ClientScript.IsClientScriptBlockRegistered("FileDeleted")) {
                    string script = "<script language='javascript'> var usermessage = 'The file has been deleted.';</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "FileDeleted", script);
                }
            }
        }
    }
}