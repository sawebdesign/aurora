﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aurora.Manage {
    public partial class Size : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            Custom.Utils.CheckSession();
            int sizeid;
            int.TryParse(Request["SizeID"], out sizeid);
            Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();

            var sizeData = from size in entities.Size
                           where size.ID == sizeid
                           select size;
            if(sizeData.Count() > 0)
            {
                Description.Value = sizeData.First().Description;
                ID.Value = sizeData.First().ID.ToString();
            }
        }
    }
}