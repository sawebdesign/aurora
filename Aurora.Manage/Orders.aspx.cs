﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Xml.Linq;

namespace Aurora.Manage {
    public partial class Orders : System.Web.UI.Page {
        /// <summary>
        /// 
        /// </summary>
        
        public struct Enquiry {
            public OrderItems OrderItems { get; set; }
            public Custom.Data.Product Product { get; set; }
            public IEnumerable<Custom.Data.Size> Size { get; set; }
            public IEnumerable<Custom.Data.Colour> Colour { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e) {
            Utils.CheckSession();
            AuroraEntities context = new AuroraEntities();
            if (Request["ID"] != null && Request["ID"] != "-1") {
                int orderID;
                int.TryParse(Request["ID"], out orderID);
                var order = (from orders in context.Orders
                             where orders.ID == orderID
                             select orders).SingleOrDefault();

                if (order != null) {
                    ClientFirstName.InnerHtml = order.ClientFirsName;
                    ClientLastName.InnerHtml = order.ClientLastName;
                    ClientEmail.InnerHtml = order.ClientEmail;
                    ClientEmail.HRef = "mailto:" + order.ClientEmail;
                    ClientContactNo.InnerHtml = order.ClientContactNo;
                    CompanyName.InnerHtml = order.CompanyName;
                    isApproved.Checked = order.isApproved;
                    ID.Value = order.ID.ToString();

					orderReference.Value = order.OrderReference;
					additionalInfo.Value = order.AdditionalInformation;
					orderEnquiry.InnerHtml = order.Enquiry;

                    //quote email
                    //Quote.InnerHtml = Quote.InnerHtml.Replace("{Name}", ClientFirstName.InnerText + " " + ClientLastName.InnerText);
                    Quote.InnerHtml = @Quote.InnerHtml.Replace("{ClientName}", CacheManager.SiteData.CompanyName);
                    Quote.InnerHtml = @Quote.InnerHtml.Replace("{Date}", order.InsertedOn.Value.ToString("dd-MM-yyyy HH:mm"));
                    ClientName.Value = CacheManager.SiteData.CompanyName;

                    //Get Items
                    var items = (from orderItems in context.OrderItems
                                 join product in context.Product on orderItems.ProductID equals product.ID
                                 join sizes in context.Size on orderItems.SizeID equals sizes.ID into leftjoinSizes
                                 from sizes in leftjoinSizes.DefaultIfEmpty()
                                 join colour in context.Colour on orderItems.ColourID equals colour.ID into outer
                                 from colour in outer.DefaultIfEmpty()
                                 where orderItems.OrderID == orderID
                                 select new {
                                     orderItems,
                                     product,
                                     leftjoinSizes,
                                     outer
                                 }).ToList().Select(u => new Enquiry {
                                     OrderItems = u.orderItems,
                                     Product = u.product,
                                     Size = u.leftjoinSizes,
                                     Colour = u.outer
                                 });

                    //Bind Data

                    rptrItemCollection.DataSource = items;
                    rptrItemCollection.DataBind();

                    //Disable UI
                    if (isApproved.Checked) {
                        isApproved.Disabled = true;
                    }
                    if (order.isApproved) {
                        btnProcessOrder.Disabled = true;
                    }

                }

            }
        }


        protected void rptrItemCollection_ItemCreated(object sender, RepeaterItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
                Enquiry x = (Enquiry)e.Item.DataItem;
                TextBox notes = (TextBox)e.Item.FindControl("Notes");
				HtmlInputText pprice = (HtmlInputText)e.Item.FindControl("ProductPrice");
				pprice.Value= Math.Round((decimal)x.Product.Price,2).ToString();
                if (x.Colour.Count() > 0) {
                    HtmlTableCell row = (HtmlTableCell)e.Item.FindControl("ColourRow");
                    row.Attributes.Add("style", "background:#" + x.Colour.First().HexValue + " url('" + x.Colour.First().ImageUrl + "')");
                }

                if (x.Size.Count() > 0) {
                    HtmlGenericControl label = (HtmlGenericControl)e.Item.FindControl("SizeName");
                    label.InnerHtml = x.Size.First().Description;
                }

                if (!string.IsNullOrEmpty(x.OrderItems.CustomXML))
                {
                    List<XElement> customData = (from xmlData in XDocument.Parse(x.OrderItems.CustomXML).Descendants("field")
                                     select xmlData).ToList();

                    notes.Text += "Colour: " + customData.Where(col => col.Attribute("name").Value == "Colour" ).SingleOrDefault().Attribute("value").Value;
                    notes.Text += Environment.NewLine + "Carat: " + customData.Where(col => col.Attribute("name").Value == "Carat").SingleOrDefault().Attribute("value").Value;
                    notes.Text += Environment.NewLine + "Clarity: " + customData.Where(col => col.Attribute("name").Value == "Clarity").SingleOrDefault().Attribute("value").Value;
                    notes.Text += Environment.NewLine + "Shape: " + customData.Where(col => col.Attribute("name").Value == "Shape").SingleOrDefault().Attribute("value").Value;
                    notes.Text += Environment.NewLine + "Comments: " + customData.Where(col => col.Attribute("name").Value == "Comments").SingleOrDefault().Attribute("value").Value;

                }
                else
                {
                    notes.Visible = false;
                }
            }
        }
    }
}