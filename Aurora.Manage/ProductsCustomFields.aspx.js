﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />

//#region Global Variables
var fieldXML;
var moveTimeOut;
//#endregion

//#region Data Manipulation

var customFields = {
    fieldCount: 0,
    fieldControls: '<td>' + '<a class="up" href="#" onclick="return customFields.moveField(this, \'Up\');" title="Move Up"><img src="/Styles/images/arrow_up.png"/></a></td>' +
                   '<td>' + '<a class="down" href="#" onclick="return customFields.moveField(this, \'Down\');" title="Move Down"><img src="/Styles/images/arrow_down.png"/></a></td>' +
                   '<td>' + '<a href="#" title="Delete" onclick="customFields.deleteField(this);"><img src="/Styles/images/brick_delete.png" /></a></td>',
    loadFields: function () {
        var onSuccess = function (result) {
            if (result.Result) {
                DisplayMessage("Messages", "success", "Fields Loaded", 5000);
                customFields.renderFields(result.Data);
                scrollToElement("Messages", -20, 100);
            }
            else {
                onError(result);
                scrollToElement("Messages", -20, 100);
            }
        };
        Aurora.Manage.Service.AuroraWS.GetProductsCustomFields(onSuccess, onError);
    },
    renderFields: function (resultXML) {
        var controlList = "";
        if (resultXML === "") {
            controlList = "No Custom Fields Found";
            customFields.fieldCount = 0;
        }
        else {
            var xml = $($.parseXML(resultXML));
            customFields.fieldCount = Number(xml.find("fieldcount").text());
            var controlList = "<table width='220px' style='margin-left: 0'>";

            xml.find("field").each(function () {
                var field = $(this);
                controlList += '<tr><td width="160px"><input style="width:150px" type="text" name="' + field.attr('fieldname') + '" value="' + field.attr('title') + '" /></td>';
                controlList += customFields.fieldControls + '</tr>';
            });
            controlList += "</table>";
        }
        $('#CustomFields').html(controlList);
        customFields.setupControls();
        return false;

    },
    setupControls: function () {
        $('#CustomFields a').show();
        $('#CustomFields tr:first-child .up').hide();
        $('#CustomFields tr:last-child .down').hide();
    },
    addNewField: function () {
        if ($('#CustomFields input').length === 0) {
            $('#CustomFields').html("<table width='220px' style='margin-left: 0'></table>");
        }
        var newField = '<tr><td width="160px"><input style="width:150px" type="text" name="Field' + (customFields.fieldCount + 1) + '" value="Field ' + (customFields.fieldCount + 1) + '" /></td>';
        newField += customFields.fieldControls + '</tr>';
        $('#CustomFields table').append(newField);

        //Increment total fieldcount
        customFields.fieldCount = customFields.fieldCount + 1;
        customFields.setupControls();
        // Prevents postback
        return false;
    },
    deleteField: function (anchor) {
        if (confirm("Are you sure you want to delete this field? It will be deleted across the whole product system")) {
            $(anchor).parent().parent().remove();
            if ($('#CustomFields input').length === 0) {
                $('#CustomFields').html('No Custom Fields Found');
            }
            customFields.setupControls();
        }
        return false;
    },
    moveField: function (anchor, direction) {
        $(anchor).parent().parent().find('input').css("background-color", "#6eb000");
        clearTimeout(moveTimeOut);
        moveTimeOut = setTimeout(function () {
            $('#CustomFields input').css("background-color", "");
        }, 1500);
        if (direction === 'Up') {
            var rowAbove = $(anchor).parent().parent().prev();
            $(anchor).parent().parent().insertBefore(rowAbove);
        }
        else if (direction === 'Down') {
            var rowBelow = $(anchor).parent().parent().next();
            $(anchor).parent().parent().insertAfter(rowBelow);
        }
        customFields.setupControls();
        return false;
    },
    saveFields: function () {
        var onSuccess = function (result) {
            if (result.Result) {
                DisplayMessage("Messages", "success", "Fields Saved", 5000);
                scrollToElement("Messages", -20, 100);
            }
            else {
                onError(result);
                scrollToElement("Messages", -20, 100);
            }
        };
        var fields = $('#CustomFields input').serializeArray();
        var customXML = '<XmlDataSchema>';
        if (fields.length > 0) {
            customXML += '<Fields type="CustomFields">';
            for (var i = 0; i < fields.length; i++) {
                customXML += '<field fieldname="' + fields[i].name + '" datatype="string" maxlength="150" x="0" y="' + i + '" style="width:285px" title="'+ fields[i].value + '">';
                customXML += '<labels><label>' + fields[i].value + '</label></labels>';
                customXML += '</field>';
            }
            customXML += '</Fields>';
        }
        customXML += '<fieldcount>' + customFields.fieldCount + '</fieldcount>';
        customXML += '</XmlDataSchema>';

        Aurora.Manage.Service.AuroraWS.UpdateProductsCustomFields(customXML, onSuccess, onError);
        return false;


    }
};
//#endregion

//#region On Document Ready
$(function () {

    customFields.loadFields();
    DisplayMessage("MessageWarning", "warn", "Any changes you make here will affect all products, future or current, stored in the system.");
});
//#endregion