﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomFieldEditor.ascx.cs"
    Inherits="Aurora.Manage.UserControls.CustomFieldEditor" %>
<link href="/Styles/DropDown.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/UserControls/CustomFieldEditor.ascx.js?<%=DateTime.Now.ToString("ss") %>"></script>
<link type="text/css" href="/Styles/ui.all.css" media="screen" rel="stylesheet" />
<div>
    <input type="hidden" value="" runat="server" clientidmode="Predictable" id="hiddenCustomModuleID" />
    <input type="hidden" value="" runat="server" clientidmode="Predictable" id="hiddenModuleName" />
    <br class="clear" />
    <div id="MessageWarning">
    </div>
    <div id="Messages">
    </div>
    <div style="float: left; margin-top: 10px; margin-right: 10px;">
        <a href="#AddNewFields" id="addit" onclick="customFields.clearFields(); $('#formmode').val('');">Add
            a New Field
            <img src="/Styles/images/brick_add.png" dataid="-1" alt="Create New Custom Field"
                title="Create New Custom Field" style="cursor: pointer" onclick="" /></a>
    </div>
    <br class="clear" />
    <div id="CustomFields">
    </div>
    <div style="display: none;">
        <div id="AddNewFields"  style="background:#fff;width:600px;height:480px;">
            <h2>Add/Edit a Custom Field</h2>
            <table width="100%" >
                <tbody>
                    <tr style="display:none;">
                        <td><input type="hidden" id="formmode" value="" /></td>
                    </tr>
                    <tr>
                        <td>
                            Field Label:
                        </td>
                        <td>
                            <input onkeyup="customFields.addToNameField($(this).val());" type="text" id="lblNewFieldLabel" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Field Name:
                        </td>
                        <td>
                            <input type="text" id="lblNewFieldName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Field Type:
                        </td>
                        <td>
                            <select id="ddlNewFieldType" onchange="customFields.showValueField();">
                               <%-- <option value="datetime">Date Picker</option>--%>
                                <option value="string" controltype="input[type=text]">Text Box</option>
                                <option value="textarea" controltype="textarea">Text Area</option>
                                <option value="lookupxml" controltype="select">Drop Down List</option>
                               <%-- <option value="lookupxmlradio">Radio Button List</option>--%>
                                <option value="bool" controltype="input[type=checkbox]">CheckBox</option>
                                <option value="fileupload" controltype="input[type=file]">File Upload</option>
                                <option value="html" controltype="HTML">HTML Label</option>
                            </select>
                        </td>
                    </tr>
                    <tr style="display:none;" id="list">
                        <td>Values (separater by  a ','):</td>
                        <td>
                            <input type="text" id="listValues" />
                        </td>
                    </tr>
                    <tr style="display:none;" id="multiples">
                        <td>Allow multiple selection:</td>
                        <td>
                            <input type="checkbox" id="allowMultiple" />
                        </td>
                    </tr>
                    <tr style="display: none;" id="area">
                        <td colspan="2">
                          Columns:&nbsp;  <input type="text" id="textareaCols"  />&nbsp;
                            Rows&nbsp;<input type="text" id="textareaRows" />
                        </td>
                    </tr>
                    <tr style="display: none;" id="htmleditor">
                        <td valign="top">HTML :</td>
                        <td>
                            <textarea id="htmltextarea" cols="50" rows="15"></textarea> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Validation:</td>
                        <td>
                            <input class="validation" type="checkbox" id="isRequired" validtype="all" /><span validtype="all">Required Field</span><br />
                            <input class="validation" type="checkbox" id="Email" /><span class="validationdesc">Email</span><br />
                            <input class="validation" type="checkbox" id="String" /><span class="validationdesc">Text Only</span><br />
                         <%--   <input class="validation" type="checkbox" id="Int" /><span class="validationdesc">Numbers Only</span><br />--%>
                            <input class="validation" type="checkbox" id="Time" /><span class="validationdesc">Time</span><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="Save" onclick="customFields.addNewField();" />
                        </td>
                        <td>
                            <input type="button" onclick="$.fancybox.close();" value="Cancel" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
