﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />

//#region Global Variables
var fieldXML;
var moveTimeOut;
//#endregion

//#region Data Manipulation
var customFields = {
    fieldCount: 0,
    fieldControls: '<td width="5%" valign="top">' + '<a class="up" href="javascript:void();" onclick="return customFields.moveField(this, \'Up\');" title="Move Up"><img src="/Styles/images/arrow_up.png"/></a></td>' +
                   '<td width="5%" valign="top">' + '<a class="down" href="javascript:void();" onclick="return customFields.moveField(this, \'Down\');" title="Move Down"><img src="/Styles/images/arrow_down.png"/></a></td>' +
                   '<td width="5%" valign="top">' + '<a href="javascript:void();" title="Edit" onclick="customFields.editField(this);"><img src="/Styles/images/icon_edit.png" /></a></td>' +
                   '<td width="5%" valign="top">' + '<a href="javascript:void();" title="Delete" onclick="customFields.deleteField(this);"><img src="/Styles/images/brick_delete.png" /></a></td>',

    loadFields: function () {
        /// <summary>Retrieves custom XML from the DB</summary>
        var onSuccess = function (result) {
            if (result.Result) {
                customFields.renderFields(result.Data);
            }
            else {
                onError(result);
                scrollToElement("MessageWarning", -20, 100);
            }
        };
        Aurora.Manage.Service.AuroraWS.GetModuleCustomFields($("#CustomFieldEditor1_hiddenModuleName").val(), Number($("#CustomFieldEditor1_hiddenCustomModuleID").val()) || 0, onSuccess, onError);
    },
    renderFields: function (resultXML) {
        /// <summary>Creates controls and previews in the UI</summary>
        /// <param name="resultXML" type="String">XML in string format</param>

        var controlList = "";
        controlList = "<table width='100%' style='margin-left: 0' class='data'>";
        controlList += "<thead><tr><th align='left'>Field Name</th><th align='left'>Preview</th><th>Move Up</th><th>Move Down</th><th>Edit</th><th>Remove</th></tr></thead>"
        controlList += "<tbody>";
        if (resultXML === "") {
            controlList += "</tbody></table>";
            customFields.fieldCount = 0;
        }
        else {
            var xml = $($.parseXML(resultXML));
            //customFields.fieldCount = Number(xml.find("fieldcount").text());
            customFields.fieldCount = 0;
            xml.find("field").each(function () {
                var field = $(this);
                switch (field.attr('datatype')) {
                    case "string":
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input style="width:150px" type="text" label="{3}" datatype="{2}" name="{1}" validation="{4}"/></td>', field.attr('title'), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "bool":
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px" align="left"><input style="width:150px" type="checkbox" label="{3}" datatype="{2}" name="{1}"/></td>', field.attr('title'), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "lookupxml":
                        var valueHTML = '<select label="{2}" datatype="{1}" name="{0}" validation="{3}" ' + (field.attr("multipleselection") == "true" ? ' multiple="multiple" ' : '') + '>';

                        field.find("listitem").each(
                            function () {
                                $values = $(this);
                                valueHTML += String.format("<option value='{0}'>{0}</option>", $values.attr("value"));
                            }
                        );

                        valueHTML += "</select>";
                        controlList += String.format('<tr><td valign="top" width="60%">{2}</td><td width="160px" align="left">' + valueHTML + '</td>', field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "lookupxmlradio":
                        var valueHTML = "<div>";

                        field.find("listitem").each(
                                                    function () {
                                                        $values = $(this);
                                                        valueHTML += String.format("<input type='radio' name='{1}' value='{0}' />", $values.attr("value"), field.attr("fieldname"));
                                                    }
                                                );

                        valueHTML += "</div>";
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px" align="left">' + valueHTML + '</td>', field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "datetime":
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input style="width:150px" type="text" label="{3}" datatype="{2}" name="{1}"  validation="{4}"/></td>', field.attr('title'), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "textarea":
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><textarea style="width:150px" cols="59" rows="10" label="{3}" datatype="{2}" name="{1}" validation="{4}"></textarea></td>', field.attr('title'), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "fileupload":
                        controlList += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input style="width:150px" type="file" label="{3}" datatype="{2}" name="{1}" validation="{4}"/></td>', field.attr('title'), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;
                    case "html":
                        controlList += String.format('<tr style="background:rgb(230, 245, 253);font-weight:bold;"><td valign="top" width="60%">This is a label field</td><td width="160px"><div style="width:150px"  label="{3}" datatype="{2}" name="{1}">{0}</div></td>', decodeHTML(field.find('label').text()), field.attr('fieldname'), field.attr('datatype'), field.attr('title'), field.attr("validation"));
                        break;


                }

                controlList += customFields.fieldControls + '</tr>';
                customFields.fieldCount++;
            });
            controlList += "</tbody></table>";
        }
        $('#CustomFields').html(controlList);
        customFields.setupControls();
        return false;

    },
    setupControls: function () {
        /// <summary>sets up controls</summary>
        $('#CustomFields a').show();
        $('#CustomFields tr:first-child .up').hide();
        $('#CustomFields tr:last-child .down').hide();
    },
    addNewField: function () {
        /// <summary>Adds/Updates a field in the current form.</summary>
        /// <returns type="Nothing" />

        //#region Check of validation has been requested 
        var required = "";
        $.map($(".validation:checked"), function (element, indx) {
            if ($(element).attr("id") === "isRequired") {
                required += "required;";
            }
            else {
                required += ("regex:" + $(element).attr("id") + ";");
            }
        });
        //#endregion

        //#region Edit Mode 
        var $formMode = $("#formmode").val();
        if ($formMode == "EDIT") {
            $("#formmode").val("");

            $currentField = $(String.format("#CustomFields table [name='{0}']", $("#lblNewFieldName").val()));

            //check if current control is a DDL then add the Not Selected Value if not there/ Remove not selected as it is not required
            if ($currentField.is("select") && required.indexOf("required;") > -1 && $("#listValues").val().indexOf("Not Selected,") == -1) {
                $("#listValues").val("Not Selected," + $("#listValues").val());
            }
            else if ($currentField.is("select") && required.indexOf("required;") < 0) {
                $("#listValues").val($("#listValues").val().replace("Not Selected,", ""));
            }

            //remove any special chars
            customFields.addToNameField($("#lblNewFieldLabel").val());
            //set control values
            $currentField
                .attr("label", encodeHTML($("#lblNewFieldLabel").val()))
                .attr("datatype", $("#ddlNewFieldType").val())
                .attr("validation", required)
                .parent().prev().html($("#lblNewFieldLabel").val());

            //check for multiple selection
            if ($currentField.is("select") && $("#allowMultiple").is(":checked")) {
                $currentField.attr("multiple", "multiple");
                $("#listValues").val($("#listValues").val().replace("Not Selected,", ""));
            }
            else if ($currentField.is("select") && $currentField.attr("multiple") != undefined) {
                $currentField.removeAttr("multiple");
            }

            //transform field if necessary 
            var controlSelector = String.format("#CustomFields table {0}[name='{1}']", $("#ddlNewFieldType option:selected").attr("controltype"), $currentField.attr("name"));
            if ($(controlSelector).length === 0) {
                var controlToCreate;
                switch ($("#ddlNewFieldType option:selected").attr("controltype")) {
                    case "select":
                        var listItems = $("#listValues").val().split(",");
                        var valueHTML = "";
                        var multipleSelect = $("#allowMultiple").is(":checked") ? "multiple=\"multiple\"" : "";
                        
                        //iterate and create option elements
                        for (var item = 0; item < listItems.length; item++) {
                            if (listItems[item] !== "") {
                                valueHTML += String.format("<option value='{0}'>{0}</option>", listItems[item]);
                            }
                        }

                        controlToCreate = String.format("<select label='{0}' name='{1}' datatype='{2}' {4}>{3}</select>"
                                                            , $currentField.attr("label")
                                                            , $currentField.attr("name")
                                                            , $currentField.attr("datatype")
                                                            , valueHTML
                                                            , multipleSelect
                                                            );

                        break;
                    case "textarea":
                        $("#listValues").val("");
                        controlToCreate = String.format("<textarea label='{0}' name='{1}' datatype='{2}'></textarea>"
                                                            , $currentField.attr("label")
                                                            , $currentField.attr("name")
                                                            , $currentField.attr("datatype")
                                                            );
                        break;
                    case "input[type=text]":
                        $("#listValues").val("");
                        controlToCreate = String.format("<input type='text' label='{0}' name='{1}' datatype='{2}' />"
                                                            , $currentField.attr("label")
                                                            , $currentField.attr("name")
                                                            , $currentField.attr("datatype")
                                                            );
                        break;
                    case "HTML":
                        $("#listValues").val("");
                        $currentField.parent().prev().html("This is a label field");
                        controlToCreate = String.format("<div type='html' label='{0}' datatype='{2}' name='{1}'>{3}</div>"
                                                            , $currentField.attr("label")
                                                            , $currentField.attr("name")
                                                            , $currentField.attr("datatype")
                                                            , decodeHTML($("#htmltextarea").val())
                                                            );
                        break;

                }

                if (controlToCreate) {
                    $(controlToCreate).insertAfter($currentField);
                    $currentField.remove();
                }
                else {
                    //Display message
                    $("<span>Controls of this type cannot be transformed.</span>").dialog({
                        show: { effect: 'drop', direction: "right" },
                        hide: "slide",
                        modal: true,
                        resizable: false,
                        title: "Error",
                        zIndex: 1200,
                        buttons: { "Okay": function () { $(this).dialog("close"); } }
                    });
                    return;
                }
            }


            if ($currentField.is("div")) {
                $currentField.html($("#htmltextarea").html());
                $currentField.attr("label", $("#htmltextarea").html());
                $currentField.parent().prev().html("This is a label field");
            }
            else if ($currentField.is("select")) {
                var listItems = $("#listValues").val().split(",");
                var valueHTML = "";

                //iterate and create option elements
                for (var item = 0; item < listItems.length; item++) {
                    if (listItems[item] !== "") {
                        valueHTML += String.format("<option value='{0}'>{0}</option>", listItems[item]);
                    }
                }

                //add new fields
                $currentField.children('option').remove();
                $currentField.append(valueHTML);

            }

            //close the form
            $.fancybox.close();

            //Display message
            $("<span>Control Updated</span>").dialog({
                show: { effect: 'drop', direction: "right" },
                hide: "slide",
                modal: true,
                resizable: false,
                title: "Success",
                zIndex: 1200,
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
            return;
        }
        //#endregion

        //#region Error Check 
        //add unique characters to end of control name
        $("#lblNewFieldName").val($("#lblNewFieldName").val().replace(/ /gi, '') + new Date().getMilliseconds());
        if ($(String.format("input[name='{0}'],textarea[name='{0}']", $("#lblNewFieldName").val())).length > 0) {
            $("<div>Sorry but a field with that name already exists.</div>").dialog({
                show: { effect: 'drop', direction: "right" },
                hide: "slide",
                modal: true,
                resizable: false,
                title: "Error",
                zIndex: 1200,
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
            return false;
        }
        $("#lblNewFieldLabel").val(encodeHTML($("#lblNewFieldLabel").val()));
        if ($("#lblNewFieldLabel").val() === "") {
            $("<div>Please enter a value for field label.</div>").dialog({
                show: { effect: 'drop', direction: "right" },
                hide: "slide",
                modal: true,
                resizable: false,
                title: "Error",
                zIndex: 1200,
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
            return false;
        }
        //#endregion

        //#region Add 
        //add this so that validation is added as an attribute 
        required = "validation=" + required;

        if (required === "validation=") {
            required = "validation=\"\"";
        }

        var newField = "";
        switch ($("#ddlNewFieldType").val()) {
            case "string":
                newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input ' + required + '  style="width:150px" type="text" label="{0}" datatype="{2}" name="{1}"/></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val());
                break;
            case "bool":
                newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px" align="left"><input ' + required + '  style="width:150px" type="checkbox" label="{0}" datatype="{2}" name="{1}"/></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val());
                break;
            case "lookupxml":
                if ($("#listValues").val().length > 0) {
                    var values = "";
                    if ($("#allowMultiple").is(":checked")) {
                        multipleSelect = 'multiple="multiple"';
                    }
                    else {
                        multipleSelect = "";
                    }
                    var arry = $("#listValues").val().split(",");
                    for (var index = 0; index < arry.length; index++) {
                        values += String.format("<option value='{0}'>{0}</option>", arry[index]);
                    }

                    newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px" align="left"><select ' + multipleSelect + ' ' + required + '  style="width:150px"  label="{0}" datatype="{2}" name="{1}">{3}</select></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val(), values);
                }
                break;
            case "lookupxmlradio":
                if ($("#listValues").val().length > 0) {
                    var values = "";
                    var arry = $("#listValues").val().split(",");
                    for (var index = 0; index < arry.length; index++) {
                        values += String.format('<input type="radio" label="{3}" datatype="{2}" name="{1}"/><label>{3}</label><br/>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val(), arry[index]);
                    }

                    newField += String.format("<tr><td valign=\"top\" width=\"60%\">{0}</td><td width=\"160px\">{1}</td>", $("#lblNewFieldLabel").val(), values);
                }
                break;
            case "datetime":
                newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input ' + required + '  style="width:150px" type="text" label="{0}" datatype="{2}" name="{1}"/></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val());
                break;
            case "textarea":
                newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><textarea ' + required + '  style="width:150px" cols="{3}" rows="{4}" label="{0}" datatype="{2}" name="{1}"></textarea></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val(), $("#textareaCols").val(), $("#textareaRows").val());
                break;
            case "fileupload":
                newField += String.format('<tr><td valign="top" width="60%">{0}</td><td width="160px"><input ' + required + '  style="width:150px" type="file" label="{0}" datatype="{2}" name="{1}"/></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val());
                break;
            case "html":
                newField += String.format('<tr style="background:rgb(230, 245, 253);font-weight:bold;"><td valign="top" width="60%">This is a label field</td><td width="160px"><div type="html" label="{0}" datatype="{2}" name="{1}">' + $("#htmltextarea").html() + '</div></td>', $("#lblNewFieldLabel").val(), $("#lblNewFieldName").val(), $("#ddlNewFieldType").val(), 50, 10);

        }

        if (newField.length == 0) {
            return;
        }

        newField += customFields.fieldControls + '</tr>';
        $('#CustomFields table tbody').append(newField);

        //Increment total fieldcount
        customFields.fieldCount = customFields.fieldCount + 1;
        customFields.setupControls();
        $("<span>Control Added</span>").dialog({
            show: { effect: 'drop', direction: "right" },
            hide: "slide",
            modal: true,
            resizable: false,
            title: "Success",
            zIndex: 1200,
            buttons: { "Im done": function () { $(this).dialog("close"); $.fancybox.close(); customFields.clearFields(); }, "Add another": function () { $(this).dialog("close"); customFields.clearFields(); } }
        });
        //#endregion

        //remove any selected validation
        $(".validation:checked").removeAttr("checked");
        // Prevents postback
        return false;
    },
    deleteField: function (anchor) {
        /// <summary>Deletes a custom field</summary>
        /// <param name="anchor" type="Object">Element that invoked this method</param>
        if (confirm("Are you sure you want to delete this field? It will be deleted across the whole form system")) {

            $(anchor).parent().parent().remove();
            customFields.setupControls();

        }
        return false;
    },
    editField: function (anchor) {
        /// <summary>Edits a custom field</summary>
        /// <param name="anchor" type="Object">Element that invoked this method</param>

        //show form
        $("#addit").click();

        //clear form
        customFields.clearFields();
        $("#allowMultiple").removeAttr("checked");
        //get control
        var $current = $(anchor).parent().prev().prev().prev().children().first();

        //update controls with current items data
        $("#lblNewFieldLabel").val($current.attr("label"));
        $("#lblNewFieldName").val($current.attr("name"));
        $("#ddlNewFieldType").val($current.attr("datatype"));
        $("#listValues").val($.map($current.children('option'), function (a) { return a.value; }).join(','));

        //special for certain elements
        if ($current.is("textarea")) {
            $("#textareaCols").val($current.attr("cols"));
            $("#textareaRows").val($current.attr("rows"));
        }

        //if html control
        if ($current.attr("datatype").toUpperCase() === "HTML") {
            $("#htmltextarea").val($current.html());
        }

        $("#formmode").val("EDIT");
        $("#ddlNewFieldType").change();
        //get control validation settings

        if ($current.attr("validation")) {
            var validationsettings = $current.attr("validation").toString().split(";");

            for (var item = 0; item < validationsettings.length; item++) {
                var regexArr = validationsettings[item].split(":");
                if (regexArr.length > 1) {
                    $("#" + regexArr[1]).attr("checked", "checked");
                }
                else {
                    $("#isRequired").attr("checked", "checked");
                }
            }
        }

        //check if multiple for ddl
        if ($current.is("select") && $current.attr("multiple")) {
            $("#allowMultiple").attr("checked", "checked");
        }
    },
    clearFields: function () {
        /// <summary>Clears all input fields</summary>
        $("#AddNewFields input[type=text]").val("");
    },
    moveField: function (anchor, direction) {
        /// <summary>Either increments or decrements field index</summary>
        /// <param name="anchor" type="Object">Target element</param>
        /// <param name="name" type="String">Determines whether to increment/decrement controls order index</param>

        $(anchor).parent().parent().find('input').css("background-color", "#6eb000");
        clearTimeout(moveTimeOut);
        moveTimeOut = setTimeout(function () {
            $('#CustomFields input').css("background-color", "");
        }, 1500);
        if (direction === 'Up') {
            var rowAbove = $(anchor).parent().parent().prev();
            $(anchor).parent().parent().insertBefore(rowAbove);
        }
        else if (direction === 'Down') {
            var rowBelow = $(anchor).parent().parent().next();
            $(anchor).parent().parent().insertAfter(rowBelow);
        }
        customFields.setupControls();
        return false;
    },
    saveFields: function () {
        var onSuccess = function (result) {
            if (result.Result) {
                DisplayMessage("MessageWarning", "success", "Fields Saved", 5000);
                scrollToElement("MessageWarning", -20, 100);
            }
            else {
                onError(result);
                scrollToElement("MessageWarning", -20, 100);
            }
        };
        var fields = $('#CustomFields input,#CustomFields textarea,#CustomFields select,#CustomFields div');
        var customXML = '<XmlDataSchema>';
        if (fields.length > 0) {
            customXML += '<Fields type="CustomFields">';
            for (var i = 0; i < fields.length; i++) {
                $control = $(fields[i]);
                var validation = $control.attr("validation") || "";
                switch ($control.attr("datatype").toString().toUpperCase()) {
                    default:
                        customXML += '<field validation="' + validation + '" fieldname="' + encodeHTML($control.attr("name")) + '" datatype="' + $control.attr("datatype") + '" maxlength="150" x="0" y="' + i + '" title="' + encodeHTML($control.attr("label")) + '">';
                        customXML += '<labels><label>' + encodeHTML($control.attr("label")) + '</label></labels>';
                        customXML += '</field>';
                        break;
                    case "TEXTAREA":
                        customXML += '<field validation="' + validation + '"  multilinecolumns="' + $control.attr("cols") + '" multilinerows="' + $control.attr("rows") + '" fieldname="' + encodeHTML($control.attr("name")) + '" datatype="' + $control.attr("datatype") + '" maxlength="150" x="0" y="' + i + '" title="' + encodeHTML($control.attr("label")) + '">';
                        customXML += '<labels><label>' + encodeHTML($control.attr("label")) + '</label></labels>';
                        customXML += '</field>';
                        break;
                    case "LOOKUPXML":
                        customXML += '<field validation="' + validation + '" multipleselection="' + ($control.attr("multiple") ? true : false) + '" fieldname="' + encodeHTML($control.attr("name")) + '" datatype="' + $control.attr("datatype") + '" maxlength="150" x="0" y="' + i + '" title="' + encodeHTML($control.attr("label")) + '">';

                        customXML += '<labels><label>' + encodeHTML($control.attr("label")) + '</label></labels><valuelist>';
                        $control.children('option').each(
                            function (indx, elm) {
                                customXML += String.format("<listitem description=\"{0}\" value=\"{0}\">{0}</listitem>", encodeHTML(elm.value));
                            }
                        );
                        customXML += '</valuelist></field>';
                        break;
                    case "LOOKUPXMLRADIO":
                        customXML += '<field validation="' + validation + '"  fieldname="' + encodeHTML($control.attr("name")) + '" datatype="' + $control.attr("datatype") + '" maxlength="150" x="0" y="' + i + '" title="' + encodeHTML($control.attr("label")) + '">';
                        customXML += '<labels><label>' + encodeHTML($control.attr("label")) + '</label></labels><valuelist>';
                        $control.children('option').each(
                            function (indx, elm) {
                                customXML += String.format("<listitem description=\"{0}\" value=\"{0}\">{0}</listitem>", encodeHTML(elm.value));
                            }
                        );
                        customXML += '</valuelist></field>';
                        break;
                    case "HTML":
                        customXML += '<field validation="' + validation + '"  fieldname="' + encodeHTML($control.attr("name")) + '" datatype="' + $control.attr("datatype") + '" maxlength="8000" x="0" y="' + i + '" title="' + encodeHTML($control.html()) + '" value="' + encodeHTML($control.html()) + '">';
                        customXML += '<labels><label>' + encodeHTML($control.html()) + '</label></labels>';
                        customXML += '</field>';
                        break;
                }

            }
            customXML += '</Fields>';
        }
        customXML += '<fieldcount>' + fields.length + '</fieldcount>';
        customXML += '</XmlDataSchema>';

        if ($("#CustomFieldEditor1_hiddenModuleName").val().length > 0) {
            Aurora.Manage.Service.AuroraWS.UpdateModuleCustomFields(customXML || "", $("#CustomFieldEditor1_hiddenModuleName").val(), onSuccess, onError);
        }
        else {
            Aurora.Manage.Service.AuroraWS.UpdateCustomModuleCustomFields(customXML || "", Number($("#CustomFieldEditor1_hiddenCustomModuleID").val()), customModule, onSuccess, onError);
        }

        return false;


    },
    showAddFields: function () {

    },
    showValueField: function () {
        //remove selected validation
        $(".validation:checked").removeAttr("checked");
        switch ($("#ddlNewFieldType").val()) {
            default:
                $("#area").hide();
                $("#list,#multiples").hide();
                $("#htmleditor").hide();
                $(".validation,.validationdesc").show();
                break;
            case "textarea":
            case "fileupload":
                // $("#area").show();
                $("#list,#multiples").hide();
                $("#htmleditor").hide();
                $(".validation,.validationdesc").not("[validtype='all']").hide()
                break;
            case "lookupxmlradio":
            case "lookupxml":
                $("#area").hide();
                $("#list,#multiples").show();
                $("#htmleditor").hide();
                $(".validation,.validationdesc").not("[validtype='all']").hide()
                break;
            case "html":

                $("#htmleditor").show();
                $("#area").hide();
                $("#list,#multiples").hide();
                $(".validation,.validationdesc").hide()
                break;

        }
    },
    addToNameField: function (char) {
        /// <summary>This event is trigger when a user starts typing inside the field label textbox & prevents any special chats getting in.</summary>
        /// <param name="char" type="String">The string of chars within the current textbox</param>
        /// <returns type="String" />

        if ($("#formmode").val() != "EDIT") {
            $("#lblNewFieldName").val(char.replace(/[^\w\s]/gi, ''));
        }
    }
};
//#endregion

//#region On Document Ready
$(function () {
    if ($("#CustomFieldEditor1_hiddenCustomModuleID").val() != "" || $("#CustomFieldEditor1_hiddenModuleName").val() != "") {
        customFields.loadFields();
        DisplayMessage("MessageWarning", "warn", "Any changes you make here will affect this module & its data stored in the system.");
        $("#addit").fancybox(
            {
                scrolling: 'no',
                titleShow: false
            }
        );

        $("#addit").show();
    }
    else {
        $("#addit").hide();
        DisplayMessage("MessageWarning", "warn", "Fields may only be added once this module has been created.");
    }

    $("#lblNewFieldLabel").restrictChars({ regex: /[a-zA-Z\s\b:]/ });
});
//#endregion