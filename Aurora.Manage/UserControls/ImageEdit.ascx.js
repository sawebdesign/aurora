﻿/// <reference path="/Scripts/jquery-1.4.1.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />

var filePath;
var fileName; 
var useHeight;
var dsMethods = null;
var time = new Date();
var ImageEdit = {
    init: function () {
        ImageEdit.createUploader();
        ImageEdit.getFilters();
    },
    createUploader: function () {
        filePath = String.format("{0}/Uploads/", $("#CurrentPath").val());
        fileName = $("#FileName").val();
        useHeight = $("#UseHeight").val();

        var uploader = new qq.FileUploader({
            // pass the dom node (ex. $(selector)[0] for jQuery users)
            element: document.getElementById('file-uploader'),
            // path to server-side upload script
            action: '/ToolPages/FileUpload.ashx',
            sizeLimit: 20971520,
            onSubmit: function (id, fName) {
                uploader.setParams({
                    UploadType: "File",
                    FileName: (!fileName && fileName != "") ? fileName : fName,
                    Path: filePath,
                    useHeight: !useHeight ? false : useHeight,
                    imageSize: 800
                });
            },
            onComplete: function (id, fileName, responseJSON) {
                if (responseJSON.success == true) {
                    // DisplayMessage("Messages", "success", "File uploaded", 7000);
                    $("#previewImage").show().attr("src", responseJSON.filename.replace("~", ""));
                    $(".uploadImage").hide();
                }
            },
            showMessage: function (message) {
                $("<div>File Upload Failed</div>").dialog({
                    show: { effect: 'drop', direction: "left" },
                    hide: "slide",
                    modal: true,
                    resizable: false,
                    title: "Error",
                    zIndex: 1200,
                    draggable: false,
                    buttons: { "Okay": function () { $(this).dialog("close"); } }
                });
            }
        });
    },
    getFilters: function () {
        var response = function (result) {
            dsMethods = $create(Sys.UI.DataView, {}, {}, {}, $get("imageEffectType"));
            dsMethods.set_data(result.Data);
            $("#imageEffectType").change(ImageEdit.showEffectOptions);

        };

        Aurora.Manage.Service.AuroraWS.GetImageEffects(response, onError);
    },
    executeFilter: function () {
        var response = function (result) {
            var imageName = $("#previewImage").attr("src").toString();
           
            if ($("#previewImage").attr("ref")) {
                imageName.replace(time, "");
            }

            time = "?" + new Date().getMilliseconds();
            $("#previewImage").attr("ref", time);
            $("#previewImage").attr("src", imageName + $("#previewImage").attr("ref"));
        };

        var imgPath = $("#previewImage").attr("src");

        
        Aurora.Manage.Service.AuroraWS.ApplyImageEffects($("#imageEffectType").val(), imgPath, [100], response, onError);
    },
    showEffectOptions: function () {

    }
};


$(
    function () {
        ImageEdit.init();
        $(document.body).css("background", "none");
    }
);