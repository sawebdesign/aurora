﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShortCuts.ascx.cs" Inherits="Aurora.Manage.UserControls.ShortCuts" %>
<!-- Begin shortcut menu -->
			<ul id="shortcut">
    			<li>
    			  <a href="modal_window.html" id="shortcut_home" title="Click me to open modal window">
				    <img src="../Styles/images/shortcut/home.png" alt="home"/><br/>
				    <strong>Home</strong>
				  </a>
				</li>
    			<li>
    			  <a href="modal_window.html" title="Click me to open modal window">
				    <img src="../Styles/images/shortcut/calendar.png" alt="calendar"/><br/>
				    <strong>Events</strong>
				  </a>
				</li>
    			<li>
    			  <a href="modal_window.html" title="Click me to open modal window">
				    <img src="../Styles/images/shortcut/stats.png" alt="stats"/><br/>
				    <strong>Stats</strong>
				  </a>
				</li>
				<li>
    			  <a href="modal_window.html" title="Click me to open modal window">
				    <img src="../Styles/images/shortcut/setting.png" alt="setting"/><br/>
				    <strong>Setting</strong>
				  </a>
				</li>
				<li>
    			  <a href="modal_window.html" id="shortcut_contacts" title="Click me to open modal window">
				    <img src="../Styles/images/shortcut/contacts.png" alt="contacts"/><br/>
				    <strong>Contacts</strong>
				  </a>
				</li>
				<li>
    			  <a href="modal_window.html" id="shortcut_posts" title="New blog entries">
				    <img src="../Styles/images/shortcut/posts.png" alt="posts"/><br/>
				    <strong>Posts</strong>
				  </a>
				</li>
  			</ul>
			<!-- End shortcut menu -->
            <!-- Begin shortcut notification -->
			<div id="shortcut_notifications">
				<span class="notification" rel="shortcut_home">10</span>
				<span class="notification" rel="shortcut_contacts">5</span>
				<span class="notification" rel="shortcut_posts">1</span>
			</div>
			<!-- End shortcut noficaton -->