<%@ Control Language="vb" AutoEventWireup="false" Codebehind="eezyImage.ascx.vb" Inherits="eezyimager.eezyImage" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language="JavaScript" src="../Scripts/dynlayer.js"></script>
<script language="JavaScript">

    onload = init
    function init() {
        mylayer = new DynLayer("mylayerDiv")

        mylayer.elm.onmousedown = layerDownHandler
        mylayer.elm.onmouseup = layerUpHandler
        mylayer.elm.onmousemove = layerMoveHandler
        if (is.ns) mylayer.elm.captureEvents(Event.MOUSEDOWN | Event.MOUSEMOVE)
		var imageEle =  document.getElementById("EezyImage1_tmpImg");
		if(imageEle){
		var sourceImg = imageEle.src.toString().replace("http://"+document.domain+"/","http://"+document.domain+"/Cli");
		imageEle.src = sourceImg;
		}
		
		
    }

    function layerDownHandler(e) {
        var x = (is.ns) ? e.layerX : event.offsetX
        var y = (is.ns) ? e.layerY : event.offsetY
        document.getElementById("Form1").x.value = x
        document.getElementById("Form1").y.value = y
        document.getElementById("Form1").xD.value = x
        document.getElementById("Form1").yD.value = y
        return false
    }

    function layerUpHandler(e) {
        var x = (is.ns) ? e.layerX : event.offsetX
        var y = (is.ns) ? e.layerY : event.offsetY
        document.getElementById("Form1").xU.value = x
        document.getElementById("Form1").yU.value = y
        return false
    }

    function layerMoveHandler(e) {
        var x = (is.ns) ? e.layerX : event.offsetX
        var y = (is.ns) ? e.layerY : event.offsetY
        document.getElementById("Form1").x.value = x
        document.getElementById("Form1").y.value = y
        return false
    }


    function dotag(tagclass, tagstyle) {
        var i
        var l = document.all.tags("TR").length;
        for (i = 0; i < l; i++) {
            if (document.all.tags("TR")(i).className == tagclass) {
                document.all.tags("TR")(i).style.cssText = tagstyle;
            }
        }
    }

</script>
<style text="text/css">
#mylayerDiv { }
</style>
<table border="0">
	<tr>
		<td><b>Select a .jpg file to upload for
				<% =Request("itype") %>
				: </b>
			<br>
			<input type="file" id="txtUpload" name="txtUpload" runat="server" class="text" size="50"></td>
	</tr>
	<tr>
		<td align="center"><input type="submit" value="Upload" onclick="document.title='Uploading..';" class="button" id="btnUpload" name="btnUpload" runat="server">&nbsp;
			<input type="button" name="skip" id="skip" value="skip" class="button" runat="server"></td>
	</tr>
</table>
<BR>
<BR>
<asp:label id="lblResults" Visible="false" runat="server" /><BR>
<BR>
<table cellpadding="0" cellspacing="0">
	<tr class="spaz" onmouseover="dotag('spaz', 'display:block'); dotag('space', 'display:none')"
		onmouseout="dotag('spaz', 'display:none'); dotag('space', 'display:block')">
		<td class="RTD"><img src="../Styles/images/clear.gif" width="25" height="21"></td>
		<td class="TRTD" bgcolor="#ebebe4">&nbsp;Width&nbsp;:&nbsp;<input name="newWidth" size="3" class="text" value='<% =iWidth %>'>&nbsp;&nbsp;</td>
	</tr>
	<tr class="space">
		<td><img src="../Styles/images/clear.gif" width="25" height="21"></td>
		<td>&nbsp;</td>
	</tr>
</table>
<table border="0" class="specTD" cellpadding="0" cellspacing="0" width="50%" id="table1">
	<tr>
		<td class="BTD" bgcolor="#ebebe4">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<input type="image" src="../Styles/images/ico_save_off.gif" value="Save" name="save" id="save" runat="server"
							onmouseover="dotag('spaz', 'display:none'); dotag('space', 'display:block');">
						<%--<input type="image" src="../Styles/images/ico_ImageSize_off.gif" value="Resize" name="resize" id="resize"
							runat="server" title="Resize" onmouseover="dotag('spaz', 'display:block'); dotag('space', 'display:none');">--%>
						<!--<input type="image" src="../Styles/images/ico_crop_off.gif" value="Crop" name="crop" id="crop" runat="server" title="Crop" onmouseover="dotag('spaz', 'display:none'); dotag('space', 'display:block');">-->
						<input type="image" src="../Styles/images/ico_delete_off.gif" value="Delete" name="delete" id="delete"
							runat="server" title="Delete">
					</td>
					<td width="1%"><!--<input type="image" src="../Styles/images/ico_close_off.gif" value="Close" name="close" id="close" runat="server" title="Close">--></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="BTD"><div id="mylayerDiv"><asp:Image id="tmpImg" runat="server"></asp:Image></div>
		</td>
	</tr>
	<tr bgcolor="#ebebe4">
		<td>
		<table cellpadding=2 cellspacing="0" width="100%">
			    <tr>
			     <td>&nbsp;X&nbsp;:&nbsp;<input id="x" type="text" name="x" size="2" value="0" class="xy"></td>
			     <td>&nbsp;Y&nbsp;:&nbsp;<input id="y" type="text" name="y" size="2" value="0" class="xy"></td>
			   </tr></tr>
</table>
</TD></TR></TABLE> <input type="hidden" name="xD" size="2" value="0"><input type="hidden" name="yD" size="2" value="0">
<input type="hidden" name="xU" size="2" value="0"><input type="hidden" name="yU" size="2" value="0">
<BLOCKQUOTE></BLOCKQUOTE>
<script language="javascript">
    dotag('spaz', 'display:none');
</script>
