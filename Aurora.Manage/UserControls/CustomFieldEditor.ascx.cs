﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aurora.Manage.UserControls {
    /// <summary>
    /// Manages custom fields on any module (user defined/system defined) within the Aurora System. 
    /// </summary>
    public partial class CustomFieldEditor : System.Web.UI.UserControl {
        #region Public Properties
        //Used for system modules.
        public string ModuleName { get; set; }
        //Used for user defined modules.
        public string CustomModuleID { get; set; }
        //The save button to attach to
        public string SaveButtonID { get; set; }
        #endregion

        #region Private Properties
        private long moduleID;
        private long customModuleID;
        #endregion

        protected void Page_Load(object sender, EventArgs e) {
           

            if (!string.IsNullOrEmpty(SaveButtonID))
            {
                //bind to save button
                Page.ClientScript.RegisterStartupScript(GetType(), "attach", "$(\"#" + SaveButtonID + "\").bind(\"click\",customFields.saveFields);", true);
            }

            //set text box values
            hiddenCustomModuleID.Value = CustomModuleID;
            hiddenModuleName.Value = ModuleName;

            
        }
    }
}