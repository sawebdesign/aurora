﻿/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />
/// <reference path="../Scripts/MicrosoftAjax.debug.js" />
/// <reference path="../Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />

//#region File Viewer Control

var fileViewer = {
    fileStruct: null,
    directoryStruct: null,
    createUploader: function () {
        var uploader = new qq.FileUploader({
            // pass the dom node (ex. $(selector)[0] for jQuery users)
            element: document.getElementById('file-uploader'),
            // path to server-side upload script
            action: '/ToolPages/FileUpload.ashx',
            sizeLimit: 20971520,
            onSubmit: function (id, fileName) {
                uploader.setParams({
                    UploadType: "File",
                    ID: $("#fileID").val(),
                    Path: String.format("~/ClientData/{0}/Uploads/{1}/", client.replace("~/ClientData", ""), $("#CurrentPath").val())
                });
            },
            onComplete: function (id, fileName, responseJSON) {
                if (responseJSON.success == true) {
                    fileViewer.fileListRefresh();
                    DisplayMessage("Messages", "success", "File uploaded", 7000);

                }
            },
            showMessage: function (message) {
                $("<div>File Upload Failed</div>").dialog({
                    show: { effect: 'drop', direction: "left" },
                    hide: "slide",
                    modal: true,
                    resizable: false,
                    title: "Error",
                    zIndex: 1200,
                    draggable: false,
                    buttons: { "Okay": function () { $(this).dialog("close"); } }
                });
            }
        });
    },
    getUsage: function () {
        var response = function (result) {
            var total = Number(result.Data);

            if (total > 0) {
                $("#Usage")
                .removeClass("sys-template")
                .html(String.format("Usage {0}", fileViewer.setSize(result.Data)));
            }
        };

        Aurora.Manage.Service.AuroraWS.GetFolderSize($("#CurrentPath").val(), response, onError);
    },
    setSize: function (size) {
        var unit = "MB"
        if (!isNaN(size)) {
            if ((size / 1024) > 1024) {
                size = (size / 1048576).toFixed(2);
            }
            else {
                size = (size / 1024).toFixed(2);
                unit = "KB";
            }
        }
        return String.format("{0} {1}", size, unit);
    },
    getFolders: function () {
        var response = function (result) {
            if (result.Count > 0) {
                GlobalVariables.BindItemToDataSet("directoryView", result.Data, true);
            }
            else {
                $("<div>No folders found.</div>").insertBefore("#directoryView");
            }
        };
        Aurora.Manage.Service.AuroraWS.GetFolders($("#CurrentPath").val(), response, onError);
    },
    getFiles: function (path) {
        var response = function (result) {
        	if (result.Count > 0) {
                GlobalVariables.BindItemToDataSet("filesView", result.Data, true);

                fileViewer.bindEvents();
                gallery.getAlbums();
            }
            else {
                $("#filesView").addClass("sys-template");
                $("<div>No files found.</div>").insertBefore("#filesView");
            }
        };
        if (path) {
            Aurora.Manage.Service.AuroraWS.GetFiles(path, response, onError);
        }
        else {
            Aurora.Manage.Service.AuroraWS.GetFiles($("#CurrentPath").val(), response, onError);
        }
    },
    fileListRefresh: function () {
        if (fileViewRefresh) {
            fileViewRefresh();
        }
    },
    openFile: function (e) {

        var currentFile = $(e.srcElement || e.currentTarget).attr("path");
        currentFile = currentFile.substr(currentFile.indexOf("/CLIENTDATA"))
        $.fancybox({ href: currentFile+"?"+new Date().getMilliseconds(), type: 'iframe', width: '80%', height: '80%' });
    },
    deleteFile: function (e) {
        e.preventDefault();
        var response = function (result) {
            if (result.Result) {
                fileViewer.fileListRefresh();
                DisplayMessage("Messages", "warn", "File removed", 7000);
            }
        };

        $("<div>Do you wish to delete this file?</div>").dialog({
            show: { effect: 'drop', direction: "left" },
            hide: "slide",
            modal: true,
            resizable: false,
            title: "Add To Album",
            zIndex: 1200,
            draggable: false,
            buttons: {
                "Cancel": function () { $(this).dialog("close"); },
                "Okay": function () { Aurora.Manage.Service.AuroraWS.FileFolderAED($(e.srcElement || e.currentTarget).attr("file"), "", 0, response, onError); $(this).dialog("close"); }
            }
        });


    },
    setPrimary: function (e) {
    	var response = function (result) {
    		debugger;
    		if (result.Count > 0) {
    			$('<div>File was successfully set the Primary Image for this member</div>').dialog({
    				modal: true,
    				buttons: {
    					"OK": function () {
    						$(this).dialog('close');
    					}
    				}
    			});
    		} else {
    			$('<div>We were unable to set this to be the Primary Image for this member</div>').dialog({
    				modal: true,
    				buttons: {
    					"OK": function () {
    						$(this).dialog('close');
    					}
    				}
    			});
    		}
    		
    	}

    	var currentFile = $(e.currentTarget).attr("dataid");
    	currentFile = currentFile.substr(currentFile.indexOf("/CLIENTDATA"));
    	Aurora.Manage.Service.AuroraWS.SetPrimaryImage(currentFile, $("#fileID").val(), response, onError);
    	return false;
    	
    },
    bindEvents: function () {
        //bind move to gallery
        $(".fileFunctions img[alt='Add To Album']")
        .unbind("click.fileFunctions")
        .bind("click.fileFunctions", gallery.addToAlbum);

        $(".fileFunctions img[alt='delete']")
        .unbind("click.fileFunctionsDelete")
        .bind("click.fileFunctionsDelete", fileViewer.deleteFile);

        $('.fileFunctions a[title="Make Primary"]').unbind('click.fileFunctionsPrimary').bind('click.fileFunctionsPrimary', fileViewer.setPrimary);

        $("#filesView li").dblclick(fileViewer.openFile);


    }
};

var gallery = {
    currentFile: "",
    albumsDS:null,
    addToAlbum: function (e) {
        $("#ImageOrder").val("-1");
        if ($("#fldegg_ref").length > 0 && $("#fldegg_ref").val() === "") {
            $("<div>Please enter a donor reference number for this donor.</div>").dialog({
                show: { effect: 'drop', direction: "left" },
                hide: "slide",
                modal: true,
                resizable: false,
                title: "Error",
                zIndex: 1200,
                draggable: false,
                width: 400,
                height: 230,
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
            return false;
        }

        gallery.currentFile = $(e.srcElement).parent().attr("file");
        $("#ImageDescription,#ImageName").html($("#fldegg_ref").val());
        $("#addToGallery").dialog({
            show: { effect: 'drop', direction: "left" },
            hide: "slide",
            modal: true,
            resizable: false,
            title: "Add To Album",
            zIndex: 1200,
            draggable: false,
            width: 400,
            height: 230,
            buttons: { "Cancel": function () { $(this).dialog("close"); }, "Save": gallery.addToAlbumSave }
        });

        return false;
    },
    addToAlbumSave: function () {
                
        $(this).dialog("close")
        $(".content").block({ message: null });
        var saveSuccess = function (result) {
            $(this).dialog("close");
            var fileMoved = function (resultFile) {
                var imageResized = function (result) {
                    $(".content").unblock();
                    if (result.Result) {
                        $("<div>Item Added to Album</div>").dialog({
                            show: { effect: 'drop', direction: "left" },
                            hide: "slide",
                            modal: true,
                            resizable: false,
                            title: "Added To Album",
                            zIndex: 1200,
                            draggable: false,
                            buttons: { "Okay": function () { $(this).dialog("close"); } }
                        });
                        
                    }
                    else {
                        $("<div>An error occured whilst creating a thumbnail,support has been notified.</div>").dialog({
                            show: { effect: 'drop', direction: "left" },
                            hide: "slide",
                            modal: true,
                            resizable: false,
                            title: "Error",
                            zIndex: 1200,
                            draggable: false,
                            buttons: { "Okay": function () { $(this).dialog("close"); } }
                        });
                    }
                };
                Aurora.Manage.Service.AuroraWS.ResizeGalleryImage(result.Data, imageResized, onError);
            };
            $("#addToGallery").dialog('close');
            Aurora.Manage.Service.AuroraWS.FileFolderAED(gallery.currentFile, String.format("gallery_01_{0}.jpg", result.Data), 2, fileMoved, onError);

        };

        var galleryEntity = new Object();
        galleryEntity.ID = -1;
        galleryEntity.CategoryID = Number($("#Albums").val());
        galleryEntity.Name = $("#ImageName").html();
        galleryEntity.Description = $("#ImageDescription").html();
        
        galleryEntity.Link = $("#ImageLink").val();
        galleryEntity.InsertedBy = 0;
        galleryEntity.InsertedOn = new Date();
        galleryEntity.MediaName = "";
        galleryEntity.MediaType = ".jpg";
        galleryEntity.GalleryIndex = $("#ImageOrder").val() != "" ? $("#ImageOrder").val() : -1 ;

        Aurora.Manage.Service.AuroraWS.UpdateGallery(galleryEntity, saveSuccess, onError);

    },
    getAlbums: function () {
        $("#ImageOrder").ForceNumericOnly();
        var response = function (result) {
            if (!gallery.albumsDS) {
                gallery.albumsDS = GlobalVariables.BindItemToDataSet("Albums", result.Data, true);
            }
        };

        Aurora.Manage.Service.AuroraWS.GetCategories(0, 0, "Images", 5, 0, response, onError);
    }
};

//init
$(function () {
    fileViewer.getUsage();
    fileViewer.getFolders();
    fileViewer.getFiles();
    fileViewer.createUploader();
});
//#endregion