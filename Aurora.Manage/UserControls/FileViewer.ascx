﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileViewer.ascx.cs"
    Inherits="Aurora.Manage.UserControls.FileViewer" %>
<script src="/UserControls/FileViewer.ascx.js" type="text/javascript"></script>

<input type="hidden" value="<%=CurrentPath%>" id="CurrentPath" />
<input type="hidden" value="<%=fileID%>" id="fileID" />
<div class="twocolumn">
    <div class="column_left" style="min-width: 400px;">
        <div class="header">
            <span>Files/Folders</span><sub class="sys-template" id="Usage" style="float: right">Usage
                {Size}</sub></div>
        <br class="clear" />
        <div class="content">
            <div class="navigation_pane" runat="server" id="navigationPane" style="width: 30%;">
                <%-- <span>Click a folder to view its files</span>--%>
                <ul class="sys-template" id="directoryView" sys:attach="dataview">
                    <li id="{{Name}}" path="{{FullName}}" onclick="fileViewer.getFiles($(this).attr('path'));">
                        {{Name}}</li>
                </ul>
            </div>
            <div class="navigation_pane" runat="server" id="filesPane" style="width: 60%;">
                <span>Double click to preview file</span>
                <ul class="sys-template" id="filesView" sys:attach="dataview">
                    <li type="{{Extension}}" path="{{RelativePath}}">
                        <div class="fileText" path="{{RelativePath}}">
                            {{Name}}</div>
                        <div class="fileFunctions" path="{{RelativePath}}">
                            <a href="javascript:void(0);" alt="Delete">
                                <img src="/Styles/images/icon_delete.png" file="{{FullName}}" alt="delete" />
                            </a><a href="javascript:void(0);" file="{{FullName}}" path="{{FullName}}" class="{{FullName.toString().toUpperCase().indexOf('.JPG') > -1 ? '' : 'sys-template' }}">
                                <img alt="Add To Album" src="/Styles/images/image_add.png" /></a>
                            <% if (primary == true) { %>
                            <a href="#" dataid="{{RelativePath}}" title="Make Primary"> 
                                <img alt="Make Primary" src="/Styles/images/icon_online.png"/>
                            </a>
                            <% } %>
                        </div>
                        <div class="clear">
                        </div>
                    </li>
                </ul>
            </div>
            <br class="clear" />
        </div>
    </div>
    <div class="column_right">
        <div class="header">
            <span>Upload A File</span>
        </div>
        <br class="clear" />
        <div class="content">
            <div id="file-uploader">
               
            </div>
        </div>
    </div>
</div>
<br class="clear" />
<%--Gallery Add Form--%>
<div style="display: none">
    <div id="addToGallery" style="width: 100%;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    Album:
                </td>
                <td>
                    <select id="Albums" class="sys-template">
                        <option value="{{ID}}">{{Name}}</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Name:
                </td>
                <td id="ImageName">
                </td>
            </tr>
            <tr>
                <td>
                    Description:
                </td>
                <td id="ImageDescription">
                </td>
            </tr>
            <tr>
                <td>
                    Order:
                </td>
                <td>
                    <input type="text" id="ImageOrder" valtype="required" value="-1" />
                </td>
            </tr>
            <tr>
                <td>
                    Link:
                </td>
                <td>
                    <input type="text" id="ImageLink" />
                </td>
            </tr>
        </table>
    </div>
</div>
