﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using System.IO;

namespace Aurora.Manage.UserControls
{
    public partial class ImageEdit : System.Web.UI.UserControl
    {
        public string lastModifiedDate = DateTime.Now.ToString();
        public string currentFile, tempFile;
        protected void Page_Load(object sender, EventArgs e)
        {
            fileID.Value = DateTime.Now.Millisecond.ToString();
            CurrentPath.Value = SessionManager.ClientBasePath;
            FileName.Value = Request["FileName"];
            UseHeight.Value = Request["UseHeight"];

            if (Request["File"] != null)
            {
                currentFile = Request["File"];
                //validation 

                if (!File.Exists(currentFile))
                {
                    //show message
                }
            }
        }
    }
}