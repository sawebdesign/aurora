﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Configuration.Provider;
using System.Text;
using System.Text.RegularExpressions;
using AutoMapper;

namespace Aurora.Manage.UserControls {
    public partial class menu : System.Web.UI.UserControl {

        private int HowDeepToScan = 100;
        private static StringBuilder mnuOutput = new StringBuilder();
        private static Aurora.Custom.Data.AuroraEntities service = new Aurora.Custom.Data.AuroraEntities();

        //get the entire sitemap table
        private static List<Aurora.Custom.Data.SiteMap> resultsSite = (from site in service.SiteMap
                                                                      where site.OrderID >= 0
                                                                      select site).ToList();

        private static List<Aurora.Custom.SiteMapWithRole> resultsSiteMap;

        //get all role site map roles
        private static List<Aurora.Custom.Data.SiteMapRole> siteRoles = (from site in service.SiteMap
                                                                   join sitemaprole in service.SiteMapRole on site.SiteMapID equals sitemaprole.SiteMapID
                                                                   where site.OrderID >= 0
                                                                   select sitemaprole).ToList();
		private static List<Aurora.Custom.Data.Roles> rolesList = service.Roles.ToList();

        protected void Page_Load(object sender, EventArgs e) {

            Mapper.CreateMap<Aurora.Custom.Data.SiteMap, Aurora.Custom.SiteMapWithRole>();
            resultsSiteMap = Mapper.Map<List<Aurora.Custom.Data.SiteMap>, List<Aurora.Custom.SiteMapWithRole>>(resultsSite);


            if (!IsPostBack) {

                if (ConfigurationManager.AppSettings["menuRootId"] == null)
                    throw new ProviderException("The root menu item has not been configured.");

                int rootMenuId = Aurora.Custom.Utils.ConvertDBNull<int>(ConfigurationManager.AppSettings["menuRootId"], 0);
                mnuOutput.Clear();
                mnuOutput.Append("<ul id='main-nav'>");
                buildMap(rootMenuId, 0);
                mnuOutput.Append("</ul>");
                lblMenu.Text = mnuOutput.ToString().Replace("<ul></ul>","");

            }
        }

        public string buildMap(int MenuId, int recursionLvl) {

            if (recursionLvl <= HowDeepToScan) {
                string currentPageURL = Request.ServerVariables["SCRIPT_NAME"].ToString();
                var result = resultsSiteMap.Where(x => x.Parent == MenuId).OrderBy(x => x.OrderID);
                foreach (Aurora.Custom.SiteMapWithRole sitemap in result) {
                    sitemap.roles = (from sr in siteRoles
                                     join roles in rolesList on sr.RoleID equals roles.ID
                                     where sr.SiteMapID == sitemap.SiteMapID
                                     select roles).ToList();

                    if (HasAccessToMenuItem(sitemap.roles)) {
                        string currentUrl = Aurora.Custom.Utils.ConvertDBNull<string>(sitemap.Url, "").ToUpper();
                        string sitemapCurrent = string.Empty;
                        if (currentUrl != "") {
                            sitemapCurrent = (currentUrl.Contains(currentPageURL.ToUpper())) ? " current " : "";
                        }

                        //match all {custom} tags to values
                        if (!string.IsNullOrEmpty(sitemap.Url)) {
                            Regex rx = new Regex("{(.|\n)*?}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                            MatchCollection matches = rx.Matches(sitemap.Url);
                            foreach (Match match in matches) {
                                //strip out the match and replace it with the property from the object
                                string tmpAttName = match.ToString().Replace("{", "").Replace("}", "");

                                switch (tmpAttName.ToUpper()) {
                                    case "CLIENTSITEID":
                                        sitemap.Url = sitemap.Url.Replace(match.ToString(), Aurora.Custom.SessionManager.ClientSiteID.ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        mnuOutput.Append("<li>");
                        mnuOutput.Append("<a href='" + sitemap.Url + "' class='" + sitemap.menuClass + sitemapCurrent + "'  title='" + sitemap.Description + "' >" + sitemap.Title + "</a>");

                        if (recursionLvl == 0) {
                            mnuOutput.Append("<ul>");
                        }
                        buildMap(sitemap.SiteMapID, recursionLvl + 1);
                        if (recursionLvl == 0) {
                            mnuOutput.Append("</ul>");
                        }
                        mnuOutput.Append("</li>");
                    }
                }
            } else {
                mnuOutput.Append("</li>");
            }
            return mnuOutput.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MenuRoles"></param>
        /// <returns></returns>
        private bool HasAccessToMenuItem(List<Aurora.Custom.Data.Roles> MenuRoles) {
            bool hasAccess = false;
            

            if (MenuRoles.Count == 0) {
                return false;
            }

            var access = (from ur in Aurora.Custom.SessionManager.UserRoles
                         join mr in MenuRoles on ur.ID equals mr.ID
                         select ur).FirstOrDefault();

            if (access != null) {
                var exclude = (from ex in Aurora.Custom.SessionManager.SiteExcludedRoles
                              join mr in MenuRoles on ex.Value equals mr.ID
                              select ex).FirstOrDefault();
                if (exclude != null) {
                    hasAccess = false;
                } else {
                    hasAccess = true;
                }
            }

            return hasAccess;
        }


    }
}