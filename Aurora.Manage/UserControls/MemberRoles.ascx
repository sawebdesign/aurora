﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberRoles.ascx.cs"
    Inherits="Aurora.Manage.UserControls.MemberRoles" ClientIDMode="Predictable" %>
<script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
<script src="/UserControls/MemberRoles.ascx.js" type="text/javascript"></script>
<div id="RolesContainer" runat="server" style="display:none;height:300px;">
    <div class="">
        <h2>Roles</h2>
        <p>To ensure that this content page is only seen by registered users, please tick the relevant group of users in the list below.<br/><br/></p>
    </div>
    <div>
        <div style="width: 200px; float: left;">
            <select id="MemberRolesSelect" runat="server" multiple="true">
            </select>
        </div>
        <div style="float:left;">
            <ul id="MemberRoles1_MemberRolesSelect_selectedItems">
            </ul>
        </div>
    </div>
</div>
<div style="display:none;" id="NoRoles" runat="server">
    <div id="Messages" class="alert_warn">
        <p>
            <img src="/Styles/images/icon_warning.png" class="mid_align" alt="info" />
              You currently do not have any roles setup. Please click <a href="/MemberManagement/RolesAED.aspx">
                  here</a> to create roles to manage. 
            </p>
    </div>
</div>
<input type="hidden" value="" id="SaveBtnID" runat="server"/>
<input type="hidden" value="" id="IDProperty" runat="server"/>
<input type="hidden" value="" id="CurrentModuleID" runat="server" />
<input type="hidden" id="CurrentModuleName" value="PageDetails" runat="server" />
