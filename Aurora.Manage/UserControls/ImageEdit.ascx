﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageEdit.ascx.cs" Inherits="Aurora.Manage.UserControls.ImageEdit" %>
<%@ Import Namespace="Aurora.Custom" %>
<link rel="Stylesheet" href="/Styles/black/screen.css" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-1.5.2.js?x="></script>
<script src="/Scripts/jquery-ui.custom.js?x=" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/jquery.img.preload.js?x="></script>
<script type="text/javascript" src="/Scripts/hint.js?x="></script>
<script type="text/javascript" src="/Scripts/visualize/jquery.visualize.js?x="></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox-1.3.0.js?x="></script>
<script type="text/javascript" src="/Scripts/jquery.tipsy.js?x="></script>
<script type="text/javascript" src="/Scripts/custom_black.js?x="></script>
<script type="text/javascript" src="/Scripts/jquery.blockUI.js?x="></script>
<script type="text/javascript" src="/Scripts/jquery.pagination.js?x="></script>
<script type="text/javascript" src="/Scripts/Utils.js?x="></script>
<script type="text/javascript" src="/Scripts/MicrosoftAjax.debug.js?x="></script>
<script type="text/javascript" src="/Scripts/MicrosoftAjaxTemplates.debug.js?x="></script>
<script type="text/javascript" src="/Scripts/generated/Aurora.Custom.js?x="></script>
<script type="text/javascript" src="/Service/AuroraWS.asmx/js"></script>
<script src="/Scripts/jquery-cookie.js" type="text/javascript"></script>
<link href="/Scripts/file-uploader/fileuploader.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/file-uploader/fileuploader.js" type="text/javascript"></script>
<script src="/UserControls/ImageEdit.ascx.js" type="text/javascript"></script>
<input type="hidden" id="fileID" runat="server" clientidmode="Static" />
<input type="hidden" id="FileName" runat="server" clientidmode="Static" />
<input type="hidden" id="UseHeight" runat="server" clientidmode="Static" />
<input type="hidden" id="CurrentPath" clientidmode="Static" runat="server" />
<div class="onecolumn" style="height: 400px; width: 99%; margin: 0px 0px 0px 10px;">
    <div class="header">
        <h2>
            Image Editor</h2>
    </div>
    <div class="content">
        <div style="width: 100%;">
            <div class="functions">
                <span><b>Image Effect</b></span>&nbsp;
                <select id="imageEffectType" class="sys-template" sys:attach="dataview">
                    <option >{{Name}}</option>
                </select>
                <input type="button" value="Apply" onclick="ImageEdit.executeFilter();"/>
            </div>
            <div class="preview">
                <img src="" id="previewImage" style="display: none" width="100%" />
                <div class="uploadImage">
                    Please click the upload button to upload an image for editing.
                    <br />
                    <div id="file-uploader" style="margin-left: 45%;">
                    </div>
                    <br />
                    <br />
                </div>
            </div>
            <br class="clear" />
        </div>
    </div>
    <br class="clear" />
</div>
