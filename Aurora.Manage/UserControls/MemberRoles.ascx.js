﻿/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />
/// <reference path="../Scripts/MicrosoftAjax.debug.js" />
/// <reference path="../Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />

var saveBtn;
var idProperty;
$(
    function () {
        var allow = false;
        $(".nav-top-item").each(
            function (indx, Elme) {
                if (Elme.innerHTML == "Members") {
                    allow = true;
                }
            }
        );

        if (!allow) {
            return;
        }

        $(".switch").css("width", "260");
        $("#tab3").show();

        //wire up to save button
        
        saveBtn = $("#MemberRoles1_SaveBtnID").val() === "0" ? "btnSave" : $("#MemberRoles1_SaveBtnID").val();
        idProperty = $("#MemberRoles1_IDProperty").val() === "0" ? $("#ID").val() : $("#MemberRoles1_IDProperty").val();


        $("#" + saveBtn).live('click', function () {
            AddItemToRoles();
        });
        $("#btnSaveTop").live('click', function () {
            AddItemToRoles();
        });
        GetCurrentRoles();
    }
);

function AddItemToRoles() {
    //retrieve id from form
    var response = function (result) {
        if (result.Result) {

        }
    };
    var deleteAll = false;
    var Roles = [];

    //build roles list
    $("#MemberRoles1_MemberRolesSelect_selectedItems").children('li').each(function (index, domEle) {
        var newRole = new AuroraEntities.RoleModule();

        // domEle == this
        if (parseInt($(this).attr("id").toString().replace("MemberRoles1_MemberRolesSelect_", ""))) {
            newRole.ModuleID = $("#MemberRoles1_CurrentModuleID").val();
            newRole.ModuleItemID = Number(idProperty);
            newRole.RoleID = Number($(this).attr("id").toString().replace("MemberRoles1_MemberRolesSelect_", ""));
            newRole.ModuleName = $("#MemberRoles1_CurrentModuleName").val();
            newRole.InsertedOn = new Date();
            newRole.InsertedBy = 0;
            Roles.push(newRole);
        }

    });

    if (Roles.length == 0) {
        deleteAll = true;
        $("input[name='MemberRoles1_MemberRolesSelect[]']").each(function (index, domEle) {
            var newRole = new AuroraEntities.RoleModule();

            // domEle == this
            if (parseInt($(this).attr("id").toString().replace("MemberRoles1_MemberRolesSelect_", ""))) {
                newRole.ModuleID = $("#MemberRoles1_CurrentModuleID").val();
                newRole.ModuleItemID = Number(idProperty);
                newRole.RoleID = Number($(this).attr("id").toString().replace("MemberRoles1_MemberRolesSelect_", ""));
                newRole.ModuleName = $("#MemberRoles1_CurrentModuleName").val();
                newRole.InsertedOn = new Date();
                newRole.InsertedBy = 0;
                Roles.push(newRole);
            }

        });
    }

    Aurora.Manage.Service.AuroraWS.AddRolesToItems(Roles, deleteAll, response, onError);
}


function GetCurrentRoles() {
    var response = function (result) {
        if (result.Result) {
            $('#MemberRoles1_MemberRolesSelect option').each(
                function () {
                    currentElement = $(this);
                    if (findByInArray(result.Data, "RoleID", currentElement.attr("value"))) {
                        currentElement.attr("selected", "selected");
                    }
                }
            );
        }


        $('#MemberRoles1_MemberRolesSelect').toChecklist({
            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: true,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });
    };

    Aurora.Manage.Service.AuroraWS.LoadRolesModules(Number(idProperty), Number($("#MemberRoles1_CurrentModuleID").val()), response, onError);
}