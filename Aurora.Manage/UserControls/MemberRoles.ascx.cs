﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage.UserControls
{
    public partial class MemberRoles : System.Web.UI.UserControl
    {
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        private long? moduleID;
        public string ModuleName { get; set; }
        public string SaveButtonID { get; set; }
        public string IDValueFieldID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //check property
            if (string.IsNullOrEmpty(ModuleName))
            {
                throw new NotImplementedException("ModuleName Property must be set in order for control to work");
            }
            else
            {
                SaveBtnID.Value = Utils.ConvertDBNull(SaveButtonID,"0");
                IDProperty.Value = Utils.ConvertDBNull(IDValueFieldID,"0");

                //Check if client has Roles module
                var hasRoles = from featureModule in entities.Module
                               join clientModule in entities.ClientModule on featureModule.ID equals
                                   clientModule.FeaturesModuleID
                               where featureModule.Name == "MemberManagement"
                               select clientModule;

                if (hasRoles.Count() != 0)
                {
                    //check if module exists and get the ID
                    moduleID = (from featureModules in entities.Module
                                where featureModules.Name == ModuleName
                                select featureModules.ID).SingleOrDefault();

                    if (moduleID == null || moduleID == 0)
                    {
                        throw new KeyNotFoundException(
                            "The module name specified does not relate to any modules found on the system");
                    }

                    CurrentModuleName.Value = ModuleName;
                    CurrentModuleID.Value = moduleID.Value.ToString();
                    //if everything passes then kick off roles acquisition
                    var roles = from userRoles in entities.Role
                                where userRoles.ClientSiteID == SessionManager.ClientSiteID
								&& userRoles.DeletedOn == null
								orderby userRoles.OrderIndex
                                select userRoles;

                    if (roles.Count() > 0)
                    {
                        MemberRolesSelect.DataSource = roles;
                        MemberRolesSelect.DataTextField = "Name";
                        MemberRolesSelect.DataValueField = "ID";
                        MemberRolesSelect.DataBind();
                        RolesContainer.Style.Add("display", "block");
                    }
                    else
                    {
                        NoRoles.Style.Add("display", "block");
                    }
                }


            }
        }
    }
}