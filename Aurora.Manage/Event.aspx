﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="Aurora.Manage.Event" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<%@ Register Src="UserControls/MemberRoles.ascx" TagName="MemberRoles" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Event Details</title>
    <link href="Styles/ui.slider.css" rel="stylesheet" type="text/css" />
    <link href="Styles/ui.all.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/custom_black.js" type="text/javascript"></script>
</head>
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
<script type="text/javascript" src="Event.aspx.js"></script>
<body>
    <form id="form1" runat="server">
    <!-- Begin content -->
    <div id="contentAddEdit">
        <!-- Begin one column window -->
        <div class="onecolumn">
            <div class="header">
                <span>Event</span>
                <div style="width: 180px;" class="switch">
                    <table width="180px" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="button" style="width: 90px;" value="Details" class="left_switch active"
                                        name="tab1" id="tab1">
                                </td>
                                <td>
                                    <input type="button" style="width: 90px;" value="Editor" class="right_switch" name="tab2"
                                        id="tab2">
                                </td>
                                <td>
                                    <input type="button" style="width: 90px; display: none;" value="Security" class="right_switch"
                                        name="tab2" id="tab3">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br class="clear" />
            <div class="tab_content" id="tab1_content">
                <div id="EventDetails" style="padding-left: 15px; width: 100%;">
                    <asp:Image runat="server" ID="Image1" ClientIDMode="Static" Visible="false" />
                    <asp:Image runat="server" ID="Image2" ClientIDMode="Static" Visible="false" />
                    <input type="hidden" id="ID" value="0" runat="server" />
                    <input type="hidden" id="ClientSiteID" value="" runat="server" />
                    <input type="hidden" id="CategoryID" value="" runat="server" />
                    <input type="hidden" id="SchemaID" value="" runat="server" />
                    <input type="hidden" id="InsertedOn" value="" runat="server" />
                    <input type="hidden" id="InsertedBy" value="" runat="server" />
                    <input type="hidden" id="DeletedOn" value="" runat="server" />
                    <input type="hidden" id="DeletedBy" value="" runat="server" />
                    <input type="hidden" id="CustomXML" value="" runat="server" />
                    <table border="1" width="100%" id="templateDetail" style="padding-left: 10px;" sys:attach="dataview">
                        <tr>
                            <td>
                                Title :
                            </td>
                            <td>
                                <input type="text" class="help" mytitle="This is name given to the event" valtype="required"
                                    id="Title" name="x" x="Title" value="" runat="server" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Preview:
                            </td>
                            <td>
                                <input type="text" valtype="required" class="help" mytitle="This text will display some teaser text detailing brief piecies of information regarding the event"
                                    id="Preview" name="preview" x="Preview" value="" runat="server" maxlength="150"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Occurrence :
                            </td>
                            <td>
                                <select type="text" valtype="required" class="help" mytitle="This will set the repeatition of the event"
                                    id="Occurance" name="occurance" x="Occurance" value="" runat="server" onchange="this.value == 'Monthly' ? $('#MonthlyFilter').show():$('#MonthlyFilter').hide(); ">
                                    <option>None</option>
                                    <option>Daily</option>
                                    <option>Weekly</option>
                                    <option>Monthly</option>
                                    <option>Yearly</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="MonthlyFilter" style="display: none;">
                            <td>
                                Monthly Occurence Type:
                            </td>
                            <td>
                                Every
                                <select id="fldWeek" name="fldWeek">
                                    <option value="1">First</option>
                                    <option value="2">Second</option>
                                    <option value="3">Third</option>
                                    <option value="4">Last</option>
                                </select>
                                week on a
                                <select id="fldDays" name="fldDays">
                                    <option value="1">Sunday</option>
                                    <option value="2">Monday</option>
                                    <option value="3">Tuesday</option>
                                    <option value="4">Wednesday</option>
                                    <option value="5">Thursday</option>
                                    <option value="6">Friday</option>
                                    <option value="7">Saturday</option>
                                </select>
                                <b>Or</b>
                                <input id="IgnoreFilter" onclick="ignoreFilter(this);" type="checkbox" value="0" />Ignore
                                filter
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date :
                            </td>
                            <td>
                                <input type="text" valtype="required;regex:date" class="help" mytitle="Indicates the date the event will commence"
                                    id="StartDate" name="startdate" x="Start Date" value="" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Expiry Date :
                            </td>
                            <td>
                                <input type="text" valtype="required;regex:date" class="help" mytitle="Indicates the date the event will expire"
                                    id="EndDate" name="enddate" x="End Date" value="" runat="server" /><input type="checkbox"
                                        id="NeverExpires" value="expires" runat="server" onclick="neverExpire(this);"
                                        class="help" mytitle="Indicates that this event will never expire" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                From Time:
                            </td>
                            <td>
                                <input type="text" runat="server" valtype="required" class="help" mytitle="Indicates the time the event will start."
                                    id="EventStartTime" />
                                &nbsp;To&nbsp;
                                <input type="text" runat="server" valtype="required" class="help" mytitle="Indicates the time the event will end."
                                    id="EventEndTime" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Display Date :
                            </td>
                            <td>
                                <input type="text" valtype="required;regex:date" id="DisplayDate" class="help" mytitle="Indicates the date the event should be allowed to be displayed on the site"
                                    name="displaydate" x="Display Date" value="" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Venue Name:
                            </td>
                            <td>
                                <input type="text" valtype="required" id="Location" class="help" mytitle="Indicates where the event will be held"
                                    name="location" x="Location" runat="server" maxlength="150" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Venue Address/GPS Co-ordinates :
                            </td>
                            <td>
                                <input type="text" id="GeoLatLong" class="help" mytitle="Describes either the full address or the Global Positioning co-ordinates of where the event is to be held"
                                    name="geolatlong" x="GeoLatLong" runat="server" maxlength="250" />
                            </td>
                        </tr>
                        <tr style="visibility: hidden; position: absolute">
                            <td>
                                Allow Reminders:
                            </td>
                            <td>
                                <input type="checkbox" id="AllowReminders" name="AllowReminders" x="AllowReminders"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Image Caption 1:
                            </td>
                            <td>
                                <input type="text" id="ImageCaption1" class="help" mytitle="Text will be accompanied by an image uploaded with the uploader"
                                    runat="server" />
                                <a id="btn_image1" href="#">
                                    <img src="Styles/images/icon_media.png" class="help" mytitle="Click here to upload an image" /></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Image Caption 2:
                            </td>
                            <td>
                                <input type="text" id="ImageCaption2" class="help" mytitle="Text will be accompanied by an image uploaded with the uploader"
                                    runat="server" />
                                <a id="btn_image2" href="#">
                                    <img src="Styles/images/icon_media.png" class="help" mytitle="Click here to upload an image" /></a>
                            </td>
                        </tr>
                        <tr id="CustomData">
                            <td colspan="2" class="fcktable">
                                <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                    <cui:XmlPanel ID="ContentXml" runat="server">
                                    </cui:XmlPanel>
                                </cui:ObjectPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="tab_content hide" id="tab2_content">
                <textarea id="Details" runat="server" style="display: none;" rows="1"></textarea>
                <span id="editorSpan"></span>
            </div>
            <div class="tab_content hide" id="tab3_content">
                <uc1:MemberRoles ID="MemberRoles1" ModuleName="Events" runat="server" />
            </div>
            <!-- End one column window -->
            <!-- End content -->
        </div>
    </div>
    </form>
</body>
</html>
