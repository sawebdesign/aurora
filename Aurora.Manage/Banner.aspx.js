﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />

//#region Variables
var BannerXY = null;
var ButtonXY = null;
//#endregion

//#region onLoad
$(function () {
    DisplayMessage("Messages", "info", "Please fill in banner information", 0);
    scrollToElement("Messages", -20, 2);
    $("#contentAddEdit").unblock();
    $("#ValidFromDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#ValidToDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#MaxImpressions,#MaxClicks,#Weight").ForceNumericOnly();
    image = $("#bannerImage").attr("src") || $("#bannerImage").attr("src");
    //LoadXML
    BannerXY = (GetJSSession("Banner_Size") || "200*200").split("*");
    ButtonXY = (GetJSSession("BannerButton_Size") || "200*200").split("*");
    BottomXY = (GetJSSession("BannerBottom_Size") || "200*200").split("*");

    //TODO Add Identifiers to XY Array and add it to the XY input to assure selected banner is the right one

    //add sizes to dropdown
    count = 0;
    var bannerMainOption = $('<option farmID="4" value="' + BannerXY[0] + ',' + BannerXY[1] + '" bannertype="Main">Banner Main (' + BannerXY[0] + 'X' + BannerXY[1] + ')</option>');
    var bannerButtonOption = $('<option farmID="7" value="' + ButtonXY[0] + ',' + ButtonXY[1] + '" bannertype="Button">Banner Button (' + ButtonXY[0] + 'X' + ButtonXY[1] + ')</option>');
    var bannerBottomOption = $('<option farmID="10" value="' + BottomXY[0] + ',' + BottomXY[1] + '" bannertype="Bottom" >Banner Bottom (' + BottomXY[0] + 'X' + BottomXY[1] + ')</option>');
    $('#XY').append(bannerMainOption)
    .append(bannerButtonOption)
    .append(bannerBottomOption);

    //set Size
    if ($("#BannerID").val() != "") {
        $('#XY').val($("#xsize").val() + "," + $("#ysize").val());
    }

    //get zones
    if ($("#BannerID").val() != "") {
        getZones($("#BannerID").val());
    }
    else {
        getZones(-1);
    }

    //hide irrelavant banners
    if (!GetJSSession("Banner_Size")) {
        $('#XY').find("[bannertype='Main']").remove();
    }

    if (!GetJSSession("BannerButton_Size")) {
        $('#XY').find("[bannertype='Button']").remove();
    }

    if (!GetJSSession("BannerBottom_Size")) {
        $('#XY').find("[bannertype='Bottom']").remove();
    }

    $("#FarmID option").each(
                function () {
                    if ($(this).val() == $("#FarmID").attr("selectId")) {
                        $(this).attr("selected", true);
                    }
                }
    );
    //set select box item
    $("#XY option").each(function () {
        if ($(this).html() === $("#TypeName").val()) {
            $("#XY option").removeAttr("selected");
            $(this).attr("selected", "selected");
        }
    });
    //sets check box value
    expires(false, true);

    //Displays the banner image once mouse hovers over image upload
    $('#btn_image').mouseover(
        function () {
            $("#bannerImage").css("left", ($("#BannerForm").width() * 0.70) + 20);
            $("#bannerImage").css("border", "2px dashed grey");
            $("#bannerImage").css("position", "absolute");
            $("#bannerImage").css("top", "91px");
            $("#bannerImage").fadeIn(500);
            $("#bannerImage").height($("#bannerImage").height() / $("#bannerImage").width * ($("#BannerForm").width() - ($("#BannerForm").width() * 0.70)));
            $("#bannerImage").width($("#BannerForm").width() - ($("#BannerForm").width() * 0.70));

        }
    );


    $('#btn_image').mouseout(
    function () {
        $("#bannerImage").fadeOut(500);
    }
    );

    //hides the page header image
    $("#bannerImage").hide();

    $(".tipsy").remove();

    $("#BannerForm .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
});
//#endregion

//#region Custom Functions
function expires(checkbox,start) {
    if (start) {
        if ($("#ValidToDate").val() == "01/01/2020") {
            $("#Expires").attr("checked", true);
            $("#ValidToDate").attr("disabled", true);
            return;
        }
        else {
            return;
        }
    }
    if (checkbox) {
        $("#ValidToDate").val("01/01/2020");
    }
    if ($("#ValidToDate").val() == "01/01/2020" && checkbox) {
        $("#Expires").attr("checked", true);
        $("#ValidToDate").attr("disabled", true);
    }
    else {
        $("#Expires").removeAttr("checked");
        $("#ValidToDate").removeAttr("disabled");
    }
}

function selectSize() {
    if ($("#XY")[0].selectedIndex == 0) {
        $("#xsize").val(ButtonXY[0]);
        $("#ysize").val(ButtonXY[1]);
    }
    else {
        $("#xsize").val(BannerXY[0]);
        $("#ysize").val(BannerXY[1]);
    }

}
//#region Filter

function showFilter(dd) {
    var filterSelection = dd.value
    switch (filterSelection) {
        case "Page":
            $('#BannerSizesContainer').hide();
          
            Aurora.Manage.Service.AuroraWS.GetAllPageBanners(response, onError);

            break;
        case "BannerType":
            bannerMainOption = null;
            bannerButtonOption = null;
            bannerBottomOption = null;

            BannerXY = (GetJSSession("Banner_Size") || "200*200").split("*");
            ButtonXY = (GetJSSession("BannerButton_Size") || "200*200").split("*");
            BottomXY = (GetJSSession("BannerBottom_Size") || "200*200").split("*");

            //TODO Add Identifiers to XY Array and add it to the XY input to assure selected banner is the right one

            //add sizes to dropdown
            $('#BannerSizesContainer').show();
            count = 0;
            var bannerMainOption = $('<option farmID="4" value="' + BannerXY[0] + ',' + BannerXY[1] + '" bannertype="Main">Banner Main (' + BannerXY[0] + 'X' + BannerXY[1] + ')</option>');
            var bannerButtonOption = $('<option farmID="7" value="' + ButtonXY[0] + ',' + ButtonXY[1] + '" bannertype="Button">Banner Button (' + ButtonXY[0] + 'X' + ButtonXY[1] + ')</option>');
            var bannerBottomOption = $('<option farmID="10" value="' + BottomXY[0] + ',' + BottomXY[1] + '" bannertype="Bottom" >Banner Bottom (' + BottomXY[0] + 'X' + BottomXY[1] + ')</option>');
            $('#BannerSizes').append(bannerMainOption)
    .append(bannerButtonOption)
    .append(bannerBottomOption);

            break;
       
        default:
    }
}

//#endregion
//#endregion