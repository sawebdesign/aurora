﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aurora.Custom;

namespace Aurora.Manage
{
    /// <summary>
    /// Summary description for DonloadFile
    /// </summary>
    public class DonloadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request["filename"] != null && context.Request["encnone"] == null)
            {
                Utils.DownloadFile(context.Request["filename"]);
                return;
            }
         
                context.Response.Redirect(context.Request["filename"]);
         
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}