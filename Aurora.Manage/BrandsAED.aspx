﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="BrandsAED.aspx.cs" Inherits="Aurora.Manage.BrandsAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script language="javascript" src="BrandsAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
<div class="onecolumn">
        <div class="header">
            <span>Product Brands List</span>
              <div style="float: right; margin-top: 10px; margin-right: 10px;">
                        <img dataid="-1" src="Styles/images/add.png" class="help" alt="Create Size" mytitle="Create Size"
                            style="cursor: pointer" dataid="{{ID}}" onclick="getDetail($(this).attr('dataid'));" />
                    </div>
                     <div style="float: left; margin-top: 10px; margin-left: 10px;">
                     <select id="filter">
                        <option selected="selected" value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                     </select>
              </div>
        </div>
        <br class="clear" />
        <div class="content" id="grid">
            <table class="data" width="100%" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th align="left">
                            Name
                        </th>
                        <th align="left">
                            Description
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" sys:attach="dataview" id="template" >
                  <tr>
                        <td>
                        {{Name}}
                        </td>
                        <td>
                        {{Description}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);"  dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" dataid="{{ID}}" onclick="getDetail(this.getAttribute('dataid')); return false;" /></a>
                            <a href="javascript:void(0);"  dataid="{{ID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" onclick="deleteItem(this.getAttribute('dataid')); return false;"/></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="Pagination"></div>
        </div>
    </div>
        <div id="Messages">
        </div>
   <div id="Edit"></div>
</asp:Content>
