﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />

var templateList;
var catList;
var Loaded = false;
var TotalCount = 0;
var CurrentPage = 0;
var newItem = false;
var Global = new GlobalVariables.GlobalVar();

function getDate(date) {
    if (!date) {
        return "";
    } else {
        return formatDateTime(date, "dd/MM/yyyy");
    }
}
function getEventCategory() {
    var lastIndex = $("#EventCat")[0].selectedIndex;
    if ($('#Messages')[0].className != "alert_success")
        $('#Messages').hide();

    var requestCats = function (result) {
        if (result.Data.length != 0) {
            if (catList == null) {
                catList = $create(Sys.UI.DataView, {}, {}, {}, $get("EventCat"));
            }

            catList.set_data(result.Data);
            if (!Loaded) {
                Loaded = true;
                $("#EventCat")[0].selectedIndex = 0;
            }
            else {
                $("#EventCat")[0].selectedIndex = lastIndex;
            }

            getEvents();
        }
        else {
            $("#contentList").hide();
            $("#AddNewCat").show();
            $('div.content').unblock();

        }

    }

    Aurora.Manage.Service.AuroraWS.GetCategories(0, 0, "", 4, 0, requestCats, onError);
}
function getEvents(eventType) {
	eventType = eventType == null ? $('#eventType').val() || "" : eventType;
    $('div.content').block({ message: null });
    var requestEvents = function (result) {
        if (result.Result) {
            $('div.content').show();
            // Add tooltip to edit and delete button

            if (templateList == null) {
                templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            }

            templateList.set_data(result.Data);
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;


            $('div.content').unblock();
            if (result.Count == 0) {
                $('div.content').hide();
                DisplayMessage("Messages", "warn", "No Events Available", false);
            }
            $(".tipper").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });


        }
        else {
            if (!result.Result) {
                $('div.content').hide();
                DisplayMessage("Messages", "Error", "Could not load Event data, please try again shortly", false);
            }
        }
    }
    Aurora.Manage.Service.AuroraWS.GetEvents($("#EventCat").val(), CurrentPage + 1, Global.PageSize, eventType, requestEvents, onError);


}
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getEvents();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getEvents();
    }
    //  $("#contentAddEdit").hide();
    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });

}
function getDetail(anchor) {
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#contentAddEdit").unblock();
            $("#contentAddEdit").show();
            $("#btnSave").show();
            $("#Cancel").show();
        }
    };
    if (anchor == undefined) {
        $("#contentAddEdit").load("Event.aspx?EventId=0&var=" + x.getMilliseconds(), success);
    }
    else {

        if (isNaN(anchor.dataid) || anchor.dataid == "")
            anchor.dataid = 0;

        $("#contentAddEdit").load("Event.aspx?EventId=" + anchor.dataid + "&var=" + x.getMilliseconds(), success);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);




}

function deleteData(anchor) {
    var msg = $("#eventType").val() == "Active"
                      ? "Item will be moved to the archived items,\nDo you wish to continue?"
                      : "Item will be removed permanently,\nDo you wish to continue?";

    if (confirm(msg)) {
        var requestDelete = function (result) {
            if (result.Data != false) {
                changeEventCat();
            }
            else {
                DisplayMessage("Messages", "error", "Sorry but we could not save your data a request has been sent through and we will contact you shortly")
            }
        }

        Aurora.Manage.Service.AuroraWS.RemoveEvent(anchor.dataid, requestDelete, onError);
    }
}



function saveData() {
    $("#contentAddEdit").block({ message: null });
    if (validateForm("EventDetails", true, "Messages", false)) {
        $("#contentAddEdit").unblock();
        return;
    }
    var values = $('#form').serializeArray();
    var events = new AuroraEntities.Event();



    events.Title = $("#Title").val();
    events.Location = $("#Location").val();
    events.GeoLatLong = $("#GeoLatLong").val();
    events.ImageCaption1 = $("#ImageCaption1").val();
    events.ImageCaption2 = $("#ImageCaption1").val();
    events.Occurance = $("#Occurance").val().toString();
    events.Details =oEdit1.getHTMLBody(); 
    events.AllowReminders = events.AllowReminders == "on" ? true : false;
    events.CustomXML = getCustomXML();
    events.ID = events.ID == "" ? Number(-1) : Number(Number($("#ID").val()));
    if (isNaN(events.ID)) {
        events.ID = Number(-1);
    }
    events.Preview = $("#Preview").val();
    events.CategoryID = Number($("#EventCat").val());
    events.DisplayDate = formatDateTimeLongDateString($("#DisplayDate").val());
    var eventStartTime = ($("#EventStartTime").val().toString().split(":").length == 3) ? $("#EventStartTime").val() : $("#EventStartTime").val() + ":00";
    var eventEndTime = ($("#EventEndTime").val().toString().split(":").length == 3) ? $("#EventEndTime").val() : $("#EventEndTime").val() + ":00";

    events.StartDate = formatDateTimeLongDateString($("#StartDate").val() + " " + eventStartTime);
    events.EndDate = formatDateTimeLongDateString($("#EndDate").val() + " " + eventEndTime);
    events.InsertedOn = events.InsertedOn == "" || events.InsertedOn == null ? new Date() : events.InsertedOn;
    events.InsertedBy = 0;

    if (events.ID != 0) {
        events.EntityKey = new Object();
        events.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();
        events.EntityKey.EntityContainerName = "AuroraEntities";
        events.EntityKey.EntitySetName = "Event";
        events.EntityKey.IsTemporary = false;
        objKey.Key = "ID";
        objKey.Value = Number(events.ID);
        events.EntityKey.EntityKeyValues[0] = objKey;
    }
  
   
    events.DeletedOn = null;
    events.DeletedBy = null;

    //check if special occurance
    switch (events.Occurance) {
        case "Monthly":
            if ($("#IgnoreFilter").val() === "0") {
                events.CustomXML = getCustomXML();
            }
            else {
                events.CustomXML = null;
            }
            

    }
    

    var updateEvent = function (result) {

        if (result.Result == true && result.Data != false) {
            DisplayMessage("Messages", "success", "data saved", 7000);
            scrollToElement("Messages", -20, 2);
            changeEventCat();
            if (newItem) {
                $("#contentAddEdit").hide();
            }
            $("#contentAddEdit").unblock();
        }
        else {
        	DisplayMessage("Messages", "error", result.ErrorMessage, 10000);
        }
        $("#contentAddEdit").unblock();
        scrollToElement("Messages", -20, 2);

    };
    if (events.ID != 0) {
        Aurora.Manage.Service.AuroraWS.UpdateEvent(events, updateEvent, onError);
    }
    else {
        Aurora.Manage.Service.AuroraWS.AddEvent(events, updateEvent, onError);
    }

    $("#fileUpload").submit("Edit.aspx" + events.ID);

}
function addNewCat() {
    $("#contentList").hide();
    $("#AddNewCat").show();
    $('div.content').unblock();
}
function AddEventCategory() {
    if (validateForm("addData", true, "AddMsg", true)) {
        return;
    }
    var cat = new Object();
    cat.ClientSiteID = 0;
    cat.Description = $("#CatDescription").val();
    cat.Name = $("#CatName").val();
    cat.OrderIndex = $("#CatOrder").val();
    cat.FeatureModuleID = 4;


    var requestAddCat = function (result) {
        if (result.Count == 0) {
            DisplayMessage("AddMsg", "success", "Category Added",7000);
            ClearForm();
        }
        else if (result.Count == 1) {
            DisplayMessage("AddMsg", "warn", "A catergory with this name already exists.",7000);
        }
        else {
            DisplayMessage("AddMsg", "error", result.ErrorMessage);
        }
    }

    Aurora.Manage.Service.AuroraWS.UpdateCategory(cat, requestAddCat, onError);

}
function changeEventCat() {

    $('div.content').block({ message: null });

    CurrentPage = 0;
    TotalCount = 0;
    getEventCategory();
    
}
function getCustomXML() {
    //Get current XML

    var custXML = "";
    var fields = "";
    var tmp = $("#MonthlyFilter select");
    if (tmp.length > 0) {
        custXML = "<XmlData><Fields>";
        //Get XML From Form
            $(tmp).each(
            function (index, elem) {
                fields += "<field fieldname='" + $(this).attr("name").replace("fld", "") + "' value='" + $(this).attr("value") + "' />"
            }
            );

        custXML += fields + "</Fields></XmlData>";

    }

    if (!custXML) {
        custXML = null;
    }

    return custXML;
}

function ignoreFilter(checkBox) {
    if ($(checkBox).is(":checked")) {
        $(checkBox).val("1");
        $("#fldWeek").attr("disabled","disabled");
        $("#fldDays").attr("disabled", "disabled");
    }
    else {
        $(checkBox).val("0").removeAttr("checked");
        $("#fldWeek").removeAttr("disabled");
        $("#fldDays").removeAttr("disabled");
    }
}
$(function () {

    if (getQueryVariable("type") == undefined) {
        document.location = document.location + "?type=Edit";
        return;
    }
    $("#CatOrder").ForceNumericOnly();
    $("#btnSave").hide();
    $("#Cancel").hide();
    if (getQueryVariable('type').toUpperCase() == "EDIT" || getQueryVariable('type').toUpperCase() == "LIST") {
        $('div.content').block({ message: null });
        //  $("#contentAddEdit").hide();
        $("#AddNewCat").hide();
        getEventCategory();
    } else if (getQueryVariable('type').toUpperCase() == "ADD") {
        $("#contentList").hide();
    }

    //load cat edit
    $('#categoryEditFancy').fancybox({
        padding: 0,
        titleShow: true,
        title: '',
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: 800,
        height: 600,
        type: "iframe",
        onClosed: function () {
            getEventCategory();
        }
    });
});

