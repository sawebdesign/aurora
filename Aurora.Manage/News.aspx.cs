﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom.Data;
using Aurora.Custom;
using System.Data;
using System.Data.SqlClient;

namespace Aurora.Manage {
    public partial class News : System.Web.UI.Page {

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);
            Utils.CheckSession();
            AuroraEntities service = new AuroraEntities();
            int NewsID = Custom.Utils.ConvertDBNull<int>(Request.QueryString["NewsId"], -1);
            int SchemaId = 0;
            int ModuleId = 0;
            XmlDocument XmlData = new XmlDocument(); //The xml news data
            string schema = string.Empty; //the schema for the extension
            XmlDocument SchemaXml = new XmlDocument(); //the schema for the extension

            //get the module that we are loading
            Utils.CheckSqlDBConnection();
            using (SqlCommand cmd = new SqlCommand()) {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[Clients].[proc_Module_GetByClientSiteIDName]";
                cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                cmd.Parameters.AddWithValue("@Name", "News");
                DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);
                if (ds.Tables[0].Rows.Count > 0) {
                    SchemaId = Custom.Utils.ConvertDBNull<int>(ds.Tables[0].Rows[0]["SchemaId"], 0);
                    schema = Custom.Utils.ConvertDBNull<string>(ds.Tables[0].Rows[0]["SchemaXML"], "");
                } else {
                    CustomData.Visible = false;
                }
                
            }


                //todo Create individual schema record

            long moduleID = (from modules in service.Module
                             where modules.Name == "News"
                             select modules).FirstOrDefault().ID;

            var categories = new Aurora.Manage.Service.AuroraWS().GetCategories(0, 0, "", 3, 0);

            ContentPanel.Visible = false;
            CategoryID.DataSource = categories["Data"];
            CategoryID.DataTextField = "Name";
            CategoryID.DataValueField = "ID";
            CategoryID.DataBind();

                if (NewsID > 0) {
                    //XmlData is Bound - must comply to XmlData/Fields/field;
                    var resultsNews = (from news in service.News
                                       where news.ID == NewsID
                                       select news).DefaultIfEmpty().First();
                    if (!string.IsNullOrEmpty(resultsNews.CustomXml)) {
                        XmlData.LoadXml(resultsNews.CustomXml.ToString());
                    }



                    //load the news into the form
                    ID.Value = resultsNews.ID.ToString();
                    ClientSiteID.Value = Custom.Utils.ConvertDBNull<string>(resultsNews.ClientSiteID, "");
                    CategoryID.SelectedValue = Custom.Utils.ConvertDBNull<string>(resultsNews.CategoryID, "");
                    Title.Value = resultsNews.Title;
                    Preview.Value = resultsNews.Preview;
                    StartDate.Value = resultsNews.StartDate.Value.ToString("dd/MM/yyyy");
                    EndDate.Value = resultsNews.EndDate.Value.ToString("dd/MM/yyyy");
                    Details.Value = resultsNews.Details;
                    ImageCaption1.Value = resultsNews.ImageCaption1;
                    InsertedOn.Value = Custom.Utils.ConvertDBNull<DateTime>(resultsNews.InsertedOn, DateTime.Now).ToString();
                    InsertedBy.Value = Custom.Utils.ConvertDBNull<string>(resultsNews.InsertedBy, "0");

                    if (File.Exists(Server.MapPath("~/ClientData/" + SessionManager.ClientSiteID + "/Uploads/news_01_" + resultsNews.ID + ".jpg"))) {
                        Image1.ImageUrl = "~/ClientData/" + SessionManager.ClientSiteID + "/Uploads/news_01_" + resultsNews.ID + ".jpg?"+DateTime.Now.Ticks;
                        Image1.Visible = true;
                    }

                }

                //XmlSchema - must comply to XmlDataSchema/Fields/field;
                if (SchemaId > 0) {

                    if (!string.IsNullOrEmpty(schema)) {
                        SchemaXml.LoadXml(schema);

                        //Load the schema and data
                        ContentXml.XmlPanelSchema = SchemaXml;
                        ContentXml.XmlPanelData = XmlData;

                        //Render the control and add it to the Form
                        ContentXml.RenderXmlControls("tbl_Form");
                    }
                }

        }

        protected void Page_Load(object sender, EventArgs e) {

        }
    }
}