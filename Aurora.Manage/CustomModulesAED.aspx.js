﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />


//#region Vars 
var globals = new GlobalVariables.GlobalVar();
var CurrentPage = 0;
var TotalCount = 0;
var template = null;
var customModule = new AuroraEntities.CustomModules();
//#endregion

//#region Init
$(
    function () {
        getCustomModules();
    }
);
//#endregion

//#region Get methods 
function getCustomModules() {
    var response = function (result) {
        if (result.Count > 0 && result.Data[0] != null) {
            if (!template) {
                template = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
            }


            if (TotalCount == 0) {
            	TotalCount = result.Count;
            	initPagination();
            }

            TotalCount = result.Count;

            template.set_data(result.Data);
            $(".data").show();
        }
        else {
            $("#Messages").insertBefore(".data");
            $(".data").hide();
            DisplayMessage("Messages", "warn", "No custom modules have been created please click the add button to create a custom module");
        }
    };

    Aurora.Manage.Service.AuroraWS.GetCustomModules(CurrentPage + 1, globals.PageSize, "", response, onError);
}
function editCustomModules(id) {
    $("#contentAddEdit").block({ message: null });
    var loadUrl = "";
    var noCache = new Date();
    if (id == 0) {
        loadUrl = "/CustomModules.aspx?ModuleID=0&var=" + noCache.getMilliseconds();
    }
    else {
        loadUrl = "/CustomModules.aspx?ModuleID=" + id + "&var=" + noCache.getMilliseconds();
    }


    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#contentAddEdit").unblock();
            $("#contentAddEdit").show();
            $("#btnSave").show();
            $("#Cancel").show();

        }
    };

    $("#contentAddEdit").load(loadUrl, success);
    //$("#Messages").insertBefore("#contentAddEdit");
    //    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    //    scrollToElement("Messages", -20, 2);

}
function deleteCustomModules(id) {
    $(".data").block({ message: null });
    var loadUrl = "";
    var noCache = new Date();
    if (id == 0) {
        loadUrl = "/CustomModules.aspx?ModuleID=0&var=" + noCache.getMilliseconds();
    }
    else {
        loadUrl = "/CustomModules.aspx?ModuleID=" + id + "&var=" + noCache.getMilliseconds();
    }


    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            saveCustomModule(true);
        }
    };

    $("#contentAddEdit").hide();
    $("#contentAddEdit").load(loadUrl, success);
    $("#Messages").insertBefore("#contentAddEdit");
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
}
//#endregion

//#region Put Methods 
function saveCustomModule(isDelete) {
    $("#contentAddEdit").block({ message: null });
    if (validateForm("CustomModulesDetails", true, "Messages", false)) {
        return;
    }


    customModule.ID = Number($("#ID").val() == "" ? 0 : $("#ID").val());
    customModule.Name = $("#inpName").val();
    customModule.EmailRecipients = $("#inpEmailRecipients").val();
    customModule.EmailSubject = $("#inpEmailSubject").val();
    customModule.EmailCustomMessage = $("#inpCustomMessage").val();
    customModule.Description = $("#inpDescription").val();
    customModule.IncludeFormContent = $('#inpIncludeForm').is(':checked');
    customModule.XMLSchema = $("#inpXML").val() == "" ? null : $("#inpXML").val();
    customModule.XMLSchema = customModule.XMLSchema || "";
    customModule.ClientSiteID = Number(client.replace("~/ClientData/", ""));
    customModule.FeaturesModuleID = $("#FeaturesModuleID").val() == "" ? 0 : $("#FeaturesModuleID").val();
    customModule.InsertedOn = new Date();
    customModule.InsertedBy = 0;
    if (isDelete) {
        if (confirm("Do you wish to delete this custom form?")) {
            customModule.DeletedOn = new Date();
            customModule.DeletedBy = 0;
        }
        else {
            $("#Messages").hide();
            return;
        }
    }
    else {
        customModule.DeletedOn = null;
        customModule.DeletedBy = null;
    }
    var response = function (result, isDelete) {
        $("#contentAddEdit").unblock();
        getCustomModules();
        if (isDelete) {
            DisplayMessage("Messages", "warn", "Item deleted", 7000);
            $(".data").unblock();
        }
        else {
            customFields.saveFields();
            $("#contentAddEdit").hide();
            DisplayMessage("Messages", "success", "Item saved", 7000);
        }
        scrollToElement("Messages", -20, 2);
    };

    Aurora.Manage.Service.AuroraWS.UpdateCustomModule(customModule, response, onError, isDelete);
}
//#endregion

//#region Pagination 
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getCustomModules();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getCustomModules();
    }
    //  $("#contentAddEdit").hide();
    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
//#endregion