﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using Aurora.Custom;

namespace Aurora.Manage {
    public partial class Gallery : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["GalleryId"] != null)
            {
                Utils.CheckSession();
                Aurora.Custom.Data.AuroraEntities service = new Custom.Data.AuroraEntities();
                int galleryID = int.Parse(Request["GalleryId"]);
                Custom.Data.Gallery galleryItem = (from gallery in service.Gallery
                                                   where gallery.ID == galleryID
                                                   select gallery).SingleOrDefault();
                if (galleryItem != null)
                {
                    ID.Value = galleryItem.ID.ToString();
                    CategoryID.Value = galleryItem.CategoryID.ToString();
                    //SchemaID.Value = galleryItem.SchemaID.ToString();
                    Name.Value = galleryItem.Name;
                    Description.Value = galleryItem.Description;
                    GalleryIndex.Value = galleryItem.GalleryIndex.ToString();
                    InsertedBy.Value = galleryItem.InsertedBy.ToString();
                    InsertedOn.Value = galleryItem.InsertedOn.ToString();
                    DeletedBy.Value = galleryItem.DeletedBy == null ? string.Empty : galleryItem.DeletedBy.Value.ToString();
                    DeletedOn.Value = galleryItem.DeletedOn == null ? string.Empty : galleryItem.DeletedOn.Value.ToString();
                    MediaType.Value = galleryItem.MediaType;
                    Index.Value = galleryItem.GalleryIndex.ToString();
                    Link.Value = Utils.ConvertDBNull(galleryItem.Link, string.Empty);
                    Link.Visible = (Request["type"].ToUpper() != "VIDEO" || Request["type"].ToUpper() == "AUDIO");
                    //Video functions
                    if (Request["type"].ToUpper() == "VIDEO")
                    {
                        HtmlInputText videoUrl = (HtmlInputText) FindControl("VideoURL");
                        videoUrl.Value = galleryItem.VideoURL;
                        HtmlTableCell feed = (HtmlTableCell) FindControl("videofeed");
                        if (!string.IsNullOrEmpty(galleryItem.VideoURL))
                        {
                        string videoPath = galleryItem.VideoURL.Substring(galleryItem.VideoURL.IndexOf("v=") + 2);
                        feed.InnerHtml =
                            string.Format(
                                "<iframe title=\"YouTube video player\" height=\"390\" src=\"http://www.youtube.com/embed/{0}\" frameborder=\"0\" width=\"480\" allowfullscreen=\"\"></iframe>",
                                videoPath);
                    }
                }
                    //image functions
                    bool image =
                        File.Exists(
                            Server.MapPath(string.Format("{0}{1}", SessionManager.ClientBasePath,
                                                         "/Uploads/gallery_01_" + galleryItem.ID + ".jpg")));

                    if (image)
                    {
                        Image.ImageUrl  = string.Format("{0}{1}", SessionManager.ClientBasePath,
                                                               "/Uploads/gallery_01_" + galleryItem.ID + ".jpg?"+DateTime.Now.Ticks);
                        Image.Visible = true;
                    }
                    

                    //Audio functions
                    if (Request["type"].ToUpper() == "AUDIO" && File.Exists(Server.MapPath(string.Format("{0}{1}", SessionManager.ClientBasePath, "/Uploads/gallery_01_" + galleryItem.ID + galleryItem.MediaType))))
                    {
                        HiddenAudioFileSource.Value = Security.Encryption.Encrypt(string.Format("{0}{1}", SessionManager.ClientBasePath,
                                                      "/Uploads/gallery_01_" + galleryItem.ID + galleryItem.MediaType));
                        LinkDownloadFile.Visible =true;

                        player.Text =string.Format("<object width=\"320\" height=\"90\" classid=\"CLSID:6BF52A52-394A-11D3-B153-00C04F79FAA6\"    codebase=\"http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/\">    <param name=\"URL\" value=\"{0}\">    <param name=\"rate\" value=\"1\">    <param name=\"balance\" value=\"0\">    <param name=\"currentPosition\" value=\"0\">    <param name=\"defaultFrame\" value=\"\">    <param name=\"playCount\" value=\"32767\">    <param name=\"autoStart\" value=\"0\">    <param name=\"currentMarker\" value=\"0\">    <param name=\"invokeURLs\" value=\"-1\">    <param name=\"baseURL\" value=\"\">    <param name=\"volume\" value=\"50\">    <param name=\"mute\" value=\"0\">    <param name=\"uiMode\" value=\"full\">    <param name=\"stretchToFit\" value=\"0\">    <param name=\"windowlessVideo\" value=\"0\">    <param name=\"enabled\" value=\"-1\">    <param name=\"enableContextMenu\" value=\"-1\">    <param name=\"fullScreen\" value=\"0\">    <param name=\"SAMIStyle\" value=\"\">    <param name=\"SAMILang\" value=\"\">    <param name=\"SAMIFilename\" value=\"\">    <param name=\"captioningID\" value=\"\">    <param name=\"enableErrorDialogs\" value=\"0\">    <param name=\"_cx\" value=\"8467\">    <param name=\"_cy\" value=\"7673\">    <param name=\"URL\" value=\"/ClientData/10001/Uploads/\">    <param name=\"rate\" value=\"1\">    <param name=\"balance\" value=\"0\">    <param name=\"currentPosition\" value=\"0\">    <param name=\"defaultFrame\" value=\"\">    <param name=\"playCount\" value=\"32767\">    <param name=\"autoStart\" value=\"-1\">    <param name=\"currentMarker\" value=\"0\">    <param name=\"invokeURLs\" value=\"-1\">    <param name=\"baseURL\" value=\"\">    <param name=\"volume\" value=\"50\">    <param name=\"mute\" value=\"0\">    <param name=\"uiMode\" value=\"full\">    <param name=\"stretchToFit\" value=\"0\">    <param name=\"windowlessVideo\" value=\"0\">    <param name=\"enabled\" value=\"-1\">    <param name=\"enableContextMenu\" value=\"-1\">    <param name=\"fullScreen\" value=\"0\">    <param name=\"SAMIStyle\" value=\"\">    <param name=\"SAMILang\" value=\"\">    <param name=\"SAMIFilename\" value=\"\">    <param name=\"captioningID\" value=\"\">    <param name=\"enableErrorDialogs\" value=\"0\">    <param name=\"_cx\" value=\"8466\">    <param name=\"_cy\" value=\"7672\">    <embed height=\"290\" type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/\" width=\"320\" src=\"{0}\" autorewind=\"1\" showdisplay=\"0\" showstatusbar=\"0\" showcontrols=\"1\" autostart=\"0\" filename=\"{0}\"> </embed></object>"
                                     ,"http://"+Request.ServerVariables["SERVER_NAME"]+"/ClientData/"+SessionManager.ClientSiteID+"/Uploads/gallery_01_" + galleryItem.ID + galleryItem.MediaType);

                    }
                    else
                    {
                        downloadRow.Style.Add("visibility", "hidden");
                    }
                }
                else
                {
                   
                    downloadRow.Style.Add("visibility","hidden");
                }
            }
        }

        protected void LinkDownloadFile_Click(object sender, EventArgs e) {
            Utils.DownloadFile(HiddenAudioFileSource.Value);
        }
    }
}