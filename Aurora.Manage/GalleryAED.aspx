﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="GalleryAED.aspx.cs" Inherits="Aurora.Manage.GalleryAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="GalleryAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <div>
        <div id="contentList">
            <div class="inner">
                <!-- Begin one column window -->
                <div class="onecolumn">
                    <!--Audio Albums-->
                    <div class="header" name="audio">
                        <span onclick="paginationEnabled = 'audio';getGallerys('audio');">Audio Albums <sub>click here to expand</sub>
                        </span>
                        <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                            <img src="Styles/images/add.png" alt="Create Album Items" mytitle="Create Album Item"
                                onclick="getDetail();" class="help" />
                        </div>
                        <div style="float: right; margin-bottom: 3px; margin-right: 10px;">
                        </div>
                        <div style="float: left; margin-top: 10px; margin-right: 10px;">
                            <div>
                                &nbsp;
                                <select id="GalleryCat" class="sys-template" sys:attach="dataview" onchange="changeGalleryCat(this); $('#contentAddEdit').hide();">
                                    <option value="{binding ID}" gallerytype="{binding Description}">{binding Name}</option>
                                </select>
                                <a class="categoryEditFancy" href="/toolpages/CategoryAED.aspx?moduleID=5&filter=Audio"
                                    mytitle="Edit Categories" class="help">
                                    <img src="Styles/images/folder_wrench.png" alt="Edit Categories" /></a>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table class="data" width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Name
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th align="left">
                                        Media Type
                                    </th>
                                    <th align="left">
                                        Order
                                    </th>
                                    <th align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="templateList" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td width="25%">
                                        {binding Name}
                                    </td>
                                    <td width="45%">
                                        {binding Description}
                                    </td>
                                    <td width="19%">
                                        {{ MediaType == '' ? 'No File Uploaded' : MediaType}}
                                    </td>
                                    <td width="1%">
                                        {binding GalleryIndex}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="getDetail(this);return false;" dataid="{binding ID}"
                                            type="Audio">
                                            <img src="../styles/images/icon_edit.png" alt="edit" class="help" mytitle="Edit" /></a>
                                        <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_delete.png" alt="delete" class="help" mytitle="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="html_body">
                        </div>
                        <!-- Begin pagination -->
                        <div id="audioPaginate">
                        </div>
                        <!-- End pagination -->
                    </div>
                    <!--Audio Albums End-->
                </div>
                <br class="clear" />
                <!--Image Albums-->
                <div class="onecolumn">
                    <div class="header" name="image">
                        <span onclick="paginationEnabled = 'image';getGallerys('image');">Image Albums<sub>click here expand</sub></span>
                      <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                            <img src="Styles/images/add.png" alt="Create Album Items" mytitle="Create Album Item"
                                onclick="getDetail();" class="help" />
                        </div>
                        <div style="float: right; margin-bottom: 3px; margin-right: 10px;">
                        </div>
                        <div style="float: left; margin-top: 10px; margin-right: 10px;">
                            <div>
                                &nbsp;
                                <select id="ImageCat" gallerytype="Images" class="sys-template" sys:attach="dataview"
                                    onchange="changeGalleryCat(this); $('#contentAddEdit').hide();">
                                    <option value="{binding ID}" gallerytype="{binding Description}">{binding Name}</option>
                                </select>
                                <a class="categoryEditFancy" href="/toolpages/CategoryAED.aspx?moduleID=5&filter=Images"
                                    mytitle="Edit Categories" class="help">
                                    <img src="Styles/images/folder_wrench.png" alt="Edit Categories" /></a>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table class="data" width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Name
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th align="left">
                                        Media Type
                                    </th>
                                    <th align="left">
                                        Order
                                    </th>
                                    <th align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="ImageData" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td width="25%">
                                        {binding Name}
                                    </td>
                                    <td width="45%">
                                        {binding Description}
                                    </td>
                                    <td width="19%">
                                        {{ MediaType == '' ? 'No File Uploaded' : MediaType}}
                                    </td>
                                    <td width="1%">
                                        {binding GalleryIndex}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="getDetail(this);return false;" dataid="{binding ID}"
                                            type="Image">
                                            <img src="../styles/images/icon_edit.png" alt="edit" class="help" mytitle="Edit" /></a>
                                        <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_delete.png" alt="delete" class="help" mytitle="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Begin pagination -->
                        <div id="imagePaginate">
                        </div>
                    </div>
                </div>
                <!--Image Albums End-->
                <!--Video Albums-->
                <br class="clear" />
                <div class="onecolumn">
                    <div class="header" name="video">
                        <span onclick="paginationEnabled = 'video';getGallerys('video');">Video Albums<sub>click here expand</sub></span>
                        <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                            <img src="Styles/images/add.png" alt="Create Album Items" mytitle="Create Album Item"
                                onclick="getDetail();" type="Video" class="help" />
                        </div>
                        <div style="float: right; margin-bottom: 3px; margin-right: 10px;">
                        </div>
                        <div style="float: left; margin-top: 10px; margin-right: 10px;">
                            <div>
                                &nbsp;
                                <select id="VideoCat" class="sys-template" gallerytype="Video" sys:attach="dataview"
                                    onchange="changeGalleryCat(this); $('#contentAddEdit').hide();">
                                    <option value="{binding ID}" gallerytype="{binding Description}">{binding Name}</option>
                                </select>  <a class="categoryEditFancy" href="/toolpages/CategoryAED.aspx?moduleID=5&filter=Video"
                                    mytitle="Edit Categories" class="help">
                                    <img src="Styles/images/folder_wrench.png" alt="Edit Categories" /></a>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table class="data" width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Name
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th align="left">
                                        Media Type
                                    </th>
                                    <th align="left">
                                        Order
                                    </th>
                                    <th align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="VideoData" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td width="20%">
                                        {binding Name}
                                    </td>
                                    <td width="45%">
                                        {binding Description}
                                    </td>
                                    <td width="19%">
                                        {{ MediaType == '' ? 'No File Uploaded' : MediaType}}
                                    </td>
                                    <td width="1%">
                                        {binding GalleryIndex}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="getDetail(this);return false;" dataid="{binding ID}"
                                            type="Video">
                                            <img src="../styles/images/icon_edit.png" alt="edit" class="help" mytitle="Edit" /></a>
                                        <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_delete.png" alt="delete" class="help" mytitle="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="videoPaginate">
                        </div>
                    </div>
                </div>
                <!--Video Albums End-->
                <div id="Messages">
                </div>
                <div id="contentAddEdit">
                </div>
                <!-- End one column window -->
            </div>
            <!-- End content -->
        </div>
    </div>
    <div id="AddNewCat">
        <div id="Add">
            <!-- Begin one column window -->
            <div class="onecolumn" id="NewCat">
                <div class="header">
                    <span>Add New Album</span>
                </div>
                <br class="clear" />
                <div id="AddMsg">
                </div>
                <div class="content">
                    <table width="100%" id="addData">
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
                                <input type="text" id="CatName" valtype="required" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Album Type:
                            </td>
                            <td>
                                <select id="CatDescription">
                                    <option>Audio</option>
                                    <option>Images</option>
                                    <option>Video</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Order
                            </td>
                            <td>
                                <input type="text" id="CatOrder" valtype="required" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="Add" onclick="AddGalleryCategory();" />
                                <input type="button" value="Back" onclick="document.location.reload()" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
