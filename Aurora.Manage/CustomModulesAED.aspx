﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="CustomModulesAED.aspx.cs" Inherits="Aurora.Manage.CustomModulesAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script src="CustomModulesAED.aspx.js?123" type="text/javascript"></script>
    <div class="onecolumn">
        <div class="header">
            <span>My Custom Forms</span>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img src="Styles/images/add.png" class="help" alt="Create Module" title="Create Event"
                    style="cursor: pointer" onclick="editCustomModules(0);" />
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                    <th align="left">
                        Name
                    </th>
                    <th align="left">
                        Description
                    </th>
                    <th align="right">
                        Function
                    </th>
                </thead>
                <tbody class="sys-template" id="template">
                    <tr>
                        <td align="left">
                            {{Name}}
                        </td>
                        <td align="left">
                            {{Description}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);" onclick="editCustomModules($(this).attr('dataid')); return false;" dataid="{binding ID}">
                                <img src="../styles/images/icon_edit.png" alt="edit" class="tipper" mytitle="Edit" /></a>
                            <a href="javascript:void(0);" onclick="deleteCustomModules($(this).attr('dataid')); return false;"
                                dataid="{binding ID}">
                                <img src="../styles/images/icon_delete.png" alt="delete" class="tipper" mytitle="Delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
			<!-- Begin pagination -->
            <div id="Pagination">
            </div>
            <!-- End pagination -->
        </div>
    </div>
    <div id="contentAddEdit" > 
    
    </div>
    <div id="Messages"></div>
</asp:Content>
