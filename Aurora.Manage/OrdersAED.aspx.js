﻿//#region Refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion

//#region Vars 
var TotalCount = 0;
var CurrentPage = 0;

var globals = new GlobalVariables.GlobalVar();
var listDataSet = null;
var processItems = "";
var processNewLineItems = "";
//#endregion

//#region Init
$(function () { getOrders(false); });
//#endregion


//#region Get Methods 
/// <summary>Get all unapproved orders</summary>
function getOrders(isApproved, save) {
    $(".content").block({ message: null });
    if (!save) {
        $("#Messages").insertAfter("#products");
    }
    $("#contentAddEdit").hide();
    var response = function (result) {
        if (result.Data.length == 0) {
            $(".data").hide();
            $(".content").unblock();
            $("#Messages").insertAfter(".data");
            DisplayMessage("Messages", "warn", "There currently no enquiries within this selection.");
        }
        else {
            if (listDataSet == null) {
                listDataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            }

            listDataSet.set_data(result.Data);

            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;

            $(".data").show();
            if (!save) {
                $("#Messages").hide();
            }

            $(".content").unblock();
        }
    };

    Aurora.Manage.Service.AuroraWS.GetEnquiries(CurrentPage + 1, globals.PageSize, isApproved, response, response);
}
function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        getOrders(false);
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        product.display();
    }
    $("#contentAddEdit").hide();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
function getOrderDetails(orderID) {
    $(".content").block({ message: null });
    var loadSuccess = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#Messages").insertBefore("#pnlAddEdit");
            scrollToElement("Messages", -100, 0);
            $("#contentAddEdit").unblock();
            $("#contentAddEdit").show();

        	//get related LineItems
            var response = function (_result) {
            	var newLines = _result.Data;
            	if (newLines.length > 0) {
					//add each item individually, its easier for everyone this way :)
            		for (var i = 0; i < newLines.length; i++) {
            			//adds a new row into the the request item table 
            			var currentRows = $("#rptrItems tr");

            			//increment index counter based on current rows
            			newLines[i].Index = currentRows.length;

            			//new row html
            			var newRow = $("#lineItemTemplate").render(newLines[i]);
            			$(newRow).insertAfter($(currentRows[newLines[i].Index - 3]));
            		}
            	}

            	$(".content").unblock();
            };
            Aurora.Manage.Service.AuroraWS.GetLineItems(orderID, response, onError);


        }


    };

    var x = new Date();
    $("#Messages").insertAfter("#products");
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
    $("#contentAddEdit").load(String.format("Orders.aspx?ID={0}&var={1}", orderID, x.getMilliseconds()), loadSuccess);
}
//#endregion

//#region Put Methods 


function saveSentQuoteData() {
    
    var response = function (result) {
        if (result.Count > 0) {
            DisplayMessage("Messages", "success", "Order information updated with saved.");
        }
        else {
            DisplayMessage("Messages", "warn", "All updates have not been applied, system unable to save some of the details sent with this quote.");
        }
    };
    Aurora.Manage.Service.AuroraWS.SaveSentEnquiryData($("#ID").val(), processItems, processNewLineItems, $("#orderReference").val(), $("#additionalInfo").val(), response, onError);
    
}

function processOrder() {
    if (processItems != "" && $("#isApproved").attr("checked")) {
        var response = function (result) {
            if (parseFloat($("#totalquan").html()) == parseFloat(result.Count)) {
                DisplayMessage("Messages", "success", "All updates have been applied items were deducted from stock.");
                getOrders(false, true);
            }
            else {
                DisplayMessage("Messages", "warn", "All updates have not been applied, there was not enough stock.");
                getOrders(false, true);
            }
        };
        Aurora.Manage.Service.AuroraWS.UpdateEnquiries($("#ID").val(), processItems, processNewLineItems, $("#orderReference").val(), $("#additionalInfo").val(), response, onError);
    }
    else {
        if (!$("#isApproved").attr("checked")) {
            DisplayMessage("Messages", "warn", "Please tick the approved check box to confirm changes to stock.", 7000);
        }
        else {
            DisplayMessage("Messages", "warn", "Please select at least one item from the requested item list to process this enquiry", 7000);
        }
        scrollToElement("Messages", -300, 0);
    }

}

function deleteItem(orderID) {
    var ans = confirm("Are you sure that you would like to permanantly remove this enquiry?");
    if (ans) {
        var response = function (result) {
            if (result.Data.length == 1) {
                DisplayMessage("Messages", "success", "Item successfully removed", 7000);
            }
            else {
                DisplayMessage("Messages", "warn", "No Items were removed", 7000);
            }

            getOrders(Boolean.parse($("#currentFilter").val()), false);
        };

        Aurora.Manage.Service.AuroraWS.RemoveEnquiry(orderID, response, onError);

    }
}

function sendQuote(isPreview) {
    if (isPreview) {
        //get selected items

        var header = $("#rptrItems thead th");
        var items = $("#rptrItems tr[selected='true']");
        if (items.length == 0) {
            $.fancybox.close();
            DisplayMessage("Messages", "warn", "In order to send a quotation you must select at least one item from the requested items list");
            scrollToElement("Messages", -30, 20);
            return false;
        }
        var totals = $("#rptrItems tr:last");
        var amount = $("#rptrItems tr:last").prev();
        $(".items").append("<thead></thead>")
        $(".items").append("<tbody></tbody>")


        header.clone().appendTo($(".items thead"));
        items.children('tr td:last').remove();
        items.clone().appendTo($(".items tbody"));
        amount.clone().appendTo($(".items tbody"));
        totals.clone().appendTo($(".items tbody"));
        $(".items tbody .select").remove();
        $(".items  thead th:last").remove();
        $(".items  thead th").css("font-weight", "bold");
        $(".items tbody input").each(
            function (index, Elem) {
                $("<span>" + $(this).attr("value") + "</span>").insertAfter(this);
                $(this).remove();
            }
        );

        $(".items tbody textarea").each(function (index, El) {
            $el = $(this);
            $("<p>" + $el.html().replace(/\r\n/g, "<br>") + "</p>").insertAfter($el);
            $el.remove()
        });
        $(".items tr[selected='true'] .price").each(
                function (idx, Elm) {
                    var curElem = $(this);
                    curElem.html("R " + $(this).html().replace("R",""));

                }
            );
        $(".items #total").html("R " + $(".items #total").html().replace("R ",""));
        $(".items tbody").append("<tr><td colspan='7'><td> <input id=\"SendConfirm\" type=\"button\" value=\"Send\" onclick=\"sendQuote();\"/></td></tr>")
        $(".items tr").removeAttr("style");

    	//set reference
        if ($("#orderReference").val().length > 0) {
        	$("#requestDataReference").html("Reference: " + $("#orderReference").val());
        } else {
        	$("#requestDataReference").html("");
        }

    	//set additional info
        if ($("input[name=additionalInfo]").val().length > 0) {
        	$("#requestDataAdditionalInfo").html($("input[name=additionalInfo]").val()).show();
        } else {
        	$("#requestDataAdditionalInfo").html("").hide();
        }

        $("#Quote").show();
        $.fancybox.center();
        return true;

    }
    else {
        $("#SendConfirm").remove();

        var response = function (result) {
            if (result) {
                $.fancybox.close();
                DisplayMessage("Messages", "success", "Your quotation has been sent to the client", 7000);
                $("#Quote").hide();
            }
        };
        $("#Quote tbody:hidden").remove();
        $("#Quote thead:hidden").remove();
        //Aurora.Manage.Service.AuroraWS.SendMail($("#ClientEmail").html(), "Products", $("#Quote").html(), "Product Enquiry", response, onError);
        Aurora.Manage.Service.AuroraWS.SendQuotations($('#Quote').html() + "<br/>", "Product Quote", $("#ID").val(), response, onError);

		//if the quote is not approved then save the data to be sent with the quote so that it can be viewed at a later stage
        if (!$("#isApproved").attr("checked")) {
        	saveSentQuoteData();
        }
    }
}
//#endregion


//#region UI methods 

function checkBoxClick(objChkBox) {
    if ($(objChkBox).is(":checked")) {
        $(objChkBox).parent().parent().css({ backgroundColor: "#fef4d2" });
        $(objChkBox).parent().parent().attr("selected", "true");
    }
    else {
        $(objChkBox).parent().parent().css({ backgroundColor: "transparent" });
        $(objChkBox).parent().parent().removeAttr("selected");
    }


    //calculate current selected items
    calculateRequestedItems();

}
function calculateRequestedItems() {
    processItems = "";
    processNewLineItems = "";
    var currentTotal = parseFloat($("#total").html());
    var totalItems = 0;
    var newTotal = 0;
    $("#rptrItems input:checkbox").each(
            function (index) {
            	if ($(this).is(":checked")) {

            		var orderItems = new AuroraEntities.OrderItems();
            			orderItems.OrderID = $("#ID").val();
            			orderItems.ProductID = $(this).attr("productid");
            			orderItems.hasSelected = true;
            			orderItems.Quantity = $(this).attr("dataquantity");

					//if item has  newLine class then its a newLine item add to processNewLineItems
            			if ($(this).hasClass("newLines")) {
            				var $currentTR = $("#LineItem_" + orderItems.ProductID),
            					name = $currentTR.find("input[name=Name]").val(),
            					code = $currentTR.find("input[name=Code]").val(),
            					price = $currentTR.find("input[name=Price]").val(),
            					quantity = $currentTR.find("input[name=Quantity]").val();
            				$currentTR.find("input[name=Price]").val(parseFloat(price).toFixed(2));
            				$(this).attr("dataprice", parseFloat(price).toFixed(2));
						//well at this point  order is important, now watch as elegance goes out the window :(
            			processNewLineItems += name + " ," + code + " ," + parseFloat(price).toFixed(2) + ", " + quantity + "|";
            		} else {
            			processItems += orderItems.OrderID + "," + orderItems.ProductID + "," + orderItems.Quantity + "|";
            		}

            		newTotal += (parseFloat($(this).attr("dataquantity")).toFixed(2) * parseFloat($(this).attr("dataprice")).toFixed(2));            		
            		totalItems = totalItems + parseFloat($(this).attr("dataquantity"));
                }

            }
        );

    if (isNaN(currentTotal)) {
        $("#total").html(newTotal);
    }
    else {
        if (newTotal == 0) {
            $("#total").html(parseFloat(newTotal).toFixed(2));
        }
        else {
        	$("#total").html(parseFloat(newTotal).toFixed(2));
        }

    }

    $("#totalquan").html(totalItems);
    return [totalItems, newTotal];
}

function setNewPrice(textField) {
    var item = $(textField);
    item.parent().parent().find("input[type='checkbox']").attr("dataprice", item.val());

    if (item.parent().parent().find("input[type='checkbox']").is(":checked")) {
        calculateRequestedItems();
    }
}

function setNewQuantity(textField) {
    var item = $(textField);
    item.parent().parent().find("input[type='checkbox']").attr("dataquantity", item.val());
    if (item.parent().parent().find("input[type='checkbox']").is(":checked")) {
        calculateRequestedItems();
    }
}


//#region page specific functions
//the object below will only impact on new rows created on the Quote, the original product item data is left as is. :)
var Quote = {
	LineItem : function () {
		this.Index = 0;
		this.Name = "";
		this.Code = "";
		this.Price = 0;
		this.Quantity = 1;
	},
	addLine: function () {
		//adds a new row into the the request item table 
		var newLine = new Quote.LineItem(),
			currentRows = $("#rptrItems tr");

		//increment index counter based on current rows
		newLine.Index = currentRows.length;

		//new row html
		var newRow = $("#lineItemTemplate").render(newLine);
		$(newRow).insertAfter($(currentRows[newLine.Index-3]));
	}
};
//#endregion