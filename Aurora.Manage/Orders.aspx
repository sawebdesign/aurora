﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="Aurora.Manage.Orders" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" src="Orders.aspx.js"></script>
	<script id="lineItemTemplate" type="text/x-jsrender">
        <tr id="LineItem_{{:Index}}">
            <td><input type="text" name="Name" value="{{:Name}}" /></td>
            <td><input type="text" name="Code" value="{{:Code}}"/></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="price"><input type="text" class="price newLinePrice" onchange="setNewPrice(this);" name="Price" value="{{:Price}}"/></td>
            <td><input type="text" name="Quantity" onchange="setNewQuantity(this);" value="{{:Quantity}}"/></td>
            <td valign="top" style="width: 5%; text-align: center;" class="select"><input type="checkbox" class="newLines" onclick="checkBoxClick(this)" hasselected="False" dataprice="{{:Price}}" dataquantity="{{:Quantity}}" productid="{{:Index}}" value="{{:Index}}" />
            </td>
        </tr>
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="onecolumn">
            <div class="header">
                <span>Enquiry Details</span>
            </div>
            <br class="clear" />
            <div class="content">
                <input type="hidden" id="ID" runat="server" />
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <h2>Visitor Details</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>First Name:
                        </td>
                        <td>
                            <label id="ClientFirstName" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Last Name:
                        </td>
                        <td>
                            <label id="ClientLastName" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Email:
                        </td>
                        <td>
                            <a id="ClientEmail" runat="server"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Company Name:
                        </td>
                        <td>
                            <label id="CompanyName" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Contact No:
                        </td>
                        <td>
                            <label id="ClientContactNo" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom:10px;">Comments/Enquiry:
                        </td>
                        <td style="padding-bottom:10px;">
                            <label id="orderEnquiry" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Requested Items</h2>
                        </td>
						<td style="text-align: right;padding-right: 54px;">
							<a href="javascript:" onclick="Quote.addLine()" class="addLineItem fakeButton">Add new Item</a>

						</td>
                    </tr>
					<tr>
						<td colspan="2" valign="top" style="padding-top: 10px;vertical-align: top">
							<table style="width:100%;margin:0;padding:0;">
								<tr>
									<td style="text-align:right;">Reference: </td>
									<td><input type="text" name="orderReference" runat="server" id="orderReference" value="" /></td>
								</tr>
								<tr>
									<td style="text-align:right;">Additional Information: </td>
									<td><input type="text" name="additionalInfo" runat="server" id="additionalInfo" style="width: 843px"></td>
								</tr>
							</table>
						</td>
					</tr>
                    <tr>
                        <td colspan="2" style="padding-top:10px;">
                            <asp:Repeater runat="server" ID="rptrItemCollection" ClientIDMode="Predictable" OnItemCreated="rptrItemCollection_ItemCreated">
                                <HeaderTemplate>
                                    <table id="rptrItems" class="data" width="90%" cellpadding="0" cellspacing="0" border="0">
                                        <thead>
                                            <tr>
                                                <th align="left">Name
                                                </th>
                                                <th align="left">Code
                                                </th>
                                                <th align="left">Comments
                                                </th>
                                                <th align="left">Colour/Texture
                                                </th>
                                                <th align="left">Size
                                                </th>
                                                <th align="left">Price (R)
                                                </th>
                                                <th align="left">Quantity
                                                </th>
                                                <th align="left">Selected
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td valign="top" style="width: 15%">
                                            <%#DataBinder.Eval(Container.DataItem, "product.Name")%>
                                        </td>
                                        <td valign="top" style="width: 13.75%">
                                            <%#DataBinder.Eval(Container.DataItem, "product.Code")%>
                                        </td>
                                        <td valign="top" style="width: 25%">
                                            <asp:TextBox TextMode="MultiLine" Rows="10" Columns="30" runat="server" ID="Notes"></asp:TextBox>
                                        </td>
                                        <td valign="top" id="ColourRow" runat="server" style="width: 13.75%; text-align:left; background: #">&nbsp;
                                        </td>
                                        <td valign="top" style="width: 13.75%">
                                            <label runat="server" id="SizeName">
                                            </label>
                                        </td>
                                        <td valign="top" style="width: 12.75%" class="price">
                                            <input type="text" onchange="setNewPrice(this);" id="ProductPrice" runat="server" />
                                        </td>
                                        <td valign="top" style="width: 14.75%">
                                            <input type="text" onchange="setNewQuantity(this);" value="<%#DataBinder.Eval(Container.DataItem, "orderItems.Quantity")%>" />
                                        </td>
                                        <td valign="top" style="width: 5%; text-align: center;" class="select">
                                            <input type="checkbox" hasselected="<%#DataBinder.Eval(Container.DataItem,"orderItems.hasSelected") %>"
                                                dataprice="<%#DataBinder.Eval(Container.DataItem, "product.Price")%>" dataquantity="<%#DataBinder.Eval(Container.DataItem, "orderItems.Quantity")%>"
                                                productid="<%#DataBinder.Eval(Container.DataItem, "product.ID")%>" value="<%#DataBinder.Eval(Container.DataItem,"product.ID") %>" />
                                        </td>
                                    </tr>

                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td colspan="1" align="left">Total Amount: </td>      <td>  <strong> <label id="total">0</label></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" align="left" >Total Items: </td>      <td><strong><label id="totalquan">0</label></strong>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>

                        </td>
                    </tr>
                   
                    <tr>
                        <td style="height: 25px;"></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div>

                                <div>
                                    Approved:
                                <input id="isApproved" type="checkbox" runat="server" />
                                </div>
                                <a href="#Quote" id="btnSendQuote" runat="server" class="fakeButton" style="width: 110px; height: 32px;">Send Quote</a>
                                <input type="button" id="btnProcessOrder" runat="server" clientidmode="Predictable"
                                    value="Process Order" onclick="processOrder();" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="Quote" runat="server" style="width: 100%; height: 100%; background: #fff; display: none;">
            <table width="100%">
                <tbody>
                    <tr>
                        <td id="test" style="height: 0px;">
                        </td>
                    </tr>
                    <tr >
                        <td></td>
                    </tr>
                    <tr>
                        <td>Following your quotation  request on {Date}</td>
                    </tr>
                    <tr>
                        <td>
							<div id="requestDataReference" style="font-weight:bold"></div>
							<div id="requestDataAdditionalInfo" style="padding: 10px 0;"></div>
                            <table id="requestedData" class="items">
                            </table>
                        </td>
                    </tr>
                  
                    <tr>
                        <td colspan="7"></td>
                    </tr>
                </tbody>
            </table>
        </div>
		
        <input type="hidden" id="ClientName" runat="server" value ="">
    </form>
</body>
</html>
