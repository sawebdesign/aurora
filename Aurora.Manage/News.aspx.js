﻿//#region Init
var saveTriggered = false;
$(function () {

    try {
        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });
        if ($("#ID").val() < 1) {
            $("#CategoryID").val($("#NewsCat").val());
        }


        $("#Image1").hide();
        $('#btn_image1').click(function () {
            if (!saveTriggered) {
                saveTriggered = true;
                saveData();
            }
        });
        $('#btn_image1').mouseover(
        function () {
            $("#Image1").css("left", ($("#tab1_content").width() * 0.70) - 20);
            $("#Image1").css("border", "2px dashed grey");
            $("#Image1").css("position", "absolute");
            $("#Image1").css("top", "91px");
            $("#Image1").fadeIn(500);
            $("#Image1").height($("#Image1").height() / $("#Image1").width * ($("#tab1_content").width() - ($("#tab1_content").width() * 0.70)));
            $("#Image1").width($("#tab1_content").width() - ($("#tab1_content").width() * 0.70));

        }
    );

        $('#btn_image1').mouseout(
    function () {
        $("#Image1").fadeOut(500);
    }
    );

        DisplayMessage("Messages", "info", "Please enter all information in the form below")
        scrollToElement("Messages", -20, 2);
        transferCustomFields("CustomData", "ContentPanel");
        $("#templateDetail .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        scrollToElement("Messages", -20, 2);
        $("#contentAddEdit").insertAfter("#Messages");
        loadEditor();
    } catch (e) {

    }


});
//#endregion

//#region Custom Methods 
function reloadAfterImage() {
    var anchor = {};
    anchor.dataid = $("#ID").val();
    loadDetail(anchor);
}
//#endregion