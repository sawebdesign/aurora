﻿/// <reference path="Scripts/MicrosoftAjax.debug.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="Service/AuroraWS.asmx/js" />
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/jquery.blockUI.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
var functionColumn = null;
var headerTemplate = null;
var template = null;
var Datatemplate = null;

$(
function () {
    SetJSSession("NoScroll", "on");
    $("#contentAddEdit").hide();
    loadModules();
    $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
}
);
function showFancyBox(obj) {
    $(obj).fancybox(
    {
        padding: 0,
        titleShow: true,
        title: "Attached Files",
        overlayColor: '#000000',
        overlayOpacity: 0.5,
        width: '90%',
        height: '90%',
        showActivity: true,
        onClosed: function () {

        },
        onComplete: function () {
            $.fancybox.center();
            $(".inpBtnOut").hide();
        },
        type: "iframe"
    });
}
function loadModules() {
    var response = function (result) {
        if (result.Count > 0) {
            var template = $create(Sys.UI.DataView, {}, {}, {}, $get("modules"));

            template.set_data(result.Data);
            $("#modules").val($("#modules option:first").val());
            createGrid();
            $(".tipper").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        }
        else {

        }
    };

    Aurora.Manage.Service.AuroraWS.GetCustomModules(0, 100, "", response, onError);

}
function createGrid() {
    $("#Messages").hide();
    $(".data").block({ message: null });
    var response = function (result) {
        //save function column
        if (!functionColumn) {
            functionColumn = $("#template #templateRow").children('td:last').clone();
        }

        if (result.Data == "") {
            $(".data").hide();
            $("#Messages").insertBefore(".data");
            $("#contentAddEdit").hide();
            DisplayMessage("Messages", "warn", "No Data Entries have been made for this module");
            return;
        }


        $("#contentAddEdit").hide();
        if ($(".data").prev().attr("id") == "Messages") {
            $("#Messages").hide();
        }

        if (!template) {
            template = $create(Sys.UI.DataView, {}, {}, {}, $get("columns"));
        }
        if (!headerTemplate) {
            headerTemplate = template._element.innerHTML;
        }
        else {
            $("#columns").html(headerTemplate);
        }
        var headers = [];
        result.Data = $.parseJSON(result.Data).NewDataSet.Table;
        var count = 0;
        var myData = !result.Data[0] ? result.Data : result.Data[0];
        for (var name in myData) {
            if (count > 5) {
                break;
            }
            headers.push({ "Name": name });
            count++;
        }

        //add function column
        headers.push({ "Name": "Function" });

        if (headers.join().indexOf("Function") == -1 && headers.length <= 2) {
            headers.push({ "Name": "Function" });
        }
        for (var column = 0; column < headers.length; column++) {
            if (headers[column].Name == "ID") {
                headers.splice(column, 1);
            }
        }

        template.set_data(headers);
        $("#columns th:last").attr("align", "right");

        //Update column names to exclude numbers
        $("#columns th").each(
            function (indx, Elm) {
                $item = $(this);
                $item.html($item.html().replace(/[0-999]/gi,""));
            }
        );

        var rows = $("#templateRow");
        //clear rows
        rows.children('td').remove();
        $("#template tr").not("#templateRow").remove();


        //create new columns
        for (var columns = 0; columns < headers.length; columns++) {
            if (headers[columns].Name != "Function") {
                $("<td>{{" + headers[columns].Name + "}}</td>").appendTo(rows);
            }
        }

        //bind Data to row
        var temp = rows.html();

        if (result.Data.length > 0) {
            for (var item = 0; item < result.Data.length; item++) {
                var current = temp;
                for (var property in result.Data[item]) {
                    current = current.replace("{{" + property + "}}", result.Data[item][property])
                }

                $("#template").append(String.format("<tr>{0}<td align='right'>{1}</td></tr>", current, functionColumn.html().replace(/{binding ID}/gi, result.Data[item].ID).replace(/{{ModuleID}}/gi, result.Data[item].ModuleID)));
            }
        }
        else {
            var singleCurrent = temp;
            for (var property in result.Data) {
                singleCurrent = singleCurrent.replace("{{" + property + "}}", result.Data[property])
            }

            $("#template").append(String.format("<tr>{0}<td align='right'>{1}</td></tr>", singleCurrent, functionColumn.html().replace(/{binding ID}/gi, result.Data.ID).replace(/{{ModuleID}}/gi, result.Data.ModuleID)));
        }



        $(".footer").hide();
        $(".data").show();
        $(".data").unblock();

        $(".tipper").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });

    };

    Aurora.Manage.Service.AuroraWS.GetCustomModulesSaved(Number($("#modules").val()), response, onError);
}

function editCustomData(id, modID) {
    $("#contentAddEdit").show();

    var loadUrl = "";
    var noCache = new Date();
    if (id == 0) {
        loadUrl = "/CustomModulesData.aspx?ModuleID=0&CustomID=" + modID + "&var=" + noCache.getMilliseconds();
    }
    else {
        loadUrl = "/CustomModulesData.aspx?ModuleID=" + id + "&CustomID=" + modID + "&var=" + noCache.getMilliseconds();
    }


    $("#contentAddEdit").attr("src", loadUrl);
    $("#Messages").insertBefore("#contentAddEdit");
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
}

function deleteCustomData(id, modID) {
    var ans = confirm("Are you sure you want to delete this item?");
    if (!ans) {
        return;
    }
    $("#contentAddEdit").hide();
    var loadUrl = "";
    var noCache = new Date();
    if (id == 0) {
        loadUrl = "/CustomModulesData.aspx?del=1&ModuleID=0&CustomID=" + modID + "&var=" + noCache.getMilliseconds();
    }
    else {
        loadUrl = "/CustomModulesData.aspx?del=1&ModuleID=" + id + "&CustomID=" + modID + "&var=" + noCache.getMilliseconds();
    }

    $("#contentAddEdit").attr("src", loadUrl);
}

function exportData() {
    var response = function (result) {
        if (result.Result && result.Count > 0) {
            $(String.format("<div>Your file has been created, we suggest opening it with Microsoft Excel</div>", result.Count)).dialog(
            {
                modal: true,
                resizable: false,
                title: "Success",
                stack: false,
                zIndex: 1200,
                buttons:
                {
                	"Download": function () { 
                		window.open("DownloadFile.ashx?encnone=1&filename=" + result.Data); 
                		$(this).dialog("close");
                	}
                }
            });
        }
        else if (result.Result && result.Count === 0) {
            $("<div>There was no data to export</div>").dialog(
            { modal: true,
                resizable: false,
                title: "Error",
                stack: false,
                zIndex: 1200,
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
        }

        else {
            errorResponse();
        }
        $(".content").unblock();
    };
    var errorResponse = function (result) {
        $("<div>There was a problem while generating your file please contact support</div>").dialog(
        { modal: true,
            resizable: false,
            title: "Error",
            stack: false,
            zIndex: 1200,
            buttons: { "Okay": function () { $(this).dialog("close"); } }
        });
        $(".content").unblock();
    }
    $(".content").block({ message: null });
    //CGCSA
    if ($("#modules").val() != "10002") {
        Aurora.Manage.Service.AuroraWS.ExportDataForCustomForm($("#modules").val(), ["*"], response, errorResponse);
    }
    else {
        Aurora.Manage.Service.AuroraWS.ExportDataForCustomForm($("#modules").val(), [
                                                                                "[0FullCompanyName]", "VATNo", "[Postal Address]", "MainEmailAddress", "AccountFirstName", "AccountSurname", "AccountContactNo", "AccountEmailAddress", "FirstName0", "Surname0", "EmailAddress0", "Position0", "DietaryRequirements0", "AttendCocktailParty0",
                                                                                "FirstName1", "Surname1", "EmailAddress1", "Position1", "DietaryRequirements1", "AttendCocktailParty1",
                                                                                "FirstName2", "Surname2", "EmailAddress2", "Position2", "DietaryRequirements2", "AttendCocktailParty2",
                                                                                "FirstName3", "Surname3", "EmailAddress3", "Position3", "DietaryRequirements3", "AttendCocktailParty3",
                                                                                "FirstName4", "Surname4", "EmailAddress4", "Position4", "DietaryRequirements4", "AttendCocktailParty4",
                                                                                "FirstName5", "Surname5", "EmailAddress5", "Position5", "DietaryRequirements5", "AttendCocktailParty5",
                                                                                "FirstName6", "Surname6", "EmailAddress6", "Position6", "DietaryRequirements6", "AttendCocktailParty6",
                                                                                "FirstName7", "Surname7", "EmailAddress7", "Position7", "DietaryRequirements7", "AttendCocktailParty7",
                                                                                "FirstName8", "Surname8", "EmailAddress8", "Position8", "DietaryRequirements8", "AttendCocktailParty8",
                                                                                "FirstName9", "Surname9", "EmailAddress9", "Position9", "DietaryRequirements9", "AttendCocktailParty9", "ContactNo","SpecialRequests"

                                                                                ], response, errorResponse);
    }
}
