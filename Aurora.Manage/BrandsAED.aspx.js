﻿//#region refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion

var globals = new GlobalVariables.GlobalVar();
var dataset = null;
var CurrentPage = 0;
var PageSize = 0;
var TotalCount = 0;

//#region Init
$(function () {
    loadBrands();
});
//#endregion

//#region Data Manipulation 
function loadBrands() {
    $("#grid").block({ message: null })
    var onSuccess = function (result) {
        if (result.Data.length === 0) {
            DisplayMessage("Messages", "warn", "No Brands have been created, please click the green add button to create a brand.");
            $(".data").hide();
            $("#Messages").insertBefore(".data");
            $("#grid").unblock();
        }
        else {
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;
            if (dataset === null) {
                dataset = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
            }
            dataset.set_data(result.Data);
            $("#grid").unblock();
        }
    };
    Aurora.Manage.Service.AuroraWS.GetBrands(CurrentPage + 1, globals.PageSize, "", onSuccess, onError);
}


function getDetail(id) {
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1 || responseText.indexOf("<title>Runtime Error</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#Edit").show();
            $("#btnSave").show();
            $("#Cancel").show();
        }
    }
    if (id === "-1") {
        $("#Edit").load("Brands.aspx?BrandID=-1&var=" + x.getMilliseconds(), success);
    }
    else {
        $("#Edit").load("Brands.aspx?BrandID=" + id + "&var=" + x.getMilliseconds(), success);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadBrands();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadBrands();
    }
    $("#Edit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
//#endregion