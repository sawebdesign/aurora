﻿//#region refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion
var CurrentPage = 0;
var PageSize = 0;
var TotalCount = 0;
var globals = new GlobalVariables.GlobalVar();
var dataset = null;
var iCurrentPage = 0;
var currentData = null;
//#region Init
$(function () {
    loadSizes();
});
//#endregion

//#region Data Manipulation 
function loadSizes() {
    $("#grid").block({message:null})
    var onSuccess = function (result) {
        if (result.Data.length === 0) {
            DisplayMessage("Messages", "warn", "No sizes have been created, please click the green add button to create a size.");
            $(".data").hide();
            $("#Messages").insertBefore(".data");
            $("#grid").unblock();
        }
        else {
            currentData = result.Data;
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;
            if (dataset === null) {
                dataset = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
            }
            dataset.set_data(result.Data);
            $("#grid").unblock();
        }
    };
    Aurora.Manage.Service.AuroraWS.GetSizes(CurrentPage + 1, globals.PageSize, "", onSuccess, onError);
}

function Save() {
    var success = function (result) {
        DisplayMessage("Messages", "success", "Size has been saved.", 0);
        $("#Edit").unblock();
        loadSizes();
    };
    $("#Edit").block({ message: null });
    var sizeObj = new AuroraEntities.Size();
    sizeObj.ClientSiteID = 0;
    sizeObj.DeletedBy = null;
    sizeObj.DeletedOn = null;
    sizeObj.Description = $("#Description").val();
    sizeObj.InsertedBy = 0;
    sizeObj.Inactive = false;
    sizeObj.InsertedOn = new Date();
    sizeObj.ID = $("#ID").val();
    if (sizeObj.ID === "") {
        sizeObj.ID = -1;
    }
    else {
        sizeObj.EntityKey = new Object();
        sizeObj.EntityKey.EntityContainerName = "AuroraEntities";
        sizeObj.EntityKey.EntitySetName = "Size";
        sizeObj.EntityKey.IsTemporary = false;
        sizeObj.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();
        objKey.Key = "ID";
        sizeObj.ID = sizeObj.ID == "" ? -1 : parseFloat(sizeObj.ID);
        objKey.Value = parseFloat(sizeObj.ID) ? parseFloat(sizeObj.ID) : -1;
        sizeObj.EntityKey.EntityKeyValues[0] = objKey;
    }
    Aurora.Manage.Service.AuroraWS.UpdateSizes(sizeObj, success, onError);
}

function deleteItem(objItem) {
 ans = confirm("By deleting this item will set all products with this size as unavailable, are you sure you would like to do this?");
 if (ans) {
     var sizeData = findByInArray(currentData, "ID", $(objItem).attr("dataid"));
     if (sizeData !== null) {
         sizeData.DeletedOn = new Date();
         sizeData.DeletedBy = 0;

         var response = function (result) {
             if (result.Result) {
                 DisplayMessage("Messages", "info", "Item has been removed", 7000);
                 $("#Edit").hide();
                 loadSizes();
             }
         };

         Aurora.Manage.Service.AuroraWS.UpdateSizes(sizeData, response, onError);
     }
 }
}
function getDetail(id) {
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#Edit").show();
            $("#btnSave").show();
            $("#Cancel").show();
        }
    }
    if (id === "-1") {
        $("#Edit").load("Size.aspx?SizeID=-1&var=" + x.getMilliseconds(), success);
    }
    else {
        $("#Edit").load("Size.aspx?SizeID=" + id + "&var=" + x.getMilliseconds(), success);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadSizes();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadSizes();
    }
    $("#Edit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

//#endregion