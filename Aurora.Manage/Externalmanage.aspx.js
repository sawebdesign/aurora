﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/Utils.js" />
var intID = 0;
$(function () {
    
    intID = setInterval(function () {
        if (GetJSSession("ExternalManage_URL")) {
            $("#cmsFrame").attr("src", String.format("{0}?u={1}&p={2}", GetJSSession("ExternalManage_URL"), GetJSSession("ExternalManage_UserName"), GetJSSession("ExternalManage_Password")));
            $("#cmsFrame").css("display", "block");
            clearInterval(intID);
        }
    }, 1000);

});