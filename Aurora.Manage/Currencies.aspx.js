﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />


//#region Vars 
var globals = new GlobalVariables.GlobalVar();
var CurrentPage = 0;
var TotalCount = 0;
var template = null;
//#endregion

//#region Init
$(
    function () {
    	getCurrencies();
    }
);
//#endregion

//#region Get methods 
function getCurrencies() {
	var response = function (result) {
		if (result.Count > 0 && result.Data[0] != null) {
			if (!template) {
				template = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
			}


			if (TotalCount == 0) {
				initPagination();
				TotalCount = result.Count;
			}

			TotalCount = result.Count;

			template.set_data(result.Data);
			$(".data").show();
		}
		else {
			$("#Messages").insertBefore(".data");
			$(".data").hide();
			DisplayMessage("Messages", "warn", "No currencies have been created please click the add button to create a currency conversion");
		}
	};

	Aurora.Manage.Service.AuroraWS.GetCurrencyConverters(CurrentPage + 1, globals.PageSize, "", response, onError);
}
function editCurrency(id) {
	var success = function (response) {
		if (response.Result == true) {
			$('#contentAddEdit').html($('#currencyConvertTmpl').render(response.Data));
			$('#CurrencyID').val(response.Data.CurrencyID);
			$("#contentAddEdit").unblock();
			$("#contentAddEdit").show();
			$("#btnSave").show();
			$("#Cancel").show();
		}
	};

	$("#contentAddEdit").block({ message: null });
	if (id == 0) {
		$('#contentAddEdit').html($('#currencyConvertTmpl').render({}));
		$("#contentAddEdit").unblock();
		$("#contentAddEdit").show();
		$("#btnSave").show();
		$("#Cancel").show();
	}
	else {
		Aurora.Manage.Service.AuroraWS.GetCurrencyConverter(id, success, onError);
	}
	
	
	//$("#contentAddEdit").load(loadUrl, success);
	//$("#Messages").insertBefore("#contentAddEdit");
	//    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
	//    scrollToElement("Messages", -20, 2);

}
function deleteCurrency(id) {
	$(".data").block({ message: null });
	$("#contentAddEdit").hide();
	var success = function (response) {
		if (response.Result == true) {
			$('.data').unblock();
			getCurrencies();
			
			DisplayMessage("Messages", "success", "Item saved", 7000);
			scrollToElement("Messages", -20, 2);
		};
	};
	Aurora.Manage.Service.AuroraWS.DeleteCurrencyConverter(id,success,onError);

}
//#endregion

//#region Put Methods 
function saveData() {
	$("#contentAddEdit").block({ message: null });
	if (validateForm("currencyConversionForm", true, "Messages", false)) {
		$("#contentAddEdit").unblock();
	} else {
		var item = new AuroraEntities.CurrencyConversions();
		$('#currencyConversionForm').mapToObject(item);
		item.CurrencyConversionID = item.CurrencyConversionID || 0;
		item.ClientSiteID = 0;
	}

	var response = function (result) {
		if (result.Data !== null && result.Count > 0) {
			$("#contentAddEdit").unblock();
			getCurrencies();
			$("#contentAddEdit").hide();
			DisplayMessage("Messages", "success", "Item saved", 7000);
			scrollToElement("Messages", -20, 2);
		} else {
			$('#contentAddEdit').unblock();
			DisplayMessage("Messages", "warning", Result.Data , 7000);
			scrollToElement("Messages", -20, 2);
		}
	};
	Aurora.Manage.Service.AuroraWS.UpdateCurrencyConverter(item, response, onError);
	//Aurora.Manage.Service.AuroraWS.UpdateCustomModule(customModule, response, onError, isDelete);
}
//#endregion

//#region Pagination 
function pageselectCallback(page_index, jq) {
	$('div.content').block({ message: null });
	if (page_index > 0) {
		//skip getData() on the first load or if causes and error
		CurrentPage = page_index;
		getCustomModules();
	} else if (CurrentPage > 0 && page_index == 0) {
		//make sure you run getData() if we are going back to the fist page
		CurrentPage = 0;
		getCustomModules();
	}
	//  $("#contentAddEdit").hide();
	$('div.content').unblock();
	return false;
}
function initPagination() {
	// Create content inside pagination element
	$("#Pagination").pagination(TotalCount, {
		callback: pageselectCallback,
		items_per_page: globals.PageSize,
		prev_text: '<<',
		next_text: '>>'
	});
}
//#endregion