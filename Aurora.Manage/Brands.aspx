﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Brands.aspx.cs" Inherits="Aurora.Manage.Brands" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" src="Brands.aspx.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span id="editing">Editing Brand</span>
        </div>
        <br class="clear" />
        <div class="content">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <input type="hidden" id="ID" runat="server"/>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                        <input type="text" runat="server" id="Name" name="Name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td>
                            <input type="text" runat="server" id="Description" name="Description" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
     <input id="btnSave" type="button" value="Save" onclick="Save();" />
    </form>
</body>
</html>
