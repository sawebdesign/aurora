﻿/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Innova/scripts/editor_source.js" />

//#region Vars 
var listTemplate;
var detailTemplate;
var parentPage;
var FeaturesList = null;
var parentIndex;
var currentData;
var newPage = false;
var Global = new GlobalVariables.GlobalVar();
var modulesList;
var selectedAnchor = null;
//#endregion

//#region Init
$(document).ready(function () {

    $('div.content').block({ message: null });
    loadList();

    $(".childtables").hide();
    $("#btnSave").hide();
    $("#btnSaveTop").hide();
    $('#pageReorderFancy').fancybox({
        padding: 0,
        titleShow: true,
        title: '',
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: 600,
        height: 600,
        type: "iframe",
        onClosed: function () {
            loadList();
        }
    });
    $("#pageSearchtxt").keydown(function (e) {
    	var code = e.which; // recommended to use e.which, it's normalized across browsers
    	if (code == 13) {
    		e.preventDefault();
    		SearchPages();
    	} 
    });

    $('#pageSearchbtn').click(SearchPages);

    $('#pageSearchClearbtn').click(function () {
    	$('#pageSearchtxt').val('');
    	loadList();
    });

});
//#endregion

function SearchPages() {
	var response = function (result) {
		if (result.Result == true) {
			currentData = result.Data;
			if (result.Data[0] != null) {
				listTemplate.set_data(result.Data);
				$('div.content').unblock();
				$('div.content').show();
				$("#listTemplate tr").each(function () {
					if ($(this).attr("isdefault") == "true" || $(this).attr("isdefault") == true) {
						$($(this.cells).children()[3]).css('display', 'none');
					}
					if ($(this).attr("childnodes") == 0) {
						$($(this.cells).children()[0]).css('display', 'none');
					}
				});
			} else {
				DisplayMessage("Messages", "warn", "No content available.")
				$("div.content").hide();
			}
		} else {
			DisplayMessage("Messages", "error", result.ErrorMessage)
		}
		// Add tooltip to edit and delete button
		$('.help').tipsy({ gravity: 'n' });
	};
	if ($('#pageSearchtxt').val().length > 0) {
		if (listTemplate == null) {
			listTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("listTemplate"));
		}
		$(".content").block({ message: null });
		Aurora.Manage.Service.AuroraWS.GetPagesBySearch($('#pageSearchtxt').val(), response, onPageError);
	}
}

//#region Custom Methods
function onPageError(result) {
    if (typeof result.status == "undefined") {
        onError(result);
    } else {
        DisplayMessage("Messages", "error", "status: " + result.status + " statusText: " + statusText)
    }

}
function removeNull(object) {
    if (object === null) {
        return "";
    }
    else {
        return object;
    }
}
//#endregion

//#region Data Manipulation
function showChild(anchor) {

    var hasChild = $(anchor).attr("haschildren");
    var id =  $(anchor).attr("pageid");
    var nodeState = $(anchor).attr("node");

    if (nodeState == "show") {
        anchor.setAttribute("node", "hide");
    }
    else {
        anchor.setAttribute("node", "show");
    }

    if (hasChild) {
        var row = $(anchor)[0].parentNode.parentNode.nextSibling;
        var myLevel = $(anchor)[0].parentNode.parentNode.getAttribute("level");
        while (row !== null && $(row).attr("level") > myLevel) {
            if ($(row).css("display") != "none") {
                if (nodeState == "hide") {
                    $(row).hide();
                }

            }
            else if ($(row).css("display") == "none" && row.getAttribute("level") > myLevel) {
                if (nodeState == "show") {
                    $(row).show();
                }
            }
            row = row.nextSibling;
        }

        return;
    }
    $(".content").block({ message: null });
    Aurora.Manage.Service.AuroraWS.GetPagesByParent(parseInt(id), onFetchChildren, onError);
    function onFetchChildren(result) {

        var insertIndex = anchor.parentNode.parentNode.rowIndex - 1;
        var level = parseInt(anchor.parentNode.parentNode.getAttribute("level")) + 1;
        var parentIndex = anchor.parentNode.parentNode.getAttribute("index") == undefined ? 0 : parseInt(anchor.parentNode.parentNode.getAttribute("index"));
        var html;
        var spacer = anchor.spacer != undefined ? anchor.spacer : anchor.getAttribute("spacer");
        for (var index = 0; index < result.Data.length; index++) {
            var entity = new AuroraEntities.PageContent(result.Data[index]);
            //Open Row
            html = String.format("<tr class='data child'  id='{0}' parentpage='{1}'  level='{2}' index='{3}' parentid='{4}' childnodes='{5}'>", entity.ID, entity.ParentId, level, entity.PageIndex, entity.ParentId, entity.HasChildren);
            //Long Title
           // html += String.format("<td title='{2}'>{0}{1}</td>", spacer, shortenString(entity.ShortTitle, 10), entity.ShortTitle);
            //Short Title
            html += String.format("<td title='{0}'>{1}<img src=\"../styles/images/childPage.png\"/>{2}</td>", entity.LongTitle, spacer, shortenString(entity.LongTitle, 40));
            //Description
            html += String.format("<td>/page/{0}</td>", (entity.Description != null ? entity.Description.toString().replace(/ /gi, "-").toLowerCase() : ""));

        	//Redirect URL
            html += String.format("<td>{0}</td>", "." + entity.RedirectURL == null ? "" : entity.RedirectURL);
        	//ID
            html += String.format("<td>{0}</td>", entity.ID);
            //Page Order
            html += String.format("<td>{0}</td>", "." + entity.PageIndex);
            //Functions
            //Show Children Button
            html += String.format("<td align='right' onclick=\"$('.rowSelected').removeClass('rowSelected');$(this.parentNode).addClass('rowSelected');\"><a href=\"javascript:void(0);\"  node=\"show\" onclick=\"showChild(this); return false;\" pageid=\"{0}\" spacer=\"{1}\" >", entity.ID, spacer + "&nbsp;&nbsp;");
            html += "<img src=\"../styles/images/arrow_branch.png\" alt=\"edit\" class=\"help\" title=\"Show Sub Pages\" /></a>&nbsp;";
            //Add New Page Button
            html += String.format("<a href=\"javascript:void(0);\" ischild=\"true\" thisid=\"{0}\" parentid=\"{1}\" onclick=\"newPage=true;parentPage=$(this).attr('thisid');loadDetail(this);return false;\" >", entity.ID, entity.ParentID);
            html += "<img src=\"Styles/images/add.png\" alt=\"add\" class='help'  title=\"Add Sub Page\" style=\"cursor: pointer;\ title=\"Create a category\" /></a>&nbsp;";
            //Edit Button
            html += String.format("<a href=\"javascript:void(0);\" onclick=\"newPage=false;parentPage=this.parentpage; loadDetail(this);return false;\" pageid=\"{0}\" pageindex=\"{1}\" parentpage=\"{2}\">", entity.ID, entity.PageIndex, entity.ParentID);
            html += "<img src=\"../styles/images/icon_edit.png\" alt=\"edit\" class=\"help\" title=\"Edit\" /></a>&nbsp;";
            html += String.format("<a href=\"javascript:void(0);\" onclick=\"deletePage(this);return false;\" pageid=\"{0}\">", entity.ID);
            html += "<img src=\"../styles/images/icon_delete.png\" alt=\"delete\" class=\"help\" title=\"Delete\" /></a>";
            //Close Row
            html += "</td></tr>";

            $(html).insertAfter($('.data > tbody > tr').eq(insertIndex));
            insertIndex++;
            anchor.haschildren = true;

            // Add tooltip
            $(".tipsy").remove();
            $(".help").tipsy({ gravity: 's', fade: true, title: 'title', trigger: 'hover', delayIn: 0, delayOut: 0 });
        }
        $("#listTemplate tr[childnodes='0'] td img[src='../styles/images/arrow_branch.png']").hide();


        $(".content").unblock();

    }

}
function loadList() {
    $('div.content').block({ message: null });
    if (listTemplate == null) {
        listTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("listTemplate"));
    }
    Aurora.Manage.Service.AuroraWS.GetPages(0, onloadDetailsSuccess, onPageError);

    function onloadDetailsSuccess(result) {
        if (result.Result == true) {
            currentData = result.Data;
            if (result.Data[0] != null) {
                listTemplate.set_data(result.Data);
                $('div.content').unblock();
                $('div.content').show();
                $("#listTemplate tr").each(function () {
                    if ($(this).attr("isdefault") == "true" || $(this).attr("isdefault") == true) {
                        $($(this.cells).children()[3]).css('display', 'none');
                    }
                    if ($(this).attr("childnodes") == 0) {
                        $($(this.cells).children()[0]).css('display', 'none');
                    }
                });
            } else {
                DisplayMessage("Messages", "warn", "No content available.")
                $("div.content").hide();
            }
        } else {
            DisplayMessage("Messages", "error", result.ErrorMessage)
        }
        // Add tooltip to edit and delete button
        $('.help').tipsy({ gravity: 'n' });

    }
}
function loadItem(id) {
    if (id) {
        var onLoadPageSuccess = function (result) {

            var columns = $("#" + result.Data.ID + " td");
            if (($("#" + result.Data.ID).attr("level")) * 1 === 0) {
                columns[0].innerHTML = result.Data.ShortTitle;
            }
            else {
                columns[0].innerHTML = $($("#" + result.Data.ID)[0].previousSibling)[0].lastChild.firstChild.getAttribute("spacer") + "<img src=\"/styles/images/childPage.png\"/>" + result.Data.ShortTitle;
            }
            //columns[1].innerHTML = result.Data.LongTitle
            columns[1].innerHTML = String.format("/page/{0}", result.Data.Description.replace(/ /gi, "-").toLowerCase());
            columns[2].innerHTML = result.Data.RedirectURL;
            //columns[3].html(result.Data.PageIndex);

            $("#" + result.Data.ID + " td:nth-child(5)").html(result.Data.PageIndex);
        }
        Aurora.Manage.Service.AuroraWS.GetPages(id, onLoadPageSuccess, onPageError)
    }

}
function loadDetail(anchor, del) {

    selectedAnchor = anchor;
    var pageid = -1;
    parentPage = (parentPage == null) ? anchor.parentNode.parentNode.getAttribute("parentid") : parentPage;

    if (parentPage != null && anchor.parentNode.parentNode.getAttribute("parentid") != "0")
        parentIndex = $("#" + anchor.parentNode.parentNode.getAttribute("parentid")).length > 0 ? $("#" + anchor.parentNode.parentNode.getAttribute("parentid"))[0].getAttribute("index") : 0;
    else
        parentIndex = "";
    var pageIndex = anchor.pageindex != undefined ? anchor.getAttribute("pageindex") : 0;
    if ($(anchor).attr("pageid") !== undefined) {
        pageid = $(anchor).attr("pageid");
        $("parentindex").val(parentIndex);
        parentPage = (anchor.parentNode.parentNode.getAttribute("parentid")) ? anchor.parentNode.parentNode.getAttribute("parentid") : 0;
    }

    $("#btnSave").show();
    $("#btnSaveTop").show();
    $("#contentAddEdit").show();
    $('div.detail').block({ message: null });


    if (pageid == -1) {

        var page = new AuroraEntities.PageContent;
        for (var property in page) {
            page[property] = "";
        }
        page.ID = -1;
        if (anchor.ischild) {
            page.ParentId = $(anchor).attr("parentId");
        }

    }

    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1 || responseText.indexOf("<title>Runtime Error</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $('div.detail').unblock();
        }
        $("#contentAddEdit").unblock();
    }
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    $("#contentAddEdit").load(String.format("Page.aspx?PageId={0}&index={1}&var={2}&dx={3}", pageid, parentIndex, x.getMilliseconds(), del), success);

    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);


}
function deletePage(anchor) {
    if (confirm("Item will be removed permanently,\nDo you wish to continue?")) {
        if ($(anchor).attr("pageid")) {

            function getPage(result) {
                if (result.Data != "" && result.Result) {
                    var page = new AuroraEntities.PageContent(result.Data);
                    page.DeletedOn = new Date();
                    function onDelete(result) {
                        if (result.Result) {
                            loadList();
                            $("#contentAddEdit").hide();
                            DisplayMessage("Messages", "info", "Page was successfully removed.", 3, 5000);
                            $("#btnSaveTop").hide();
                            $("#btnSave").hide();
                        }
                    }
                    Aurora.Manage.Service.AuroraWS.UpdatePage(page, onDelete, onError);

                }
            }
            Aurora.Manage.Service.AuroraWS.GetPages($(anchor).attr("pageid"), getPage, onError);
        }
        else {
            document.title = "no PageID";
        }
    }
}
//elton 29/11/11  Trims an  string 
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}
function saveData() {

	if (validateForm("detailTemplate", true, "Messages", false)) {
		$('#tab1').click();
        return;
    }  

    var pageEntity = new AuroraEntities.PageContent;
    Global.populateEntityFromForm(pageEntity, $('#form').serializeArray());
    for (var property in pageEntity) {
        $control = $("#" + property);
        if ($control.length > 0) {
            pageEntity[property] = $control.val();
        }
        if (pageEntity[property] == "null") {
            pageEntity[property] = null;
        }

    }
    pageEntity.PageContent1 = oEdit1.getHTMLBody();


    pageEntity.InsertedOn = new Date(pageEntity.InsertedOn) === undefined ? new Date : pageEntity.InsertedOn;
    pageEntity.EntityKey = new Object();
    pageEntity.EntityKey.EntityContainerName = "AuroraEntities";
    pageEntity.EntityKey.EntitySetName = "PageContent";
    pageEntity.EntityKey.IsTemporary = false;
    pageEntity.EntityKey.EntityKeyValues = new Array();
    objKey = new Object();
    objKey.Key = "ID";
    pageEntity.ID = pageEntity.ID == "" ? -1 : parseInt(pageEntity.ID);
    objKey.Value = parseInt(pageEntity.ID) ? parseInt(pageEntity.ID) : -1;
    pageEntity.ParentId = parentPage;
    pageEntity.isArchived = pageEntity.isArchived == "" ? false : pageEntity.isArchived;
    pageEntity.isDefault = $("#isDefault").is(":checked");
    pageEntity.DeletedOn = (pageEntity.DeletedOn == "") ? null : pageEntity.DeletedOn;
    pageEntity.DeletedBy = (pageEntity.DeletedBy == "") ? null : pageEntity.DeletedBy;
    pageEntity.InsertedBy = (pageEntity.InsertedBy == "") ? null : pageEntity.InsertedBy;
    pageEntity.InsertedOn = (pageEntity.InsertedOn == "" || pageEntity.InsertedOn == "NaN") ? new Date() : pageEntity.InsertedOn;
    pageEntity.PageIndex = pageEntity.PageIndex == null ? -1 : pageEntity.PageIndex;

    //alert($("#PageIndex").length);

    if ($("#showPage").attr("checked"))
        pageEntity.PageIndex = pageEntity.PageIndex == null ? -1 : (pageEntity.PageIndex * -1);

    pageEntity.EntityKey.EntityKeyValues[0] = objKey;
    pageEntity.RedirectURL = $("#RedirectURL").val();
    pageEntity.PermanentRedirect = $('#PermanentRedirect').is(':checked');
    pageEntity.IsSecured = $("#IsSecured").is(":checked");
    if (pageEntity.IsSecured) {
        pageEntity.UserName = $("#UserName").val();
        pageEntity.Password = $("#Password").val();
    }


   
    pageEntity.isDefault = pageEntity.isDefault == "on" || pageEntity.isDefault ? true : false;



    Aurora.Manage.Service.AuroraWS.UpdatePage(pageEntity, onSaveDataSuccess, onError);

}
function onSaveDataSuccess(result) {
    if (result.Result == true) {
        DisplayMessage("Messages", "success", "Page was successfully saved.", 5000);
        scrollToElement("Messages", -20, 2);
        if (newPage) {
            $("#contentAddEdit").hide();
            $("#btnSaveTop").hide();
            $("#btnSave").hide();
            newPage = false;
            loadList();
        }
        else {
            loadItem($("#ID").val());
        }
    } else {
        DisplayMessage("Messages", "error", result.ErrorMessage, 10000)
    }
}
//#endregion
