﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Aurora.Manage.Account.Login" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Website Title -->
    <title>BoltCMS | Login</title>
    <!-- Meta data for SEO -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- Template stylesheet -->
    <link href="/Styles/Black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/Styles/Black/datepicker.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/Scripts/visualize/visualize.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="Shortcut Icon" href="~/Styles/images/bolticon.ico"/>
    <!--[if IE]>
	<link href="~/Styles/ie.css" rel="stylesheet" type="text/css" media="all">
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
<![endif]-->
    <!-- Jquery and plug-in -->
    <script src="../Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="../Scripts/hint.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">
        $(function () {
            // find all the input elements with title attributes
            $('#login_info').slideToggle('fast');
            $('input[title!=""]').hint();

            $('#login_info').click(function () {
                $(this).slideToggle('fast');
            });

            if ($('#login_info').is(":visible") && $(".failureNotification")[0].innerHTML != "") {
                $('#login_info').slideToggle('fast');
            }

            if ($.browser.msie == null) {
                if ($(".failureNotification")[0].innerHTML != "") {
                    $(".failureNotification").append("We recommend using Internet Explorer when logging into the manage area.")
                    //$('#login_info').slideToggle('fast');
                }
            }
        });
    </script>
</head>
<body class="login">
    <form id="form1" runat="server">
    <!-- Begin login window -->
    <div id="login_wrapper">
        <asp:Login ID="LoginUser" runat="server" DestinationPageUrl="~/Dashboard.aspx" EnableViewState="false" RenderOuterTable="false">
            <LayoutTemplate>
                <br class="clear" />
                <div id="login_top_window">
                    <img src="../styles/images/black/top_login_window.png" alt="top window" />
                </div>
                <!-- Begin content -->
                <div id="login_body_window">
                    <div class="inner">
                        <img src="../styles/images/login_logo.gif" alt="logo" />
                        <div class="accountInfo">
                            <fieldset class="login">
                                <p>
                                    <asp:TextBox ID="UserName" runat="server" name="username" Style="width: 285px" title="Username"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"
                                        name="password" Style="width: 285px" title="******"></asp:TextBox>
                                </p>
                                <p style="margin-top: 40px">
                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" class="Login"
                                        Style="margin-right: 5px" />
                                    <asp:CheckBox ID="RememberMe" name="remember" runat="server" />Remember my password
                                </p>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div id="login_info" class="alert_warning" style="width: 350px; margin: auto; padding: auto;">
                    <p>
                        <img src="../styles/images/icon_warning.png" alt="success" class="mid_align" />
                        <span class="failureNotification">
                            <asp:Literal ID="FailureText" runat="server" ClientIDMode="Static"></asp:Literal>
                        </span>
                    </p>
                </div>
                <!-- End content -->
                <div id="login_footer_window">
                    <img src="../styles/images/black/footer_login_window.png" alt="footer window" />
                </div>
                <div id="login_reflect">
                    <img src="../styles/images/black/reflect.png" alt="window reflect" />
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
    <!-- End login window -->
    </form>
</body>
</html>
