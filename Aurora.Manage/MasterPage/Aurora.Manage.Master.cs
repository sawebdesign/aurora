﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage {
    public partial class SiteMaster : System.Web.UI.MasterPage {
        public string lastModifiedDate = new FileInfo(HttpContext.Current.Server.MapPath("~/Scripts/Utils.js")).LastWriteTime.ToString("ddMMyyyyHHss");
        protected void Page_Load(object sender, EventArgs e) {

            Custom.Data.AuroraEntities auroraEntities = new Custom.Data.AuroraEntities();
            if (SessionManager.ClientSiteID == 0) {
                FormsAuthentication.SignOut();
                Response.Redirect("~/Public/login.aspx?ReturnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
            }
            else {
                //set site data cache object 
                var clientData = (from clients in auroraEntities.ClientSite
                                  where clients.ClientSiteID == SessionManager.ClientSiteID
                                  select clients).SingleOrDefault();

                CacheManager.SiteData = clientData;


            }
            //get first valid domain
            var selectedDomain = (from domain in auroraEntities.Domain
                                 where domain.DomainName.Contains("boltcms.com") == false
                                       && domain.ClientSiteID == SessionManager.ClientSiteID
                                       && domain.DomainName.Contains("www") == true
                                 select domain.DomainName).FirstOrDefault();
            if (selectedDomain != null)
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "domainname",
                                                        "var clientDomainName='http://" + selectedDomain + "'", true);    
            }
            else
            {
                selectedDomain = (from domain in auroraEntities.Domain
                                  where domain.DomainName.Contains("boltcms.com") == true
                                        && domain.ClientSiteID == SessionManager.ClientSiteID
                                  select domain.DomainName).FirstOrDefault();
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "domainname",
                                                        "var clientDomainName='http://" + selectedDomain + "'", true);    
            }
            
        }
    }
}
