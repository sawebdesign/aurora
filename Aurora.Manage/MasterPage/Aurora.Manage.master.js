﻿/// <reference path="../Scripts/Utils.js" />
/// <reference path="../Service/AuroraWS.asmx/jsdebug" />
/// <reference path="../Scripts/jquery-cookie.js" />


$(function () {
    var shortcutsshow = $.cookie('shortcuts') == null ? "false" : $.cookie('shortcuts');
    try {
        LoadGlobalConfig();
        $("#shortcut li.hideable").hide();
        showShortCuts();
    } catch (e) {
        alert(e);
    }

    setInterval(function () { keepAlive(); }, 30000);
    //set focus to fix focus bug;
    $("#contentAddEdit").mouseover(
        function () {
            $("#contentAddEdit input[type='text']:visible").click(
                function () {
                    $(this).focus();
                }
            );


        }
    )

    //shortcuts
    $("#shortcutBtn").click(
        function () {
            if ($("#shortcuts").css("height") == "100px") {
                $("#shortcut").fadeOut(500).hide();
                $("#shortcuts").animate(
                { height: 0 },
                500
            );
                shortcutsshow = true;
                $.cookie('shortcuts', shortcutsshow, { expires: 60 });
            }
            else {
                $("#shortcuts").animate(
                    { height: 100 },
                    600,
                    'easeOutBack'
                );
                $("#shortcut").fadeIn(500);
                shortcutsshow = false;
                $.cookie('shortcuts', shortcutsshow, { expires: 60 });
            }
        }
    );

    $("#Bug").fancybox({
        padding: 0,
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        titleShow: true,
        title: "Bug Report",
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        autoDimensions: false,
        width: 800,
        height: 510,
        onStart: function () { },
        onClosed: function () {

        }
    });

    //Send bug report
    $("#btnSendBugReport").click(
        function () {
            var response = function () {
                $("#BugReportMessage").html("");
                DisplayMessage("BugMessage", "success", "We have recieved your report, and will update you on its progress", 3000);
                setTimeout(function () { $.fancybox.close(); }, 3000);
            };
            Aurora.Manage.Service.AuroraWS.SendMail("support@sawebdesign.co.za", "Bug report", $("#BugReportMessage").html(), "Client:" + client.replace("~/ClientData/", "") + " has sent a bug report", response, onError);
        }
    );

    if (!Boolean.parse(shortcutsshow)) {
        $("#shortcutBtn").click();
        shortcutsshow = true;
    }

    //global tooltips
    if (document.location.toString().toLowerCase().indexOf("pageaed.aspx?") == -1) {
        var timerID = setInterval(function () {
            stopTimer = 0;
            if ($("img[src='../styles/images/icon_edit.png']").not(".tipsy").length != 0) {
                $("img[src='../styles/images/icon_edit.png']").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            }
            else {
                stopTimer++;
            }

            if ($("img[src='../styles/images/icon_delete.png']:not(.tipsy)").length != 0) {
                $("img[src='../styles/images/icon_delete.png']").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            }
            else {
                stopTimer++;
            }

            if (stopTimer === 2) {
                clearInterval(timerID);
            }
        }
    , 800);
    }
    

});
function ResizeFrame(element) {
    var mheight;
    var mwidth;
    mheight = document.body.scrollHeight;
    mwidth = document.body.scrollWidth;
    var iframeElement = parent.document.getElementById(element);
    iframeElement.style.height = mheight + "px";  //PX TO FORCE OTHER BROWSERS
    //iframeElement.style.width = mwidth+""//= "700; //100px or 100% 
}

