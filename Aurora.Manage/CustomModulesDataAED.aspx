﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="CustomModulesDataAED.aspx.cs" Inherits="Aurora.Manage.CustomModulesDataAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="CustomModulesDataAED.aspx.js"></script>
    <div class="onecolumn">
        <div class="header">
            <span>Custom Module Data</span>
            <div style="float: left; margin-top: 5px; margin-left: 10px;">
                <select id="modules" class="sys-template" onchange="createGrid(this.value);">
                    <option value="{{ID}}">{{Name}}</option>
                </select>
            </div>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img src="Styles/images/report_go.png" class="help" mytitle="Export Data" title="Create"
                    style="cursor: pointer" onclick="exportData();" />
            </div>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img src="Styles/images/add.png" class="help" mytitle="Create Item" title="Create"
                    style="cursor: pointer" onclick="editCustomData(0,$('#modules').val());" />
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <table class="data" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                    <tr id="columns" class="sys-template">
                        <th align="left">
                            {{Name}}
                        </th>
                    </tr>
                </thead>
                <tbody  id="template">
                    <tr id="templateRow" class="sys-template">
                        <td align="left">
                            {{Name}}
                        </td>
                        <td align="left">
                            {{Description}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);" onclick="editCustomData($(this).attr('dataid'),$(this).attr('modid')); return false;"
                                dataid="{binding ID}" modid="{{ModuleID}}">
                                <img src="../styles/images/icon_edit.png" alt="edit" class="tipper" mytitle="Edit" /></a>
                            <a href="javascript:void(0);" onclick="deleteCustomData($(this).attr('dataid'),$(this).attr('modid')); return false;"
                                dataid="{binding ID}" modid="{{ModuleID}}">
                                <img src="../styles/images/icon_delete.png" alt="delete" class="tipper" mytitle="Delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br class="clear" />
        </div>
    </div>
    <div id="Messages">
    </div>
    <div>
        <iframe width="100%" height="auto" scrolling="no" id="contentAddEdit" frameborder="0" allowtransparency="true"></iframe>
    </div>
</asp:Content>
