﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Aurora.Custom.Data;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI.WebControls;
using Aurora.Custom;


namespace Aurora.Manage
{
    public partial class Banner : System.Web.UI.Page
    {
        private AuroraEntities context = new AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utils.CheckSession();
            bannerImage.Visible = false;
            if (Request["BannerId"] != null)
            {
                int bannerID;
                int.TryParse(Request["BannerId"], out bannerID);
                var bannerDetails = (from banners in context.AdBanner
                                     where banners.BannerID == bannerID
                                     select banners).SingleOrDefault();

                if (bannerDetails != null)
                {
                    ClientSiteID.Value = bannerDetails.ClientSiteID.Value.ToString();
                    BannerID.Value = Utils.ConvertDBNull(bannerDetails.BannerID, string.Empty);
                    Name.Value = bannerDetails.Name;
                    RedirURL.Value = bannerDetails.RedirURL;
                    Alttext.Value = bannerDetails.Alttext;
                    Weight.Value = Utils.ConvertDBNull(bannerDetails.Weight, string.Empty);
                    UnderText.Value = bannerDetails.UnderText;
                    UnderURL.Value = bannerDetails.UnderURL;
                    UnderClickCount.Value = Utils.ConvertDBNull(bannerDetails.UnderClickCount, string.Empty);
                    xsize.Value = Utils.ConvertDBNull(bannerDetails.xsize, string.Empty);
                    ysize.Value = Utils.ConvertDBNull(bannerDetails.ysize, string.Empty);
                    ValidFromDate.Value = DateTime.Parse(Utils.ConvertDBNull(bannerDetails.ValidFromDate, string.Empty)).ToString("dd/MM/yyyy");
                    ValidToDate.Value = DateTime.Parse(Utils.ConvertDBNull(bannerDetails.ValidToDate, string.Empty)).ToString("dd/MM/yyyy");
                    ShowCount.Value = Utils.ConvertDBNull(bannerDetails.ShowCount, string.Empty);
                    ClickCount.Value = Utils.ConvertDBNull(bannerDetails.ClickCount, string.Empty);
                    FarmID.Attributes.Add("selectID", Utils.ConvertDBNull(bannerDetails.FarmID, string.Empty));
                    MaxImpressions.Value = Utils.ConvertDBNull(bannerDetails.MaxImpressions, string.Empty);
                    MaxClicks.Value = Utils.ConvertDBNull(bannerDetails.MaxClicks, string.Empty);
                    Category.Value = bannerDetails.CategoryID;
                    IsHtml.Value = bannerDetails.IsHtml.ToString();
                    bannerImage.Src = bannerDetails.BannerURL;
                    TypeName.Value = Utils.ConvertDBNull(bannerDetails.TypeName, string.Empty);
                    if (!String.IsNullOrEmpty(bannerImage.Src)) {
                        bannerImage.Visible = true;
                    }

                }
                //get Zones
                Aurora.Manage.Service.AuroraWS BannerPages = new Service.AuroraWS();
                var zones = BannerPages.GetAllPageBanners()["Data"];



                //where zone.Key == "Data"
                //select new
                //{
                //   val = zone.Value
                //}).FirstOrDefault();

                rptrZoneOptions.DataSource = zones;
                rptrZoneOptions.DataBind();
            };

        }
    }
}
