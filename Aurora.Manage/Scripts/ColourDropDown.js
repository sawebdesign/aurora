﻿//#region refs
/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.js" />
/// <reference path="MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Utils.js" />
/// <reference path="jquery.toChecklist.js" />
/// <reference path="generated/Aurora.Custom.js" />
//#endregion
var init = false;
var hasSelected = false;

var colourSelector = function (objOptions) {

    var selectedItem = null;
    var colourDropDowns = $.find(".dropdownSel");
    var colorData = [];
    init = true;
    $(colourDropDowns).each(function (index, domEle) {

        objOptions.currentItem = objOptions.currentItem.toString().split(',');

        $(objOptions.currentItem).each(
            function () {
                if (this != "")
                    $("#" + this).addClass("thumbsSelected");
            }
        );
        //populate array
        $(".thumbsSelected").each(
            function () {
                colorData[colorData.length] = $(this).attr("id");
            }
        );


        var container = $("<div></div>");
        container.attr("id", "ddlContainer");
        $(container).insertBefore($(this));

        var dropdownOff = String.format("<div class='ddLlist'><div class='swatch' style='background-color:#{0};'></div><div class='description'>None</div><div class='ddlListArrow'/></div>", $("#" + colorData[0]).attr("color"));

        $(dropdownOff).appendTo(container);

        $("#" + colorData[0]).attr("class", "thumbsSelected");

        $(".description").text($("#" + colorData[0]).attr("mytitle"));

        $(this).hide();

        //create thumbs
        var thumbs = $("<div class='thumbs'><img id=\"colour\" mytitle=\"Add a new colour to your color spectrum.\" class=\"swatcher\" alt=\"colour picker\" src=\"Styles/images/color_swatch.png\" style=\"cursor: pointer;float:left;\" /></div>");
        $(this).appendTo(thumbs);
        $(thumbs).appendTo(container);
        $(this).show();
        $(thumbs).hide();

        //set events
        $(".ddLlist").click(function (e) {
            $(".thumbs").slideToggle(550);
            return false;

        }).mouseout(
            function () {
                hasSelected = false;
            }
        );

        //remove any previous click events
        $(document).unbind('click');

        //append click events
        $(document).click(function () {
            if ($(".thumbs").is(":visible")) {
                $(".thumbs").slideToggle(550);
            }
        });
        $(".thumbs").click(function () {
            return false;
        });



        //create colour picker
        $('#colour').ColorPicker({
            eventName: "click",
            flat: false,
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).ColorPickerHide();
                colour.add(hex);
            }
        });
        $("<div style='float:left;font-size:7pt;color:#ffffff;'>Double click a colour to select/deselect it.</div>").insertAfter($("#colour"));
        $("#ColourChoose div").each(
                function () {
                    if ($(this).attr("color")) {
                        var color = "#" + $(this).attr("color").replace(" ", "").trim();

                        this.style.backgroundColor = color != "#" ? color : "";

                        $(this).dblclick(function (e) {
                            e.preventDefault();
                            var item = $(this);
                            if ($(this).attr("class") == "thumbsSelected") {
                                $(colorData).each(
                                    function (index, ele) {
                                        if (this == $(item).attr("id")) {
                                            colorData.splice(index, 1);
                                        }
                                    }
                                );

                                $(this).attr("class", "");
                            }
                            else {
                                //change selected colour
                                $(".swatch").attr('style', "background-color:#" + $(this).attr("color").toString());
                                $(this).attr("class", "thumbsSelected");
                                $(".thumbsSelected").each(
                                    function () {
                                        var found = false;
                                        var id = $(this).attr("id");
                                        $(colorData).each(
                                            function () {
                                                if (this == id) {
                                                    found = true;
                                                }
                                            }
                                        );
                                        if (!found) {
                                            colorData[colorData.length] = $(this).attr("id");
                                        }


                                    }
                                );

                                $(".swatch").data("selected", colorData);
                                $(".description").text($(this).attr("mytitle"));

                            }

                        }).mousedown(function () { hasSelected = true; })

                    }
                }

                );
        $(".swatcher,.thumbsSelected").tipsy({ gravity: 'n', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    }
    );
}   

