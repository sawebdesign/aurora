﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="jquery-ui-1.8.11.custom.min.js" />


//#region Global Utilities 
var JSSession = {};
var editorTimer = 0;
var oEdit1 = null;
var GlobalDataSets = [];
var GlobalVariables = new function () {

    this.GlobalBase = function () {
        var t = this;
    };

    this.GlobalVar = function (wo) {
        var t = this;
        t.type = "Global Variables:#Aurora";
    };

    this.GlobalVar.prototype = new this.GlobalBase();
    this.GlobalVar.prototype.constructor = this.GlobalVar;
    this.GlobalVar.prototype.PageSize = 10;
    this.GlobalVar.prototype.TimeOutID = 0;
    this.GlobalVar.prototype.GlobalDataSet = [];

    this.GlobalVar.prototype.populateEntityFromForm = function (entity, formsource, formfieldprfxlength) {
        //if we've passed in an array of form elements then set the properties to the values in the array if they match
        if (formsource !== undefined) {
            if ((formfieldprfxlength == undefined) || (formfieldprfxlength == null))
                formfieldprfxlength = 0;

            for (var i = 0; i < formsource.length; i++) {
                //if (formsource[i].name.substr(formfieldprfxlength, entname.length + 1) == entname + "_") {
                if (typeof entity[formsource[i].name.substr(formfieldprfxlength)] != "undefined")
                    entity[formsource[i].name.substr(formfieldprfxlength)] = formsource[i].value;
                //}
            }
        }
    };

    this.BindItemToDataSet = function (elementName, dataSet) {
        var tempDS = Sys.Application.findComponent(elementName);

        if (!tempDS) {
            tempDS = $create(Sys.UI.DataView, {}, {}, {}, $get(elementName));
        }
        else {
            tempDS.dispose();
            tempDS = null;
            tempDS = $create(Sys.UI.DataView, {}, {}, {}, $get(elementName));
        }

        tempDS.set_data(dataSet);

        return tempDS;
    };

}

function LoadGlobalConfig() {
    var onSuccess = function (result) {
        for (var index = 0; index < result.Data.length; index++) {
            if (result.Data[index].clientModules.ConfigXML || result.Data[index].featureModules.ConfigXML) {
                var file = (result.Data[index].clientModules.ConfigXML !== null) ? result.Data[index].clientModules.ConfigXML : result.Data[index].featureModules.ConfigXML;
                xmlDoc = $.parseXML(file);
                $xml = $(xmlDoc);
                $fields = $xml.find('field');
                JSSession;
                $.each($fields, function (i, field) {
                    SetJSSession($(field).attr('fieldname'), $(field).attr('value'));
                });
            }
        }
    };
    //todo figure out why this is unavailible on testimonies
    var x = function () { };
    Aurora.Manage.Service.AuroraWS.GetModuleConfig(0, 0, "", onSuccess, x);


}

var scriptsToLoad = [];

function loadScript(url, callback, index) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                script.onreadystatechange = null;
                if (callback) {
                    callback(index + 1);
                }
            }
        };
    } else {  //Others
        script.onload = function () {
            if (callback) {
                callback(index + 1);
            }
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function GetJSSession(getVar) {
    /// <summary>Gets the value of the requested getVar from the JSSession</summary>
    /// <param name="getVar" type="String">The name of the variable you require</param>
    return JSSession[getVar];
};

function SetJSSession(setVar, value) {
    /// <summary>Set the variable setVar and value into the JSSession</summary>
    /// <param name="setVar" type="String">The name of the variable you want to set</param>
    /// <param name="value" type="String">The value of the variable you are settings</param>
    JSSession[setVar] = value;
};

/// <summary>Keeps the server client connection alive</summary>
function keepAlive() {
    try {
        $("#dialog").dialog("close");
        $("#dialog").remove();
        var request = function (result) {
            if (result["ErrorMessage"]) {
                if (result.ErrorMessage == "SessionExpired" || result.ErrorMessage == "Runtime Error") {
                    $("<div id='dialog'>Sorry but your session has expired, you will now be redirected to the login screen</div>").dialog({ modal: true,
                        resizable: false,
                        title: "Error",
                        stack: false,
                        zIndex: 1200,
                        buttons: { "Okay": function () { $(document.body).attr("disabled", "disabled"), document.location = document.URL; $(this).dialog("close"); } }
                    });

                }
            }

        };
        var fail = function (result) {
            if (result._message == "SessionExpired" || result._message == "Runtime Error") {
                $("<div id='dialog'>Sorry but your session has expired, you will now be redirected to the login screen</div>").dialog({ modal: true,
                    resizable: false,
                    title: "Error",
                    buttons: { "Okay": function () { document.location = document.URL; $(this).dialog("close"); } }
                }); ;

            }
            else {
                $("<div id='dialog'>Please check your internet connection as we cannot make contact with the server.</div>").dialog({ modal: true,
                    resizable: false,
                    title: "Error",
                    buttons: { "Okay": function () { $(this).dialog("close"); } }
                }); ;
            }
        }
        Aurora.Manage.Service.AuroraWS.KeepAlive(request, fail);
    } catch (e) {
        $("<div id='dialog'>Please check your internet connection as we cannot make contact with the server.</div>").dialog({ modal: true,
            resizable: false,
            title: "Error",
            buttons: { "Okay": function () { $(this).dialog("close"); } }
        }); ;
    }
}
//#endregion

//#region String Utilities 
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}
String.prototype.isNullOrEmpty = function (replace) {
    /// <summary>Checks if a string is set to empty or null</summary>
    /// <param name="replace" type="String">If specified then returns that value, if not returns T|F</param>
    if (!this || this == "null") {
        if (!replace) {
            return true;
        }
        else {
            return replace;
        }
    }
    if (this == "") {
        if (!replace) {
            return true;
        }
        else {
            return replace;
        }
    }
    return this;
};
String.prototype.ReplaceSpecialChars = function () {
    return this.replace(/[^\w\s]/gi, '');

};
function PadZero(padint) {
    if (padint < 10) {
        return '0' + padint;
    } else {
        return padint;
    }
}

function encodeHTML(html) {
    return String(html)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}
function decodeHTML(html) {
    var str = html;
    str = str.replace(/&quot;/g, '"');
    str = str.replace(/&amp;/g, "&");
    str = str.replace(/&lt;/g, "<");
    str = str.replace(/&gt;/g, ">");

    return str;
};
function removeLineBreaks(text, replaceWith) {
    replaceWith = !replaceWith ? "" : replaceWith;
    var parsedText = text.replace(String.fromCharCode(10), "", text);
    return text.replace(String.fromCharCode(13), "", text);

}
function shortenString(stringValue,length) {
    if (stringValue.length > length) {
        stringValue = stringValue.substr(0, length)+"...";
    }

    return stringValue;
}
//#endregion

//#region URL Utilities 

function getQueryVariable(variable) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="variable" type="String">
    ///		The name of the query string param that you are looking for
    ///	</param>

    var query = urldecode(window.location.search.substring(1));
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var check = pair[0];
        if (check.toLowerCase() === variable.toLowerCase()) {
            return pair[1];
        }
    }
    return null;
}

function urldecode(str) {
    str = str.replace('+', ' ');
    str = unescape(str);
    return str;
}
//#endregion

//#region Array Utilities 

function findInArray(ary, idfield) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The name of the column that you want to interrogate
    ///	</param>

    for (var i = 0; i < ary.length; i++) {
        if (ary[i].name == idfield) {
            return ary[i].value;
        }
    }
    return null;
};

function findByInArray(ary, idfield, value) {
    ///	<summary>
    ///		Get elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The name of the column that you want to interrogate
    ///	</param>
    ///	<param name="value" type="Variable">
    ///		The value for the idfield column
    ///	</param>

    for (var i = 0; i < ary.length; i++) {
        if (value == ary[i][idfield]) {
            return ary[i];
        }
    }
    return null;
};

function populateEntityFromForm(entity, formid) {
    /// <summary>Populates an entity object from a specified form</summary>
    /// <param name="entity" type="AuroraEntities"></param>
    /// <param name="formid" type="String">No # required</param>
    for (var property in entity) {
        $control = $("#" + formid + " #" + property);
        if ($control.length > 0) {
            entity[property] = $control.val();
        }
    }
}

function findSetByInArray(ary, idfield, value) {
    ///	<summary>
    ///		Returns an array of objects where the idfield's value is == value
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The name of the column that you want to interrogate
    ///	</param>
    ///	<param name="value" type="Variable">
    ///		The value for the idfield column to compare against.
    ///	</param>
    var arraySet = [];
    for (var i = 0; i < ary.length; i++) {
        var fieldval = ary[i][idfield];
        if (value == ary[i][idfield]) {
            arraySet[arraySet.length] = ary[i];
        }
    }
    return arraySet;
}

//#endregion

//#region Error Utilities 
function onError(result) {
    if (result["_messageresult"]) {
        keepAlive();
    }
    else {

        var success = function () { };
        var error = function () { };
        var execp = new Error();
        var pagePrint = "";

        $(".inner div:visible").each(
            function (index, elem) {
                pagePrint += pagePrint.indexOf(elem.innerHTML) == -1 ? elem.innerHTML : "";
            }
        );
        execp.name = "UnExpected Error!";

        if (result["ErrorMessage"]) {
            //Server will handle this error with its own error mail service.
            execp.message = result.ErrorMessage;
            throw (execp);
        }
        else {

            execp.message = String.format("<b>Page:</b>{0}<br/><b>Type:</b>{1}<br/><b>Message:</b>{2}<br/><b>Stack Trace:</b>{3}<br/><b>Page</b>{4}<br/><b>JSStack trace:</b>{5}", document.location, result._exceptionType, result._message, result._stackTrace, printStackTrace());
        }
        if (result["ErrorMessage"]) {
            DisplayMessage("Messages", "error", "Sorry but an unexpected error has occured, support has been notified we will respond shortly.");
            //Aurora.Manage.Service.AuroraWS.SendMail("support@sawebdesign.co.za", "Aurora", execp.message + "<br/><br/>Page Print:<br/>" + pagePrint, String.format("Error on manage for Client:{0}", client.replace("~/ClientData/", "")), success, error);
        }
        else if (execp) {
            DisplayMessage("Messages", "error", "Sorry but an unexpected error has occured, support has been notified we will respond shortly.");
            //Aurora.Manage.Service.AuroraWS.SendMail("support@sawebdesign.co.za", "Aurora", execp.message + "<br/><br/><b>Page Print:</b><br/>" + pagePrint, String.format("Error on manage for Client:{0}", client.replace("~/ClientData/", "")), success, error);
        }
        throw (execp);

    }
}
function printStackTrace() {
    /// <summary>
    /// When an error occurs this will return a call stack of methods led us to this error.
    /// </summary>
    /// <returns type="String" />

    var callstack = [];
    var isCallstackPopulated = false;
    try {
        i.dont.exist += 0; //doesn't exist- that's the point
    } catch (e) {
        if (e.stack) { //Firefox
            var lines = e.stack.split('\n');
            for (var i = 0, len = lines.length; i < len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    callstack.push(lines[i]);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
        else if (window.opera && e.message) { //Opera
            var lines = e.message.split('\n');
            for (var i = 0, len = lines.length; i < len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    var entry = lines[i];
                    //Append next line also since it has the file info
                    if (lines[i + 1]) {
                        entry += ' at ' + lines[i + 1];
                        i++;
                    }
                    callstack.push(entry);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
    }
    if (!isCallstackPopulated) { //IE and Safari
        var currentFunction = arguments.callee.caller;
        while (currentFunction) {
            var fn = currentFunction.toString();
            var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf('')) || 'anonymous';
            callstack.push(fname);
            currentFunction = currentFunction.caller;
        }
    }
    return callstack.join('\n\n');
}
//#endregion

//#region DateTime Utilities 

function formatDateTime(dateTime, format) {
    /// <summary>Formats a UTC date to a short date string</summary>
    /// <param name="dateTime" type="Date">Represents a UTC date time format</param>
    /// <param name="format" type="String">format string when specified will format date</param>
    /// <returns type="String" />

    if (!format) {
        format = "dd MMM yyyy";
    }
    var newDate = new Date(dateTime);
    return newDate.format(format);
};
function formatDateTimeLongDateString(dateString) {
    /// <summary></summary>
    var dateSplit = dateString.toString().split("/");
    if (dateSplit[1] === "09" || dateSplit[1] === "08") {
        dateSplit[1] = dateSplit[1].substr(1, 1);
    }
    var Months = ["None", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return dateSplit[0] + " " + Months[parseInt(dateSplit[1])] + " " + dateSplit[2];
}
function rectime(sec) {
    var hr = Math.floor(sec / 3600);
    var min = Math.floor((sec - (hr * 3600)) / 60);
    sec -= ((hr * 3600) + (min * 60));
    sec += ''; min += '';
    while (min.length < 2) { min = '0' + min; }
    while (sec.length < 2) { sec = '0' + sec; }
    hr = (hr) ? ':' + hr : '';
    return hr + min + ':' + sec;
}

//#endregion

//#region UI Utilities 
function scrollToElement(elementID, offset, duration) {
    /// <summary>Scrolls the page smoothly to the element ID specifed</summary>
    ///	<param name="elementID" type="String">
    ///		The ID of the element to scroll to.
    ///	</param>
    ///	<param name="offset" type="Int">
    ///		The offset to apply from the element. Negative numbers offset above. Positive numbers offset below.
    ///	</param>
    ///	<param name="duration" type="Int">
    ///		The duration of the animation in milliseconds. 
    ///	</param>
    if (offset == null) { offset = 0 };
    if (duration == null) { duration = 2000 };
    if (elementID != null) {
        $('html, body').animate({
            scrollTop: $("#" + elementID).offset().top + offset
        }, duration);
    };
}
function showHideColumn(colno, doshow, tableId) {
    /// <summary>shows or hides columns inside a specified table</summary>
    /// <param name="colno" type="Number">cell index to hide</param>
    /// <param name="doshow" type="Boolean">hide/show true = show false = hide</param>
    /// <param name="tableId" type="String">the table to which modifications must occur</param>

    var style;
    if (doshow) style = 'block'
    else style = 'none';

    var tbl = document.getElementById(tableId);
    var rows = tbl.getElementsByTagName('tr');

    for (var row = 0; row < rows.length; row++) {
        var cels = rows[row].getElementsByTagName('td')
        cels[colno].style.display = style;
    }
};
function DisplayMessage(containerId, messageType, message, timeOut) {
    /// <summary>Displays a relative message in a message box </summary>
    /// <param name="containerId" type="String">The unique identifier for the message place holder</param>
    /// <param name="messageType" type="String">This determines how the message looks the types are warn error info success</param>
    /// <param name="message" type="String">The text that will be displayed to the user</param>
    /// <param name="timeOut" type="Number">Sets the interval for the message to disappear in milliseconds.When 0 item does not disappear</param>
    clearTimeout(GlobalVariables.GlobalVar.prototype.TimeOutID);
    var container = $("#" + containerId)[0];
    $("#" + containerId).css({ opacity: '100' });
    var text = $("<p/>")[0];
    var icon = $("<img/>")[0];
    var tempMsg = message.substr(0, 1).toUpperCase();
    message = tempMsg + message.substr(1).toLowerCase();
    container.innerHTML = "";



    //type
    switch (messageType.toUpperCase()) {
        case "WARN":
            container.className = "alert_warning";
            icon.src = "/Styles/images/icon_warning.png";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "INFO":
            icon.src = "/Styles/images/icon_info.png";
            container.className = "alert_info";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "ERROR":
            icon.src = "/Styles/images/icon_error.png";
            container.className = "alert_error";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "SUCCESS":
            icon.src = "/Styles/images/icon_accept.png";
            container.className = "alert_success";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
    }


    $("#" + containerId).show();
    if (!GetJSSession("NoScroll")) {
        scrollToElement(containerId, -300, 10);
    }

    if (timeOut > 0) {

        GlobalVariables.GlobalVar.prototype.TimeOutID = setTimeout(function () {
            if ($("#" + containerId).is(":visible")) {
                $("#" + containerId).fadeIn(650).hide();
                clearTimeout(GlobalVariables.GlobalVar.prototype.TimeOutID);
                GlobalVariables.GlobalVar.prototype.TimeOutID = 0;
            }

        }, timeOut);
    }

}

function hideMenu(e) {
    if ($("#sidebar").css("display") != "none") {
        $('#sidebar').css({ display: 'none' });
        $("#content").css('margin-left', '30px');
        $(document.body).css({ backgroundImage: 'none' });
        $(e).children('img').attr("src", "/Styles/images/menurestore.png");
        $("#textpld").html("Show Menu");
    }
    else {
        $('#sidebar').css({ display: 'block' });
        $("#content").css({ 'margin-left': '240px' });
        $(document.body).removeAttr("style");
        $(e).children('img').attr("src", "/Styles/images/menuhide.png");
        $("#textpld").html("Hide Menu");
    }

}

function showShortCuts() {
    $("#main-nav a").each(
        function () {
            if ($("#shortcut_" + this.innerHTML).length > 0) {
                $("#shortcut_" + this.innerHTML).parent().show();
            }

        }
    );
}
//#endregion

//#region Number Utilities 
function formatNumber(nStr) {
    /// <summary>Formats numbers into a currency style </summary>
    /// <param name="nStr" type="Number">Number to be formatted</param>

    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '.00';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
};

function isNumeric(checkValue) {
    /// <summary>Checks if a value is a number</summary>
    /// <param name="name" type="String">Description</param>
    /// <returns type="Boolean" />

    var compare = checkValue;
    checkValue = parseFloat(checkValue);
    return (checkValue != 'NaN' && compare == checkValue);
};
jQuery.fn.ForceNumericOnly =

function () {
    /// <summary>Only allows numeric values in a text field</summary>
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 36 ||
                key == 109 ||
                key == 8 ||
                key == 9 ||
                key == 46 ||
                key == 190 ||
                key == 110 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        })
    });

    return this;
};
function calc(type, data) {
    var data = data.split("-");
    var ans;
    switch (type) {
        case "Exp":
            var date = new Date(data[0]);
            var today = new Date();
            var dayDiff = (Math.ceil(date - today) / (1000 * 60 * 60 * 24));
            ans = (dayDiff / data[1]);
            ans = ans.toFixed(2);
            break;
        case "%":
            ans = (data[0] / data[1]) * 100;
            ans = ans.toFixed(2);
            break;
        case "ratio":
            ans = data[0] / data[1];
            ans = ans.toFixed(0);
            break;

    }

    if (!isNaN(ans) && ans > 1 && ans != Infinity) {
        return ans;
    }
    else {
        return 0;
    }
}
function formatDecimal(value) {
    return value.substr(0, value.indexOf(".")) + value.substr(value.indexOf("."), 3);
}
//#endregion

//#region Form Utilities 

function validateForm(formId, displayMessage, messageContainer, clearForm, elementID) {
    /// <summary>Validates all input tags in a container.More work to do here only does text</summary>
    
    var form;
    if (elementID == undefined) {
        form = $("#" + formId + " input");
    } else {
        form = $('#' + elementID + '');
    }
    var hasErrors = false;

    for (var index = 0; index < form.length; index++) {
        if (form[index].getAttribute("valtype") != null) {

            var toValidate = form[index].getAttribute("valtype").split(";");
            var isRequired = false;
            if (form[index].getAttribute("valtype").toUpperCase().indexOf("REQUIRED") > -1) {
                isRequired = true;
            }
            for (i = 0; i < toValidate.length; i++) {
                var validate = toValidate[i].split(":");

                switch (validate[0].trim().toUpperCase()) {
                    default:
                        break;
                    case "REQUIRED":
                        if (form[index].value == "" || form[index].selectedIndex == -1) {
                            hasErrors = true;
                            validateFail(form, formId, index);
                        }
                        else {
                            validatePass(form, formId, index);
                        }
                        break;
                    case "DATEREQ":
                        break;
                    case "REGEX":

                        if ((isRequired == true) || (form[index].value != "")) {
                            var pattern = validateRegex(validate[1]);
                            var val = form[index].value;

                            test = val.match(new RegExp(pattern, 'gi'));
                            if (test == null) {
                                hasErrors = true;
                                validateFail(form, formId, index);
                            } else {
                                validatePass(form, formId, index);
                            }
                        }
                        break
                    case "MUSTMATCH":
                        var elemToMatch = $("#" + validate[1]).val()
                        if (form[index].value == elemToMatch && elemToMatch != "") {
                            validatePass(form, formId, index);
                        }
                        else {
                            hasErrors = true;
                            if (elemToMatch == "") {
                                validateFail(form, formId, index, "Please enter a password above, then repeat that sequence in this text box");
                            }
                            else {
                                validateFail(form, formId, index, "Passwords do not match");
                            }

                        }
                        break;
                }
            }
        }
    }
    if (hasErrors && displayMessage) {
        DisplayMessage(messageContainer, "warn", "items in form have not been entered");
    }
    return hasErrors;
}

function validatePass(form, formId, element) {
    var icon = form[element].parentNode.lastChild;
    if (form[element].parentNode.lastChild.id == "icon_" + form[element].name) {
        form[element].parentNode.removeChild(icon);
    }
}

function validateFail(form, formId, element, customToolTipMsg) {
    var icon = document.createElement("img");
    icon.src = "/Styles/images/exclamation.png";
    icon.id = "icon_" + form[element].name;
    if (form[element].getAttribute("valmessage") != null) {
        icon.title = form[element].getAttribute("valmessage");
    }
    else if (customToolTipMsg) {
        icon.title = customToolTipMsg;
    }
    else {
        icon.title = "Please enter a value for this field";
    }

    if (form[element].parentNode.lastChild.id != icon.id) {
        form[element].parentNode.appendChild(icon);
    }
    else {
        form[element].parentNode.lastChild.title = icon.title;
    }

    //    if ($.browser.msie) {
    //        $('#' + form[element].id + '').bind('focusout', function () {
    //            validateForm(formId, true, "Messages", false, form[element].id)
    //        });
    //    } else {
    //        $('#' + form[element].id + '').blur(function () {
    //            validateForm(formId, true, "Messages", false, form[element].id)
    //        });
    //    }

}

function validateRegex(regType) {
    switch (regType.trim().toUpperCase()) {
        case "DATE":
            return "^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
        case "STRING":
            return "[a-z][A-Z]";
        case "INT":
            return "^[-+]?\\d*$";
        case "PHONE":
            return "^[\\d]{10,12}$";
        case "PASSWORD":
            return "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$"; //Password matching expression. Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit
        case "EMAIL":
            return "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        case "URL":
            return "^(http:\\/\\/|https:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$";
        case "TIME":
            return "^([0-1]?\d|2[0-3]):([0-5]\d)$"
        case "SPECIAL":
            return "^[a-zA-Z]+\.[a-zA-Z]+$"
        default:
            return regType;
    }
}

function ClearForm() {
    for (var element = 0; element < form.length; element++) {
        if (form[element].type == "text")
            form[element].value = "";
    }
}

jQuery.fn.restrictChars = function (options) {
    /// <summary>Allows a restriction of characters by regex validation</summary>
    /// <param name="options" type="JSON">regex:regex,callback:function to be called</param>

    return this.each(function () {
        $(this).keypress(function (e) {
            var key = e.charCode || e.keyCode || 0;
            var valid = true;

            // keys within the regex expression
            if (!options.regex.test(String.fromCharCode(key))) {
                // Cancel the original event
                e.preventDefault();
                e.stopPropagation();
                valid = false
            }
            else {
                valid = true
            }
            return (valid);
        })
    });

};

//#endregion

//#region Innova Utils 
var testimoniesForm = "<div id=Testimonies>   <table style='MARGIN-LEFT: 25px' id=CustomerFeedback>     <tbody>       <tr>         <td>First Name: </td>         <td>          <input id=FirstName class=text_box valtype='required' /></td>      </tr>       <tr>         <td>Last Name: </td>         <td>          <input id=LastName class=text_box valtype='required' /></td>      </tr>       <tr>         <td>Email: </td>         <td>          <input id=Email class=text_box valtype='required' /></td>      </tr>       <tr>         <td>Contact No: </td>         <td>          <input id=ContactNo class=text_box valtype='required' /></td>      </tr>       <tr>         <td style='BACKGROUND-IMAGE: none; BACKGROUND-REPEAT: repeat; BACKGROUND-POSITION: 0% 0%; VERTICAL-ALIGN: top'>Your Comment:</td>         <td>          <textarea style='WIDTH: 282px; HEIGHT: 90px' class=text_area cols=17 rows=1></textarea><input type='hidden' id='sendToClient'/></td>      </tr>       <tr>         <td></td>         <td align=right>          <input id=sendBtn class=submit_button value=Send type=button /></td>      </tr>       <tr>         <td colspan=2>           <div id=sendit class=QapTcha>&nbsp;</div></td>      </tr>    </tbody>  </table></div><script type=text/javascript>$( function(){ $('#inputcontent #sendit').QapTcha({buttonLock:'#sendBtn',buttonFunc:function(){AuroraJS.Modules.sendFeedback()}});  });</script>"
function InitEditor(contentPlacHolder, editorContainer, loadModuletags, customFields, dontLoadStyles) {
    /// <summary>
    /// Creates a new Innova editor instance
    /// </summary>
    /// <param name="contentPlacHolder" type="String">Server textarea control(content loads inside this)</param>
    /// <param name="editorContainer" type="String">Client side span control (editor loads inside this)</param>
    /// <param name="loadModuletags" type="Boolean">Loads additional functions into the editor bespoke for Aurora</param>
    /// <param name="customFields" type="String">JSON value must be formatted in : fields{name:"value"}</param>
    /// <param name="loadStyles" type="Boolean">Disables site stlyesheet</param>
    var customTag = customFields ? "CustomTag" : "";
    oEdit1 = null;
    oEdit1 = new InnovaEditor("oEdit1");
    //
    oEdit1.toolbarMode = 1;
    oEdit1.btnSpellCheck = false;
    if (customFields && customFields.constructor === Array) {
        //Define custom tag selection must be a double nested array
        oEdit1.arrCustomTag = customFields;
    }

    if (loadModuletags) {
        oEdit1.arrCustomButtons = [
                                    ["Modules", "modelessDialogShow('/Innova/common/Modules.aspx',600,680)", "Insert a Module", "wandInnova.png"],
                                    //["FormWizard", "modelessDialogShow('/Innova/common/FormWizard.aspx',1024,560)", "Insert a Custom Form", "btnEmailFormWiz.gif"],
                                    ["Tags", "modelessDialogShow('/Innova/common/TemplateWizard.aspx',1024,560)", "Insert a new Tag", "wandInnova.png"],
                                    ["TestimoniesForm", "oEdit1.insertHTML(testimoniesForm);", "Insert a new Testimonies Form", "btnStyle.gif"]
                                  ];
        oEdit1.tabs = [
        ["tabHome", "Home", ["grpEdit", "grpFont", "grpPara"]],
        ["tabStyle", "Insert", ["grpInsert", "grpTables", "grpMedia"]],
        ["tabModules", "Insert Modules", ["grpModules"]]
    ];
    }



    oEdit1.groups = [
    ["grpEdit", "", ["XHTMLSource", "FullScreen", "Search", "RemoveFormat", "SpellCheck", "BRK", "Undo", "Redo", "Cut", "Copy", "Paste", "PasteWord", "PasteText"]],
    ["grpFont", "", ["FontName", "FontSize", "Strikethrough", "Superscript", "BRK", "Bold", "Italic", "Underline", "ForeColor", "BackColor", "Guidelines"]],
    ["grpPara", "", ["Paragraph", "Indent", "Outdent", "Styles", "StyleAndFormatting", "BRK", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyFull", "Numbering", "Bullets"]],
    ["grpInsert", "", ["Hyperlink", "Bookmark", "BRK", "Image", customTag]],
    ["grpTables", "", ["Table", "Guidelines", "BRK", "AutoTable"]],
    ["grpMedia", "", ["Media", "Flash", "BRK", "Characters", "Line"]],
    ["grpModules", "x", ["Modules", "FormWizard", "TestimoniesForm"]]
    ];


    oEdit1.useTagSelector = true;
    oEdit1.showResizeBar = true;
    oEdit1.width = "100%";
    oEdit1.cmdAssetManager = "modalDialogShow('/Innova/assetmanager/assetmanager.asp?ClientID=" + client.replace("~/ClientData/", "") + "',720,550)"; //Command to open the Asset Manager add-on.
    if (!dontLoadStyles) {
        oEdit1.css = client.replace("~", "") + "/Styles/Site.css?" + new Date().getMilliseconds();
    }
    editorTimer = setInterval(alignEditorTabs, 100);

    oEdit1.REPLACE(contentPlacHolder, editorContainer);

    //Removes body styles
    $($("#idContentoEdit1")[0].contentWindow.document).find('body')
    .css({ backgroundImage: 'none' })
    .attr("id", "innovaBody")
    .attr("style", "background-image:none;overflow-x:auto;");

    setTimeout(function () {
        $($("#idContentoEdit1")[0].contentWindow.document).find('body')
    .css({ backgroundImage: 'none' })
    .attr("id", "innovaBody")
    .attr("style", "background-image:none;overflow-x:auto;");
    }, 1000);

    if ($.browser.webkit) {
        $("#idContentoEdit1").css("margin-top", "35px");
    }
}
function sendTestMail() {
    var mails = prompt('Please enter an email address, separate by ; for multiple', "");
    if (!mails) {
        DisplayMessage("Messages", "info", "No emails were detected test mail aborted", 7000);
        return;
    }
    mails = mails.split(';');
    if (mails[0] === "") {
        DisplayMessage("Messages", "info", "No emails were detected test mail aborted", 7000);
        return;
    }
    var response = function (result) {
        DisplayMessage("Messages", "success", "mail was sent successfully", 7000);
    };
    DisplayMessage("Messages", "info", "sending email...", 7000);

    for (var item = 0; item < mails.length; item++) {
        Aurora.Manage.Service.AuroraWS.SendMail(mails[item], "Aurora", oEdit1.getHTMLBody(), "Newsletter", response, onError);
    }

}
function InitNewsletterEditor(contentPlacHolder, editorContainer) {
    oEdit1 = null;
    oEdit1 = new InnovaEditor("oEdit1");
    //

    oEdit1.toolbarMode = 1;
    oEdit1.btnSpellCheck = false;

    //Define custom tag selection
    oEdit1.arrCustomTag = [["First Name", "{name}"],
                                 ["Last Name", "{lastname}"],
                                 ["Email", "{email}"],
                                 ["Mobile", "{mobile}"],
                                 ["WorkTel", "{worktel}"],
                                 ["View Online Link", "{ViewOnline}"],
                                 ["Unsubscribe Link", "{Unsubscribe}"]
                                ];

    oEdit1.arrCustomButtons = [["SendMail", "sendTestMail();", "Send a Test Mail", "email_test.gif"]];
    //check if user qualifies for certain functions
    if ($("#main-nav a:contains('News')").length > 0) {
        ["News", "modelessDialogShow('/Innova/common/Newsletter.aspx?type=News',1024,560)", "Insert items from my News module", "newspaper_Add.gif"]
    }
    if ($("#main-nav a:contains('Voucher')").length > 0) {
        oEdit1.arrCustomButtons.push(["Vouchers", "modelessDialogShow('/Innova/common/GetVouherCampaign.aspx', 1024, 560)", "Append voucher codes to email message", "voucher.gif"]);
    }

    oEdit1.tabs = [
        ["tabHome", "Home", ["grpEdit", "grpFont", "grpPara"]],
        ["tabStyle", "Insert", ["grpInsert", "grpTables", "grpMedia"]],
        ["tabNewsletter", "Newsletter Tool Box", ["grpNewsletter"]]
    ];

    oEdit1.groups = [
    ["grpEdit", "", ["XHTMLSource", "FullScreen", "Search", "RemoveFormat", "SpellCheck", "BRK", "Undo", "Redo", "Cut", "Copy", "Paste", "PasteWord", "PasteText"]],
    ["grpFont", "", ["FontName", "FontSize", "Strikethrough", "Superscript", "BRK", "Bold", "Italic", "Underline", "ForeColor", "BackColor", "Guidelines"]],
    ["grpPara", "", ["Paragraph", "Indent", "Outdent", "Styles", "StyleAndFormatting", "BRK", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyFull", "Numbering", "Bullets"]],
    ["grpInsert", "", ["Hyperlink", "Bookmark"]],
    ["grpTables", "", ["Table", "Guidelines", "BRK", "AutoTable"]],
    ["grpMedia", "", ["Media", "Flash", "BRK", "Characters", "Line"]],
    ["grpNewsletter", "", ["Vouchers", "News", "SendMail", "CustomTag", "Image"]]
    ];


    oEdit1.useTagSelector = true;
    oEdit1.showResizeBar = true;
    oEdit1.width = "100%";
    oEdit1.cmdAssetManager = "modalDialogShow('/Innova/assetmanager/assetmanager.asp?ClientID=" + client.replace("~/ClientData/", "") + "',720,550)"; //Command to open the Asset Manager add-on.
    oEdit1.css = client.replace("~", "") + "/Styles/Site.css?" + new Date().getMilliseconds();
    editorTimer = setInterval(alignEditorTabs, 100);

    oEdit1.REPLACE(contentPlacHolder, editorContainer);

    //Removes body styles
    $($("#idContentoEdit1")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' });
    $($("#idContentoEdit1")[0].contentWindow.document).find('body').attr("style", "background-image:none;overflow-x:auto;");

    setTimeout(function () { $($("#idContentoEdit1")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' }); }, 1000);
}
function innovaHideBodybg() {
    $($("#idContentoEdit1")[0].contentWindow.document).find('body').css({ backgroundImage: 'none!important' });
}
function alignEditorTabs() {
    /// <summary>
    /// This re-aligns the tabs to the left
    /// </summary>
    if ($(".tabcontent").length > 0) {
        $(".tabcontent").css("float", "left");
        $(".tabcontent").parent().css("backgroundColor", "#f7f7f7");
        clearInterval(editorTimer);
    }
}
//#endregion

//#region XML Utilities 
function getCustomXML(xmlRoot) {
    /// <summary>Gets all the custom fields on a form and places them inside an XML tree</summary>
    /// <returns type="String" />
    if (!xmlRoot) {
        xmlRoot = "XmlData";
    }
    var tmp = $('[name*="fld"]').serializeArray();
    if (tmp.length > 0) {
        var CustomXML = "<" + xmlRoot + "><Fields>";
        for (var i = 0; i < tmp.length; i++) {
            CustomXML += "<field fieldname='" + tmp[i].name.replace("fld", "") + "' value='" + tmp[i].value + "' />"
        }
        CustomXML += "</Fields></" + xmlRoot + ">";
        return CustomXML;
    }
    return null;
}
function LoadDataFromXML(xmlString, fieldName) {
    /// <summary>Returns a value from a specified field in XML</summary>
    /// <param name="xmlString" type="String">the data that needs to be searched</param>
    /// <param name="fieldName" type="String">The Field that contains the data</param>
    var xmlDoc = $.parseXML(xmlString);
    var item;
    $xml = $(xmlDoc);
    $fields = $xml.find('field');
    if (fieldName) {
        $.each($fields, function (i, field) {

            if ($(field).attr("fieldname") == fieldName) {
                item = $(field).attr('value');
            }
        });
    }
    else {
        return $xml;
    }

    return item;
}
function transferCustomFields(customObjectContainer, generatedFieldsContainer, fieldClass) {
    /// <summary>Moves custom xml fields to the main table of a form for formatting</summary>
    $("#" + generatedFieldsContainer + " tr").each(
        function () {
            $(this).children("td").attr("align", "left");
            $(this).insertBefore($("#" + customObjectContainer));
            if (fieldClass) {
                $(this).addClass(fieldClass);
            }
        }
    );
}
function buildCustomXMLBySelector(selector, valueField, addTitle) {
    /// <summary>Takes any Jquery element selector and converts it into XML Data</summary>
    var tmp = selector;
    valueField = (!valueField) ? "value" : valueField;
    if (tmp.length > 0) {
        //#region Varables
        /// <summary>Container for XML</summary>
        var CustomXML = "<XmlData><Fields>";
        /// <summary>Temporarily holds field value</summary>
        var data = "";
        //#endregion

        //iterate through controls
        for (var i = 0; i < tmp.length; i++) {
            data = $(tmp[i]).attr(valueField);
            var titleField = addTitle ? $(tmp[i]).attr("title") : "";

            //#region Exception for multiple select dropdowns 
            try {
                if (typeof (eval($(tmp[i]).val())) === "object") {
                    var tempObj = eval($(tmp[i]).val());
                    data = "";
                    for (var item in tempObj) {
                        data += tempObj[item] + ",";
                    }
                }
            } catch (e) { }
            //#endregion

            if ($(tmp[i]).is("input[type='checkbox']")) {
                CustomXML += "<field fieldname=\"" + tmp[i].name.replace("fld", "") + "\" value=\"" + $(tmp[i]).is(":checked") + "\" title=\"" + encodeHTML(titleField) + "\" />"
            }
            else {
                CustomXML += "<field fieldname=\"" + tmp[i].name.replace("fld", "") + "\" value=\"" + encodeHTML(data) + "\" title=\"" + encodeHTML(titleField) + "\" />"
            }

        }

        CustomXML += "</Fields></XmlData>";
        return CustomXML;
    }
    return null;
}
//#endregion

//#region JQuery templating
function bindDataToGrid(templateContainer, template, data) {

    $(templateContainer).children().remove();

    $(template).tmpl(data).appendTo(templateContainer);

}
//#endregion

//#region JQuery Extensions
//#region $.fn.mapInputsToObject - Maps form element values to an object by field.
$.fn.mapToObject = function (object, field) {
    /// <summary>
    /// Maps form element values to an object by field name.
    /// Currently only maps text, hidden, checkbox, selects but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">(optional) The object to map values to / extend.</param>
    /// <param name="field" type="string">(optional) The field name to map values to.</param>
    /// <returns type="String" />
    object = object || {};
    field = field || "id";
    var inputs = this.find(':input');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select, textarea')) {
            if (i.hasClass("isDatePicker")) {
                object[i.attr(field)] = i.datepicker("getDate");
            } else {
                object[i.attr(field)] = (i.val() !== "") ? i.val() : null;
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            object[i.attr(field)] = i.is(":checked");
        } else if (i.is("input[type='radio']")) {

            if (i.is(":checked")) {
                object[i.attr(field)] = i.val();
            }
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion
//#region $.fn.applyObjecttoInputs- Maps object values to form elements by field.
$.fn.mapObjectTo = function (object, field, overwrite) {
    /// <summary>
    /// Maps object values to form elements by field.
    /// Currently only maps text, hidden, checkbox but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">The object to draw values from.</param>
    /// <param name="field" type="string">(optional) The field to use as key for value mapping.</param>
    /// <param name="overwrite" type="boolean">(optional) Whether or not to overwrite existing form values</param>
    /// <returns type="String" />
    
    object = object || {};
    overwrite = overwrite || false;
    field = field || "id";
    var inputs = this.find(':input, .field[' + field + ']');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select, textarea')) {
            var value;

            if (object[i.attr(field)] instanceof Date) {
                value = object[i.attr(field)].format("dd/MM/yyyy");
            }
            else {
                value = object[i.attr(field)];
            }


            if (overwrite) {
                i.val(value !== null ? value : "");
            }
            else {
                i.val(value !== null ? value : i.val());
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            if (object[i.attr(field)]) {
                i.attr('checked', 'checked');
            }
            else {
                i.removeAttr('checked');
            }
        }
        else if (i.is('.field')) {
            i.html(object[i.attr(field)] || "&nbsp;");
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion
//#endregion