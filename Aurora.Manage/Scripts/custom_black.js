/// <reference path="jquery.js" />
/// <reference path="Utils.js" />

/*

Global values
*/

var chartWidth = '650px';
var chartHeight = '240px';

/*
Find element's Y axis position
*/

function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (1) {
            curtop += obj.offsetTop;
            if (!obj.offsetParent) {
                break;
            }
            obj = obj.offsetParent;
        }
    }
    else if (obj.y) {
        curtop += obj.y;
    }

    return curtop;
}

/*
Find element's X axis position
*/

function findPosX(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (1) {
            curtop += obj.offsetLeft;
            if (!obj.offsetParent) {
                break;
            }
            obj = obj.offsetParent;
        }
    }
    else if (obj.x) {
        curtop += obj.x;
    }

    return curtop;
}

/*
Setup chart from given table and type
*/

function setChart(tableId, type, wrapper) {
    //clear existing chart before create new one
    $(wrapper).html('');

    $(tableId).visualize({
        type: type,
        width: chartWidth,
        height: chartHeight,
        colors: ['#7EC421', '#9FBAD4']
    }).appendTo(wrapper);


    $('.visualize').trigger('visualizeRefresh');

}

/*
Setup notification badges for shortcut
*/
function setNotifications() {
    // Setup notification badges for shortcut
    $('#shortcut_notifications span').each(function () {
        if ($(this).attr('rel') != '') {
            target = $(this).attr('rel');

            if ($('#' + target).length > 0) {
                var Ypos = findPosY(document.getElementById(target));
                var Xpos = findPosX(document.getElementById(target));

                $(this).css('top', Ypos - 24 + 'px');
                $(this).css('left', Xpos + 60 + 'px');
            }
        }
    });
    $('#shortcut_notifications').css('display', 'block');
}

$(function () {

    //    // Preload images
    //    $.preloadCssImages();

    // Find all the input elements with title attributes and add hint to it
   // $('input[title!=""]').hint();

    //Sidebar Accordion Menu:
    /// <summary>Initialises site navigation by setting current page</summary>
    $("#main-nav").find("a.current").parents("li").children(".nav-top-item").addClass("current");

    $("#main-nav li ul").hide(); // Hide all sub menus
    $("#main-nav li a.current").parent().find("ul").slideToggle("slow"); // Slide down the current menu item's sub menu

    $("#main-nav li a.nav-top-item").click( // When a top menu item is clicked...
			function () {
			    $(this).parent().siblings().find("ul").slideUp("normal"); // Slide up all sub menus except the one clicked
			    $(this).next().slideDown("normal"); // Slide down the clicked sub menu
			    return false;
			});

    $("#main-nav li a.no-submenu").click( // When a menu item with no sub menu is clicked...
			function () {
			    window.location.href = (this.href); // Just open the link instead of a sub menu
			    return false;
			});

    // Sidebar Accordion Menu Hover Effect:
    $("#main-nav li .nav-top-item").hover(
			function () {
			    $(this).stop().animate({ paddingRight: "25px" }, 200);
			},
			function () {
			    $(this).stop().animate({ paddingRight: "15px" });
			});

    // Setup charts example	

    // Chart bar type
    $('#chart_bar').click(function () {
        setChart('table#graph_data', 'bar', '#chart_wrapper');

        //switch menu
        $(this).parent().parent().find('td input').removeClass('active');
        $(this).addClass('active');
    });


    // Chart area type
    $('#chart_area').click(function () {
        setChart('table#graph_data', 'area', '#chart_wrapper');

        //switch menu
        $(this).parent().parent().find('td input').removeClass('active');
        $(this).addClass('active');
    });


    // Chart pie type
    $('#chart_pie').click(function () {
        setChart('table#graph_data', 'pie', '#chart_wrapper');

        //switch menu
        $(this).parent().parent().find('td input').removeClass('active');
        $(this).addClass('active');
    });


    // Chart line type
    $('#chart_line').click(function () {
        setChart('table#graph_data', 'line', '#chart_wrapper');

        //switch menu
        $(this).parent().parent().find('td input').removeClass('active');
        $(this).addClass('active');
    });

    // Setup datepicker input
    $("#datepicker").datepicker({
        nextText: '>',
        prevText: '<',
        showAnim: 'slideDown',
        width: '400'
    });

    // Setup minimize and maximize window
    $('.onecolumn .header span').click(function () {
        if ($(this).parent().parent().children('.content').css('display') == 'block') {
            $(this).css('cursor', 's-resize');
        }
        else {
            $(this).css('cursor', 'n-resize');
        }

        $(this).parent().parent().children('.content').slideToggle('fast');
    });

    $('.twocolumn .header span').click(function () {
        if ($(this).parent().parent().children('.content').css('display') == 'block') {
            $(this).css('cursor', 's-resize');
        }
        else {
            $(this).css('cursor', 'n-resize');
        }

        $(this).parent().parent().children('.content').slideToggle('fast');
    });

    $('.threecolumn .header span').click(function () {
        if ($(this).parent().parent().children('.content').css('display') == 'block') {
            $(this).css('cursor', 's-resize');
        }
        else {
            $(this).css('cursor', 'n-resize');
        }

        $(this).parent().children('.content').slideToggle('fast');
    });


    // Check or uncheck all checkboxes
    $('#check_all').click(function () {
        if ($(this).is(':checked')) {
            $('form#form_data input:checkbox').attr('checked', true);
        }
        else {
            $('form#form_data input:checkbox').attr('checked', false);
        }
    });


    // Setup notification badges for shortcut
    setNotifications();


    // Add tooltip to shortcut
    $('#shortcut li a').tipsy({ gravity: 's' });


    // Setup tab contents
    $(".left_switch,.right_switch,.middle_switch").click(
     function () {
         $(this).parent().parent().find('td input').removeClass('active');
         $(this).addClass('active');
         
         //show tab content
         $('.tab_content').addClass('hide');
         $('#' + $(this).attr('id') + '_content').removeClass('hide');

     }
    );
    
    // Setup sortable to threecolumn div
    //Updated by Wesley Perumal for pages.
    var selectedItem;
    $("#s").sortable({
        opacity: 0.8,
        connectWith: '.drageach',
        items: 'tr.drageach',
        containment: 'parent',
        revert: true,
        start: function (item) {

        },
        stop: function (item, ui) { stopDrag(item); }
    });

    function stopDrag(item, ui) {
        var index = 1;
        var rows = $(".data tr").each(
                    function () {
                        if (this.cells[0].nodeName != "TH") {
                            this.cells[3].innerHTML = index;
                            index++;
                        }
                    }
                );
        //UpdateRowOrder();
    }
});

function UpdateRowOrder() {
    $(".content").block({ message: "Updating Page Order, Please wait..." });
    var rowIndex = 0;
    $(".data tr").each(
        function () {
            if ($(this).attr('id') != "") {
                var page = findByInArray(currentData, "ID", $(this).attr('id'));

                if (page == null) {
                    return;
                }
                page.PageIndex = this.cells[3].innerHTML;
                page.EntityKey = new Object();
                page.EntityKey.EntityContainerName = "AuroraEntities";
                page.EntityKey.EntitySetName = "PageContent";
                page.EntityKey.IsTemporary = false;
                page.EntityKey.EntityKeyValues = new Array();
                page.PageContent1 = page.PageContent;
                objKey = new Object();
                objKey.Key = "ID";
                objKey.Value = parseInt(page.ID);
                page.EntityKey.EntityKeyValues[0] = objKey;

                var currentRow = this;

                var success = function (result) {
                    if (rowIndex == $(".data tr").length - 2) {
                        loadList();
                    }
                    rowIndex++;
                };

                Aurora.Manage.Service.AuroraWS.UpdatePage(page, success, onError);

            }
        });
}


$(document).ready(function () {
    //need to set timeout 0.5 sec for cross browser rendering
    //setTimeout('setChart(\'table#graph_data\', \'bar\', \'#chart_wrapper\')', 500);
    $(".data").delegate("tr td[align='right']", "click",
    function () {
        $('.rowSelected').removeClass('rowSelected');
        $(this.parentNode).addClass('rowSelected');
    });
    
});