﻿//Extension to correct the datetime offset error in the serializer.
//Line 46 and 47 were the modifications to compensate for datetime offset.

$(function () {
    Sys.Serialization.JavaScriptSerializer._serializeWithBuilder = function Sys$Serialization$JavaScriptSerializer$_serializeWithBuilder(object, stringBuilder, sort, prevObjects) {
        var i;
        switch (typeof object) {
            case 'object':
                if (object) {
                    if (prevObjects) {
                        for (var j = 0; j < prevObjects.length; j++) {
                            if (prevObjects[j] === object) {
                                throw Error.invalidOperation(Sys.Res.cannotSerializeObjectWithCycle);
                            }
                        }
                    }
                    else {
                        prevObjects = new Array();
                    }
                    try {
                        Array.add(prevObjects, object);

                        if (Number.isInstanceOfType(object)) {
                            Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
                        }
                        else if (Boolean.isInstanceOfType(object)) {
                            Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
                        }
                        else if (String.isInstanceOfType(object)) {
                            Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
                        }

                        else if (Array.isInstanceOfType(object)) {
                            stringBuilder.append('[');

                            for (i = 0; i < object.length; ++i) {
                                if (i > 0) {
                                    stringBuilder.append(',');
                                }
                                Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(object[i], stringBuilder, false, prevObjects);
                            }
                            stringBuilder.append(']');
                        }
                        else {
                            if (Date.isInstanceOfType(object)) {
                                stringBuilder.append('"');

                                stringBuilder.append(object.toJSON());
                                stringBuilder.append('"');
                                break;
                            }
                            var properties = [];
                            var propertyCount = 0;
                            for (var name in object) {
                                if (name.startsWith('$')) {
                                    continue;
                                }
                                if (name === Sys.Serialization.JavaScriptSerializer._serverTypeFieldName && propertyCount !== 0) {
                                    properties[propertyCount++] = properties[0];
                                    properties[0] = name;
                                }
                                else {
                                    properties[propertyCount++] = name;
                                }
                            }
                            if (sort) properties.sort();
                            stringBuilder.append('{');
                            var needComma = false;

                            for (i = 0; i < propertyCount; i++) {
                                var value = object[properties[i]];
                                if (typeof value !== 'undefined' && typeof value !== 'function') {
                                    if (needComma) {
                                        stringBuilder.append(',');
                                    }
                                    else {
                                        needComma = true;
                                    }

                                    Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(properties[i], stringBuilder, sort, prevObjects);
                                    stringBuilder.append(':');
                                    Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(value, stringBuilder, sort, prevObjects);

                                }
                            }
                            stringBuilder.append('}');
                        }
                    }
                    finally {
                        Array.removeAt(prevObjects, prevObjects.length - 1);
                    }
                }
                else {
                    stringBuilder.append('null');
                }
                break;
            case 'number':
                Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
                break;
            case 'string':
                Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
                break;
            case 'boolean':
                Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
                break;
            default:
                stringBuilder.append('null');
                break;
        }
    }
});