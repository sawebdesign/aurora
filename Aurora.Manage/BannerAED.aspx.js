﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />

//#region Variables
var service = Aurora.Manage.Service.AuroraWS;
var templateList;
var currentData;
var Loaded = false;
var TotalCount = 0;
var CurrentPage = 0
var newItem = false;
var Global = new GlobalVariables.GlobalVar();
var image;
var bannerMainOption = null;
var bannerButtonOption = null;
var bannerBottomOption = null;
var Globalitem = new Object();

//#endregion

//#region onLoad
$(function () {
    loadBannerList(false);
    $("#btnSave").hide();
});
//#endregion

//#region Custom Functions

function setAssetValue(imageUrl) {
    /// <summary>fck editor uploader function</summary>
    image = imageUrl;
    if (imageUrl != "") {

        var item = new Object();
        item.dataid = $("#BannerID").val();
        if (saveData(image)) {
            //setTimeout(getDetail(item), 4000);
        }
    }
}

function getZones(bannerID) {
    /// <summary>Loads all pages that are attached to the banner</summary>
    var requestZones = function (result) {
        if (result.Result) {
            var selectAll = false;
            $("<option value='0'>Everywhere</option>").insertBefore($("#ZoneId").children()[0]);
            for (var zone in result.Data) {
                $("#ZoneId option").each(
                    function () {
                        if (this.value == result.Data[zone].zoneid) {
                            this.selected = true;
                        }

                    }
                );
            }



            $('#ZoneId').toChecklist({

                /**** Available settings, listed with default values. ****/
                addScrollBar: true,
                addSearchBox: true,
                searchBoxText: 'Type here to search list...',
                showCheckboxes: true,
                showSelectedItems: true,
                submitDataAsArray: true // This one allows compatibility with languages that use arrays

            });

            $("#ZoneId_checklist").attr("mytitle", "The pages that should display this banner");
            $("#ZoneId_checklist").addClass("help");

            $(".tipsy").remove();

            $("#BannerForm .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        }
    };

    service.GetBannerZones(bannerID, requestZones, onError);
}
//#endregion

//#region Pagination
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        loadBannerList(false);
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        loadBannerList(false);
    }

    $('div.content').unblock();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

//#endregion

//#region Data Manipulation
function loadBannerList(isArchived) {

    /// <summary>Loads all the banners for a particular client.</summary>
    $("#Messages").hide();
    var bannerSuccess = function (result) {
        //block content
        $('div.content').block({ message: null });

        $('div.content').show();
        // Add tooltip to edit and delete button

        if (result.Result) {
            currentData = result.Data;
            if (templateList == null) {
                templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            }

            templateList.set_data(result.Data);
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;


            $('div.content').unblock();
            if (result.Count == 0) {
                $('div.content').hide();
                DisplayMessage("Messages", "warn", "No Banners Available", false);
            }
            $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });

            //Loads stat box
            $('.Stats').fancybox({
                padding: 0,
                titleShow: true,
                title: '',
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                width: 850,
                height: 600,
                type: "iframe"
            });
        }
        else {
            DisplayMessage("Messages", "error", "Could not load banner data, please try again later.", false);
        }
    }


    //Call to service
    service.GetBanners(CurrentPage + 1, 10, "", 0, isArchived, bannerSuccess, onError);

}
function loadBannerListByType(BannerType, isArchived) {

    /// <summary>Loads all the banners for a particular client.</summary>
    $("#Messages").hide();
    var bannerSuccess = function (result) {
        //block content
        $('div.content').block({ message: null });

        $('div.content').show();
        // Add tooltip to edit and delete button

        if (result.Result) {
            currentData = result.Data;
            if (templateList == null) {
                templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            }

            templateList.set_data(result.Data);
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;


            $('div.content').unblock();
            if (result.Count == 0) {
                $('div.content').hide();
                DisplayMessage("Messages", "warn", "No Banners Available", false);
            }
            $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });

            //Loads stat box
            $('.Stats').fancybox({
                padding: 0,
                titleShow: true,
                title: '',
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                width: 850,
                height: 600,
                type: "iframe"
            });


        }
        else {
            DisplayMessage("Messages", "error", "Could not load banner data, please try again later.", false);
        }
    }


    //Call to service

    service.GetBannersByType(CurrentPage + 1, 10, BannerType, 0, isArchived, bannerSuccess, onError);

}
function getDetail(anchor) {
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#contentAddEdit").show();
            $("#btnSave").show();
            $("#Cancel").show();
        }
    }
    if (anchor == undefined) {
        $("#contentAddEdit").load("Banner.aspx?BannerId=0&var=" + x.getMilliseconds(), success);
    }
    else {
        $("#contentAddEdit").load("Banner.aspx?BannerId=" + $(anchor).attr("dataid") + "&var=" + x.getMilliseconds(), success);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);



}

function saveData(imageurl) {
    $("#contentAddEdit").block({ message: null });

    //Validate form
    if (validateForm("BannerForm", true, "Messages", false)) {
        $("#contentAddEdit").unblock();
        scrollToElement("Messages", -20, 2);
        return false;
    }

    //check Zones

    //Get Data
    var banner = new AuroraEntities.AdBanner();

    Global.populateEntityFromForm(banner, $('#BannerForm input').serializeArray());
    for (property in banner) {
        if (banner[property] == null) {
            banner[property] = "";
        }
    }

    var saveSuccess = function (result) {
        if (result.Result) {
            pages = [];
            var item = new Object();
            item.dataid = result.Data.BannerID;
            getDetail(item);
            setTimeout(function () {
                DisplayMessage("Messages", "success", "banner has been saved", false);
                scrollToElement("Messages", -20, 2);
            }, 2000);

        }
        else {
            DisplayMessage("Messages", "error", "Banner could not be saved, please try again later.");
        }


        $("#contentAddEdit").unblock();
        loadBannerList(false);

        return true;
    };

    if (Number($("#MaxImpressions").val()) < 1 && Number($("#MaxClicks").val()) < 1) {
        DisplayMessage("Messages", "warn", "Max number of views & Max number of clicks must be larger than 1");
        scrollToElement("Messages", -20, 2);
        $("#contentAddEdit").unblock();
        return false;
    }

    banner.EntityKey = new Object();
    banner.EntityKey.EntityContainerName = "AuroraEntities";
    banner.EntityKey.EntitySetName = "AdBanner";
    banner.EntityKey.IsTemporary = false;
    banner.EntityKey.EntityKeyValues = new Array();
    objKey = new Object();
    objKey.Key = "BannerID";
    banner.BannerID = banner.BannerID == "" ? -1 : parseFloat(banner.BannerID);
    objKey.Value = parseFloat(banner.BannerID) ? parseFloat(banner.BannerID) : -1;
    banner.EntityKey.EntityKeyValues[0] = objKey;
    banner.FarmID = parseInt($("#XY option:selected").attr('farmID'));
    banner.ValidToDate = formatDateTimeLongDateString($("#ValidToDate").val());
    banner.ValidFromDate = formatDateTimeLongDateString($("#ValidFromDate").val());
    banner.IsHtml = banner.IsHtml == "" ? false : banner.IsHtml;
    banner.BannerURL = imageurl === undefined ? $("#bannerImage").attr("src") : imageurl;
    if (imageurl === undefined && $("#bannerImage").attr("src") === undefined) {

        banner.BannerURL = $("#imagesrc").val();
       
    }
    var banSize = $("#XY").val().split(",");
    banner.xsize = banSize[0];
    banner.ysize = banSize[1];
    banner.TypeName = $("#XY option:selected").html();
    var formData = $('#form').serializeArray();
    var pages = [];
    arrayCount = 0;
    $("input:checked[name='ZoneId[]']").each(function () {
        pages.push($(this).val());
    });


    service.UpdateBanners(banner, pages, false, saveSuccess, onError);

}

function deleteData(anchor) {
    var msg = "Item will be removed permanently,\nDo you wish to continue?";

    if (confirm(msg)) {
        var requestDelete = function (result) {
            if (result.Result) {
                loadBannerList(false);
                $("#contentAddEdit").hide();
            }
            else {
                DisplayMessage("Messages", "error", "Sorry but we could not save your data a request has been sent through and we will contact you shortly")
            }
        }

        var item = findByInArray(currentData, "BannerID", anchor.getAttribute("dataid"));


        var getZone = function (result) {
            if (result.Result) {
                service.UpdateBanners(item, result.Data, true, requestDelete, onError);
            }
        }
        service.GetBannerZones(item.BannerID, getZone, onError);

    }
}

function showFilter(dd) {

    var filterSelection = dd.value

    switch (filterSelection) {
        case "Page":
            $('#BannerSizesContainer').hide();
            $('#BannerSizesContainer').css("display", "none")

            loadBannerList(false);
            break;
        case "BannerType":
            $('#BannerSizesContainer').show();
            $('#BannerSizesContainer').css("display", "block")
            bannerMainOption = null;
            bannerButtonOption = null;
            bannerBottomOption = null;

            BannerXY = (GetJSSession("Banner_Size") || "200*200").split("*");
            ButtonXY = (GetJSSession("BannerButton_Size") || "200*200").split("*");
            BottomXY = (GetJSSession("BannerBottom_Size") || "200*200").split("*");

            //TODO Add Identifiers to XY Array and add it to the XY input to assure selected banner is the right one

            //add sizes to dropdown
            $('#BannerSizesContainer').show();

            if ($('#BannerSizesContainer').has("option").length == 0) {
                count = 0;
                var bannerMainOption = $('<option farmID="4" value="' + BannerXY[0] + ',' + BannerXY[1] + '" bannertype="Main">Banner Main (' + BannerXY[0] + 'X' + BannerXY[1] + ')</option>');
                var bannerButtonOption = $('<option farmID="7" value="' + ButtonXY[0] + ',' + ButtonXY[1] + '" bannertype="Button">Banner Button (' + ButtonXY[0] + 'X' + ButtonXY[1] + ')</option>');
                var bannerBottomOption = $('<option farmID="10" value="' + BottomXY[0] + ',' + BottomXY[1] + '" bannertype="Bottom" >Banner Bottom (' + BottomXY[0] + 'X' + BottomXY[1] + ')</option>');
                $('#BannerSizes').append(bannerMainOption)
                .append(bannerButtonOption)
                .append(bannerBottomOption);
                //add a default item 
                $("<option value='0'>Select One..</option>").insertBefore("#BannerSizes option:selected");
                $("#BannerSizes").val("0");
            }
            break;

        default:


    }
}

function getBannerNames() {
    /// <summary>Loads banners in the grid as per the selected filter</summary>
    var $currentItem = $("#BannerSizes option:selected");
    if (!$currentItem.attr("farmID")) {
        return;
    }

    var IsArchived = false;
    var $bannerSize = $currentItem.attr("farmID");
    loadBannerListByType($bannerSize, IsArchived);

}
//#endregion


