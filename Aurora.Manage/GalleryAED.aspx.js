﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />

//#region vars
var templateList;
var menu = "";
var imageTemplate;
var videoTemplate;
var catListAudio;
var catListImage;
var catListVideo;
var Loaded = false;
var TotalCount = new Array();
var CurrentPage = new Array();
var currentSlide;
var CurrentData;
var currentID = 0;
var paginationEnabled = null;
var Global = new GlobalVariables.GlobalVar();
Global.PageSize = 5;
var saveCompleted = false;
//#endregion

//#region Init
$(function () {

    if (getQueryVariable("type") == undefined) {
        document.location = document.location + "?type=Edit";
        return;
    }

    $("#btnSave").hide();
    $("#Cancel").hide();
    if (getQueryVariable('type').toUpperCase() == "EDIT" || getQueryVariable('type').toUpperCase() == "LIST") {
        $('div.content').block({ message: null });
        // $("#contentAddEdit").hide();
        $("#AddNewCat").hide();
        getGalleryCategory();


    } else if (getQueryVariable('type').toUpperCase() == "ADD") {
        $("#contentList").hide();
    }

    //hide headers
    $('.onecolumn .header span').each(function () {


        $(this).parent().parent().children('.content').slideToggle('fast');
        $(this).parent().children('div').slideToggle('fast');
        $(this).parent().children('span').children('sub').show();

    });

    //header click
    $('.onecolumn .header span').click(function () {
        var index = this;
        // Add tooltip to edit and delete button


        $("#contentAddEdit").hide();



        $('.onecolumn .header span').each(function () {
            if (this == index) {

                currentSlide = $(this).parent().attr("name");


                $(this).parent().children('div').slideToggle('fast');

                if ($(this).parent().attr('on')) {
                    $(this).parent().children('span').children('sub').show();
                }
                else {
                    $(this).parent().children('span').children('sub').hide();
                    $(this).parent().attr('on', null);
                }
            }
            else {

                $(this).parent().children('span').children('sub').show();
                $(this).parent().children('div').hide();
                $(this).parent().attr('on', null);

                if ($(this).parent().parent().children('.content').is(':visible'))
                    $(this).parent().parent().children('.content').slideToggle('fast');
            }

        });
    });


    //add category fancy box
    $('.categoryEditFancy').fancybox({
        padding: 0,
        titleShow: true,
        title: '',
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: 800,
        height: 600,
        type: "iframe",
        onClosed: function () {
            getGalleryCategory();
        }
    });
});
//#endregion

//#region Data Manipulation
function getGalleryCategory() {
    var lastIndexAudio = $("#GalleryCat")[0].selectedIndex;
    var lastIndexImage = $("#ImageCat")[0].selectedIndex;
    var lastIndexVideo = $("#VideoCat")[0].selectedIndex;

    var requestCats = function (result) {
        if (result.Data.length != 0) {
            if (catListAudio == null)
                catListAudio = $create(Sys.UI.DataView, {}, {}, {}, $get("GalleryCat"));
            if (catListImage == null)
                catListImage = $create(Sys.UI.DataView, {}, {}, {}, $get("ImageCat"));
            if (catListVideo == null)
                catListVideo = $create(Sys.UI.DataView, {}, {}, {}, $get("VideoCat"));

            var audio = new Array();
            var video = new Array;
            var images = new Array();
            var countAudio = 0;
            var countImages = 0;
            var countVideo = 0;
            for (var item in result.Data) {
                if (result.Data[item].Description == "Audio") {
                    audio[countAudio] = result.Data[item];
                    countAudio++;
                }
                else if (result.Data[item].Description == "Images") {
                    images[countImages] = result.Data[item];
                    countImages++;
                }
                else if (result.Data[item].Description == "Video") {
                    video[countVideo] = result.Data[item];
                    countVideo++;
                }

            }

            catListAudio.set_data(audio);
            // getGallerys('audio');
            catListImage.set_data(images);
            // getGallerys('image');
            catListVideo.set_data(video);

            //getGallerys('video');

            if (!Loaded) {
                Loaded = true;
                $("#GalleryCat")[0].selectedIndex = 0;
                $("#ImageCat")[0].selectedIndex = 0;
                $("#VideoCat")[0].selectedIndex = 0;
            }
            else {
                $("#GalleryCat")[0].selectedIndex = lastIndexAudio;
                $("#ImageCat")[0].selectedIndex = lastIndexImage;
                $("#VideoCat")[0].selectedIndex = lastIndexVideo;
            }



        }
        else {
            $("#contentList").hide();
            $('div.content').unblock();
            $("#AddNewCat").show();
            DisplayMessage("AddMsg", "info", "To start using the Galleries module you need to create an Album.", 0);
        }

    }

    Aurora.Manage.Service.AuroraWS.GetCategories(0, 0, "", 5, 0, requestCats, onError);
}
function getGallerys(template) {
    $(".content").block({ message: null });
    var requestGallerys = function (result) {
        if (result.Data != false) {
        	currentSlide = template;
            var imageData = new Array();
            var videoData = new Array();
            var audioData = new Array();

            CurrentData = result.Data;
            var select = $("div.header").each(
                        function () {
                            if ($(this).attr("name") == currentSlide) {
                                return this;
                            }
                        }
                    );


            switch (template) {
                case "audio":
                    if (templateList == null) {
                        templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
                    }
                    templateList.set_data(result.Data);
                    $("#Messages").hide();
                    break;
                case "image":

                    if (result.Data.length > 0) {
                        if (imageTemplate == null) {
                            imageTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("ImageData"));
                        }
                        imageTemplate.set_data(result.Data);
                        $("#Messages").hide();
                    }
                    else {
                        $(".content").hide();
                        DisplayMessage("Messages", "warn", "No Album Items please add items.", false);
                    }


                    break;
                case "video":

                    if (result.Data.length > 0) {
                        if (videoTemplate == null) {
                            videoTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("VideoData"));
                        }
                        videoTemplate.set_data(result.Data);
                        $("#Messages").hide();
                    }
                    else {
                        $(".content").hide();
                        DisplayMessage("Messages", "warn", "No Album Items please add items.", false);
                    }
                    break;

            }

            // Add tooltip to edit and delete button


            if (TotalCount[template] == undefined && template != undefined) {
                TotalCount[template] = result.Count;

            }
            TotalCount[template] = result.Count;
            if (TotalCount[template] > 5 && paginationEnabled == template) {
                $("#" + template + "Paginate").show();
                initPagination();
                paginationEnabled = null;
            }
            else if (CurrentPage[template] == 0 && TotalCount[template] <= 5) {
                $("#" + template + "Paginate").hide();
            }


            $('div.content').unblock();

            $('.onecolumn').each(
                    function () {
                        if ($(this).attr("id") != "NewCat")
                            if ($(this).children('.header').attr('name') == currentSlide) {

                                $(".tipsy").remove();
                                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', delayIn: 100, delayOut: 0 });

                            }
                    }
                );

            $(".content").unblock();
        }
        else {
            if (result.Data.length == 0) {

                var select;
                $("div.header").each(
                        function () {
                            if ($(this).attr("name") == currentSlide) {
                                select = this;
                            }
                        }
                        );
                $(select).parent().children('.content').hide();
                $('#Messages').insertAfter($(select).parent().children('.content'));
                DisplayMessage("Messages", "warn", "No items in the album.");
                $(".content").unblock();


            }
        }
    }
    //assign a number to current page to stop any null reference exceptions
    if (CurrentPage[template] == undefined) {
        CurrentPage[template] = 0;
    }
    switch (template) {
        case "audio":
            if ($("#GalleryCat").val()) {

                Aurora.Manage.Service.AuroraWS.GetGallery($("#GalleryCat").val(), CurrentPage[template] + 1, Global.PageSize, '', requestGallerys, onError);
                $(".data").show();
            } else {
                DisplayMessage("Messages", "warn", "No Albums created please click the create album button.", false);
                var select = $("div.header[name=" + template + "]");
                $("#Messages").insertAfter(select.next().next());
                $("#Messages").css("display", "block");
                $(".content").unblock();
                $(".data").hide();
                $(".tipsy").remove();
                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', delayIn: 100, delayOut: 0 });
            }
            break;
        case "image":
            if ($("#ImageCat").val()) {

                Aurora.Manage.Service.AuroraWS.GetGallery($("#ImageCat").val(), CurrentPage[template] + 1, Global.PageSize, '', requestGallerys, onError);
                $(".data").show();
            } else {
                DisplayMessage("Messages", "warn", "No Albums created please click the create album button.", false);
                var select = $("div.header[name=" + template + "]");
                $("#Messages").insertAfter(select.next().next());
                $("#Messages").css("display", "block");
                $(".content").unblock();
                $(".data").hide();
                $(".tipsy").remove();
                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', delayIn: 100, delayOut: 0 });
            }
            break;
        case "video":
            if ($("#VideoCat").val()) {
                Aurora.Manage.Service.AuroraWS.GetGallery($("#VideoCat").val(), CurrentPage[template] + 1, Global.PageSize, '', requestGallerys, onError);
                $(".data").show();
            }
            else {
                DisplayMessage("Messages", "warn", "No Albums created please click the create album button.", false);
                var select = $("div.header[name=" + template + "]");
                $("#Messages").insertAfter(select.next().next());
                $("#Messages").css("display", "block");
                $(".content").unblock();
                $(".data").hide();
                $(".tipsy").remove();
                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', delayIn: 100, delayOut: 0 });
            }
            break;

    }



}
function getDetail(anchor) {
    $(".tipsy").remove();
    $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', delayIn: 100, delayOut: 0 });
    var x = new Date();
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#btnSave").show();
            $("#Cancel").show();

        }
    };
    $("#contentAddEdit").block({ message: null });
    if (anchor == undefined) {
        $("#contentAddEdit").load("Gallery.aspx?GalleryId=0&type=" + currentSlide + "&var=" + x.getMilliseconds(), success);
    }
    else {
        $("#contentAddEdit").load("Gallery.aspx?GalleryId=" + anchor.dataid + "&type=" + currentSlide + "&var=" + x.getMilliseconds(), success);
    }

    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
    $('.onecolumn').each(
                function () {
                    if ($(this).attr("id") != "NewCat")
                        if ($(this).children('.header').attr('name') == currentSlide) {
                            $("#contentAddEdit")[0].innerHTML = '';
                            $("#contentAddEdit").css('height', '100px');
                            $("#Messages").insertAfter(this);
                            $("#contentAddEdit").insertAfter("#Messages");
                        }
                }
            );


}
function deleteData(anchor) {
    if (confirm("Item will be removed permanently,\nDo you wish to continue?", "Delete Item")) {
        var gallery = findByInArray(CurrentData, "ID", anchor.dataid);
        if (gallery) {
            gallery.DeletedOn = new Date();
            var requestDelete = function (result) {
                if (result.Result) {
                    page_index = 0;
                    CurrentPage[currentSlide] = page_index;
                    getGallerys(currentSlide);
                    $("#contentAddEdit").hide();

                    $('.onecolumn').each(
                            function () {
                                if ($(this).attr("id") != "NewCat")
                                    if ($(this).children('.header').attr('name') == currentSlide) {
                                        $("#Messages").insertAfter(this);
                                        DisplayMessage("Messages", "success", "Item has been removed", false);
                                    }
                            }
                        );


                }
                else {
                    DisplayMessage("Messages", "error", "Sorry but we could not save your data a request has been sent through and we will contact you shortly")
                }
            }

            Aurora.Manage.Service.AuroraWS.UpdateGallery(gallery, requestDelete, onError);

        }
    }
}
function saveData(saveClick) {

    if (validateForm("GalleryDetails", true, "Messages", false)) {
        return;
    }

    var values = $('#form').serializeArray();
    var Gallerys = new AuroraEntities.Gallery();

    for (property in Gallerys) {
        Gallerys[property] = findInArray(values, property);
    }

    Gallerys.ID = $("#ID").val() == "" || $("#ID").val() == null ? -1 : $("#ID").val();
    var currentCatDropDown = null;
    switch (currentSlide) {
        case "audio":
            Gallerys.CategoryID = Gallerys.CategoryID == "" || Gallerys.CategoryID == null ? $("#GalleryCat").val() : Gallerys.CategoryID;
            currentCatDropDown = $("#GalleryCat");
            break;
        case "image":
            Gallerys.CategoryID = Gallerys.CategoryID == "" || Gallerys.CategoryID == null ? $("#ImageCat").val() : Gallerys.CategoryID;
            currentCatDropDown = $("#ImageCat");
            break;
        case "video":
            Gallerys.CategoryID = Gallerys.CategoryID == "" || Gallerys.CategoryID == null ? $("#VideoCat").val() : Gallerys.CategoryID;
            currentCatDropDown = $("#VideoCat");
            break;

    }

    Gallerys.InsertedOn = Gallerys.InsertedOn == "" || Gallerys.InsertedOn == null ? new Date() : Gallerys.InsertedOn;
    Gallerys.InsertedBy = Gallerys.InsertedBy == "" || Gallerys.InsertedBy == null ? 0 : Gallerys.InsertedBy;
    Gallerys.DeletedBy = Gallerys.DeletedBy == "" || Gallerys.DeletedBy == null ? null : Gallerys.DeletedBy;
    Gallerys.DeletedOn = Gallerys.DeletedOn == "" || Gallerys.DeletedOn == null ? null : Gallerys.DeletedOn;
    Gallerys.Name = Gallerys.Name == null || Gallerys.Name == "" ? $("#Name").val() : $("#Name").val();
    Gallerys.Description = Gallerys.Description == null || Gallerys.Description == "" ? $("#Name").val() : $("#Description").val();
    Gallerys.GalleryIndex = Gallerys.Index == null || Gallerys.Index == "" ? $("#GalleryIndex").val() : $("#GalleryIndex").val();
    Gallerys.Link = Gallerys.Link != null || Gallerys.Link !== "" ? $("#Link").val() : Gallerys.Link;
    if ($(currentCatDropDown).attr("gallerytype") == "Images") {
        Gallerys.MediaType = ".jpg";
    }
    if ($(currentCatDropDown).attr("gallerytype") == "Video") {
        Gallerys.MediaType = "URL";
    }

    if (Gallerys.EntityKey == null && Gallerys.ID != -1) {
        Gallerys.EntityKey = new Object();
        Gallerys.EntityKey.EntityContainerName = "AuroraEntities";
        Gallerys.EntityKey.EntitySetName = "Gallery";
        Gallerys.EntityKey.IsTemporary = false;
        Gallerys.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();
        objKey.Key = "ID";
        Gallerys.ID = Gallerys.ID == "" ? -1 : parseInt(Gallerys.ID);
        objKey.Value = parseInt(Gallerys.ID) ? parseInt(Gallerys.ID) : -1;
        Gallerys.EntityKey.EntityKeyValues[0] = objKey;
        currentID = Gallerys.ID;
    }

    var updateGallery = function (result) {
        if (result.Result) {
            client = client.replace(/~/gi, "");
            var title;
            var galleryType = currentSlide;
            if (galleryType == "image") {
                title = "Current Image";
                if ($('#btn_image1')[0]) {
                    $('#btn_image1')[0].href = String.format("WebImage/WebImageUpload.aspx?itype=images&ipath={0}/Uploads/&iName=gallery_01_{1}.jpg&smWidth={2}&smPrefix={3}&lgWidth={4}", client, result.Data, GetJSSession("GalleryImages_smWidth"), GetJSSession("GalleryImages_smPrefix"), GetJSSession("GalleryImages_lgWidth"));
                }
            } else if (galleryType == "video") {
                title = "Current Image";
                if ($('#btn_image1')[0]) {
                    $('#btn_image1')[0].href = String.format("WebImage/WebImageUpload.aspx?ipath={0}/Uploads/&iName=gallery_01_{1}.jpg&smWidth={2}&smPrefix={3}&lgWidth={4}", client, result.Data, GetJSSession("GalleryVideo_smWidth"), GetJSSession("GalleryVideo_smPrefix"), GetJSSession("GalleryVideo_lgWidth"));
                }
            } else if (galleryType == "audio") {
                title = "Upload File";
                if ($('#btn_image1')[0]) {
                    $('#btn_image1')[0].href = String.format("FileUpload.aspx?extension=.mp3&path={0}/Uploads/&prefix=gallery_01_&id={1}", client, result.Data);
                }
            }
            if (galleryType == "audio") {
                $('#btn_image1').fancybox({
                    padding: 0,
                    transitionIn: 'elastic',
                    transitionOut: 'elastic',
                    titleShow: true,
                    title: title,
                    overlayColor: '#000000',
                    overlayOpacity: .5,
                    showActivity: true,
                    width: 800,
                    height: 200,
                    onClosed: function () { relaodAfterImage(); },
                    type: "iframe"
                });
            }
            else {
                $('#btn_image1').fancybox({
                    padding: 0,
                    transitionIn: 'elastic',
                    transitionOut: 'elastic',
                    titleShow: true,
                    title: title,
                    overlayColor: '#000000',
                    overlayOpacity: .5,
                    showActivity: true,
                    width: '90%',
                    height: '90%',
                    onClosed: function () { relaodAfterImage(); },
                    type: "iframe"
                });
            }

            if (!saveClick) {
                $('#btn_image1')[0].onclick = null;
                $('#btn_image1').click();
            }
            saveCompleted = true;

            changeGalleryCat();
            var anchorGen = new Object();
            anchorGen.dataid = result.Data;
            anchorGen.type = currentSlide;
            getDetail(anchorGen);
        }
        else {
            DisplayMessage("Messages", "error", "There was an error while trying to save the Gallery please try again later.", 4000);
        }
    };

    Aurora.Manage.Service.AuroraWS.UpdateGallery(Gallerys, updateGallery, onError);
}

function addNewCat() {
    $("#contentList").hide();
    $("#AddNewCat").show();
    $('div.content').unblock();
}
function AddGalleryCategory() {
    if (validateForm("addData", true, "AddMsg", true)) {
        return;
    }
    var cat = new Object();
    cat.ClientSiteID = 0;
    cat.Description = $("#CatDescription").val();
    cat.Name = $("#CatName").val();
    cat.OrderIndex = $("#CatOrder").val();
    switch ($("#CatDescription").val()) {
        case "Images":
            cat.FeatureModuleID = 5;
            break;
        case "Audio":
            cat.FeatureModuleID = 5;
            break;
        case "Video":
            cat.FeatureModuleID = 5;
            break;

    }

    cat.InsertedOn = new Date();
    cat.ParentID = 0;


    var requestAddCat = function (result) {
        if (result.Count == 0) {
            DisplayMessage("AddMsg", "success", "Category Added");
            ClearForm();
        }
        else if (result.Count == 1) {
            DisplayMessage("AddMsg", "warn", "A catergory with this name already exists.");
        }
        else {
            DisplayMessage("AddMsg", "error", result.ErrorMessage);
        }
    }

    Aurora.Manage.Service.AuroraWS.UpdateCategory(cat, requestAddCat, onError);

}
function changeGalleryCat() {

    $('div.content').block({ message: null });
    $("#btnSave").hide();
    $("#Cancel").hide();
    var select;
    $("div.header").each(
                        function () {
                            if ($(this).attr("name") == currentSlide) {
                                select = this;
                            }
                        }
                        );

    $(select).parent().children('.content').show();
    $("#Messages").hide();
    CurrentPage[currentSlide] = 0;
    TotalCount[currentSlide] = 0;
    paginationEnabled = currentSlide;
    getGallerys(currentSlide);



}
//#endregion

//#region Custom Methods
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage[currentSlide] = page_index;
        getGallerys(currentSlide);
        $("#btnSave").hide();
        $("#Cancel").hide();
        $("#contentAddEdit").hide();

    } else if (CurrentPage[currentSlide] > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the first page
        CurrentPage[currentSlide] = 0;
        getGallerys(currentSlide);
    }
    // $("#contentAddEdit").hide();
    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#" + currentSlide + "Paginate").pagination(TotalCount[currentSlide], {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
function getDate(date) {
    if (!date) {
        return "";
    } else {
        return formatDateTime(date, "dd/MM/yyyy");
    }
}

function relaodAfterImage() {
    getGallerys(parent.currentSlide);
    var anchor = new Object();
    anchor.dataid = currentID;
    saveCompleted = true;
    getDetail(anchor);

}

//#endregion 

              
