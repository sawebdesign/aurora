﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Aurora.Manage.ToolPages {
    public partial class CategoryAED : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (Request["filter"] != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "hideDescription", "$('#Description').parent().parent().hide();$('#Description').val('" + Request["filter"] + "');", true);
            }
        }
    }
}