﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.ToolPages {
	public partial class MasterEditor : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			var files = Directory.GetFiles(Server.MapPath("/ClientData/" + SessionManager.ClientSiteID + "/MasterPage/"), "*.master");
			var fileInfo = files.Select(s => new FileInfo(s));
			MasterPageDDL.DataSource = fileInfo;
			MasterPageDDL.DataTextField = "Name";
			MasterPageDDL.DataValueField = "FullName";
			MasterPageDDL.DataBind();
			masterpageTextArea.InnerHtml = File.ReadAllText(MasterPageDDL.SelectedValue);
			
			
		}

		protected void MasterPageDDL_SelectedIndexChanged(object sender, EventArgs e) {
			masterpageTextArea.InnerHtml =  File.ReadAllText(MasterPageDDL.SelectedValue);
		}
	}
}