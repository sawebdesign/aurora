﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Scripts/Utils.js" />


var domainManager = {
    createDomain: function() {
        var response = function (result) {
            DisplayMessage("Messages", "success", "Domain Added", 7000);
            domainManager.getDomains();
        };
        //validate
        var domain = $("#domainname").val();
        if (domain.split(".").length > 1 && domain.length > 3 && domain.indexOf("boltcms") == -1) {
            Aurora.Manage.Service.AuroraWS.CreateDomainForClient(domain.toLowerCase().replace("http://",""), $("#boltdomain").val(), response, onError);
        }
        else {
            DisplayMessage("Messages", "warn", "invalid domain entry,please remember that .boltcms domains are not allowed", 7000);
        }
    },
    getDomains: function() {
        var response = function (result) {
            $("#domainsgrid").children().remove();
            var template = ' <tr><td>{0}</td><td align="right"><a href="javascript:void(0);" onclick="domainManager.editDomain(\'{0}\'); return false;"><img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" /></a> <a href="javascript:void(0);" onclick="domainManager.deleteDomain(\'{0}\');return false;" dataid="{{ID}}"><img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a></td></tr>';
            var data = result.Data;
            var hasRoot = (data.hasRoot) || false;
            if (data !== "") {
                for (var item = 0; item < data.domains.length; item++) {
                    $("#domainsgrid").append(String.format(template, data.domains[item]));
                }
            }

            if (hasRoot == false) {
            	DisplayMessage("Messages", "warn", "No root domain is set", 7000);
            }
        };
        
        Aurora.Manage.Service.AuroraWS.GetSiteDomains($("#boltdomain").val(), response, onError);
    },
    editDomain: function (domainName) {
        $("#AddNewDomain").block({ message: null });

        var onSuccess = function (result) {
           domainManager.populateDomainForm(result.Data);
           $("#AddNewDomain").fadeIn(500);
           $("#AddNewDomain").unblock();
           $(".tipsy").remove();
           $("#addData .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        };

        Aurora.Manage.Service.AuroraWS.GetSiteDomain(domainName, onSuccess, onError);
    },
    populateDomainForm: function (values) {
        //$("#Messages").insertBefore($("#AddNewCat"));
        for (var property in values) {
            $('#' + property).each(function () {
                if (property.toLocaleLowerCase() == 'isroot') {
                    if (values[property] == true) {
                        $('#' + property).attr("checked" ,"checked");
                    } else {
                        $('#' + property).removeAttr('checked');
                    }
                }
                $('#' + property).val(values[property]);
            });

        }
        $("#IsHidden").attr('checked', values.IsHidden)

    },
    checkDomain: function() {
        var response = function (result) {
            if (!result.Data) {
                domainManager.createDomain();
            }
            else {
                DisplayMessage("Messages","warn", "That domains already exists", 7000);
            }
        };

        Aurora.Manage.Service.AuroraWS.CheckDomains($("#boltdomain").val(),$("#domainname").val(),response);
    },
    deleteDomain: function(name) {
        var response= function () {
            DisplayMessage("Messages", "warn", "Domain removed", 7000);
            domainManager.getDomains();
        };
        if (confirm("Are your certain you would like to remove this domain binding?")) {
            Aurora.Manage.Service.AuroraWS.RemoveDomain(name, $("#boltdomain").val(), response, onError);
        }
    },
    syncDomains: function() {
        var response = function () {
            domainManager.getDomains();
        };
        Aurora.Manage.Service.AuroraWS.SyncDomains($("#boltdomain").val(),response);
    },
    saveDomain: function () {
        var response = function (result) {
            if (result.Count == 0) {
                DisplayMessage("Messages", "success", "Domain has successfully been updated.", 7000);
                $("#AddNewDomain").hide();
                domainManager.getDomains();
            } else {
                DisplayMessage("Messages", "error", result.ErrorMessage);
            }
        };

        Aurora.Manage.Service.AuroraWS.saveDomain($("#DomainName").val(), $("#isRoot").is(':checked'), response);
    }
};

$(function () {
    domainManager.syncDomains();
    $("#domainname").restrictChars({ regex: /[a-zA-Z\s\b.]/ });
    $(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
});
