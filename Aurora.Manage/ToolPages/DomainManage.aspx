﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="DomainManage.aspx.cs" Inherits="Aurora.Manage.ToolPages.DomainManage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <script src="domainmanage.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>" type="text/javascript"></script>
    <div>
         <div id="Messages">
        </div>
        <div class="onecolumn">
            <input type="hidden" id="boltdomain" clientidmode="Static" runat="server" />
            <div class="header">
                <span>Site Domains</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <div>
                        &nbsp;Add a Domain<input id="domainname" value="" mytitle="Enter a new domain name." class="help" />
                        <img src="/Styles/images/add.png" onclick="domainManager.checkDomain();" style="cursor: pointer;" mytitle="Create a Domain" class="help" />
                    </div>
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <thead>
                        <tr>
                            <th align="left">
                               Domain
                            </th>
                            <th align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody  id="domainsgrid">
                       
                    </tbody>
                </table>
            </div>
        </div>
        <div id="AddMsg">
        </div>
        <div id="AddNewDomain" style="display: none;">
            <div id="Add">
                <div class="inner">
                    <!-- Begin one column window -->
                    <div class="onecolumn">
                        <div class="header">
                            <span>Category</span>

                            <div class="buttonArea">
                                <div class="barButton help" mytitle="Cancel" onclick="domainManager.getDomains();return false"><img src="/Styles/images/cancel.png" alt="Cancel" />Cancel</div>
                                <div class="barButton help" mytitle="Save" onclick="domainManager.saveDomain(); return false;"><img src="/Styles/images/icon_accept.png" alt="Save" />Save</div>
                            </div>
                        </div>
                        <br class="clear" />
                      
                        <div class="content">
                            <table width="100%" id="addData"  >
                                <tr>
                                    <td>
                                        Domain:
                                    </td>
                                    <td>
                                        <input class="help" type="text" id="DomainName" value="" mytitle="Domain Name" disabled />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        isRoot
                                    </td>
                                    <td>
                                        <input type="checkbox" id="isRoot" class="help" mytitle="If this domain is the root for this site. If ticked all requestes to other domain for this "/>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
