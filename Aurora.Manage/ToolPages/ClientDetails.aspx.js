﻿
function validate() {
	var validated = validateForm('siteForm', '', 'MainContent_Messages', false);
	return !validated;
}

$(function () {
	$('#MainContent_ButtonSave').click(validate);
});