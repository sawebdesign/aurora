﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Collections.ObjectModel;

namespace Aurora.Manage.ToolPages {
	public partial class ClientDetails : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
            if (!SessionManager.UserRoles.Select(p => p.RoleName).ToArray().Contains("Admin")) {
				Response.Redirect("~/Dashboard.aspx");
			}
			if (!Page.IsPostBack) {

				InitialiseSiteList();
			}
		}
		void InitialiseSiteList() {
			siteForm.Visible = false;
			using (AuroraEntities context = new AuroraEntities()) {
				var sitesList = (from sites in context.ClientSite
								 orderby sites.CompanyName
								 select new {
									 label = sites.CompanyName,
									 value = sites.ClientSiteID
								 });
				sitesDDL.Items.Clear();
				sitesDDL.DataSource = sitesList;
				sitesDDL.DataTextField = "label";
				sitesDDL.DataValueField = "value";
				sitesDDL.DataBind();
				sitesDDL.Items.Insert(0, new ListItem("-- Select a site --", ""));
			}
		}

		void LoadClientSite(long clientSiteID) {
			sitesDDL.Visible = false;
			siteForm.Visible = true;
			using (AuroraEntities context = new AuroraEntities()) {
				var site = context.ClientSite.Where(w => w.ClientSiteID == clientSiteID).FirstOrDefault();
				var sitesList = (from sites in context.ClientSite
								 where sites.ClientSiteID != clientSiteID
								 orderby sites.CompanyName
								 select new {
									 label = sites.CompanyName,
									 value = sites.ClientSiteID
								 });
				if (site != null) {

					parentsDDL.Items.Clear();
					parentsDDL.DataSource = sitesList;
					parentsDDL.DataTextField = "label";
					parentsDDL.DataValueField = "value";
					parentsDDL.DataBind();
					parentsDDL.Items.Insert(0, new ListItem("None"));
					parentsDDL.SelectedValue = site.ParentClientSiteID.ToString();
					ReadOnlyCollection<TimeZoneInfo> tz = TimeZoneInfo.GetSystemTimeZones();

					SiteTimeZone.DataSource = tz;
					SiteTimeZone.DataTextField = "DisplayName";
					SiteTimeZone.DataValueField = "Id";
					SiteTimeZone.DataBind();
					SiteTimeZone.SelectedValue = site.TimeZoneID ?? "South Africa Standard Time";

					CompanyNameLabel.InnerText = site.CompanyName;
					CompanyName.Value = site.CompanyName;
					CompanyDescription.Value = site.CompanyDescription;
					PhysicalAddressLine1.Value = site.PhysicalAddr1;
					PhysicalAddressLine2.Value = site.PhysicalAddr2;
					PhysicalAddressLine3.Value = site.PhysicalAddr3;
					PhysicalAddressLine4.Value = site.PhysicalAddr4;
					PostalAddressLine1.Value = site.PostalAddr1;
					PostalAddressLine2.Value = site.PostalAddr2;
					PostalAddressLine3.Value = site.PostalAddr3;
					PostalAddressLine4.Value = site.PostalAddr4;
					EmailAddress1.Value = site.Email1;
					EmailAddress2.Value = site.Email2;
					Telephone1.Value = site.Tel1;
					Telephone2.Value = site.Tel2;
					Telephone3.Value = site.Tel3;
					Telephone4.Value = site.Tel4;
					FaxNumber1.Value = site.Fax1;
					FaxNumber2.Value = site.Fax2;
					
				}
			}
		}

		protected void sitesDDL_SelectedIndexChanged(object sender, EventArgs e) {
			if (!string.IsNullOrEmpty(sitesDDL.SelectedValue)) {
				LoadClientSite(Convert.ToInt64(sitesDDL.SelectedValue));
			}
		}

		protected void ButtonSave_Click(object sender, EventArgs e) {
			using (AuroraEntities context = new AuroraEntities()) {
				var siteID = Convert.ToInt64(sitesDDL.SelectedValue);
				var site = context.ClientSite.Where(w => w.ClientSiteID == siteID ).FirstOrDefault();
				if (site != null) {

                    site.ParentClientSiteID = long.Parse(parentsDDL.SelectedValue);
					site.CompanyName = CompanyName.Value;
					site.CompanyDescription = CompanyDescription.Value;
					site.PhysicalAddr1 = PhysicalAddressLine1.Value;
					site.PhysicalAddr2 = PhysicalAddressLine2.Value;
					site.PhysicalAddr3 = PhysicalAddressLine3.Value;
					site.PhysicalAddr4 = PhysicalAddressLine4.Value;
					site.PostalAddr1 = PostalAddressLine1.Value;
					site.PostalAddr2 = PostalAddressLine2.Value;
					site.PostalAddr3 = PostalAddressLine3.Value;
					site.PostalAddr4 = PostalAddressLine4.Value;
					site.Email1 = EmailAddress1.Value;
					site.Email2 = EmailAddress2.Value;
					site.Tel1 = Telephone1.Value;
					site.Tel2 = Telephone2.Value;
					site.Tel3 = Telephone3.Value;
					site.Tel4 = Telephone4.Value;
					site.Fax1 = FaxNumber1.Value;
					site.Fax2 = FaxNumber2.Value;

					context.SaveChanges();
				}
			}
			sitesDDL.Visible = true;
			siteForm.Visible = false;
			SuccessMessage.Visible = true;
		}
	}
	
}