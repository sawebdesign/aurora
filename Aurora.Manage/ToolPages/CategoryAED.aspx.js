﻿/// <reference path="../Scripts/jquery-1.5.2.js" />
/// <reference path="../Scripts/Utils.js" />
/// <reference path="../Scripts/MicrosoftAjax.js" />
/// <reference path="../Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="../Service/AuroraWS.asmx" />

//#region Page Variables
var moduleID;
var filter;
var catList;
var categoryTemplate;
var categoryDetail;
var hasImages;
var TotalCount = 0;
var CurrentPage = 0;
var Global = new GlobalVariables.GlobalVar();
//#endregion

$(function () {
    $(".content").hide();
    $("body").css("background", "none");
    moduleID = getQueryVariable("moduleID") || -1;
    filter = getQueryVariable("filter") || "";
    hasImages = getQueryVariable("hasImages") || false;
    if (hasImages) {
        $('.hideImage').show();
    }
    CategoryAED.getCategories(moduleID);
    $('.alert_warning, .alert_info').click(function () {
        $(this).fadeOut();
    });

});

//#region Page Functions
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        CategoryAED.getCategories(moduleID);
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        CategoryAED.getCategories(moduleID);
    }
    //  $("#contentAddEdit").hide();
    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

function showList() {
    $('#AddNewCat').hide();
    $("#CategoryListContainer").show();
}
//#endregion

//#region CategoryAED
var CategoryAED = {
    getCategories: function (moduleID) {
        function success(result) {
            if (result.Result) {
                $('div.content').show();
                // Add tooltip to edit and delete button

                if (!catList) {
                    catList = $create(Sys.UI.DataView, {}, {}, {}, $get("CategoryList"));
                }

            
                catList.set_data(result.Data);
                if (TotalCount == 0) {
                    TotalCount = result.Count;
                    initPagination();
                }
                TotalCount = result.Count;


                $('div.content').unblock();
                if (result.Count == 0) {
                    $('div.content').hide();
                    DisplayMessage("Messages", "warn", "No Categories Available", false);
                }
                $(".tipsy").remove();
                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });


            }
            else {
                if (!result.Result) {
                    $('div.content').hide();
                    DisplayMessage("Messages", "Error", "Could not load Category data, please try again shortly", false);
                }
            }
        }
        Aurora.Manage.Service.AuroraWS.GetEditCategories(Global.PageSize, CurrentPage + 1, filter, moduleID, 0, success, onError)
    },
    editCategory: function (catID) {
        $("#AddNewCat").block({ message: null });
        if (catID.getAttribute("dataid") != "-1") {
            var onSuccess = function (result) {
                categoryDetail = result.Data[0];
                CategoryAED.populateCategoryForm(categoryDetail);
                $("#AddNewCat").fadeIn(500);
                $("#CategoryListContainer").hide();
                $("#AddNewCat").unblock();
                $(".tipsy").remove();
                $("#addData .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            };

            Aurora.Manage.Service.AuroraWS.GetEditCategories(0, 0, "", moduleID, $(catID).attr("dataid"), onSuccess, onError);
        }
        else {
            $("#AddNewCat").fadeIn(500);
            $("#CategoryListContainer").hide();
            categoryDetail = new AuroraEntities.Category();
            categoryDetail.ID = 0;
            categoryDetail.ParentID = 0;
            categoryDetail.FeatureModuleID = moduleID;
            CategoryAED.populateCategoryForm(categoryDetail);
            $("#AddNewCat").unblock();
            $(".tipsy").remove();
            $("#addData .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
        }


    },
    populateCategoryForm: function (values) {
        //$("#Messages").insertBefore($("#AddNewCat"));
        for (var property in values) {
            $('#' + property).each(function () {
                $('#' + property).val(values[property]);
            });
        }
        $("#IsHidden").attr('checked', values.IsHidden)

    },
    deleteCategory: function (catID) {
        if (catID) {

            if (confirm("Do you wish to delete the this category? Items inside it will be moved to the orphaned category")) {
                var response = function () {
                    DisplayMessage("Messages", "info", "Category Deleted", 7000);
                    $("#AddNewCat").hide();
                    $("#CategoryListContainer").show();
                    CategoryAED.getCategories(moduleID);
                };

                Aurora.Manage.Service.AuroraWS.DeleteCategory(catID, response, onError);
            }
        }

    },
    addCategory: function () {
        /// <summary>Adds a new category/subcategory</summary>
        if (validateForm("addData", "Please ensure you have filled in all fields.", "AddMsg", false)) {
            return;
        }
        var onSuccess = function (result) {
            if (result.Count == 0) {
                DisplayMessage("Messages", "success", "Catergory has successfully been added.", 7000);
                $("#AddNewCat").hide();
                $("#CategoryListContainer").show();
                CategoryAED.getCategories(moduleID);
            }
            else if (result.Count == 1) {
                DisplayMessage("Messages", "warn", "A catergory with this name already exists.", 7000);
            }
            else {
                DisplayMessage("Messages", "error", result.ErrorMessage);

            }
        };

        for (property in categoryDetail) {
            switch (property) {
                case "Description":
                    categoryDetail[property] = getQueryVariable("filter") ? getQueryVariable("filter") : $('#' + property).val();
                    break;
                case "Name":
                case "OrderIndex":
                    categoryDetail[property] = $('#' + property).val();
                    break;
                case "IsHidden":
                    categoryDetail[property] = $('#' + property).is(":checked") ? true : false;
                    break;
                case "ImageWidth":
                    if (hasImages) {
                        categoryDetail[property] = isNumeric($('#' + property).val()) ? $('#' + property).val() : 600;
                    }
                    else {
                        categoryDetail[property] = null;
                    }

            }
        }

        if (categoryDetail.ID === 0) {
            categoryDetail.InsertedOn = new Date();
        }

        Aurora.Manage.Service.AuroraWS.UpdateCategory(categoryDetail, onSuccess, onError);
    }
};

//#endregion