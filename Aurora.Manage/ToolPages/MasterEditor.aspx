﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="MasterEditor.aspx.cs" Inherits="Aurora.Manage.ToolPages.MasterEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<asp:DropDownList runat="server" ID="MasterPageDDL" AutoPostBack="True" OnSelectedIndexChanged="MasterPageDDL_SelectedIndexChanged">
	</asp:DropDownList><br />
	<textarea runat="server" id="masterpageTextArea" style="width:100%; height:500px;">
		
	</textarea>
</asp:Content>
