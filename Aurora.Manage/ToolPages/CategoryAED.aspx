﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoryAED.aspx.cs" Inherits="Aurora.Manage.ToolPages.CategoryAED" %>
<%@ Import Namespace = "Aurora.Custom" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Category Management</title>
    <link href="/Styles/ui.all.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/tipsy.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.custom.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jquery.img.preload.js"></script>
    <script type="text/javascript" src="/Scripts/hint.js"></script>
    <script type="text/javascript" src="/Scripts/visualize/jquery.visualize.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.pagination.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.tipsy.js"></script>
    <script type="text/javascript" src="/Scripts/custom_black.js"></script>
    <script src="/Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/generated/Aurora.Custom.js"></script>
    <script src="/Service/AuroraWS.asmx/js" type="text/javascript"></script>
    <script src="/Scripts/Utils.js" type="text/javascript"></script>
    <script type="text/javascript" src="CategoryAED.aspx.js?123"></script>
        <script type="text/javascript">
            var client = '<%=SessionManager.ClientBasePath%>';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 0px 15px;">
    <div id="Messages"></div>
        <div id="CategoryListContainer">
            <!-- Begin one column window -->
            <div class="inner">
                <div class="onecolumn">
                
                    <div class="header">
                        <span>Categories</span>
                        <div style="float: right; margin-top: 10px; margin-right: 10px;">
                            <img src="/Styles/images/add.png" dataid="-1" class="help" alt="Create Category" mytitle="Add"
                                style="cursor: pointer" onclick="CategoryAED.editCategory(this);" dataid="-1"/>
                        </div>

                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table width="100%" class="data" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Name
                                    </th>
                                    <th align="left">
                                        Order
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th>
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="CategoryList" class="sys-template" sys:attach="dataview">
                                <tr ishidden="{{IsHidden}}">
                                    <td width="30%">
                                        {{Name}}
                                    </td>
                                    <td width="10%">
                                        {{OrderIndex}}
                                    </td>
                                    <td width="10%">
                                        {{Description}}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="CategoryAED.editCategory(this);return false;"
                                            dataid="{{ID}}" >
                                            <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" /></a> <a href="javascript:void(0);"
                                                onclick="CategoryAED.deleteCategory(this.getAttribute('dataid'));return false;" dataid="{{ID}}" >
                                                <img src="../styles/images/icon_delete.png"  class="help" mytitle="Delete"/></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Begin pagination -->
                        <div id="Pagination">
                        </div>
                        <!-- End pagination -->
                    </div>

                </div>
            </div>
        </div>
        <div id="AddMsg">
        </div>
        <div id="AddNewCat" style="display: none;">
            <div id="Add">
                <div class="inner">
                    <!-- Begin one column window -->
                    <div class="onecolumn">
                        <div class="header">
                            <span>Category</span>

                            <div class="buttonArea">
                                <div class="barButton help" mytitle="Cancel" onclick="showList();return false"><img src="/Styles/images/cancel.png" alt="Cancel" />Cancel</div>
                                <div class="barButton help" mytitle="Save" onclick="CategoryAED.addCategory(); return false;"><img src="/Styles/images/icon_accept.png" alt="Save" />Save</div>
                            </div>
                        </div>
                        <br class="clear" />
                      
                        <div class="content">
                            <input type="hidden" id="CatID" />
                            <table width="100%" id="addData"  >
                                <tr>
                                    <td>
                                        Name:
                                    </td>
                                    <td>
                                        <input class="help" type="text" id="Name" valtype="required" value="" mytitle="Category Name"/>
                                    </td>
                                </tr>
                                <tr >
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <input type="text" id="Description" value="" class="help" mytitle="Category Description" value="<%=Request["filter"] %>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Order
                                    </td>
                                    <td>
                                        <input type="text" id="OrderIndex" maxlength="3" valtype="required" value="" class="help" mytitle="Category Order"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Hide Category
                                    </td>
                                    <td>
                                        <input type="checkbox" id="IsHidden" class="help" mytitle="Whether or not a category will be hidden from view"/>
                                    </td>
                                </tr>
                                <tr class="hideImage" style="display:none">
                                    <td>
                                        Image Width
                                    </td>
                                    <td>
                                        <input type="text" id="ImageWidth" value="" class="help" mytitle="The width of images for this category" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
