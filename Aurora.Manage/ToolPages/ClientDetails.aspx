﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="ClientDetails.aspx.cs" Inherits="Aurora.Manage.ToolPages.ClientDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="ClientDetails.aspx.js" type="text/javascript"></script>
    <div class="onecolumn">
        <div class="header">
            <span>Site Details</span>
        </div>
        <div class="clear"></div>
        <div class="content">
            <asp:DropDownList ID="sitesDDL" runat="server" ClientIDMode="Static" AutoPostBack="True" OnSelectedIndexChanged="sitesDDL_SelectedIndexChanged"></asp:DropDownList>
            
            <div class="alert_success" runat="server" id="SuccessMessage" visible="false">
                <p >
                    <img class="mid_align" alt="success" src="../Styles/images/icon_accept.png"/>
                    Site client information updated.
                </p>
            </div>
            <div id="siteForm" runat="server" clientidmode="static"> 
                <div class="alert_info" runat="server" id="Messages">
                <p runat="server" id="messagetext">
                    <img id="icon" class="mid_align" alt="success" src="../Styles/images/icon_info.png"
                        runat="server" />
                    Please enter all the details in the fields provided.
                </p>
            </div>
            
            <div style="width: 400px; float: left;" >
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <th colspan="2">
                            <h3 runat="server" id="CompanyNameLabel">Company Details</h3>
                        </th>
                    </tr>
                    <tr>
                        <td>Site Parent:</td>
                        <td>
                            <asp:DropDownList ID="parentsDDL" runat="server" ClientIDMode="Static" style="width:297px;"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Site Time Zone:</td>
                        <td>
                            <asp:DropDownList ID="SiteTimeZone" runat="server" ClientIDMode="Static" style="width:297px;"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Company Name:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="CompanyName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Company Description:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="CompanyDescription" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="PhysicalAddressLine1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 2:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="PhysicalAddressLine2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 3:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="PhysicalAddressLine3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 4:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="PhysicalAddressLine4" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="PostalAddressLine1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="PostalAddressLine2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 3:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="PostalAddressLine3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 4:
                        </td>
                        <td>
                            <input type="text" runat="server" id="PostalAddressLine4" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" Text="Save" ID="ButtonSave" OnClick="ButtonSave_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 400px; float: left; margin-top:-17px;">
                <br />
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <th>
                            <h3>Contact Details</h3>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Email Address 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="EmailAddress1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email Address 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="EmailAddress2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" maxlength="12" id="Telephone1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 2:
                        </td>
                        <td>
                            <input type="text" runat="server" maxlength="12" id="Telephone2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 3:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="Telephone3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 4:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="Telephone4" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fax Number 1:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="FaxNumber1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fax Number 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="FaxNumber2" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 400px;">
                <br class="clear" />
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
            </div>
        </div>
    </div>
</asp:Content>
