﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="301Redirects.aspx.cs" Inherits="Aurora.Manage.ToolPages._301Redirects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script src="301Redirects.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>" type="text/javascript"></script>
    <!-- List Template -->
	<script id="listTemplate" type="text/x-jsrender">
        <tr id="LineItem_{{:Index}}">
            <td>{{:OldURL}}</td>
            <td>{{:NewURL}}</td>
            <td align="right">
				 <a href="javascript:void(0);" class="loadDetail" dataid="{{:ID}}">
					<img src="../styles/images/icon_edit.png" alt="edit" class="help" mytitle="Edit" />
				 </a>
                <a href="javascript:void(0);" class="deleteData" dataid="{{:ID}}">
					<img src="../styles/images/icon_delete.png" alt="delete" class="help" mytitle="Delete" />
                </a>
            </td>
        </tr>
	</script>
	<!-- End List Template -->

	<!-- Edit Template -->
	<script id="editTemplate" type="text/x-jsrender">
		 <div class="onecolumn">
                <div class="header">
                    <span>301 Redirect</span>
                </div>
                <br class="clear" />
                <div class="content">
                 <div class="tab_content" id="tab1_content">
                    <div id="ItemDetails" style="width:100%;">
                        <fieldset>
                            <input type="hidden" id="ID" name="ID" value="{{:ID}}" />
							<table width="100%">
								<tr>
									<td>Old URL :</td>
									<td>
										<input type="text" id="OldURL" name="OldURL" valtype="required;regex:URL" class="help" 
											mytitle="Enter your sites old URL E.G: http://www.site.co.za/mypage.php" value="{{:OldURL}}" />
									</td>
								</tr>
								<tr>
									<td>New URL :</td>
									<td>
										<input type="text" id="NewURL" name="NewURL" valtype="required;regex:URL" class="help" 
											mytitle="Enter your sites new URL E.G: http://www.site.co.za/page/mypage" value="{{:NewURL}}">
									</td>
								</tr>
							</table>
                        </fieldset>
					  </div>
                    </div>
                </div>
      
      </div>
       <p style="margin-top: 20px; margin-right: 15px" align="right">
            <input type="button" id="btnSave" class="saveRedirect" value="Save" />
        </p>
	</script>
	<!-- End Edit Template -->
	<div>
		<div id="warnnigSEOLandMine" class="alert_warning"></div>
        <div class="onecolumn">
			
            <div class="header">
                <span>301 Redirects</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <div>
                      <img src="/Styles/images/add.png" dataid="0" style="cursor: pointer;"
                          mytitle="Create a Domain" class="help loadDetail" />
                    </div>
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data" width="100%" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 40%" align="left">
                                Old URL	
                            </th>
                            <th style="width: 40%" align="left">
                                New URL
                            </th>
                            <th style="width: 20%" align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody id="redirectsList">
					</tbody>
				</table>
            </div>
        </div>
        <div id="Messages"></div>
		<div id="EditorContainer" style="margin-top: 20px;"></div>
    </div>
</asp:Content>
