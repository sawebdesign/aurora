﻿var RedirectsPage = {
	getRedirects: function () {
		$("#redirectsList").block({message:""});
		var response = function (result) {
			$("#redirectsList").html($("#listTemplate").render(result.Data));
			$("#redirectsList").unblock();

			$(".loadDetail").unbind();
			$(".deleteData").unbind();
			PageEvents.bindEvents();

			$(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
		};

		Aurora.Manage.Service.AuroraWS.Get301Redirects(response, onError);
	},
	loadEditor: function () {

		var response = function (result) {
			$("#EditorContainer").html($("#editTemplate").render(result.Data));
			scrollToElement("EditorContainer", -20, 2);

			$(".saveRedirect").unbind();
			$(".saveRedirect").click(RedirectsPage.updateRedirect);

			$(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
		};
		Aurora.Manage.Service.AuroraWS.Get301RedirectById($(this).attr("dataid"),response, onError);
	},
	updateRedirect: function () {
		//validte form returns a bool of hasErrors. 
		if (!validateForm("EditorContainer", true, "Messages", false)) {
			var dbEntity = new AuroraEntities.C301Redirect();
			$("#EditorContainer").mapToObject(dbEntity, "name");
			var response = function (result) {
				if (result.Data.success) {
					$("#EditorContainer").html('');
					RedirectsPage.getRedirects();
					DisplayMessage("Messages", "success", "successfully saved the 301 redirect rule.");


					if (!result.Data.isRoot) {
						$("<div>You have created a <strong>301 Redirect</strong> rule, that is different from your <strong>root</strong> domain. We advise that you carefully check this record, as an infinite redirect loop could easily arise from within these conditions.</div>").dialog({
							width: 500,
							title: "Warning",
							modal: true,
							buttons:[
									{
										text: "I take full Responsibility",
										click: function () {
											$(this).dialog("close");
										}
									}
								]
						});
					}
				} else {
					DisplayMessage("Messages", "error", "Unable to save your 301 Redirect rule, please try again or contact our support desk.");
				}
			};
			Aurora.Manage.Service.AuroraWS.Updatate301redirect(dbEntity.ID, dbEntity.OldURL, dbEntity.NewURL, response, onError);
		}
	},
	deleteRedirect: function () {

		$("#redirectsList").block({ message: "" });
		var response = function () {
			PageEvents.init();
			$("#redirectsList").unblock();
		};
		Aurora.Manage.Service.AuroraWS.Delete301redirect($(this).attr("dataid"), response, onError);
	}

};

var PageEvents = {
	init: function () {
		DisplayMessage("warnnigSEOLandMine", "error", "Warning: Messing with 301 Redirects can <STRONG style='text-transform:uppercase;'>OBLITERATE</STRONG> your sites <span style='text-transform:uppercase;'>SEO</span> rankings! <img src='/styles/images/seo_Death.png' />");
		RedirectsPage.getRedirects();
		this.bindEvents();
	},
	bindEvents: function () {
		$(".loadDetail").click(RedirectsPage.loadEditor);
		$(".deleteData").click(PageEvents.delete_Click);
	},
	delete_Click: function () {
		var btn = $(this);
		$("<div>You are about to delete this 301 Redirect Rule!</div>").dialog({
			modal: true,
			width: 300,
			buttons:[
				{
					text: "OK",
					click: function () {
						RedirectsPage.deleteRedirect.call(btn);
						$(this).dialog("close");
					}
				}
			]
		});
	}
};

$(function () {
    PageEvents.init();
});