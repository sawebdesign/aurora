﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Aurora.Custom;

namespace Aurora.Manage.ToolPages {
    /// <summary>
    /// Summary description for FileUpload1
    /// </summary>
    public class FileUpload1 : IHttpHandler, IReadOnlySessionState {

        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context) {
            context.Response.ContentType = "text/plain";
            context.Response.Write(Uploader(context));
            return;
        }

        private string Uploader(HttpContext contextUpload) {
            string uploadMessage = string.Empty;
            try {

                HttpPostedFile file = contextUpload.Request.Files["qqfile"];

                string uploadType = (contextUpload.Request.QueryString["UploadType"] == "") ? null : contextUpload.Request.QueryString["UploadType"];
                string fName;
                string DisplayName = (contextUpload.Request.QueryString["DisplayName"] == "") ? null : contextUpload.Request.QueryString["DisplayName"];
                string FileName = (contextUpload.Request.QueryString["FileName"] == "") ? null : contextUpload.Request.QueryString["FileName"];
                string Path = (contextUpload.Request.QueryString["Path"] == "") ? null : contextUpload.Request.QueryString["Path"];
                string fileIDName = (contextUpload.Request["ID"] == "") ? null : contextUpload.Request["ID"];
                int imageSize = (contextUpload.Request["imageSize"] == null) ? 0 :int.Parse(contextUpload.Request["imageSize"]);
                bool useHeight = (contextUpload.Request["useHeight"] == null) ? false : (bool.Parse(contextUpload.Request["useHeight"]));

                if (uploadType == "File") {

                    if (file == null) {
                        //if mozila
                        byte[] buffer = new byte[1024];
                        if (fileIDName == null)
                        {
                            fName = Path + contextUpload.Request.QueryString["qqfile"];
                        }
                        else
                        {
                            fName = Utils.CreateFileNameFromID(fileIDName, ref contextUpload, Path + contextUpload.Request["qqfile"]);
                        }

                       
                        System.IO.File.WriteAllBytes(fName, ReadToEnd(contextUpload.Request.InputStream));

                    }
                    else {
                        //else IE
                        if (fileIDName == null) {
                            fName = Path + file.FileName.Split('\\').Last();
                        }
                        else {
                            fName = Utils.CreateFileNameFromID(fileIDName, ref contextUpload, Path + file.FileName.Split('\\').Last()).Replace("file_",string.Empty);
                        }

                        if (fName.Contains("\\")) {
                            file.SaveAs(fName);
                        }
                        else{
                            file.SaveAs(contextUpload.Server.MapPath(fName));
                        }

                        if (imageSize > 0)
                        {
                            Utils.ResizeImageFromFile(contextUpload.Server.MapPath(fName), imageSize, contextUpload.Server.MapPath(fName), useHeight);
                        }                        
                    }

                    uploadMessage = "{\"success\":true,\"filename\":'"+fName+"'}";
                }

            }
            catch (Exception exception) {

                if (exception.Message == "An error occurred while updating the entries. See the inner exception for details.") {
                    if (exception.InnerException.Message == "Violation of UNIQUE KEY constraint 'uc_FileName'. Cannot insert duplicate key in object 'admin.Repository'.\r\nThe statement has been terminated.")
                        uploadMessage = "{\"error\":\"" + "A file with this name has already been uploaded in this location." + "\"}";
                }
                else {
                    uploadMessage = "{\"error\":\"" + exception.Message + "\"}";
                }
            }
            return uploadMessage;
        }

        public bool IsReusable {
            get {
                return false;
            }
        }

        public static byte[] ReadToEnd(System.IO.Stream stream) {
            long originalPosition = stream.Position;
            stream.Position = 0;
            try {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length) {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1) {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp; totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead) {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally {
                stream.Position = originalPosition;
            }
        }

       
        #endregion
    }
}