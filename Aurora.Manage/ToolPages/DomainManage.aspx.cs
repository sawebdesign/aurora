﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;
using Microsoft.Web.Administration;
using System.Configuration;

namespace Aurora.Manage.ToolPages {
    public partial class DomainManage : System.Web.UI.Page {

        protected void Page_Load(object sender, EventArgs e) {
            using (var entities = new AuroraEntities()) {
				string domainSetting = ConfigurationManager.AppSettings["DefaultDomain"];
                var defaultDomain = (from domains in entities.Domain
                                     where domains.ClientSiteID == SessionManager.ClientSiteID
									 && domains.DomainName.Contains(domainSetting)
                                     select domains).SingleOrDefault();

                boltdomain.Value = defaultDomain.DomainName;
            }
        }
    }
       
}