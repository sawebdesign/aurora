﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Custom.UI;

namespace Aurora.Manage {
    public partial class CustomModulesData : System.Web.UI.Page {
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        private Custom.Data.CustomModuleData customDataXML = new Custom.Data.CustomModuleData();
        private string customModuleName;

        protected void Page_Load(object sender, EventArgs e) {
            int customID, moduleID;
            int.TryParse(Request["CustomID"], out customID);
            int.TryParse(Request["ModuleID"], out moduleID);

            ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();
            ModuleID.Value = Request["CustomID"];

            //create xdoc to parse to XML panel
            XmlDocument moduleSchema = new XmlDocument();
            XmlDocument moduleData = new XmlDocument();
            if (moduleID != 0) {
                //get schema and data xml

                Utils.CheckSqlDBConnection();
                SqlCommand dbCommand = new SqlCommand();

                dbCommand.CommandType = CommandType.StoredProcedure;
                dbCommand.CommandText = "[dbo].[proc_CustomModules_GetData]";
                dbCommand.Parameters.AddWithValue("@CustomModuleID", customID);
                dbCommand.Parameters.AddWithValue("@CustomModuleDataID", moduleID);
                DataSet ds = Utils.SqlDb.ExecuteDataSet(dbCommand);


                //Load XML
                if (!String.IsNullOrEmpty(ds.Tables[0].Rows[0]["XMLSchema"].ToString()))
                {
               
                    moduleSchema.LoadXml(Utils.ConvertDBNull(ds.Tables[0].Rows[0]["DataSchemaXML"], ds.Tables[0].Rows[0]["XMLSchema"].ToString()));
                }

                if (!String.IsNullOrEmpty(ds.Tables[0].Rows[0]["Data"].ToString())) {
                    moduleData.LoadXml(ds.Tables[0].Rows[0]["Data"].ToString());
                }

                customDataXML = (from custData in entities.CustomModuleData
                                 where custData.ID == moduleID
                                 select custData).SingleOrDefault();

                ID.Value = customDataXML.ID.ToString();
                InsertedOn.Value = customDataXML.InsertedOn.Value.ToString();

            }
            else {
                var schema = (from customModules in entities.CustomModules
                              where customModules.ID == customID
                              select customModules).SingleOrDefault();

                moduleSchema.LoadXml(schema.XMLSchema);
            }


            ContentXml.XmlPanelSchema = moduleSchema;
            ContentXml.XmlPanelData = moduleData;
            ContentXml.UploaderEventHandler += UploadFile;
            ContentXml.RenderXmlControls("tbl_Form");

            if (Request["del"] != null) {
                customDataXML.DeletedBy = SessionManager.UserID;
                customDataXML.DeletedOn = DateTime.Now;

                customDataXML.SetAllModified(entities);
                entities.SaveChanges();
            }

            //Load Files
            customModuleName = (from customModules in entities.CustomModules
                                where customModules.ID == customID
                                select customModules.Name).Single();

            string path = Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID,
                                          customModuleName, customDataXML.ID));
            if (Directory.Exists(path)) {
                if (Directory.GetFiles(path).Count() > 0) {
                    fileLink.Visible = true;
                    fileLink.InnerHtml = "View Files(" + Directory.GetFiles(path).Count() + ")";
                    fileLink.HRef = String.Format("http://auroramanage/Innova/assetmanager/assetmanager.asp?ClientID={0}&ffilter=&inpCurrFolder={1}", SessionManager.ClientSiteID, Server.UrlEncode(path));
                }
            }

        }

        //protected void btnSave_Click(object sender, EventArgs e) {

        //    if (customDataXML.ClientSiteID == 0)
        //    {
        //        long moduleID = long.Parse(Request["CustomID"]);

        //        string schemaXML = (from schemas in entities.CustomModules
        //                           where schemas.ID == moduleID
        //                           select schemas.XMLSchema).SingleOrDefault();

        //        //check for multiline check boxes
        //        string customXMLData = ContentPanel.WriteObjectToXml().InnerXml.Replace("ContentPanel", "XmlData");

        //        customDataXML = new Custom.Data.CustomModuleData {
        //            ClientSiteID = SessionManager.ClientSiteID.Value,
        //            Data = customXMLData,
        //            InsertedOn = DateTime.Now,
        //            DataSchemaXML = schemaXML,
        //            ModuleID = long.Parse(Request["CustomID"])
        //        };

        //        entities.AddToCustomModuleData(customDataXML);
        //    }
        //    else {
        //        customDataXML.Data = ContentPanel.WriteObjectToXml().InnerXml.Replace("ContentPanel", "XmlData");
        //        customDataXML.ClientSiteID = SessionManager.ClientSiteID.Value;
        //        customDataXML.ID = long.Parse(ID.Value);
        //        customDataXML.InsertedOn = DateTime.Parse(InsertedOn.Value);
        //        customDataXML.SetAllModified(entities);
        //    }
        //    entities.SaveChanges();



        //    Response.Redirect(Request.Url.ToString() + "&ex=1");
        //}

        protected void UploadFile(object sender, EventArgs e) {
            int instances = 0;
            System.Web.UI.WebControls.FileUpload myUploader = (System.Web.UI.WebControls.FileUpload)sender;

            if (myUploader.HasFile) {


                if (!Directory.Exists(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, customModuleName)))) {
                    Directory.CreateDirectory(Server.MapPath(String.Format("~/ClientData/{0}/Uploads", SessionManager.ClientSiteID)) + "/" + customModuleName);
                }

                if (Directory.Exists(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID, customModuleName, customDataXML.ID)))) {
                    instances =
                        Directory.GetFiles(
                            Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID, customModuleName, customDataXML.ID))).Count();
                }
                else {
                    Directory.CreateDirectory(
                        Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID,
                                                     customModuleName)) + "/" + customDataXML.ID);
                }

                myUploader.SaveAs(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/file_{2}_{4}{3}", SessionManager.ClientSiteID, customModuleName, customDataXML.ID, myUploader.PostedFile.FileName.Substring(myUploader.PostedFile.FileName.IndexOf(".")), instances + 1)));
            }
        }
    }
}