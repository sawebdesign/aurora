﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/MemberManagement/SiteMembersAED.aspx.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
$(function () {
    if (!checkList) {
        checkList = $create(Sys.UI.DataView, {}, {}, {}, $get("Roles"));
    }
    else {
        checkList.dispose();
        checkList = $create(Sys.UI.DataView, {}, {}, {}, $get("Roles"));
    }
    checkList.set_data(rolesData);
    var rolesList = $("#Roles option");

    for (var option = 0; option < rolesList.length; option++) {
        if (findByInArray(currentMemberRole, "RoleID", rolesList[option].value)) {
            rolesList[option].setAttribute("selected", "selected");
        }
    }
    $('#Roles').toChecklist({
        /**** Available settings, listed with default values. ****/
        addScrollBar: true,
        addSearchBox: true,
        searchBoxText: 'Type here to search list...',
        showCheckboxes: true,
        showSelectedItems: true,
        submitDataAsArray: true // This one allows compatibility with languages that use arrays

    });

    transferCustomFields("CustomData", "ContentPanel");
    //multiple select ddl
    $("[multipleselectvalues]").each(
        function () {
            var $curselect = $(this);
            var items = $curselect.attr("multipleselectvalues").toString().split(",");

            for (var val = 0; val < items.length; val++) {
                if (items[val] !== "") {
                    var toSelect = $curselect.find("option[value='" + items[val] + "']");
                    if (toSelect.length > 0) {
                        toSelect.attr("selected", "selected");
                    }
                }
            }

        }
    );
    //used for custom fields adds a style to HTML Labels
    $(".fldhtmlContent").parent().parent().css({ border: '1px solid #eee', height: '35px', padding: '5px;', fontWeight: 'bold', backgroundImage: 'none', backgroundAttachment: 'scroll', backgroundRepeat: 'repeat', backgroundColor: '#fef4d2' })
    $("#flddateofbirth").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true,
        changeYear: true, yearRange: "-100:+1"
    });

});