﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" ClientIDMode="Static" CodeBehind="SiteMembersAED.aspx.cs"
    Inherits="Aurora.Manage.MemberMangement.SiteMembers" %>
<%@ Import Namespace="Aurora.Custom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="SiteMembersAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"
        type="text/javascript"></script>
    <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/fg.menu.js"></script>
    <link type="text/css" href="/Styles/fg.menu.css" media="screen" rel="stylesheet" />
    <style>
        #memberBox
        {
            position:fixed;
            bottom:0;
            right:0;
        }
    </style>
    <div class="onecolumn" id="Members">
        <div class="header">
            <span>Members</span>
            <div style="float: left; margin-top: 10px; margin-right: 10px;">
                <div>
                    &nbsp; <strong>Categories</strong>
                    <select id="Categories" onchange="CurrentPage = 0; TotalCount=0; $('#Criteria').hide(); getMembers(false,true);" style="width:250px">
                        
                    </select>
                </div>
            </div>
            <div style="float: left; margin-top: 10px; margin-right: 10px;">
                <div>
                    &nbsp; <strong>Status</strong>
                    <select id="Status" onchange="CurrentPage = 0; TotalCount=0; $('#Criteria').hide(); getMembers(false,true);">
                        <option>Active</option>
                        <option value="InActive">In-Active</option>
                        <option>Pending</option>
                    </select>
                </div>
				
            </div>
			<img src="/Styles/images/report_go.png" class="exportMemberButton" mytitle="Export Data" style="cursor: pointer" onclick="exportData();">
            <!--div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img id="Add" src="/Styles/images/add.png" onclick="editMember(0);" style="cursor: pointer;"
                    mytitle="Create a Member" class="help" />
            </div-->
            <div style="float: right; margin-top: 10px;">
                <a id="lnkFilter" href="javascript:void(0);" onclick="$('#Criteria').slideToggle();">Filter</a>
                <div id="Criteria" style="min-width: 200px; margin-top: 15px; margin-right:30px;display:none;">
                    <div style="float: left">
                        &nbsp; <b>Filter By:</b>
                        <select id="FilterBy" onchange="changeFilter();">
                            <option value="Latest">Latest Members</option>
                            <option value="MemberName">Member Name/Email</option>
                            <option value="Custom">Custom Fields</option>
                        </select>
                    </div>
                    <div style="float: right; line-height: 19px; padding: 5px">
                        <a href="javascript:void(0);" id="btnSearch" onclick="getMembers(false,true);">Search</a>
                    </div>
                    <div style="float: right">
                        <input type="text" id="nameSearch" value="" style=" width: 100px;display:none;" />
                    </div>
                </div>
                &nbsp;
            </div>
           
        </div>
        <br class="clear" />
        <div class="content" id="memberDiv">
            <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%">
                <thead>
                    <tr>
                        <th align="left">
                            Select
                        </th>
                        <th align="left">
                            First Name
                        </th>
                        <th align="left">
                            Last Name
                        </th>
                        <th align="left">
                            Email
                        </th>
                        <th align="left">
                            Mobile
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" id="membersgrid">
                    <tr>
                        <td>
                            <input type="checkbox" value="{{ID}}" name="{{FirstName + ' ' + LastName}}"
                                class="selectBox" />
                        </td>
                        <td>
                            {{FirstName}}
                        </td>
                        <td>
                            {{LastName}}
                        </td>
                        <td>
                            {{Email}}
                        </td>
                        <td>
                            {{String(Mobile).isNullOrEmpty("N/A")}}
                        </td>
                        <td align="right">
                            <a href="javascript:void(0);" onclick="editMember($(this).attr('dataid'), $(this).attr('categoryID'));return false;"
                                dataid="{{ID}}" categoryID="{{MemberCategoryID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                            <%--<a href="javascript:void(0);" onclick="editMember($(this).attr('dataid'), $(this).attr('categoryID'),true);return false;"
                                dataid="{{ID}}" categoryID="{{MemberCategoryID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a>--%>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="Pagination"></div>
        </div>
    </div>
    <div id="Messages">
    </div>
    <br class="clear" />
    <div id="contentEditAdd">
    </div>
    <div style="float: right;">
        <input id="btnSave" onclick="doSave();" type="button" value="Save" />
    </div>
</asp:Content>
