﻿//#region refs
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.Debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.Debug.js" />
/// <reference path="/Service/AuroraWS.asmx" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
//#endregion

//#region Vars
var membersDS = null;
var rolesDS = null;
var checkList = null;
var rolesData = null;
var currentMemberRole = [];
var CurrentPage = 0;
var TotalCount = 0;
var globals = new GlobalVariables.GlobalVar();
var organisationDS = null;
var categoryData;
var currentOrganisation = 0;
var donorSelectList = [];
var fileViewRefresh = function () {
    editMember($("#MemberForm [name=ID]").val(), $('#CategoryID').val());
   
}
//#endregion

//#region Category Management Functions
var categoryFunctions = {
    payMembership : function (memberCategoryID) {
        Aurora.Manage.Service.AuroraWS.PayMembership(memberCategoryID, categoryEvents.response, onerror, memberCategoryID);
    },
    activateMembership: function (memberCategoryID) {

        Aurora.Manage.Service.AuroraWS.ActivateMembership(memberCategoryID, categoryEvents.response, onerror, memberCategoryID);
    },
    extendMembership: function (expiryDate, memberCategoryID) {
        Aurora.Manage.Service.AuroraWS.ExtendMembership(expiryDate, memberCategoryID, categoryEvents.response, onerror, memberCategoryID);
    },
    changeMembership: function (memberCategoryID, newCategoryID) {
        Aurora.Manage.Service.AuroraWS.ChangeMembership(memberCategoryID, newCategoryID, categoryEvents.response, onerror, memberCategoryID);
    },
    cancelMembership: function (memberCategoryID) {
        Aurora.Manage.Service.AuroraWS.CancelMembership(memberCategoryID, categoryEvents.response, onerror, memberCategoryID);
    }
}

var categoryEvents = {
    payMembershipClick: function (e) {
        //var memberCatID = $('#MemberCategoryID').val();
        categoryFunctions.payMembership($('#MemberCategoryID').val());
    },
    activateMembershipClick: function (e) {
        //$this = $(e.currentTarget);
        //var memberCatID = $('#MemberCategoryID').val();
        categoryFunctions.activateMembership($('#MemberCategoryID').val());
    },
    extendMembershipClick: function (e) {
        //$this = $(e.currentTarget);
        var memberCatID = $('#MemberCategoryID').val();
        if ($('#ExtendMemberModal').length === 0) {
            var $dialog = $('<div id="ExtendMemberModal"></div>');
            var $datePicker = $('<div>Pick the date you would like this membership to expire: <input type="text" id="ExtensionDate" class="Datepicker"/>');
            $dialog.append($datePicker);
            $dialog.dialog({
                modal: true,
                buttons: {
                    'Change Expiry Date': function () {
                        if ($('#ExtensionDate').val() === "Never") {
                            return false;
                        }
                        $(this).dialog('close');
                        $("#contentEditAdd").block({ message: null });
                        categoryFunctions.extendMembership($('#ExtensionDate').val(), $('#MemberCategoryID').val());
                    }
                }
            });
        }
        else {
            $('#ExtendMemberModal').dialog('open');
        }
        
        $('#ExtendMemberModal .Datepicker').datepicker({
            dateFormat: 'yy/mm/dd'
        }).val($('#ExpiresOnlbl').text());
    },
    changeMembershipClick: function (e) {
        var memberCatID = $('#MemberCategoryID').val();
        if ($('#ChangeMemberModal').length === 0) {
            var $dialog = $('<div id="ChangeMemberModal"></div>');
            var $content = $('<div>Please note that this will create a new membership. Any previous expiry dates and costings will no longer apply: <select id="ChangeMembershipSelect" style="width:250px"/>');
            $dialog.append($content);
            $dialog.dialog({
                modal: true,
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    },
                    'Change Membership': function () {
                        $(this).dialog('close');
                        $("#contentEditAdd").block({ message: null });
                        categoryFunctions.changeMembership($('#MemberCategoryID').val(), $('#ChangeMembershipSelect').val());
                    }
                },
                autoOpen: false
            });
        }
        $('#ChangeMembershipSelect').html($('#Categories option').clone())
        $('#ChangeMemberModal').dialog('open');
    },
    cancelMembershipClick: function (e) {
        var memberCatID = $('#MemberCategoryID').val();
        if ($('#DeleteModal').length === 0) {
            var $dialog = $('<div id="DeleteModal"></div>');
            var $content = $('<div>Are you sure you wish to cancel this member\'s access to this category? This cannot be undone.</div>');
            $dialog.append($content);
            $dialog.dialog({
                modal: true,
                buttons: {
                    'Cancel': function () {
                        $(this).dialog('close');
                    },
                    'Yes, Cancel This Membership': function () {
                        $(this).dialog('close');
                        $("#contentEditAdd").block({ message: null });
                        categoryFunctions.cancelMembership($('#MemberCategoryID').val());
                    }
                },
                autoOpen: false
            });
        }
        $('#DeleteModal').dialog('open');
    },
    response : function (result, context) {
        if (result.Count === 1) {
            DisplayMessage("Messages", "success", result.Data);
            getCategories();
            editMember($('#ID').val(), context);
        } else if (result.Count === 2) {
            DisplayMessage("Messages", "success", result.Data);
            getCategories();
            $("#contentEditAdd").hide();
            $("#btnSave").hide();
        } else if (result.Count === undefined) {
            Display("Messages", "error", "An error occurred. Your change may not have been applied. Please check the member you were modifying");
            getCategories();
            $("#contentEditAdd").hide();
            $("#btnSave").hide();
        }
        else {
            $("#contentEditAdd").unblock();
            DisplayMessage("Messages", "warn", result.Data);
        }
    }
}
//#endregion

//#region Get Methods
function doSave() {
    if ($("#Members").is(":visible")) {
        updateMember();
    }
    else if ($("#EditOrganistion").is(":visible")) {
        updateOrganisation();
    }
    else if ($("#Departments").is(":visible")) {
        updateDepartment();
    }
}
function getRoles() {
    var response = function (result) {
        if (!result.Data || result.Data.length === 0) {
            DisplayMessage("Messages", "warn", "You currently do not have any roles to assign your members to. Please create your roles first then return to this page.");
            scrollToElement("Messages", -200, 100);
            $("#Add").hide();
            $("#Status").attr("disabled", "disabled");
        }
        else {
            rolesData = result.Data;
        }
    };

    Aurora.Manage.Service.AuroraWS.GetRolesForMembers(0, 0, "", response, onError);
}
function getMembers(isDepartment, state) {
    var response = function (result, stateLoad) {
        if (result == undefined || result.Data.length === 0) {
            $("#Members .data").hide();
            DisplayMessage("Messages", "warn", "No members were found in this organisation under this status ");
            scrollToElement("Messages", -200, 100);
            $("#Pagination").hide();

        }
        else {
            $("#Pagination").show();
            $("#Members .data").show();
            if (!$("#Departments").is(":visible")) {
                $("#Messages").hide();
            }

            if (!membersDS) {
                membersDS = $create(Sys.UI.DataView, {}, {}, {}, $get("membersgrid"));
                TotalCount = result.Count;
                initPagination();
            }

            if (stateLoad) {
                TotalCount = result.Count;
                initPagination();
            }
            //store members in a seperate var
            TotalCount = result.Count;
            currentMemberRole = [];
            //for (var entity = 0; entity < result.Data.length; entity++) {
            //    for (var memberRoles = 0; memberRoles < result.Data[entity].memberRole.length; memberRoles++) {
            //        currentMemberRole.push(result.Data[entity].memberRole[memberRoles]);
            //    }

            //}

            membersDS.set_data(result.Data);


        }
        $("#Members .data").unblock();
        $(".help").tipsy({ gravity: 'n', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    };

    $("#Members .data").block({ message: null });
    $("#contentEditAdd").hide();
    $("#btnSave").hide();
    var categoryID = $('#Categories').val() 
    if (categoryID != undefined) {
        //get filter criteria
        var filterCriteria = "";
        var filter = '';
        var status = $("#Status").val();
        if ($("#Criteria").is(":visible")) {
            filter = $("#FilterBy").val();
            filterCriteria = $("#nameSearch").val().trim();
        }
        Aurora.Manage.Service.AuroraWS.GetMembers(categoryID, CurrentPage + 1, globals.PageSize, status, filter, filterCriteria, response, onError, state);
    }
    else {
        DisplayMessage("Messages", "warn", "No Categories have been created, please click the Categories menu to create an Membership Category", 7000);
        $("#Members .data").unblock();
    }

}
function changeFilter() {
    if ($("#FilterBy").val() == "Latest") {
        $("#nameSearch").hide();
    }
    else {
        $("#nameSearch").show();
    }
}

function getCategories() {
    var response = function (result, context) {
        //Remember current Category
        currentCategory = $('#Categories').val();
        //Refresh Dropdown
        $('#Categories option').remove();
        $('#Categories').append($.render.categoryOptions(result.Data));
        //Replace Category and refresh members
        $('#Categories').val(currentCategory);
        getMembers();
    };

    Aurora.Manage.Service.AuroraWS.GetRolesForMembers(0, 0, '', response, onError);
    
}

function getChildItems(item, list, index) {
    if (!list) {
        return "";
    }
    if (!list.length) {
        list.length = 1;
    }
    for (var child = 0; child < list.length; child++) {
        item += String.format("<li class='childrenLi' ><a id='child_" + list.ID + "'  href='javascript:void(0);'  id='" + list.ID + "'>" + list.Name + "</a>{0}<li>");
    }
    return item;
}

function exportData() {
	var iFilter = $('#Status').val();
	var categoryID = $('#Categories').val();
	window.open('/MemberManagement/MembershipCSVExport.ashx?ifilter=' + iFilter + '&categoryid=' + categoryID, '_blank');
}
//#endregion

//#region Put Methods
function editMember(id, categoryID, isDelete) {
    $("#contentEditAdd").block({ message: null });
    var url = String.format("/MemberManagement/SiteMembers.aspx?MemberID={0}&CategoryID={1}&xt={2}", id, categoryID, new Date().getMilliseconds());

    if (isDelete && $("#Status").val() == "") {
        ans = confirm("Are you sure that you want to set this member to In-Active?");
        if (!ans) {
            return;
        }
        url = url + "&softdel=1";
    }
    else if (isDelete) {
        ans = confirm("Are you sure that you want to delete this member?");
        if (!ans) {
            return;
        }
        url = url + "&del=1";
    }

    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='/Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
    //callback for load
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
        	if (!isDelete) {
        		$('#tbl_CustomFields input[type=file]').parent().parent().remove();
                $("#contentEditAdd").unblock();
                $("#contentEditAdd").show();
                $("#btnSave").show();
                $("#Cancel").show();
                DisplayMessage("Messages", "info", "Please fill in all fields in the form below", 7000);
                scrollToElement("Messages", -20, 2);
                if ($("#isDefault").val() == "True") {
                    $("#Roles").hide();
                }
                //new entry
                if (id === 0) {
                    $("#tab2,#tab3").attr("disabled", "disabled");
                    DisplayMessage("Messages", "warn", "Some tabs have been disabled, they will be functional after a member has been saved");
                }
            }
            else {
                $("#contentEditAdd").hide();
                $("#btnSave").hide();
                $("#contentEditAdd").unblock();
                updateMember(id,true);
            }

        }
    };

    $("#contentEditAdd").load(url, null, success);
}
function updateMember(id, isDelete) {
    var response = function (result) {
        if (result.Result && result.Data != null) {
            DisplayMessage("Messages", "success", "Member successfully saved", 7000);
            if (result.Data.DepartmentID) {
                getMembers(true);
            }
            else {
                getMembers(false);
            }
        }
        else {
            DisplayMessage("Messages", "error", "There was an error while saving your member, please try again");
        }
        scrollToElement("Messages", -200, 100);
    };

    if (!isDelete && validateForm("MemberForm", "Please ensure all the marked fields have been filled in.", "Messages", false)) {
        scrollToElement("Messages", -200, 100);
        return;
    }
    var Member = new AuroraEntities.Member();
    var chkListRoles = [];

    for (var property in Member) {
        if ($("#MemberForm #" + property).length > 0) {
            if ($("#MemberForm #" + property).val() != "") {
                Member[property] = $("#MemberForm #" + property).val();
            }
        }
    }

    chkListRoles = [];
    $("#Roles_selectedItems").children('li').each(function (index, domEle) {
        chkListRoles.push(findByInArray(rolesData, "ID", $(this).attr("id").toString().replace("Roles_", "")));
    });

    if (chkListRoles.length === 0 && $("#isDefault").val() == "False" && !isDelete) {
        DisplayMessage("Messages", "warn", "you must assign a role(s) to a user.");
        scrollToElement("Messages", -200, 100);
        return;
    }
    Member.isActive = Member.isActive == "on" ? true : false;
    Member.InsertedOn = new Date(Member.InsertedOn);
    Member.isDefault = Member.isDefault == "True" ? true : false;
    Member.ExpiresOn = new Date(Member.ExpiresOn);
    Member.DepartmentID = Member.DepartmentID == "null" ? null : Member.DepartmentID;
    Member.OrganisationID = currentOrganisation;
    Member.isPending = $("#isPending").is(":checked");
    Member.LastUpdated = new Date();
    //save custom fields
    if (isDelete) {
        Member.DeletedBy = 0;
        Member.DeletedOn = new Date();
    }
    $("#fldegg_avail").attr("title", "egg_avail");
    Member.CustomXML = buildCustomXMLBySelector($('#MemberForm input[name^="fld"],#MemberForm select[name^="fld"],#MemberForm textarea[name^="fld"]'), "value", true);
    var additionalFields = buildCustomXMLBySelector($('#tbl_AdditionalData input[name^="fld"],#tbl_AdditionalData select[name^="fld"],#tbl_AdditionalData textarea[name^="fld"]'), "value");

    Aurora.Manage.Service.AuroraWS.UpdateMembers(Member, chkListRoles, additionalFields, response, onError);
}
function editOrganisation(id, isDelete) {
    $("#Departments").hide();
    $("#btnSave").show();
    var onSuccess = function (result, isDelete) {
        if (result.Data.length > 0) {
            for (var property in result.Data[0]) {
                $("#" + property).val(result.Data[0][property]);
            }
        }

        if (!isDelete) {
            $("#EditOrganistion").show();
        }
        else {
            updateOrganisation(isDelete);
        }
    };

    if (isDelete) {
        if (!confirm("do you wish to delete this organisation?")) {
            return;
        }
    }
    Aurora.Manage.Service.AuroraWS.GetOrganisations(id, onSuccess, onError, isDelete);
}
function updateOrganisation(isDelete) {
    var organisation = new AuroraEntities.Company();
    var clean = {};
    var onSuccess = function (result, isDelete) {
        if (isDelete) {
            DisplayMessage("Messages", "warn", "Organisation removed", 7000);
        }
        else {
            DisplayMessage("Messages", "success", "Organisation saved", 7000);
        }

    };

    for (var property in organisation) {
        $propVal = $("#EditOrganistion #" + property);
        if ($propVal.length > 0) {
            if (property.indexOf("ID") > -1) {
                organisation[property] = Number($propVal.val()) || 0;
            }
            else if (property.indexOf("InsertedOn") > -1) {
                organisation[property] = new Date();
            }
            else if (property.indexOf("InsertedBy") > -1) {
                organisation[property] = Number($propVal.val()) || 0;
            }
            else {
                organisation[property] = $propVal.val();
            }
        }

    }


    if (isDelete) {
        organisation.DeletedBy = 0;
        organisation.DeletedOn = new Date();
    }
    else {
        organisation.DeletedBy = null;
        organisation.DeletedOn = null;
    }
    Aurora.Manage.Service.AuroraWS.UpdateOrganistaion(organisation, onSuccess, onError, isDelete);
}
function editDepartment(id, isDelete) {
    $("#EditOrganistion").hide();
    var organisationData = organisationDS.get_data();
    var department = null;

    for (var company = 0; company < organisationData.length; company++) {
        if (organisationData[company].departments) {
            if (organisationData[company].departments.length === 1) {
                department = organisationData[company].departments;
            }
            else {
                department = findByInArray(organisationData[company].departments, "ID", id);
            }
        }

        if (department) {
            for (var property in department) {
                var $field = $("#Department_" + property);
                if ($field.length > 0) {
                    $field.val(department[property]);
                }
            }
            break;
        }
    }

    $("#Department_CompanyID").val(id);
    if (isDelete) {
        updateDepartment(id, isDelete);
        return;
    }
    $("#Departments").show();
    $("#EditOrganisation").hide();
    $("#btnSave").show();
}

function updateDepartment(id, isDelete) {
    var departmentEntity = new AuroraEntities.Departments();
    if (isDelete) {
        if (confirm("Do you wish to delete this department?")) {
            departmentEntity.DeletedBy = Number($("#Department_UserID").val());
            departmentEntity.DeletedOn = new Date();
        }
        else {
            return;
        }

    }
    var onSuccess = function (result, isDelete) {
        if (result.Result) {
            if (isDelete) {
                DisplayMessage("Messages", "warn", "Department removed from company.", 7000);
                $("#Departments").hide();
            }
            else {
                DisplayMessage("Messages", "success", "Department saved to company.", 7000);
            }

            getOrganisations(true);
        }
        else {
            DisplayMessage("Messages", "error", "We could not save your data, support has been notified", 7000);
        }
    };

    if (validateForm("Departments", "Please fill in all fields", "Messages")) {
        return;
    }

    departmentEntity.Name = $("#Department_Name").val();
    departmentEntity.Description = $("#Department_Description").val();
    departmentEntity.CompanyID = Number($("#Department_CompanyID").val());
    departmentEntity.ClientSiteID = Number($("#Department_ClientSiteID").val());
    departmentEntity.ID = Number($("#Department_ID").val());
    departmentEntity.InsertedBy = Number($("#Department_InsertedBy").val());
    departmentEntity.InsertedOn = new Date($("#Department_InsertedOn").val());

    if (departmentEntity.ID == 0) {
        departmentEntity.InsertedBy = Number($("#Department_UserID").val());
        departmentEntity.InsertedOn = new Date();
    }


    Aurora.Manage.Service.AuroraWS.UpdateDepartment(departmentEntity, onSuccess, onError, isDelete);
}
function showDepartments(companyID) {
    var $departmentNode = $("#dep_" + companyID);
    if ($departmentNode.length === 1) {
        if ($departmentNode.attr("open") === "true") {
            $departmentNode.hide();
            $departmentNode.attr("open", "false");
        }
        else {
            $departmentNode.attr("open", "true");
            $departmentNode.show();
        }

        return;
    }
    var departments;
    var organisationData = organisationDS.get_data();

    for (var company = 0; company < organisationData.length; company++) {
        if (organisationData[company].company.ID == companyID) {
            departments = organisationData[company].departments;
            break;
        }
    }
    if (!departments.length) {
        departments.length = 1;
    }
    for (var department = 0; department < departments.length; department++) {
        var row = $("<tr id='dep_" + companyID + "' class='child' open='true'></tr>");
        var cell1, cell2, cell3;
        var departmentID = 0;

        cell1 = $("<td></td>");
        cell2 = $("<td></td>")
        cell3 = $("<td class='departmentfunctions' align='right'></td>")
        if (departments.length == 1) {
            cell1.html(departments.Name);
            cell2.html(departments.Description);
            cell3.html($(".data td:last").html());
            departmentID = departments.ID;
        }
        else {
            cell1.html(departments[department].Name);
            cell2.html(departments[department].Description);
            cell3.html($(".data td:last").html());
            departmentID = departments[department].ID;
        }

        //perform a cleanup on functions
        cell3.children("a").first().remove();
        cell3.children("a").first().remove();

        cell3.children('a')
        .addClass("depFunctions")
        .attr("href", "javascript:void");
        //change btns functions
        cell3.children('a').first().click(function (e) { editDepartment(departmentID); return false; });
        cell3.children('a').last().click(function (e) { editDepartment(departmentID, true); return false; })

        row.append(cell1, cell2, cell3);
        row.insertAfter($("#Organisations .data tr[id='" + companyID + "']"));
    }
}
//#endregion

//#region Pagination
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getMembers();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getMembers();
    }

    $('div.content').unblock();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

//#endregion

//#region Basket 
function showBox() {
    if (client.replace("~/ClientData/","" === "10016")) {
        if ($("#memberBox").length === 0) {
            var elem = '<div id="memberBox" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-resizable">';
            elem += '<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">';
            elem += '<span id="ui-dialog-title-dialog" class="ui-dialog-title">Members</span><input type="button" onclick="createPDF();" value="Generate PDF" style="float:right;margin-right:20px;" />';
            elem += '<a class="ui-dialog-titlebar-close ui-corner-all" href="#"><span class="ui-icon ui-icon-closethick">close</span></a>';
            elem += '</div>';
            elem += '<div style="height: 200px; min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content">';
            elem += '<p><ul id="listItemsMembers"></ul></p>';
            elem += '</div>';
            elem += '</div>';


            $(elem).appendTo(".inner");
        }
    }
    
}
function killBox() {
    $(".selectBox:checked").removeAttr("checked");
    $("#memberBox").remove();
}
function createPDF() {
    $("#memberBox").block({ message: "PDF generating, Please wait.." });
    var response = function (result) {
        mypopup = window.open("http://" + document.domain + "/" + result.Data);
        if (mypopup) {
            killBox();
        }
        else {
            alert("A popup was blocked from your browser. Please allow pop-ups for this site so you may obtain your file.");
        }
        $('#memberBox').unblock();
    };

    Aurora.Manage.Service.AuroraWS.PlaceEnquiry($("#memberBox li").map(function () { return $(this).attr("memid"); }).toArray(), response, onError);
}
//#endregion

$(function () {
    //Initialise JsRender Templates
    $.templates({
        categoryOptions: "<option value='{{attr:Role.ID}}'>{{>Role.Name}} ({{>ActiveCount}})</option>"
    });

    $("#btnSave").hide();
    getCategories();
    //getRoles();

    //Bind events
    $('#contentEditAdd').delegate('div.categoryButton.activate', 'click.SiteMembers', categoryEvents.activateMembershipClick);
    $('#contentEditAdd').delegate('div.categoryButton.extend', 'click.SiteMembers', categoryEvents.extendMembershipClick);
    $('#contentEditAdd').delegate('div.categoryButton.change', 'click.SiteMembers', categoryEvents.changeMembershipClick);
    $('#contentEditAdd').delegate('div.categoryButton.cancel', 'click.SiteMembers', categoryEvents.cancelMembershipClick);
    $('#contentEditAdd').delegate('div.categoryButton.pay', 'click.SiteMembers', categoryEvents.payMembershipClick);
    
    
}); 