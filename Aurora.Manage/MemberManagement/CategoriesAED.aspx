﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="CategoriesAED.aspx.cs" Inherits="Aurora.Manage.MemberMangement.Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="CategoriesAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"
        type="text/javascript"></script>
    <div>
        <div class="onecolumn">
            <div class="header">
                <span>Categories</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <div>
                        &nbsp;
                      <img src="/Styles/images/add.png" onclick="editRoles(0);" style="cursor: pointer;"
                          title="Create a Role" class="help" />
                    </div>
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <thead>
                        <tr>
                            <th>
                                Category Name
                            </th>
                            <th>
                                Amount Of Users linked
                            </th>
                            <th align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody class="sys-template" id="rolesgrid">
                        <tr>
                            <td>
                                {{Role.Name}}
                            </td>
                            <td style="text-align:center">
                                {{ActiveCount}}
                            </td>
                            <td align="right">
                                <a href="javascript:void(0);" dataid="{{Role.ID}}" onclick="editRoles($(this).attr('dataid'));return false;"
                                    dataid="{{Role.ID}}">
                                    <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                                <a href="javascript:void(0);" dataid="{{Role.ID}}" onclick="editRoles($(this).attr('dataid'),true);return false;"
                                    dataid="{{Role.ID}}">
                                    <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="Messages">
        </div>
        <div id="contentEditAdd">
        </div>
        <div style="float:right;">
            <input id="btnSave" onclick="updateRoles();" type="button" value="Save" />
        </div>
    </div>
</asp:Content>
