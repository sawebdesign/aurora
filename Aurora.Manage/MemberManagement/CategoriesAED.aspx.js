﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />

//#region Vars
var rolesDS = null;
//#endregion

$(function () {
    $("#btnSave").hide();
    getRoles();
});

var responseFunctions = {
    deleteRoleResponse : function (result) {
        if (result.Data === true) {
            DisplayMessage("Messages", "warn", "Category has been removed", 7000);
            $("#btnSave").hide();
            $("#contentEditAdd").hide();
        } else {
            DisplayMessage("Messages", "warn", "The category could not be removed. There may be active members assigned to it.");
        }

        getRoles();
    }
}

//#region Get Methods 
function getRoles() {
    $(".data").block({ message: null });
    var response = function (result) {
        if (!result.Data || result.Data.length == 0) {
            DisplayMessage("Messages", "warn", "You currently do not have any roles please click the green add button to start creating your roles.");
        }
        else {
            if (!rolesDS) {
                rolesDS = $create(Sys.UI.DataView, {}, {}, {}, $get("rolesgrid"));
            }

            rolesDS.set_data(result.Data);
        }
        $(".data").unblock();
    };

    Aurora.Manage.Service.AuroraWS.GetRolesForMembers(0, 0, "", response, onError);
}

function editRoles(id, isDelete) {
  
    $("#contentEditAdd").block({ message: null });
    var url = String.format("/MemberManagement/Categories.aspx?RoleID={0}&xt={1}", id, new Date().getMilliseconds());

    if (isDelete) {
        ans = confirm("Are you sure that you want to delete this role?");
        if (!ans) {
            return;
        } else {
            Aurora.Manage.Service.AuroraWS.DeleteRole(id, responseFunctions.deleteRoleResponse, onError);
            return;
        }
        url = url + "&del=1";
    }

    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);
    //callback for load
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            if (!isDelete) {
                $("#contentEditAdd").unblock();
                $("#contentEditAdd").show();
                $("#btnSave").show();
                $("#Cancel").show();
                DisplayMessage("Messages", "info", "Please fill in all fields in the form below", 7000);
            }
            else {
                $("#contentEditAdd").hide();
                $("#btnSave").hide();
                $("#contentEditAdd").unblock();
                updateRoles();
            }

        }
    };

    $("#contentEditAdd").load(url, null, success);
}
//#endregion

//#region Put Methods 
function updateRoles() {
    var response = function (result) {
       
        if (result.Result) {
            if ($("#DeletedOn").val() == "") {
                DisplayMessage("Messages", "success", "Category saved", 7000);
                $("#contentEditAdd").hide();
                $("#btnSave").hide();
                $("#contentEditAdd").unblock();
            }
            else {
                DisplayMessage("Messages", "warn", "Category has been removed", 7000);
                $("#btnSave").hide();
            }

            getRoles();
        }
        else {
            DisplayMessage("Messages", "error", "We were unable to save your Category. Please try again, if the problem persists please contact support");
        }
    };

    var roleEntity = new AuroraEntities.Role();
    //validate
    if (validateForm("RolesForm", true, "Messages", false)) {
        DisplayMessage("Messages","warn","Please fill in the marked fields below",7000);
        return;
    }

    $('.content').mapToObject(roleEntity);
    

    Aurora.Manage.Service.AuroraWS.UpdateRoles(roleEntity, response, onError);
}
//#endregion