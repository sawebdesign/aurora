﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.MemberMangement {
    public partial class SiteMembers1 : System.Web.UI.Page {
        private Member MemberInfo = new Member();
        protected void Page_Load(object sender, EventArgs e) {
            int MemberID;
            int categoryID;
            int.TryParse(Request["MemberID"], out MemberID);
            int.TryParse(Request["CategoryID"], out categoryID);
            FileViewer1.CurrentPath = String.Format("Members/{0}",MemberID);
            FileViewer1.fileID = MemberID.ToString();
			FileViewer1.primary = true;
            ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();
            XmlDocument XmlData = new XmlDocument(); //The xml data
            XmlDocument SchemaXml = new XmlDocument(); //the schema for the extension
            if (MemberID == 0) {
                ID.Value = "0";
                InsertedBy.Value = SessionManager.UserID.ToString();
                InsertedOn.Value = DateTime.Now.ToString();

            }
            else {
                Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();

                MemberInfo = (from Members in entities.Member
                              where Members.ID == MemberID
                              select Members).SingleOrDefault();
                var CategoryInfo = (from mr in entities.MemberRole
                                    join cat in entities.Role on mr.RoleID equals cat.ID
                                    where mr.ID == categoryID
                                    select new {
                                        MemberCategory = mr,
                                        Category = cat
                                    }).SingleOrDefault();

                if (MemberInfo != null && CategoryInfo != null) {
                    FirstName.Value = MemberInfo.FirstName;
                    LastName.Value = MemberInfo.LastName;
                    Email.Value = MemberInfo.Email;
                    Mobile.Value = MemberInfo.Mobile;
					PrimaryImageURL.Value = MemberInfo.PrimaryImageURL;
                    //Password.Value = MemberInfo.Password;
                    //ConfirmPassword.Value = MemberInfo.Password;
                    InsertedBy.Value = MemberInfo.InsertedBy.Value.ToString();
                    InsertedOn.Value = MemberInfo.InsertedOn.ToString();
                    ID.Value = MemberInfo.ID.ToString();
                    CustomXML.Value = Utils.ConvertDBNull(MemberInfo.CustomXML, string.Empty);
                    LastUpdated.Value = MemberInfo.LastUpdated.ToString();

                    CategoryID.Value = CategoryInfo.Category.ID.ToString();
                    MemberCategoryID.Value = categoryID.ToString();
                    CategoryNamelbl.InnerHtml = CategoryInfo.Category.Name;
                    IsActivated.Checked = CategoryInfo.MemberCategory.IsActivated ?? false;
                    if (CategoryInfo.MemberCategory.CommencedOn == null) {
                        CommencedOn.InnerHtml = "Membership has not been Activated";
                    } else {
                        activatebtn.Visible = false;
                        CommencedOn.InnerHtml = String.Format("{0:d}", CategoryInfo.MemberCategory.CommencedOn);
                    }
                    if (CategoryInfo.Category.Cost > 0) {
                        if (CategoryInfo.MemberCategory.PaidOn == null) {
                            PaidOnlbl.InnerHtml = "Never";
                            if (CategoryInfo.Category.Cost > 0) {
                                activatebtn.Visible = false;
                            }
                        } else {
                            paybtn.Visible = false;
                            PaidOnlbl.InnerHtml = String.Format("{0:d}", CategoryInfo.MemberCategory.PaidOn);
                        }
                    } else {
                        PaidOnlbl.InnerHtml = "Free Membership";
                        paybtn.Visible = false;
                    }
                    if (CategoryInfo.MemberCategory.ExpiresOn == null) {
                        ExpiresOnlbl.InnerHtml = "Never";
                    } else {
                        ExpiresOnlbl.InnerHtml = String.Format("{0:d}", CategoryInfo.MemberCategory.ExpiresOn);
                    }
                    if (CategoryInfo.MemberCategory.LapsedOn == null) {
                        MembershipCancelledlbl.InnerHtml = "Never";
                    } else {
                        activatebtn.Visible = false;
                        extendbtn.Visible = false;
                        cancelbtn.Visible = false;
                        paybtn.Visible = false;
                        MembershipCancelledlbl.InnerHtml = String.Format("{0:d}", CategoryInfo.MemberCategory.LapsedOn);
                    }
                    

                    var schema = (from schemas in entities.Schema
                                  join featuresModule in entities.Module on schemas.ModuleID equals featuresModule.ID
                                  where featuresModule.Name == "MemberManagement"
                                        && schemas.ClientSiteID == SessionManager.ClientSiteID
                                  select schemas.SchemaXML).SingleOrDefault();

                    if (!string.IsNullOrEmpty(schema) && XDocument.Parse(schema).Descendants("field").Count() > 0) {

                        SchemaXml.LoadXml(schema);
                        if (!String.IsNullOrEmpty(MemberInfo.CustomXML)) {
                            XmlData.LoadXml(MemberInfo.CustomXML);
                        }

                        //Load the schema and data
                        ContentXml.XmlPanelSchema = SchemaXml;
                        ContentXml.XmlPanelData = XmlData;

                        //Render the control and add it to the Form
                        ContentXml.RenderXmlControls("tbl_CustomFields");
                        ContentPanel.Visible = true;

                        string path = Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID,
                                         "Members", MemberInfo.ID));

                        if (Directory.Exists(path)) {
                            if (Directory.GetFiles(path).Count() > 0) {
                                fileLink.Visible = true;
                                fileLink.InnerHtml = "View Files(" + Directory.GetFiles(path).Count() + ")";
                                fileLink.HRef = String.Format("http://"+Request.ServerVariables["SERVER_NAME"]+"/Innova/assetmanager/assetmanager.asp?ClientID={0}&ffilter=&inpCurrFolder={1}", SessionManager.ClientSiteID, Server.UrlEncode(path));
                            }
                        }

                        ContentXml.UploaderEventHandler += UploadFile;
                    }
                }

                if (Request["del"] != null) {
                    DeletedBy.Value = SessionManager.UserID.ToString();
                    DeletedOn.Value = DateTime.Now.ToString();
                }
                else if (Request["softdel"] != null) {
                    DeletedBy.Value = "0";
                    DeletedOn.Value = DateTime.Now.ToString();
                }

				/*
				 * It would appear this code was written to facilitate Baby2Mom's additional data. It is causing problems with the new
				 * membership system, so I am disabling it until it is needed. - Garth
				 * 
                //Check if the members module has been linked to custom modules
                long? moduleID =
                    (from modules in entities.Module
                     where modules.Name == "MemberManagement"
                     select modules.ID).FirstOrDefault();

                var additionalData = (from customData in entities.CustomModules
                                      join customModuleData in entities.CustomModuleData on customData.ID equals customModuleData.ModuleID
                                      into leftJoin
                                      from customModuleData in leftJoin.Where(ctd => ctd.SystemModuleID == MemberInfo.ID).DefaultIfEmpty()
                                      where customData.FeaturesModuleID == moduleID 
                                      && customData.ClientSiteID == SessionManager.ClientSiteID
                                      select new { customModuleData, customData }).SingleOrDefault();

                if (additionalData != null) {
                    ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();
                    XmlDocument XmlAdditionalData = new XmlDocument(); //The xml data
                    XmlDocument SchemaXmlAdditional = new XmlDocument(); //the schema for the extension

                    if (additionalData.customModuleData != null)
                    {
                        XmlAdditionalData.LoadXml(additionalData.customModuleData.Data ?? "<XmlData></XmlData>");
                        SchemaXmlAdditional.LoadXml(additionalData.customModuleData.DataSchemaXML ?? additionalData.customData.XMLSchema);
                    }
                    else
                    {
                        SchemaXmlAdditional.LoadXml(additionalData.customData.XMLSchema);
                    }
                    

                    //Load the schema and data
                    XmlPanelAdditional.XmlPanelSchema = SchemaXmlAdditional;
                    XmlPanelAdditional.XmlPanelData = XmlAdditionalData;

                    //Render the control and add it to the Form
                    XmlPanelAdditional.RenderXmlControls("tbl_AdditionalData");
                    ObjectPanelAdditional.Visible = true;

                }*/
            }
        }
        protected void UploadFile(object sender, EventArgs e) {
            int instances = 0;
            System.Web.UI.WebControls.FileUpload myUploader = (System.Web.UI.WebControls.FileUpload)sender;

            if (myUploader.HasFile) {


                if (!Directory.Exists(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, "Members")))) {
                    Directory.CreateDirectory(Server.MapPath(String.Format("~/ClientData/{0}/Uploads", SessionManager.ClientSiteID)) + "/Members");
                }

                if (Directory.Exists(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID, "Members", MemberInfo.ID)))) {
                    instances =
                        Directory.GetFiles(
                            Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID, "Members", MemberInfo.ID))).Count();
                }
                else {
                    Directory.CreateDirectory(
                        Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID,
                                                     "Members")) + "/" + MemberInfo.ID);
                }

                myUploader.SaveAs(Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/file_{2}_{4}{3}", SessionManager.ClientSiteID, "Members", MemberInfo.ID, myUploader.PostedFile.FileName.Substring(myUploader.PostedFile.FileName.IndexOf(".")), instances + 1)));
            }
        }
    }
}