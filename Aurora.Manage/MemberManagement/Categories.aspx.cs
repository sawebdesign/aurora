﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage.MemberMangement
{
    public partial class Roles1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int RoleID;
            int.TryParse(Request["RoleID"], out RoleID);
            ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();

            if (RoleID == 0)
            {
                ID.Value = "0";
                InsertedBy.Value = SessionManager.UserID.ToString();
                InsertedOn.Value = DateTime.Now.ToString();

            }
            else
            {
                Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
				bool supportsRecurring = ((from pgcs in entities.PaymentSchemaClientSite
										   join pg in entities.PaymentSchema on pgcs.PaymentSchemaID equals pg.ID
										   where pg.SupportsRecurring == true && pgcs.ClientSiteID == SessionManager.ClientSiteID
										   select pgcs.ID).Count() > 0);
                var roleInfo = (from roles in entities.Role
                                where roles.ID == RoleID
                                select roles).SingleOrDefault();

                if (roleInfo != null)
                {
                    Name.Value = roleInfo.Name;
                    RoleDuration.Value = roleInfo.RoleDuration.ToString();
                    RenewalAllowedAt.Value = roleInfo.RenewalAllowedAt.ToString();
                    Cost.Value = String.Format("{0: 0.00}",roleInfo.Cost);
                    IsPublic.Checked = roleInfo.IsPublic ?? false;
                    DefaultToPending.Checked = roleInfo.DefaultToPending ?? false;
                    OrderIndex.Value = roleInfo.OrderIndex.ToString();
					LandingPageURL.Value = roleInfo.LandingPageURL;
                    InsertedBy.Value = roleInfo.InsertedBy.Value.ToString();
                    InsertedOn.Value = roleInfo.InsertedOn.ToString();
                    ID.Value = roleInfo.ID.ToString();
					if (supportsRecurring) {
						IsRecurring.Checked = roleInfo.IsRecurring ?? false;
					} else {
						RecurringBilling.Visible = false;
					}

                    if (roleInfo.CustomXML != null) {
                        CustomXML.Value = roleInfo.CustomXML.ToString();
                    }
                }

                if (Request["del"] != null)
                {
                    DeletedBy.Value = SessionManager.UserID.ToString();
                    DeletedOn.Value = DateTime.Now.ToString();
                }
            }

        }
    }
}