﻿using Aurora.Custom;
using Aurora.Custom.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Configuration;


namespace Aurora.Manage.MemberManagement {
	/// <summary>
	/// Summary description for MembershipCSVExport
	/// </summary>
	public class MembershipCSVExport : IHttpHandler, IReadOnlySessionState {

		public void ProcessRequest(HttpContext context) {
			try {
				string iFilter = context.Request.QueryString["ifilter"];
				long categoryID;
				if (long.TryParse(context.Request.QueryString["categoryid"], out categoryID)) {

					using (AuroraEntities service = new AuroraEntities()) {
						var message = service.GetMemberCSVExport(iFilter, categoryID, SessionManager.ClientSiteID);
						context.Response.AddHeader("Content-Disposition", "attachment;filename=" + DateTime.Now.ToShortDateString() + "-" + iFilter + ".csv");
						context.Response.ContentType = "text/csv";
						context.Response.Write(@message.FirstOrDefault());
					}
				} else {
					context.Response.Redirect("~/errors/_errors/503.html");
				}
				
			} catch (Exception) {
				context.Response.Redirect("~/errors/_errors/404.html");
			}
		}

		public bool IsReusable {
			get {
				return false;
			}
		}
	}
}