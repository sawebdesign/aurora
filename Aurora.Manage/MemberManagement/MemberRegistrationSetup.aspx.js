﻿/// <reference path="/scripts/jquery-1.5.2.js" />
/// <reference path="/scripts/microsoftajax.js" />
/// <reference path="/scripts/microsoftajaxtemplates.debug.js" />
/// <reference path="/scripts/utils.js" />
/// <reference path="/service/auroraws.asmx/js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />


var currentData = [];
var ds = null;
var checkInit = false;
var paymentGatewaysDS = null;
var paymentSettingDS = null;

$(function () {
    LoadPricePlans();
    getPaymentSchemas();
    $("#Price").ForceNumericOnly();
    $("#Expiration").ForceNumericOnly();
});

function paymentOnOff(eTarget) {
    if (eTarget) {
        if ($(eTarget).is(":checked")) {
            $("#payments").show();
        }
        else {
            $("#payments").hide();
        }
    }
}

function LoadPricePlans() {
    var response = function (result) {
        if (result.Data.length === 0) {
            DisplayMessage("Messages", "warn", "No Price plans have been created", 7000);
        }
        else {
            if (!ds) {
                ds = $create(Sys.UI.DataView, {}, {}, {}, $get("grid"));
            }
            ds.set_data(result.Data);
        }
    };
    Aurora.Manage.Service.AuroraWS.GetMembershipSettings(response, onError);
}

function editPlan(id, isDelete) {
    var item = ds != null ? findByInArray(ds._data, "ID", id) : null;

    if (!checkInit) {
        $('#MainContent_Roles').toChecklist({
            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: true,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });
        $('#MainContent_Gateways').toChecklist({
            /**** Available settings, listed with default values. ****/
            addScrollBar: true,
            addSearchBox: true,
            searchBoxText: 'Type here to search list...',
            showCheckboxes: true,
            showSelectedItems: true,
            submitDataAsArray: true // This one allows compatibility with languages that use arrays

        });

        checkInit = true;

    }
    getCurrentSettingPlans(id);
    $("input[@name='MainContent_Roles[]']:checked").removeAttr("checked");
    $("input[@name='MainContent_Roles[]']:checked").parent().removeClass("checked");


    if (item) {
        currentData = item;
        $("#MainContent_Gateways").val(currentData.PaymentSchemaID);
        for (var property in item) {
            var control = $("#" + property);
            if (control.length > 0) {
                control.val(item[property])
            }
        }
        if (currentData.Roles) {
            var select = currentData.Roles.split(",");
            for (var ele = 0; ele < select.length; ele++) {
                $("MainContent_Roles_" + select[ele]).attr("checked", "checked");
            }
        }
        if (currentData.Roles) {
            var roles = currentData.Roles.split(",")

            $(":input[name='MainContent_Roles[]'][type='checkbox']").each(
                function (indx, Elem) {
                    for (var index = 0; index < roles.length; index++) {
                        if (roles[index] == Elem.value) {
                            var $elem = $(Elem);
                            $elem.attr("checked", "checked");
                            $elem.parent().addClass("checked");
                        }
                    }
                }
            );
        }

    }
    else {
        $("#ID").val(0);
        $("#Edit input[type='text']").val("");
        $("input[@name='MainContent_Roles[]']:checked").removeAttr("checked");

    }
    if (!isDelete) {
        $("#Edit").show();
        $("#btnSave").show();
    }
    else {
        updatePlan(true);
    }


}

function updatePlan(isDelete) {
    var response = function (result, isDelete) {
        if (isDelete) {
            DisplayMessage("Messages", "warn", "Plan Settings removed.", 7000);
            LoadPricePlans();
            return;
        }
        if (!result.ErrorMessage) {
            DisplayMessage("Messages", "success", "Plan Settings saved.", 7000);
            LoadPricePlans();
        }
        else {
            DisplayMessage("Messages", "error", "There was a problem whislt saving your plans settings, please try again if the problem persists contact support");
        }
    };

    if (isDelete) {
        if (!confirm("Are you sure you want to delete this item?")) {
            return;
        }
    }
    else {
        isDelete = false;
    }

    var entity = new AuroraEntities.MembershipSettings();
    for (var property in entity) {
        var control = $("#" + property);
        if (control.length > 0) {
            entity[property] = control.val();
        }
    }
    entity.Roles = "";
    $("#MainContent_Roles input[@name='MainContent_Roles[]']:checked").each(
        function (indx, Elem) {
            entity.Roles += Elem.value + ",";
        }
    );
    entity.Roles.substr(0, entity.Roles.length - 1);
    entity.PaymentSchemaID = $("#MainContent_Gateways").val();

    var paymentPlans = [];
    $("#MainContent_Gateways input").each(
        function (indx, Elem) {
            var item = findByInArray(paymentGatewaysDS, "ID", Elem.value);
            if (item) {
                var payment = new AuroraEntities.MemberSettingsPaymentSchema();
                payment.MemberSettingsID = entity.ID;
                payment.ClientSiteID = Number(client.replace("~/ClientData/", ""));
                payment.PaymentSchemaID = item.ID;
                payment.InsertedOn = item.InsertedOn;
                payment.InsertedBy = item.InsertedBy;
                if (!$(Elem).is(":checked")) {
                    payment.DeletedBy = 0;
                    payment.DeletedOn = new Date();
                }

                paymentPlans.push(payment);
            }
        }
    );
    Aurora.Manage.Service.AuroraWS.UpdateMemberSettings(entity, paymentPlans, isDelete, response, onError, isDelete);
}

function getPaymentSchemas() {
    var response = function (result) {
        if (result.Result) {
            paymentGatewaysDS = result.Data;
           
        }

    };

    Aurora.Manage.Service.AuroraWS.GetPaymentPlans(0, response, onError);
}

function getCurrentSettingPlans(id) {
    var response = function (result) {
        if (result.Result) {
            paymentSettingDS = result.Data;
            $("#MainContent_Gateways input").each(
                function (indx, Elem) {
                    var item = findByInArray(paymentSettingDS, "ID", Elem.value);
                    var $control = $(Elem);
                    if (item && !$control.is(":checked")) {
                        $control.parent().click();
                    }
                    else if (!item && $control.is(":checked")) {
                        $control.parent().click();
                    }
                }
            );
        }

    };

    Aurora.Manage.Service.AuroraWS.GetPaymentPlans(id, response, onError);
}