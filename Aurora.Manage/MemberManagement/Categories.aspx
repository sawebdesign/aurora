﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="Aurora.Manage.MemberMangement.Roles1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Categories.aspx.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span id="roleName">Editing : </span>
        </div>
        <br class="clear" />
        <div class="content" id="RolesForm">
            <input type="hidden" runat="server" id="ID" />
            <input type="hidden" runat="server" id="InsertedOn" />
            <input type="hidden" runat="server" id="InsertedBy" />
            <input type="hidden" runat="server" id="DeletedOn" />
            <input type="hidden" runat="server" id="DeletedBy" />
            <input type="hidden" runat="server" id="ClientSiteID" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <input type="text" id="Name" valtype="required" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duration (in days, 30 days in a month):
                        </td>
                        <td>
                            <input type="text" id="RoleDuration" valtype="required" runat="server" mytitle="Set this to zero if you do not want memberships on this category to expire" value="0"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Allow Renewal When this many days remain (days):
                        </td>
                        <td>
                            <input type="text" id="RenewalAllowedAt" valtype="required" runat="server" mytitle="Set this to -1 if you do not want memberships on this category to be able to renew " value="0"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cost:
                        </td>
                        <td>
                            <input type="text" id="Cost" valtype="required" runat="server" mytitle="Set this to zero if you want this category to be free" value="0.00"/>
                        </td>
                    </tr>
                    <tr runat="server" id="RecurringBilling">
                        <td>
                            Monthly Billing:
                        </td>
                        <td>
                            <input type="checkbox" id="IsRecurring" runat="server" mytitle="Do you want to use monthly billing for this category?" checked="checked"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Publicly Visible:
                        </td>
                        <td>
                            <input type="checkbox" id="IsPublic" runat="server" mytitle="Is this category available for signup by the public?" checked="checked"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Members Require Activation:
                        </td>
                        <td>
                            <input type="checkbox" id="DefaultToPending" runat="server" mytitle="Do members on this category require activation before being allowed to access the system?"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order:
                        </td>
                        <td>
                            <input type="text" id="OrderIndex" valtype="required" runat="server"  value="0"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category Landing Page URL:
                        </td>
                        <td>
                            <input type="text" id="LandingPageURL" mytitle="Defaults to the Member Content page if not specified" runat="server"  value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CustomXML:
                        </td>
                        <td>
                            <input type="text" id="CustomXML" mytitle="Advanced XML for custom use" runat="server" value=""/>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
        
    </form>
</body>
</html>
