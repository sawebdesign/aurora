﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="MemberRegistrationSetup.aspx.cs" Inherits="Aurora.Manage.MemberRegistrationSetup" %>
<%@ Import Namespace="Aurora.Custom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="MemberRegistrationSetup.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"
        type="text/javascript"></script>
    <script src="../Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <div class="onecolumn">
        <div class="header">
                <span>Current Price Plans</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                        <img src="/Styles/images/add.png" dataid="0" class="help" alt="Create Plan" mytitle="Create Plan"
                            style="cursor: pointer" onclick="editPlan($(this).attr('dataid'));" />
                    </div>
        </div>
        <br class="clear" />
        <div class="content">
            <table class="data" border="0" cellpadding="0" cellspacing="0" width="100%" >
                <thead>
                    <tr>
                        <th align="left">
                            Name
                        </th>
                        <th align="left">
                            Price
                        </th>
                        <th align="right">
                            Functions
                        </th>
                    </tr></thead>
                    <tbody class="sys-template" id="grid">
                    <tr>
                        <td>{{Name}}</td>
                        <td>{{Price}}</td>
                          <td align="right" width="10%">
                            <a href="javascript:void(0);"  dataid="{{ClientSiteID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" dataid="{{ID}}"
                                    onclick="editPlan($(this).attr('dataid'));return false;" /></a>
                            <a href="javascript:void(0);" dataid="{{ID}}" onclick="editPlan($(this).attr('dataid'),true);return false;">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete"  /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="Messages"></div>
    <div class="onecolumn" style="display:none;" id="Edit">
        <div class="header">
            <span>Member Registration Setup</span>
        </div>
        <br class="clear" />
        <div class="content" >
            <input type="hidden" id="ID" />
            <input type="hidden" id="ClientSiteID" value="<%=SessionManager.ClientSiteID%>"
            <input type="hidden" id="CustomXML" />
            <table border="0" cellpadding="0" cellspacing="5" width="100%" id="payments">
                <tr>
                    <td width="20%" valign="top">
                        Payment Gateway:
                    </td>
                    <td>
                        <div style="float: left;">
                        <select id="Gateways" multiple="true" runat="server" style="height: 200px; width: 300px">
                        </select>
                        </div>
                        <div style="float: left; display: block;">
                            <ul id="Gateways_selectedItems">
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Roles:
                    </td>
                    <td>
                        <div style="float: left;">
                            <select id="Roles" name="Roles" title="Roles" style="height: 200px; width: 300px"
                                multiple="true" runat="server">
                              
                            </select></div>
                        <div style="float: left; display: block;">
                            <ul id="Roles_selectedItems">
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr style="display:none">
                    <td>EFT Information:</td>
                    <td><textarea id="EFT" cols="50" rows="10"></textarea></td>
                </tr>
                <tr>
                    <td>
                        <h2>
                            License Type:</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        License Name:
                    </td>
                    <td>
                        <input type="text" id="Name" /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        License Price:
                    </td>
                    <td>
                        <input type="text" id="Price" />
                    </td>
                </tr>
                <tr>
                    <td>License Period(Days):</td>
                    <td> <input type="text" id="Expiration" /></td>
                </tr>
                <tr>
                    <td>
                        Amount of Users per license:
                    </td>
                    <td>
                        <input type="text" valtype="required" id="UsersPerLicense" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <p style="text-align: right">
        <input type="button" style="display: none;" onclick="updatePlan();" value="Save" id="btnSave" />
    </p>
</asp:Content>
