﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SiteMembers.aspx.cs" Inherits="Aurora.Manage.MemberMangement.SiteMembers1" %>

<%@ Register Src="../UserControls/CustomFieldEditor.ascx" TagName="CustomFieldEditor"
    TagPrefix="uc1" %>
<%@ Register Assembly="Aurora.Custom" Namespace="Aurora.Custom.UI" TagPrefix="cui" %>
<%@ Register Src="../UserControls/FileViewer.ascx" TagName="FileViewer" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <script src="/Scripts/custom_black.js" type="text/javascript"></script>
    <script src="SiteMembers.aspx.js" type="text/javascript"></script>
    <style>
        #tbl_CustomFields, #tbl_AdditionalData
        {
            width: 100%;
        }
        #tbl_CustomFields td, #tbl_AdditionalData td
        {
            text-align: left !important;
        }
        #fileLink
        {
            display:none;
        }

        #CategoryInfo {
            position: absolute;
            top: 50px;
            right: 8px;
            border: #ddd 1px solid;
            padding: 8px;
            width: 250px;
        }

        .categoryButton {
            height: 32px; 
            width: 32px; 
            float: left; 
            margin: 8px;
            cursor: pointer;
        }

            .categoryButton.activate {
                background: url('/styles/images/Button Check.png');
            }
            .categoryButton.extend {
                background: url('/styles/images/Button Load.png');
            }
            .categoryButton.change {
                background: url('/styles/images/Button Color Circle.png');
            }
            .categoryButton.cancel {
                background: url('/styles/images/Button Stop.png');
            }
            .categoryButton.pay {
                background: url('/styles/images/Smiley Happy.png');
            }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Edit</span>
            <div style="width: auto;" class="switch">
                <table cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td>
                                <input type="button" style="width: 90px;" value="Details" class="left_switch active"
                                    name="tab1" id="tab1">
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Files" class="middle_switch"
                                    name="tab2" id="tab2">
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Additional Info" class="right_switch"
                                    name="tab3" id="tab3">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br class="clear" />
        <div class="tab_content" id="tab1_content">
            <div id="MemberForm" style="min-height: 300px">
                <input type="hidden" id="ID" runat="server" />
                <input type="hidden" id="InsertedOn" runat="server" />
                <input type="hidden" id="InsertedBy" runat="server" />
                <input type="hidden" id="DeletedOn" runat="server" />
                <input type="hidden" id="DeletedBy" runat="server" />
                <input type="hidden" id="ClientSiteID" runat="server" />
                <input type="hidden" name="LastUpdated" value="" id="LastUpdated" runat="server" />
                <input type="hidden" id="CustomXML" runat="server" />
                <input type="hidden" id="PrimaryImageURL" runat="server" />
                <table cellpadding="5" cellspacing="5" border="0" width="75%" style="margin-left: 0;">
                    <tr>
                        <td>
                            First Name:
                        </td>
                        <td>
                            <input type="text" runat="server" valtype="required" id="FirstName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <input type="text" runat="server" valtype="required" id="LastName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            <input type="text" runat="server" valtype="required:regex:email" id="Email" />
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td>
                            Mobile:
                        </td>
                        <td>
                            <input type="text" runat="server" id="Mobile" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Custom Fields
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a runat="server" id="fileLink">View Files</a>
                        </td>
                    </tr>
                    <tr id="CustomData">
                        <td colspan="2">
                            <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                <cui:XmlPanel ID="ContentXml" runat="server">
                                </cui:XmlPanel>
                            </cui:ObjectPanel>
                        </td>
                    </tr>
                </table>
                <div id="CategoryInfo" runat="server">
                    <label id="CategoryNamelbl" runat="server"></label>
                    <input type="hidden" id="CategoryID" runat="server" />
                    <input type="hidden" id="MemberCategoryID" runat="server" />
                    <table width="100%">
                        <tr>
                            <td>
                                Is Activated?
                            </td>
                            <td>
                                <input type="checkbox" id="IsActivated" runat="server" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid On
                            </td>
                            <td>
                                <label id="PaidOnlbl" runat="server"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Started On
                            </td>
                            <td>
                                <label id="CommencedOn" runat="server"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Expires On
                            </td>
                            <td>
                                <label id="ExpiresOnlbl" runat="server"></label>
                            </td>
                        </tr>
                        <tr id="MembershipCancelled" runat="server">
                            <td>Membership Cancelled On</td>
                            <td><label id="MembershipCancelledlbl" runat="server"></label></td>
                        </tr>
                    </table>
                    <div id="paybtn" class="categoryButton pay"  title="Mark as Paid" runat="server"></div>
                    <div id="activatebtn" class="categoryButton activate" title="Activate Membership" runat="server"></div>
                    <div id="extendbtn" class="categoryButton extend" title="Extend Membership" runat="server"></div>
                    <div id="changebtn" class="categoryButton change" title="Change Category" runat="server"></div>
                    <div id="cancelbtn" class="categoryButton cancel" title="Cancel Membership" runat="server"></div>
                </div>
            </div>
        </div>
        <div class="tab_content hide" id="tab2_content">
            <uc2:FileViewer ID="FileViewer1" runat="server" />
        </div>
        <div class="tab_content hide" id="tab3_content">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <cui:ObjectPanel ID="ObjectPanelAdditional" Visible="false" runat="server">
                            <cui:XmlPanel ID="XmlPanelAdditional" runat="server">
                            </cui:XmlPanel>
                        </cui:ObjectPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
