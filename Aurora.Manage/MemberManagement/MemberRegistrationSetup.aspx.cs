﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage
{
    public partial class MemberRegistrationSetup : System.Web.UI.Page{
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionManager.VistorSecureSessionID == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            var roles = from userRoles in entities.Role
                        where userRoles.ClientSiteID == SessionManager.ClientSiteID
                        select userRoles;

            Roles.DataSource = roles;
            Roles.DataTextField = "Name";
            Roles.DataValueField = "ID";
            Roles.DataBind();

            
            var paymentGateways = from payment in entities.PaymentSchema
                                  select payment;
            Gateways.DataSource = paymentGateways;
            Gateways.DataTextField = "Name";
            Gateways.DataValueField = "ID";
            Gateways.DataBind();

        }
    }
}