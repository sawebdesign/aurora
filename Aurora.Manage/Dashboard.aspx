﻿<%@ Page Language="C#" Title="Home" AutoEventWireup="true" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    CodeBehind="Dashboard.aspx.cs" Inherits="Aurora.Manage.Dashboard" %>
<%@ Import Namespace="Aurora.Custom" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript" src="dashboard.aspx.js"></script>
    <div class="content">
        <div class="twocolumn">
            <div class="column_left">
                <div class="header">
                    <span>My Information</span>
                </div>
                    <div id="myInfo" class="content">
                        The below is a list of sites that you can edit from your profile. Click on the user icon and you will then be able to edit the content of that site.<br />
						<div style="margin:10px 0;"><input id="siteSearchInput" name="siteSearch" type="text" placeholder="Enter company name" />&nbsp;&nbsp;<button type="button" id="searchSitesButton">Search</button>&nbsp;&nbsp;<button type="button" id="clearSearchSitesButton">Clear</button></div>
                        <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%" id="sites">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Company
                                    </th>
                                    <th align="left">
                                        Email
                                    </th>
                                    <th align="left">
                                        Tel
                                    </th>
                                    <th align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="sys-template" id="clientsgrid">
                                <tr>
                                    <td>
                                        {{CompanyName}}
                                    </td>
                                    <td>
                                        {{Email1}}
                                    </td>
                                    <td>
                                        {{Tel1}}
                                    </td>
                                    <td align="right">
                                        <a href="javascript:void(0);" onclick="setSite($(this).attr('dataid'), $(this).attr('companyname'));return false;" dataid="{{ClientSiteID}}" companyname="{{CompanyName}}">
                                            <img src="../styles/images/icon_users.png" class="help" mytitle="Impersonate" alt="Impersonate" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="Pagination"></div>
                    </div>
            </div>
            <div class="column_right">
                <div class="header">
                    <span>What's New In BoltCMS</span></div>
                <div class="content" id="dynamicContent" runat="server">
                </div>
            </div>
        </div>
        <div class="twocolumn">
            <div class="column_left" style="margin-top: 10px!important;">
                <div class="header">
                    <span>Video Tutorials</span>
                </div>
                <div class="content">
                    <p>
                        <div id="inputcontent">
                            <div>
                                Our system is easy to use, but thought you'd find it beneficial to watch a video,
                                showing you the steps. Click on a link below to view the tutorial:</div>
                            <div>
                                &nbsp;</div>
                            <div>
                                <strong>Page</strong></div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/pvqFAX1QAHE" target="_blank">Create
                                    a Page</a>
                            </div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/5wc8yeZn1Mw" target="_blank">Edit
                                    a Page</a>
                            </div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/8_3qIdfJGhI" target="_blank">Hide
                                    a Page</a>
                            </div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/Q-I5fygkCeE" target="_blank">Link
                                    a Page</a></div>
                            <div>
                                &nbsp;</div>
                            <div>
                                <strong>Gallery</strong></div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/n1c_aZ3By0w" target="_blank">Create
                                    an Album</a></div>
                            <div>
                               <a class="video" href="http://www.youtube.com/embed/ocitomG2faw" target="_blank">
                                    Add an Image to an Album</a></div>
                            <div>
                               <a class="video" href="http://www.youtube.com/embed/Brc6asQ1-Hg" target="_blank">
                                    Edit an Image Caption/Title and Change an Image</a></div>
                            <div>
                                <a class="video" href="http://www.youtube.com/embed/B_LzTQdnqxI" target="_blank">Insert
                                    an Album into a Page</a></div>
                            <div>
                                &nbsp;</div>
                            <div>
                                Please let us know if you'd like know how to do something on BoltCMS, we'd be happy
                                to upload the video tutorial. Send a mail to <a href="mailto:support@prepeeled.com">
                                    support@prepeeled.com</a></div>
                            <p>
                            </p>
                        </div>
                        <p>
                        </p>
                        <script type="text/javascript">
                            $(".video").fancybox({ type: 'iframe' });
                        
                        </script>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
