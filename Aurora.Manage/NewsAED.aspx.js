﻿//#region Vars 
var TotalCount = 0;
var CurrentPage = 0
var templateList;
var templateDetail;
var _newsType = null;
var _categoryId = 0;
var CurrentData = {};
var catList = null;
var Loaded = false;
var Global = new GlobalVariables.GlobalVar();
//#endregion

//#region Init
$(function () {
    $("#btnSave").hide();
    if (getQueryVariable('type') == "List" || getQueryVariable('type') == "Delete") {
        $("#contentAddEdit").hide();
        $("#AddNewCat").hide();
        getNewsCategory();
    } else if (getQueryVariable('type') == "Add") {
        $("#contentList").hide();
    }
    $('div.content').block({ message: null });
    $('#categoryEditFancy').fancybox({
        padding: 0,
        titleShow: true,
        title: '',
        overlayColor: '#000000',
        overlayOpacity: .5,
        showActivity: true,
        width: 800,
        height: 600,
        type: "iframe",
        onClosed: function () {
            getNewsCategory();
        }
    });
});
//#endregion

//#region Custom Methods 

function onPageError(result) {
    if (typeof result.status == "undefined") {
        onError(result);
    } else {
        DisplayMessage("Messages", "error", "status: " + result.status + " statusText: " + statusText)
    }
}

function getDate(date) {
    if (!date) {
        return "";
    } else {
        return formatDateTime(date, "dd/MM/yyyy");
    }
}

function loadEditor() {
    InitEditor("Details", "editorSpan",true); 
}
//#endregion

//#region Data Manipulation 

function loadList(newsType, changeType, categoryId) {
    //try {
        $('div.content').block({ message: null });
        _newsType = newsType;
        _categoryId = categoryId;
        if (!newsType) {
            newsType = "";
        }

        if (changeType) {
            $("#contentAddEdit").hide();
            $("#btnSave").hide();
            $("#Messages").hide();
        }
        if ($("#NewsCat").val() > -1) {
            Aurora.Manage.Service.AuroraWS.GetNewsAndCount($("#NewsCat").val(), CurrentPage + 1, Global.PageSize, newsType, onloadListSuccess, onError);
        }
        else {
            Aurora.Manage.Service.AuroraWS.GetOrphanedNewsAndCount(CurrentPage + 1, Global.PageSize, newsType, onloadListSuccess, onError);
        }

        function onloadListSuccess(result) {
            if (result.Result == true) {

                if (templateList == null)
                    templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
                if (result.Data[0] != null) {
                    $("div.content").show();
                    templateList.set_data(result.Data);
                    if (TotalCount == 0) {
                        TotalCount = result.Count;
                        initPagination();
                    }
                    TotalCount = result.Count;
                    CurrentData = result.Data;
         

                
                } else {
                    DisplayMessage("Messages", "warn", "No news available for current selection.", 5000);
                    $("#contentAddEdit").hide();
                    $("#btnSave").hide();
                    $("div.content").hide();
                }
                $('div.content').unblock();
                $(".tipsy").remove();
                $(".help").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            } else {
                DisplayMessage("Messages", "error", result.ErrorMessage)
            }
        }
  //  } catch (e) {

 //   }
}

function loadDetail(anchor) {

    DisplayMessage("Messages", "into", "Loading news details.");
    $("#btnSave").show();
    $("#contentAddEdit").show();
    $("#contentAddEdit").block({ message: null });

    //stop the page page from caching
    var noCache = new Date();
    if (anchor.dataid) {
        $("#contentAddEdit").load("news.aspx?newsid=" + anchor.dataid + "&nocache=" + noCache.getMilliseconds(), onLoadSuccess);
    }
    else {
        $("#contentAddEdit").load("news.aspx?newsid=-1&nocache=" + noCache.getMilliseconds(), onLoadSuccess);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    $("#contentAddEdit").block({ message: null });
//    scrollToElement("Messages", -20, 2);

//    scrollToElement("Messages", -20, 2);
    $("#contentAddEdit").unblock();


    function onLoadSuccess(responseText, status, xhr) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        } else {
            $("#contentAddEdit").unblock();
        }
        scrollToElement("Messages", -20, 2);
       //$("#contentAddEdit").insertAfter("#Messages");
    }
}

function saveData(deleteData) {

    $("#contentAddEdit").block({ message: null });

    var news = new AuroraEntities.News();

    if (deleteData) {
        var msg = $("#newsType").val() == "Active"
                      ? "Item will be moved to the archived items,\nDo you wish to continue?"
                      : "Item will be removed permanently,\nDo you wish to continue?";
        if (confirm(msg)) {
            news = findByInArray(CurrentData, "ID", $(deleteData).attr("dataid"))
            if ($("#newsType").val() == "Active") {
                news.EndDate = new Date();
                news.EndDate = new Date(news.EndDate.setMonth(news.EndDate.getMonth() - 1));
            }
            else {
                news.DeletedOn = new Date();
            }
        } else {
            return;
        }

    } else {

        if (validateForm("ItemDetails", true, "Messages", false)) {
            //$("#contentAddEdit").insertAfter("#Messages");
            scrollToElement("Messages", -20, 2);
            $("#contentAddEdit").unblock();
            try {
                saveTriggered = false;
            } catch (e) {

            }
            return false;
        }

        //get the form data
        Global.populateEntityFromForm(news, $('#form').serializeArray());
        news.ID = $("#ID").val();
        if (news.ID == "") {
            news.ID = -1
        }
        news.Title = $("#Title").val();
        news.Preview = $("#Preview").val();
        news.CustomXML = getCustomXML();
        news.Details = oEdit1.getHTMLBody();
        news.StartDate = formatDateTimeLongDateString($("#StartDate").val());
        news.EndDate = formatDateTimeLongDateString($("#EndDate").val());
        news.InsertedOn = news.InsertedOn == "" ? new Date() : news.InsertedOn;
        news.CategoryID = $("#CategoryID").val();
        news.ClientSiteID = 0;
        news.Image1Caption1 = $("#ImageCaption1").val();
        news.EntityKey = new Object();
        news.EntityKey.EntityContainerName = "AuroraEntities";
        news.EntityKey.EntitySetName = "News";
        news.EntityKey.IsTemporary = false;
        news.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();
        objKey.Key = "ID";
        objKey.Value = parseFloat(news.ID) ? parseFloat(news.ID) : -1;
        news.EntityKey.EntityKeyValues[0] = objKey;

    }

    Aurora.Manage.Service.AuroraWS.SaveNews(news, onSaveDataSuccess, onError); 
   
    function onSaveDataSuccess(result) {
        if (result.Result == true) {
           
            DisplayMessage("Messages", "success", "News was successfully updated.");
            $("#ID").val(result.Data.ID);
            $("#contentAddEdit").hide();
            $('#btn_image1').fancybox({
                padding: 0,
                titleShow: true,
                title: "Upload An Image for Caption 1",
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                width: '80%',
                height: '80%',
                width: '90%',
                height: '90%',
                onStart: function () { saveData() },
                onComplete:function(){$.fancybox.center();},
                onClosed: function () { reloadAfterImage(); },
                type: "iframe"
            });


            try {
                if (saveTriggered) {
                    $('#btn_image1')[0].href = String.format('WebImage/WebImageUpload.aspx?ipath={0}/Uploads/&iName=news_01_{1}.jpg&smWidth={2}&lgWidth={3}&smPrefix=sm_', client.replace("~", ""), result.Data.ID, GetJSSession("News_smWidth") == undefined ? 200 : GetJSSession("News_smWidth"), GetJSSession("News_lgWidth") == undefined ? 600 : GetJSSession("News_lgWidth"));
                    $('#btn_image1').trigger('click');
                    saveTriggered = false;
                    //$("#contentAddEdit").insertAfter("#Messages");
                }
            }
            catch (e) { }
            
            loadList($("#newsType").val());
        } else {
            DisplayMessage("Messages", "error", result.ErrorMessage)
        }
        $("#contentAddEdit").unblock();
        //$("#contentAddEdit").insertAfter("#Messages");
        scrollToElement("Messages", -20, 2);
    }
}

function deleteData(anchor) {
    saveData(anchor);
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadList(_newsType,null,_categoryId);
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadList(_newsType, null, _categoryId);
    }
    $("#contentAddEdit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

function getNewsCategory() {
    var lastIndex = $("#NewsCat")[0].selectedIndex;
    if ($('#Messages')[0].className != "alert_success")
        $('#Messages').hide();

    var requestCats = function (result) {
        if (result.Data.length != 0) {
            if (catList == null) {
                catList = $create(Sys.UI.DataView, {}, {}, {}, $get("NewsCat"));
            }

            catList.set_data(result.Data);
            $("#NewsCat").append("<option  value='-1'>Uncategorised Items</option>");
            if (!Loaded) {
                Loaded = true;
                $("#NewsCat")[0].selectedIndex = 0;
            }
            else {
                $("#NewsCat")[0].selectedIndex = lastIndex;
            }

            loadList(null, null, lastIndex);
        }
        else {
            $("#contentList").hide();
            $("#AddNewCat").show();
            $('div.content').unblock();

        }
        $('#newsType').val('Active');
    }

    Aurora.Manage.Service.AuroraWS.GetCategories(0, 0, "", 3, 0, requestCats, onError);
}

function addNewCat() {
    $("#contentList").hide();
    $("#AddNewCat").show();
    if (!$("#AddNewCat .content").is(":visible")) {
        $("#AddNewCat .header span").click();
    }
    $('div.content').unblock();
}
function AddNewsCategory() {
    if (validateForm("addData", true, "AddMsg", true)) {
        return;
    }
    var cat = new Object();
    cat.ClientSiteID = 0;
    cat.Description = $("#CatDescription").val();
    cat.Name = $("#CatName").val();
    cat.OrderIndex = $("#CatOrder").val();
    cat.FeatureModuleID = 3;


    var requestAddCat = function (result) {
        if (result.Count == 0) {
            DisplayMessage("AddMsg", "success", "Category Added");
            ClearForm();
        }
        else if (result.Count == 1) {
            DisplayMessage("AddMsg", "warn", "A catergory with this name already exists.");
        }
        else {
            DisplayMessage("AddMsg", "error", result.ErrorMessage);
        }
    }

    Aurora.Manage.Service.AuroraWS.UpdateCategory(cat, requestAddCat, onError);

}
function changeNewsCat() {

    $('div.content').block({ message: null });
   
    CurrentPage = 0;
    TotalCount = 0;
    getNewsCategory();
    initPagination();
}
//#endregion