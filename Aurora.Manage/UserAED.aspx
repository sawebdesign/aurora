﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="UserAED.aspx.cs" Inherits="Aurora.Manage.UserAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <script type="text/javascript" src="UserAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>

    <asp:Panel ID="pnlList" runat="server">
	<!-- Begin content -->
	<div id="contentList">
		<div class="inner">
			<!-- Begin one column window -->
			<div class="onecolumn">
				<div class="header">
					<span>User List</span>
                    <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                        <a href="javascript:void(0);" onclick="loadDetail(this);return false;" userid="-1" ><img src="Styles/images/icon_users.png" alt="Create User" title="Create User" /></a>
                    </div>
				</div>
				<br class="clear"/>
				<div class="content">
						<table class="data" width="100%" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
                                    <th style="width: 30%" align="left">Company Name</th>
									<th style="width:30%" align="left">First Name</th>
									<th style="width:20%" align="left">Last Name</th>
									<th style="width:30%" align="left">Email</th>
									<th style="width:15%" align="center">Function</th>
								</tr>
							</thead>
							<tbody id="listTemplate" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td>{binding CompanyName}</td>
							        <td>{binding FirstName}</td>
							        <td>{binding LastName}</td>
                                    <td><a class="mail" href="#">{binding Email}</a></td>
				  		            <td align="center">
							            <a href="javascript:void(0);" onclick="loadDetail(this);return false;" userid="{binding ID}" ><img src="../styles/images/icon_edit.png" alt="edit" class="help" title="Edit"/></a>
							            <a href="javascript:void(0);" onclick="DeleteUser(this);return false;" userid="{binding ID}"><img src="../styles/images/icon_delete.png" alt="delete" class="help" title="Delete"/></a>
							        </td>
						        </tr>
							</tbody>
						</table>
					
					<!-- Begin pagination -->
                        <div id="Pagination"></div>
					<!-- End pagination -->
					
				</div>
			</div>
			<!-- End one column window -->
	    </div>
	<!-- End content -->
    </div>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td><div id="Messages"></div></td>
        </tr>
    </table>
    <asp:Panel ID="pnlAddEdit" runat="server">
    <!-- Begin content -->
	<div id="contentAddEdit">
		<div class="inner">
			<!-- Begin one column window -->
			<div class="onecolumn">
				<div class="header">
					<span>User Data</span>
				</div>
				<br class="clear"/>
                <div class="detail">
                    <div class="content" style="text-align: left;" id="UserDetails">
						<table width="100%" cellpadding="2" cellspacing="4">
							<tbody id="detailTemplate" class="sys-template" sys:attach="dataview">
                                <input type="hidden" id="UserID" name="ID" value="{binding ID}" />
                                <input type="hidden" id="ClientSiteID" name="ClientSiteID" value="{binding ClientSiteID}" />
                                <input type="hidden" id="LastLoginDate" name="LastLoginDate" value="{binding LastLoginDate}" />
                                <input type="hidden" id="LastActivityDate" name="LastActivityDate" value="{binding LastActivityDate}" />
                                <input type="hidden" id="LastPasswordChangedDate" name="LastPasswordChangedDate" value="{binding LastPasswordChangedDate}" />
                                <input type="hidden" id="LastLockedOutDate" name="LastLockedOutDate" value="{binding LastLockedOutDate}" />
                                <input type="hidden" id="InsertedOn" name="InsertedOn" value="{binding InsertedOn}" />
                                <input type="hidden" id="InsertedBy" name="InsertedBy" value="{binding InsertedBy}" />
                                <input type="hidden" id="DeletedOn" name="DeletedOn" value="{binding DeletedOn}" />
                                <input type="hidden" id="DeletedBy" name="DeletedBy" value="{binding DeletedBy}" />
                                <tr>
							        <td>First Name :</td>
							        <td><input type="text" id="FirstName" name="FirstName" valtype="required" Style="width: 285px" title="First Name" value="{binding FirstName}" /></td>
						        </tr>
                                <tr>
							        <td>Last Name :</td>
							        <td><input type="text" id="LastName" name="LastName" valtype="required" Style="width: 285px" title="Last Name" value="{binding LastName}" /></td>
						        </tr>
                                <tr>
							        <td>Email :</td>
							        <td><input type="text" id="Email" name="Email" valtype="required; regex:email" valmessage="Email is in the incorrect format please use: yourname@company.co.za" Style="width: 285px" title="Email" value="{binding Email}" /></td>
						        </tr>
                                <tr>
							        <td>Phone :</td>
							        <td><input type="text" id="Phone" name="Phone" valtype="required; regex:phone" valmessage="Phone Number is in the incorrect format please use: 0315551234" Style="width: 285px" title="Phone" value="{binding Phone}" /></td>
						        </tr>
                                <tr>
							        <td>Fax :</td>
							        <td><input type="text" id="Fax" name="Fax" valtype="regex:phone" valmessage="Fax Number is in the incorrect format please use: 0315551234" Style="width: 285px" title="Fax" value="{binding Fax}" /></td>
						        </tr>
                                <tr>
							        <td>User Name :</td>
							        <td><input type="text" id="UserName" name="UserName" valtype="required" Style="width: 285px" title="User Name" value="{binding UserName}" /></td>
						        </tr>
                                <tr>
							        <td>Password :</td>
							        <td><input type="text" id="Password" name="Password" valtype="required; regex:Password" valmessage="Password must be at least 4 characters, no more than 12 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" Style="width: 285px" title="Password" value="{binding Password}" /></td>
						        </tr>
                                <tr>
							        <td>IsLocked :</td>
							        <td><input type="checkbox" id="IsLocked" name="IsLocked" title="IsLocked" value="{binding IsLocked}" /></td>
						        </tr>
                                <tr>
							        <td>Password Question :</td>
							        <td><input type="text" id="PasswordQuestion" name="PasswordQuestion" valtype="required" Style="width: 285px" title="PasswordQuestion" value="{binding PasswordQuestion}" /></td>
						        </tr>
                                <tr>
							        <td>PasswordAnwser :</td>
							        <td> <input type="text" id="PasswordAnwser" name="PasswordAnwser" valtype="required" Style="width: 285px" title="PasswordAnwser" value="{binding PasswordAnwser}" /></td>
						        </tr>
                                <tr>
							        <td>Last Login Date :</td>
							        <td>{binding LastLoginDate, convert=getDate}</td>
						        </tr>
                                <tr>
							        <td>Last Activity Date :</td>
							        <td>{binding LastActivityDate, convert=getDate}</td>
						        </tr>
                                <tr>
							        <td>Roles :</td>
							        <td>
                                        <table width="100%" cellpadding="0" cellspacing="0"> 
                                            <tr>
                                                <td width="1%" valign="top"><select id="Roles" name="Roles" title="Roles" Style="width: 285px; height: 100px" multiple="multiple"></select></td>
                                                <td width="99%" valign="top"><ul id="Roles_selectedItems"></ul></td>
                                            </tr>
                                        </table>
                                    </td>
						        </tr>
                                <tr>
							        <td>Sites :</td>
							        <td>
                                        <table width="100%" cellpadding="0" cellspacing="0"> 
                                            <tr>
                                                <td width="1%" valign="top"><select ID="Sites" name="Sites" title="Sites" Style="width: 285px; height: 100px" multiple="multiple"></select></td>
                                                <td width="99%" valign="top"><ul id="Sites_selectedItems"></ul></td>
                                            </tr>
                                        </table>
                                    </td>
						        </tr>
                                <tr>
                                    <td>Default Site:</td>
                                    <td><select id="DefaultSite"></select></td>
                                </tr>
							</tbody>
						</table>

                        <p style="margin-top: 50px" align="right">
                            <input type="button" id="btnSave" onclick="saveData();" value="Save" class="Login" Style="margin-right: 5px" />
                        </p>
                        <br />
                    </div>
                </div>
			</div>
			<!-- End one column window -->
	    </div>
	<!-- End content -->
    </asp:Panel>
</asp:Content>
