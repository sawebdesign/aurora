﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="Aurora.Manage.News" %>
<%@ Register Assembly="Aurora.Custom" Namespace="Aurora.Custom.UI" TagPrefix="cui" %>
<%@ Register src="UserControls/MemberRoles.ascx" tagname="MemberRoles" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>News Details</title>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
        <script src="Scripts/custom_black.js" type="text/javascript"></script>
        <link href="/Styles/ui.all.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="News.aspx.js"></script>
<body>
    <form id="form1" runat="server">

        <!-- Begin content -->
      
            <!-- Begin one column window -->
            <div class="onecolumn">
                <div class="header">
                    <span>News</span>
                    <div style="width: 180px;" class="switch">
                        <table width="180px" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="button" style="width: 90px;" value="Details" class="left_switch active"
                                            name="tab1" id="tab1">
                                    </td>
                                    <td>
                                        <input type="button" style="width: 90px;" value="Editor" class="middle_switch" name="tab2"
                                            id="tab2">
                                    </td>
                                    <td>
                                        <input type="button" style="width: 90px;display:none" value="Security" class="right_switch" name="tab3"
                                            id="tab3">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br class="clear" />
                <div class="content" style="padding-margin:-15px;">
                 <div class="tab_content" id="tab1_content">
                    <div id="ItemDetails" style="width:100%;">
                        <fieldset id="templateDetail"   sys:attach="dataview">
                            <input type="hidden" id="ID" value="" runat="server" />
                            <input type="hidden" id="ClientSiteID" value="" runat="server" />
                            <input type="hidden" id="SchemaID" value="" runat="server" />
                            <input type="hidden" id="InsertedOn" value="" runat="server" />
                            <input type="hidden" id="InsertedBy" value="" runat="server" />
                            <input type="hidden" id="DeletedOn" value="" runat="server" />
                            <input type="hidden" id="DeletedBy" value="" runat="server" />
                            <asp:Image runat="server" ID="Image1" Visible="false" />
                            <table width="100%">
                                <tr>
                                    <td valign="top" align="left">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Title :
                                                </td>
                                                <td>
                                                    <input type="text" id="Title" name="title" valtype="required" class="help" 
                                                     maxlength="250"   mytitle="This text will appear as the heading for your article" value="" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Preview :
                                                </td>
                                                <td>
                                                    <input type="text" id="Preview" name="title" valtype="required" class="help" 
                                                        maxlength="150" mytitle="This text will appear in a feed on your website" value=""
                                                        runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Category :
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="CategoryID" runat="server" ClientIDMode="Static" Width="166px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Start Date :
                                                </td>
                                                <td>
                                                    <input type="text" id="StartDate" name="startdate"  valtype="required;regex:date" class="help"
                                                        mytitle="The date the article must display" value="" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    End Date :
                                                </td>
                                                <td>
                                                    <input type="text" id="EndDate" name="enddate"  valtype="required;regex:date" class="help"
                                                        mytitle="The date at which this news item shall expire and be moved to your archives" value="" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Image Upload:
                                                </td>
                                                <td>
                                                    <input type="text" id="ImageCaption1" runat="server" class="help" mytitle="This text appears under the uploaded image" />
                                                    <a id="btn_image1" href="#" class="help" mytitle="click here to upload an image">
                                                        <img src="Styles/images/icon_media.png" /></a>
                                                </td>
                                            </tr>
                                            <tr id="CustomData" runat="server">
                                                <td colspan="2">
                                                    <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                                        <cui:XmlPanel ID="ContentXml" runat="server">
                                                        </cui:XmlPanel>
                                                    </cui:ObjectPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                   
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    </div>
                    <div class="tab_content hide" id="tab2_content">
                      <textarea id="Details" class="blaablaa" clientidmode="Static" runat="server" rows="4" cols="30"></textarea>
                      <span id="editorSpan"></span>
                    </div>
                    <div id="tab3_content" class="tab_content hide">
                        <uc1:MemberRoles ID="MemberRoles1" ModuleName="News" runat="server" />
                    </div>
                    <!-- End one column window -->
                </div>
      
      </div>
       <p style="margin-top: 50px; margin-right: 15px" align="right">
            <input type="button" id="btnSave" onclick="saveData();" value="Save" class="Login"
                style="margin-right: 6px" />
        </p>
    <!-- End content -->
        
    </form>
</body>
</html>
