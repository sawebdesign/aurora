﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="SizeAED.aspx.cs" Inherits="Aurora.Manage.SizeAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" language="javascript" src="SizeAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
<div class="onecolumn">
        <div class="header">
            <span>Product Size List</span>
            <div style="float: left; margin-top: 10px; margin-left: 10px;">
                     <select id="filter">
                        <option selected="selected">Active</option>
                        <option>Inactive</option>
                     </select>
              </div>
              <div style="float: right; margin-top: 10px; margin-right: 10px;">
                        <img dataid="-1" src="Styles/images/add.png" class="help" alt="Create Size" mytitle="Create Size"
                            style="cursor: pointer" dataid="{{ID}}" onclick="getDetail($(this).attr('dataid'));" />
              </div>
        </div>
        <br class="clear" />
        <div class="content" id="grid">
            <table class="data" width="100%" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th align="left">
                            Size
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" sys:attach="dataview" id="template" >
                  <tr>
                        <td>
                        {{Description}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);"  dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" dataid="{{ID}}" onclick="getDetail(this.getAttribute('dataid')); return false;" /></a>
                            <a href="javascript:void(0);"  dataid="{{ID}}" onclick="deleteItem(this); return false;">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="Pagination"></div>
        </div>
    </div>
        <div id="Messages">
        </div>
   <div id="Edit"></div>
</asp:Content>
