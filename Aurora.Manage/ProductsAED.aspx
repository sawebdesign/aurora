﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="ProductsAED.aspx.cs" Inherits="Aurora.Manage.ProductsAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Styles/DropDown.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="ProductsAED.aspx.js?cache=<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <script type="text/javascript" src="/Scripts/fg.menu.js"></script>
    <link type="text/css" href="/Styles/fg.menu.css" media="screen" rel="stylesheet" />
    <link type="text/css" href="/Styles/ui.all.css" media="screen" rel="stylesheet" />
    <div id="contentList">
        <!-- Begin one column window -->
        <div class="onecolumn" id="products">
            <div class="header">
                <span>Product List</span>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
               
                    <img src="Styles/images/add.png" dataid="-1" alt="Create Product" title="Create Product"
                        style="cursor: pointer" onclick="currentItemID = 0; product.edit(-1);" />
                </div>
                 <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <input type="text" id="SearchBox" value="Search by Name/Code.."  onclick="this.value='';TotalCount=0; CurrentPage=0;" onkeyup="product.search(this.value);" />
                </div>
                
                <div style="float: left; margin-top: 12px; margin-right: 10px;">
                    <div class="myfgbutton" style="margin-top:-10px;margin-left:10px;">
                        &nbsp; <a tabindex="0" href="#news-items-2" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all"
                            id="hierarchybreadcrumb"><span style="float: right; height: 10px;" class="ui-icon ui-icon-triangle-1-s">
                            </span>Product Categories</a>
                    </div>
                </div>
                <div id="CatURL" style="float: left; font-weight: bold;font-size: 14px; margin-top: 10px; margin-left: 10px;">
                </div>
                <%--<div style="float: left; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                    &nbsp;<select id="ProductType" onchange="product.display(this.value);">
                        <option value="false">Active</option>
                        <option value="true">Archived</option>
                    </select>
                </div>--%>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data" id="prodGrid" width="100%" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th align="left">
                                Name
                            </th>
                            <th align="left">
                                URL
                            </th>
                            <th align="left">
                                Size
                            </th>
                            <th align="left">
                                Price
                            </th>
                            <th align="left">
                                Order
                            </th>
                            <th align="right">
                                Functions
                            </th>
                        </tr>
                    </thead>
                    <tbody id="template" class="sys-template" sys:attach="dataview">
                        <tr myid="{{products.ID}}" ishidden="{{products.isHidden}}">
                            <td width="20%">
                                {{products.Name}}
                            </td>
                            <td width="10%">
                                {{"/Product/"+products.CategoryID+"/"+products.ID}}
                            </td>
                            <td width="20%">
                                {{Description}}
                            </td>
                            <td width="10%">
                                {{products.Price}}
                            </td>
                            <td width="10%">
                                {{products.OrderIndex == null ? "Not Set" : products.OrderIndex}}
                            </td>
                            <td align="right" width="10%" >
                                <a hasChildren="{{hasChildren}}" href="javascript:void(0);" onclick="product.showChildren(this); return false;" state="closed" loaded="false" sys:dataid="{{products.ID}}" class="showChildren">
                                    <img src="../styles/images/arrow_branch.png" alt="edit" class="img" mytitle="Show Other Sizes" />
                                </a>
                                <a href="javascript:void(0);" onclick="">
                                    <img src="Styles/images/add.png" dataid="{{products.ID}}" class="img" alt="add" onclick="isNewSize=true;product.edit($(this).attr('dataid'));return false;"
                                        mytitle="Append a different size" style="cursor: pointer;" /></a> 
                                <a href="javascript:void(0);"
                                            onclick="product.edit($(this).attr('dataid'));return false;" dataid="{{products.ID}}">
                                            <img src="../styles/images/icon_edit.png" class="img" mytitle="Edit" /></a>
                                <a href="javascript:void(0);" onclick="product.remove(this.getAttribute('dataid'));return false;"
                                    dataid="{{products.ID}}">
                                    <img src="../styles/images/icon_delete.png" class="img" mytitle="Delete" /></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="html_body">
                </div>
                <!-- Begin pagination -->
                <div id="Pagination">
                </div>
                <!-- End pagination -->
            </div>
        </div>
        <div id="Messages">
        </div>
        <asp:Panel ID="pnlAddEdit" runat="server" ClientIDMode="Static">
            <input type="hidden" id="CatParentVal" />
            <div id="contentAddEdit">
            </div>
            <p align="right">
                <input type="button" id="btnSave" style="display: none;" dataid="" onclick="product.update();"
                    value="Save" class="Login" />
            </p>
        </asp:Panel>
        <!-- End one column window -->
        <!-- End content -->
        <!--Add Cat-->
        <div id="CategoryListContainer" style="display: none;">
            <!-- Begin one column window -->
            <div class="inner">
                <div class="onecolumn">
                    <div class="header">
                        <span>Categories</span>
                        <div style="float: right; margin-top: 10px; margin-right: 10px;">
                            <img src="Styles/images/add.png" dataid="-1" class="help" alt="Create Category" title="Create Category"
                                style="cursor: pointer" onclick="categories.editCategory(this);" />
                        </div>
                        <div style="float: left; margin-top: 12px; margin-right: 10px;">
                            <div class="myfgbutton" style="margin-top:-10px;margin-left:10px;">
                                &nbsp; <a tabindex="0" onclick="categories.showCategories(false);" href="#news-items-2" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all"
                                    id="hierarchybreadcrumb2">Back to products</a>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table width="100%" class="data" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                   
                                    <th align="left">
                                        Name
                                    </th>
                                    <th align="left">
                                        Parent Name
                                    </th>
                                    <th align="left">
                                        Order
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th>
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="CategoryList" class="sys-template" sys:attach="dataview">
                                <tr style="{{isHidden ? 'color:#eeeeee;' : '';}}">
                                    
                                    <td width="30%">
                                        {{Name}}
                                    </td>
                                    <td width="30%">
                                        {{categories.findCatInResult(ParentID,"Self")}}
                                    </td>
                                    <td width="10%">
                                        {{OrderIndex}}
                                    </td>
                                    <td width="10%">
                                        {{Description}}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="categories.editCategory(this);return false;"
                                            dataid="{{ID}}">
                                            <img src="../styles/images/icon_edit.png" class="help" title="Edit" /></a> <a href="javascript:void(0);"
                                                onclick="categories.deleteCategory(this.getAttribute('dataid'));return false;" dataid="{{ID}}">
                                                <img src="../styles/images/icon_delete.png" class="help" title="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Begin pagination -->
                        <div id="CatPagination">
                        </div>
                    </div>
                    <!-- End pagination -->
                </div>
            </div>
        </div>
        <div id="AddMsg">
        </div>
        <div id="AddNewCat" style="display: none;">
            <div id="Add">
                <div class="inner">
                    <!-- Begin one column window -->
                    <div class="onecolumn">
                        <div class="header">
                            <span>Category</span>
                        </div>
                        <br class="clear" />
                      
                        <div class="content">
                            <input type="hidden" id="CatID" />
                            <asp:Image runat="server" ID="Image1" ClientIDMode="Static" Visible="true" />
                            <table width="100%" id="addData">
                                <tr>
                                    <td>
                                        Name:
                                    </td>
                                    <td>
                                        <input type="text" id="CatName" valtype="required" />
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <input type="text" id="CatDescription" value="Product" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Order
                                    </td>
                                    <td>
                                        <input type="text" id="CatOrder" maxlength="3" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Parent Category:
                                    </td>
                                    <td>
                                        <input type="hidden" id="CatSelectID" />
                                        <div style="width: 150px;">
                                            <a tabindex="2" href="#news-item-2" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all"
                                                id="CatParent"><span style="float: right; height: 10px;" class="ui-icon ui-icon-triangle-1-s">
                                                </span>Product Categories</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Category Image:</td>
                                    <td>
                                        <a id="btn_catimage">
                                            <img src="Styles/images/icon_media.png" class="help" mytitle="Click here to upload an image" /></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" value="Save" onclick="categories.addCategory();" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
