﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Colour.aspx.cs" Inherits="Aurora.Manage.Colour" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" media="screen" type="text/css" href="/Styles/colorpicker.css" />
     <script type="text/javascript" src="/Scripts/colorpicker.js"></script>
    <script type="text/javascript" src="Colour.aspx.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Editing</span>
        </div>
        <br class="clear" />
        <div class="content">
            <asp:Image runat="server" ID="Image1" />
            <div style="width:40%;">
                <input type="hidden" runat="server" id="ID" />
                <input type="hidden" id="ClientSiteID" runat="server" />
                <input type="hidden" id="CustomXML" runat="server" />
                <input type="hidden" id="HexValue" runat="server" />
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                 <tr>
                    <td>Name:</td>
                    <td> 
                        <input  runat="server" id="Name"/>
                    </td>
                </tr>
                <tr>
                    <td>Upload A Texture Image(mouse over to view):</td>
                    <td> <a id="btn_image1" href="#">
                                    <img src="Styles/images/icon_media.png" class="help" mytitle="Click here to upload an image" /></a></td>
                </tr>
                <tr>
                    <td>
                       Choose A Colour:
                    </td>
                    <td>
                        <div id="colour" mytitle="Add a new colour to your color spectrum." class="help"  style="width: 30px; height: 30px;cursor: pointer; background-image: url('Styles/images/select.png'); background-position-x: center; background-position-y: 50%;">
                        </div>
                    </td>
                </tr>
            </table>
            </div>
        </div>
        
    </div>
    <input style="float:right;" value="Save" type="button" onclick="saveColours();" />
    </form>
</body>
</html>
