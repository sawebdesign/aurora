﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modules.aspx.cs" Inherits="Aurora.Manage.Innova.common.Modules" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insert A Module</title>
	<script type="text/javascript" src="/Scripts/jquery-1.5.2.min.js"></script>
    <script type="text/javascript">
        var currentItem;
        function insertObject(sHTML) {
            if (sHTML!= undefined && sHTML != "") {
                var obj = (opener ? opener : openerWin).oUtil.obj;

                //Use insertHTML() function to insert your custom html
                obj.insertHTML(sHTML);
            }
            else {
                alert("Please select a module that you would like to add to this page");
            }
            
        }
        function setAttr(objElem,classBased) {
            var elem = null;
            for (var opts = 0; opts < objElem.length; opts++) {
                if (objElem[opts].selected) {
                    elem = objElem[opts];
                }
            }
            if (!elem && elem.value != "null") {
                return;
            }

            var div;

            if (!classBased) {
            	div = document.getElementById(objElem.getAttribute("id").replace("_option", ""));


            	//Custom forms
            	if (div.id === "CustomModules") {
            		div.setAttribute("catFilter", elem.value);
            		div.setAttribute("filter", elem.value);
            	}
            	else {
            		div.setAttribute("catFilter", elem.innerHTML);
            		div.setAttribute("filter", elem.innerHTML);
            	}


            	div.firstChild.innerHTML = div.firstChild.innerHTML.replace("Placeholder", "Place holder showing only items from '" + elem.innerHTML + "'");
            	currentItem = document.getElementById(objElem.getAttribute("id").replace("_option", "_container")).innerHTML;
            } else {
            	
            	div = $("#" + objElem.getAttribute("id").replace("_option", "") + "_container " + "." + objElem.getAttribute("id").replace("_option", ""));
            	div.attr("categoryfilter", elem.value);
            	div.attr("catFilter", elem.value);
            	div.attr("filter", elem.value);

            	div.html(div.html().replace("Placeholder", "Place holder showing only items from '" + elem.innerHTML + "'"));

            	currentItem = document.getElementById(objElem.getAttribute("id").replace("_option", "_container")).innerHTML;
            }
        }

        function setModuleOptions(element, attributeName, moduleName) {
        	element = $(element);
        	var moduleDiv = $("#" + moduleName + "_container");

			//check if there is a class based item
        	if (moduleDiv.find("." + moduleName).length > 0) {
        		moduleDiv.find("." + moduleName).attr(attributeName, element.val());
        	} else {
				//otherwise just set the attributes on the main div
        		moduleDiv.find("#" + moduleName).attr(attributeName, element.val());
        	}

        	if (element.attr("id").indexOf("_option") !== -1 && element.is("select")) {
        		var selectedString = "";

        		element.find("option").each(function () {
        			if ($(this).is(":selected")) {
        				selectedString += $(this).text() + ", ";
        			}

        			moduleDiv.find(".placeholder").html("[" + moduleName + " Placeholder showing items from '" + selectedString.substring(0, selectedString.length - 2) + "']");
        		});
        	}

        	currentItem = moduleDiv.html();
        }
    </script>
   <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body style="background:none;margin-left:15px;background-color:#ffffff;padding-bottom:50px;">
        <p><br/>To insert a module into your content page, please follow these simple steps:<br/><br/></p>
        <ol style="margin-left:20px;">
            <li style="font-weight:bold;">Select the module you’d like to insert into your content page.<br/><br/></li>
            <p>If the module you have selected has various categories, for example the module News has categories, General, Latest, Members.  You are able to select a specific category to display in your content page.
Simply click on the drop down list and select the category, i.e Members.<br/><br/></p>
            <li style="font-weight:bold;">Click apply.<br/><br/></li>
        </ol>
    <table class="data" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <th align="left">Module Name:</th>
        <th align="left">Filter by Category:</th>
    </tr>
        <%=moduleList%>
    </table>
    <%=moduleHTML %>
    <%=moduleCat%>
    <div style="position:fixed;bottom:0px;width:100%;background-color:#eeeeee;">
    <input value="Apply" type="button" onclick="insertObject(currentItem);self.close();" />&nbsp;
    <input value="Close" type="button" onclick="self.close();" />
    </div>
</body>
</html>
