﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetVouherCampaign.aspx.cs" Inherits="Aurora.Manage.Innova.common.GetVouherCampaign" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select the voucher campaign you wish to insert</title>
    <link href="/Styles/ui.all.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <script src="/Scripts/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="/Scripts/Utils.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script src="/Service/AuroraWS.asmx/jsdebug" type="text/javascript"></script>
    <script type="text/javascript">
        
        var obj = (opener ? opener : openerWin).oUtil.obj;
        var voucherId = 0;
        $(function () {
            var response = function (result) {
                var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("myVouchers"));
                ds.set_data(result.Data);
            };

            Aurora.Manage.Service.AuroraWS.GetAllVouchers(response, onError);
        });

        function setID(obj) {
            $object = $(obj);

            if ($object.is(":checked")) {
                voucherId = $object.val();
            }
        }
        function applyVoucher() {
            obj.insertHTML('{vouchercodeplaceholder:'+voucherId+'}');
            self.close();
        }
    </script>
</head>
<body style="background:none;margin:20px;">
    <form id="form1" runat="server">
    <div><b>Here you may select the voucher campaign that you wish to distribue via this
        news letter, from the list of radio buttons below:</b></div>
    <div id="myVouchers" class="sys-template">
        <input type="radio" name="vouchers" onclick="setID(this);" value="{{ID}}" />{{Name}} <br />
    </div>
    <div>
        <input value="Apply" type="button" onclick="applyVoucher();" />&nbsp;<input
            value="Cancel" onclick="self.close();" type="button" />
    </div>
    </form>
</body>
</html>
