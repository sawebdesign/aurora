﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Newsletter.aspx.cs" Inherits="Aurora.Manage.Innova.common.Newsletter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Setup Content for this Newsletter </title>
    <link href="/Styles/ui.all.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <script src="/Scripts/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="/Scripts/Utils.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script src="/Service/AuroraWS.asmx/jsdebug" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(function () {
            var dates = $("#from, #to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        });

        var obj = (opener ? opener : openerWin).oUtil.obj;
        var ds = null;
        function ViewNews() {
           
            var response = function (result) {
                if (result.Result) {
                    if (!ds) {
                        ds = $create(Sys.UI.DataView, {}, {}, {}, $get("NewsPreview"));
                    }
                    ds.set_data(result.Data);
                }
            };
            if ($("#Criteria").val() == "0") {
            //current news
                Aurora.Manage.Service.AuroraWS.GetAllNews(new Date(), new Date("1901/01/01"), $((opener ? opener : openerWin).document.body).find("#ID").first().val(), response, onError);

            }
            else {
                //specified dates
                if ($("#from").val() != "" && $("#to").val() != "") {
                    Aurora.Manage.Service.AuroraWS.GetAllNews(new Date($("#from").val()), new Date($("#to").val()), $((opener ? opener : openerWin).document.body).find("#ID").first().val(),response,onError);
                }
                else {
                    return;
                }
            }

            $(".preview").show();
        }

        function addDatatoEditor() {
            var sHTML = "";

            $("#NewsPreview input").each(
                function (indx, elem) {
                    var $elem = $(elem);
                    if ($elem.is(":checked")) {
                        var data = findByInArray(ds._data, "ID", $elem.attr("id"));

                        sHTML += String.format("<div><b>{0}</b><br/><p>{1}</p></div>",data.Title,data.Details);
                    }

                }
            );

           //Use insertHTML() function to insert your custom html
            obj.insertHTML(sHTML);
        }
    </script>
</head>
<body style="background: none!important; padding: 10px;">
    <form id="form1" runat="server">
    <div id="NewsSetup">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <b>Select News Articles from:</b>
                    <select onchange="this.selectedIndex == 0 ? $('#dates').hide():$('#dates').show();"
                        id="Criteria">
                        <option value="0">Last Sent News Letter Date</option>
                        <option>Specific Dates</option>
                    </select>
                    <div id="dates" style="display:none;">
                        <label for="from">
                            From</label>
                        <input type="text" id="from" name="from" />
                        <label for="to">
                            to</label>
                        <input type="text" id="to" name="to" />
                    </div>
                    &nbsp;
                    <br />
                    <input id="btnView" type="button" value="View" onclick="ViewNews();" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr style="display: none;" class="preview">
                <td>
                    <h3>
                        Articles Preview:</h3>
                </td>
            </tr>
            <tr style="display: none;" class="preview">
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data">
                        <thead>
                            <tr>
                                <th align="left">
                                    Title
                                </th>
                                <th align="left">
                                    Preview
                                </th>
                                <th align="right">
                                    Select<input type="checkbox" onclick="$(this).is(':checked') == true ? $('#NewsPreview input').attr('checked','checked') : $('#NewsPreview input').removeAttr('checked');" />
                                </th>
                            </tr>
                        </thead>
                        <tbody id="NewsPreview" class="sys-template">
                            <tr>
                                <td align="left">
                                    {{Title}}
                                </td>
                                <td align="left">
                                    {{Preview}}
                                </td>
                                <td align="right">&nbsp;
                                    <input type="checkbox" id="{{ID}}" class="SelectedItems" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="display: none;text-align:right;" class="preview">
                <td><br /><input type="button" onclick="addDatatoEditor();" value="Add data to my form" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
