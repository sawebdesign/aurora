﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormWizard.aspx.cs" Inherits="Aurora.Manage.Innova.common.FormWizard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Form Wizard</title>
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <script src="../../Scripts/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../scripts/innovaeditor.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var editorOn = false;
        var gotEmail = false;
        var getFromEmail = false;
        function AddFields() {
            var name = document.getElementById("fldName");
            var label = document.getElementById("fldLabel");
            if (name.value == "" || label.value == "") {
                return;
            }

            var list = document.getElementById("fields");
            var req = document.getElementById("valrequired").checked ? document.getElementById("valrequired").value : "";
            var email = document.getElementById("valEmail").checked ? document.getElementById("valEmail").value : "";
            var opt = document.createElement("option");
            var type = document.getElementById("fldType").value;

            if (name.value.toUpperCase().indexOf("EMAIL") > -1 && !gotEmail) {
                opt.setAttribute("value", "email-FromEmail");
                gotEmail = true;
            }
            else {
                opt.setAttribute("value", name.value);
            }
            opt.setAttribute("type", type);
            opt.innerHTML = label.value;
            opt.setAttribute("validation", req + ";" + email);
            list.appendChild(opt);
            name.value = "";
            label.value = "";
            $("input:checkbox").removeAttr("checked");

        }

        function removeItem() {
            var list = document.getElementById("fields");
            if (list.selectedIndex == -1) {
                alert("Please click on an item to select and then click remove.")
                return;
            }

            list.removeChild(list[list.selectedIndex]);
        }

        function buildHTML() {
            var obj = (opener ? opener : openerWin).oUtil.obj;
            var sHTML = "<table cellpadding=0 cellspacing=2 border=0 id=EmailDetail>";
            var fields = $("#fields option");
            var subject = document.getElementById("subject").value;
            var thanksMessage = document.getElementById('ThankMessage').value;
            var mailDestination = document.getElementById("rbtnlstEmailSend").selectedIndex;
            var buttonName = document.getElementById("buttonName").value != ""
                             ? document.getElementById("buttonName").value
                             : "Send";

            if (mailDestination == -1 || mailDestination == 0) {
                alert('Please select a value for the where we should send the email.');
                return;
            }
            //validate form
            if (subject == "") {
                alert('Please enter a value for the subject field.');
                return;
            }

            if (fields.length == 0) {
                alert("Please add some fields to your form");
                return;
            }



            //subj
            sHTML += "<tr style='display:none;'><td>Subject:</td><td><input type='text' id=email-subject name=email-subject value='" + subject + "' /></td></tr>";

            //send to client
            if (mailDestination == 1) {
                sHTML += "<tr style='display:none;'><td>Send To Client:</td><td><input type='text' id='email-sendToClient' name='email-sendToClient' value='true' /></td></tr>";
            }

            if (thanksMessage != "") {
                sHTML += "<tr style='display:none;'><td>Thank You Message:</td><td id='email-SuccessMessage'><input type='text' value='" + thanksMessage + "' /></td></tr>";
            }
            sHTML += "<tr style='display:none;' colspan='2'><td>Everything above this text will not be visible on the front-end</td></tr>";
            //loop through controls and get data
            for (var item = 0; item < fields.length; item++) {
                if (fields[item]) {
                    if (fields[item].getAttribute("type") == "textarea") {
                        sHTML += "<tr><td valign='tpp'>" + fields[item].innerHTML + "</td><td><textarea cols='40' rows='10' valtype=" + fields[item].getAttribute("validation") + " id=email-" + fields[item].getAttribute("value") + " name=email-" + fields[item].getAttribute("value") + "></textarea></td></tr>";
                    }
                    else if (fields[item].getAttribute("type") == "freetext") {
                        sHTML += "<tr><td colspan='2' id='email-" + fields[item].getAttribute("value") + "' name='email-" + fields[item].getAttribute("value") + "'>Please enter your content in here.</td></tr>";
                    }
                    else {
                        if (fields[item].getAttribute("value").toString().indexOf("Email") > -1 && !getFromEmail) {
                            fields[item].setAttribute("value", "FromEmail");
                            getFromEmail = true;
                        }
                        sHTML += "<tr><td>" + fields[item].innerHTML + "</td><td><input type='text' valtype=" + fields[item].getAttribute("validation") + " id='email-" + fields[item].getAttribute("value") + "' name='email-" + fields[item].getAttribute("value") + "' /></td></tr>";
                    }
                }
            }
            //Add Qaptcha
            sHTML += "<tr><td colspan='2'><div id=QapTcha></div></td></tr>";
            //Add Send Button
            sHTML += "<tr><td><input type='button' id=SendMail name=SendMail value='" + buttonName + "'/></td></tr>";
          
           
            try {
                if (oDescription) {
                    //Add response message
                    sHTML += "<tr style='display:none'><td id='responsemsg' name='responsemsg'>" + oDescription.getHTMLBody() + "</td></tr>";
                }
            }
            catch (e) {
                
            }
            //Complete
            sHTML += "</table>";
            obj.insertHTML(sHTML);
            self.closeWin();
        }

        function cEdit(e) {
            $("#rbtnlstEmailSend").val("1");
            if ($("#fields option").length == 0) {
                showEdit(false);
                alert('Please create your fields before you add a custom message.');
                return false;
            }
            oDescription = new InnovaEditor("oDescription");
            oDescription.toolbarMode = 0;
            var newFields = [];

            $("#fields option").each(
                function () {
                    newFields.push([this.innerHTML, "{" + this.value.replace("email-", "") + "}"]);
                }
            );

            //Define custom tag selection
            oDescription.arrCustomTag = newFields;

            oDescription.features = ["Table", "AutoTable", "Guidelines", "Absolute", "Flash", "Media", "YoutubeVideo", "InternalLink",
                                    "Form", "Characters", "ClearAll", "BRK",
                                    "Cut", "Copy", "Paste", "PasteWord", "PasteText",
                                    "Undo", "Redo", "Hyperlink", "Bookmark", "Image",
                                    "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyFull",
                                    "Numbering", "Bullets", "Indent", "Outdent",
                                    "Line", "RemoveFormat", "BRK",
                                    "StyleAndFormatting", "TextFormatting", "ListFormatting",
                                    "BoxFormatting", "ParagraphFormatting", "CssText", "Styles",
                                    "CustomTag", "Paragraph", "FontName", "FontSize",
                                    "Bold", "Italic", "Underline", "Strikethrough", "Superscript", "Subscript",
                                    "ForeColor", "BackColor"]; // => Custom Button Placement

            oDescription.useTagSelector = true;
            oDescription.showResizeBar = true;
            oDescription.width = "100%";
            oDescription.REPLACE("DescDetail", "DescSpan");
            $($("#idContentoDescription")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' });
            $($("#idContentoDescription")[0].contentWindow.document).find('body').attr("style", "background-image:none;");

            setTimeout(function () { $($("#idContentoDescription")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' }); }, 1000);


        }

        function showEdit(obj) {
            if (obj) {
                $("#Message").show();
                $("#Creator").hide();
                if (!editorOn) {
                    cEdit(this);
                    editorOn = true;
                }

            }
            else {
                $("#Message").hide();
                $("#Creator").show();
            }

        }
    </script>
</head>
<body style="background: none; margin-left: 15px; background-color: #ffffff;">
    <form id="form1" runat="server">
    <div style="width: 100%;" id="Creator">
        <h2>
            Welcome To the Email Form wizard!</h2>
        <p>
            Here you can create custom forms that will be emailed to you or your vistors.<br />
        </p>
        <p>
            <br />
            <b>First we need to know a few things before we begin:</b></p>
        <table border="0" cellpadding="2" cellspacing="2" width="100%" class="myForm">
            <tr>
                <td width="35%">
                    Where should we send the filled out form?
                </td>
                <td>
                    <select id="rbtnlstEmailSend" title="If you select the Me Only option then the form's data will only be sent to you , if you choose the last option then a copy of the form will be sent to the vistor as well.">
                        <option>Select One..</option>
                        <option value="0">Me Only</option>
                        <option value="1">Vistors & Me</option>
                    </select>
                </td>
                <td>
                </td>
                <td rowspan="10" valign="top">
                    <h2 style="bottom: 0px; margin: 0; padding: 0;">
                        Fields</h2>
                    <select id="fields" multiple="multiple" style="min-width: 150px; height: 300px;">
                    </select>
                    <br />
                    <input type="button" value="Remove Item" onclick="removeItem();" />
                    <br />
                    <br />
                </td>
            </tr>
            <%--<tr>
                <td>
                    Do you require a secure capture lock(recommended for anti-spam)?
                </td>
                <td>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" ClientIDMode="Predictable">
                        <asp:ListItem Text="Yes" />
                        <asp:ListItem Text="No" />
                    </asp:RadioButtonList>
                </td>
            </tr>--%>
            <tr>
                <td>
                    Subject of the email:
                </td>
                <td>
                    <input id="subject" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Thank you message:
                </td>
                <td>
                    <input id="ThankMessage" value="" title="This text appears after the form has been sent" />
                </td>
            </tr>
            <tr>
                <td>
                    Name of send button(this is the button that will send the Email):
                </td>
                <td>
                    <input id="buttonName" value="" title="this text replaces the text that will be displayed in the button that will send the form" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>What Fields will you need?<br />
                        Please enter the name of the field and some text for the label which the client
                        will have to respond to:</b>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table border="0" cellpadding="2" cellspacing="2" width="100%">
                        <tr>
                            <td>
                                Field Name:
                            </td>
                            <td>
                                <input id="fldName" title="This text appears in the email message" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Field Label:
                            </td>
                            <td>
                                <input id="fldLabel" title="This text will appear next to the fields you create." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Field Validation:
                            </td>
                            <td>
                                <input type="checkbox" value="required" id="valrequired" title="This does not allow the form to be sent if this field is blank" />Must
                                be filled in<br />
                                <input type="checkbox" value="regex:email" id="valEmail" title="This ensures that the text within the fields is a valid email address" />Must
                                be an Email
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Field Type:
                            </td>
                            <td>
                                <select id="fldType" title="This option will allow you to choose between having a standard Text box for short inputs or a multi-line text box for longer peices information like comments.">
                                    <option value="text" selected="selected">Text Box</option>
                                    <option value="textarea">Multi-Line Text Box</option>
                                    <option value="freetext">Free text column</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="button" id="btnAdd" value="Add" onclick="AddFields();" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Add a custom response message?<br />
                                    This is optional and it basically sends an email to your visitors after they have
                                    completed the form.</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" onclick="showEdit(true);" value="Add" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <input type="button" onclick="buildHTML();" value="Generate My Form" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="Message" style="width: 100%; display: none;">
        <input type="button" value="Back" onclick="showEdit(false);" />
        <h2>
            Reponse Message:</h2>
        <p>
            Here you can set a custom message that will be sent to the visitor after the form
            has been completed.You may use the
            <img alt="" src="../assets/customtag.gif" />
            button to insert the fields you have created into the message.
        </p>
        <table border="0" cellpadding="2" cellspacing="2" width="100%" class="">
            <tr>
                <td>
                    <textarea id="DescDetail" clientidmode="Static" runat="server" style="display: none;"
                        name="DescDetail" rows="4" cols="30"></textarea>
                    <span id="DescSpan"></span>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
