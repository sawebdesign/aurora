﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.Innova.common {
    public partial class Modules : System.Web.UI.Page {
        public string moduleList;
        public string moduleHTML;
        public string moduleCat;
        protected void Page_Load(object sender, EventArgs e) {
            Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
            var query = from modules in entities.ClientModule
                        join moduleFeatures in entities.Module on modules.FeaturesModuleID equals moduleFeatures.ID
                        where modules.ClientSiteID == SessionManager.ClientSiteID
                        && moduleFeatures.isDefault == false
                        && moduleFeatures.FriendlyName != null
                        && moduleFeatures.DisplayOnSite == true
                        && modules.DeletedOn == null
                        orderby moduleFeatures.FriendlyName ascending
                        select moduleFeatures;

            var categories = from cats in entities.Category
                             where cats.ClientSiteID == SessionManager.ClientSiteID
                             && cats.DeletedBy == null 
                             && cats.DeletedOn == null
							 && (cats.IsHidden == null || cats.IsHidden == false)
                             select cats;

            var customModules = from custommodules in entities.CustomModules
                                where custommodules.ClientSiteID == SessionManager.ClientSiteID
                                && custommodules.DeletedBy == null
                                && custommodules.DeletedOn == null
                                select custommodules;

            foreach (Module module in query) {
                if (!module.Name.Contains("Detail") || module.Name.Contains("ListDetail")) {
                    string categoryList = string.Empty;
                    if (categories.Where(item => item.FeatureModuleID == module.ID).Count() > 0) {
                        var moduleCats = categories.Where(moduleCat => moduleCat.FeatureModuleID == module.ID);

                        categoryList += string.Format("<select id='{0}_option' onchange='setAttr(this);' style='display:'>", module.Name);
                        categoryList += string.Format("<option value='null'>No Filter</option>");
                        foreach (Category Item in moduleCats) {
                            if (module.Name == "GalleryImages" && Item.Description == "Images") {
                                categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);
                            }
                            else if (module.Name == "GalleryAudio" && Item.Description == "Audio") {
                                categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);
                            }
                            else if (module.Name == "GalleryImages" && Item.Description == "Video") {
                                categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);
                            }
                            else {
                                categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);
                            }

                        }
                        categoryList += "<select>";
                    }
                    else if (module.Name == "CustomModules") {

                        categoryList += string.Format("<select id='{0}_option' onchange='setAttr(this);' style='display:'>", module.Name);
                        categoryList += string.Format("<option value='null'>No Filter</option>");
                        foreach (Custom.Data.CustomModules Item in customModules) {
                            categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);

                        }
                        categoryList += "<select>";
					} else if(module.Name == "ThumbnailGallery") {

						categoryList += string.Format("<select multiple=\"multiple\" id='{0}_option' onchange='setModuleOptions(this,\"data-galleryids\",\"{0}\");' style='width:150px;'>", module.Name);
						
						//get the categories for the gallery images module
						var moduleCats = categories.Where(moduleCat => moduleCat.FeatureModuleID == 5);

						foreach(Category Item in moduleCats) {
							categoryList += string.Format("<option value='{0}'>{1}</option>", Item.ID, Item.Name);
						}
						categoryList += string.Format("</select>&nbsp;&nbsp;Layout&nbsp;&nbsp;<select id='{0}_layout' onchange='setModuleOptions(this,\"data-layout\",\"{0}\");'><option value='default'>Polaroid</option><option value='dropdown'>Drop Down</option></select>", module.Name);
					}

                    moduleList += string.Format("<tr><td><input type='radio' name='selector' onclick=\"currentItem=document.getElementById('{0}_container').innerHTML;\"/><u>{1}</u></td><td>{2}</td></tr>", module.Name, module.FriendlyName, categoryList);
					if(!module.IsClassBased) {
						moduleHTML += string.Format("<div id='{0}_container' style='display:none'><div id='{0}'><div style='COLOR: red;border: 1px solid #000000;' class='placeholder'>[{0} Placeholder DO NOT REMOVE!!]</div></div></div>", module.Name);
					} else {
						moduleHTML += string.Format("<div id='{0}_container' style='display:none'><div class='{0} incontent'><div style='COLOR: red;border: 1px solid #000000;' class='placeholder'>[{0} Placeholder DO NOT REMOVE!!]</div></div></div>", module.Name);
					}
                }

            }
        }
    }
}