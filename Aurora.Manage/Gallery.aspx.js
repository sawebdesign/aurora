﻿$("#GalleryIndex").ForceNumericOnly();
$("#GalleryDetails .help").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'focus', delayIn: 0, delayOut: 0 });

$("#contentAddEdit").show();
$("#contentAddEdit").css('height', '0px');
$("#editor").show();

scrollToElement("Messages", -20, 2);
if (saveCompleted) {
    DisplayMessage("Messages", "success", "data saved", 5000);
    saveCompleted = false;
    setTimeout(function () {
        DisplayMessage("Messages", "info", "Please fill in all the information below.", 5000);
    }, 6000);
}
else {
    DisplayMessage("Messages", "info", "Please fill in all the information below.", 5000);
}

$("#Image").hide();
$('#btn_image1').mouseover(
        function () {
            $("#Image").css("left", ($("#contentAddEdit .content").width() * 0.85) - 10);
            $("#Image").css("border", "2px dashed grey");
            $("#Image").css("position", "absolute");
            $("#Image").css("top", "55px");
            $("#Image").fadeIn(500);
            $("#Image").height($("#Image").height() / $("#Image").width * ($("#contentAddEdit .content").width() - ($("#contentAddEdit .content").width() * 0.85)));
            $("#Image").width($("#contentAddEdit .content").width() - ($("#contentAddEdit .content").width() * 0.85));

        }
    );

$('#btn_image1').mouseout(
    function () {
        $("#Image").fadeOut(500);
    }
    );