﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="EventsAED.aspx.cs" Inherits="Aurora.Manage.EventsAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="Innova/scripts/innovaeditor.js" type="text/javascript"></script>
    <script type="text/javascript" src="EventsAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <div>
        <div id="contentList">
            <!-- Begin one column window -->
            <div class="onecolumn">
                <div class="header">
                    <span>Event List</span>
                    <div style="float: right; margin-top: 10px; margin-right: 10px;">
                        <img src="Styles/images/add.png" class="help" alt="Create Event" title="Create Event"
                            style="cursor: pointer" onclick="newItem=true; getDetail();" />
                    </div>
                    <div style="float: left; margin-top: 10px; margin-right: 10px;">
                        <div>
                            &nbsp;
                            <select id="EventCat" class="sys-template" sys:attach="dataview" onchange="changeEventCat();$('#contentAddEdit').hide();">
                                <option value="{binding ID}">{binding Name}</option>
                            </select>
                            <a id="categoryEditFancy" href="/toolpages/CategoryAED.aspx?moduleID=4" mytitle="Edit Categories"
                                class="help">
                                <img src="Styles/images/folder_wrench.png" alt="Edit Categories" /></a>
                            <img src="Styles/images/script_add.png" onclick="addNewCat();" style="cursor: pointer;
                                display: none;" mytitle="Create a category" class="help" />
                        </div>
                    </div>
                    <div style="float: left; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                        &nbsp;<select id="eventType" onchange="changeEventCat();$('#contentAddEdit').hide();">
                            <option value="Active">Active</option>
                            <option value="Archived">Archived</option>
                        </select>
                    </div>
                </div>
                <br class="clear" />
                <div class="content">
                    <table class="data" width="100%" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th align="left">
                                    Title
                                </th>
                                <th align="left">
                                    Start Date
                                </th>
                                <th align="left">
                                    End Date
                                </th>
                                <th align="left">
                                    Occurance
                                </th>
                                <th align="right">
                                    Function
                                </th>
                            </tr>
                        </thead>
                        <tbody id="templateList" class="sys-template" sys:attach="dataview">
                            <tr>
                                <td width="40%">
                                    {binding Title}
                                </td>
                                <td width="20%">
                                    {binding StartDate, convert=getDate}
                                </td>
                                <td width="20%">
                                    {binding EndDate, convert=getDate}
                                </td>
                                <td width="10%">
                                    {binding Occurance}
                                </td>
                                <td align="right" width="10%">
                                    <a href="javascript:void(0);" onclick="newItem =false;getDetail(this);return false;"
                                        dataid="{binding ID}">
                                        <img src="../styles/images/icon_edit.png" alt="edit" class="tipper" mytitle="Edit" /></a>
                                    <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                        <img src="../styles/images/icon_delete.png" alt="delete" class="tipper" mytitle="Delete" /></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="html_body">
                    </div>
                    <!-- Begin pagination -->
                    <div id="Pagination">
                    </div>
                    <!-- End pagination -->
                </div>
            </div>
            <div id="Messages">
            </div>
            <asp:Panel ID="pnlAddEdit" runat="server" ClientIDMode="Static">
                <div id="contentAddEdit">
                </div>
                <p align="right">
                    <input type="button" id="btnSave" onclick="saveData();" value="Save" class="Login" />
                </p>
            </asp:Panel>
            <!-- End one column window -->
            <!-- End content -->
        </div>
    </div>
    <div id="AddNewCat">
        <div id="Add">
            <div class="inner">
                <!-- Begin one column window -->
                <div class="onecolumn">
                    <div class="header">
                        <span>Add New Event Category</span>
                    </div>
                    <br class="clear" />
                    <div id="AddMsg">
                    </div>
                    <div class="content">
                        <table width="100%" id="addData">
                            <tr>
                                <td>
                                    Name:
                                </td>
                                <td>
                                    <input type="text" id="CatName" valtype="required" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description:
                                </td>
                                <td>
                                    <input type="text" id="CatDescription" valtype="required" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Order
                                </td>
                                <td>
                                    <input type="text" id="CatOrder" valtype="required" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" value="Add" onclick="AddEventCategory();" />
                                    <input type="button" value="Done" onclick="document.location.reload()" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
