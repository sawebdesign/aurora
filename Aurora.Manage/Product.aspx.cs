﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage {
    public partial class Product : System.Web.UI.Page{
        private Custom.Data.AuroraEntities context = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e) {
            if (Request["ProductID"] != null)
            {
                Utils.CheckSession();
                int productID;
                int.TryParse(Request["ProductID"], out productID);
                var productDetail = (from products in context.Product 
                                     //join colourSizeProduct in context.ColourSizeProductGallery on products.ID equals colourSizeProduct.ProductID
									 join productCategory in context.Category on products.CategoryID equals productCategory.ID
                                     join brandProduct in context.BrandProduct on productID equals brandProduct.ProductID 
                                    where products.ID == productID
                                    select new { brandProduct, products, productCategory}).SingleOrDefault();
                
                var colourSizeProductGallery = from cspg in context.ColourSizeProductGallery
                                               where cspg.ProductID == productID
                                               select cspg;

                if (productDetail != null)
                {
                    ID.Value = productDetail.products.ID.ToString();
                    ClientSiteID.Value = productDetail.products.ClientSiteID.ToString();
                    Code.Value = productDetail.products.Code;
                    SizeID.Value = productDetail.products.SizeID.Value.ToString();
                    CustomXML.Value = productDetail.products.CustomXML;
                    BrandID.Value = productDetail.brandProduct.BrandID.ToString();
                    Name.Value = productDetail.products.Name;
                    DetailText.Value = productDetail.products.Description;
                    Specification.Value = productDetail.products.Specification;
                    IsFeatured.Checked = productDetail.products.IsFeatured;
                    ShortDescription.Value = productDetail.products.Short_Description;
                    prodCategoryID.Value = productDetail.products.CategoryID.ToString();
					prodCategoryName.Value = productDetail.productCategory.Name;
                    Price.Value = productDetail.products.Price.ToString();
                    isZeroShipping.Checked = productDetail.products.isZeroShipping ?? false;
                    isFixedShipping.Checked = productDetail.products.isFixedShipping ?? false; ;
					ShippingPriceLoc.Value = Utils.ConvertDBNull(productDetail.products.ShippingPriceLoc, string.Empty);
                    ShippingPriceInt.Value = Utils.ConvertDBNull(productDetail.products.ShippingPriceInt, string.Empty);
                    DownloadURL.Value = Utils.ConvertDBNull(productDetail.products.DownloadURL, string.Empty);
                    OrderIndex.Value = Utils.ConvertDBNull(productDetail.products.OrderIndex,string.Empty);
                    isHidden.Checked = productDetail.products.isHidden;
                    if (Request["parent"] != null && Request["parent"] != String.Empty) {
                        ParentID.Value = Request["parent"];
                        ID.Value = "-1";
                    } else
                    {
                        ParentID.Value = productDetail.products.ParentID.ToString();
                    }

                    foreach (ColourSizeProductGallery sizeProductGallery in colourSizeProductGallery)
                    {
                        ColourID.Value += sizeProductGallery.ColourID+",";
                    }

                    IsTemplate.Checked = productDetail.products.isTemplate;
                    StockAvailablity.Value = productDetail.products.Availablity;
                    Total.Value = Utils.ConvertDBNull(productDetail.products.Total,string.Empty);
                    Weight.Value = Utils.ConvertDBNull(productDetail.products.Weight, string.Empty);
                    Height.Value = Utils.ConvertDBNull(productDetail.products.Height, string.Empty);
                    Width.Value = Utils.ConvertDBNull(productDetail.products.Width, string.Empty);
                    Length.Value = Utils.ConvertDBNull(productDetail.products.Length, string.Empty);
                }
                else
                {
                    prodCategoryID.Value = "0";
					prodCategoryName.Value = "";
                }
            }
        }
    }
}