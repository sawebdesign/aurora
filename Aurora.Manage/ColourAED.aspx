﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="ColourAED.aspx.cs" Inherits="Aurora.Manage.ColourAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript" type="text/javascript" src="ColourAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <link rel="stylesheet" media="screen" type="text/css" href="/Styles/colorpicker.css" />
    <script type="text/javascript" src="/Scripts/colorpicker.js"></script>
    
    <div class="onecolumn">
        <div class="header">
            <span>Colours & Textures List</span>
            <div style="float:left;margin-top:7px">
            &nbsp;
               &nbsp;
                <select id="Sizes" sys:attach="dataview" class="sys-template" onchange="loadColorList(this.value)">
                    <option value="{{ID}}">
                        {{Description}}
                    </option>
                </select>
            </div>
             <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <img src="Styles/images/add.png" dataid="-1" alt="Create Product" title="Create Product"
                        style="cursor: pointer" onclick="getColourDetails(this.getAttribute('dataid'));" />
                </div>
        </div>
        <br class="clear" />
        <div class="content" id="grid">
            <table class="data" width="100%" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th align="left">Name</th>
                        <th align="left">
                            Colour
                        </th>
                        <th align="left">
                            Texture
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" sys:attach="dataview" id="template" >
                  <tr>
                        <td>{{Name == null ? "" : Name}}</td>
                        <td color="{{'#'+HexValue}}"  width="5%">
                        </td>
                        <td image="{{ImageUrl}}" align="right">
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);"  dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" dataid="{{ID}}" onclick="getColourDetails(this.getAttribute('dataid')); return false;" /></a>
                            <a href="javascript:void(0);"  dataid="{{ID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" onclick="deleteItem(this.getAttribute('dataid')); return false;"/></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="Pagination"></div>
    </div>
        <div id="Messages">
        </div>
   <div id="Edit"></div>
</asp:Content>
