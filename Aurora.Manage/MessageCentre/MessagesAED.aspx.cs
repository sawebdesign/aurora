﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.MessageCentre {
    public partial class MessagesAED : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (!Page.IsPostBack) {
                using (AuroraEntities context = new AuroraEntities()) {
                    categorySelect.DataTextField = "Name";
                    categorySelect.DataValueField = "ID";
                    categorySelect.DataSource = (from cats in context.Role
                                                 where cats.DeletedOn == null
                                                 && cats.ClientSiteID == SessionManager.ClientSiteID
                                                 orderby cats.OrderIndex
                                                 select cats);
                    categorySelect.DataBind();
                    categorySelect.Items.Insert(0, new ListItem { Text = "System Events", Value = "-1" });
                }
            }
        }
    }
}