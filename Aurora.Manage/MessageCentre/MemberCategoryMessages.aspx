﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberCategoryMessages.aspx.cs" Inherits="Aurora.Manage.MessageCentre.MemberCategoryMessages" %>

<%@ Import Namespace="Aurora.Custom" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Message Details</title>
    <script src="/Scripts/custom_black.js" type="text/javascript"></script>
    <script type="text/javascript" src="MemberCategoryMessages.aspx.js"></script>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
</head>
<body>
    <div class="onecolumn">
        <div class="header">
            <span>Edit</span>
        </div>
        <br class="clear" />
        <div class="content">
            <input type="hidden" runat="server" id="ID" />
            <input type="hidden" runat="server" id="MemberCategoryID" value="0" />
            <input type="hidden" runat="server" id="PredictedData" />
            <table width="100%">
                <tr>
                    <td width="10%">
                        Category Name 
                    </td>
                    <td width="90%" align="left">
                        <label runat="server" id="CategoryName"></label>
                    </td>
                </tr>
                <tr>
                    <td width="10%">
                        Name 
                    </td>
                    <td width="90%" align="left">
                        <input type="text" runat="server" id="Name" valtype="required"/>
                    </td>
                </tr>
                <tr>
                    <td width="10%">
                        Trigger 
                    </td>
                    <td width="90%" align="left">
                        <input type="text" runat="server" id="Day" valtype="required"/>
                        <span>Days </span>
                        <span id="reverseLabel">After Commencement</span><input type="checkbox" runat="server" id="IsReversed" />
                    </td>
                </tr>
                <tr>
                    <td width="10%">
                        Subject 
                    </td>
                    <td width="90%" align="left">
                        <input type="text" runat="server" id="Subject" style="width: 96%;" valtype="required"/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Message:
                    </td>
                    <td width="70%" align="left">
                        <textarea id="MessageHTML" runat="server" style="display: none;" rows="1"></textarea>
                        <span id="editorSpan"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%-- 
                            Added for future use of custom fields
                        --%>
                        <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                            <cui:XmlPanel ID="ContentXml" runat="server">
                            </cui:XmlPanel>
                        </cui:ObjectPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
