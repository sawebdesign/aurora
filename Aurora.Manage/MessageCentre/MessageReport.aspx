﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="MessageReport.aspx.cs" Inherits="Aurora.Manage.MessageCentre.MessageReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Message Report</span>
        </div>
        <br class="clear" />
        <div class="content">
            <table class="data" cellpadding="0" cellspacing="0" border="0" width="100%">
                <thead>
                    <tr>
                        <th align="left">
                            Name
                        </th>
                        <th align="left">
                            Module
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" id="rolesgrid">
                    <tr>
                        <td>
                            {{eventData.EventType}}
                        </td>
                        <td>
                            {{module.FriendlyName}}
                        </td>
                        <td align="right">
                            <a href="javascript:void(0);" dataid="{{message.ID}}" onclick="messageCentre.edit($(this).attr('dataid'));return false;"
                                dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                            <a href="javascript:void(0);" dataid="{{message.ID}}" onclick="messageCentre.edit($(this).attr('dataid'),true);return false;"
                                dataid="{{ID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="Messages">
    </div>
    <div id="contentEditAdd">
    </div>
</asp:Content>
