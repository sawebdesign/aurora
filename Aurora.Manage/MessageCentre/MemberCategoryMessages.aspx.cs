﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Custom;

namespace Aurora.Manage.MessageCentre {
    public partial class MemberCategoryMessages : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            long MessageID;
            long CategoryID;
            bool IsDelete;
            AuroraEntities entities = new AuroraEntities();
            long.TryParse(Request["MessageID"], out MessageID);
            long.TryParse(Request["CategoryID"], out CategoryID);
            bool.TryParse(Request["IsDelete"], out IsDelete);
            var Category = (from cats in entities.Role
                            where cats.ClientSiteID == SessionManager.ClientSiteID
                            && cats.ID == CategoryID
                            select cats).FirstOrDefault();
            if (MessageID > 0) {
                var messageData = (from messages in entities.MemberCategoryMessage
                                   where messages.ID == MessageID
                                   select messages).SingleOrDefault();
                if (!IsDelete) {
                    if (CategoryID != messageData.MemberCategoryID) {
                        Category = entities.Role.Where(w => w.ClientSiteID == SessionManager.ClientSiteID && w.ID == messageData.MemberCategoryID).FirstOrDefault();
                    }
                    ID.Value = messageData.ID.ToString();
                    Name.Value = messageData.Name;
                    Day.Value = messageData.Day.ToString();
                    IsReversed.Checked = messageData.IsReversed ?? false;
                    Subject.Value = messageData.Subject;
                    MessageHTML.Value = messageData.HTML;
                } else {
                    messageData.DeletedBy = SessionManager.UserID;
                    messageData.DeletedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
            else {
                ID.Value = "0";
                Day.Value = "0";
            }
            MemberCategoryID.Value = CategoryID.ToString();
            PredictedData.Value = "[[\"Category Name\", \"{categoryname}\"],[\"Site Name\", \"{sitename}\"],[\"First Name\", \"{firstname}\"],[\"Last Name\", \"{lastname}\"],[\"Email\", \"{email}\"]]";
            if (Category != null) {
                CategoryName.InnerHtml = Category.Name;
            }
        }
    }
}