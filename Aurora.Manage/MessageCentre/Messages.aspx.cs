﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Custom;

namespace Aurora.Manage.MessageCentre {
    public partial class Messages : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            long MessageID;
            AuroraEntities entities = new AuroraEntities();
            long.TryParse(Request["MessageID"], out MessageID);

            if (MessageID > 0) {
                var messageData = (from messages in entities.Message
                                   where messages.ID == MessageID
                                   select messages).SingleOrDefault();

                ID.Value = messageData.ID.ToString();
                InsertedBy.Value = messageData.InsertedBy.ToString();
                InsertedOn.Value = messageData.InsertedOn.ToString();
                MessageEventID.Value = messageData.MessageEventID.ToString();
                MessageHTML.Value = messageData.MessageHTML;
                FeatureModuleID.Value = messageData.FeatureModuleID.ToString();
                PredictedData.Value = (from evtData in entities.MessageEvent where evtData.ID == messageData.MessageEventID select evtData).SingleOrDefault().PredictedData;

            }
            else {
                ID.Value = "0";
                FeatureModuleID.Value = "0";
                InsertedBy.Value = SessionManager.UserID.ToString();
                InsertedOn.Value = DateTime.Now.ToString();
                MessageEventID.Value = "0";
            }

            ClientSiteID.Value = SessionManager.ClientSiteID.ToString();
        }
    }
}