﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="MessagesAED.aspx.cs" Inherits="Aurora.Manage.MessageCentre.MessagesAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="MessagesAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"
    type="text/javascript"></script>
    <div>
        <div class="onecolumn">
            <div class="header">
                <span>Messages</span>
                <div style="float:left; margin-top: 4px; margin-left: 10px;">
                    <select id="categorySelect" runat="server" clientidmode="Static" ></select>
                </div>
                <div style="float: right; margin-top: 10px; margin-right: 10px;">
                    <div>
                        &nbsp;
                        <img alt="edit" src="/Styles/images/add.png" onclick="messageCentre.addMessage();" style="cursor: pointer;"
                            mytitle="Create a Message" class="help" />
                    </div>
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <table class="data system" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <thead>
                        <tr>
                            <th align="left">
                                Name
                            </th>
                            <th align="left">
                                Module
                            </th>
                            <th align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody class="sys-template" id="rolesgrid">
                        <tr>
                            <td>
                                {{eventData.EventType}}
                            </td>
                            <td>
                                {{module.FriendlyName}}
                            </td>
                            <td align="right">
                                <a href="javascript:void(0);" dataid="{{message.ID}}" onclick="messageCentre.edit($(this).attr('dataid'));return false;"
                                    dataid="{{ID}}">
                                    <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                                <a href="javascript:void(0);" dataid="{{message.ID}}" onclick="messageCentre.edit($(this).attr('dataid'),true);return false;"
                                    dataid="{{ID}}">
                                    <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="data member" cellpadding="0" cellspacing="0" border="0" width="100%" style="display:none;">
                    <thead>
                        <tr>
                            <th align="left">
                                Name
                            </th>
                            <th align="left">
                                Day
                            </th>
                            <th align="right">
                                Function
                            </th>
                        </tr>
                    </thead>
                    <tbody class="sys-template" id="memberCategoryGrid">
                        <tr>
                            <td>
                                {{Name}}
                            </td>
                            <td>
                                {{Day || 0}} {{(IsReversed ? "Before Expiry" : "After Commencement")}}
                            </td>
                            <td align="right">
                                <a href="javascript:void(0);" dataid="{{ID}}" catid="{{MemberCategoryID}}" onclick="messageCentre.categoryedit(this);return false;">
                                    <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                                <a href="javascript:void(0);" dataid="{{ID}}" catid="{{MemberCategoryID}}" onclick="messageCentre.categoryedit(this,true);return false;">
                                    <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete" /></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="Messages">
        </div>
        <div id="contentEditAdd">
        </div>
        <div style="float: right;">
            <input id="btnSave" style="display: none;" onclick="messageCentre.chooseUpdate();" type="button"
                value="Save" />
        </div>
    </div>
</asp:Content>
