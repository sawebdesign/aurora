﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="Aurora.Manage.MessageCentre.Messages" %>

<%@ Import Namespace="Aurora.Custom" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Message Details</title>
    <script src="/Scripts/custom_black.js" type="text/javascript"></script>
    <script type="text/javascript" src="Messages.aspx.js"></script>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
</head>
<body>
    <div class="onecolumn">
        <div class="header">
            <span>Edit</span>
        </div>
        <br class="clear" />
        <div class="content">
            <input type="hidden" runat="server" id="ID" />
            <input type="hidden" runat="server" id="FeatureModuleID" />
            <input type="hidden" runat="server" id="InsertedOn" />
            <input type="hidden" runat="server" id="InsertedBy" />
            <input type="hidden" runat="server" id="ClientSiteID" />
            <input type="hidden" runat="server" id="MessageEventID" value="0" />
            <input type="hidden" runat="server" id="PredictedData" />
            <table width="100%">
                <tr>
                    <td width="10%">
                        Event Type:
                    </td>
                    <td width="90%" align="left">
                        <select name="EventID" id="EventID" onchange="eventTypes.change(this);"  class="sys-template">
                        <option value="{{eventmsg.ID}}" ftmid="{{module.ID}}" prdata="{{eventmsg.PredictedData}}">
                            {{module.FriendlyName}} : {{eventmsg.EventType}}
                        </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Message:
                    </td>
                    <td width="70%" align="left">
                        <textarea id="MessageHTML" runat="server" style="display: none;" rows="1"></textarea>
                        <span id="editorSpan"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%-- 
                            Added for future use of custom fields
                        --%>
                        <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                            <cui:XmlPanel ID="ContentXml" runat="server">
                            </cui:XmlPanel>
                        </cui:ObjectPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
