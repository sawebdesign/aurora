﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />

var globals = new GlobalVariables.GlobalVar();
var deleteItem = false;
$(function () {
    messageCentre.display($('#categorySelect').val());
    $('#categorySelect').change(function () {
        messageCentre.display($(this).val());
    });
    $(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
});


var messageCentre = {
    display: function (id) {
        id = id || $('#categorySelect').val();
        $(".content").block({ message: null });
        if (id == -1) {
            $('table.data.system').show();
            $('table.data.member').hide();
            Aurora.Manage.Service.AuroraWS.GetSytemMessagesForClient(messageCentre.response, onError, "rolesgrid");
        } else {
            $('table.data.system').hide();
            $('table.data.member').show();
            Aurora.Manage.Service.AuroraWS.GetMemberCategoryMessagesForClient(id, messageCentre.response, onError, "memberCategoryGrid");
        }
    },
    response: function (result, template) {
        if (messageCentre.dataSet[template] == null) {
            messageCentre.dataSet[template] = $create(Sys.UI.DataView, {}, {}, {}, $get(template));
        }
        if (result.Data.length > 0) {
            messageCentre.dataSet[template].set_data(result.Data);
            $(".content").unblock();
        }
        else {
            messageCentre.dataSet[template].set_data(null);
            DisplayMessage("Messages", "warn", "There are no items to display. To add an item please click the green add button.");
            $(".content").unblock();
        }

    },
    dataSet: {},
    edit: function (messageID, isDelete) {
        if (isDelete) {
            if (!confirm("Do you wish to permanantly delete this message?", "Delete?")) {
                return;
            }
        }

        $(".content").block({ message: null });
        if (!isDelete) {
            $("#contentEditAdd").show();
        }
        else {
            $("#contentEditAdd").hide();
        }
        var onSuccess = function (success, status, xhr) {
            if (status === "success" && isDelete) {
                deleteItem = true;
            }
            if (status == "success") {
                DisplayMessage("Messages", "info", "Please fill in the form below", 7000);
                $("#btnSave").show();

            }
            else {
                DisplayMessage("Messages", "error", "An error occured while the page was loading, please try again");
                $("#contentEditAdd").hide();
            }
            $(".content").unblock();
        };

        $("#contentEditAdd").load("/MessageCentre/Messages.aspx", { MessageID: messageID }, onSuccess);
    },
    categoryedit: function (anchor, isDelete) {
        var $this = $(anchor),
            MessageID = $this.attr('dataid')
            CategoryID = $this.attr('catid');
        
        if (isDelete) {
            if (!confirm("Do you wish to permanantly delete this message?", "Delete?")) {
                return;
            }
        }

        $(".content").block({ message: null });
        if (!isDelete) {
            $("#contentEditAdd").show();
        }
        else {
            $("#contentEditAdd").hide();
        }
        var onSuccess = function (success, status, xhr) {
            if (status === "success" && isDelete) {
                //deleteItem = true;
                DisplayMessage("Messages", "info", "Message Deleted", 7000);
                $("#contentEditAdd").hide();
                $("#btnSave").hide();
                messageCentre.display();
                return;
            }
            if (status == "success") {
                DisplayMessage("Messages", "info", "Please fill in the form below", 7000);
                $("#btnSave").show();

            }
            else {
                DisplayMessage("Messages", "error", "An error occured while the page was loading, please try again");
                $("#contentEditAdd").hide();
            }
            $(".content").unblock();
        };

        $("#contentEditAdd").load("/MessageCentre/MemberCategoryMessages.aspx", { MessageID: MessageID, CategoryID: CategoryID, IsDelete: isDelete }, onSuccess);
    },
    update: function (isDelete) {
        var data = $("#contentEditAdd input,#contentEditAdd select,#contentEditAdd textarea");
        var message = new AuroraEntities.Message();
        eventTypes.change($("#EventID"));
        globals.populateEntityFromForm(message, data);
        var response = function (result) {
            if (result.Result && result.Data.constructor !== String && !isDelete) {
                DisplayMessage("Messages", "success", "Message saved", 7000);
                $("#EventID").attr("disabled", "disabled");
                messageCentre.display();
            }
            else if (result.Data.constructor === String) {
                DisplayMessage("Messages", "error", result.Data.replace("Error: ", ""));
            }
            else if (isDelete) {
                DisplayMessage("Messages", "warn", "Message removed", 7000);
                $("#btnSave").hide();
                messageCentre.display();
            }
            else {
                DisplayMessage("Messages", "error", "An unexpected error occured, ", 7000);
            }
        };
        message.MessageHTML = oEdit1.getHTMLBody();
        if (isDelete) {
            message.DeletedOn = new Date();
            message.DeletedBy = 0;
        }
        Aurora.Manage.Service.AuroraWS.UpdateSystemMessage(message, response, onError);
    },
    categoryUpdate: function (isDelete) {
        if (validateForm("contentEditAdd", true, "Messages", false)) {

        } else {
            isDelete = isDelete || false;
            var data = $("#contentEditAdd");
            var message = new AuroraEntities.MemberCategoryMessage();
            data.mapToObject(message, "ID");
            //globals.populateEntityFromForm(message, data);
            $("#contentEditAdd").hide();
            var response = function (result) {
                if (result.Result && result.Data.constructor !== String && !isDelete) {
                    DisplayMessage("Messages", "success", "Message saved", 7000);
                    $("#EventID").attr("disabled", "disabled");
                    messageCentre.display();
                }
                else if (result.Data.constructor === String) {
                    DisplayMessage("Messages", "error", result.Data.replace("Error: ", ""));
                    $('#contentEditAdd').show();
                }
                else if (isDelete) {
                    DisplayMessage("Messages", "warn", "Message removed", 7000);
                    $("#btnSave").hide();
                    messageCentre.display();
                }
                else {
                    DisplayMessage("Messages", "error", "An unexpected error occured, ", 7000);
                    $("#contentEditAdd").show();
                }
            };
            message.HTML = oEdit1.getHTMLBody();
            Aurora.Manage.Service.AuroraWS.UpdateMemberCategoryMessage(message, response, onError);
        }
    },
    toggleTrigger: function () {
        if ($('#IsReversed').is(':checked')) {
            $('#reverseLabel').html('Before Expiry');
        } else {
            $('#reverseLabel').html('After Commencement');
        }
    },
    addMessage: function () {
        if ($('#categorySelect').val() == -1) {
            messageCentre.edit(0);
        } else {
            messageCentre.categoryedit({ dataid: 0, catid: $('#categorySelect').val() });
        }
    },
    chooseUpdate: function () {
        if ($('#MemberCategoryID').length > 0) {
            messageCentre.categoryUpdate();
        } else {
            messageCentre.update();
        }
    }
};

var eventTypes = {
    display: function () {
        var response = function (result) {
            if (eventTypes.dataSet == null) {
                eventTypes.dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("EventID"));
            }
            else {
                eventTypes.dataSet.dispose();
                eventTypes.dataSet = null;
                eventTypes.dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("EventID"));
            }

            eventTypes.dataSet.set_data(result.Data);

            if (Number($("#MessageEventID").val()) > 0) {
                $("#EventID").val($("#MessageEventID").val());
                $("#EventID").attr("disabled", "disabled");
            }
            if (deleteItem) {
                messageCentre.update(true);
                deleteItem = false;
            }
        };


        Aurora.Manage.Service.AuroraWS.GetSystemMessageEventsForModules(0, response, onError);

    },
    dataSet: null,
    change: function (elem) {
        $('#MessageEventID').val($(elem).val());
        $('#FeatureModuleID').val($(elem).children('option').filter(":selected").attr('ftmid'));
        $('#PredictedData').val($(elem).children('option').filter(":selected").attr('prdata'));
        //reload editor
        var content = oEdit1.getHTMLBody();
        $("#editorSpan").html("");
        oEdit1 = null;
        InitEditor("MessageHTML", "editorSpan", false, eval($("#PredictedData").val()));
        oEdit1.putHTML(content);

    }
};