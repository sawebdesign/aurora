﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />
$(function () {
    $(".tipsy").remove();
    $(".help").tipsy({ gravity: 'e', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    eventTypes.display();

    InitEditor("MessageHTML", "editorSpan", false, eval($("#PredictedData").val()),true);

});