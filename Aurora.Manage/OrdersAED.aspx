﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="OrdersAED.aspx.cs" Inherits="Aurora.Manage.OrdersAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script language="javascript" src="OrdersAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
<div class="onecolumn">
                <div class="header">
                    <span>Enquiry List</span>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                        <select id="currentFilter" onchange="getOrders(this.value);">
                            <option value="false">New Enquiries</option>
                            <option value="true">Processed Enquiries</option>
                        </select>
                    </div>
                </div>
                <br class="clear" />
                <div class="content">
                    <table class="data" width="100%" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th align="left">
                                    Visitor Name
                                </th>
                                <th align="left">
                                    Company
                                </th>
                                <th align="left">
                                    Total Quantity
                                </th>
                                <th align="left">
                                    Recieved Date
                                </th>
                                <th align="right">
                                    Function
                                </th>
                            </tr>
                        </thead>
                        <tbody id="templateList" class="sys-template" sys:attach="dataview">
                            <tr>
                                <td width="30%">
                                    {{ClientFirsName +' ' +ClientLastName }}
                                </td>
                               
                                <td width="10%">
                                    {{CompanyName}}
                                </td>
                               <td width="10%">
                                    
                                </td>
                                <td width="10%">
                                    {{formatDateTime(InsertedOn,"dd-MM-yyyy")}}
                                </td>
                                
                                <td align="right" width="10%">
                                    <a href="javascript:void(0);" onclick="getOrderDetails(this.getAttribute('dataid'));return false;" dataid="{{ID}}">
                                        <img src="../styles/images/icon_edit.png"  class="help" mytitle="Edit" /></a>
                                    <a href="javascript:void(0);" onclick="deleteItem(this.getAttribute('dataid'));return false;" dataid="{{ID}}">
                                        <img src="../styles/images/icon_delete.png"  class="help" mytitle="Delete" /></a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <div id="html_body">
                    </div>
                    <!-- Begin pagination -->
                    <div id="Pagination">
                    </div>
                    <!-- End pagination -->
                </div>
 

            </div>
            <div id="Messages">
            </div>
            <asp:Panel ID="pnlAddEdit" runat="server" ClientIDMode="Static">
                <div id="contentAddEdit">
                </div>
               
            </asp:Panel>
            <!-- End one column window -->
            <!-- End content -->
        
</asp:Content>
