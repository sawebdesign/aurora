﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Size.aspx.cs" Inherits="Aurora.Manage.Size" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" src="Size.aspx.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Edit Size</span>
        </div>
        <br class="clear" />
        <div class="content">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <input type="hidden" id="ID" runat="server" />
            <tbody>
                <tr>
                    <td>Name:</td>
                    <td><input id="Description" name="Description" runat ="server" valtype="required"/></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
    <input type="button" value="Save" onclick="Save();"/>
    </form>
</body>
</html>
