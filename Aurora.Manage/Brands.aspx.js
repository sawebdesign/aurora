﻿//#region refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion


//#region Init
$(function () {
    DisplayMessage("Messages", "info", "Please Complete the form below", 7000);
    $("#Messages").insertBefore("#Edit");
    scrollToElement("Messages", -20, 2);
    if (!$(".data").is(":visible")) {
        $("#grid").toggle(500);
    }
});
//#endregion

//#region Data Manipulation
function Save() {
    $("#Edit").block({ message: null });
    var brandEntity = new AuroraEntities.Brand();

    var success = function () {
        DisplayMessage("Messages", "success", "The Brand has been updated", 7000);
        loadBrands();
        $("#Edit").hide();
        $("#Edit").unblock();
    };
    brandEntity.ClientSiteID = 0;
    brandEntity.DeletedBy = 0;
    brandEntity.Description = $("#Description").val();
    brandEntity.Name = $("#Name").val();
    brandEntity.ID = $("#ID").val();
    brandEntity.InsertedBy = 0;
    brandEntity.Inactive = false;
    brandEntity.InsertedOn = new Date();
    if (brandEntity.ID === "") {
        brandEntity.ID = -1;
    }
    else {
        brandEntity.EntityKey = new Object();
        brandEntity.EntityKey.EntityContainerName = "AuroraEntities";
        brandEntity.EntityKey.EntitySetName = "Brand";
        brandEntity.EntityKey.IsTemporary = false;
        brandEntity.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();
        objKey.Key = "ID";
        brandEntity.ID = brandEntity.ID == "" ? -1 : parseFloat(brandEntity.ID);
        objKey.Value = parseFloat(brandEntity.ID) ? parseFloat(brandEntity.ID) : -1;
        brandEntity.EntityKey.EntityKeyValues[0] = objKey;
    }
    Aurora.Manage.Service.AuroraWS.UpdateBrands(brandEntity, success, onError);
}

function deleteItem(id) {
    var ans = false;

    if ($("#filter").val() == "Active") {
        ans = confirm("By deleting this all products linked to this brand will be disassociated.");
    }
    else {
        ans = confirm("By deleting this all products linked to this brand will be disassociated.");
    }
}
//#endregion