﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage {
    public partial class Dashboard : System.Web.UI.Page {
        public string domainNames;
        protected void Page_Load(object sender, EventArgs e) {
            Custom.Data.AuroraEntities entity = new Custom.Data.AuroraEntities();

            var domains = from domain in entity.Domain
                          where domain.ClientSiteID == SessionManager.ClientSiteID
                          select domain;

            domainNames = "<ul>";
            foreach (var domainentity in domains) {
                domainNames += string.Format("<li>{0}</li>", domainentity.DomainName);
            }

            domainNames += "</ul>";

            //get dynamic content
            var page = (from pages in entity.PageContent
                        where pages.LongTitle == "DashboardText"
                              && pages.ClientSiteID == 10000
                              && pages.isArchived == false
                        select pages
                       ).SingleOrDefault();

            if (page != null)
            {
                dynamicContent.InnerHtml = page.PageContent1;
            }
        }
    }
}