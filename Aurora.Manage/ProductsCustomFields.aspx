﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="ProductsCustomFields.aspx.cs" Inherits="Aurora.Manage.ProductsCustomFields" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Styles/DropDown.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="ProductsCustomFields.aspx.js?cache=<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <script type="text/javascript" src="/Scripts/fg.menu.js"></script>
    <link type="text/css" href="/Styles/fg.menu.css" media="screen" rel="stylesheet" />
    <link type="text/css" href="/Styles/ui.all.css" media="screen" rel="stylesheet" />
    <div class="onecolumn">
        <div class="header">
            <span>Product Custom Fields</span>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img src="Styles/images/brick_add.png" dataid="-1" alt="Create New Custom Field"
                    title="Create New Custom Field" style="cursor: pointer" onclick="customFields.addNewField(); return false;" />
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <div id="MessageWarning">
            </div>
            <div id="Messages">
            </div>
            <div id="CustomFields">
            </div>
        </div>

    </div>
            <p style="margin-right: 20px;" align="right">
            <input type="button" id="btnSave" onclick="customFields.saveFields();" value="Save" class="Login"
                style="" />
        </p>
</asp:Content>
