﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage {
    public partial class WebForm1 : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            long CategoryID = Utils.ConvertDBNull(Request["categoryID"], 0);
            if (CategoryID != 0) {
                AuroraEntities context = new AuroraEntities();
                string path = Server.MapPath("~/ClientData/10079/Bulk");
                DirectoryInfo dir = new DirectoryInfo(path);
                FileInfo[] files = dir.GetFiles();
                int counter = 1;
                foreach (FileInfo currentFile in files) {
                    string noExtension =  currentFile.Name.Replace(".jpg", string.Empty);
                    var newImage = new Custom.Data.Gallery {
                        CategoryID = CategoryID,
                        Description =noExtension.Length > 250 ?noExtension.Substring(0,250):noExtension,
                        GalleryIndex = counter,
                        MediaType = ".jpg",
                        Name = noExtension.Length > 50 ?noExtension.Substring(0,50):noExtension,
                        InsertedOn = DateTime.Now,
                        InsertedBy = 10000
                    };

                    context.Gallery.AddObject(newImage);
                    context.SaveChanges();
                    string galleryImageName = String.Format("{0}\\gallery_01_{1}.jpg", Server.MapPath("~/ClientData/10079/Uploads/"), newImage.ID);
                    string thumbgalleryImageName = String.Format("{0}\\sm_gallery_01_{1}.jpg", Server.MapPath("~/ClientData/10079/Uploads/"), newImage.ID);
                    File.Copy(currentFile.FullName, galleryImageName);
                    Utils.ResizeImageFromFile(galleryImageName, 800, galleryImageName);
                    Utils.ResizeImageFromFile(galleryImageName, 200, thumbgalleryImageName);
                    File.Delete(currentFile.FullName);

                    counter++;
                    countFiles.InnerHtml = counter.ToString();
                }
            } else {
                Response.Write("No category id specified");
            }
        }
    }
}