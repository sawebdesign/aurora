﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Banner.aspx.cs" Inherits="Aurora.Manage.Banner" %>

<%@ Import Namespace="Aurora.Custom" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <script type="text/javascript" src="Banner.aspx.js">
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <!-- Begin one column window -->
    <div class="onecolumn">
        <div class="header">
            <span>Banner</span>
        </div>
        <br class="clear" />
        <div class="content" style="padding-left: -15px;" id="BannerForm">
            <input type="hidden" id="BannerID" runat="server" value="" />
            <input type="hidden" id="IsHtml" runat="server" />
            <input type="hidden" id="ClientSiteID" runat="server" />
            <input type="hidden" id="TypeName" runat="server" />
            <img src="" id="bannerImage" runat="server" />
            <table cellspacing="5" cellpadding="0" border="0" width="100%">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <h2>
                                General</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <input type="text" runat="server" value="" mytitle="The name you wish to associate with this banner"
                                class="text help" size="30" id="Name" valtype="required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Redirect URL:
                        </td>
                        <td>
                            <input type="text" runat="server" mytitle="A URL you wish the banner to redirect the user when it has been clicked on,for outside sites please use http:// before the address eg.http://www.example.com"
                                value="#" class="text help" size="30" id="RedirURL" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Banner Zone (Size):
                        </td>
                        <td>
                            <select id="XY" onchange="selectSize()" class="help" mytitle="This will set a predefined size to your banners">
                            </select>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Y size (height):
                        </td>
                        <td>
                            <input type="text" runat="server" value="" class="text" size="10" id="ysize" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Alternative Text:
                        </td>
                        <td>
                            <input type="text" mytitle="This appears as a tooltip and is displayed when a user's mouse is over the banner"
                                runat="server" value="" class="text help" size="30" id="Alttext" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Banner Weighting:
                        </td>
                        <td>
                            <input type="text" mytitle="Weighting determines how frequently or in what position this banner will display in the Zone. To show it on the top/most frequent set the weighting to the highest value, i.e. 10. The second most important banner, set the weight to a lower value, i.e. 5."
                                runat="server" value="1" class="text help" size="10" id="Weight" valtype="required" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            Banner Text:
                        </td>
                        <td>
                            <input type="text" runat="server" value="" class="text help" size="30" mytitle="This appears below the banner image"
                                id="UnderText" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            Banner Text URL:
                        </td>
                        <td>
                            <input type="text" runat="server" value="" class="text help" size="30" id="UnderURL"
                                mytitle="This makes the banner text into a link that can redirect your website visitors" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            X size (width):
                        </td>
                        <td>
                            <input type="text" runat="server" value="" class="text" size="10" id="xsize" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Valid From</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date:
                        </td>
                        <td>
                            <input class="text help" mytitle="The date you wish your banner to start displaying"
                                type="text" runat="server" value="" size="30" id="ValidFromDate" valtype="required;regex:date" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Date:
                        </td>
                        <td>
                            <input type="text" class="text help" mytitle="The date you wish your banner to expire"
                                runat="server" valtype="required;regex:date" value="" size="30" id="ValidToDate" />&nbsp;Never
                            Expires<input class="help" mytitle="Check this box if you never wish for this banner to expire by date"
                                id="Expires" onclick="expires(this.checked);" type="checkbox" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            Show count:
                        </td>
                        <td>
                            <input type="text" runat="server" value="0" class="text help" size="10" id="ShowCount" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            Click count:
                        </td>
                        <td>
                            <input type="text" runat="server" value="0" class="text" size="10" id="ClickCount" />
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>
                            Under click count:
                        </td>
                        <td>
                            <input type="text" runat="server" value="0" class="text" size="10" id="UnderClickCount" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Max number of views:
                        </td>
                        <td>
                            <input valtype="required" valmessage="A value of 1 > is required" type="text" runat="server"
                                value="100" class="text help" size="10" id="MaxImpressions" mytitle="When a banner has been viewed this many times it will expire and not show on the website" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Max number of clicks:
                        </td>
                        <td>
                            <input valtype="required" valmessage="A value of 1 > is required" type="text" runat="server"
                                value="100" class="text help" size="10" id="MaxClicks" mytitle="When a banner has been clicked this many times it will expire and not show on the website" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Target</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Banner Image (mouse over to view):
                        </td>
                        <td>
                            <a id="btn_image" class="help" mytitle="The graphical representation of the banner"
                                href="javascript:void(0);" onclick="modalDialogShow('/Innova/assetmanager/assetmanager.asp?ClientID=<%=SessionManager.ClientSiteID %>',720,550)">
                                Upload A File</a>
                            <input type="hidden" id ="imagesrc" value=<%=bannerImage.Src %>>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Type:
                        </td>
                        <td>
                            <select style="width: 178px" width="155" runat="server" class="text help" id="FarmID"
                                mytitle="The type of advert that will be displayed.Banners are large and wide in width.Buttons are small and small in width">
                                <option value="4">Banners</option>
                                <option value="7">Buttons</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                    
                    </tr>
                    <tr>
                        <td>
                            Area:
                        </td>
                        <td>
                            <select size="4" multiple="true" style="width: 300px; height: 150px" width="200"
                                class="text help" id="ZoneId">
                                <asp:Repeater runat="server" ID="rptrZoneOptions">
                                    <ItemTemplate>
                                        <option parentid='<%#DataBinder.Eval(Container.DataItem, "ShortTitle")%>' value='<%#DataBinder.Eval(Container.DataItem, "ID")%>'>
                                            <%#DataBinder.Eval(Container.DataItem, "ShortTitle")%>
                                        </option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Page to display on:
                        </td>
                        <td>
                            <input type="text" runat="server" id="Category" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- End one column window -->
        </div>
    </div>
    </form>
</body>
</html>
