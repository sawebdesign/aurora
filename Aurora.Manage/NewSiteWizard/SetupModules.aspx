﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="SetupModules.aspx.cs" Inherits="Aurora.Manage.NewSiteWizard.SetupModules" ClientIDMode="Predictable" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
<script type="text/javascript" src="SetupModules.aspx.js"></script>
    <div id="contentList">
        <!-- Begin one column window -->
        <div class="onecolumn">
            <div class="header">
                <span>Please Select the modules you wish this client to have.</span>
                <input type="hidden" clientidmode="Static" runat="server" id="ModList" />
                 <input type="hidden" clientidmode="Static" runat="server" id="clientid" />
            </div>
            <br class="clear" />
            <div class="content">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr runat="server" id="trClients"> 
                        <td>Select A Client</td>
                        <td><asp:DropDownList OnSelectedIndexChanged="ddlClients_SelectedIndexChanged" runat="server"
                                ID="ddlClients" AutoPostBack="true"></asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trModules">
                        <td width="1%" valign="top">
                            <select  id="Modules" name="Modules" title="Modules" style="width: 285px; height: 200px" multiple="multiple" >
                            </select>
                        </td>
                        <td width="99%" valign="top">
                            <ul id="Modules_selectedItems">
                            </ul>
                        </td>
                    </tr>
                    <tr>
                    <td><br />
                    
                    <input type="button" ID="Save"  value="Next Step" runat="server" 
                            onclick="SaveModules();" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
