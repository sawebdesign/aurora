﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;

namespace Aurora.Manage.NewSiteWizard
{
    public partial class AddTemplates : System.Web.UI.Page{
        private long clientSiteId;
        private AuroraEntities auroraEntities = new AuroraEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            long.TryParse(Request["ClientSiteID"], out clientSiteId);
            if (!IsPostBack) {
                
                if (clientSiteId == 0) {
                    Response.Redirect("CreateClient.aspx");
                }
                DropDownMasterPages.DataSource = from templates in auroraEntities.SiteTemplates
                                                 select templates;
                DropDownMasterPages.DataTextField = "Name";
                DropDownMasterPages.DataValueField = "ClientSiteID";
                DropDownMasterPages.DataBind();
                DropDownMasterPages.Items.Insert(0, new ListItem("None", "-1")); 
            }
        }

        private void CreateDirectory(string impersonateID)
        {
            var clientSite = (from clients in auroraEntities.ClientSite
                             where clients.ClientSiteID == clientSiteId
                             orderby clients.CompanyName ascending
                             select clients).SingleOrDefault();

             #region Create Directory Structure

                string clientdir = Server.MapPath("~/ClientData");

                //Dev purposes only
                clientdir = clientdir.Replace("Aurora.Manage", "Aurora.Web");

                if (!Directory.Exists(clientdir))
                {
                    Directory.CreateDirectory(clientdir);
                }

                if (!Directory.Exists(clientdir + "\\" + clientSite.ClientSiteID))
                {
                    Directory.CreateDirectory(clientdir + "\\" + clientSite.ClientSiteID);
                    Directory.CreateDirectory(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage");
                    Directory.CreateDirectory(clientdir + "\\" + clientSite.ClientSiteID + "\\Styles");
                    Directory.CreateDirectory(clientdir + "\\" + clientSite.ClientSiteID + "\\Scripts");
                    Directory.CreateDirectory(clientdir + "\\" + clientSite.ClientSiteID + "\\Uploads");
                }

                if (FileUploadMaster.HasFile)
                {
                    FileUploadMaster.SaveAs(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage\\" + clientSite.ClientSiteID + "_Default.master");


                } else if (!string.IsNullOrEmpty(impersonateID) && DropDownMasterPages.SelectedIndex != -1)
                {
                    File.Copy(string.Format("{0}\\{1}\\MasterPage\\{1}_Default.master", clientdir, impersonateID), string.Format("{0}\\{1}\\MasterPage\\{1}_Default.master", clientdir, clientSiteId));
                }
                if (FileUploadSecondary.HasFile)
                {
                    FileUploadMaster.SaveAs(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage\\" + clientSite.ClientSiteID + "_Secondary.master");
                } else if (!string.IsNullOrEmpty(impersonateID) && DropDownMasterPages.SelectedIndex != -1) {
                    File.Copy(string.Format("{0}\\{1}\\MasterPage\\{1}_Secondary.master", clientdir, impersonateID), string.Format("{0}\\{1}\\MasterPage\\{1}_Secondary.master", clientdir, clientSiteId));
                }

                if (FileUploadStyleSheet.HasFile)
                {
                    FileUploadStyleSheet.SaveAs(clientdir + "\\" + clientSite.ClientSiteID + "\\Styles\\" + FileUploadStyleSheet.FileName);

                } else if (!string.IsNullOrEmpty(impersonateID) && DropDownMasterPages.SelectedIndex != -1) {
                    File.Copy(string.Format("{0}\\{1}\\Styles\\Site.css", clientdir, impersonateID), string.Format("{0}\\{1}\\Styles\\Site.css", clientdir, clientSiteId));
                    if (Directory.Exists(string.Format("{0}\\{1}\\Styles\\images", clientdir, impersonateID)))
                    {
                        Directory.CreateDirectory(string.Format("{0}\\{1}\\Styles\\images", clientdir, clientSiteId));

                        foreach (string file in Directory.GetFiles(string.Format("{0}\\{1}\\Styles\\images", clientdir, impersonateID)))
                        {
                            File.Copy(file, file.Replace(impersonateID,clientSiteId.ToString()));
                        }
                    }
                }

                if (FileUploadJavaScript.HasFile)
                {
                    FileUploadJavaScript.SaveAs(clientdir + "\\" + clientSite.ClientSiteID + "\\Scripts\\" + FileUploadJavaScript.FileName + ".js");
                } 
                #endregion

             #region Save templates
                if (FileUploadMaster.HasFile || DropDownMasterPages.SelectedIndex > -1)
                {
                    var template = new SiteTemplates()
                    {
                        Author = "Aurora",
                        ClientSiteID = clientSite.ClientSiteID,
                        Comments = string.Empty,
                        DeletedBy = null,
                        DeletedOn = null,
                        ID = clientSite.ClientSiteID,
                        InsertedBy = 0,
                        InsertedOn = DateTime.Now,
                        JScriptFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/Scripts/" + FileUploadJavaScript.FileName + ".js",
                        MasterPageFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/MasterPage/" + clientSite.ClientSiteID + "_Default.master",
                        Name = clientSite.CompanyName + " Default",
                        StyleSheetFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/Styles/" + FileUploadStyleSheet.FileName + ".css",
                        ParentID = 0


                    };

                    auroraEntities.AddToSiteTemplates(template);
                    auroraEntities.SaveChanges();

                    if (FileUploadSecondary.HasFile)
                    {
                        var secondarytemplate = new SiteTemplates()
                        {
                            Author = "Aurora",
                            ClientSiteID = clientSite.ClientSiteID,
                            Comments = string.Empty,
                            DeletedBy = null,
                            DeletedOn = null,
                            ID = clientSite.ClientSiteID,
                            InsertedBy = 0,
                            InsertedOn = DateTime.Now,
                            JScriptFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/Scripts/" + FileUploadJavaScript.FileName + ".js",
                            MasterPageFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/MasterPage/" + clientSite.ClientSiteID + "_Secondary.master",
                            Name = clientSite.CompanyName + " Content",
                            StyleSheetFilePath = "~/ClientData/" + clientSite.ClientSiteID + "/Styles/" + FileUploadStyleSheet.FileName + ".css",
                            ParentID = template.ID


                        };

                        auroraEntities.AddToSiteTemplates(secondarytemplate);
                        auroraEntities.SaveChanges();

                        var data = from clients in auroraEntities.ClientSite
                                   where clients.ClientSiteID == clientSite.ClientSiteID
                                   select clients;

                        data.First().TemplateID = template.ID;

                        auroraEntities.SaveChanges();
                    }
                }


                //TODO update this to use regex
                //Update master page string to reference base master page
                if (File.Exists(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage\\" + clientSite.ClientSiteID + "_Default.master"))
            {
                string[] lines = File.ReadAllLines(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage\\" + clientSite.ClientSiteID + "_Default.master");
                lines[0] = "<%@ Master Language=\"C#\" AutoEventWireup=\"true\" CodeBehind=\"~/Common/BaseMaster.cs\" Inherits=\"Aurora.SiteMaster\" %>";

                //Update Style & JS references

                for (int index = 0; index < lines.Length; index++)
                {
                    if (string.IsNullOrEmpty(impersonateID))
                    {
                        if (lines[index].Contains("url('"))
                        {
                            lines[index] = lines[index].Replace("url('",
                                                                "url('ClientData/" + clientSite.ClientSiteID + "/");
                        }

                        if (lines[index].Contains("<link href=\"~"))
                        {
                            lines[index] = lines[index].Replace("<link href=\"~",
                                                                "<link href=\"~/ClientData/" + clientSite.ClientSiteID +
                                                                "/");
                        }
                        else if (lines[index].Contains("<link href=\"../"))
                        {
                            lines[index] = lines[index].Replace("<link href=\"../",
                                                                "<link href=\"../ClientData/" + clientSite.ClientSiteID +
                                                                "/");
                        }
                    }
                    else
                    {
                        lines[index] = lines[index].Replace(impersonateID, clientSiteId.ToString());
                    }
                }
                File.WriteAllLines(clientdir + "\\" + clientSite.ClientSiteID + "\\MasterPage\\" + clientSite.ClientSiteID + "_Default.master", lines);
            }
               
                #endregion
        }

        protected void ButtonComplete_Click(object sender, EventArgs e) {
            if (DropDownMasterPages.SelectedIndex != -1)
            {
                CreateDirectory(DropDownMasterPages.SelectedValue);
                Response.Redirect("Complete.aspx");
            }
        }
    }
}