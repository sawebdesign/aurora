﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="CreateClient.aspx.cs" Inherits="Aurora.Manage.NewSiteWizard.CreateClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="onecolumn" style="padding: 3px;">
        <div class="header">
            <span>Client Information</span>
        </div>
        <br class="clear" />
        <div class="content" id="SiteInfo" runat="server">
            <div class="alert_info" runat="server" id="Messages">
                <p runat="server" id="messagetext">
                    <img id="icon" class="mid_align" alt="success" src="../Styles/images/icon_info.png"
                        runat="server" />
                    Please enter all the details in the fields provided.
                </p>
            </div>
            
            <div style="width: 400px; float: left;" >
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <th>
                            <h3>Company Details</h3>
                        </th>
                    </tr>
                    <tr>
                        <td>Site Parent:</td>
                        <td>
                            <asp:DropDownList runat="server" ID="SiteParent" AutoPostBack="true" 
                                Width="83%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Site Time Zone:</td>
                        <td>
                            <asp:DropDownList runat="server" ID="SiteTimeZone" AutoPostBack="false" 
                                Width="83%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Company Name:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputCompanyName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Company Description:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputCompanyDescription" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputPhysicalAddressLine1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 2:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputPhysicalAddressLine2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 3:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputPhysicalAddressLine3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Physical Address Line 4:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputPhysicalAddressLine4" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputPostalAddressLine1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="InputPostalAddressLine2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 3:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="InputPostalAddressLine3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Postal Address Line 4:
                        </td>
                        <td>
                            <input type="text" runat="server" id="InputPostalAddressLine4" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <h3>Site Details</h3>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Domain Name(separate by a ; for multiple):
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputDomainName" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" Text="Next Step" ID="ButtonSave" OnClick="ButtonSave_Click"/>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 400px; float: left; margin-top:-17px;">
                <br />
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <th>
                            <h3>Contact Details</h3>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Email Address 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" id="InputEmailAddress1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email Address 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" id="InputEmailAddress2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 1:
                        </td>
                        <td>
                            <input type="text" valtype="required" runat="server" maxlength="12" id="InputTelephone1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 2:
                        </td>
                        <td>
                            <input type="text" runat="server" maxlength="12" id="InputTelephone2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 3:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="InputTelephone3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone Number 4:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="InputTelephone4" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fax Number 1:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="InputFaxNumber1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fax Number 2:
                        </td>
                        <td>
                            <input type="text"  runat="server" maxlength="12" id="InputFaxNumber2" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 400px;">
                <br class="clear" />
                <table style="width: 100%; text-align: left;">
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
