﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master" AutoEventWireup="true" CodeBehind="Complete.aspx.cs" Inherits="Aurora.Manage.NewSiteWizard.Complete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Site Setup Complete</span>
        </div>
        <br class="clear" />
        <div class="content">
            The site has been setup please <a href="../UserAED.aspx?type=List">create a user</a> so that the client may now login and view their site.
        </div>
    </div>
</asp:Content>
