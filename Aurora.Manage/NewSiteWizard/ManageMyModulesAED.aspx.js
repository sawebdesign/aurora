﻿/// <reference path="../Scripts/jquery-1.5.2.js" />
/// <reference path="../Scripts/MicrosoftAjax.js" />
/// <reference path="../Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="../Service/AuroraWS.asmx/jsdebug" />
/// <reference path="../Scripts/Utils.js" />
/// <reference path="../Scripts/jquery.toChecklist.js" />
/// <reference path="../Scripts/generated/Aurora.Custom.js" />
//#region Init
var templateList = null;
var currentData = {};
var length = 0;
var TotalCount = 0;
var CurrentPage = 0;
var isSave = false;
$(function () {
    loadModules();

});
//#endregion

//#region Data Manipulation 
function loadModules() {
    $("#grid").block({ message: null });
    var onSuccess = function (result) {
        if (templateList == null) {
            templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
        }
        length = result.Count;
        if (TotalCount == 0) {
            TotalCount = length;
            initPagination();
        }

        currentData = result.Data;
        templateList.set_data(result.Data);
        $("#grid").unblock();
        $("#Edit").show();
    };

    Aurora.Manage.Service.AuroraWS.GetModuleConfig(CurrentPage + 1, new GlobalVariables.GlobalVar().PageSize, "page", onSuccess, onError);
}

function getModuleDetails(id, moduleName) {
    
    $("#Edit")[0].src = String.format("ManageMyModules.aspx?ModuleID={0}&ModuleName={1}&nocache={2}", id, moduleName, new Date().getMilliseconds());
    $("#Edit")[0].width = $("#templateList").width();    
}

function setSize() {
    $("#Edit").height($($("#Edit").first()[0].document).find(".content").height())
    $("#Edit").width("100%");
    $("#wrapper").unblock();
}

function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        loadModules();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        loadModules();
    }
    $("#contentAddEdit").hide();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: new GlobalVariables.GlobalVar().PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

function saveStart() {
    isSave = true;
    $("#wrapper").block({ message: null });
}
//#endregion