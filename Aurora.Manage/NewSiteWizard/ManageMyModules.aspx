﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageMyModules.aspx.cs"
    Inherits="Aurora.Manage.NewSiteWizard.ManageMyModules" %>

<%@ Import Namespace="Aurora.Custom" %>
<%@ Register Assembly="Aurora.Custom" Namespace="Aurora.Custom.UI" TagPrefix="cui" %>
<%@ Register Src="../UserControls/CustomFieldEditor.ascx" TagName="CustomFieldEditor"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Styles/black/screen.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/Styles/black/datepicker.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/Styles/tipsy.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/Scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css"
        media="all" />
    <script src="../Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox-1.3.0.js"></script>
    <script type="text/javascript" src="/Scripts/MicrosoftAjax.debug.js"></script>
    <script type="text/javascript" src="/Scripts/MicrosoftAjaxTemplates.debug.js"></script>
    <script type="text/javascript" src="/Scripts/generated/Aurora.Custom.js"></script>
    <script type="text/javascript" src="/Service/AuroraWS.asmx/js"></script>
    <script src="/Innova/scripts/innovaeditor.js" type="text/javascript"></script>
    <script src="../Scripts/Utils.js" type="text/javascript"></script>
    <script src="ManageMyModules.aspx.js?<%=System.DateTime.Now.ToString("MMddyyyHHss") %>"
        type="text/javascript"></script>
    <script>
        var client = "http://manage.boltcms.com<%=SessionManager.ClientBasePath %>";
    </script>
    <style type="text/css">
        #tbl_Form
        {
            width: 100%;
        }
    </style>
</head>
<body style="background: transparent url('');">
    <form id="form1" runat="server">
    <div>
        <div class="onecolumn">
            <div class="header">
                <span id="moduleName" runat="server">Editing</span>
                <div style="width: auto; float: right; margin: 0 10px 0 0;" class="switch">
                    <table cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="button" style="width: 90px;" value="Properties" class="left_switch active"
                                        name="tab1" id="tab1" />
                                </td>
                                <td>
                                    <input type="button" style="width: 90px;" value="Template" class="middle_switch"
                                        name="tab2" id="tab2" />
                                </td>
                                <td>
                                    <input type="button" style="width: 130px;" value="Custom Fields" class="right_switch"
                                        name="tab5" id="tab3" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br class="clear" />
            <div class="content">
                <div class="tab_content" id="tab1_content" style="text-align: left;">
                    <input type="hidden" runat="server" id="ID" />
                    <input type="hidden" id="ClientSiteID" runat="server" />
                    <div id="MessagesProp"></div>
                    <cui:ObjectPanel ID="ContentPanel" Visible="true" runat="server">
                        <cui:XmlPanel ID="XmlData" runat="server">
                        </cui:XmlPanel>
                    </cui:ObjectPanel>
                    <br />
                    <input type="button" style="float: right;" value="Save Properties" onclick="saveProp();"
                        id="saveProperties" />
                    <br class="clear" />
                </div>
                <div class="tab_content hide" id="tab2_content">
                 <div id="MessagesTemp"></div>
                    <textarea id="DetailText" clientidmode="Static" runat="server" style="width:100%;height:550px;" name="DetailText" rows="4" cols="30"></textarea>
                    <br />
                    <input type="button" id="saveTemplate" value="Save Template" style="float: right;" onclick="saveTempl();" />
                    <br class="clear" />
                </div>
                <div class="tab_content hide" id="tab3_content">
                    <uc1:CustomFieldEditor ID="CustomFieldEditor1" runat="server" SaveButtonID="saveCustomModule" />
                    <br />
                    <input type="button" id="saveCustomModule"  value="Save Fields" style="float: right;" />
                    <br class="clear" />
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
