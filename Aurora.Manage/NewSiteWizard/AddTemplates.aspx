﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="AddTemplates.aspx.cs" Inherits="Aurora.Manage.NewSiteWizard.AddTemplates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Add Or Assign a template </span>
        </div>
        <br class="clear" />
        <div class="content">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        Use Existing Master Page Template
                    </td>
                    <td>
                        <asp:DropDownList runat="server" AutoPostBack="true" ID="DropDownMasterPages">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>This Feature is coming soon..</td>
                </tr>
                <tr>
                    <td>
                        Upload Default Master Page Template:
                    </td>
                    <td>
                        <asp:FileUpload runat="server" ID="FileUploadMaster" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload Secondary Master Page Template:
                    </td>
                    <td>
                        <asp:FileUpload runat="server" ID="FileUploadSecondary" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload Style Sheet:
                    </td>
                    <td>
                        <asp:FileUpload runat="server" ID="FileUploadStyleSheet" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload Javascript File:
                    </td>
                    <td>
                        <asp:FileUpload runat="server" ID="FileUploadJavaScript" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="ButtonComplete" Text="Complete" OnClick="ButtonComplete_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
