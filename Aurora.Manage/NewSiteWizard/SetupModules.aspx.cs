﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.NewSiteWizard
{
    public partial class SetupModules : System.Web.UI.Page{
        private AuroraEntities entities = new AuroraEntities();
        private long clientSiteID;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!SessionManager.UserRoles.Select(p => p.RoleName).ToArray().Contains("Admin"))
                {
                    Response.Redirect("~/Dashboard.aspx");
                }
                trClients.Visible = false;
                long.TryParse(Request["ClientSiteID"], out clientSiteID);
                if (clientSiteID == 0)
                {
                    trModules.Visible = false;
                    Save.Visible = false;
                    trClients.Visible = true;

                    var clients = from sites in entities.ClientSite
                                  where sites.DeletedBy == null
                                        && sites.DeletedOn == null
                                  orderby sites.CompanyName
                                  select sites;

                    ddlClients.DataSource = clients;
                    ddlClients.DataTextField = "CompanyName";
                    ddlClients.DataValueField = "ClientSiteID";
                    ddlClients.DataBind();
                    ddlClients.Items.Insert(0,new ListItem
                    {
                        Text="Select One",
                        Value="0"
                    });
                
                }

                clientid.Value = clientSiteID.ToString();

              
            }
        }

        protected void ddlClients_SelectedIndexChanged(object sender,EventArgs e)
        {
            if (ddlClients.SelectedValue != "0")
            {
                Response.Redirect("~/NewSiteWizard/SetupModules.aspx?ClientSiteID=" + ddlClients.SelectedValue);
            }
          
        }

    
    }
}