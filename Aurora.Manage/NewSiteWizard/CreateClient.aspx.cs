﻿/// Author       : Wesley Perumal
/// Description  : Creates a new client for Aurora
/// Date         : 13 Apr 2011
/// 
#region Directives
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.IO;
using System.Data.Common;
using System.Collections.ObjectModel;
#endregion

namespace Aurora.Manage.NewSiteWizard {
    public partial class CreateClient : System.Web.UI.Page {
        AuroraEntities auroraEntities = new AuroraEntities();

        private bool createdClient;
        private bool createdTemplate;
        private bool createdDomain;
        private string newSiteName;
        private string boltCmsDomain = string.Empty;
        private string clientSiteID = string.Empty;

        protected void Page_Load(object sender, EventArgs e) {
            if (!SessionManager.UserRoles.Select(p => p.RoleName).ToArray().Contains("Admin")) {
                Response.Redirect("~/Dashboard.aspx");
            }
            if (!Page.IsPostBack) {
                Title = "New Website Wizard";

                ReadOnlyCollection<TimeZoneInfo> tz = TimeZoneInfo.GetSystemTimeZones();

                int defaultTZIndex = 0;

                SiteTimeZone.DataSource = tz;
                SiteTimeZone.DataTextField = "DisplayName";
                SiteTimeZone.DataValueField = "Id";
                SiteTimeZone.DataBind();

                foreach (var item in tz) {
                    if (item.Id == "South Africa Standard Time") {
                        break;
                    }
                    defaultTZIndex++;
                }

                SiteTimeZone.SelectedIndex = defaultTZIndex;

                //Get All Sites
                var sites = from clientSite in auroraEntities.ClientSite
                            orderby clientSite.CompanyName
                            select clientSite;

                SiteParent.DataSource = sites;
                SiteParent.DataTextField = "CompanyName";
                SiteParent.DataValueField = "ClientSiteID";
                SiteParent.DataBind();

                SiteParent.Items.Insert(0, new ListItem("None", "0"));
            }
        }

        /// <summary>
        /// Creates default setup for a client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonSave_Click(object sender, EventArgs e) {
            string[] domainvalues = InputDomainName.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string domainvalue in domainvalues) {
                //Check domain
                var isValidDomain = from domains in auroraEntities.Domain
                                    where domains.DomainName == domainvalue
                                    select domains;

                if (isValidDomain.Count() > 0) {
                    messagetext.InnerHtml =
                        "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> The domain name \"" + InputDomainName.Value + "\" already exists please select another name.";

                    Messages.Attributes.Add("class", "alert_warning");
                    return;
                }

                if (domainvalue.Contains("boltcms.com")) {
                    boltCmsDomain = domainvalue;
                }
            }

            if (String.IsNullOrEmpty(boltCmsDomain)) {
                messagetext.InnerHtml =
                        "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> The domain name for BoltCMS does not exist please add the BoltCMS domain name.";

                Messages.Attributes.Add("class", "alert_warning");
                return;
            }
            //Validate user input
            if (ValidateEntry())
                CreateSite();
            else {
                messagetext.InnerHtml =
                    "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> There were un-entered fields. Please enter all fields that have been highlighted.";
                Messages.Attributes.Add("class", "alert_warning");
            }
        }

        /// <summary>
        /// Validates the form
        /// </summary>
        /// <returns></returns>
        private bool ValidateEntry() {
            bool returnVal = true;

            foreach (Control htmlControl in SiteInfo.Controls) {
                if (htmlControl.GetType() == typeof(HtmlInputText)) {
                    HtmlInputText textField = (HtmlInputText)htmlControl;
                    if (textField.Attributes["valtype"] != null) {
                        if (String.IsNullOrEmpty(textField.Value)) {
                            returnVal = false;
                            textField.Style.Add("background-color", "#fcf2a5");
                        }
                        else {
                            textField.Style.Add("background-color", "");
                        }
                    }
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Saves form data and creates site domain in domain table.Redirects to the module selection page.
        /// </summary>
        private void CreateSite() {
            auroraEntities.Connection.Open();
            using (DbTransaction transaction = auroraEntities.Connection.BeginTransaction()) {
                try {

                    #region Save Form
                    var clientSite = new ClientSite() {
                        AdminAreaLink = string.Empty,
                        ClientSiteID = 0,
                        CompanyDescription = InputCompanyDescription.Value,
                        CompanyName = InputCompanyName.Value,
                        DeletedBy = null,
                        DeletedOn = null,
                        Email1 = InputEmailAddress1.Value,
                        Email2 = InputEmailAddress2.Value,
                        InsertedBy = 0,
                        InsertedOn = DateTime.Now,
                        PhysicalAddr1 = InputPhysicalAddressLine1.Value,
                        PhysicalAddr2 = InputPhysicalAddressLine2.Value,
                        PhysicalAddr3 = InputPhysicalAddressLine3.Value,
                        PhysicalAddr4 = InputPhysicalAddressLine4.Value,
                        PostalAddr1 = InputPostalAddressLine1.Value,
                        PostalAddr2 = InputPostalAddressLine2.Value,
                        PostalAddr3 = InputPostalAddressLine3.Value,
                        PostalAddr4 = InputPostalAddressLine4.Value,
                        Fax1 = InputFaxNumber1.Value,
                        Fax2 = InputFaxNumber2.Value,
                        Tel1 = InputTelephone1.Value,
                        Tel2 = InputTelephone2.Value,
                        Tel3 = InputTelephone3.Value,
                        Tel4 = InputTelephone4.Value,
                        UseParentContactDetails = false,
                        ParentClientSiteID = long.Parse(SiteParent.SelectedValue),
                        TemplateID = 0,
                        TimeZoneID = SiteTimeZone.SelectedValue
                    };

                    auroraEntities.AddToClientSite(clientSite);
                    auroraEntities.SaveChanges();

                    clientSiteID = clientSite.ClientSiteID.ToString();
                    #endregion

                    //create default page
                    auroraEntities.AddToPageContent(new PageContent {
                        ClientSiteID = clientSite.ClientSiteID,
                        Description = "First Page",
                        isArchived = false,
                        isDefault = true,
                        LongTitle = "Welcome to your new home",
                        ShortTitle = "Home",
                        MetaDescription = "Home Page",
                        MetaKeywords = "Home Page",
                        IsSecured = false,
                        PageContent1 = "Welcome to your BoltCMS Home",
                        InsertedBy = 10000,
                        InsertedOn = DateTime.Now

                    });


                    #region Create Domain
                    string[] domainvalues = InputDomainName.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    if (Server.MachineName == ConfigurationManager.AppSettings["LiveServerName"]) {
                        SitelutionsAPI sl =
                            new SitelutionsAPI(System.Configuration.ConfigurationManager.AppSettings["slLocation"],
                                               System.Configuration.ConfigurationManager.AppSettings["slUri"],
                                               System.Configuration.ConfigurationManager.AppSettings["slUserName"],
                                               System.Configuration.ConfigurationManager.AppSettings["slPassword"]);
                        foreach (string domainvalue in domainvalues) {
                            bool isSubDomain = domainvalue.Contains("www.");
                            var domain = new Domain() {
                                DomainName = domainvalue,
                                ClientSiteID = clientSite.ClientSiteID,
                                isSubDomain = isSubDomain

                            };
                            auroraEntities.AddToDomain(domain);
                            auroraEntities.SaveChanges();

                            if (domainvalue.Contains("boltcms.com")) {
                                try {
                                    string result =
                                        sl.addRR(long.Parse(ConfigurationManager.AppSettings["slDefaultDomainID"]),
                                                 "boltcms.com.", "cname",
                                                 domainvalue.Replace(".boltcms.com", string.Empty), 3600);
                                }
                                catch (Exception ex) {
                                    //sadly this is the only way to prevent the page from breaking
                                }
                            }

                            #region Create a root domain

                            //else
                            //{
                            //    string[] newDomain = domainvalue.Split('.');
                            //    //check of domain exists?
                            //    try
                            //    {
                            //        if (newDomain.Length == 3)
                            //        {
                            //            string result = sl.getDomainByName(newDomain[1] + "." + newDomain[2]);
                            //        }
                            //        else
                            //        {
                            //            string result = sl.getDomainByName(newDomain[1] + "." + newDomain[2] + "." + newDomain[3]);
                            //        }
                            //    }
                            //    catch (Exception)
                            //    {
                            //        try
                            //        {
                            //            if (newDomain.Length == 3)
                            //            {
                            //                string result = sl.addRR(0, ConfigurationManager.AppSettings["ServerIP"], "cname", newDomain[1] + "." + newDomain[2], 3200);
                            //            }
                            //            else
                            //            {
                            //                string result = sl.addRR(0, ConfigurationManager.AppSettings["ServerIP"], "cname", newDomain[1] + "." + newDomain[2] + "." + newDomain[3], 3200);
                            //            }

                            //        }
                            //        catch (Exception)
                            //        {
                            //            //this is the only way to ensure the page does not break
                            //        }
                            //    } 
                            //}

                            #endregion

                        }
                    }
                    else //Developer mode
                    {
                        var domain = new Domain() {
                            DomainName = boltCmsDomain,
                            ClientSiteID = clientSite.ClientSiteID,
                            isSubDomain = true
                        };
                        auroraEntities.AddToDomain(domain);
                        auroraEntities.SaveChanges();
                    }
                    //Create Default IIS Web Site 

                    Utils.CreateIISWebsite(boltCmsDomain, "http", ConfigurationManager.AppSettings["ServerIP"] + ":80:" + boltCmsDomain, ConfigurationManager.AppSettings["AuroraWebPath"], ConfigurationManager.AppSettings["AppPool"]);
                    newSiteName = boltCmsDomain;
                    Utils.AddDomainToSite(domainvalues, boltCmsDomain);

                    #endregion

                    transaction.Commit();

                }
                catch (Exception ex) {
                    //remove site that has been created
                    if (!String.IsNullOrEmpty(newSiteName)) {
                        Utils.DeleteSite(newSiteName);
                    }
                    if (transaction != null) {
                        transaction.Rollback();
                    }

                    var message = string.Format("Message:{0} <br/> StackTrace:{1} <br/> TargetSite:{2} <br/> InnerException:{3} <br/> Source:{4}", ex.Message, ex.StackTrace, ex.TargetSite, ex.InnerException, ex.Source);

                    Utils.SendErrorMail(ConfigurationManager.AppSettings["ErrorMailListFrom"], "Aurora", message, "Manage reported an error");

                    messagetext.InnerHtml = " <img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_error.png\"runat=\"server\" />There was an error while trying to save the Clients information please try again later.";
                    Messages.Attributes.Add("class", "alert_error");
                    return;
                }

                //redirect to module Page
                Response.Redirect("SetupModules.aspx?ClientSiteID=" + clientSiteID + "&NewClient=1");
            }
        }
    }
}