﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Manage.NewSiteWizard {
    public partial class ManageMyModules : System.Web.UI.Page {
        Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        private ClientModule currentModule = new ClientModule();
        protected void Page_Load(object sender, EventArgs e) {
            ID.Value = Request["ModuleID"];

            CustomFieldEditor1.ModuleName = Request["ModuleName"];
            if (Request["ex"] == null)
            {
                Utils.CheckSession();
                int? SchemaId = 0;
                int ModuleId = Utils.ConvertDBNull(Request["ModuleID"], -1);
                
                string ModuleName = Utils.ConvertDBNull(Request["ModuleName"], string.Empty);
                XmlDocument ConfigXML = new XmlDocument();
                string schema = string.Empty; //the schema for the extension
                XmlDocument SchemaXml = new XmlDocument(); //the schema for the extension

                Utils.CheckSqlDBConnection();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[Clients].[proc_Module_GetByClientSiteIDName]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    cmd.Parameters.AddWithValue("@Name", ModuleName);
                    DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        SchemaId = Utils.ConvertDBNull(ds.Tables[0].Rows[0]["SchemaId"], 0);
                        schema = Utils.ConvertDBNull(ds.Tables[0].Rows[0]["SchemaXML"], "");
                    }
                    else
                    {
                        ContentPanel.Visible = false;
                    }

                }
                //get module data

                var moduleData = (from modules in entities.ClientModule
                                  join features in entities.Module on modules.FeaturesModuleID equals features.ID
                                  where modules.ID == ModuleId
                                        && modules.ClientSiteID == SessionManager.ClientSiteID
                                  select new {modules, features}).SingleOrDefault();
                //assign to global
                currentModule = moduleData.modules;

                //Check if default xml data exits
                if (!string.IsNullOrEmpty(moduleData.modules.ConfigXML))
                {
                    ConfigXML.LoadXml(moduleData.modules.ConfigXML);
                }
                else if (!string.IsNullOrEmpty(moduleData.features.ConfigXML))
                {
                    ConfigXML.LoadXml(moduleData.features.ConfigXML);
                }

                if (SchemaId > 0 && !String.IsNullOrEmpty(schema))
                {

                    //Load the schema and data
                    SchemaXml.LoadXml(schema);
                    XmlData.XmlPanelSchema = SchemaXml;
                    XmlData.XmlPanelData = ConfigXML;

                    //Render the control and add it to the Form
                    XmlData.RenderXmlControls("tbl_Form");

                    
                }
                //else
                //{

                //    Response.Redirect(Request.Url.ToString() + "&ex=1");
                //}

                moduleName.InnerHtml = string.Format("Editing {0}", moduleData.features.FriendlyName);
            }
        }

        protected void Save_Click(object sender, EventArgs e) {

            currentModule.ConfigXML = ContentPanel.WriteObjectToXml().InnerXml.Replace("ContentPanel", "XmlData");
            currentModule.SetAllModified(entities);
            entities.SaveChanges();

        }
    }
}