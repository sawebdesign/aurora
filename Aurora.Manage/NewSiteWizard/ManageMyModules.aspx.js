﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
/// <reference path="/Scripts/timePicker-addOn.js" />
$(function () {
    //Get module data
    getModuleTemplate();
    // Setup tab contents
    $(".left_switch,.right_switch,.middle_switch").click(
     function () {
         $(this).parent().parent().find('td input').removeClass('active');
         $(this).addClass('active');

         //show tab content
         $('.tab_content').addClass('hide');
         $('#' + $(this).attr('id') + '_content').removeClass('hide');
     }
    );
    if ($("#tbl_Form").length == 0) {
        $("#tab1,#tab1_content").hide();
        $("#tab2_content").removeClass("hide").addClass("active");
        $("#tab2").addClass("active");
    }
    else {
        if (parent.isSave) {
            parent.DisplayMessage("Messages", "success", "Module config saved", 5000);
            setTimeout(function () {
                parent.DisplayMessage('Messages', 'info', 'Module configuration loaded.');
                parent.isSave = false;
            }, 4000)
        }
        else {
            parent.DisplayMessage('Messages', 'info', 'Module configuration loaded.');
        }

        parent.$("#Edit").show();
    }
    parent.scrollToElement("Messages", 0, 2);
    parent.setSize();

})

function saveStart() {
    parent.saveStart();
}

function getModuleTemplate() {
    var response = function (result) {
        if (result.Data.length) {
        	$("#DetailText").val(result.Data[0].modules.HTML || result.Data[0].moduleFeatures.HTML);
        }
    };

    Aurora.Manage.Service.AuroraWS.GetModulesByClientModuleID(Number($("#ID").val()), response, onError);
}

function saveProp() {
    var customXMLData = buildCustomXMLBySelector($("#tbl_Form input,#tbl_Form select,#tbl_Form textarea"));
    var response = function (result) {
        if (result.Data.ID) {
            DisplayMessage("MessagesProp", "success", "Module properties successfully updated", 7000);
        }
        else {
            DisplayMessage("MessagesProp", "warn", "We were not able to update your properties please try again", 7000);
        }
    };

    Aurora.Manage.Service.AuroraWS.UpdateModuleProperties($("#ID").val(), customXMLData,response,onError);
}

function saveTempl() {
    var response = function (result) {
        if (result.Data.ID) {
            DisplayMessage("MessagesTemp", "success", "Module properties successfully updated", 7000);
        }
        else {
            DisplayMessage("MessagesTemp", "warn", "We were not able to update your properties please try again", 7000);
        }
    };

    Aurora.Manage.Service.AuroraWS.UpdateModuleTemplate($("#ID").val(), $("#DetailText").val(), response, onError);
}