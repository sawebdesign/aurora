﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
	CodeBehind="ManageClientModulesAED.aspx.cs" Inherits="Aurora.Manage.NewSiteWizard.ManageClientModulesAED" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script language="javascript" src="ManageClientModulesAED.aspx.js"></script>
    <div class="onecolumn">
        <div class="header">
            <span>Module List</span>
        </div>
        <br class="clear" />
        <div class="content" id="grid">
            <table class="data" width="100%" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th align="left">
                            Name
                        </th>
                        <th align="left">
                            Description
                        </th>
                        <th align="right">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" sys:attach="dataview" id="templateList">
                    <tr>
                        <td width="20%">
                        {{featureModules.FriendlyName}}
                        </td>
                        <td align="left">
                        {{featureModules.Description}}
                        </td>
                        <td align="right" width="10%">
                            <a href="javascript:void(0);" dataid="{{featureModules.ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" datadesc="{{featureModules.Name}}" dataid="{{clientModules.ID}}"
                                    onclick="$('#wrapper').block({message:null});getModuleDetails(this.getAttribute('dataid'),this.getAttribute('datadesc')); return false;" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
         <div id="Pagination">
         </div>
    </div>
    <div id="Messages">
    </div>
    <div id="wrapper">
    <iframe id="Edit" style="display:none;" onload="ResizeFrame('Edit');" allowtransparency="true" scrolling="no" frameborder="0"  width="$('#templateList').width();" marginheight="0" marginwidth="0">
    </iframe>
    </div>
</asp:Content>
