﻿/// <reference path="../Scripts/jquery-1.5.2.js" />
/// <reference path="../Scripts/MicrosoftAjax.debug.js" />
/// <reference path="../Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="../Service/AuroraWS.asmx/js" />
/// <reference path="../Scripts/Utils.js" />

var allModules = null;
var myModules = null;

var onError = function (result) {
    //alert(result);
};

$(function () {
    if (getQueryVariable("NewClient") == null) {
        $("#MainContent_Save").val("Save");
    }
    var allmodulesDef = $.Deferred();

    //Load modules into module check list
    var getModules = function (result) {
        if (result.Count > 0) {
            allModules = result.Data;
        }
        allmodulesDef.resolve();
    };

    Aurora.Manage.Service.AuroraWS.GetAllModules(getModules, onError);
    var myModulesdef = $.Deferred();
    var getClientModules = function (result) {
        if (result.Count > 0) {
            myModules = result.Data;
        }
        myModulesdef.resolve();
    };
    Aurora.Manage.Service.AuroraWS.GetModules(getClientModules, onError);
    var complete = $.when(allmodulesDef, myModulesdef).then(loadModules);
});

function AppendToModList(object, func) {
    if (func == "Add") {
        $("#ModList").val($("#ModList").val() + object.value + ",");
    } else {
        var item = object.value + ",";
        var list = $("#ModList").val();
        var left = list.substr(0, list.indexOf(item));
        var right = list.substr(list.indexOf(item) + item.length);
        $("#ModList").val(left + right);
    }
}

function checkEvent(e) {
    if (e.checked) {
        AppendToModList(e, "Add");
    } else {
        AppendToModList(e);
    }
}

function SaveModules() {
    var save = function (result) {
        if (result.Result) {
            if (getQueryVariable("NewClient") != null) {
                document.location = "AddTemplates.aspx?ClientSiteID=" + $("#clientid").val();
            }
            else {
                $("<div>All Module changes have been updated.</div>").dialog({ modal: true,
                    resizable: false,
                    title: "Success",
                    buttons: { "Okay": function () { $(this).dialog("close"); } }
                });
            }
        }
    }

    var cleanModList = new Array();
    var modList = $("#Modules_selectedItems li");
    modList.each(function (index,Elem) {
        cleanModList.push($(Elem).attr("id").toString().replace("Modules_",""));
    });
    Aurora.Manage.Service.AuroraWS.AddModulesToClient(cleanModList, $("#clientid").val(), save, onError);
}

function loadModules() {
    if (allModules) {
        for (var i = 0; i < allModules.length; i++) {

            var Selected = (allModules[i].Name == "PageDetail" || allModules[i].Name == "PrePeeledFooter");
            if (myModules != null) {
                for (var item = 0; item < myModules.length; item++) {
                    if (myModules[item].FeaturesModuleID == allModules[i].ID) {
                        Selected = "selected";
                    }
                }
            }

            $("#Modules").append("<option value='" + allModules[i].ID + "' " + Selected + ">" + allModules[i].FriendlyName + "</option>");
            if (Selected) {
                $("#ModList").val($("#ModList").val() + allModules[i].ID + ",");
            }
        }
    }


    $('#Modules').toChecklist({

        /**** Available settings, listed with default values. ****/
        addScrollBar: true,
        addSearchBox: true,
        searchBoxText: 'Type here to search list...',
        showCheckboxes: true,
        showSelectedItems: true,
        submitDataAsArray: true // This one allows compatibility with languages that use arrays

    });

    $("#Modules ul li input").click(function () {
        checkEvent(this);
    });
}