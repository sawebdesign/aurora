﻿//#region Refs
/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />
//#endregion

//#region Init
$(function () {
	DisplayMessage("Messages", "info", "Please fill in the form below", 7000);
	$("#rptrItems input:checkbox").click(function () { checkBoxClick(this); });

	$("#rptrItems input:checkbox[hasselected='True']").attr("checked", "checked");
	calculateRequestedItems();
	$("#rptrItems input:checkbox[hasselected='True']").attr("disabled", "disabled");
	

	$('#btnSendQuote').fancybox({
		padding: 0,
		transitionIn: 'elastic',
		transitionOut: 'elastic',
		titleShow: true,
		title: "Quote Preview",
		overlayColor: '#000000',
		overlayOpacity: .5,
		showActivity: true,
		autoDimensions: false,
		width: 800,
		height: 600,
		onStart: function () { return sendQuote(true); },
		onClosed: function () {
			$("#Quote .items tbody").hide();
			$("#Quote .items thead").hide();
			$("#Quote").hide();
		}
	});

});
//#endregion


