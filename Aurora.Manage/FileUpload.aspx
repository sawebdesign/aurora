﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="FileUpload.aspx.cs" Inherits="Aurora.Manage.FileUpload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload</title>
    <script src="/Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="/Scripts/Utils.js" type="text/javascript"></script>
        <script type="text/javascript">
            var usermessage;
            $(function () {

                $("#ButtonUploadFile").hide();
                $("#loader").hide();
                $('#FileUploadControl').change(function () {
                    $("#loader").show();
                    $("#FileUploadControl").hide();
                    $('#ButtonUploadFile').click();
                });

                $("#message").hide();
                $('#message').click(function () {
                    $("#message").slideToggle();
                });

                loadfile();

                if (usermessage !== undefined) {
                    message(usermessage);
                } else {
                    message('To select a <% =uploadExtension %> file to upload click the Browse button on the top left.');
                }
            });

            function loadfile() {
                //load the file
            }

            function message(lable) {
                $("#message").html(lable);
                $("#message").slideToggle();
            }

        </script>

    <style type="text/css">
        html, body, table, td {
	        FONT: 11px/20px Tahoma, Helvetica, sans-serif; 
        }

        .fileinputdiv {
	        width: 200px; 
	        height: 26px; 
	        overflow: hidden;
	        background-color:#E3E9FF;
	        vertical-align:middle;
        }

        .fileinputbutton {
            BACKGROUND: url(images/buttonbg.gif) repeat-x;
            margin-top:2px;
	        position: absolute; 
	        background-color: #FFFFFF;
	        color: #999999;
	        border-style: solid;
	        border-bottom: 1px solid #666666;
	        border-top: 1px solid #666666;
	        border-left: 1px solid #666666;
	        border-right: 1px solid #333333;
        }

        .filepreloader {
            margin-top:2px;
	        position: absolute; 
        }

        .fileinputhidden {
	        font-size: 45px; 
	        position: absolute; 
	        right: 0px; 
	        top: 0px; 
	        opacity: 0; 
	        filter: alpha(opacity=0); 
	        -ms-filter: "alpha(opacity=0)"; 
	        -khtml-opacity: 0; 
	        -moz-opacity: 0;
        }

        .filemessage {
	        position:relative; 
	        background-color:#666666;
	        opacity: 90; 
	        filter: alpha(opacity=90); 
	        -ms-filter: "alpha(opacity=90)"; 
	        -khtml-opacity: 90; 
	        -moz-opacity: 90;
	        width:100%;
	        height:50px;
            color: #FFFFFF;
            font: 14px/20px Tahoma, Helvetica, sans-serif bold;
        }

        .filtdleft {
	        border-right: 2px solid #E3E9FF;
        }

        .filetdmain  {
            border-bottom: 1px solid #E3E9FF;
        }

        .filetdbg  {
            background-color:#BBCCFF;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="top" class="fileleft">
                    <div class="fileinputdiv">
                        <img src="/styles/images/preload.gif" id="loader" class="filepreloader" alt="loading" width="20" height="20" />
                        <asp:FileUpload runat="server" Width="83" class="fileinputbutton" ID="FileUploadControl"  />
                    </div>
                    <asp:Button runat="server"  ID="ButtonUploadFile" CssClass="button" Text="Upload" onclick="ButtonUploadFile_Click" />
                    <br /><b>File:</b><br /><% =Request["prefix"] %><% =Request["id"] %><br />
                    <% if (!string.IsNullOrEmpty(uploadExtension)) { %>
                        <br /><b>File Type:</b><br /><% =uploadExtension %><br />
                    <% } %>
                </td>
                <td valign="top" width="99%" class="filetdmain">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1">
	                    <tr>
		                    <td class="filetdbg">
                                <div id="imageMenu">
						            <!--<input type="image" src="/styles/images/ico_save_off.gif" value="Save" name="save" id="save" runat="server" />-->
						            <input type="image" src="/styles/images/ico_delete_off.gif" value="Delete" name="delete" id="delete" runat="server" title="Delete" onserverclick="delete_ServerClick" />
                                </div>
		                    </td>
	                    </tr>
                        <tr>
                            <td>
                                <div id="message" class="filemessage">File loaded</div>
                            </td>
                        </tr>
	                    <tr>
		                    <td>
                                <div id="mylayerDiv">
                                    <% if (!string.IsNullOrEmpty(uploadExtension)) { %>
                                        <% if (uploadExtension == ".mp3") { %>
                                        <asp:HiddenField runat="server" ID="HiddenAudioFileSource" />
                                        <asp:ImageButton Style="display: none;" AlternateText="Download Item" runat="server" ID="LinkDownloadFile" Width="70" Height="40" ImageUrl="~/Styles/Images/download.png" OnClick="LinkDownloadFile_Click"></asp:ImageButton>
                                        <asp:Literal runat="server" ID="player"></asp:Literal>
                                        <% } %>
                                    <% } %>
                                </div>
		                    </td>
	                    </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
