﻿/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Service/AuroraWS.asmx/js" />
/// <reference path="/Utils.js" />
//#region Vars
var templateList;
var catList;
var Loaded = false;
var TotalCount = 0;
var CurrentPage = 0
var Global = new GlobalVariables.GlobalVar();
var currentData = {};
//#endregion
//#region Init
$(function () {

    if (getQueryVariable("type") == undefined) {
        document.location = document.location + "?type=Edit";
        return;
    }
    getFeedback();
    $("#btnSave").hide();
    $("#Cancel").hide();
    $("#btnDecline").hide();
    if (getQueryVariable('type').toUpperCase() == "EDIT" || getQueryVariable('type').toUpperCase() == "LIST") {
        $('div.content').block({ message: null });
        //  $("#contentAddEdit").hide();
        $("#AddNewCat").hide();

    } else if (getQueryVariable('type').toUpperCase() == "ADD") {
        $("#contentList").hide();
    }




});
//#endregion

//#region Data Manipulation 

function getFeedback() {
    //  $("#contentAddEdit").hide();
    $("#btnSave").hide();
    $("#btnDecline").hide();
    $("#Cancel").hide();

    $("div.content").block({ message: null })
    var requestFeedback = function (result) {
        if (result.Result) {
            if (result.Data.length == 0) {
                $('div.content').hide();
                DisplayMessage("Messages", "Warn", "No Testimonies Available", false);
                return;
            }
            $('div.content').show();
            $("#Messages").hide();

            currentData = result.Data;
            if (templateList == null) {
                templateList = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
            }

            templateList.set_data(result.Data);
            if (TotalCount == 0) {
                TotalCount = result.Count;
                initPagination();
            }
            TotalCount = result.Count;

            $('div.content').unblock();
            // Add tooltip to edit and delete button
            $('.help').tipsy({ gravity: 's' });
        }
        else {
            if (!result.Result) {
                DisplayMessage("Messages", "error", "Sorry your data is not currently available, please try again later", false);
            }
        }
    }
    Aurora.Manage.Service.AuroraWS.GetFeedBack(CurrentPage + 1, Global.PageSize, $("#FeedType").val(), requestFeedback, onError);


}
function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getFeedback();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getFeedback();
    }
    //  $("#contentAddEdit").hide();
    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
function getDetail(anchor) {
    var x = new Date();
    $("#contentAddEdit").block({ message: null });
    var success = function (responseText, status, XHR) {
        if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
            var obj = {};
            obj._message = "SessionExpired"
            onError(obj);
        }
        else {
            $("#contentAddEdit").unblock();
            $("#contentAddEdit").show();
            $("#btnSave").show();
            $("#Cancel").show();
            $("#btnDecline").show();
        }
    };
    if (anchor == undefined) {
        $("#contentAddEdit").load("CustomerFeedBack.aspx?feedback=0&var=" + x.getMilliseconds(), success);
    }
    else {
        $("#contentAddEdit").load("Testmonies.aspx?feedback=" + anchor.dataid + "&var=" + x.getMilliseconds(), success);
    }
    DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
    scrollToElement("Messages", -20, 2);



}

function deleteData(anchor) {
    if (confirm("Item will be removed permanently,\nDo you wish to continue?", "Delete Item")) {
        var Feedback = new AuroraEntities.CustomerFeedback(findByInArray(currentData, "ID", anchor.dataid));
        Feedback.DeletedBy = 0;
        Feedback.ApprovedOn = null;
        Feedback.Approvedby = null;
        Feedback.DeletedOn = new Date();
        Feedback.isApproved = false;

        var requestDelete = function (result) {
            if (result.Result) {
                changeFeedType();
            }
            else {
                DisplayMessage("Messages", "error", "Sorry but we could not save your data a request has been sent through and we will contact you shortly")
            }
        }

        Aurora.Manage.Service.AuroraWS.UpdateFeedback(Feedback, requestDelete, onError);
    }
}

function cancel() {
    $("#contentAddEdit").hide();
    $("#btnSave").hide();
    $("#Cancel").hide();
    $("#btnDecline").hide();
    $("#Messages").hide();
}

function saveData(accept) {
    if ($("#Preview").val() == "") {
        DisplayMessage("Messages", "warn", "Please enter some preview text for this testmony as it will not display without one.");
        return;
    }

    var Feedback = new AuroraEntities.CustomerFeedback(findByInArray(currentData, "ID", $("#ID").val()));

    Feedback.ContactNumber = $("#ContactNumber").val();
    Feedback.Email = $("#Email").val();
    Feedback.FirstName = $("#FirstName").val();
    Feedback.LastName = $("#LastName").val();
    Feedback.Comment = oEdit1.getHTMLBody();
    Feedback.ID = Feedback.ID == "" ? 0 : parseInt(Feedback.ID);
    Feedback.InsertedOn = Feedback.InsertedOn == "" ? new Date() : new Date(Feedback.InsertedOn);
    Feedback.isApproved = accept;
    Feedback.Preview = $("#Preview").val();

    if (Feedback.isApproved) {
        Feedback.ApprovedBy = 0;
        Feedback.ApprovedOn = new Date();
    }
    else {
        Feedback.ApprovedBy = null;
        Feedback.ApprovedOn = null;
    }
    if (accept) {
        Feedback.DeletedBy = null;
        Feedback.DeletedOn = null;
    }
    else {
        Feedback.DeletedBy = Feedback.DeletedBy == "" ? null : Feedback.DeletedBy;
        Feedback.DeletedOn = Feedback.DeletedOn == "" ? new Date() : Feedback.DeletedOn;
    }


    var updateEvent = function (result) {

        if (result.Result != false) {
            DisplayMessage("Messages", "success", "data saved", false);
            changeFeedType();
        }
        else {
            DisplayMessage("Messages", "error", "There was an error while trying to save the testimony please try again later.", false);
        }

    };

    Aurora.Manage.Service.AuroraWS.UpdateFeedback(Feedback, updateEvent, onError);
}

function changeFeedType() {

    $('div.content').block({ message: null });
    $("#btnSave").hide();
    $("#Cancel").hide();

    CurrentPage = 0;
    TotalCount = 0;
    getFeedback();
    initPagination();
}


//#endregion

