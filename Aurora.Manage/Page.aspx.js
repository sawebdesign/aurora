﻿/// <reference path="Scripts/jquery-1.4.1.js" />
/// <reference path="Scripts/Utils.js" />
/*globals saveData,newPage,parent,alert,scrollToElement,DisplayMessage,FeaturesList,$create,Sys,$get,confirm,Aurora,onError,selectedAnchor 
_comma_separated_list_of_variables_*/


//#region Init
$(function () {
    //set config size
    $("#imgSize").val(GetJSSession("Page_ImageSize"))
    //tips
    $(".details").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'focus', delayIn: 0, delayOut: 0 });


    $("#delImage").attr("pageid", $("#ID").val());
    //checks if a page is a default page
    if ($("#isDefault")[0].checked) {
        $("#isDefault").attr("disabled", "true");
    }

    //checks if a page is hidden 
    if (Number($("#PageIndex").val()) <= -1) {
        $("#showPage")[0].checked = true;
        $("#PageIndex").val(Number($("#PageIndex").val())*-1);
    }
    else {
        $("#showPage")[0].checked = false;
    }


    //Displays the page header image once mouse hovers over image upload
    $('#btn_image1').mouseover(
        function () {
            $("#PageImage").css("left", ($("#tab1_content").width() * 0.70) + 20);
            $("#PageImage").css("border", "2px dashed grey");
            $("#PageImage").css("top", "91px");
            $("#PageImage").fadeIn(500);
            $("#PageImage").height($("#PageImage").height() / $("#PageImage").width * ($("#tab1_content").width() - ($("#tab1_content").width() * 0.60)));
            $("#PageImage").width($("#tab1_content").width() - ($("#tab1_content").width() * 0.70));
        }
    );


    $('#btn_image1').mouseout(
    function () {
        $("#PageImage").fadeOut(500);
    }
    );

    //hides the page header image
    $("#PageImage").hide();

    //Allows only numerics into the following inputs
    $("#PageIndex").ForceNumericOnly();
    $("#imgSize").ForceNumericOnly();


    //uses href for eezy imager
    $('#btn_image1')[0].href = 'WebImage/WebImageUpload.aspx?ipath=' + client.replace("~", "..") + '/Uploads/&redir=../Default.aspx&iName=page_' + $("#ID").val() + '.jpg&smWidth=25&lgWidth=' + $("#imgSize").val() + '&smPrefix=sm_';
    $('#btn_image1').fancybox({
        padding: 0,
        titleShow: true,
        title: "Upload An Image.",
        overlayColor: '#000000',
        overlayOpacity: 0.5,
        width: '90%',
        height: '90%',
        showActivity: true,
        onClosed: function () { reloadAfterImage(); },
        type: "iframe"
    });
    $('#btn_image1').click(
    function () {
        if ($("#imgSize").val() !== "") {
            $('#btn_image1')[0].href = 'WebImage/WebImageUpload.aspx?ipath=' + client.replace("~", "..") + '/Uploads/&iName=page_' + $("#ID").val() + '.jpg&smWidth=25&lgWidth=' + $("#imgSize").val() + '&smPrefix=sm_';
            saveData();
            $('#btn_image1').fancybox({
                padding: 0,
                titleShow: true,
                title: "Upload An Image.",
                overlayColor: '#000000',
                overlayOpacity: 0.5,
                width: '90%',
                height: '90%',
                showActivity: true,
                type: "iframe"
            });

        }
        else {
            parent.$.fancybox.close();
            alert("Please enter a value for image size");
        }
        return false;
    }
    );



    //sets the default tab
    if (newPage) {
        $('#tab1').click();
    }
    else {
        $('#tab2').click();
    }

    //notification 

    scrollToElement("Messages", -20, 2);
    DisplayMessage("Messages", "info", "Please enter the all the information in the form below.", 5000);
    InitEditor("DetailText", "editorSpan", true);

    enableSecured($("#IsSecured")[0]);
    
    //if the page id is empty then disable security and show a message
    if ($("#ID").val() == "") {
        $(".right_switch").attr("disabled", "disabled");
        DisplayMessage("Messages", "warn", "The Security tab has been disabled, it will only be activated once this page has been saved");
    }
});
//#endregion

//#region Custom Methods 

function onModulesSuccess(result) {
    /// <summary>when a module has been linked to a page this method is called.</summary>
    if (result.Result === true) {
        if (FeaturesList === null) {
            FeaturesList = $create(Sys.UI.DataView, {}, {}, {}, $get("FeaturesList"));
        }
        else {
            FeaturesList.dispose();
            FeaturesList = $create(Sys.UI.DataView, {}, {}, {}, $get("FeaturesList"));
        }

        FeaturesList.set_data(result.Data);
        $("<option value='0'>Nothing</option>").insertBefore($("#FeaturesList").children()[0]);
        //set module link
        if ($("#ClientModuleID").val() == "") {
            $("#ClientModuleID").val("0");
        }
        $("#FeaturesList").first().val($("#ClientModuleID").val())

    }


}

function CreateLink(anchor) {
    /// <summary>Creates a link between a page and the selected module.</summary>
    var ans;
    if (anchor[0].selectedIndex != 0) {
        ans = confirm("Are you sure you want to link the " + $("#ShortTitle").val() + " page to the '" + anchor[0][anchor[0].selectedIndex].innerHTML + "' Feature?");
    }
    else {
        ans = confirm("Selecting this will remove any links to a Feature for this page, continue?");
    }

    if (ans) {
        var currentPage;
        var getPage = function (result) {

            currentPage = result.Data;

            currentPage.ClientModuleID = $(anchor[0][anchor[0].selectedIndex]) == -1 ? null : anchor[0][anchor[0].selectedIndex].value;

            var updatePage = function (result) {

                DisplayMessage("Messages", "success", "Feature has been linked to the '" + $("#ShortTitle").val() + "' page.", 10000);
                $('#ModulesMenu').css('visibility', 'hidden');
                scrollToElement("Messages", -20, 2);

            };

            Aurora.Manage.Service.AuroraWS.UpdatePage(currentPage, updatePage, onError);
        };

        if ($("#ID").val() === null || $("#ID").val() === "") {
            DisplayMessage("Messages", "warn", "Feature has cannot be linked until the page has been saved.'" + $("#ShortTitle").val() + "' page.", 10000);
            scrollToElement("Messages", -20, 2);
            $('#ModulesMenu').css('visibility', 'hidden');
            return;
        }

        Aurora.Manage.Service.AuroraWS.GetPages($("#ID").val(), getPage, onError);
    }

    Aurora.Manage.Service.AuroraWS.GetModules(onModulesSuccess, onError);
}


function getDetail() {
    /// <summary>Loads a pages information</summary>
    parent.loadDetail(selectedAnchor);
}

function reloadAfterImage() {
    getDetail();
}

function enableSecured(checkbox) {
    if (checkbox.checked) {
        $("#UserName").removeAttr("disabled");
        $("#Password").removeAttr("disabled");
    }
    else {
        $("#UserName").attr("disabled","disabled");
        $("#Password").attr("disabled", "disabled");
    }
}
//#endregion