﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageReorder.aspx.cs" Inherits="Aurora.Manage.PageReorder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Page Reorder</title>
    <link href="/Styles/ui.all.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.js" type="text/javascript"></script>
    <script src="Scripts/jquery.ui.nestedSortable.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <script src="Scripts/MicrosoftAjaxTemplates.debug.js" type="text/javascript"></script>
    <script src="Service/AuroraWS.asmx/js" type="text/javascript"></script>
    <script src="Scripts/Utils.js" type="text/javascript"></script>
    <script type="text/javascript" src="PageReorder.aspx.js"></script>
    <link href="Styles/black/screen.css" rel="stylesheet" type="text/css" />
    <link href="Styles/black/nestedSortable.css" rel="stylesheet" type="text/css" />
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="contentList">
            <!-- Begin one column window --> 
            <div class="onecolumn">
                <div class="header" id="name">
                    <span>Reorder Pages</span>
                    <div style="margin-top: 5px; margin-right: 5px; float: right; cursor: pointer;">
                     <div class="barButton" onclick="reorderPages();return false">Save</div>
                     </div>
                </div>
                <br class="clear" />
                <div class="alert_info">
                <p><img class="mid_align" src="/styles/images/icon_info.png">Drag the pages up and down to reorder them. Move them left or right to make them subpages of another page.</p></div>
                <div style="margin-top:0" class="alert_warning">
				    <p>
					    <img class="mid_align" src="/styles/images/icon_warning.png">
					    Modifying the page order here will overwrite any custom ordering you have.
				    </p>
			    </div>
                <div class="content">
                </div>
            </div>
            <!-- End one column window -->
            <!-- End content -->
        </div>
    </div>
    </form>
</body>
</html>
