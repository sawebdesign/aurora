﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Testmonies.aspx.cs"
    Inherits="Aurora.Manage.CustomerFeedBack" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Testimonies Details</title>
    <script src="Scripts/custom_black.js" type="text/javascript"></script>
    <script type="text/javascript" src="Testmonies.aspx.js"></script>
</head>
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
<body>
    <form id="form1" runat="server">
    <!-- Begin content -->
    <div id="contentAddEdit">
        <!-- Begin one column window -->
        <div class="onecolumn">
            <div class="header">
                <span>Testimonies Design</span>
                <div style="width: 100px;" class="switch">
                    <table width="100px" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <input type="button" style="width: 50px;" value="Details" class="left_switch active"
                                        name="tab1" id="tab1">
                                </td>
                                <td>
                                    <input type="button" style="width: 50px;" value="Editor" class="middle_switch" name="tab2"
                                        id="tab2">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br class="clear" />
            <div class="content" id="FeedbackDetails">
                <div class="tab_content" id="tab1_content" style="width:100%;">
                    
                        <fieldset id="templateDetail"  sys:attach="dataview">
                            <input type="hidden" id="ID" value="" runat="server" />
                            <input type="hidden" id="ClientSiteID" value="" runat="server" />
                            <input type="hidden" id="SchemaID" value="" runat="server" />
                            <input type="hidden" id="InsertedOn" value="" runat="server" />
                            <input type="hidden" id="InsertedByIP" value="" runat="server" />
                            <input type="hidden" id="DeletedOn" value="" runat="server" />
                            <input type="hidden" id="DeletedBy" value="" runat="server" />
                            <table border="1" width="100%" class="fcktable" style="display: inline-table;">
                                <tr>
                                    <td>
                                        First Name:
                                    </td>
                                    <td>
                                        <input type="text" disabled="disabled" id="FirstName" name="FirstName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Name:
                                    </td>
                                    <td>
                                        <input type="text" disabled="disabled" id="LastName" name="LastName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email:
                                    </td>
                                    <td>
                                        <a href="" runat="server" id="Email" name="Email">mail@mail.com</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Number:
                                    </td>
                                    <td>
                                        <input type="text" disabled="true" id="ContactNumber" name="ContactNumber" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Preview:</td>
                                    <td><input type="text" id="Preview" valtype="required" name="Preview" runat="server" maxlength="100" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fcktable">
                                        <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                            <cui:XmlPanel ID="ContentXml" runat="server">
                                            </cui:XmlPanel>
                                        </cui:ObjectPanel>
                                    </td>
                                </tr>
                            </table>
                 </fieldset>
        
                </div>
                <div class="tab_content hide" id="tab2_content">
                   <textarea id="Comment" clientidmode="Static" runat="server" style="display:none;" name="DetailText" rows="4" cols="30"></textarea>
                <span id="editorSpan"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->
    </form>
</body>
</html>
