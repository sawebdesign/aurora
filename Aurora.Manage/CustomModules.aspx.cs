﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage
{
    public partial class CustomModules : System.Web.UI.Page{
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["ModuleID"] != null)
            {
                int moduleID;
                int.TryParse(Request["ModuleID"], out moduleID);
                if (moduleID != 0)
                {
                    var customModule = from custModules in entities.CustomModules
                                       where custModules.ClientSiteID == SessionManager.ClientSiteID
                                             && custModules.ID == moduleID
                                       select custModules;
                    if (customModule.Count() > 0)
                    {
                        ClientSiteID.Value = customModule.First().ClientSiteID.ToString();
                        ID.Value = customModule.First().ID.ToString();
                        inpName.Value = customModule.First().Name;
                        inpDescription.Value = customModule.First().Description;
                        inpEmailRecipients.Value = customModule.First().EmailRecipients;
                        inpEmailSubject.Value = customModule.First().EmailSubject;
                        inpCustomMessage.Value = customModule.First().EmailCustomMessage;
						inpIncludeForm.Checked = customModule.First().IncludeFormContent == true ? true : false;
                        FeaturesModuleID.Value = Utils.ConvertDBNull(customModule.First().FeaturesModuleID,string.Empty);
                        CustomFieldEditor1.CustomModuleID = customModule.First().ID.ToString();
                        CustomFieldEditor1.SaveButtonID = "btnSave";

                    }

                }    
            }
            
        }
    }
}