﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="NewsAED.aspx.cs" Inherits="Aurora.Manage.NewsAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <%--<script type="text/javascript" src="NewsAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>--%>
    <script type="text/javascript" src="NewsAED.aspx.js?3"></script>

    <asp:Panel ID="pnlList" runat="server">
        <!-- Begin content -->
        <div id="contentList">
            <div class="inner">
                <!-- Begin one column window -->
                <div class="onecolumn">
                    <div class="header">
                        <span>News List</span>
                        <div style="float: left; margin-top: 10px; margin-right: 10px;">
                            <div>
                                &nbsp;
                                <select id="NewsCat" class="sys-template" sys:attach="dataview" onchange="changeNewsCat();$('#contentAddEdit').hide();">
                                    <option value="{binding ID}">{binding Name}</option>
                                </select>
                                <a id="categoryEditFancy" href="/toolpages/CategoryAED.aspx?moduleID=3" mytitle="Edit Categories"
                                    class="help">
                                    <img src="Styles/images/folder_wrench.png" alt="Edit Categories" /></a>
                                <img src="Styles/images/script_add.png" onclick="addNewCat();" style="cursor: pointer;
                                    display: none;" mytitle="Create a category" class="help" />
                            </div>
                        </div>
                        <div style="float: left; margin-top: 10px; margin-left: 10px;">
                            <select id="newsType" onchange="TotalCount=0;loadList(this.value); $('#contentAddEdit').hide();">
                                <option value="Active">Active</option>
                                <option value="Archived">Archived</option>
                            </select>
                        </div>
                        <div style="float: right; margin-top: 10px; margin-right: 10px; cursor: pointer;">
                            <a href="javascript:void(0);" class="help" mytitle="Create News Item" onclick="loadDetail(this);return false;"
                                dataid="-1">
                                <img src="Styles/images/add.png" alt="Create User" title="Create News" /></a>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table class="data" width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 70%" align="left">
                                        Title
                                    </th>
                                    <th style="width: 10%" align="left">
                                        Start Date
                                    </th>
                                    <th style="width: 10%" align="left">
                                        End Date
                                    </th>
                                    <th style="width: 10%" align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="templateList" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td>
                                        {binding Title}
                                    </td>
                                    <td>
                                        {binding StartDate, convert=getDate}
                                    </td>
                                    <td>
                                        {binding EndDate, convert=getDate}
                                    </td>
                                    <td align="right">
                                        <a href="javascript:void(0);" onclick="loadDetail(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_edit.png" alt="edit" class="help" mytitle="Edit" /></a>
                                        <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_delete.png" alt="delete" class="help" mytitle="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Begin pagination -->
                        <div id="Pagination">
                        </div>
                        <!-- End pagination -->
                    </div>
                </div>
                <!-- End one column window -->
                <table width="100%">
                    <tr>
                        <td>
                            <div id="Messages" style="width: 100%">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- End content -->
        </div>
        <div id="AddNewCat">
            <div id="Add">
                <div class="inner">
                    <!-- Begin one column window -->
                    <div class="onecolumn">
                        <div class="header">
                            <span>Add a News Category</span>
                        </div>
                        <br class="clear" />
                        <div id="AddMsg">
                        </div>
                        <div class="content">
                            <table width="100%" id="addData">
                                <tr>
                                    <td>
                                        Name:
                                    </td>
                                    <td>
                                        <input type="text" id="CatName" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <input type="text" id="CatDescription" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Order
                                    </td>
                                    <td>
                                        <input type="text" id="CatOrder" valtype="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" value="Add" onclick="AddNewsCategory();" />
                                        <input type="button" value="Done" onclick="document.location.reload()" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div id="contentAddEdit" style="display: none;">
        </div>
    </asp:Panel>
</asp:Content>
