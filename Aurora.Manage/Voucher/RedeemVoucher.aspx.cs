﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Security;

namespace Aurora.Manage.Voucher {
    public partial class RedeemVoucher : System.Web.UI.Page {
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        private Custom.Data.VoucherItem currentVoucherItem;

        protected void Page_Load(object sender, EventArgs e) {
            if (securedSession.Value != "True") {
                login.Visible = true;
                redeemVoucher.Visible = false;
                MemberInfo.Visible = false;
                this.ClientScript.RegisterStartupScript(this.GetType(), "none", "showMessages();", true);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e) {
            string encryptedPassword = Encryption.Encrypt(Pwd.Text);

            var user = (from users in entities.Users
                        where users.ClientSiteID == SessionManager.ClientSiteID
                              && users.Password == encryptedPassword
                              && users.UserName == UName.Text
                        select users).SingleOrDefault();

            if (user != null) {
                login.Visible = false;
                redeemVoucher.Visible = true;
                securedSession.Value = "True";

                messagetext.InnerHtml =
              "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_info.png\"runat=\"server\" />  Please enter the voucher code you wish to validate.";
                Messages.Attributes.Add("class", "alert_info");
            }
            else {
                messagetext.InnerHtml =
                   "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> The user name password combination does not match any of our records, please try again.";
                Messages.Attributes.Add("class", "alert_warning");
            }
        }

        protected void btnRedeem_Click(object sender, EventArgs e) {
            var voucher = (from vouchers in entities.VoucherItem
                           where vouchers.Code == redeemAmount.Text
                           select vouchers).SingleOrDefault();

            if (voucher != null) {
                if (voucher.Redeemed.Value) {
                    messagetext.InnerHtml =
                "<img id=\"icon\" class=\"mid_align\" alt=\"error\" src=\"../Styles/images/icon_error.png\"runat=\"server\" /> Voucher code has been redeemed.";
                    Messages.Attributes.Add("class", "alert_error");
                    return;
                }
                VoucherID.Value = voucher.ID.ToString();
                //get member info
                var member = (from members in entities.Member
                              where members.ID == voucher.MemberID
                              select members).SingleOrDefault();

                if (member != null) {
                    MemberEmail.Text = member.Email;
                    MemberName.Text = member.FirstName;
                    MemberSurname.Text = member.LastName;
                    MemberInfo.Visible = true;
                    login.Visible = false;
                    redeemVoucher.Visible = false;
                    securedSession.Value = "True";

                    messagetext.InnerHtml =
                "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> Please the following details with the client:";
                    Messages.Attributes.Add("class", "alert_warning");

                }
                else {
                    messagetext.InnerHtml =
                  "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> Voucher code was found but was not issued to a member, and is therefore not redeemable.";
                    Messages.Attributes.Add("class", "alert_warning");
                }
            }
            else {
                messagetext.InnerHtml =
                 "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" /> Voucher code was not found in our system, please try again.";
                Messages.Attributes.Add("class", "alert_warning");
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e) {



            long voucherID;
            long.TryParse(VoucherID.Value, out voucherID);
            currentVoucherItem = (from vouchers in entities.VoucherItem
                                  where vouchers.ID == voucherID
                                  select vouchers).SingleOrDefault();
            //check if voucher is still valid
            var voucherSetting = (from voucherSettings in entities.Voucher
                                  where voucherSettings.ID == currentVoucherItem.VoucherID
                                  select voucherSettings).SingleOrDefault();
            XmlDocument settings = new XmlDocument();
            settings.LoadXml(voucherSetting.CustomXML);
            bool valid = false;
            string reason = string.Empty;

            foreach (XmlNode setting in settings.GetElementsByTagName("field")) {
                switch (setting.Attributes["fieldname"].Value) {
                    default:
                        valid = true;
                        break;
                    case "Date Range":
                        string[] dates = setting.Attributes["value"].Value.Split('|');
                        DateTime from, to;
                        DateTime.TryParse(dates[0], out from);
                        DateTime.TryParse(dates[1], out to);

                        valid = DateTime.Now >= from && DateTime.Now <= to;
                        reason = "This voucher has execeeded it valid time period for redemption.";
                        break;
                    case "Purchase Over X":
                        //valid = basketTotal > Convert.ToDouble((setting.Attributes["value"].Value));
                        reason = String.Format("This current purchase does not exceed {0} which is required for this voucher to be valid", setting.Attributes["value"].Value);
                        break;
                    case "InStore":
                        valid = true;
                        break;


                }

                if (!valid) {
                    break;
                }
            }

            if (!valid) {
                messagetext.InnerHtml =
               "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_warning.png\"runat=\"server\" />" + reason;
                Messages.Attributes.Add("class", "alert_warning");
                return;
            }
            currentVoucherItem.Redeemed = true;
            currentVoucherItem.SetAllModified(entities);
            entities.SaveChanges();

            messagetext.InnerHtml =
                 "<img id=\"icon\" class=\"mid_align\" alt=\"success\" src=\"../Styles/images/icon_accept.png\"runat=\"server\" /> Voucher has been redeemed";
            Messages.Attributes.Add("class", "alert_success");

            this.ClientScript.RegisterStartupScript(this.GetType(), "none", "setTimeout(hideMessages,5000);", true);
            login.Visible = true;
            redeemVoucher.Visible = false;
            MemberInfo.Visible = false;
            VoucherID.Value = "";
            securedSession.Value = "False";
            UName.Text = "";
            Pwd.Text = "";
            redeemAmount.Text = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e) {
            Response.Redirect(Request.Url.ToString());
        }
    }
}