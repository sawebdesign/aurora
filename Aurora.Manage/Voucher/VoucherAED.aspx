﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="VoucherAED.aspx.cs" Inherits="Aurora.Manage.Voucher.VoucherAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script src="VoucherAED.aspx.js" type="text/javascript"></script>
    <div class="onecolumn">
        <div class="header">
            <span>Current Campaigns</span>
            <div style="float: right; margin-top: 10px; margin-right: 10px;">
                <img id="Add" src="/Styles/images/add.png" onclick="editVoucher(0);" style="cursor: pointer;"
                    mytitle="Create a Voucher campaign" class="help" />
            </div>
        </div>
        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="data" width="100%">
                <thead>
                    <tr>
                        <th align="left" width="40%">
                            Name
                        </th>
                        <th align="left" width="20%">
                            Codes Remaining
                        </th>
                        <th align="left" width="20%">
                            Codes Redeemed
                        </th>
                        <th align="right" width="20%">
                            Function
                        </th>
                    </tr>
                </thead>
                <tbody class="sys-template" id="VoucherTable">
                    <tr>
                        <td align="left">
                            {{Name}}
                        </td>
                        <td align="left">
                        {{Remaining}}
                        </td>
                        <td align="left">
                        {{Redeemed}}
                        </td>
                        <td align="right">
                            <a href="javascript:void(0);" dataid="{{ID}}" onclick="editVoucher($(this).attr('dataid'));return false;"
                                dataid="{{ID}}">
                                <img src="../styles/images/icon_edit.png" class="help" mytitle="Edit" alt="Edit" /></a>
                            <a href="javascript:void(0);" dataid="{{ID}}" onclick="editVoucher($(this).attr('dataid'),true);return false;"
                                dataid="{{ID}}">
                                <img src="../styles/images/icon_delete.png" class="help" mytitle="Delete" alt="delete"
                                    /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br class="clear" />
    <div id="Messages">
    </div>
    <br class="clear" />
    <div id="contentAddEdit" style="display:none;"></div>
    <br />
    <div style="float:right;">
        <input type="button" value="Save" onclick="saveVoucher();" style="display: none;" id="btnSave" />
    </div>
</asp:Content>
