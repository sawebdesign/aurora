﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora.Manage.Voucher {
    public partial class Voucher : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

            Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
            long voucherID;
            long.TryParse(Request["ID"], out voucherID);
            var query = (from voucher in entities.Voucher
                         where voucher.ID == voucherID
                         select voucher).SingleOrDefault();


            if (query != null) {
                ID.Value = query.ID.ToString();
                InsertedBy.Value = query.InsertedBy.ToString();
                InsertedOn.Value = query.InsertedOn.ToString();
                Name.Value = query.Name;
                Description.Value = query.Description;
                CustomXML.Value = query.CustomXML;
                VoucherTypeID.Value = query.VoucherTypeID.ToString();
                VoucherTypeValue.Value = Utils.ConvertDBNull(query.VoucherTypeValue, "");

            }
            else {

                InsertedBy.Value = SessionManager.UserID.ToString();
                InsertedOn.Value = DateTime.Now.ToString();
                ID.Value = "0";
            }
            ClientSiteID.Value = SessionManager.ClientSiteID.ToString();
        }
    }
}