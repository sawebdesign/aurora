﻿//#region refs
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
//#endregion
var voucherDS = null;
var voucherTypeDS = null;
var voucherConditionsDS = null;
$(
    function () {
        loadVouchers();
    }
);

function loadVouchers() {
    var response = function (result) {
        if (result.Data.length > 0) {
            if (!voucherDS) {
                voucherDS = $create(Sys.UI.DataView, {}, {}, {}, $get("VoucherTable"));
            }
            voucherDS.set_data(result.Data);
        }
        else {
            DisplayMessage("Messages", "warn", "no vouchers have been created, please create a voucher by clicking on the green cross on the top right", 10000);
        }
    };

    Aurora.Manage.Service.AuroraWS.GetAllVouchers(response, onError);
}

function editVoucher(id, isDelete) {
    if (isDelete) {
        if (!confirm("Are you sure you want to delete this voucher?")) {
            return false;
        }
    }
    var loadResponse = function (response, status, xhr) {
        if (status === "success") {
            $("#VoucherTypeValue").ForceNumericOnly();
           
            $(".tipsy").remove();
            $("#content input[type='text'],#content select").tipsy({ gravity: 'w', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            //Load types
            var reponseType = function (result) {
                if (!voucherTypeDS) {
                    voucherTypeDS = $create(Sys.UI.DataView, {}, {}, {}, $get("voucherType"));
                }
                else {
                    voucherTypeDS.dispose();
                    voucherTypeDS = null;
                    voucherTypeDS = $create(Sys.UI.DataView, {}, {}, {}, $get("voucherType"));
                }
                voucherTypeDS.set_data(result.Data);
                $("#voucherType").val($("#VoucherTypeID").val());

            };

            Aurora.Manage.Service.AuroraWS.GetVoucherTypes(reponseType, onError);

            //Load conditions
            var responseCondition = function (result) {
                $("#contentAddEdit").unblock();
                $("#btnSave").show();
                if (!voucherConditionsDS) {
                    voucherConditionsDS = $create(Sys.UI.DataView, {}, {}, {}, $get("conditions"));
                }
                else {
                    voucherConditionsDS.dispose();
                    voucherConditionsDS = null;
                    voucherConditionsDS = $create(Sys.UI.DataView, {}, {}, {}, $get("conditions"));
                }

                voucherConditionsDS.set_data(result.Data);
                var customXML = $("#CustomXML").val();

                if (customXML != "") {
                    $("#conditions input").each(function (idx, Elme) {
                        $item = $(Elme);
                        var itemData = LoadDataFromXML(customXML, $item.attr("name").replace("fld", ""));
                        if (!(itemData instanceof Object) && itemData !== undefined) {
                            $item.attr("checked", "checked");
                            $item.val(itemData);
                            applyTextField($item, itemData);
                        }
                    });
                }
            };

            Aurora.Manage.Service.AuroraWS.GetVoucherConditions(responseCondition, onError);
            $("#contentAddEdit").unblock();
            $("#btnSave").show();
            if (!isDelete) {
                $("#contentAddEdit").show();
                $("#btnSave").show();
            }
            else {
                $("#contentAddEdit").hide();
                $("#btnSave").hide();
                $("#DeletedOn").val(new Date().format("yyyy-MM-dd HH:mm"));
                $("#DeletedBy").val("0");
                saveVoucher();
            }

        }
    };

    var url = String.format("Voucher.aspx?ID={0}&nocache={1}", id, new Date().getMilliseconds());

    if (isDelete) {
        $("#contentAddEdit").block({ message: null });
        $("#contentAddEdit").show();

    }
    else {
        $("#contentAddEdit").hide();
    }

    $("#contentAddEdit").load(url, null, loadResponse);
}

function saveVoucher() {
    if (validateForm("contentAddEdit", "Please enter all the marked fields", "Messages", false)) {
        return;
    }
    var newEntity = new AuroraEntities.Voucher();
    populateEntityFromForm(newEntity, "tab1_content");
    newEntity.CustomXML = buildCustomXMLBySelector($("input:checked[type='checkbox'][name^='fld']"), "value");

    var response = function (result) {
        if (result.Result && !result.Data.DeletedOn) {
            DisplayMessage("Messages", "success", "Voucher saved", 7000);
            loadVouchers();

        }
        else if (result.Result && result.Data.DeletedOn) {
            DisplayMessage("Messages", "warn", "Voucher removed", 7000);
            $("#contentAddEdit").hide();
            loadVouchers();
            $("#btnSave").hide();
        }
        else {
            DisplayMessage("Messages", "error", "Voucher could not be saved,please try again.If the problem persists please contact support", 7000);
        }
    };

    Aurora.Manage.Service.AuroraWS.UpdateVoucher(newEntity, response, onError);
}

function applyTextField(object, value) {
    var textBox = $(object).next();
    if (object.is(":checked")) {
        switch (object.attr("vtype")) {
            default:
                break;
            case "amount":
                object.next().show();
                textBox.attr("valtype", "required");
                $(object).next().ForceNumericOnly();
                break;
            case "date":
                //create pickers
                if ($("#from").length > 0) {
                    $("#from").remove();
                }
                if ($("#to").length > 0) {
                    $("#to").remove();
                }
                if ($("#txtTo").length > 0) {
                    $("#txtTo").remove();
                }

                $("<input type='text' id='from' valtype='required' /><label id='txtTo'> To </label><input type='text' valtype='required' id='to' />").insertAfter(textBox);
                var dates = $("#from, #to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    onSelect: function (selectedDate) {
                        var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
                        object.val($("#from").val() + "|" + $("#to").val());
                        dates.not(this).datepicker("option", option, date);

                    }
                });
                break;

        }

        if (value) {
            switch (object.attr("vtype")) {
                default:
                    textBox.val(value);
                    textBox.attr("valtype", "required");
                    break;
                case "date":

                    //create pickers
                    if ($("#from").length > 0) {
                        $("#from").remove();
                    }
                    if ($("#to").length > 0) {
                        $("#to").remove();
                    }
                    if ($("#txtTo").length > 0) {
                        $("#txtTo").remove();
                    }

                    $("<input type='text' id='from' /><label id='txtTo' valtype='required'> To </label><input valtype='required' type='text' id='to' />").insertAfter(textBox);
                    var currentdates = value.split('|');
                    if (currentdates[0] != "null") {
                        $("#from").val(currentdates[0]);
                        $("#to").val(currentdates[1]);
                    }
                    var dates = $("#from, #to").datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        onSelect: function (selectedDate) {
                            var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
                            object.val($("#from").val() + "|" + $("#to").val());
                            dates.not(this).datepicker("option", option, date);

                        }
                    });
                    break;

            }

        }
    }
    else {
        textBox.hide();
        textBox.val("");
        textBox.removeAttr("valtype");
        //remove pickers
        if ($("#from").length > 0) {
            $("#from").remove();
        }
        if ($("#to").length > 0) {
            $("#to").remove();
        }
        if ($("#txtTo").length > 0) {
            $("#txtTo").remove();
        }
    }
}


function generateVoucherCodes() {
    $("#contentAddEdit").block({ message: null });
    var response = function (result) {
        if (result.Result) {
            DisplayMessage("Messages", "success", "Voucher codes generated", 7000);
            loadVouchers();
            $("#VoucherAmount").val("");
            $("#contentAddEdit").unblock();
        }
    };
    var amount = Number($("#VoucherAmount").val());
    if (amount > 0 && Number($("#ID").val()) > 0) {
        Aurora.Manage.Service.AuroraWS.GenerateVoucherCodes(Number($("#ID").val()), amount, response, onError);
    }
    else {
        if (amount == 0) {
            alert('Please enter the amount of vouchers that will need');
        }
        else {
            alert("Please save this voucher setting before you generate any vouchers");
        }

    }
}


function discardVouchers() {
    if (confirm("Do you wish to delete all the vouchers codes for this voucher?")) {
        $("#contentAddEdit").block({ message: null });
        var response = function (result) {
            if (result.Result) {
                DisplayMessage("Messages", "success", "Voucher codes discarded", 7000);
                loadVouchers();
                $("#contentAddEdit").unblock();
            }
        };
        Aurora.Manage.Service.AuroraWS.DiscardVouchers(Number($("#ID").val()), response, onError);
    }
}