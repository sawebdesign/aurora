﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Voucher.aspx.cs" Inherits="Aurora.Manage.Voucher.Voucher"
    ClientIDMode="Predictable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Scripts/custom_black.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="onecolumn">
        <div class="header">
            <span>Editing Voucher:</span>
            <div style="width: 180px;" class="switch">
                <table width="180px" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td>
                                <input type="button" style="width: 90px;" value="Properties" class="left_switch active"
                                    name="tab1" id="tab1" />
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Generate" class="right_switch" name="tab2"
                                    id="tab2" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <div class="tab_content" id="tab1_content">
                <input type="hidden" runat="server" />
                <input id="ID" type="hidden" runat="server" />
                <input id="ClientSiteID" type="hidden" runat="server" />
                <input id="InsertedOn" type="hidden" runat="server" />
                <input id="InsertedBy" type="hidden" runat="server" />
                <input id="DeletedOn" type="hidden" runat="server" />
                <input id="DeletedBy" type="hidden" runat="server" />
                <input id="VoucherTypeID" type="hidden" runat="server" />
                <input id="CustomXML" type="hidden" runat="server" />
                <table border="0" cellpadding="0" cellspacing="5" width="100%">
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <input type="text" id="Name" runat="server" valtype="required" mytitle="The name which you wish to assign to this voucher" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td>
                            <input type="text" id="Description" runat="server" valtype="required" mytitle="A short piece of text that will summarise the purpose of this vouchers" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Type:
                        </td>
                        <td>
                            <select id="voucherType" onchange="$('#VoucherTypeID').val(this.value);" class="sys-template"
                                mytitle="The method which the voucher discounts a value">
                                <option value="{{ID}}">{{Name}}</option>
                            </select>
                            <input id="VoucherTypeValue" runat="server" type="text" valtype="required" mytitle="The amount this type will deduct from the total amount of purchase."/>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Conditions:
                        </td>
                        <td>
                            <div id="conditions" class="sys-template">
                                <div>
                                    <input mytitle="The business rules that need to be applied to this voucher" type="checkbox"
                                        value="{{ID}}" name="{{'fld'+Name}}" vtype="{{Algorithm}}" onclick="applyTextField($(this));" />{{Name}}
                                    <input type="text" onchange="$(this).prev().val(this.value);" style="display: none;" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="tab_content hide" id="tab2_content">
                <p>
                    <b>Here is where you create, replenish,redeem or discard vouchers for your products.</b>
                    <ul style="margin-left:15px;">
                        <li>To create vouchers simply enter the amount you require then click Generate</li>
                        <li>To redeem vouchers enter the amount you require then click Redeem</li>
                        <li>If you wish to discard the vouchers then click the Discard All button.</li>
                    </ul>
                </p>
                <br />
                <table border="0" cellpadding="0" cellspacing="5" width="100%">
                    <tr>
                        <td width="10%">
                            <input type="button" value="Generate" onclick="generateVoucherCodes();" style="width: 90px;" />
                        </td>
                        <td>
                            <input type="text" id="VoucherAmount" mytitle="The amount of vouchers to be created." />
                        </td>
                    </tr>
                    <%--This has been moved to  the redeem voucher page left it here for future purposes --%>
                    <tr style="display:none;">
                        <td>
                            <input type="button" value="Redeem" style="width: 90px;" />
                        </td>
                        <td>
                            <input type="text" id="redeemAmount" mytitle="The amount of vouchers you wish to redeem." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="Discard All" onclick="discardVouchers();" style="width: 90px;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
