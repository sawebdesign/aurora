﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="RedeemVoucher.aspx.cs" Inherits="Aurora.Manage.Voucher.RedeemVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    function hideMessages() {
        $("#MainContent_Messages").fadeOut(500);
    }
    function showMessages() {
        $("#MainContent_Messages").fadeIn(500);
    }
</script>
    <div class="onecolumn">
        <div class="header">
            <span>Redeem a Voucher</span>
        </div>
        <div class="content">
            <br class="clear" />
            <br />
            <div class="alert_info" runat="server" id="Messages" clientidmode="Predictable">
                <p runat="server" id="messagetext">
                    <img id="icon" class="mid_align" alt="success" src="../Styles/images/icon_info.png"
                        runat="server" />
                    Please enter your user name & password.
                </p>
            </div>
            <br />

            <div runat="server" id="redeemVoucher">
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td align="left" style="width: 90px;">
                            <asp:Button runat="server" ID="btnRedeem" Text="Redeem" OnClick="btnRedeem_Click" />
                        </td>
                        <td>
                            <asp:TextBox ID="redeemAmount" runat="server" ToolTip="The Voucher code you wish to redeem"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div runat="server" id="login">
                <asp:HiddenField runat="server" ID="securedSession" Value="No" />
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td>
                            User Name:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UName"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox TextMode="Password" runat="server" ID="Pwd"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" Text="Login" ID="btnLogin" OnClick="btnLogin_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div runat="server" id="MemberInfo">
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                  
                    <tr>
                        <td style="width: 219px">
                            First Name:
                        </td>
                        <td>
                            <asp:Label ID="MemberName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 219px">
                            Surame:
                        </td>
                        <td>
                            <asp:Label ID="MemberSurname" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 219px">
                            Email Address:
                        </td>
                        <td>
                            <asp:Label ID="MemberEmail" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 219px">
                            &nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnConfirm" Text="Confirm" 
                                onclick="btnConfirm_Click" />
                        &nbsp;
                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" 
                                onclick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField runat="server" ID="VoucherID" Value="" />
        </div>
    </div>
</asp:Content>
