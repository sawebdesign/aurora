﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="TestmoniesAED.aspx.cs" Inherits="Aurora.Manage.CustomerFeedBackAED" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="TestmoniesAED.aspx.js?<%=((Aurora.Manage.SiteMaster)this.Master).lastModifiedDate%>"></script>
    <div>
        <div id="contentList">
            <div class="inner">
                <!-- Begin one column window -->
                <div class="onecolumn">
                    <div class="header">
                        <span id="filter">Testimonies List</span>
                        <div style="float: left; margin-top: 10px; margin-right: 10px;">
                            <div>
                               &nbsp;
                                <select id="FeedType"  onchange="TotalCount = 0;getFeedback(); $('#contentAddEdit').hide();" sys:attach="dataview" >
                                    <option value="Pending">Pending</option>
                                    <option value="Approved">Approved</option>
                                    <option value="Declined">Declined</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br class="clear" />
                    <div class="content">
                        <table class="data" width="100%" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th align="left">
                                        Visitor Name
                                    </th>
                                    <th align="left">
                                        Posted Date
                                    </th>
                                    <th align="left">
                                        IP Address
                                    </th>
                                    <th align="left">
                                        Status
                                    </th>
                                    <th align="right">
                                        Function
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="templateList" class="sys-template" sys:attach="dataview">
                                <tr>
                                    <td width="60%">
                                        {{FirstName}}&nbsp;{{LastName != "null" ? "" : LastName}}
                                    </td>
                                    <td width="10%">
                                        {{ formatDateTime(InsertedOn,"dd/MM/yyyy")}}
                                    </td>
                                    <td width="10%">
                                        {{ InsertedByIP}}
                                    </td>
                                    <td width="10%">
                                        {{ isApproved == 0 ? (DeletedOn == null ? 'Pending' : 'Declined' ) : 'Approved'}}
                                    </td>
                                    <td align="right" width="10%">
                                        <a href="javascript:void(0);" onclick="getDetail(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_edit.png" alt="edit" class="help" title="Edit" /></a>
                                        <a href="javascript:void(0);" onclick="deleteData(this);return false;" dataid="{binding ID}">
                                            <img src="../styles/images/icon_delete.png" alt="delete" class="help" title="Delete" /></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="html_body">
                        </div>
                        <!-- Begin pagination -->
                        <div id="Pagination">
                        </div>
                        <!-- End pagination -->
                    </div>
                </div>
                <div id="Messages">
                </div>
                <asp:Panel ID="pnlAddEdit" runat="server" ClientIDMode="Static">
                    <div id="contentAddEdit">
                    </div>
                    <p style=" margin-right: -6px;" align="right">
                        <input type="button" id="btnSave" onclick="saveData(true);" value="Approve" class="Login"
                            style="margin-right: 5px" />
                        <input type="button" id="btnDecline" onclick="saveData(false);" value="Decline" class="Login"
                            style="margin-right: 5px" />
                        <input type="button" id="Cancel" onclick="cancel();" value="Cancel" class="Login"
                            style="margin-right: 5px" />
                    </p>
                </asp:Panel>
                <!-- End one column window -->
            </div>
            <!-- End content -->
        </div>
    </div>

</asp:Content>
