﻿//#region refs
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.Debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.Debug.js" />
/// <reference path="/Service/AuroraWS.asmx" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.toChecklist.js" />
/// <reference path="/Scripts/generated/Aurora.Custom.js" />
//#endregion

//#region Vars

var Clients = null;
var CurrentPage = 0;
var TotalCount = 0;
var globals = new GlobalVariables.GlobalVar();

//#endregion


//#region Pagination
function pageselectCallback(page_index, jq) {
    //$('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getSites();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getSites();
    }

    //$('div.content').unblock();
    return false;
}

function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: globals.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

//#endregion

function getSites(reset) {

    var response = function (result) {
        if (result == undefined || result.Data.length === 0) {
            $("#sites").hide();
            $("#Pagination").hide();
        } else {
            $("#Pagination").show();
            $("#sites").show();

            if (!Clients) {
                Clients = $create(Sys.UI.DataView, {}, {}, {}, $get("clientsgrid"));
                TotalCount = result.Count;
                initPagination();
            }

            TotalCount = result.Count;

            if (reset) {
            	initPagination();
            }

            Clients.set_data(result.Data);
        }

        $("#sites").unblock();
        $(".help").tipsy({ gravity: 'n', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
    };

    $("#sites").block({ message: null });

    var searchTxt = $("input[name=siteSearch]").val();

    if (searchTxt == "") {
    	searchTxt = null;
    }

    Aurora.Manage.Service.AuroraWS.GetClientSites(CurrentPage + 1, globals.PageSize, searchTxt, response, onError);

}

function setSite(SiteID, CompanyName) {

    var response = function (result) {
        if (result == true) {
            window.document.location = "/dashboard.aspx";
        }
    };

    Aurora.Manage.Service.AuroraWS.SetClientSite(SiteID, CompanyName, response, onError);
}

$(function () {

    getSites();

    $("#siteSearchInput").focus();

    $("#searchSitesButton").click(function () {
    	CurrentPage = 0;
    	getSites(true);
    });

    $("#clearSearchSitesButton").click(function () {
    	CurrentPage = 0;
    	$("#siteSearchInput").val("");
    	getSites(true);
    });

    $("#siteSearchInput").keydown(function (e) {
    	var code = e.which; // recommended to use e.which, it's normalized across browsers
    	if (code == 13) {
    		e.preventDefault();
    		CurrentPage = 0;
    		getSites(true);
    	} // missing closing if brace
    });

});