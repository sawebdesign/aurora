﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="Aurora.Manage.Product" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" media="screen" type="text/css" href="/Styles/colorpicker.css" />
    <link href="Styles/ui.slider.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Scripts/colorpicker.js"></script>
    <script type="text/javascript" src="/Scripts/ColourDropDown.js"></script>
    <script src="/Scripts/jquery.toChecklist.js" type="text/javascript"></script>
    <script src="Scripts/custom_black.js" type="text/javascript"></script>
    <script src="Product.aspx.js" type="text/javascript"></script>
    <style type="text/css">
        .groupContainer
        {
            background: #555555;
            float: left;
            margin-left: 10px;
        }
        .groupContainer:hover
        {
            background: #61a700;
        }
        .groupContainer div
        {
            padding: 5px 5px 0px 5px;
            top: 0px;
        }
        .groupContainer div img
        {
            cursor: pointer;
            width: 100px;
            height: 80px;
        }
        .groupContainer span
        {
            text-align: right;
            float: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <!-- Begin one column window -->
    <div class="onecolumn">
        <div class="header">
            <span id="ProductTitle">Editing</span>
            <div style="width: 530px;" class="switch">
                <table width="360px" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td>
                                <input type="button" style="width: 90px;" value="Details" class="left_switch active"
                                    name="tab1" id="tab1" />
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Settings" class="middle_switch"
                                    name="tab2" id="tab2" />
                            </td>
                            <td>
                                <input type="button" style="width: 90px;" value="Media" class="middle_switch" name="tab3"
                                    id="tab3" />
                            </td>
                            <td>
                                <input type="button" style="width: 100px;" value="Full Description " class="middle_switch"
                                    name="tab4" id="tab4" />
                            </td>
                            <td>
                                <input type="button" style="width: 130px;" value="Additional Information" onclick="getCurrentTabs();cEdit(this);"
                                    class="right_switch" name="tab5" id="tab5" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br class="clear" />
        <div class="content" style="padding-left: -15px;" id="ProductForm">
            <input type="hidden" id="ID" runat="server" value="" />
            <input type="hidden" id="SizeID" runat="server" />
            <input type="hidden" id="BrandID" runat="server" />
            <input type="hidden" id="ClientSiteID" runat="server" />
            <input type="hidden" id="ColourID" runat="server" />
            <input type="hidden" id="CustomXML" runat="server" />
            <input type="hidden" id="ParentID" runat="server" />
            <div class="tab_content" id="tab1_content">
                <table cellspacing="5" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <div style="margin: auto; display: block" id="login_info" class="alert_warning" jquery152014924581387261693="1">
                                <p>
                                    <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                                    <span class="failureNotification">Please note that not all fields listed below will
                                        display on the front end of the site. Fields displayed are based on the approved
                                        design of the site. Please contact support@sawebdesign.co.za to activate fields.</span>
                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Product Details</h2>
                        </td>
                    </tr>
					<tr>
                        <td>
                            Brand:
                        </td>
                        <td>
                            <select id="Brand" class="sys-template" sys:attach="dataview">
                                <option value="{{ID}}" mytitle="{{Description}}">{{Name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <input type="text" class="help" valtype="required" id="Name" runat="server" mytitle="The name to which describes the product." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Code:
                        </td>
                        <td>
                            <input type="text" class="help" valtype="required" id="Code" runat="server" mytitle="This is your reference to identify the product." />
                        </td>
                    </tr>
					<tr>
                        <td>
                            Short Description:
                        </td>
                        <td>
                            <input type="text" class="help" runat="server" id="ShortDescription" valtype="required"
                                mytitle="A small peice of text that will describe the product" maxlength="50" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Size:
                        </td>
                        <td>
                            <select id="Sizes" name="Size" class="sys-template" style="width: 204px">
                                <option value="{{ID}}">{{Description}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Colour(s):
                        </td>
                        <td>
                            <div id="ColourChoose" class="sys-template dropdownSel help" mytitle="Allows you to assign multiple colours/textures to this product"
                                sys:attach="dataview">
                                <div color="{{HexValue}}" img="{{ImageUrl}}" class="swatcher" mytitle="{{Name}}"
                                    id="{{ID}}">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Assigned Category:
                        </td>
                        <td>
                            <div class="help myfgbutton" mytitle="Allows you to change the category this product sits under"
                                style="width: 184px;">
                                <a tabindex="0" href="#news-items-2" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all"
                                    id="prodCategory"><span style="float: right; height: 10px;" class="ui-icon ui-icon-triangle-1-s">
                                    </span>Product Categories</a>
                            </div>
                            <input id="prodCategoryID" type="hidden" runat="server" />
							<input id="prodCategoryName" type="hidden" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            To link this product to <b>another</b> category, select from list:
                        </td>
                        <td>
                            <div style="float: left; width: auto;">
                                <select id="AllCats" name="AllCats" title="AllCats" style="height: 240px;min-width: 200px;
                                    max-width: 400px;" multiple="multiple" class="sys-template">
                                    <option sys:value="{{ID}}">{{Name}} {{categories.findCatInResult(ParentID,null,"&nbsp;({0})")}}</option>
                                </select>
                            </div>
                            <div style="float: left; display: block; width: auto;">
                                <ul id="AllCats_selectedItems">
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Specification:
                        </td>
                        <td>
                            <input type="text" class="help" valtype="required" id="Specification" runat="server"
                                mytitle="Technical data may be expressed here." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Price:
                        </td>
                        <td>
                            <input type="text" class="help" valtype="required" id="Price" runat="server" mytitle="Retail price of the product." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order:
                        </td>
                        <td>
                            <input type="text" class="help" valtype="required" id="OrderIndex" runat="server"
                                mytitle="This will set the priority of this product,the lower numbers take presidence." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Hidden:
                        </td>
                        <td>
                            <input type="checkbox" class="help" valtype="required" id="isHidden" runat="server"
                                mytitle="This when checked will hide the product from the front-end of the website." />
                        </td>
                    </tr>

					<tr><td colspan="2"><h3 class="subheading">Digital Products</h3></td></tr>
                    <tr>
                        <td>
                            Download URL:
                        </td>
                        <td>
                            <input type="text" class="help" id="DownloadURL" runat="server" mytitle="The full URL were a product can be downloaded. http://sitename.com/clientdata/10000/products/test.zip" />
                        </td>
                    </tr>
					<tr>
						<td colspan="2">
							<h3 class="subheading">Shipping</h3>
						</td>
					</tr>
                    <tr>
                        <td>
                            No Shipping:
                        </td>
                        <td>
                            <input type="checkbox" class="help" id="isZeroShipping" runat="server" mytitle="Usually for downloadable products." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fixed Shipping:
                        </td>
                        <td>
                            <input type="checkbox" class="help" id="isFixedShipping" runat="server" mytitle="Usually for magazines products" />
                        </td>
                    </tr>
					<tr class="fixedShipping">
                        <td>
                            Special Shipping Cost Local:
                        </td>
                        <td>
                            <input type="text" class="help" id="ShippingPriceLoc" runat="server" mytitle="Special shipping cost for a single item shipment. Use this to overide weight calculations on items that can be shipped singly for a different amount. Leave blank to allow weight calculation. Only applies to SAPO shipping." />
                        </td>
                    </tr>
                    <tr class="fixedShipping">
                        <td>
                            Special Shipping Cost International:
                        </td>
                        <td>
                            <input type="text" class="help" id="ShippingPriceInt" runat="server" mytitle="Special shipping cost for a single item shipment. Use this to overide weight calculations on items that can be shipped singly for a different amount. Leave blank to allow weight calculation. Only applies to SAPO shipping." />
                        </td>
                    </tr>
					<tr>
						<td colspan="2">
							<h3 class="subheading">Dimensions</h3>
						</td>
					</tr>
                    <tr>
                        <td>
                            Height (mm):
                        </td>
                        <td>
                            <input type="text" class="help" id="Height" runat="server" mytitle="The metric height of the product." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Width (mm):
                        </td>
                        <td>
                            <input type="text" class="help" id="Width" runat="server" mytitle="The metric width of the product." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Length (mm):
                        </td>
                        <td>
                            <input type="text" class="help" id="Length" runat="server" mytitle="The metric Length of the product." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Weight (grams):
                        </td>
                        <td>
                            <input type="text" class="help" id="Weight" runat="server" mytitle="the metric weight of the product." />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Description:
                        </td>
                        <td>
                            <textarea cols="25" rows="5" class="help" style="font-family: Arial;" valtype="required"
                                id="Description" runat="server" mytitle="A little paragraph that explains the usages of the product."></textarea>
                        </td>
                    </tr>
					<tr>
						<td colspan="2">
							<h3 class="subheading">Inventory</h3>
						</td>
					</tr>
                    <tr>
                        <td>
                            Stock Availability:
                        </td>
                        <td>
                            <input name="StockAvailablity" id="StockAvailablity" runat="server" class="help"
                                mytitle="Displays a stock status for this product" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Stock Item Total:
                        </td>
                        <td>
                            <input name="StockItemTotal" id="Total" runat="server" class="help" mytitle="The total items in stock for this product" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Custom Fields</h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="margin: auto; display: block" id="login_info" class="alert_warning">
                                <p>
                                    <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                                    <span class="failureNotification">Please note that not all fields listed below will
                                        display on the front end of the site. Fields displayed are based on the approved
                                        design of the site. Please contact support@sawebdesign.co.za to activate fields.</span>
                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr id="CustomData">
                        <td colspan="2" id="CustomFields">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="tab_content" id="tab2_content" style="min-height: 494px;">
                <table cellspacing="5" cellpadding="0" border="0" width="100%">
                    <tr style="display: none;">
                        <td>
                            Template:
                        </td>
                        <td>
                            <input type="checkbox" class="help" id="IsTemplate" runat="server" mytitle="Converts this entry into a generic, that can be applied to all products" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            Featured Product:
                        </td>
                        <td>
                            <input type="checkbox" class="help" id="IsFeatured" runat="server" mytitle="Decides wether to display this product on your product home page." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>
                                Product Specials</h2>
                        </td>
                    </tr>
                    <tr id="NoSpecial" style="display: none">
                        <td colspan="2">
                            <div style="margin: auto; display: block" id="Div1" class="alert_warning">
                                <p>
                                    <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                                    <span class="failureNotification">This functionality has not been specified. Please
                                        contact support@sawebdesign.co.za for further information.</span>
                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <input id="specialID" type="hidden" value="-1" />
                        <td>
                            Place Product on special:
                        </td>
                        <td>
                            <input id="specialOn" type="checkbox" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td>
                            <input id="specialFrom" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td>
                            <input id="specialTo" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            New Price:
                        </td>
                        <td>
                            <input id="specialPrice" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input id="AddSpecial" onclick="product.addSpecial();" type="button" value="Place Product on Special" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>
                                Associated Products</h2>
                        </td>
                    </tr>
                    <tr id="NoAssociated" style="display: none">
                        <td colspan="2">
                            <div style="margin: auto; display: block" id="Div2" class="alert_warning">
                                <p>
                                    <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                                    <span class="failureNotification">This functionality has not been specified. Please
                                        contact support@sawebdesign.co.za for further information.</span>
                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Select a product category:
                        </td>
                        <td>
                            <select id="AssProdCategories" onchange="loadProducts();" class="sys-template">
                                <option sys:value="{{ID}}">{{Name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Products:
                        </td>
                        <td id="ProdSel">
                            <select id="AssProducts" name="AssProducts" title="Products" style="height: 200px"
                                multiple="multiple" class="sys-template">
                                <option sys:value="{{ID}}">{{Name}}</option>
                            </select>
                            <div style="float: right; display: block;">
                                <ul id="AssProducts_selectedItems">
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button" onclick="product.associate();" value="Associate Selected Items" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="tab_content" id="tab3_content" style="min-height: 494px;">
                <table cellspacing="5" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <h2>
                                Media</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4>
                                Images <a>
                                    <img id="AddImageBtn" src="Styles/images/add.png" dataid="-1" class="help" alt="Add an Image"
                                        mytitle="Add an Image" style="cursor: pointer" onclick="$('#imgID').val(0);$('#AddImage').slideToggle(500);$(this).mouseout(); this.setAttribute('disabled','disabled');$('imgID').val(0);" /></a></h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="AddImage" style="float: left; display: none; width: 100%;">
                                <div style="float: left;">
                                    <table>
                                        <input type="hidden" id="imgID" value="0" />
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Name:
                                                </td>
                                                <td>
                                                    <input id="imgName" valtype="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Description:
                                                </td>
                                                <td>
                                                    <input id="imgDescrip" valtype="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Upload Album Item(mouse over to view):
                                                </td>
                                                <td>
                                                    <a id="btn_image1" href="#" onclick="gallery.add(currentItemID,'Images'); return false;"
                                                        class="help" mytitle="Upload/Delete media">
                                                        <img src="Styles/images/icon_media.png" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" value="Save Changes" onclick="gallery.add(currentItemID,'Images');" />&nbsp;<input
                                                        onclick="$('#imgDescrip').val('');$('#imgName').val('');$('#AddImage').slideToggle(500);$('#AddImageBtn').removeAttr('disabled'); "
                                                        type="button" value="Cancel" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="NoneImages" style="float: left">
                                No Images available.</div>
                            <div id="galleryImages" sys:attach="dataview" class="sys-template" style="width: 100%;">
                                <div class="groupContainer">
                                    <div>
                                        <a rel="productImages" class="group" onclick="gallery.showImage(this);" sys:title="{{Name}}"
                                            sys:desc="{{Description}}" sys:largeimage="{{ client.replace('~', '..')+'/Uploads/product_'+ID+'.jpg'}}">
                                            <img sys:id="{{'image_'+ID}}" sys:src="{{ checkImage(ID,client.replace('~', '..')+'/Uploads/sm_product_'+ID+'.jpg?'+new Date().getMilliseconds())}}" /></a></div>
                                    <span class="buttons"><a href="javascript:void(0);" dataid="{{ID}}" onclick="gallery.editItem(currentItemID,'Images',this.getAttribute('dataid'),false); return false;">
                                        <img src="../styles/images/icon_edit.png" sys:dataid="{{ID}}" class="img" mytitle="Edit"
                                            mytitle="Add an Image" /></a> <a href="#" onclick="$(this).mouseout();$('#imgID').val($(this).attr('dataid'));gallery.add(currentItemID,'Images',true);return false;"
                                                dataid="{{ID}}">
                                                <img src="../styles/images/icon_delete.png" sys:dataid="{{ID}}"  class="img" mytitle="Delete" />
                                            </a></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <%--<tr>
                            <td>
                                <h4>
                                    Videos<a><img src="Styles/images/add.png" dataid="-1" class="help" alt="Add a Video"
                                        mytitle="Add a Video" style="cursor: pointer" /></a></h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="addVideo" style="float: left; display: none; width: 100%;">
                                    <div style="float: left;">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Name:
                                                    </td>
                                                    <td>
                                                        <input id="vidName" valtype="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <input id="vidDescription" valtype="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Upload Album Item:
                                                    </td>
                                                    <td>
                                                        <a id="A1" href="#" onclick="gallery.add(currentItemID,'Images'); return false;"
                                                            class="help" mytitle="Upload/Delete media">
                                                            <img src="Styles/images/icon_media.png" /></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="Save Changes" onclick="gallery.add(currentItemID,'Images');" />
                                                        &nbsp;<input onclick="$('#AddImage').slideToggle(500);$('#AddImageBtn').removeAttr('disabled');"
                                                            type="button" value="Cancel" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="galleryVideo" sys:attach="dataview" class="sys-template" style="width: 100%;">
                                    <div class="groupContainer">
                                        <div>
                                            <a rel="productImages" class="group" onclick="gallery.showImage(this);" sys:title="{{Name}}"
                                                sys:desc="{{Description}}" sys:largeimage="{{VideoURL}}">
                                                <img sys:src="{{'/ClientData/10003/Uploads/sm_product_'+ID+'.jpg'+new Date().getMilliseconds()}}" /></a></div>
                                        <span class="buttons"><a href="#" onclick="" dataid="{{ID}}">
                                            <img src="../styles/images/icon_edit.png" class="img" mytitle="Edit" mytitle="Add an Image" /></a>
                                            <a href="#" onclick="" dataid="{{ID}}">
                                                <img src="../styles/images/icon_delete.png" class="img" mytitle="Delete" />
                                            </a></span>
                                    </div>
                                </div>
                                <div id="NoneVideos">
                                    No Videos available.</div>
                            </td>
                        </tr>--%>
                </table>
            </div>
            <div class="tab_content" id="tab4_content">
                <div style="margin: auto; display: block" id="Div3" class="alert_warning">
                    <p>
                        <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                        <span class="failureNotification">Please note that not all fields listed below will
                            display on the front end of the site. Fields displayed are based on the approved
                            design of the site. Please contact support@sawebdesign.co.za to activate fields.</span>
                    </p>
                </div>
                <textarea id="DetailText" clientidmode="Static" runat="server" style="display: none;"
                    name="DetailText" rows="4" cols="30"></textarea>
                <span id="editorSpan"></span>
            </div>
            <div class="tab_content" id="tab5_content">
                <div style="margin: auto; display: block" id="Div4" class="alert_warning">
                    <p>
                        <img class="mid_align" alt="success" src="../styles/images/icon_warning.png">
                        <span class="failureNotification">Please note that not all fields listed below will
                            display on the front end of the site. Fields displayed are based on the approved
                            design of the site. Please contact support@sawebdesign.co.za to activate fields.</span>
                    </p>
                </div>
                <h2>
                    Additional Info</h2>
                <p>
                    <br />
                    This information appears in the form of tabbed details within your products module.It
                    can only be viewed when a user wishes to view the full detail of a product.
                    <br />
                    It can be used to specify more information on a product like warranty information
                    or documents and so on.<br />
                </p>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height: 30px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="onecolumn">
                                <div class="header">
                                    <span>Current Tabs</span>
                                    <div style="float: right; margin-top: 10px; margin-right: 10px;">
                                        <img src="Styles/images/add.png" dataid="-1" class="img" alt="Create Tab" mytitle="Create Tab"
                                            style="cursor: pointer" onclick="addTab();" />
                                    </div>
                                </div>
                                <br class="clear" />
                                <div class="content">
                                    <table class="data" width="100%" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th align="left">
                                                    Tab Name
                                                </th>
                                                <th align="left">
                                                    Order
                                                </th>
                                                <th align="right">
                                                    Functions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabDataView" class="sys-template" sys:attach="dataview">
                                            <tr>
                                                <td width="20%">
                                                    {{name}}
                                                </td>
                                                <td width="20%">
                                                    {{order}}
                                                </td>
                                                <td align="right" width="10%">
                                                    <a class="productGrid" href="javascript:void(0);" onclick="currentTab=this;editTab(this);return false;"
                                                        sys:content="{{desc}}" sys:tabname="{{name}}" sys:order="{{order}}">
                                                        <img src="../styles/images/icon_edit.png" class="img" mytitle="Edit" /></a>
                                                    <a href="javascript:void(0);" onclick="deleteTab($(this).parent().parent()); return false;">
                                                        <img src="../styles/images/icon_delete.png" class="img" mytitle="Delete" /></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="onecolumn" id="tabedit" style="display: none;">
                                <div class="header">
                                    <span id="tabTitle">Editing:</span>
                                </div>
                                <br class="clear" />
                                <div class="content">
                                    <table border="0" width="100%" cellpadding="0" cellspacing="2" id="tabForm">
                                        <tr>
                                            <td>
                                                Tab Name:
                                            </td>
                                            <td>
                                                <input id="tabName" name="fldTabName" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tab Order:
                                            </td>
                                            <td>
                                                <input id="tabOrder" name="TabOrder" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tab Content:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input type="hidden" id="fldTabDescription" name="fldTabDescription" value="" />
                                                <textarea id="DescDetail" clientidmode="Static" runat="server" style="display: none;"
                                                    name="DescDetail" rows="4" cols="30"></textarea>
                                                <span id="DescSpan"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" value="Save Tab" onclick="getTabs();" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- End one column window -->
        </div>
    </div>
    </form>
</body>
</html>
