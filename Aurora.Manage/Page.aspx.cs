﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom.Data;
using Aurora.Manage.Service;
using Aurora.Custom;

namespace Aurora.Manage
{
    public partial class Page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionManager.UserRoles.Select(p => p.RoleName).ToArray().Contains("CustomMenu")) {
                MenuClassBody.Visible = true;
            }
            AuroraWS webService =  new AuroraWS();
            parentindex.InnerHtml =string.Format("{0}{1}{2}.","<b>",Request["index"],"</b>");
            PageContent pageDetail = (PageContent)webService.GetPages(Request["PageId"] != null ? Int32.Parse(Request["PageId"]) : -1 )["Data"];
            if (pageDetail != null)
            {
                
                ClientSiteID.Value = pageDetail.ClientSiteID.Value.ToString();
                ParentId.Value = pageDetail.ParentId.Value.ToString();
                ID.Value = pageDetail.ID.ToString();
                PageIndex.Value = pageDetail.PageIndex.Value.ToString();
                if (PageIndex.Value == "-1")
                {
                    PageIndex.Disabled = false;
                    showPage.Checked = false;
                }
                DetailText.Value = pageDetail.PageContent1;
                LongTitle.Value = pageDetail.LongTitle;
                ShortTitle.Value = pageDetail.ShortTitle;
                Description.Value = pageDetail.Description;
                MetaKeywords.Value = pageDetail.MetaKeywords;
                PageTitle.Value = pageDetail.PageTitle;
                MetaDescription.Value = pageDetail.MetaDescription;
                CustomXML.Value = pageDetail.CustomXML;
                isDefault.Checked = pageDetail.isDefault;
                //InsertedOn.Value = pageDetail.InsertedOn.ToString("dd/MM/yyyy"); Disabled due to different client versions reading this strangely. Shouldn't cause any code problems
                InsertedBy.Value = pageDetail.InsertedBy.Value.ToString();
                isArchived.Value = pageDetail.isArchived.ToString();
                RedirectURL.Value = pageDetail.RedirectURL;
				PermanentRedirect.Checked = pageDetail.PermanentRedirect;
                ClientModuleID.Value = Utils.ConvertDBNull<long>(pageDetail.ClientModuleID,0).ToString();
                if (pageDetail.DeletedOn != null) DeletedOn.Value = pageDetail.DeletedOn.Value.ToString();
                if (pageDetail.DeletedBy != null) DeletedBy.Value = pageDetail.DeletedBy.Value.ToString();
                IsSecured.Checked = Utils.ConvertDBNull(pageDetail.IsSecured,false);
                UserName.Value = pageDetail.UserName;
                Password.Value = pageDetail.Password;
                MenuClass.Value = Utils.ConvertDBNull<string>(pageDetail.MenuClass, "");

                AuroraEntities context = new AuroraEntities();
                var moduleTemplate = from modules in context.Module
                                     join clientModule in context.ClientModule on modules.ID equals clientModule.FeaturesModuleID 
                                     where modules.ID == pageDetail.ClientModuleID
                                     select clientModule;
                if (moduleTemplate.Count() > 0)
                {
                    //ModuleDetails.Value = moduleTemplate.FirstOrDefault().HTML;    
                }

                if (File.Exists(Server.MapPath(SessionManager.ClientBasePath + "/uploads/page_" + pageDetail.ID + ".jpg")))
                {
                    PageImage.ImageUrl = "/ClientData/" + SessionManager.ClientSiteID + "/uploads/page_" + pageDetail.ID + ".jpg?"+DateTime.Now.Ticks.ToString();
                    PageImage.Visible = true;
                }

            }
            else
            {
                ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();
            }

            if (Request["del"] == "1")
            {
                if (File.Exists("/ClientData/" + SessionManager.ClientSiteID + "/uploads/page_" + pageDetail.ID + ".jpg"))
                {
                    File.Delete(
                        Server.MapPath("/ClientData/" + SessionManager.ClientSiteID + "/uploads/page_" + pageDetail.ID +
                                       ".jpg"));
                }
            }
        }
    }
}