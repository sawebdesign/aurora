﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Custom;

namespace Aurora.Manage
{
    public partial class CustomerFeedBack : System.Web.UI.Page
    {
        AuroraEntities service = new AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["Feedback"] != null) {
                Utils.CheckSession();
                int feedbackId;
                int.TryParse(Request["Feedback"],out feedbackId);
                var feeback = (from vistors in service.CustomerFeedback
                              where vistors.ID == feedbackId
                              select vistors).SingleOrDefault();
                if (feeback != null)
                {
                    InsertedByIP.Value = feeback.InsertedByIP;
                    InsertedOn.Value = feeback.InsertedOn.Value.ToString();
                    DeletedOn.Value = Utils.ConvertDBNull<string>(feeback.DeletedOn, string.Empty);
                    DeletedBy.Value = Utils.ConvertDBNull<string>(feeback.DeletedBy, string.Empty);
                    FirstName.Value = feeback.FirstName;
                    LastName.Value = feeback.LastName;
                    ContactNumber.Value = feeback.ContactNumber;
                    Email.HRef = string.Format("mailto:{0}?subject=Thank you for you feedback", feeback.Email);
                    Email.InnerHtml = feeback.Email;
                    Comment.Value = feeback.Comment;
                    ID.Value = feeback.ID.ToString();
                    Preview.Value = feeback.Preview;
                    ClientSiteID.Value = feeback.ClientSiteID.Value.ToString();    
                }
                
            }
        }
    }
}