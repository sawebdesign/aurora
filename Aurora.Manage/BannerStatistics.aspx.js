﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx" />

var ready = 0;
var TotalCount = 0;
var CurrentPage = 0;
var Global = new GlobalVariables.GlobalVar();
var bannerID;
var dataStats = null;

$(function () {
    $(".content").block({message:null});
    $("body").css("background", "");
    bannerID = getQueryVariable("BannerID");
    if (bannerID != "" || bannerID != null) {
        //Get Banner
        getBanner(bannerID);
        //Load click Stats
        getStats(bannerID);
    }

});

function getBanner(id) {
    /// <summary>Gets a  specified banner</summary>
    var success = function (result) {
        var dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("templateList"));
        dataSet.set_data(result.Data);
        $("#name").html($("#name").html().replace("%CampName%", result.Data[0].Name));
        unblock();
    }

    Aurora.Manage.Service.AuroraWS.GetBanners(0, 0, "", id,false,success,onError);
}

function getStats(id) {
    /// <summary>Gets click details for a banner</summary>
    var success = function (result) {
        if (dataStats == null) {
            dataStats = $create(Sys.UI.DataView, {}, {}, {}, $get("BannerClickedView"));
        }
        dataStats.set_data(result.Data);
        if (TotalCount == 0) {
            TotalCount = result.Count;
            Global.PageSize = 5;
            initPagination();
        }
        TotalCount = result.Count;

        unblock();
    }

    Aurora.Manage.Service.AuroraWS.GetBannerClicks(CurrentPage + 1,5,"",id, success, onError);
}

function unblock() {
    ready++;
    if (ready >= 2) {
        $(".content").unblock()
    }
}

function pageselectCallback(page_index, jq) {
    $('div.content').block({ message: null });
    if (page_index > 0) {
        //skip getData() on the first load or if causes and error
        CurrentPage = page_index;
        getStats(bannerID);
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run getData() if we are going back to the fist page
        CurrentPage = 0;
        getStats(bannerID);
    }

    $('div.content').unblock();
    return false;
}
function initPagination() {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: pageselectCallback,
        items_per_page: Global.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}