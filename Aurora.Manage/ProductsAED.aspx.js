﻿/// <reference path="Scripts/jquery-1.5.2.js" />
/// <reference path="Scripts/MicrosoftAjax.js" />
/// <reference path="Scripts/MicrosoftAjaxTemplates.js" />
/// <reference path="Service/AuroraWS.asmx/jsdebug" />
/// <reference path="Scripts/Utils.js" />
/// <reference path="Scripts/jquery.toChecklist.js" />
/// <reference path="Scripts/generated/Aurora.Custom.js" />

//#region Vars 
var CurrentPage = 0;
var PageSize = 0;
var TotalCount = 0;
var globalVars = new GlobalVariables.GlobalVar;
var catData = null;
var webService = Aurora.Manage.Service.AuroraWS;
var template = null;
var productDetail = "";
var colourTemplate = null;
var datasetSizes = null;
var datasetColours = null;
var datasetBrands = null;
var defferredColours = $.Deferred();
var datasetGalleryImages = null;
var datasetGalleryVideo = null;
var prodHasData = null;
var currentItemID = -1;
var parentProductID = 0;
var itemSaved = false;
var productCategories = null;
var linkCats = null;
var assProdList = null;
var currentAssociatedProducts = null;
var tabData = null;
var currentTab = null;
var assData = null;
var isNewSize = false;
var currentSizeName = "";
var categoryData = null;
var editAdd = "<ul><li><a href='javascript:' id='0' parentid='0' onclick='editsetCat(this);'>Current</a></li>";
var reloadProductCatList = false;
var currentCatPage = 0;
var CatTotal = 0;
var categoriesForFind = null;
var imageGalleryTemplate = null;
//#endregion

//#region Init 
$(function () {

    categories.getCategoriesForDropDown();

});
//#endregion

//#region Data Manipulation
var product = {
    actionStart: function (element) {
        if (element) {
            $(element).block({ message: null });
        } else {
            $(".content").block({ message: null });
        }
    },
    actionComplete: function (element) {
        if (element) {
            $(element).unblock();
        } else {
            $(".content").unblock();
        }
    },
    edit: function (productID) {
        product.actionStart();
        var loadSuccess = function (responseText, status, XHR) {
            if (status = "error" && responseText.indexOf("<title>SessionExpired</title>") != -1) {
                var obj = {};
                obj._message = "SessionExpired"
                onError(obj);
            }
            else {
                $("#pnlAddEdit").show();
                $("#Messages").insertBefore("#pnlAddEdit");
                scrollToElement("Messages", -100, 0);
                if (!prodHasData) {
                    $("#prodGrid").parent().toggle(500);
                }
                $("#contentAddEdit").unblock();
                $("#contentAddEdit").show();
                $("#btnSave").show();
                $("#Cancel").show();
                if (currentItemID == -1) {
                    $(".middle_switch,.right_switch").attr("disabled", "disabled");
                    DisplayMessage("Messages", "warn", "Certain elements have been disabled, please enter product information then save to activate these elements");
                }
                else {
                    $(".middle_switch,.right_switch").removeAttr("disabled");
                }
            }

            product.actionComplete();
        };

        if (productID == "-1") {
            product.actionStart();
            currentItemID = -1;
            var x = new Date();
            $("#Messages").insertAfter("#products");
            DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
            scrollToElement("Messages", -20, 2);
            $("#contentAddEdit").load("Product.aspx?ProductID=" + productID + "&var=" + x.getMilliseconds(), loadSuccess);
        }
        else {
        	var parentId = isNewSize ? productID : "";
            if (parentId != "") {
                currentItemID = -1;
                parentProductID = productID;
            }
            else {
            	currentItemID = productID;
            }

            product.actionStart();
            var x = new Date();
            $("#Messages").insertAfter("#products");
            DisplayMessage("Messages", "info", "Loading Information please wait.. <img src='Styles/images/message-loader.gif'/>", 0);
            scrollToElement("Messages", -20, 2);
            isNewSize = false;
            $("#contentAddEdit").load("Product.aspx?ProductID=" + productID + "&var=" + x.getMilliseconds() + "&parent=" + parentId, loadSuccess);
        }


    },
    remove: function (productItem) {
        var request = function (result) {
            if (result.Result) {
                DisplayMessage("Messages", "success", "Product has been removed", 5000);
                $("#contentAddEdit").hide(500);
                $("#btnSave").hide();
                product.display();
                scrollToElement("Messages", -20, 2);
            }
        };
        var promp = confirm("Do you wish to permanantly delete this item?");
        if (promp) {
            Aurora.Manage.Service.AuroraWS.RemoveProduct(productItem, request, onError);
        }

    },
    update: function (productItem) {
        product.actionStart();

        if (validateForm("ProductForm", true, "Messages", false)) {
            $("#contentAddEdit").unblock();
            scrollToElement("Messages", -20, 2);
            return false;
        }
        var onSuccess = function (result) {
            product.actionStart();
            if (result.Result && result.Count != 1) {
                itemSaved = true;
                product.actionComplete();
                product.display(undefined, result.Data.ID);
                DisplayMessage("Messages", "success", "Product has been saved", 5000);
                scrollToElement("Messages", -20, 100);
                getCurrentTabs();
            }
            else if (result.Count == 1) {
                DisplayMessage("Messages", "warn", "A Product with that size already exists.", false);
                scrollToElement("Messages", -20, 100);
                product.actionComplete();
            }
            else {
                onError(result);
                scrollToElement("Messages", -20, 100);
            }
        };

        //local vars
        var productEntity = new Object();
        var sizeID;
        productEntity.EntityKey = new Object();
        productEntity.EntityKey.EntityKeyValues = new Array();
        objKey = new Object();

        productEntity.ID = currentItemID == "" ? -1 : currentItemID;

        productEntity.ClientSiteID = 0;
        productEntity.SizeID = $("#Sizes").val();
        productEntity.Name = $("#Name").val();
        productEntity.Code = $("#Code").val();
        productEntity.OrderIndex = $("#OrderIndex").val();
        productEntity.isHidden = $("#isHidden").is(":checked");
        productEntity.Description = oEdit1.getHTMLBody();
        productEntity.Specification = $("#Specification").val();
        productEntity.Price = parseFloat($("#Price").val());
        
        productEntity.InsertedOn = new Date();

        productEntity.DownloadURL = $('#DownloadURL').val();
        productEntity.isZeroShipping = $("#isZeroShipping").is(":checked");
        productEntity.isFixedShipping = $("#isFixedShipping").is(":checked");
        productEntity.ShippingPriceLoc = $('#ShippingPriceLoc').val();
        productEntity.ShippingPriceInt = $('#ShippingPriceInt').val();

        productEntity.IsFeatured = $("#IsFeatured").is(":checked");
        productEntity.Availablity = $("#StockAvailablity").val();
        productEntity.Total = parseInt($("#Total").val().toString().isNullOrEmpty(0));
        productEntity.Short_Description = $("#ShortDescription").val();
        parentProductID = $("#ParentID").val();
        productEntity.ParentID = parentProductID;
        if (isNaN(productEntity.Total)) {
            productEntity.Total = 0;
        }
        productEntity.isTemplate = $("#IsTemplate").is(":checked");
        productEntity.Weight = $("#Weight").val().length > 0 ? $("#Weight").val() : 0.00;
        productEntity.Height = $("#Height").val().length > 0 ? $("#Height").val() : 0.00;
        productEntity.Length = $("#Length").val().length > 0 ? $("#Length").val() : 0.00;
        productEntity.Width = $("#Width").val().length > 0 ? $("#Width").val() : 0.00;
        //Entity object
        productEntity.EntityKey.EntityContainerName = "AuroraEntities";
        productEntity.EntityKey.EntitySetName = "Product";
        productEntity.EntityKey.IsTemporary = false;
        productEntity.CategoryID = $("#CatParentVal").val();
        productEntity.CategoryID = Number($('#prodCategoryID').val()) == 0 ? $("#CatParentVal").val() : Number($('#prodCategoryID').val());

        objKey.Key = "ID";
        objKey.Value = parseInt(productEntity.ID);
        productEntity.EntityKey.EntityKeyValues[0] = objKey;
        productEntity.CustomXML = $("#CustomXML").val();

        sizeID = parseInt($("#Sizes").val());
        if ($(".swatch").data("selected") && $(".swatch").data("selected").toString().isNullOrEmpty(-2) == -2) {
            $(".swatch").data("selected", -1);
        }
        var colours = $(".swatch").data("selected") ? $(".swatch").data("selected").toString().split(',') : [0];

        $(colours).each(function (index) {
            if (this == "") {
                colours.splice(index, 1);
            }
            else {
                colours[index] = parseInt(this);
            }
        });
        var brandID = $("#Brand").val();
        if (brandID == "{{ID}}") {
            brandID = 0;
        }

        if (productEntity.ParentID == "") {
            productEntity.ParentID = 0;
        }

        var fields = $('.CustomField input').serializeArray();
        var customXML = '<XmlData>';
        if (fields.length > 0) {
            customXML += '<Fields type="CustomFields">';
            for (var i = 0; i < fields.length; i++) {
                customXML += '<field fieldname="' + fields[i].name.replace("fld", "") + '" value="' + fields[i].value + '" />';
            }
            customXML += '</Fields>';
        }
        customXML += '</XmlData>';


        var newItems = [];

        $("#AllCats_selectedItems li").each(
            function (index, Element) {
                newItems.push(Number($(this).attr("id").toString().replace("AllCats_", "")));
            }
        );
        webService.UpdateProduct(productEntity, parseInt(brandID), sizeID, colours, customXML, newItems, onSuccess, onError);


    },
    display: function (status, prdID) {
        /// <summary>Presents all products to the user</summary>
        product.actionStart();
        if (status == undefined) {
            status = "";
        }
        var onSuccess = function (result, editProdID) {
            if (result.Result) {
                var cleanedData = [];

                for (var row = 0; row < result.Data.length; row++) {
                    if (result.Data[row] != null) {
                        cleanedData.push(result.Data[row]);
                    }
                }


                if (template == null) {
                    template = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
                }

                if ($("#" + $("#CatParentVal").val()).children('span').length > 0) {
                    $("#hierarchybreadcrumb").children().first()[0].nextSibling.nodeValue = decodeHTML($("#" + $("#CatParentVal").val()).children('span').first().html());
                }
                else if ($("#" + $("#CatParentVal").val()).length > 0) {
                    $("#hierarchybreadcrumb").children().first()[0].nextSibling.nodeValue = decodeHTML($("#" + $("#CatParentVal").val()).html());
                }
                $("#CatURL").html(String.format("URL: /Products/{0}/{1}", $("#CatParentVal").val(), $("#hierarchybreadcrumb").children().first()[0].nextSibling.nodeValue));
                if (TotalCount == 0) {
                    TotalCount = result.Count;
                    initPagination();
                }
                TotalCount = result.Count;
                template.set_data(cleanedData);
                //check if children exist
                $("#template a[hasChildren='0']").hide();
                $(".data").show();
                if (!itemSaved) {
                    $("#Messages").hide();
                }
                else {
                    itemSaved = false;
                }
                prodHasData = true;

                if (cleanedData.length == 0) {
                    $("#Messages").appendTo(".content");
                    $(".data").hide();
                    $("#Messages").show();
                    DisplayMessage("Messages", "warn", "There are no products within this category");
                }
            }
            else {
                $("#Messages").appendTo(".content");
                if (!$("#CategoryListContainer").is(":visible")) {
                    $(".data").hide();
                    DisplayMessage("Messages", "warn", "There are no products within this category");
                }

            }
            $(".tipsy").remove();
            $(".img").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            if (prodHasData) {
                $("#prodGrid").show();
            }
            if (editProdID) {
            	product.edit(editProdID);
            }
            product.actionComplete();
        };

        webService.GetProductsWithChildren(CurrentPage + 1, globalVars.PageSize, $("#CatParentVal").val(), 0, onSuccess, onError, prdID);

    },
    associate: function () {
        var assItems = [];
        var response = function (result) {
            DisplayMessage("Messages", "success", "Items have been associated with this product", 7000);
            scrollToElement("Messages", -20, 100);
            $(".contents").unblock();
        };

        $("#AssProducts_selectedItems").children('li').each(function (index, domEle) {
            // domEle == this
            if (parseInt($(this).attr("id").toString().replace("AssProducts_", "")) != currentItemID) {
                assItems.push(parseInt($(this).attr("id").toString().replace("AssProducts_", "")));
            }
            else {
                assItems = [];
                DisplayMessage("Messages", "warn", "You are not allowed to associate a product to itself.", 7000);
                $(".contents").unblock();
                scrollToElement("Messages", -20, 100);
                return;
            }

        });

        if (currentItemID == -1) {
            DisplayMessage("Messages", "warn", "Please save this product before you associate other products to it.", 7000);
            $(".contents").unblock();
            scrollToElement("Messages", -20, 100);
            return;
        }

        $(".contents").block({ message: null });


        Aurora.Manage.Service.AuroraWS.UpdateAssociatedProducts(assItems, currentItemID, response, onError);


    },
    showChildren: function (objElem) {
        var response = function (result) {
            if (result.Data[0] != null) {
                $(result.Data).each(function (index, domEle) {
                    // domEle == this
                    $(String.format("<tr class='{5}'><td><img src=\"../styles/images/childPage.png\"/>&nbsp;{1}</td><td>/Product/{7}/{8}</td><td>{2}</td><td>{3}</td><td>{6}</td><td align='right'>{4}</td></tr>",
                this.products.ID,
                this.products.Name,
                this.Description,
                this.products.Price,
                "<a href=\"javascript:void(0);\" onclick=\"product.edit(" + this.products.ID + ");return false;\" dataid='" + this.products.ID + "'><img src=\"../styles/images/icon_edit.png\" class=\"img\" mytitle=\"Edit\" /></a><a href=\"javascript:void(0);\" onclick=\"product.remove(this.getAttribute('dataid'));return false;\"dataid='" + this.products.ID + "'><img src=\"../styles/images/icon_delete.png\" class=\"img\" mytitle=\"Delete\" /></a>",
                 $(objElem).attr("dataid"),
                 this.products.OrderIndex,
                 this.products.CategoryID,
                 this.products.ID
                )).insertAfter($(objElem).parent().parent());

                });
            }

            $(".tipsy").remove();
            $(".img").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });
            product.actionComplete();
            $(objElem).attr("state", "open");
            $(objElem).attr("loaded", "true");
        };

        if ($(objElem).attr("state") == "closed" && $(objElem).attr("loaded") == "false") {
            product.actionStart();
            Aurora.Manage.Service.AuroraWS.GetChildProducts($(objElem).attr("dataid"), response, onError);
        }
        else if ($(objElem).attr("loaded") == "true" && $(objElem).attr("state") == "closed") {
            $("." + $(objElem).attr("dataid")).show();
            $(objElem).attr("state", "open");
        }
        else {
            $("." + $(objElem).attr("dataid")).hide();
            $(objElem).attr("state", "closed");
        }

    },
    getSpecial: function () {
        $("#tab_content2").block({ message: null });
        $("#specialOn").change(function () {
            if (!$(this).is(":checked")) {
                $("#specialFrom").attr("disabled", "disabled");
                $("#specialTo").attr("disabled", "disabled");
                $("#specialPrice").attr("disabled", "disabled");
                $("#specialID").attr("disabled", "disabled");
                $("#AddSpecial").attr("disabled", "disabled");
                product.addSpecial();
            }
            else {
                $("#specialFrom").removeAttr("disabled");
                $("#specialTo").removeAttr("disabled");
                $("#specialPrice").removeAttr("disabled");
                $("#specialID").removeAttr("disabled");
                $("#AddSpecial").removeAttr("disabled");
            }

        });
        var response = function (result) {

            if (result.Data != null) {
                //populate the form
                $("#specialFrom").val(formatDateTime(result.Data.DateFrom, "dd/MM/yyyy HH:mm"));
                $("#specialTo").val(formatDateTime(result.Data.DateTo, "dd/MM/yyyy HH:mm"));
                $("#specialPrice").val(result.Data.Price);
                $("#specialID").val(result.Data.ID);
                $("#specialOn").attr("checked", "checked");

            }
            else {
                $("#specialFrom").attr("disabled", "disabled");
                $("#specialTo").attr("disabled", "disabled");
                $("#specialPrice").attr("disabled", "disabled");
                $("#specialID").attr("disabled", "disabled");
                $("#AddSpecial").attr("disabled", "disabled");
            }
            $("#tab_content2").unblock();
        };
        if (!isNaN(parseInt($("#ID").val()))) {
            Aurora.Manage.Service.AuroraWS.GetSpecials(parseInt($("#ID").val()), response, onError);
        }
    },
    addSpecial: function () {
        $("#tab_content2").block({ message: null });
        //check if special is turned o/f
        var specialOnOff = !$("#specialOn").is(":checked");
        //validate specials form
        var valid = $("#specialFrom").val() != "" &&
                    $("#specialTo").val() != "" &&
                    $("#specialPrice").val() != "";

        if (valid) {
            //result
            var response = function (result) {
                if (result.Data.ID != undefined && result.Data.DeletedOn == null) {
                    $("#specialID").val(result.Data.ID);
                    DisplayMessage("Messages", "info", String.format("Product now on special from {0} to {1} at {2} each.", formatDateTime(result.Data.DateFrom, "dd/MM/yyyy HH:mm"), formatDateTime(result.Data.DateTo, "dd/MM/yyyy HH:mm"), result.Data.Price));
                    scrollToElement("Messages", -300, 0);
                }
                else {
                    DisplayMessage("Messages", "info", "Product is now back to normal prices");
                    scrollToElement("Messages", -300, 0);
                }
                $("#tab_content2").unblock();
            };

            //entity
            var specialEntity = new AuroraEntities.Specials();
            specialEntity.ID = parseInt($("#specialID").val());
            specialEntity.DateFrom = formatDateTimeLongDateString($("#specialFrom").val());
            specialEntity.DateTo = formatDateTimeLongDateString($("#specialTo").val());
            specialEntity.Price = parseFloat($("#specialPrice").val());
            specialEntity.DeletedOn = "";
            specialEntity.DeletedBy = "";
            specialEntity.InsertedBy = 0;
            specialEntity.InsertedOn = new Date();
            specialEntity.ProductID = parseInt($("#ID").val());
            specialEntity.EntityKey = new Array();
            var entityValues = new Object();
            entityValues.Key = "ID";
            entityValues.Value = parseInt(specialEntity.ID);
            specialEntity.EntityKey = entityValues;


            Aurora.Manage.Service.AuroraWS.UpdateSpecials(specialEntity, specialOnOff, response, onError);
        }
        else {
            DisplayMessage("Messages", "warn", "please enter all the information under product specials.");
        }
    },
    searchTimer: 0,
    search: function (filter) {
        $("#Messages").insertAfter("#prodGrid");
        if (filter.length >= 3) {
            $("#pnlAddEdit").hide();
            product.actionStart();
            clearTimeout(product.searchTimer);

            product.searchTimer = setTimeout(function () {
                var response = function (result) {
                    $("#template").addClass("sys-template");
                    if (result.Data[0] == null) {

                        DisplayMessage("Messages", "warn", "Your search produced no results, please try another filter.", 7000);
                        product.actionComplete();
                        return;
                    }
                    if (template == null) {
                        template = $create(Sys.UI.DataView, {}, {}, {}, $get("template"));
                    }

                    if (TotalCount == 0) {
                        TotalCount = result.Count;
                        initPagination(true);
                    }
                    TotalCount = result.Count;
                    template.set_data(result.Data);
                    $("#template a[hasChildren='0']").hide();
                    product.actionComplete();

                    var currentCount = 0;

                    if (CurrentPage >= 1) {
                        var pagesPastCount = 0;
                        for (var inc = 0; inc < CurrentPage; inc++) {
                            pagesPastCount = pagesPastCount + 10;
                        }

                        currentCount = pagesPastCount + Number($("#prodGrid tr").length - 1);
                    }
                    else {
                        currentCount = Number($("#prodGrid tr").length - 1);
                    }

                    DisplayMessage("Messages", "success", String.format("Items found viewing {0} of {1}", currentCount, result.Count), 7000);
                };
                Aurora.Manage.Service.AuroraWS.GetProductsWithChildren(CurrentPage + 1, globalVars.PageSize, filter, 0, response, onError);
            }, 1000);
        }
        else {
            DisplayMessage("Messages", "warn", "To search for products your filter has to be greater than 3 characters", 7000);
        }


    },
    getCustomFieldData: function (productID) {
        var onSuccess = function (result) {
            if (result.Result) {
                if (result.Data) {
                    $("#CustomFields").html(result.Data);
                    transferCustomFields('CustomData', 'tbl_Form', 'CustomField');
                }
                else {
                    $("#CustomFields").html("No Custom Fields have been set up");
                }
            }
            else {
                onError(result);
                scrollToElement("Messages", -20, 100);
            }
        };
        productID = (productID === "") ? -1 : productID;
        Aurora.Manage.Service.AuroraWS.GetCustomProductXMLData(productID, onSuccess, onError);
    }

};

var categories = {
    getCategoriesForDropDown: function () {
        /// <summary>Gets all the categories for the clients products</summary>
        var onSuccess = function (result) {
            var menu = "<ul><li><a id='editadd' href='javascript:' onclick=\"$('.data').show();categories.showCategories(true);return false;\">Add/Edit Categories</a></li>";
            var menuAdd = "<ul><li><a href='javascript:' id='0' parentid='0' onclick='categories.setCat(this);'>None</a></li>";
            
            if (result.Data.length > 0) {
                categoriesForFind = result.Data;
                $(".backTo").show();
                productCategories = result.Data;
                $("#CatParentVal").val(result.Data[0].ID);
                $("#hierarchybreadcrumb").children().first()[0].nextSibling.nodeValue = result.Data[0].Name;
                for (var item in result.Data) {
                    if (result.Data[item].ParentID == 0) {

                        //get Children
                        var childItem = "";
                        var menuAddChild = "";
                        var editMenuChild = ""

                        childItem = categories.getChildItems(childItem, result.Data, item);
                        menuAddChild = categories.getChildItems(menuAddChild, result.Data, item);
                        editMenuChild = categories.getChildItems(menuAddChild, result.Data, item);

                        if (childItem != "") {
                            menu += "<li   ><a id='" + result.Data[item].ID + "' parentid='" + result.Data[item].ParentID + "' href='javascript:void(0);' onclick='categories.changeCategory(this.id);'>" + result.Data[item].Name + "</a><ul>" + childItem + "</ul><li>";
                            menuAdd += "<li   ><a id='" + result.Data[item].ID + "' href='javascript:void(0);' onclick='categories.setCat(this);'>" + result.Data[item].Name + "</a><ul>" + menuAddChild + "</ul><li>";
                            editAdd += "<li   ><a id='" + result.Data[item].ID + "' href='javascript:void(0);' onclick='editsetCat(this);'>" + result.Data[item].Name + "</a><ul>" + menuAddChild + "</ul><li>";


                        }
                        else {
                            menu += "<li ><a id='" + result.Data[item].ID + "' parentid='" + result.Data[item].ParentID + "'  href='javascript:void(0);' onclick='categories.changeCategory(this.id);'>" + result.Data[item].Name + "</a><li>";
                            menuAdd += "<li><a id='" + result.Data[item].ID + "' href='javascript:void(0);' onclick='categories.setCat(this);'>" + result.Data[item].Name + "</a><li>";
                            editAdd += "<li><a id='" + result.Data[item].ID + "' href='javascript:void(0);' onclick='editsetCat(this);'>" + result.Data[item].Name + "</a><li>";
                        }
                    }
                }
                menu += "<ul/>";
                menuAdd += "<ul/>";
                editAdd += "<ul/>";
                //assign the current HTML content to the global variable
                catMenuHTML = menu;

                categories.showCategories(false);


                // BUTTONS
                $('.fg-button').hover(
    		function () { $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
    		function () { $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
            );
                $('#hierarchybreadcrumb').menu({
                    content: menu,
                    backLink: true,
                    flyOut: true,
                    positionOpts: {
                    	posX: 'left',
                    	posY: 'bottom',
                    	offsetX: 0,
                    	offsetY: 0,
                    	directionH: 'right',
                    	directionV: 'down',
                    	detectH: false, // do horizontal collision detection  
                    	detectV: false, // do vertical collision detection
                    	linkToFront: false
                    },
                    customClick: function (itemid) {
                        categories.setCat(itemid);
                        TotalCount = 0;
                        CurrentPage = 0;
                        if (!$("#CategoryListContainer").is(":visible")) {
                            product.display();
                        }

                    }
                });
                $('#CatParent').menu({
                    content: menuAdd,
                    backLink: true,
                    flyOut: true,
                    customClick: function (itemid) {
                        categories.setCat(itemid, true);
                    }
                });


                product.display();

            }
            else {
                //show categories add.i
                categories.showCategories(true);
                $(".backTo").hide();
            }

        }

        webService.GetCategories(0, 0, "", 19, 0, onSuccess, onError);
    },
    getChildItems: function (item, list, index) {
        for (var child in list) {
            var newChildren = "";
            if (list[child].ParentID == list[index].ID && list[child].ParentID > 0) {
                newChildren = "<ul>" + categories.getChildItems(newChildren, list, child) + "</ul>";
                if (newChildren == "<ul></ul>") {
                    newChildren = "";
                }
                item += String.format("<li class='childrenLi' parentid='" + list[index].ID + "'><a id='" + list[child].ID + "'  href='javascript:void(0);' onclick='categories.changeCategory(this.id);' id='" + list[child].ID + "'>" + list[child].Name + "</a>{0}<li>", newChildren);
                item.replace("<ul></ul>");
            }
        }

        return item;
    },
    setCat: function (item, addForm) {
        $("#btnSave").hide();
        if (addForm) {
            $('#CatSelectID').val(item);
            var itemName = $("#" + item).children('span').first().html();
            if (itemName == "" || itemName == undefined) {
                itemName = $("#" + item).html();
            }
            $("#CatParent").children().first()[0].nextSibling.nodeValue = itemName;
        }
        else {
            $('#CatParentVal').val(item);

            var itemName = $("#" + item).children('span').first().html();
            if (itemName == "" || itemName == undefined) {
                itemName = $("#" + item).html();
            }

            itemName = decodeHTML(itemName);

            $($("#hierarchybreadcrumb").children().first()).next().html(itemName);
        }

        $("#contentAddEdit").hide();
    },
    getCategory: function () {
        $("#CategoryListContainer").fadeIn(500);
        product.actionStart("#CategoryListContainer .content");

        var onSuccess = function (result) {
            if (result.Result) {
                categoryData = result.Data;

                CatTotal = result.Count;
                if (catData == null) {
                    catList = $create(Sys.UI.DataView, {}, {}, {}, $get("CategoryList"));
                    initCatPagination();
                }

                catData = catList;
                catData.set_data(result.Data);

                product.actionComplete("#CategoryListContainer .content");
            }
            else {

            }
        };

        if (catData == null && !reloadProductCatList) {
            webService.GetCategories(0, 0, "", 19, 0, onSuccess, onError);
        }
        else if (catData == null && reloadProductCatList) {
            webService.GetCategories(10, currentCatPage + 1, "", 19, 0, onSuccess, onError);
            $("#CategoryListContainer").show();
            product.actionComplete("#CategoryListContainer .content");
        }
        else {
            webService.GetCategories(10, currentCatPage + 1, "", 19, 0, onSuccess, onError);
            $("#CategoryListContainer").show();
            product.actionComplete("#CategoryListContainer .content");
        }

    },
    changeCategory: function (catID) {
        /// <summary>Changes the current view to show all products within the specified category</summary>
        $("#SearchBox").val("Search by Name/Code..");
        if (catID != undefined) {

        }
    },
    editCategory: function (catID) {
        $("#AddNewCat").block({ message: null });
        if (catID.getAttribute("dataid") != "-1") {
            var onSuccess = function (result) {
                categories.populateCategoryForm(result.Data[0]);
                $("#AddNewCat").fadeIn(500);
                $('#CatSelectID').val(result.Data[0].ParentID);
                var parent = $('#CatSelectID').val();
                var itemName = $("#" + parent).children('span').first().html();
                if (itemName == "" || itemName == undefined) {
                    itemName = $("#" + parent).html();
                }
                if (itemName == null) {
                    itemName = "None";
                }
                $("#CatParent").children().first()[0].nextSibling.nodeValue = itemName;
                categories.getCategory();
                $("#AddNewCat").unblock();
                //image
                $('#btn_catimage')[0].href = 'WebImage/WebImageUpload.aspx?ipath=' + client.replace("~", "..") + '/Uploads/&redir=../Default.aspx&iName=productcat_' + result.Data[0].ID + '.jpg&smWidth=' + (GetJSSession("Products_ThumbnailWidth")) + '&lgWidth=' + (GetJSSession("Page_ImageSize") || 800) + '&smPrefix=sm_';
                $("#btn_catimage").click(categories.addImage);
                $("#Image1").attr("src", client.replace("~", "..") + '/Uploads/productcat_' + result.Data[0].ID + '.jpg');
                $("#Image1").hide();
                $('#btn_catimage').mouseover(
                function () {
                    $("#Image1").css("left", ($("#tab1_content").width() * 0.70) - 20);
                    $("#Image1").css("border", "2px dashed grey");
                    $("#Image1").css("position", "absolute");
                    $("#Image1").css("top", "91px");
                    $("#Image1").fadeIn(500);
                    $("#Image1").height($("#Image1").height() / $("#Image1").width * ($("#tab1_content").width() - ($("#tab1_content").width() * 0.70)));
                    $("#Image1").width($("#tab1_content").width() - ($("#tab1_content").width() * 0.70));

                });

                $('#btn_catimage').mouseout(
                function () {
                    $("#Image1").fadeOut(500);
                }
                );
            };

            webService.GetCategories(0, 0, "", 19, $(catID).attr("dataid"), onSuccess, onError);
        }
        else {
            $("#AddNewCat").fadeIn(500);
            categories.populateCategoryForm({ Name: '', Description: '', OrderIndex: '', ID: -1, ParentID: 0 });
            $("#AddNewCat").unblock();
        }

    },
    deleteCategory: function (catID) {
        if (catID) {

            var currentCat = findByInArray(categoryData, "ID", catID);

            if (currentCat && confirm(String.format("Do you wish to delete the '{0}' category?", currentCat.Name))) {
                var response = function () {
                    DisplayMessage("Messages", "info", "Category Deleted", 7000);
                    $("#AddNewCat").hide();
                    categories.getCategory();
                    categories.getCategoriesForDropDown();
                };

                currentCat.DeletedOn = new Date();
                currentCat.DeletedBy = 0;

                Aurora.Manage.Service.AuroraWS.UpdateCategory(currentCat, response, onError);
            }
        }

    },
    addCategory: function () {
        /// <summary>Adds a new category/subcategory</summary>
        if (validateForm("addData", "Please ensure you have filled in all fields.", "AddMsg", false)) {
            return;
        }
        var onSuccess = function (result) {
            if (result.Count == 0) {
                DisplayMessage("AddMsg", "success", "Catergory has successfully been added.", 7000);
                categories.getCategory();
            }
            else if (result.Count == 1) {
                DisplayMessage("AddMsg", "warn", "A catergory with this name already exists.", 7000);
            }
            else {
                DisplayMessage("AddMsg", "error", result.ErrorMessage);

            }
        };
        var category = new Object();
        category.FeatureModuleID = 19;
        category.Description = "Product";
        category.Name = $("#CatName").val();
        category.OrderIndex = parseInt($("#CatOrder").val());

        category.ClientSiteID = 0;
        category.ID = $("#CatID").val() != "" ? parseInt($("#CatID").val()) : 0;
        if (category.ID == -1) {
            category.ID = 0;
        }
        category.InsertedOn = new Date();
        category.EntityKey = new Object();
        category.EntityKey.EntityContainerName = "AuroraEntities";
        category.EntityKey.EntitySetName = "Category";
        category.EntityKey.IsTemporary = false;
        category.EntityKey.EntityKeyValues = new Array();

        objKey = new Object();
        objKey.Key = "ID";
        objKey.Value = parseFloat(category.ID) ? parseFloat(category.ID) : 0;
        category.EntityKey.EntityKeyValues[0] = objKey;


        category.ParentID = $("#CatSelectID").val();
        if (category.ParentID == "") {
            category.ParentID = 0;
        }
        if (category.ParentID == category.ID && category.ID != 0) {
            DisplayMessage("Messages", "warn", "You cannot add a category as its own parent category", 7000);
            return;
        }

        webService.UpdateCategory(category, onSuccess, onError);
    },
    showCategories: function (display) {
        $("#CatOrder").ForceNumericOnly();
        if (display) {
            $("#products").hide(0);
            $("#contentAddEdit").hide();
            $("#Messages").hide();
            $("#btnSave").hide();
            reloadProductCatList = true;

            categories.getCategory();
        }
        else {
            $("#products").fadeIn(500);
            $("#AddNewCat").hide();
            $("#CategoryListContainer").hide();
            if (reloadProductCatList) {
                reloadProductCatList = false;
                categories.getCategoriesForDropDown();
            }

        }
    },
    addImage: function () {
        $.fancybox($('#btn_catimage').attr("href"), {
            padding: 0,
            titleShow: true,
            title: "Upload An Image.",
            overlayColor: '#000000',
            overlayOpacity: 0.5,
            width: '90%',
            height: '90%',
            showActivity: true,
            onComplete: function () {
                $.fancybox.center();
            },
            type: "iframe"
        });

        return false;
    },
    populateCategoryForm: function (values) {
        $("#Messages").insertBefore($("#AddNewCat"));
        $("#CatName").val(values.Name);
        $("#CatDescription").val(values.Description);
        $("#CatOrder").val(values.OrderIndex);
        $("#CatID").val(values.ID);
        $("#CatParentValue").val(values.ParentID);
        if (values.ParentID != "0") {
            var arrow = $("#CatParent span");
            // $("#CatParent").html($("#Cat_" + values.ParentID).html());
            //$("#CatParent").remove(arrow);
            //$("#CatParent").append(arrow);
        }
        else {
            var arrow = $("#CatParent span");
            //$("#CatParent").html("Product Categories");
            //$("#CatParent").remove(arrow);

        }
    },
    findCatInResult: function (id, replace, wrapper) {
        for (var entry = 0; entry < categoriesForFind.length; entry++) {
            if (categoriesForFind[entry].ID == id) {
                if (wrapper) {
                    return String.format(wrapper, categoriesForFind[entry].Name);
                }
                else {
                    return categoriesForFind[entry].Name;
                }
            }
        }

        return !replace ? '' : replace;
    }
};

var colour = {
    display: function () {

        var onSuccess = function (result) {
            if (result.Result && result.Data.length > 0) {
                if (colourTemplate == null) {
                    colourTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("ColourChoose"));
                }
                else {
                    colourTemplate.dispose();
                    colourTemplate = $create(Sys.UI.DataView, {}, {}, {}, $get("ColourChoose"));
                }
                colourTemplate.set_data(result.Data);
                colourSelector(
                                { currentItem: $("#ColourID").val(),
                                    formName: "ProductForm"
                                });
                $(".swatch").data("selected", $("#ColourID").val());
            }

            product.actionComplete();
            defferredColours.resolve();
        };
        webService.GetColours(0, 0, "", onSuccess, onError);
    },
    add: function (hexVal) {
        product.actionStart();
        var onSuccess = function (result) {
            product.edit($("#ID").val());
            product.actionComplete();
        };

        var colourEntity = new Object();
        colourEntity.ProductID = $("#ID").val();
        colourEntity.ID = -1
        colourEntity.HexValue = hexVal;

        webService.UpdateColour(colourEntity, onSuccess, onError);
    }
};

var gallery = {
    add: function (productID, galleryType, deleteItem) {
        if (validateForm("AddImage", "Please enter all the fields in the form before uploading an image", "Messages", false) && !deleteItem) {
            return;
        }
        if (deleteItem) {
            var exec = confirm('This will remove this item permanently, Are you sure that you want to do this?', 'Delete Image');
            if (!exec) {
                return;
            }
            $("#AddImage").hide();
        }
        if ($("#imgID").val().isNullOrEmpty()) {
            itemID = 0;
        }

        if (!deleteItem) {
            deleteItem = false;
        }

        var updateSuccess = function (result, userContext) {
            if (result.Result) {

                if (!userContext[1]) {
                    DisplayMessage("Messages", "success", "Image was successfully added", 5000);
                    $('#AddImage').slideToggle(500);
                    $('#btn_image1')[0].href = 'WebImage/WebImageUpload.aspx?ipath=' + client.replace("~", "..") + '/Uploads/&redir=../Default.aspx&iName=product_' + result.Data.ID + '.jpg&smWidth=' + (GetJSSession("Products_ThumbnailWidth") || 100) + '&lgWidth=' + (GetJSSession("ProductDetails_ImageWidth") || 800) + '&smPrefix=sm_';
                    $.fancybox($('#btn_image1').attr("href"), {
                        padding: 0,
                        titleShow: true,
                        title: "Upload An Image.",
                        overlayColor: '#000000',
                        overlayOpacity: 0.5,
                        width: '90%',
                        height: '90%',
                        showActivity: true,
                        onClosed: function () {

                            $("#AddImage input[type=text]").val("");
                            gallery.loadGallery(currentItemID, 'Images', undefined);

                        },
                        onComplete: function () {
                            $.fancybox.center();
                        },
                        onLoad: function () {
                            $.fancybox.center();
                        },
                        type: "iframe"
                    });


                }
                else {
                    $("img[dataid='" + $("#imgID").val() + "']").parent().parent().parent().remove();
                    $("#imgID").val(0);
                    DisplayMessage("Messages", "success", "Image was successfully removed", 5000);

                }
                scrollToElement("Messages", -20, 100);


            }
            else {
                DisplayMessage("Messages", "success", "We couldn't save that image ,please try again if the problem persists then contact support", 5000);
                scrollToElement("Messages", -20, 100);
            }

            gallery.loadGallery(currentItemID, 'Images', true);
        }


        Aurora.Manage.Service.AuroraWS.GalleryUpdate($("#imgID").val(), productID, $("#imgName").val(), $("#imgDescrip").val(), "images", null, deleteItem, updateSuccess, onError, [galleryType, deleteItem]);
    },
    loadGallery: function (productID, galleryType, isEdit) {

        if (!galleryType) {
            galleryType = "Images";
        }

        var getGallerys = function (result, userContext) {
            if (result.Count > 0) {
                if (userContext == "Images") {
                    $("#NoneImages").hide();
                    if (!imageGalleryTemplate) {
                        imageGalleryTemplate = $("#galleryImages").parent().html();
                    }
                    else {
                        $("#galleryImages").parent().html(imageGalleryTemplate);
                    }

                    if (datasetGalleryImages) {
                        if (datasetGalleryImages._element) {
                            datasetGalleryImages._element.control = undefined;
                            $(datasetGalleryImages._element).addClass("sys-template");
                            datasetGalleryImages.dispose();
                            datasetGalleryImages = null;

                        }
                    }

                    datasetGalleryImages = $create(Sys.UI.DataView, {}, {}, {}, $get("galleryImages"));
                    datasetGalleryImages.set_data(result.Data);

                }
                else if (userContext == "Video") {
                    //if (datasetGalleryVideo == null)
                    // datasetGalleryVideo = $create(Sys.UI.DataView, {}, {}, {}, $get("galleryVideo"));

                }
            }

            //            if (userContext == "Images") {
            //                galleryType = "Video";
            //                Aurora.Manage.Service.AuroraWS.GetGalleryForProduct(productID, getGallerys, onError, galleryType);
            //            }
            $(".tipsy").remove();
            $(".img").tipsy({ gravity: 's', fade: true, title: 'mytitle', trigger: 'hover', delayIn: 0, delayOut: 0 });

        };

        productID = productID.toString().isNullOrEmpty(-1);

        Aurora.Manage.Service.AuroraWS.GetGalleryForProduct(productID, getGallerys, onError, galleryType);
    },
    showImage: function (item) {
        if ($(item).find("img").attr("src") !== "/Styles/images/no-image.jpg") {
            $.fancybox($(item).attr("largeimage"), {
                padding: 0,
                titleShow: true,
                title: $(item).attr("title"),
                overlayColor: '#000000',
                overlayOpacity: 0.5,
                showActivity: true,
                type: "image",
                onClosed: function () {
                    gallery.loadGallery(currentItemID, "Images", true);
                }
            });
        }
    },
    editItem: function (productID, galleryType, itemID) {
        if (!itemID) {
            itemID = 0;
        }

        var getItem = function (result, userContext) {
            if (result.Data[0].MediaType == ".jpg") {
                $("#imgName").val(result.Data[0].Name);
                $("#imgDescrip").val(result.Data[0].Description);
                if (!$('#AddImage').is(":visible")) {
                    $('#AddImage').slideToggle(500);
                }
                $('#AddImageBtn').mouseout();
                $('#AddImageBtn').attr('disabled', 'disabled');
                $("#imgID").val(result.Data[0].ID);
            }
            else {

            }
        }

        Aurora.Manage.Service.AuroraWS.GetGalleryItemForProduct(itemID, getItem, onError);

        return false;
    }

};

//#endregion

//#region Utilities 
function cEdit(e) {
    if (e.getAttribute("editorinit") != "1") {
        e.setAttribute("editorinit", "1");
        oDescription = new InnovaEditor("oDescription");
        oDescription.toolbarMode = 1;
        oDescription.useTagSelector = true;
        oDescription.showResizeBar = true;
        oDescription.width = "100%";
        oDescription.cmdAssetManager = "modalDialogShow('/Innova/assetmanager/assetmanager.asp?ClientID=" + client.replace("~/ClientData/", "") + "',720,550)"; //Command to open the Asset Manager add-on.
        oDescription.css = client.replace("~", "") + "/Styles/Site.css?" + new Date().getMilliseconds();
        editorTimer = setInterval(alignEditorTabs, 100);
        oDescription.REPLACE("DescDetail", "DescSpan");
        //Removes body styles
        $($("#idContentoDescription")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' });
        $($("#idContentoDescription")[0].contentWindow.document).find('body').attr("style", "background-image:none;");

        setTimeout(function () { $($("#idContentoDescription")[0].contentWindow.document).find('body').css({ backgroundImage: 'none' }); }, 1000);
    }

}
function addTab() {
    currentTab = null;
    $("#tabedit").show();
    $("#tabedit input[type='text']").val('');
    oDescription.putHTML('');
}
var tabDel = false;
function getTabs() {
    //Check if current item already exists
    if (currentTab) {
        var html = encodeHTML(oDescription.getHTMLBody());
        $(currentTab).attr("content", html);
        $(currentTab).attr("tabname", $("#tabName").val());
        $(currentTab).attr("order", $("#tabOrder").val());

        $("#tabName").val("");
        $("#tabOrder").val("");
        oDescription.putHTML("");


    }

    $("#fldTabDescription").val(encodeHTML(oDescription.getXHTMLBody()));

    var custXML = "";
    var fields = "";

    var tmp = $('#tabForm input');
    var gridData = $(".productGrid");
    if (tmp.length > 0) {
        custXML = "<XmlData><Fields type='Tabs'>";
        if (!currentTab) {

            //Get XML From Form
            $(tmp).each(
            function (index, elem) {
                if (($(this).attr("type") == "text" || $(this).attr("type") == "hidden") && $(this).attr("value") != "") {
                    fields += "<field fieldname='" + $(this).attr("name").replace("fld", "") + "' value='" + $(this).attr("value") + "' />"
                }
            }
            );
        }
        else {
            currentTab = null;
        }
        //Get XML from Grid
        $(gridData).each(
        function (index, elem) {
            if ($(this).attr("tabname")) {
                fields += "<field fieldname='TabName' value='" + $(this).attr("tabname") + "' />"
                fields += "<field fieldname='TabOrder' value='" + $(this).attr("order") + "' />"
                var description = encodeHTML($(this).attr("content"));
                description = description.replace(/&nbsp;/gi, ' ');
                description = description.replace(/\'/gi, '&apos;');
                fields += "<field fieldname='TabDescription' value='" + description + "' />"
            }
        }
        );
        custXML += fields + "</Fields></XmlData>";

    }

    if (!custXML) {
        custXML = null;
    }

    $("#CustomXML").val(custXML)

    var response = function (result) {
        if (tabDel) {
            DisplayMessage("Messages", "success", "tab successfully removed");
            tabDel = false;
        }
        else if (!result.ErrorMessage) {
            DisplayMessage("Messages", "success", "tab successfully added");
        }
        else {
            DisplayMessage("Messages", "error", "An Error has occurred whilst saving this tab,we have been notified and will contact you with the solution");
        }
        scrollToElement("Messages", -20, 2);
        $("#tabedit").hide();
        getCurrentTabs();
    };

    Aurora.Manage.Service.AuroraWS.UpdateProductTabs(currentItemID, custXML, response, onError);
}
function getCurrentTabs() {

    var currentXML = $("#CustomXML").val();
    if (currentXML && currentXML != "") {
        var xmlDoc = $.parseXML(currentXML);
        var item = [];
        $xml = $(xmlDoc);
        $fields = $xml.find('field');
        $.each($fields, function (i, field) {
            if ($(field).attr("fieldname") == "TabName") {
                item.push({ name: $(field).attr('value'), order: $(field).next().attr('value'), desc: $(field).next().next().attr('value') });
            }
        });

        if (tabData == null && !tabInit) {
            tabData = $create(Sys.UI.DataView, {}, {}, {}, $get("tabDataView"));
            tabInit = true;
        }
        else if (!tabInit) {
            tabData.dispose();
            tabData = $create(Sys.UI.DataView, {}, {}, {}, $get("tabDataView"));
            tabInit = true;
        }

        tabData.set_data(item);



    }
}
function editTab(e) {
    $("#tabTitle").html(String.format("Editing: {0} Tab", $(e).attr("tabname")));
    $("#tabName").val($(e).attr("tabname"));
    $("#tabOrder").val($(e).attr("order"));
    oDescription.putHTML(decodeHTML($(e).attr("content")));
    $("#tabedit").show();

}
function deleteTab(elem) {
    var ans = confirm("Do you wish permanently remove this tab?");
    if (ans) {
        tabDel = true;
        var child = elem.get(0);
        child.parentNode.removeChild(child);
        getTabs();
    }

}
function searchCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        product.search($("#SearchBox").val());
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        product.search($("#SearchBox").val());
    }
    $("#contentAddEdit").hide();
}
function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        CurrentPage = page_index;
        product.display();
    } else if (CurrentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        CurrentPage = 0;
        product.display();
    }
    $("#contentAddEdit").hide();
    return false;
}
function initPagination(isSearch) {
    // Create content inside pagination element
    $("#Pagination").pagination(TotalCount, {
        callback: !isSearch ? pageselectCallback : searchCallback,
        items_per_page: globalVars.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
function initCatPagination() {
    // Create content inside pagination element
    $("#CatPagination").pagination(CatTotal, {
        callback: categoryselectselectCallback,
        items_per_page: globalVars.PageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}
function categoryselectselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        currentCatPage = page_index;
        categories.getCategory();
    } else if (currentCatPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        currentCatPage = 0;
        categories.getCategory();
    }

    return false;
}
function checkImage(imageTagID, imageUrl) {
    var failOver = "/Styles/images/no-image.jpg";
    var loadImage = function (result) {

        var imageElement = document.getElementById("image_" + imageTagID);
        imageUrl = imageUrl.substr(0, imageUrl.indexOf("?"))+"?"+new Date().getMilliseconds();
        imageElement.src = imageUrl;
        $(imageElement).load(function () {
            $(this).fadeIn(500);
        });

    };
    var fail = function (result) {
        if ($("#image_" + imageTagID).length > 0) {
            $("#image_" + imageTagID).attr("src", failOver);
        }
        $("#Contents").unblock();
    };

    //Go get image from server
    $.ajax({ url: imageUrl, type: 'HEAD', error: fail, success: loadImage });
}
//#endregion