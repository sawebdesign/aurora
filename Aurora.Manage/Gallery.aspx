﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="Aurora.Manage.Gallery" %>

<%@ Register Assembly="Aurora.Custom" TagPrefix="cui" Namespace="Aurora.Custom.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<script src="Scripts/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="Scripts/jquery.galleryview-2.1.1.js" type="text/javascript"></script>
<script src="Scripts/jquery.timers-1.2.js" type="text/javascript"></script>
<script type="text/javascript" src="Gallery.aspx.js"></script>
<%if (Request["response"] == "404")
      ClientScript.RegisterClientScriptBlock(this.GetType(), "404", "<script>alert('The file you have requested was not found.');</script>");     
%>
<body>
    <form id="form1" runat="server">
    <div id="editor" style="display: none;">
        <!-- Begin one column window -->
        <div class="onecolumn">
            <div class="header">
                <span>Album Item Details</span>
            </div>
            <div class="content" id="GalleryDetails" style="min-height: 200px;">
                <asp:Image ID="Image" runat="server" Visible="false" />
                <table width="100%;">
                    <input type="hidden" id="ID" value="" runat="server" />
                    <input type="hidden" id="ClientSiteID" value="" runat="server" />
                    <input type="hidden" id="CategoryID" value="" runat="server" />
                    <input type="hidden" id="SchemaID" value="" runat="server" />
                    <input type="hidden" id="InsertedOn" value="" runat="server" />
                    <input type="hidden" id="InsertedBy" value="" runat="server" />
                    <input type="hidden" id="DeletedOn" value="" runat="server" />
                    <input type="hidden" id="DeletedBy" value="" runat="server" />
                    <input type="hidden" id="MediaType" value="" runat="server" />
                    <input type="hidden" id="Index" value="" runat="server" />
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            <input type="text" value="" maxlength="100" valtype="required" id="Name" name="Name" runat="server"
                                class="help" mytitle="This represents the text that will identify this album item" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td>
                            <input type="text" value="" valtype="required" id="Description" name="Description"
                                runat="server" class="help" mytitle="This text will provide a small detail about this item" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Link:
                        </td>
                        <td>
                            <input type="text" value="" id="Link" name="Link" runat="server" class="help" mytitle="Insert a link in here to allow image to redirect your users.Please note this function only works on slider galleries." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Display Order:
                        </td>
                        <td>
                            <input type="text" value="" valtype="required" id="GalleryIndex" name="GalleryIndex"
                                runat="server" class="help" mytitle="The sequence of this item in the Album" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Upload Album Item(mouse over to view):
                        </td>
                        <td>
                            <a id="btn_image1" onclick="saveData(); return false;" href="#" class="help" mytitle="Upload/Delete media">
                                <img src="Styles/images/icon_media.png" /></a>
                        </td>
                    </tr>
                    <%if (Request["type"].ToUpper() == "VIDEO") {%>
                    <tr>
                        <td>
                            Video Url:
                        </td>
                        <td>
                            <input type="text" name="VideoURL" runat="server" id="VideoURL" valtype="required"
                                class="help" mytitle="URL to where the video is hosted" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Preview:
                        </td>
                        <td id="videofeed" colspan="2" runat="server">
                        </td>
                    </tr>
                    <% }%>
                    <%if (Request["type"].ToUpper() == "AUDIO") {%>
                    <tr id="downloadRow" runat="server">
                        <td valign="top">
                        </td>
                        <td>
                            <asp:HiddenField runat="server" ID="HiddenAudioFileSource" />
                            <asp:ImageButton Style="display: none;" AlternateText="Download Item" runat="server"
                                ID="LinkDownloadFile" Width="70" Height="40" ImageUrl="~/Styles/Images/download.png"
                                OnClick="LinkDownloadFile_Click"></asp:ImageButton>
                            <asp:Literal runat="server" ID="player"></asp:Literal>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td>
                            <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                <cui:XmlPanel ID="ContentXml" runat="server">
                                </cui:XmlPanel>
                            </cui:ObjectPanel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <p style="margin-top: 0px; margin-right: 0px" align="right" id="savecontainer">
        <input type="button" id="btnSave" onclick="saveData(true);" value="Save" class="Login"
            style="margin-right: 0px" />
    </p>
    <!-- End content -->
    </form>
</body>
</html>
