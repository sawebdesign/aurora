﻿<%@ Page Title="Google Analytics Dashboard" Language="C#" MasterPageFile="~/MasterPage/Aurora.Manage.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Aurora.Manage._Default" %>

<%@ Import Namespace="Aurora.Custom" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%
        Session.Timeout = 60;
    %>
    <script type="text/javascript" language="javascript" src="Default.aspx.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">


        var feed = 'https://www.google.com/analytics/feeds';
        var TotalPageviews = 0;
        var totalCalled = 0;
        //start and end date for 30 days from today
        var startdate = new Date();
        startdate.setMonth(startdate.getMonth())
        var startd = startdate.getFullYear() + '-' + PadZero(startdate.getMonth() + 1) + '-' + PadZero(startdate.getDate());

        var enddate = new Date();
        enddate.setMonth(enddate.getMonth() + 1);
        var endd = enddate.getFullYear() + '-' + PadZero(enddate.getMonth() + 1) + '-' + PadZero(enddate.getDate() );

        // Load the Google data JavaScript client library.
        google.load('gdata', '1.x', { packages: ['analytics'] });

        // Set the callback function when the library is ready.
        google.setOnLoadCallback(init);

        function doGetInfo() {
            if (google.accounts.user.checkLogin(feed)) {
                google.accounts.user.getInfo(handleInfo);
            }
        }

        function doLogin() {
            var token = google.accounts.user.login(feed);
        }

        function doLogout() {
            if (google.accounts.user.checkLogin(feed)) {
                google.accounts.user.logout();
            }
        }

        /**
        * This is called once the Google Data JavaScript library has been loaded.
        * It creates a new AnalyticsService object, adds a click handler to the
        * authentication button and updates the button text depending on the status.
        */
        function init() {
            //fancybox
            $('#help').fancybox({
                padding: 0,
                transitionIn: 'elastic',
                transitionOut: 'elastic',
                titleShow: true,
                title: "Google Analytics",
                overlayColor: '#000000',
                overlayOpacity: .5,
                showActivity: true,
                autoDimensions: false,
                width: 800,
                height: 600
            }
        );
            myService = new google.gdata.analytics.AnalyticsService('gaExportAPI_acctSample_v1.0');
            var button = document.getElementById('authButton');

            // Add a click handler to the Authentication button.
            button.onclick = function () {
                // Test if the user is not authenticated.
                if (!google.accounts.user.checkLogin(feed)) {
                    // Authenticate the user.
                    doLogin();
                } else {
                    // Log the user out.
                    google.accounts.user.logout();
                    getStatus();
                }
            }
            getStatus();
        }

        /**
        * Utility method to display the user controls if the user is 
        * logged in. If user is logged in, get Account data and
        * get Report Data buttons are displayed.
        */
        function getStatus() {

            //var getTopPagesButton = document.getElementById('getTopPages');
            //getTopPagesButton.onclick = getTotalPageviews;

            //var getVisitsPageviewsButton = document.getElementById('getVisitsPageviews');
            //getVisitsPageviewsButton.onclick = getVisitsPageviews;

            var dataControls = document.getElementById('dataControls');
            var loginButton = document.getElementById('authButton');
            if (!google.accounts.user.checkLogin(feed)) {
                dataControls.style.display = 'none';   // hide control div
                loginButton.innerHTML = 'Access Google Analytics';
            } else {
                getAccountFeed();
                dataControls.style.display = 'block';  // show control div
                loginButton.innerHTML = 'Logout';
            }
        }

        /**
        * Main method to get account data from the API.
        */
        function getAccountFeed() {
            myService.getAccountFeed(feed + '/accounts/default?max-results=50', handleAccountFeed, handleError);
        }

        /**
        * Handle the account data returned by the Export API
        */
        function handleAccountFeed(result) {
            // An array of analytics feed entries.
            var entries = result.feed.getEntries();
            $('#GAAccounts').html('');

            for (var i = 0, entry; entry = entries[i]; ++i) {
                $('#GAAccounts').append($("<option></option>").attr("value", entry.getTableId().getValue()).text(entry.getPropertyValue('ga:AccountName')));
            }

            if (i = 1) {
                getTotalPageviews();
            } else {
                $("#GAAccounts").bind("change", function () { getTotalPageviews(); });
            }
        }

        /**
        * Get total Data
        */
        function getData(startdata, endDate, dimensions, metrics, callback) {

            var myFeedUri = feed + '/data' +
            '?start-date=' + startdata +
            '&end-date=' + endDate +
            '&dimensions=' + dimensions +
            '&metrics=' + metrics +
            '&ids=' + $('#GAAccounts').val();
            myService.getDataFeed(myFeedUri, callback, handleError);
        }

        /**
        * Get total Pageviews
        */
        function getTotalPageviews() {
            getData(startd, endd, '', 'ga:pageviews', handleTotalPageviews);
        }

        /**
        * Handle the data returned by the Export API for get the TotalPageviews
        */
        function handleTotalPageviews(result) {
            var entries = result.feed.getEntries();
            for (var i = 0, entry; entry = entries[i]; ++i) {
                TotalPageviews = entry.getValueOf('ga:pageviews');
                $('#TotalPageviews').html(TotalPageviews);
            }
            getTopPages();
            getUniquePageviews();
            getAvgTimeOnSite();
            getBounceRate();
            getVisitsPageviews()
            loader();
        }

        /**
        * Get total Unique Pageviews
        */
        function getUniquePageviews() {
            getData(startd, endd, '', 'ga:uniquePageviews', handleUniquePageviews);
        }

        /**
        * Handle the data returned by the Export API for get the Unique Pageviews
        */
        function handleUniquePageviews(result) {
            var entries = result.feed.getEntries();
            for (var i = 0, entry; entry = entries[i]; ++i) {
                $('#UniquePageviews').html(entry.getValueOf('ga:uniquePageviews'));
                loader();
            }
        }

        /**
        * Get total Unique AvgTimeOnSite
        */
        function getAvgTimeOnSite() {
            getData(startd, endd, '', 'ga:avgTimeOnSite', handleAvgTimeOnSite);
        }

        /**
        * Handle the data returned by the Export API for get the AvgTimeOnSite
        */
        function handleAvgTimeOnSite(result) {
            var entries = result.feed.getEntries();
            for (var i = 0, entry; entry = entries[i]; ++i) {
                var time = entry.getValueOf('ga:avgTimeOnSite');
                $('#AvgTimeOnSite').html(rectime(time.toFixed(0)));
                loader();
            }
        }

        /**
        * Get total Unique BounceRate
        */
        function getBounceRate() {
            getData(startd, endd, '', 'ga:visitBounceRate', handleBounceRate);
        }

        /**
        * Handle the data returned by the Export API for get the BounceRate
        */
        function handleBounceRate(result) {
            var entries = result.feed.getEntries();
            for (var i = 0, entry; entry = entries[i]; ++i) {
                var rate = entry.getValueOf('ga:visitBounceRate');
                $('#BounceRate').html(rate.toFixed(2) + '%');
                loader();
            }
        }

        /**
        * Get the top 10 pages
        */
        function getTopPages() {
            var startdate = new Date();
            startdate.setMonth(startdate.getMonth() + 1)
            var startd = startdate.getFullYear() + '-' + PadZero(startdate.getMonth() + 1) + '-' + PadZero(startdate.getDate());

            var enddate = new Date();
            enddate.setMonth(enddate.getMonth() + 1);
            var endd = enddate.getFullYear() + '-' + PadZero(enddate.getMonth() + 1) + '-' + PadZero(enddate.getDate());

            var myFeedUri = feed + '/data' +
            '?start-date=' + startd +
            '&end-date=' + endd +
            '&dimensions=ga:pageTitle,ga:pagePath' +
            '&metrics=ga:pageviews' +
            '&sort=-ga:pageviews' +
            '&max-results=10' +
            '&ids=' + $('#GAAccounts').val();
            myService.getDataFeed(myFeedUri, handleTopPages, handleError);
        }

        /**
        * Handle the data returned by the Export API for get the top 10 pages
        */
        function handleTopPages(result) {

            // An array of Analytics feed entries.
            var entries = result.feed.getEntries();

            // Create an HTML Table using an array of elements.
            var outputTable = ['<table width="100%" class="data" style="line-height:1" cellpadding="0" cellspacing="0"><thead><tr>',
                    '<th style="width:1%">&nbsp;</th>',
                    '<th style="width:40%">Page Title</th>',
                    '<th style="width:40%">Page Path</th>',
                    '<th style="width:10%">Views</th>',
                    '<th style="width:10%">%</th></tr><tbody>'];

            // Iterate through the feed entries and add the data as table rows.
            for (var i = 0, entry; entry = entries[i]; ++i) {
                var per = (entry.getValueOf('ga:pageviews') / TotalPageviews) * 100;
                // Add a row in the HTML Table array.
                var row = [
                (i + 1),
                entry.getValueOf('ga:pageTitle'),
                entry.getValueOf('ga:pagePath'),
                entry.getValueOf('ga:pageviews'),
                per.toFixed(2) + '%'
                ].join('</td><td>');
                outputTable.push('<tr><td>', row, '</td></tr>');
            }
            outputTable.push('</tbody></table>');

            // Insert the table into the UI.
            document.getElementById('topContent').innerHTML = outputTable.join('');
            loader();
        }


        /**
        * 30 Days Of Visits and Pageviews
        */
        function getVisitsPageviews() {
            var d = new Date();
            d.setMonth(d.getMonth());
            var endd = d.getFullYear() + '-' + PadZero(d.getMonth() + 1) + '-' + PadZero(d.getDate());

            d.setDate(d.getDate() - 15);
            var startd = d.getFullYear() + '-' + PadZero(d.getMonth() + 1) + '-' + PadZero(d.getDate());

            var myFeedUri = feed + '/data' +
            '?start-date=' + startd +
            '&end-date=' + endd +
            '&dimensions=ga:date' +
            '&metrics=ga:visits,ga:pageviews' +
            '&sort=ga:date' +
            '&ids=' + $('#GAAccounts').val();

            myService.getDataFeed(myFeedUri, handleVisitsPageviews, handleError);
        }

        /**
        * Handle the data returned by the Export API for 30 Days Of Visits and Pageviews
        */
        function handleVisitsPageviews(result) {

            // An array of Analytics feed entries.
            // An array of analytics entries
            var entries = result.feed.entry;

            // create an HTML Table using an array of elements
            var outputTable = ['<table id="graph_data" class="data" rel="bar" cellpadding="0" cellspacing="0" width="100%"><caption>Google Analytics last 15 days Visits & Views for:' + $("#GAAccounts option:selected").html() + '</caption><thead><tr><td class="no_input"> </td>'];
            for (var i = 0; i < entries.length; i++) {
                var entry = entries[i];
                var heading = entry.getValueOf('ga:date');
                outputTable.push('<th>' + heading.substr(6, heading.length) + '</th>');
            }
            outputTable.push('</tr></thead>');

            outputTable.push('<tbody><tr><th>Visits</th>');
            // Iterate through the feed entries and add the data as table rows for visits
            for (var i = 0; i < entries.length; i++) {
                var entry = entries[i];
                outputTable.push('<td>' + entry.getValueOf('ga:visits') + '</td>');
            }
            outputTable.push('</tr>');

            outputTable.push('<tr><th>Views</th>');
            // Iterate through the feed entries and add the data as table rows for pageviews
            for (var i = 0; i < entries.length; i++) {
                var entry = entries[i];
                outputTable.push('<td>' + entry.getValueOf('ga:pageviews') + '</td>');
            }
            outputTable.push('</tr></tbody>');
            outputTable.push('</table>');

            // Insert the table into the UI.
            document.getElementById('VisitsPageViews').innerHTML = outputTable.join('');
            setChart('#graph_data', 'bar', '#chart_wrapper');
            setTimeout('setChart(\'table#graph_data\', \'bar\', \'#chart_wrapper\')', 500);
            loader();

        }

        /**
        * Alert any errors that come from the API request.
        * @param {object} e The error object returned by the Analytics API.
        */
        function handleError(e) {
            var error = 'There was an error!\n';
            if (e.cause) {
                error += e.cause.status;
            } else {
                error.message;
            }
            alert(error);
        }
        function loader() {
            if (totalCalled == 6) {
                $("#chart_wrapper").unblock();
            }
            else {
                totalCalled = totalCalled + 1;
            }
        }
        function reload() {
            totalCalled = 0;
            $("#chart_wrapper,#topContent").block({ message: null });
            getTotalPageviews();
            getUniquePageviews();
            getAvgTimeOnSite();
            getBounceRate();
            getTopPages();
            getVisitsPageviews();

        }

      
        
    </script>
    <!-- Begin two column window -->
    <!-- Begin left column window -->
    <div class="twocolumn">
        <div class="column_left">
            <div class="header">
                <span>Google Analytics <a id="help" title="What's This?" onclick="return false;"
                    href="#What">?</a></span>
            </div>
            <div class="content">
                <br class="clear" />
                <div>
                    In order for us to access your analytics data we need you to grant us permission first.
                    <br />Please click the button below and once you have been redirected click the "Grant Access" button 
                    to give us permission to query your statistics.
                </div>
                <div style="float: left;">
                    <button class="fakeButton" id="authButton" type="button">
                        Loading...</button>&nbsp;
                </div>
                <img src="/Styles/images/dummy.gif" style="display: none" alt="required for Google Data" />
                <br class="clear" />
            </div>
        </div>
        <div class="column_right">
            <div class="header">
                <span>Account Information </span>
            </div>
            <div class="content" style="height:108px;">
            <div>Websites linked to my account.</div>
             <div id="dataControls" style="display: none;float:left;">
                    Accounts:
                    <select id="GAAccounts" onchange="reload();">
                        <option value="">Loading...</option>
                    </select>
                    <!--<p>show top 10 pageviews in descending order: <button id="getTopPages">Get Report Data</button></p>
                            <p>show 30 Days Of Visits and Pageviews: <button id="getVisitsPageviews">Get Report Data</button></p>-->
                </div>
            </div>
        </div>
    </div>
    <br class="clear" />
    <div class="onecolumn">
        <div>
            <div class="header">
                <span>
                    <div class="GoolgeA" title="Google Analytics">
                    </div>
                    Top Content Last 30 Days</span>
            </div>
            <div class="content" style="padding: 0">
                <table class="data" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="20%" style="border-right: 1px solid #cccccc" valign="top" nowrap>
                            <div class="hlp" id="hlpPageViews" title="The total number of pages viewed. Repeated views of a single page are counted">
                            </div>
                            Pageviews<br />
                            <h3>
                                <label id="TotalPageviews">
                                </label>
                            </h3>
                        </td>
                        <td width="30%" style="border-right: 1px solid #cccccc" valign="top" nowrap>
                            <div class="hlp" id="hlpUniquePageviews" title="The number of visits during which one or more of these pages was viewed">
                            </div>
                            Unique Pageviews<br />
                            <h3>
                                <label id="UniquePageviews">
                                </label>
                            </h3>
                        </td>
                        <td width="25%" style="border-right: 1px solid #cccccc" valign="top" nowrap>
                            <div class="hlp" id="hlpAvgTime" title="The average amount of time visitors spent viewing this set of pages or page">
                            </div>
                            Avg. Time on Site<br />
                            <h3>
                                <label id="AvgTimeOnSite">
                                </label>
                            </h3>
                        </td>
                        <td width="25%" valign="top" nowrap>
                            <div class="hlp" id="hlpBounceRate" title="The percentage of single page visits resulting from this set of pages or page">
                            </div>
                            Bounce Rate<br />
                            <h3>
                                <label id="BounceRate">
                                </label>
                            </h3>
                        </td>
                    </tr>
                </table>
                <div id="topContent">
                </div>
            </div>
        </div>
        <!-- End right column window -->
    </div>
    <!-- End two column window -->
    <!-- Begin graph window -->
    <div class="onecolumn">
        <div class="header">
            <span>
                <div class="GoolgeA" title="Google Analytics">
                </div>
                Visits & Views Last 15 Days</span>
            <div class="switch" style="width: 200px">
                <table width="200px" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                <input type="button" id="chart_bar" name="chart_bar" class="left_switch active" value="Bar"
                                    style="width: 50px" />
                            </td>
                            <td>
                                <input type="button" id="chart_area" name="chart_area" class="middle_switch" value="Area"
                                    style="width: 50px" />
                            </td>
                            <td>
                                <input type="button" id="chart_pie" name="chart_pie" class="middle_switch" value="Pie"
                                    style="width: 50px" />
                            </td>
                            <td>
                                <input type="button" id="chart_line" name="chart_line" class="right_switch" value="Line"
                                    style="width: 50px" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br class="clear" />
        <div class="content">
            <div id="chart_wrapper" class="chart_wrapper">
            </div>
            <br class="clear" />
            <br class="clear" />
            <form id="form_data" name="form_data" action="" method="post">
            <div id="VisitsPageViews" style="display: none">
            </div>
            <!-- End bar chart table-->
        </div>
    </div>
    <!-- End graph window -->
    <!--message-->
    <div id="WhatContainer" style="display: none">
        <div id="What" style="background: #fff; height: 100%">
            <h2>
                What is it?</h2>
            Google Analytics is the enterprise-class web analytics solution that gives you rich
            insights into your website traffic and marketing effectiveness. Powerful, flexible
            and easy-to-use features now let you see and analyze your traffic data in an entirely
            new way. With Google Analytics, you're more prepared to write better-targeted ads,
            strengthen your marketing initiatives and create higher converting websites.
            <br />
            <br />
            We have already set up a <a href="http://www.google.com/analytics/" target="_blank">
                Google Analytics</a> account for you and we are tracking statics. If you would
            like to view those statistics you will need a Google Account you can get one from
            the above link. Once you have an account please send us your account name and we
            link your Google Analytics to your account. Then you can use the “Access Google
            Analytics” button below to view the reports.
            <br />
            <br />
            <b>Note:</b> In order to execute the following reports, you must authenticate using
            your Google Account credentials.
            <br />
        </div>
    </div>
</asp:Content>
