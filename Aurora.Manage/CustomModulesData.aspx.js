﻿/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Service/AuroraWS.asmx/jsdebug" />
/// <reference path="/Scripts/jquery-cookie.js" />
var delegateCount = 1;
$(
    function () {
   
        parent.$get("contentAddEdit").height = $(document.body).height();
        parent.showFancyBox($("#fileLink"));
        $("#tbl_Form tr td:first-child").attr("align", "left");
        $("#tbl_Form").css({ width: '100%' });
        if (document.location.toString().indexOf('ex=1') != -1) {

            parent.createGrid();
            parent.$("#Edit").hide();
            parent.DisplayMessage("Messages", "success", "Module data saved.", 7000);
        }
        else if (document.location.toString().indexOf('del=1') != -1) {

            parent.createGrid();
            parent.DisplayMessage("Messages", "warn", "Item has been removed.", 7000);
        }
        else {
            parent.DisplayMessage("Messages", "info", "Please fill in the information below.");
        }

        loadFields();

        $("<tr><td colspan='10'><hr/></td></tr>").insertBefore($("#fldFirstName0").parent().parent());
        $("<tr><td colspan='10'><hr/></td></tr>").insertBefore($("#fldSpecialRequests").parent().parent());
        $($("#fldAttendCocktailParty0").parent().parent()).insertAfter($("#fldDietaryRequirements0").parent().parent());
        $("<tr><td colspan='10'><hr/></td></tr>").insertAfter($("#fldAttendCocktailParty0").parent().parent());

        //multiple select boxes
        $("[multipleselectvalues]").each(
        function () {

            var $curselect = $(this);
            var items = $curselect.attr("multipleselectvalues").toString().split(",");

            for (var val = 0; val < items.length; val++) {
                if (items[val] !== "") {
                    var toSelect = $curselect.find("option[value*='" + items[val] + "']");
                    if (toSelect.length > 0) {
                        toSelect.attr("selected", "selected");
                    }
                }
            }

        }
    );
    }

);

function loadFields() {
    ResizeFrame("contentAddEdit");
    if ($("#ClientSiteID").val() === "10079"){
        var elements = $($("#tbl_Form [id$=" + (delegateCount) + "]")).each(
            function (indx, Elem) {
                var row = $("<tr><td id='x" + delegateCount + "'>" + $("#" + $(Elem).attr("id").replace(delegateCount, "").replace("fld", "lbl") + "0").html() + "</td><td id='y" + delegateCount + "'></td></tr>");
                (row.find("#y" + delegateCount)).append($(Elem));
                row.insertBefore($("#tbl_Form tr:last").prev().prev());
                $(Elem).show();
                if (indx == 5 && $("#tbl_Form [id$=" + (delegateCount + 1) + "]").length > 0) {
                    $("<tr><td colspan='10'><hr/></td></tr>").insertAfter(row);
                    delegateCount++;
                    loadFields();
                }
            }
        );
    }
    if ($("#ClientSiteID").val() === "10095") {
        var elements = $($("#tbl_Form [id$=" + (delegateCount) + "]")).each(
        function (indx, Elem) {
            var row = $("<tr><td id='x" + delegateCount + "'>" + $("#" + $(Elem).attr("id").replace(delegateCount, "").replace("fld", "lbl") + "0").html() + "</td><td id='y" + delegateCount + "'></td></tr>");
            (row.find("#y" + delegateCount)).append($(Elem));
            row.insertBefore($("#fldSpecialRequests").parent().parent());
            if (indx === 0 && delegateCount === 1) {
                $('<tr><td colspan="10"><hr/></td></tr>').insertBefore(row);
            }
            $(Elem).show();
            if (indx == 4 && $("#tbl_Form [id$=" + (delegateCount + 1) + "]").length > 0) {
                $("<tr><td colspan='10'><hr/></td></tr>").insertAfter(row);
                delegateCount++;
                loadFields();
            }
    }
);
    };
}

function ResizeFrame(element) {
    var mheight;
    var mwidth;
    mheight = document.body.scrollHeight;
    mwidth = document.body.scrollWidth;
    var iframeElement = parent.document.getElementById(element);
    iframeElement.style.height = (mheight + 250) + "px";  //PX TO FORCE OTHER BROWSERS
    //iframeElement.style.width = mwidth+""//= "700; //100px or 100% 
}

function saveCustomDataXML() {
    $(".content").block({message:null});
    var response = function () {
        document.forms[0].action = document.forms[0].action + "?ex=1";
        document.forms[0].submit();
    };

    var xmlString = buildCustomXMLBySelector($('#tbl_Form input[name^="fld"],#tbl_Form select[name^="fld"],#tbl_Form textarea[name^="fld"]'));

    Aurora.Manage.Service.AuroraWS.UpdateCustomFormData(xmlString, $("#ID").val() == "" ? 0 : $("#ID").val(), $("#ModuleID").val(), response, onError);
}