﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomModules.aspx.cs" Inherits="Aurora.Manage.CustomModules" ClientIDMode="Predictable" %>

<%@ Register src="UserControls/CustomFieldEditor.ascx" tagname="CustomFieldEditor" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <script src="CustomModules.aspx.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="onecolumn">
        <div class="header">
            <span id="Name">
               
            </span>
        </div>
        <br class="clear" />
        <div class="content" id="CustomModulesDetails">
            <br class="clear" />
            <input type="hidden" id="ID" runat="server" />
            <input type="hidden" id="ClientSiteID" runat="server" />
            <input type="hidden" id="FeaturesModuleID" runat="server" />
            <table width="100%" cellpadding="10" cellspacing="10">
             
                <tr>
                    <td width="30%">Name of form:</td>
                    <td><input type="text" valtype="required" id="inpName" runat="server" /></td>
                </tr>
                <tr>
                    <td>
                    Description of form:
                    </td>
                    <td>
                    <input type="text" valtype="required" id="inpDescription" runat="server" />
                    </td>
                </tr>
                  <tr>
                    <td>Message to show user after form submission</td>
                    <td><textarea class="details" mytitle="This message will appear in a popup dialog once the user successfully completes the form." id="inpCustomMessage" runat="server" cols="60" rows="10"></textarea></td>
                </tr>
                 </table>
           <%-- <fieldset style="border:1px solid #CCC;margin-right:50px;"><legend>Notification Settings</legend>--%>
            <table width="100%" cellpadding="10" cellspacing="10">
                
                <tr>
                    <td width="30%">
                    Addresses to receive notification email:
                    </td>
                    <td>
                        <input type="hidden" id="inpEmailRecipients" size="60" runat="server" />
                        <div style="width:390px;padding-bottom:5px;"><a id="add-email" title="Add an email address">Add a Recipient <img title="Add an email address" class="help" style="cursor: pointer;" alt="Add Email" src="Styles/images/add.png"/></a></div>
                        <div class="details" mytitle="These email addresses will receive a notification email when a visitor completes the form. Defaults to the site owner's email address if none are specified." id="receipientList"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    Notification Email Subject:
                    </td>
                    <td>
                    <input class="details" mytitle="The subject of the notification email." type="text" id="inpEmailSubject" size="60" runat="server" />
                    </td>
                </tr>
				<tr>
                    <td>
                    Include Custom Form in Email:
                    </td>
                    <td>
                    <input class="details" mytitle="Include Custom Form in Email" type="checkbox" id="inpIncludeForm" size="60" runat="server" />
                    </td>
                </tr>
                </table>
                   <%-- </fieldset>--%>
              <table width="100%" cellpadding="10" cellspacing="10">
                <tr>
                    <td colspan="2">
                        <uc1:CustomFieldEditor ID="CustomFieldEditor1" runat="server"  />
                    </td>
                </tr>
            </table>
                
        </div>
        <input type="button" id="btnSave" value="Save" style="float:right;" onclick="saveCustomModule();" />
    </div>
    </div>
            
    </form>

    <div id="dialog-form" title="Add Email Address">
	        <form>
	        <fieldset>
                 <div id="validMessage"></div>
		        <label for="email">Email</label>
		        <input type="text" name="email" id="email" size="35" value="" class="text ui-widget-content ui-corner-all" />
	        </fieldset>
	        </form>
        </div>
</body>
</html>
