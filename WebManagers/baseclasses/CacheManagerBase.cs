﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;



namespace SAWD.WebManagers
{
    public abstract class CacheManagerBase
    {

        /// <summary>
        /// Method that saves an object to the system cache and creates a SQLDependency for it
        /// </summary>
        /// <param name="name">Name of the cached object</param>
        /// <param name="value">The cached object</param>
        /// <param name="sqlcommand">A query or stored procedure that the dependency will be based on</param>
        /// <param name="sqlparams">Optional paramter list to pass to the stored proc used in the command</param>
        protected static void SetCacheSQLDepend(string name, object value, string sqlcommand, params SqlParameter[] sqlparams)
        {
            
            HttpContext context = HttpContext.Current;
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["netTiersConnectionString"].ToString()))
            {                
                connection.Open();
                using (SqlCommand command = new SqlCommand(sqlcommand, connection))
                {
                    //iterate through the parameters that may or may not have been passed in
                    if (sqlparams != null)
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        for (int i = 0; i < sqlparams.Length; i++)
                        {
                            command.Parameters.Add(sqlparams[i]);
                        }
                    }
                    //create the dependency object
                    System.Web.Caching.SqlCacheDependency dep = new System.Web.Caching.SqlCacheDependency(command);

                    //execute the command. This must be done in oorder for the dependency to be registered                    
                    command.ExecuteReader();                   
                    context.Cache.Insert(name, value, dep);                    
                }                
            }           
        }


        /// <summary>
        /// Method that saves an object to the system cache
        /// </summary>
        /// <param name="name">Name used to identify the item in the cache</param>
        /// <param name="value">The object to be placed in the cache</param>
        protected static void SetCache(string name, object value)
        {
            SetCache(name, value, new TimeSpan(0,30,0));
        }


        /// <summary>
        /// Method that saves an object to the system cache
        /// </summary>
        /// <param name="name">Name used to identify the item in the cache</param>
        /// <param name="value">The object to be placed in the cache</param>
        /// <param name="cacheexpiry">The time span for the cache expiry</param>
        protected static void SetCache(string name, object value, TimeSpan cacheexpiry)
        {

            HttpContext context = HttpContext.Current;
			if (context.Cache[name] == null && value != null) {
				context.Cache.Add(name, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, cacheexpiry, System.Web.Caching.CacheItemPriority.Normal, null);
			} else {
				if (value == null) {
					context.Cache.Remove(name);
				} else {
					context.Cache[name] = value;
				}
			}
            //new System.Web.Caching.SqlCacheDependency(
        }


        /// <summary>
        /// Method that gets a named object from the system cache
        /// </summary>
        /// <param name="name">Name of the object to return from cache</param>
        /// <returns>Returns an object from the cache</returns>
        protected static object GetCache(string name)
        {

            return GetCache(name, null);
        }

        /// <summary>
        /// Method that gets a named object from the system cache - this overload returns a default value if the object is null
        /// </summary>
        /// <param name="name">Name of the object to return from cache</param>
        /// <param name="defaultvalue">Value to return if the cache is null</param>
        /// <returns>An object from the cache</returns>
        protected static object GetCache(string name, object defaultvalue)
        {

            HttpContext context = HttpContext.Current;
            if (context.Cache[name] == null && defaultvalue != null)
                return defaultvalue;
            return context.Cache[name];
        }
    }
}
