﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SAWD.WebManagers
{
    public abstract class SessionManagerBase
    {
        /// <summary>
        /// Base method that sets a session variable
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="value">The value to be placed in the variable</param>
        protected static void SetSession(string name, object value)
        {
           HttpContext context = HttpContext.Current;
           if (context.Session[name] == null)
               context.Session.Add(name, value);
           else
               context.Session[name] = value;
          
        }

        /// <summary>
        /// Returns a value in the session based on the name
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <returns>An object</returns>
        protected static object GetSession(string name)
        {
            return GetSession(name, null);
        }

        /// <summary>
        /// Returns a value in the session based on the name
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="defaultvalue">A default value to return if the session value is null</param>
        /// <returns>An object</returns>
        protected static object GetSession(string name, object defaultvalue)
        {

            HttpContext context = HttpContext.Current;
            if (context.Session[name] == null && defaultvalue != null)
                return defaultvalue;
            return context.Session[name];
        }

        public static void ClearSession()
        {
            HttpContext context = HttpContext.Current;
            context.Session.Clear();
        }

    }
}
