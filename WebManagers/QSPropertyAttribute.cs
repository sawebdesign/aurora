﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    [AttributeUsage(AttributeTargets.Property)]
    public class QSPropertyAttribute : Attribute
    {

        private string querystringvarname;
        private bool showifnull;
        private object nullequivalent;


        public bool ShowIfNull
        {
            get { return showifnull; }
            set { showifnull = value; }
        }

        //public object NullEquivalent
        //{
        //    get { return nullequivalent; }
        //    set { nullequivalent = value; }
        //}
        
        public string QuerystringVarName
        {
            get { return querystringvarname; }
            set { querystringvarname = value; }
        }



    }
}
