﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Aurora._Default" EnableViewState="false" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <link href="/styles/global.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-1.5.2.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <style type="text/css">
        .placeholder
        {
            display: none;
        }

    </style>
    <noscript>
        <style type="text/css">
    body
    {
        margin-top:70px!important;
    }
  
    </style>
        <div style='top: 0px; left: 0px; position: absolute; width: 100%; height: 70px; float: left;
            background: #000;z-index:9999999;'>
            <div style="float: left;">
                <img src="/styles/images/boltman.png" width="80" height="70" />
            </div>
            <div style="float: left; font-weight: bold; font-size: 20px; margin-top: 15px; color: #fff700;">
                Um...There is content missing, click <a style="color: red;" href="http://www.google.com/support/bin/answer.py?answer=23852">
                    here</a> to find out why?</div>
        </div>
    </noscript>
   
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <div id="inputcontent" runat="server" clientidmode="Static" enableviewstate="True">
    </div>
    <div id="AttachedModules">
    </div>
    <div id="AttachedModulesDetails">
    </div>
   
    
    <script src="/scripts/microsoftajax.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="/scripts/utils.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/feedbackcontentslider.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery-cookie.js?<%=lastModifiedDate%>" type="text/javascript"></script>
	<script src="/scripts/plugins/boltaccordion.js" type="text/javascript"></script>
    <script src="/scripts/modules.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
	<script src="/scripts/accessiblelist.js" type="text/javascript"></script>
    <script src="/scripts/common.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?<%=lastModifiedDate%>" type="text/javascript" defer="defer"></script>
    <script src="/scripts/calendarcontrol.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/touchpunch.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/jsrender.js?<%=lastModifiedDate %>" type="text/javascript"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?<%=lastModifiedDate%>" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/superfish.js?<%=lastModifiedDate%>" type="text/javascript"></script>
	
   	<script type="text/javascript" src="/scripts/plugins/contentslide.js"></script>
    <script type="text/javascript" defer="async">
        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
</asp:Content>
