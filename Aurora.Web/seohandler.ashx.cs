﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora {
	/// <summary>
	/// Summary description for seohandler
	/// </summary>
	public class seohandler : IHttpHandler {

		public void ProcessRequest(HttpContext context) {

			long ClientSiteID = 0;
			string requestType = context.Request.QueryString["requesttype"];
			context.Response.ContentType = "text/plain";
			string currentDomain = context.Request.Url.Host;

			if (currentDomain == "auroraweb") {
				currentDomain = "simplyinvesting.boltcms.com";
			}

			using (var dbcontext = new Aurora.Custom.Data.AuroraEntities()) {

				var domain = dbcontext.Domain.Where(d => d.DomainName.Contains(currentDomain)).FirstOrDefault();

				if (domain != null) {
					ClientSiteID = domain.ClientSiteID;
				}

				if (ClientSiteID == 0) {
					ErrorReturn(ref context);
					return;
				} else {
					if (requestType == "robots") {
						RobotsReturn(ref context, ClientSiteID);
					} else if (requestType == "sitemap") {
						SitemapReturn(ref context, ClientSiteID);
					} else {
						ErrorReturn(ref context);
					}
				}
			}

		}

		public void RobotsReturn(ref HttpContext context, long clientsiteID) {
			string path = context.Server.MapPath("\\clientdata\\" + clientsiteID + "\\uploads\\");
			string filename = "robots.txt";
			context.Response.ContentType = "text/plain";
			if (File.Exists(path + filename)) {
				context.Response.Write(File.ReadAllText(path + filename));
			} else {
				context.Response.Write("User-agent: * \nAllow: /");
			}
			return;
		}

		public void SitemapReturn(ref HttpContext context, long clientsiteID) {
			string path = context.Server.MapPath("\\clientdata\\" + clientsiteID + "\\uploads\\");
			string filename = "sitemap.xml";
			string currentDomain = context.Request.Url.Host;
			context.Response.ContentType = "text/xml";
			if (File.Exists(path + filename)) {
				context.Response.Write(File.ReadAllText(path + filename));
			} else {
				context.Response.Charset = Encoding.UTF8.WebName;
				XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
				XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
				XElement root = new XElement(ns + "urlset");
				doc.Add(root);
				
				using (AuroraEntities service = new AuroraEntities()) {
					List<PageContent> pages = (from s in service.PageContent
											   join r in service.RoleModule on s.ID equals r.ModuleItemID into roles
											   where s.ClientSiteID == clientsiteID
											   && s.isArchived == false
											   && s.DeletedOn == null
											   && roles.Count() == 0
											   && s.IsSecured == false
											   && s.PageIndex >= 0
											   orderby s.isDefault descending, s.PageIndex
											   
											   select s).ToList();
					foreach (PageContent p in pages) {
						XElement url = new XElement(ns+"url");
						if (p.isDefault) {
							url.Add(new XElement(ns + "loc", "http://" + currentDomain + "/"));
						} else {
							url.Add(new XElement(ns + "loc", "http://" + currentDomain + "/page/" + p.Description.Replace(" ", "-").Replace("?", "").ToLower()));
						}
						url.Add(new XElement(ns + "lastmod", String.Format("{0:yyyy-MM-dd}", p.InsertedOn)));
						root.Add(url);
					}
				}

				context.Response.Write(doc.ToString());
				
			}
			return;
		}

		public void ErrorReturn (ref HttpContext context){
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetNoStore();
			context.Response.Cache.SetExpires(DateTime.MinValue);

			//Internal Server Error
			context.Response.StatusCode = 404;
			context.Response.End();
			return;
		}

		public bool IsReusable {
			get {
				return false;
			}
		}
	}
}