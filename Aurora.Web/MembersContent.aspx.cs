﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora {
    public partial class MembersContent : Common.BasePage {
        protected void Page_Load(object sender, EventArgs e) {
            if (SessionManager.VistorSecureSessionID != Guid.Empty && SessionManager.MemberData != null) {

                AuroraEntities entity = new AuroraEntities();

                var roles = (from memberRoles in entity.MemberRole
                             where memberRoles.MemberID == SessionManager.MemberData.ID
                             && memberRoles.LapsedOn == null
                             && memberRoles.IsActivated == true
                             && memberRoles.ClientSiteID == SessionManager.ClientSiteID
                             && memberRoles.Replaced == null
                             select memberRoles.RoleID).ToList<long>();

                var menuItems = from roleModule in entity.RoleModule
                                where roles.Contains(roleModule.RoleID)
                                select roleModule;
				Content.Text += "<tr><td colspan='2'><h3 class='memberContent'>Below is a list of content that is available for you. Click a link below to read more.</h3></td></tr>";
													  
                string currentModule = "";
                foreach (RoleModule roleModules in menuItems) {
                    Aurora.Custom.Link itemLink = new Aurora.Custom.Link();

                    currentModule = roleModules.ModuleName;
                    switch (roleModules.ModuleName) {
                        case "News":
                            itemLink.Title = (from news in entity.News
                                         where news.ID == roleModules.ModuleItemID
										 && news.DeletedBy == null
										 && news.DeletedOn == null
                                         select news.Title).SingleOrDefault();
                            break;
                        case "Events":
                            itemLink.Title = (from events in entity.Event
                                         where events.ID == roleModules.ModuleItemID
										 && events.DeletedBy == null
										 && events.DeletedOn == null
                                         select events.Title).SingleOrDefault();

                            break;
                        case "PageDetail":
                            var item = (from page in entity.PageContent
                                         where page.ID == roleModules.ModuleItemID
										 && page.DeletedBy == null
										 && page.DeletedOn == null
                                         select new {page.Description, page.LongTitle }).SingleOrDefault();
                            itemLink.Title = item.LongTitle;
                            itemLink.URL = item.Description;
                            break;
                    }
                    if (!String.IsNullOrEmpty(itemLink.Title)) {
                        Content.Text +=
                        String.Format(
							"<tr><td colspan='2'><a dataid='{0}' datatitle='{1}' href=\"/{1}/{3}\">{2}</a></td></tr>",
                            roleModules.ModuleItemID, 
							roleModules.ModuleName.Replace("Detail",string.Empty).ToLower(),
							itemLink.Title,
                            itemLink.URL.Replace(" ", "-").Replace("?", "").ToLower());    
                    }
                    
                }
            }
            else {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}