﻿using System;
using System.Web;
using System.Xml;
using Aurora.Custom;
using System.Linq;
using System.Web.SessionState;
namespace Aurora
{
    public class iCalendar : IHttpHandler, IRequiresSessionState
    {
        string[] dayNames = { "NA", "SU", "MO", "TU", "WE", "TH", "FR", "SA" };
        int weekNumber = 0;
        int dayNumber = 0;

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        string DateFormat
        {
            get
            {
                return "yyyyMMddTHHmmss"; // 20060215T092000Z
                                           // 20111122T120000
            }
        }

        DateTime FindNextMonthlyDate(DateTime dateToStart) {
            int currentWeek = dateToStart.GetCurrentMonthWeekNumber();
            DateTime nextMonthlyDate = new DateTime(dateToStart.Ticks);
            while (nextMonthlyDate.DayOfWeek.ToString().Substring(0, 2).ToUpper() != dayNames[dayNumber] ||
                weekNumber != currentWeek) {
                    if (nextMonthlyDate.DayOfWeek.ToString().Substring(0, 2).ToUpper() == dayNames[dayNumber]) {
                        nextMonthlyDate = nextMonthlyDate.AddDays(7);
                    }
                    else {
                        nextMonthlyDate = nextMonthlyDate.AddDays(1);
                    }
                    currentWeek = nextMonthlyDate.GetCurrentMonthWeekNumber();
            }
            
            return nextMonthlyDate;
        }

        int GetNumberOfOccurrences(string frequency, DateTime dateToStart, DateTime dateToEnd) {
            int counter = 0;
            while (dateToStart < dateToEnd) {
                ++counter;
                if (frequency == "Monthly") {
                    if (weekNumber == 0 && dayNumber == 0) {
                        dateToStart = dateToStart.AddMonths(1);
                    }
                    else {
                        dateToStart = FindNextMonthlyDate(dateToStart.AddDays(1));
                    }
                }
                else if (frequency == "Yearly") {
                    dateToStart = dateToStart.AddYears(1);
                }
            }
            return counter;
        }

        int TotalMonths(DateTime start, DateTime end) {
            return (start.Year * 12 + start.Month) - (end.Year * 12 + end.Month);
        }

        public void ProcessRequest(HttpContext httpcontext)
        {
            Aurora.Custom.Data.AuroraEntities context = new Aurora.Custom.Data.AuroraEntities();

            long ClientSiteID = (long)SessionManager.ClientSiteID;
            string timeZoneID = "South Africa Standard Time";

            if (httpcontext.Request["EventID"] != null)
            {
                int evtID = 0;
                int.TryParse(httpcontext.Request["EventID"], out evtID);

                var site = (from siteAlias in context.ClientSite
                            where siteAlias.ClientSiteID == ClientSiteID
                            select siteAlias).SingleOrDefault();

                var events = (from evnt in context.Event
                            where evnt.ID == evtID
                            select evnt).SingleOrDefault();


                if (events != null)
                {

                    if (site != null) 
                        if (!string.IsNullOrEmpty(site.TimeZoneID))
                            timeZoneID = site.TimeZoneID;
                        
                    
                    DateTime nextAvailableDay = events.StartDate.Value;
                    string dateStart = "\nDTSTART:16010101T000000";

                    httpcontext.Response.ContentType = "text/calendar";
                    httpcontext.Response.AddHeader("Content-disposition", String.Format("attachment; filename=appointment_{0}.ics",events.Title));
                   //some safety date checks
                    if (events.CustomXML != null)
                    {

                        XmlDocument filter = new XmlDocument();
                        filter.LoadXml(events.CustomXML);
                       
                        if (filter.DocumentElement.ParentNode.ChildNodes != null)
                            foreach (XmlNode nodes in filter.DocumentElement.ParentNode.ChildNodes)
                            {
                                weekNumber =
                                    int.Parse(nodes.ChildNodes.Item(0).ChildNodes.Item(0).Attributes.Item(1).Value);
                                dayNumber =
                                    int.Parse(nodes.ChildNodes.Item(0).ChildNodes.Item(1).Attributes.Item(1).Value);
                            }

                        if (weekNumber == 5)
                        {
                            weekNumber = -1;
                        }

                        int currentWeekNum = events.StartDate.Value.GetCurrentMonthWeekNumber();
                        if (events.Occurance == "Monthly") {

                            //Find first applicable date for the event to occur on
                            events.StartDate = FindNextMonthlyDate((DateTime)events.StartDate);
                        }
                    }
                    httpcontext.Response.Write("BEGIN:VCALENDAR");
                    httpcontext.Response.Write("\nPRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN");
                    httpcontext.Response.Write("\nVERSION:2.0");
                    httpcontext.Response.Write("\nMETHOD:REQUEST");
                    httpcontext.Response.Write("\nX-MS-OLK-FORCEINSPECTOROPEN:TRUE");
                    httpcontext.Response.Write("\nBEGIN:VTIMEZONE");
                    httpcontext.Response.Write("\nTZID:" + timeZoneID + "");
                    httpcontext.Response.Write("\nBEGIN:STANDARD");
                    httpcontext.Response.Write(dateStart);
                    httpcontext.Response.Write("\nTZOFFSETFROM:+0200");
                    httpcontext.Response.Write("\nTZOFFSETTO:+0200");
                    httpcontext.Response.Write("\nEND:STANDARD");
                    httpcontext.Response.Write("\nEND:VTIMEZONE");
                    httpcontext.Response.Write("\nBEGIN:VEVENT");
                    httpcontext.Response.Write("\nCLASS:PUBLIC");
                    httpcontext.Response.Write("\nCREATED:" + events.InsertedOn.ToString(DateFormat+"Z"));
                    if (events.Occurance != "None")
                    {
                        DateTime s = new DateTime(events.StartDate.Value.Ticks);
                        TimeSpan ts = new TimeSpan(events.EndDate.Value.Hour, events.EndDate.Value.Minute, 0);
                        s = s.Date + ts;
                        httpcontext.Response.Write("\nDTEND;TZID=\"" + timeZoneID + "\":" +
                                                   s.ToString(DateFormat));
                        //httpcontext.Response.Write("\nDTEND;TZID=\"South Africa Standard Time\":" +
                        //                           events.EndDate.Value.ToString(DateFormat));
                    }
                    httpcontext.Response.Write("\nDTSTAMP:" + events.StartDate.Value.ToString(DateFormat + "Z"));
                    if (events.Occurance != "None")
                    {
                        httpcontext.Response.Write("\nDTSTART;TZID=\"" + timeZoneID + "\":" +
                                                   events.StartDate.Value.ToString(DateFormat));
                    }
                    httpcontext.Response.Write("\nLAST-MODIFIED:" + DateTime.Now.ToString(DateFormat+"Z"));
                    httpcontext.Response.Write("\nLOCATION:"+events.Location);
                    httpcontext.Response.Write("\nORGANIZER;CN="+CacheManager.SiteData.Email1+":mailto:"+CacheManager.SiteData.Email1);
                    httpcontext.Response.Write("\nPRIORITY:5");
                    //check occurance
                    switch (events.Occurance)
                    {
                        case "Weekly":
                            httpcontext.Response.Write("\nRRULE:FREQ=WEEKLY;");
                            if (events.EndDate.Value.Year != 2050) {
                                TimeSpan span = (DateTime)events.EndDate - (DateTime)events.StartDate;
                                httpcontext.Response.Write("COUNT=" + Math.Ceiling(span.TotalDays / 7) + ";");
                            }
                            httpcontext.Response.Write(string.Format("BYDAY={0}", events.StartDate.Value.DayOfWeek.ToString().Substring(0, 2)));
                            break;
                        case "Monthly":
                            httpcontext.Response.Write("\nRRULE:FREQ=MONTHLY;");
                            if (events.EndDate.Value.Year != 2050) {
                                httpcontext.Response.Write("COUNT=" + GetNumberOfOccurrences(events.Occurance, (DateTime)events.StartDate, (DateTime)events.EndDate) + ";");
                            }
                            if (events.CustomXML == null)
                            {
                                httpcontext.Response.Write(string.Format("BYMONTHDAY={0}", events.StartDate.Value.Day));
                            }
                            else
                            {
                               
                                httpcontext.Response.Write(string.Format("BYDAY={0};BYSETPOS={1}", dayNames[dayNumber],weekNumber));
                            }

                            break;
                        case "Daily":
                            
                            httpcontext.Response.Write("\nRRULE:FREQ=DAILY;");
                            if (events.EndDate.Value.Year != 2050) {
                                TimeSpan span = (DateTime)events.EndDate - (DateTime)events.StartDate;
                                httpcontext.Response.Write("COUNT=" + Math.Ceiling(span.TotalDays));
                            }

                            break;
                        case "Yearly":
                            httpcontext.Response.Write("\nRRULE:FREQ=YEARLY;");
                            if (events.EndDate.Value.Year != 2050) {
                                httpcontext.Response.Write("COUNT=" + GetNumberOfOccurrences(events.Occurance, (DateTime)events.StartDate, (DateTime)events.EndDate) + ";");
                            }
                            httpcontext.Response.Write(string.Format("BYMONTHDAY={0};BYMONTH={1}", events.StartDate.Value.Day, events.StartDate.Value.Month));
                            break;
                        case "None":
                            
                            break;
                    }
                    httpcontext.Response.Write("\nSEQUENCE:0");
                    httpcontext.Response.Write("\nSUMMARY;LANGUAGE=en-za:"+events.Preview);
                    httpcontext.Response.Write("\nTRANSP:OPAQUE");
                    httpcontext.Response.Write("\nUID:"+events.InsertedOn.ToString(DateFormat)+"Z@mysite.com");
                    httpcontext.Response.Write("\nX-ALT-DESC;FMTTYPE=text/html:<html>" + events.Details + "</html>");
                    httpcontext.Response.Write("\nEND:VEVENT");
                    httpcontext.Response.Write("\nEND:VCALENDAR");

                    httpcontext.Response.End(); 
                }
                else
                {
                    httpcontext.Response.Write("<script>window.close();<script>");
                    httpcontext.Response.End();
                }
            }
        }
    }
}