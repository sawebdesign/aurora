﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Xml.Linq;
using System.Globalization;

namespace Aurora {
    public partial class ProcessPayment : System.Web.UI.Page {
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e) {
            if (Request["trns"] == null) {
                GetPaymentSchema();
            } else {
                Response.Redirect("~/CompletedPayment.aspx");
            }
        }


        protected void GetPaymentForm(long paymentSchemaID, decimal total, long transactionID) {

            var paymentForm = (from clientForms in entities.PaymentSchemaClientSite
                               join paymentSchema in entities.PaymentSchema on clientForms.PaymentSchemaID equals paymentSchema.ID
                               where clientForms.ClientSiteID == SessionManager.ClientSiteID
                                     && clientForms.PaymentSchemaID == paymentSchemaID
                               select new { clientForms, paymentSchema }).SingleOrDefault();
				total = Convert.ToDecimal(Utils.ConvertCurrency(Convert.ToDouble(total), null, paymentForm.paymentSchema.CurrencyID));

            if (paymentForm != null && !paymentForm.paymentSchema.isEFT) {
                //added to pad numbers to the right as per the pagyment gateways reques
                string newTotal = String.Format("{0:0.00}", total);
                if (newTotal.Contains(".") && paymentForm.paymentSchema.Name == "PayGate") {
                    newTotal = newTotal.Replace(".", string.Empty);
                } else if (!newTotal.Contains(".") && paymentForm.paymentSchema.Name == "PayGate") {
                    newTotal = newTotal + "00";
                }

                //Bind customer data to form
                string trasactionDate = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm");
                var transActionData = (from transactions in entities.TransactionLog
                                       where transactions.ID == transactionID
                                       select transactions).SingleOrDefault();

                if (transActionData.DataXML != null) {
                    XDocument doc = XDocument.Parse(transActionData.DataXML);
                    doc.Root.SetAttributeValue("PaymentGatewayTotal", total);
                    transActionData.DataXML = doc.ToString();
                }
				entities.SaveChanges();
                if (transActionData.PaymentType == "Donation") {
                    //Get Donors Email Address
                    XmlDocument tranactionXML = new XmlDocument();
                    tranactionXML.LoadXml(transActionData.DataXML);

                    var donorEmailAddress = (from xmldata in tranactionXML.GetElementsByTagName("field").Cast<XmlNode>()
                                             where xmldata.Attributes["fieldname"].Value == "donationEmailAddress"
                                             select xmldata.Attributes["value"].Value).FirstOrDefault();

                    string reference = (from xmldata in tranactionXML.GetElementsByTagName("field").Cast<XmlNode>()
                                        where xmldata.Attributes["fieldname"].Value == "reference"
                                        select xmldata.Attributes["value"].Value).FirstOrDefault() ?? "";

                    if (donorEmailAddress == null) {
                        donorEmailAddress = CacheManager.SiteData.Email1;
                    }

                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{ClientName}}", CacheManager.SiteData.CompanyName + "Donations");
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Email}}", Utils.ConvertDBNull(donorEmailAddress, ""));
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Total}}", newTotal);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Date}}", trasactionDate);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Currency}}", "ZAR");
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Return}}", Request.Url.ToString().ToUpper().Replace("PROCESSPAYMENT.ASPX", "CompletedPayment.aspx?trns=") + transactionID.ToString());
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{MerchantID}}", paymentForm.clientForms.MerchantID);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{ID}}", transactionID.ToString());
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Reference}}", transactionID.ToString() + "-" + reference);

                    paymentForm.clientForms.FormHTML = isRecurring(paymentForm.clientForms.FormHTML, paymentForm.clientForms.RecurringHTML, paymentForm.paymentSchema.Name, ref transActionData, ref newTotal);

                    if (paymentForm.clientForms.FormHTML.IndexOf("{{CheckSum}}") > -1) {

                        if (paymentForm.paymentSchema.Name.ToLower() == "vcs") {
                            paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{CheckSum}}"
                                                               , Utils.GetMD5Hash(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}"
                                                               , paymentForm.clientForms.MerchantID
                                                               , transactionID.ToString() + "-" + reference
                                                               , newTotal
                                                               , "ZAR"
                                                               //, Request.Url.ToString().ToUpper().Replace("PROCESSPAYMENT.ASPX", "CompletedPayment.aspx?trns=") + transactionID.ToString()
                                                               , "N"
                                                               , transActionData.Duration.ToString()
                                                               , "M"
                                                               , newTotal
                                                               , "secret")));
                        } else {
                            paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{CheckSum}}"
                                                               , Utils.GetMD5Hash(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}"
                                                               , paymentForm.clientForms.MerchantID
                                                               , transactionID.ToString() + "-" + reference
                                                               , newTotal
                                                               , "ZAR"
                                                               , Request.Url.ToString().ToUpper().Replace("PROCESSPAYMENT.ASPX", "CompletedPayment.aspx?trns=") + transactionID.ToString()
                                                               , trasactionDate
                                                               , donorEmailAddress
                                                               , "secret")));
                        }
                    }
                
                } else {

                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{ClientName}}", SessionManager.MemberData.FirstName + " " + SessionManager.MemberData.LastName);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Email}}", Utils.ConvertDBNull(SessionManager.MemberData.Email, ""));
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Total}}", newTotal);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Date}}", trasactionDate);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Currency}}", "ZAR");
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Return}}", Request.Url.ToString().ToUpper().Replace("PROCESSPAYMENT.ASPX", "CompletedPayment.aspx?trns=") + transactionID.ToString());
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{MerchantID}}", paymentForm.clientForms.MerchantID);
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{ID}}", transactionID.ToString());
                    paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{Reference}}", transactionID.ToString());

                    paymentForm.clientForms.FormHTML = isRecurring(paymentForm.clientForms.FormHTML, paymentForm.clientForms.RecurringHTML, paymentForm.paymentSchema.Name, ref transActionData, ref newTotal);

                    if (paymentForm.clientForms.FormHTML.IndexOf("{{CheckSum}}") > -1) {

                            paymentForm.clientForms.FormHTML = paymentForm.clientForms.FormHTML.Replace("{{CheckSum}}"
                                                               , Utils.GetMD5Hash(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}"
                                                               , paymentForm.clientForms.MerchantID
                                                               , SessionManager.MemberData.FirstName + " " + SessionManager.MemberData.LastName
                                                               , newTotal
                                                               , "ZAR"
                                                               , Request.Url.ToString().ToUpper().Replace("PROCESSPAYMENT.ASPX", "CompletedPayment.aspx?trns=") + transactionID.ToString()
                                                               , trasactionDate
                                                               , Utils.ConvertDBNull(SessionManager.MemberData.Email, "")
                                                               , "secret")));
                     
                    }
                }

                if (paymentForm.clientForms.FormHTML.IndexOf("{*}") != -1) {
                    formData.Value = paymentForm.clientForms.FormHTML.Substring(0, paymentForm.clientForms.FormHTML.IndexOf("{*}"));
                }

                string actionUrl = paymentForm.clientForms.FormHTML.Substring(paymentForm.clientForms.FormHTML.IndexOf("formaction"));
                actionUrl = actionUrl.Substring(actionUrl.IndexOf("value="));
                actionUrl = actionUrl.Replace("value=\"", string.Empty);
                if (actionUrl.IndexOf(@"/>") > -1) {
                    actionUrl = actionUrl.Substring(0, actionUrl.IndexOf(@"/>")).Replace("\">", string.Empty);
                } else if (actionUrl.IndexOf(">") > -1) {
                    actionUrl = actionUrl.Substring(0, actionUrl.IndexOf(">")).Replace(">", string.Empty);
                }

                action.Value = actionUrl.Replace("\"", string.Empty);


                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "execute", "$(function(){ $('#sendPayment').html($('#formData').val());$('#sendPayment').attr('action',$('#action').val()); $('#sendPayment')[0].submit();});", true);
            }
            if (paymentForm != null && paymentForm.paymentSchema.isEFT) {
                Response.Redirect("~/CompletedPayment.aspx?eft=1&trns=" + Server.UrlEncode(transactionID.ToString()));
            } else if (paymentForm != null) {
                Response.Write("We are redirecting you to the payment gateway please wait...");
            } else if (paymentForm == null) {
                Response.Write("There were no transactions to process");
            }


        }

        protected string isRecurring(string FormHTML, string RecurringHTML, string paymentSchemaName, ref TransactionLog transActionData, ref string newTotal) {
            string retVal = "";
            string recurringString = "";
            if (FormHTML.IndexOf("{{Recurring}}") > -1) {
                if (transActionData.IsRecurring == true) {
                    var duration = Math.Ceiling((double)transActionData.Duration / 30);

                    //if (duration > 1 || duration == 0) {
                    recurringString = RecurringHTML;
                    string durationText;
                    switch (paymentSchemaName.ToLower()) {
                        case "paypal":
                            recurringString = recurringString.Replace("{{Total}}", (Math.Round(decimal.Parse(newTotal)/(decimal)duration,2)).ToString());

                            if (duration > 1) {
                                recurringString = recurringString.Replace("{{Recurrence}}", "1");
                                recurringString += String.Format("<input type=\"hidden\" name=\"srt\" value=\"{0}\">", duration.ToString());
                            } else if (duration == 0) {
                                recurringString = recurringString.Replace("{{Recurrence}}", "0");
                            }
                            FormHTML = FormHTML.Replace("_xclick", "_xclick-subscriptions");
                            break;
                        case "vcs":
                            recurringString = recurringString.Replace("{{Total}}", newTotal);
                            recurringString = recurringString.Replace("{{Recurrence}}", transActionData.Duration.ToString());
                            break;
                        default:
                            durationText = "";
                            recurringString = recurringString.Replace("{{Recurrence}}", "1 Month");
                            if (duration > 1) {
                                durationText = duration.ToString("0") + " Month";
                            } else if (duration == 0) {
                                durationText = "Forever";
                            }
                            recurringString = recurringString.Replace("{{Duration}}", durationText);
                            break;
                    }
                    //}
                }
            }
            retVal = FormHTML.Replace("{{Recurring}}", recurringString);
            return retVal;
        }

        protected void GetPaymentSchema() {
            if (SessionManager.CurrentTransactions != null) {
                //TODO: need to figure out a way to actually use the list
                decimal totalAmount = 0;
                long paymentID = 0;
                long transactionID = 0;
                //Garth: I've disabled this code because it looks like it is doing scary stuff
                //foreach (TransactionLog currentTransaction in SessionManager.CurrentTransactions) {
                //    totalAmount = totalAmount + currentTransaction.TotalAmount.Value;
                //    if (currentTransaction.PaymentSchemaID != null) paymentID = currentTransaction.PaymentSchemaID.Value;
                //    transactionID = currentTransaction.ID;
                //}
                if (SessionManager.CurrentTransactions.Count() > 0) {
                    var lastLog = SessionManager.CurrentTransactions.Last();
                    paymentID = lastLog.PaymentSchemaID ?? 0;
                    transactionID = lastLog.ID;
                    totalAmount = lastLog.TotalAmount ?? 0;
                    GetPaymentForm(paymentID, totalAmount, transactionID);
                }
            } else {
                Response.Write("There were no transactions to process");
            }

        }
    }
}