﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Data.SqlClient;
using System.Data;

namespace Aurora {
    /// <summary>
    /// Summary description for RSS
    /// </summary>
    public class RSS : IHttpHandler {

        public void ProcessRequest(HttpContext context) {

            long ClientSiteID = 0;
            int CatID = 0;

            Int32.TryParse(context.Request.QueryString ["catid"], out CatID);
            string ModuleType = context.Request.QueryString ["feedtype"];

            if(!string.IsNullOrEmpty(ModuleType)) {
                ModuleType = ModuleType.Trim();
            }

            string currentDomain = context.Request.Url.Host;

            if(currentDomain == "auroraweb") {
                currentDomain = "centrawealth.com.au";
            }

            using(var dbcontext = new Aurora.Custom.Data.AuroraEntities()) {

                var domain = dbcontext.Domain.Where(d => d.DomainName.Contains(currentDomain)).FirstOrDefault();

                if(domain != null) {
                    ClientSiteID = domain.ClientSiteID;
                }

                XDocument RootDoc = new XDocument();
                XElement RSSRoot = new XElement("rss");
                XElement RSSChannel = new XElement("channel");
                RSSRoot.Add(new XAttribute("version","2.0"));

                context.Response.ContentType = "text/xml";//application/rss+xml

            if(ClientSiteID != 0) {

                    //set the required channel elements
                    var site = dbcontext.ClientSite.Where(cs => cs.ClientSiteID == ClientSiteID && cs.DeletedOn == null).FirstOrDefault();

                    if(site != null) {

                        XElement title = new XElement("title");
                        XElement channelLink = new XElement("link");
                        XElement channelDesc = new XElement("description");
                        string siteDomain = "";

                        title.Value = site.CompanyName;
                        channelDesc.Value = site.CompanyDescription;

                        RSSChannel.Add(title);
                        RSSRoot.Add(RSSChannel);

                        if(domain != null) {
                            channelLink.Value = "http://" + domain.DomainName + "/";
                            siteDomain = domain.DomainName;
                        } else {
                            channelLink.Value = "http://www.sawebdesign.co.za";
                            siteDomain = "sawebdesign.co.za";
                        }

                        RSSChannel.Add(channelLink);
                        RSSChannel.Add(channelDesc);

                        //determine what information to export based on the type of module, it will always follow RSS format but the actual data will be differrent
                        switch(ModuleType) {
                            case "baby2mombragbook": {



                                    break;
                                }

                            case "news": {

                                    var newsItems = new List<News>();

                                    if(CatID != 0) {
                                        newsItems = dbcontext.News.Where(n => n.ClientSiteID == ClientSiteID && n.DeletedOn == null && n.CategoryID == CatID && n.EndDate >= DateTime.Now && n.StartDate <= DateTime.Now).OrderBy(o => o.StartDate).ToList();
                                    } else {
                                        newsItems = dbcontext.News.Where(n => n.ClientSiteID == ClientSiteID && n.DeletedOn == null && n.EndDate >= DateTime.Now && n.StartDate <= DateTime.Now).OrderBy(o => o.StartDate).ToList();
                                    }

                                    XElement channelItem, channelItemTitle, channelItemLink, channelItemDesc, pubDate;

                                    foreach(var newsItem in newsItems) {
                                        //declare new instances 
                                        channelItem = new XElement("item");
                                        channelItemTitle = new XElement("title");
                                        channelItemLink = new XElement("link");
                                        channelItemDesc = new XElement("description");
                                        pubDate = new XElement("pubDate");

                                        channelItemTitle.Value = newsItem.Title;
                                        channelItemLink.Value = "http://" + siteDomain + "/News/" + newsItem.ID + "/" + context.Server.UrlEncode(newsItem.Title.Replace(" ", "-"));
                                        channelItemDesc.Value = newsItem.Preview;

                                        //add the child elements to the item element
                                        channelItem.Add(channelItemTitle);
                                        channelItem.Add(channelItemLink);
                                        channelItem.Add(channelItemDesc);

                                        RSSChannel.Add(channelItem);
                                    }

                                    break;
                                }
                            case "events": {

                                    var siteEvents = CatID == 0 ?
                                        dbcontext.Event.Join(dbcontext.Category, evt => evt.CategoryID, category => category.ID,
                                                          (evt, category) => new { evt, category }).Where(
                                                              @t =>
                                                              @t.category.ClientSiteID == ClientSiteID &&
                                                              @t.evt.EndDate >= DateTime.Now
                                                              && @t.evt.DeletedOn == null
                                           ).Select(@t => @t.evt)
                                           : dbcontext.Event.Join(dbcontext.Category, evt => evt.CategoryID, category => category.ID,
                                                          (evt, category) => new { evt, category }).Where(
                                                              @t =>
                                                              @t.category.ClientSiteID == ClientSiteID &&
                                                              @t.evt.EndDate >= DateTime.Now
                                                               && @t.evt.DeletedOn == null
                                                              && @t.evt.CategoryID == CatID

                                           ).Select(@t => @t.evt);

                                    XElement channelItem, channelItemTitle, channelItemLink, channelItemDesc, pubDate;

                                    foreach(var eventItem in siteEvents) {
                                        //declare new instances 
                                        channelItem = new XElement("item");
                                        channelItemTitle = new XElement("title");
                                        channelItemLink = new XElement("link");
                                        channelItemDesc = new XElement("description");
                                        pubDate = new XElement("pubDate");

                                        channelItemTitle.Value = eventItem.Title;
                                        channelItemLink.Value = "http://" + siteDomain + "/Event/" + eventItem.ID + "/" + context.Server.UrlEncode(eventItem.Title.Replace(" ", "-"));
                                        channelItemDesc.Value = eventItem.Preview;

                                        //add the child elements to the item element
                                        channelItem.Add(channelItemTitle);
                                        channelItem.Add(channelItemLink);
                                        channelItem.Add(channelItemDesc);

                                        RSSChannel.Add(channelItem);
                                    }

                                    break;
                                }
                            default: {



                                    break;
                                }
                        }
                    }

                }

                RootDoc.Add(RSSRoot);
                context.Response.Write(RootDoc);
            }

        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}