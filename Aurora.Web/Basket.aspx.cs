﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Custom;

namespace Aurora {
    public partial class Basket : Aurora.Common.BasePage {
        private Custom.Data.AuroraEntities context = new Custom.Data.AuroraEntities();

        protected void Page_Load(object sender, EventArgs e) {
            if (!Page.IsPostBack)
            {
                bool hasBasket = ((from clientModule in context.ClientModule
                                   join featureModule in context.Module on clientModule.FeaturesModuleID equals
                                       featureModule.ID
                                   where clientModule.ClientSiteID == SessionManager.ClientSiteID &&
                                         featureModule.Name == "ProductBasket"
                                   select clientModule).SingleOrDefault() != null);
                if (!hasBasket)
                {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                 if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0 )
                 {
                     Response.Redirect("ProcessPayment.aspx");
                 }
            }
        }

    }
}