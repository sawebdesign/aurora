﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora
{
    public partial class ManageLicenses : Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionManager.VistorSecureSessionID == Guid.Empty)
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}