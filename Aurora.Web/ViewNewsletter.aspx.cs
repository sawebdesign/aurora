﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;

namespace Aurora {
    public partial class ViewNewsletter : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            #region Variables
            Guid messageID;
            long memberID;
            bool viewOnline;
            string emailAddress;
            var entities = new AuroraEntities(); 
            #endregion

            #region Parse Values from request
            long.TryParse(Request["memberid"], out memberID);
            Guid.TryParse(Request["messageid"], out messageID);
            bool.TryParse(Request["viewonline"], out viewOnline); 
            #endregion

            var member = entities.Member.Where(mem => mem.ID == memberID).FirstOrDefault();
            var commengine = entities.NewsLetterCommEngine.Where(comm => comm.SystemMessageID == messageID).FirstOrDefault();
            emailAddress = member.Email;
            #region Set Email to Opened
            if (member != null && commengine != null) {
                entities.UpdateCommEngineMessageOpened(commengine.CommEngineMessageID, member.Email);

            }
            #endregion

            #region Load Email For View Online
            if (viewOnline) {
                var message = entities.GetCommEngineMessage(commengine.CommEngineMessageID, emailAddress).FirstOrDefault();
                if (message != null) {
                    Response.Write(message.Message);
                }
            }
            #endregion

            #region Load Blank Image if from email client
            if (!viewOnline) {
                Response.ContentType = "image/gif";
                Response.AddHeader("Content-disposition","attachment; filename=blank.gif");
                Response.WriteFile(Server.MapPath("~/Styles/images/blank.gif"));
            }
            #endregion
        }
    }
}