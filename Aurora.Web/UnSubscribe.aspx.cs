﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;
#endregion

namespace Aurora {
    public partial class UnSubscribe : Aurora.Common.BasePage {

        #region Variables
        string emailAddress, messageComplete;
        bool formRequired = false;

        Guid messageID;
        long memberID;
        Member memberInfo;
        AuroraEntities context = new AuroraEntities();
        Dictionary<string, string> unsubscribeSettings = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e) {
            #region Populate vars
            Guid.TryParse(Request["mail"], out messageID);
            unsubscribeSettings = Utils.GetFieldsFromCustomXML("UnSubscribe");
            long.TryParse(Request["memberid"], out memberID);
            #endregion

            #region Get Member information
            memberInfo = context.Member.Where(mem =>
                mem.ClientSiteID == SessionManager.ClientSiteID
                && mem.ID == memberID)
                .FirstOrDefault();
            #endregion

            #region Verify information
            if (unsubscribeSettings.Count == 0) {
                headerPage.InnerHtml = "Module not setup.";
                tblUnsubscribe.Visible = false;
                return;
            } else if (memberID == 0 || messageID == Guid.Empty) {
                headerPage.InnerHtml = "Values submitted are incorrect";
                tblUnsubscribe.Visible = false;
                return;
            } else if (memberInfo == null) {
                headerPage.InnerHtml = "Your information was not found, please contact us.";
                tblUnsubscribe.Visible = false;
                return;
            }
            #endregion

            var optOutBefore = context.CommunicationOptOut.Where(op => op.SystemMessageID == messageID && op.MemberID == memberID).FirstOrDefault();
            if (optOutBefore != null) {
                divMessages.InnerHtml = "You have already been removed from our mailing list for this newsletter";
                tblUnsubscribe.Visible = false;
                return;
            }

            //set Email address 
            inpEmail.Value = memberInfo.Email;

            //Load News Letters
             if (bool.Parse(unsubscribeSettings["Unsubscribe_ShowOtherNewsLetter"])) {
                var newsletters = context.Newsletter.Where(news => news.ClientSiteID == SessionManager.ClientSiteID && news.isVisible == true);
                lstNewsletters.DataTextField = "Name";
                lstNewsletters.DataValueField = "ID";
            } else {
                 //hide newsletter list
                Page.Form.Style.Add(".reasons", "display:none;");
            }
            #region Load Module Settings
            lstReasons.Items.Clear();
            string[] unsubscribeReasons = unsubscribeSettings["Unsubscribe_Reasons"].Split('|');

            for (int reason = 0; reason < unsubscribeReasons.Length; reason++) {
                string[] reasonItem = unsubscribeReasons.ElementAt(reason).Split(',');
                lstReasons.Items.Add(new ListItem { Text = reasonItem[0], Value = reasonItem[1] });
            }


            txtOtherReason.Visible = bool.Parse(unsubscribeSettings["Unsubscribe_ShowOther"]);
            headerPage.InnerHtml = unsubscribeSettings["Unsubscribe_Header"];
            messageComplete = unsubscribeSettings["Unsubscribe_CompletedText"];
            divCMSZone.Attributes.Add("page", unsubscribeSettings["Unsubscribe_CMSZone"]);
            #endregion
        }

        /// <summary>
        /// Unsubcribes the user
        /// </summary>
        private void ValidateEmailAndOptOut() {
            string reasonForLeaving = string.Empty;

            //get the reason for leaving
            if (lstReasons.SelectedIndex > -1) {
                reasonForLeaving = lstReasons.SelectedValue;
            } else {
                reasonForLeaving = txtOtherReason.Text;
            }

            if (String.IsNullOrEmpty(reasonForLeaving) && bool.Parse(unsubscribeSettings["Unsubscribe_ReasonRequired"])) {
                ClientScript.RegisterStartupScript(GetType(), "noreason", "  $('<div>Please select a reason for un-subscription.</div>').dialog({modal: true,resizable: false,title: 'Error',width: '400',buttons: { 'Okay': function () { $(this).dialog('close') } }});", true);
                return;
            }


            foreach (ListItem newsletter in lstNewsletters.Items) {
                if (newsletter.Selected) {
                    //update opt out
                    CommunicationOptOut optOut = new CommunicationOptOut {
                        ClientSiteID = SessionManager.ClientSiteID,
                        SystemMessageID = messageID,
                        InsertedOn = DateTime.Now,
                        Reason = reasonForLeaving,
                        MemberID = memberID,
                        NewsletterID = long.Parse(newsletter.Value)
                    };

                    //save add to opt out
                    context.AddObject("CommunicationOptOut", optOut);
                }

            }
            context.SaveChanges();

            //alert
            ClientScript.RegisterStartupScript(GetType(), "complete", "  $('<div>Thank You for your time, you have been excluded from our mailing list.</div>').dialog({modal: true,resizable: false,title: 'Thank You',width: '400',buttons: { 'Okay': function () { $(this).dialog('close') } }});", true);
        }

        protected void btnUnSubscribe_Click(object sender, EventArgs e) {
            ValidateEmailAndOptOut();
        }
    }
}