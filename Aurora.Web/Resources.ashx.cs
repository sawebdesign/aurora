﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aurora.Custom;

namespace Aurora
{
    /// <summary>
    /// Summary description for Resources
    /// </summary>
    public class Resources : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Utils.LoadScripts();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}