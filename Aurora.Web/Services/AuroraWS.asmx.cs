﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Data.SqlClient;
using System.Globalization;
using Aurora.Custom.Providers.Shipping;
using Aurora.Custom.UI;
using System.Data.Common;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Net;
using System.Xml.Serialization;
using System.Web.Script.Serialization;


namespace Aurora.Services {
    /// <summary>
    /// Summary description for AuroraWebService
    /// </summary>
    [WebService(Namespace = "AuroraWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AuroraWebService : WebService {
        private AuroraEntities context = new AuroraEntities();

        #region News
        /// <summary>
        /// Gets all news items for the current site
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetNews() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                Utils.CheckSqlDBConnection();

                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[proc_News_GetNewsByDate]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    cmd.Parameters.AddWithValue("@CurrentDate", DateTime.Now);
                    DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(ds.GetXml());

                    Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);

                    return data;
                }
            } catch (Exception exception) {
                Utils.ServiceErrorResultHandler(exception, ref data);
            }

            return data;
        }

		/// <summary>
		/// Gets all news items for the current site
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetDetailNews() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				Utils.CheckSqlDBConnection();

				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[proc_News_GetDetailNewsByDate]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					cmd.Parameters.AddWithValue("@CurrentDate", DateTime.Now);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					XmlDocument xdoc = new XmlDocument();
					xdoc.LoadXml(ds.GetXml());

					Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);

					return data;
				}
			} catch (Exception exception) {
				Utils.ServiceErrorResultHandler(exception, ref data);
			}

			return data;
		}

        /// <summary>
        /// Gets a single news item for the current item
        /// </summary>
        /// <param name="newsID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetNewsItem(int newsID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                data.Add("Data", context.News.Where(n => n.ID == newsID));
                data.Add("Return", true);
                data.Add("ErrorMessage", string.Empty);
            } catch (Exception exception) {
                data.Add("Data", string.Empty);
                data.Add("Return", false);
                data.Add("ErrorMessage", exception.Message);
            }

            return data;
        }

        /// <summary>
        /// Gets all news articles within a specified category
        /// </summary>
        /// <param name="catFilter">category name</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetNewsWithFilter(string catFilter) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Utils.CheckSession();
                Utils.CheckSqlDBConnection();

                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[dbo].[proc_News_GetNewsByDateByCatName]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    cmd.Parameters.AddWithValue("@CurrentDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@CatName", catFilter);
                    DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(ds.GetXml());

                    Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), 0);
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

		/// <summary>
		/// Gets all news articles within a specified category
		/// </summary>
		/// <param name="catFilter">category name</param>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetDetailNewsWithFilter(string catFilter) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSession();
				Utils.CheckSqlDBConnection();

				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "[dbo].[proc_News_GetDetailNewsByDateByCatName]";
					cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					cmd.Parameters.AddWithValue("@CurrentDate", DateTime.Now);
					cmd.Parameters.AddWithValue("@CatName", catFilter);
					DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

					XmlDocument xdoc = new XmlDocument();
					xdoc.LoadXml(ds.GetXml());

					Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), 0);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
        #endregion

        #region Modules

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetClientModules() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var module = from clientModules in context.ClientModule
                             join featureModules in context.Module on clientModules.FeaturesModuleID equals
                                 featureModules.ID
                             where clientModules.ClientSiteID == SessionManager.ClientSiteID
                             && clientModules.DeletedOn == null
                             orderby featureModules.Name
                             select new { clientModules, featureModules };
				var moduleList = new List<dynamic>(module);

				// Begin the creation of the pseudomodule, Currency
				Currency defaultCurrency = (from currcon in context.CurrencyConversions
											join curr in context.Currency on currcon.CurrencyID equals curr.CurrencyID
											where currcon.ClientSiteID == SessionManager.ClientSiteID
											&& currcon.IsDefault == true
											&& currcon.DeletedOn == null
											select curr).FirstOrDefault();
				//If no default currency is found, default to ZAR
				if (defaultCurrency == null) {
					defaultCurrency = context.Currency.Where(w => w.CurrencyCode == "ZAR").FirstOrDefault();
				}
				//Generate custom ConfigXML
				XElement configXML = new XElement("XmlData",
					new XElement("Fields",
						new XElement("field",
							new XAttribute("fieldname", "Currency_Code"),
							new XAttribute("value", defaultCurrency.CurrencyCode)),
						new XElement("field",
							new XAttribute("fieldname", "Currency_Name"),
							new XAttribute("value", defaultCurrency.CurrencyName)),
						new XElement("field",
							new XAttribute("fieldname", "Currency_Symbol"),
							new XAttribute("value", defaultCurrency.CurrencySymbol)),
						new XElement("field",
							new XAttribute("fieldname", "Currency_ID"),
							new XAttribute("value", defaultCurrency.CurrencyID)),
						new XElement("field",
							new XAttribute("fieldname", "Currency_Possessive"),
							new XAttribute("value", defaultCurrency.CurrencyPossessive))
							)
						);
				
				Module currency = new Module { 
					Name = "Currency",
					ConfigXML = configXML.ToString()};

				moduleList.Add(new {
					clientModules = new ClientModule(),
					featureModules = currency
				});
				

                data.Add("Data", moduleList);
                data.Add("Return", true);
            } catch (Exception exception) {
                Utils.ServiceErrorResultHandler(exception, ref data);
                //data.Add("Data", string.Empty);
                //data.Add("Return", false);
                //data.Add("ErrorMessage", exception.Message);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetCustomModuleHTML(long customModuleID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //get schema and data xml
                var customModules = (from custModules in context.CustomModules
                                     where custModules.ClientSiteID == SessionManager.ClientSiteID
                                     && custModules.DeletedOn == null
                                     && custModules.ID == customModuleID
                                     select custModules).FirstOrDefault();

                //create a variable for message
                string returnHTML = "";

				if(customModules != null) {
					data.Add("Name", customModules.Name);

                //create in-memory object for xml panel
                XmlDocument moduleSchema = new XmlDocument();
                XmlDocument moduleData = new XmlDocument();

                //Load XML
					moduleSchema.LoadXml(customModules.XMLSchema);

                ObjectPanel xmlContainer = new ObjectPanel();
                XmlPanel xmlRenderContainer = new XmlPanel();

                xmlContainer.ID = "ContentPanel";
                xmlRenderContainer.ID = "ContentXml";

                xmlContainer.Controls.Add(xmlRenderContainer);
                xmlRenderContainer.XmlPanelSchema = moduleSchema;
                xmlRenderContainer.XmlPanelData = moduleData;

                xmlRenderContainer.XmlPanelSchema = moduleSchema;
                xmlRenderContainer.XmlPanelData = moduleData;
                System.IO.StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                xmlRenderContainer.RenderXmlControls("tbl_Form");
                xmlRenderContainer.RenderControl(htw);

					if(!string.IsNullOrEmpty(customModules.EmailCustomMessage)) {
						returnHTML = htw.InnerWriter.ToString() + "<div style=\"display:none;\"><div id=\"CustomFormMessage\" customform=\"" + customModules.ID + "\"><p>" + customModules.EmailCustomMessage + "</p></div></div>";
                } else {
                    returnHTML = htw.InnerWriter.ToString();
                }
				}

                Utils.ServiceSuccessResultHandler(ref data, returnHTML, 1);

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// Saves custom form data and sends out comms 
        /// </summary>
        /// <param name="formData">XML Form data</param>
        /// <param name="customModuleID">Form ID to Save against</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> SaveCustomModuleData(string formData, long customModuleID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            string subjectStr;
            string[] recipientArr = new string[0];

            try {
                var customForm = (from customForms in context.CustomModules
                                  where customForms.ID == customModuleID
                                  select customForms).SingleOrDefault();
                if (customForm != null) {

                    CustomModuleData custdata = new CustomModuleData {
                        ClientSiteID = SessionManager.ClientSiteID.Value,
                        Data = formData,
                        DataSchemaXML = customForm.XMLSchema,
                        InsertedOn = System.DateTime.Now,
                        ModuleID = customModuleID,
                        DeletedBy = null,
                        DeletedOn = null
                    };

                    context.AddObject("CustomModuleData", custdata);
                    context.SaveChanges();

					string customFormed = "";

					if (customForm.IncludeFormContent == true) {
						XDocument formXML = XDocument.Parse(formData);
						XDocument schema = XDocument.Parse(customForm.XMLSchema);
						//formXML.LoadXml(formData);
						StringBuilder formString = new StringBuilder();
						foreach (var x in schema.Descendants("field")) {
							string label = x.Descendants("label").FirstOrDefault().Value;
							var valueNode = formXML.Descendants("field").Where(w => w.Attribute("fieldname").Value == x.Attribute("fieldname").Value).FirstOrDefault();
                            if (valueNode == null) {
                                
                                formString.AppendLine(label + " : " + "<br/>");
                            } else {
 
							string value = valueNode.Attribute("value").Value;
							formString.AppendLine(label + " : " + value + "<br/>");
						}
                           
						}
						customFormed = formString.ToString();
					}
					
                    #region Send an email to the site owner
                    //send communication
                    if (!string.IsNullOrEmpty(customForm.EmailRecipients))
                        recipientArr = customForm.EmailRecipients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    //check if the user has supplied a custom list of recipients
                    if (recipientArr.Length == 0) {
                        //if not get the owners email from the user account
                        var userData = new List<dynamic>();
						var recipient = new {
							Email = CacheManager.SiteData.Email1,
							CompanyName = CacheManager.SiteData.CompanyName
						};
						userData.Add(recipient);
						
                        //send comms to site owner
                        Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysCustomFormSave.GetEnumDescriptionAttribute(), customFormed, null, customForm.Name + " Submission", Utils.GenerateCommunicationXML(userData));
                        //send comms to form applicant

                    } else {
						
                        var recipients = from arr in recipientArr
										 select new {
											 Email = arr,
											 CompanyName = CacheManager.SiteData.CompanyName
										 };
						var userData = new List<dynamic>();
						userData.AddRange(recipients);

                        //send the message to the message centre
						Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysCustomFormSave.GetEnumDescriptionAttribute(), customFormed, null, customForm.Name + " Submission", Utils.GenerateCommunicationXML(userData));
                    }

                    XmlDocument formXml = new XmlDocument();
                    formXml.LoadXml(formData);
                    #endregion

                    #region Send an email to the form applicant if an email exists within the form

                    subjectStr = !string.IsNullOrEmpty(customForm.EmailSubject) ? customForm.EmailSubject : "Form Submission From: " + CacheManager.SiteData.CompanyName;

                    var userEmail = (from xml in formXml.GetElementsByTagName("field").Cast<XmlNode>().ToList()
                                     where xml.Attributes["fieldname"].Value.ToUpper().Contains("EMAIL")
                                     select xml).ToList();

                    if (userEmail.Count > 0) {
                        string commEngineXML = "<xmldata><users><user email=\"" + Utils.EncodeXML(userEmail.First().Attributes["value"].Value) + "\" /></users></xmldata>";
                        Utils.SendToMessageCentre(Utils.MessageCentreEvents.CustomFormSave.GetEnumDescriptionAttribute(), null, null, subjectStr, commEngineXML.ToLower(), CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
                    }
                    #endregion

                    Utils.ServiceSuccessResultHandler(ref data, custdata, 1);

                } else {
                    Utils.ServiceSuccessResultHandler(ref data, "Error: No custom form found", 0);
                }


            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }



        #endregion

        #region Pages

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetBulkContent(int[] pageIds) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var pages = from pagecontent in context.PageContent
                            where pageIds.Contains(pagecontent.ID) && pagecontent.DeletedOn == null
                            select pagecontent;

                Utils.ServiceSuccessResultHandler(ref data, pages.ToList(), pages.Count());
            } catch (Exception ex) {

                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetPageContent(long? pageId, bool isDefault) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                bool authenticated = true;
                var pageData =
                         isDefault
                             ? context.PageContent.Where(
								 p => p.ClientSiteID == SessionManager.ClientSiteID && p.isDefault && p.isArchived == false && p.DeletedOn == null).ToList()
                             : context.PageContent.Where(page => page.ID == pageId && page.isArchived == false && page.DeletedOn == null).ToList();

				if (pageData.Count() > 0) {
					IEnumerable<RoleModule> secureRoles = Utils.CheckItemSecurity("PageDetail", pageData.First().ID);

					if (secureRoles.Count() > 0) {
						authenticated = false;
						if (SessionManager.MemberData != null) {
							var roleMatches = (from memRoles in context.GetActiveMemberCategories(SessionManager.MemberData.ID, SessionManager.ClientSiteID)
											   join s in secureRoles on memRoles equals s.RoleID
											   select memRoles);
							if (roleMatches.Count() > 0)
								authenticated = true;
						}
					}
				}

                if (!authenticated) {
                    Utils.ServiceSuccessResultHandler(ref data, "RoleSecurity", pageData.Count());
                } else if (pageData.Count() > 0 && pageData.First().IsSecured) {
                    Utils.ServiceSuccessResultHandler(ref data, "PageSecurity", pageData.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, pageData, pageData.Count());
                }

            } catch (Exception exception) {
                Utils.ServiceErrorResultHandler(exception, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetPages(int? pageId) {
            SessionManager.ClientSiteID = SessionManager.ClientSiteID;
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                if (pageId == 0) {

                    //GetChldPageList
                    Utils.CheckSqlDBConnection();
                    using (SqlCommand cmd = new SqlCommand()) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "[Features].[proc_PageContent_GetPagesWithChildStatus]";
                        cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                        DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                        var pageList = from tb in ds.Tables[0].AsEnumerable()
                                       select new {
                                           ID = tb.Field<int?>("ID"),
                                           Category = tb.Field<long?>("CategoryID"),
                                           ParentId = tb.Field<long?>("ParentID"),
                                           ClientSiteID = tb.Field<long?>("ClientSiteID"),
                                           ShortTitle = tb.Field<string>("ShortTitle"),
                                           LongTitle = tb.Field<string>("LongTitle"),
                                           Description = tb.Field<string>("Description"),
                                           PageContent = tb.Field<string>("PageContent"),
                                           PageIndex = tb.Field<int>("PageIndex"),
                                           isArchived = tb.Field<bool?>("isArchived"),
                                           isDefault = tb.Field<bool?>("isDefault"),
                                           MetaKeywords = tb.Field<string>("MetaKeywords"),
                                           MetaDescription = tb.Field<string>("MetaDescription"),
                                           CustomXML = tb.Field<string>("CustomXML"),
                                           InsertedOn = tb.Field<DateTime?>("InsertedOn"),
                                           InsertedBy = tb.Field<long?>("InsertedBy"),
                                           DeletedOn = tb.Field<DateTime?>("DeletedOn"),
                                           DeletedBy = tb.Field<long?>("DeletedBy"),
                                           HasChildren = tb.Field<int?>("HasChildren")

                                       };


                        Utils.ServiceSuccessResultHandler(ref data, pageList, pageList.Count());
                    }

                } else {
                    //Single page
                    var page = (from pages in context.PageContent
                                where pages.ID == pageId && pages.DeletedOn == null
                                select pages).First();

                    if (page.IsSecured) {
                        Utils.ServiceSuccessResultHandler(ref data, "Login", 1);
                    } else {
                        Utils.ServiceSuccessResultHandler(ref data, page, 1);
                    }

                }


            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetLinkedModuleOnPage(int moduleID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                if (moduleID > 0) {
                    var moduleData = from module in context.Module
                                     join clientModule in context.ClientModule on module.ID equals clientModule.FeaturesModuleID
                                     where module.ID == moduleID && clientModule.ClientSiteID == SessionManager.ClientSiteID
                                     select new { clientModule, module };

                    data.Add("Data", moduleData.ToList());
                    data.Add("Result", true);
                    data.Add("ErrorMessage", string.Empty);
                }
            } catch (Exception ex) {
                data.Add("Data", string.Empty);
                data.Add("Result", false);
                data.Add("ErrorMessage", ex.Message);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetPagesByName(int[] pageNames, bool HiddenOnly) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                var pages = HiddenOnly == false ?
                            from pageContent in context.PageContent
                            where pageNames.Contains(pageContent.ID)
                            && pageContent.ClientSiteID == SessionManager.ClientSiteID
                            select pageContent
                            :
                            from pageContent in context.PageContent
                            where pageNames.Contains(pageContent.ID)
                            && pageContent.PageIndex == -1
                            && pageContent.ClientSiteID == SessionManager.ClientSiteID
                            select pageContent
                            ;

                Utils.ServiceSuccessResultHandler(ref data, pages, pages.Count());
            } catch (Exception exception) {
                Utils.ServiceErrorResultHandler(exception, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> PageLogin(string username, string password, int pageID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                var pageLogin = from pages in context.PageContent
                                where pages.UserName == username
                                      && pages.Password == password
                                      && pages.ID == pageID
                                select pages;

                if (pageLogin.Count() > 0) {
                    Utils.ServiceSuccessResultHandler(ref data, pageLogin, pageLogin.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, "Failed Login", 0);
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        #endregion

        #region Events

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetEvent(int eventId) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                var eventData = context.Event.Where(evt => evt.ID == eventId).Single();
                var firstDayOfWeek = DateTime.Now.FirstDateOfWeek();
                bool found = false;
                int counter = 0;

                //limiting while loop to a thousand limits the possibility of an infinite loop
                while (!found && counter <= 1000) {
                    //calculate if any of the events fall within this week
                    switch (eventData.Occurance) {
                        case "Weekly":
                            if (firstDayOfWeek.AddDays(counter + 1).DayOfWeek == eventData.StartDate.Value.DayOfWeek) {
                                eventData.StartDate = firstDayOfWeek.AddDays(counter + 1).AddHours(eventData.StartDate.Value.Hour).AddMinutes(eventData.StartDate.Value.Minute);
                                found = true;
                            }
                            break;
                        case "Monthly":
                            if (firstDayOfWeek.AddDays(counter + 1).Day == eventData.StartDate.Value.Day) {
                                eventData.StartDate = firstDayOfWeek.AddDays(counter + 1).AddHours(eventData.StartDate.Value.Hour).AddMinutes(eventData.StartDate.Value.Minute); ;
                                found = true;
                            }
                            break;
                    }
                    counter++;
                }

                if (Utils.CheckItemSecurity("Event", eventData.ID).Count() > 0) {
                    Utils.ServiceSuccessResultHandler(ref data, "Secure", 1);
                } else {

                    Utils.ServiceSuccessResultHandler(ref data, Utils.getAnonEvent(eventData), 1);
                }

            } catch (Exception exception) {
                Utils.ServiceErrorResultHandler(exception, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetEvents(int? limit, bool? paginate, int? iPageNum, int? iPageSize, bool isCurrentWeek, string iFilter) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                long categoryId = 0;

                if (!string.IsNullOrEmpty(iFilter)) {
                    var getModuleId = from modules in context.Module
                                      where modules.Name == "Events"
                                      select modules;
                    if (getModuleId.Count() > 0) {
                        long moduleId = getModuleId.First().ID;
                        var getCatID = from category in context.Category
                                       where category.Name == iFilter
                                             && category.FeatureModuleID == moduleId
                                             && category.ClientSiteID == SessionManager.ClientSiteID
                                       select category;

                        if (getCatID.Count() > 0) {
                            categoryId = getCatID.First().ID;
                        }

                    }

                }

                int currentWeekNum = DateTime.Now.GetWeekNumber();
				//starts previous day 11pm
				DateTime today = DateTime.Now;
				//today = today.AddHours(-(today.Hour)).AddMinutes(-today.Minute).AddSeconds(-today.Second).AddMilliseconds(-today.Millisecond);

				DateTime midnight = DateTime.Now.AddHours(-today.Hour).AddHours(23);

                var events = categoryId == 0
                                 ? context.Event.Where(evt => evt.DisplayDate <=midnight && evt.EndDate >= midnight).Join(context.Category, evt => evt.CategoryID, category => category.ID,
                                                      (evt, category) => new { evt, category }).Where(
                                                          @t =>
                                                          @t.category.ClientSiteID == SessionManager.ClientSiteID &&
                                                          @t.evt.EndDate >= DateTime.Now
                                                          && @t.evt.DeletedOn == null
                                       ).Select(@t => @t.evt)
								 : context.Event.Where(evt => evt.DisplayDate <= midnight && evt.EndDate >= midnight).Join(context.Category, evt => evt.CategoryID, category => category.ID,
                                                      (evt, category) => new { evt, category }).Where(
                                                          @t =>
                                                          @t.category.ClientSiteID == SessionManager.ClientSiteID &&
                                                          @t.evt.EndDate >= DateTime.Now
                                                           && @t.evt.DeletedOn == null
                                                          && @t.evt.CategoryID == categoryId

                                       ).Select(@t => @t.evt);


                if (Utils.ConvertDBNull(limit, 0) != 0)
                {
                    events = events.Take(limit.Value).OrderBy(o => o.StartDate);
                }
                else
                {
                    events = events.OrderBy(o => o.StartDate);
                
                }

                if (Utils.ConvertDBNull(paginate.Value, false) && !isCurrentWeek) {
                    SessionManager.iPageCount = iPageSize.Value;

                    Utils.ServiceSuccessResultHandler(ref data, Utils.getAnonEntityList(events).Paginate(Utils.ConvertDBNull(iPageNum, 0), Utils.ConvertDBNull(iPageSize, 0)), events.Count());
                    return data;
                }

                //this only works for weekly & monthly events
                if (isCurrentWeek) {
                    List<Event> thisWeeksEvents = new List<Event>();
                    foreach (Event eventItem in events) {
                        if (eventItem.EndDate > DateTime.Now) {
                            var neweventItem = eventItem;
                            var firstDayOfWeek = DateTime.Now.FirstDateOfWeek();
                            int counter = 0;

                            while (counter < 6) {
                                //calculate if any of the events fall within this week
                                switch (eventItem.Occurance) {
                                    case "Weekly":
                                        if (firstDayOfWeek.AddDays(counter + 1).DayOfWeek == neweventItem.StartDate.Value.DayOfWeek) {
                                            neweventItem.StartDate = firstDayOfWeek.AddDays(counter + 1);
                                            thisWeeksEvents.Add(neweventItem);
                                        }
                                        break;
                                    case "Monthly":
                                        if (firstDayOfWeek.AddDays(counter + 1).Day == neweventItem.StartDate.Value.Day) {
                                            neweventItem.StartDate = firstDayOfWeek.AddDays(counter + 1);
                                            thisWeeksEvents.Add(neweventItem);
                                        }
                                        break;
                                }
                                counter++;
                            }

                        }

                    }
                    Utils.ServiceSuccessResultHandler(ref data, Utils.getAnonEntityList(thisWeeksEvents), thisWeeksEvents.Count());
                } else {

                    Utils.ServiceSuccessResultHandler(ref data,Utils.getAnonEntityList(events.OrderBy(evt => evt.StartDate.Value.Year).ThenBy(evt => evt.StartDate.Value.Month).ThenBy(evt => evt.StartDate.Value.Day)), events.Count());
                }

                return data;
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetCalendarEvents(int? limit, bool? paginate, int? iPageNum, int? iPageSize, bool isCurrentWeek, string iFilter, DateTime maxDate) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                long categoryId = 0;

                if (!string.IsNullOrEmpty(iFilter)) {
                    var getModuleId = from modules in context.Module
                                      where modules.Name == "Events"
                                      select modules;
                    if (getModuleId.Count() > 0) {
                        long moduleId = getModuleId.First().ID;
                        var getCatID = from category in context.Category
                                       where category.Name == iFilter
                                             && category.FeatureModuleID == moduleId
                                             && category.ClientSiteID == SessionManager.ClientSiteID
                                       select category;

                        if (getCatID.Count() > 0) {
                            categoryId = getCatID.First().ID;
                        }

                    }

                }

                int currentWeekNum = DateTime.Now.GetWeekNumber();

                var events = categoryId == 0
                                 ? context.Event.Join(context.Category, evt => evt.CategoryID, category => category.ID,
                                                      (evt, category) => new { evt, category }).Where(
                                                          @t =>
                                                          @t.category.ClientSiteID == SessionManager.ClientSiteID &&
                                                          (@t.evt.EndDate >= DateTime.Now && @t.evt.EndDate <= maxDate)
                                                          && @t.evt.DeletedOn == null
                                       ).Select(@t => @t.evt)
                                 : context.Event.Join(context.Category, evt => evt.CategoryID, category => category.ID,
                                                      (evt, category) => new { evt, category }).Where(
                                                          @t =>
                                                          @t.category.ClientSiteID == SessionManager.ClientSiteID &&
                                                          (@t.evt.EndDate >= DateTime.Now && @t.evt.EndDate <= maxDate)
                                                           && @t.evt.DeletedOn == null
                                                          && @t.evt.CategoryID == categoryId

                                       ).Select(@t => @t.evt);


                if (Utils.ConvertDBNull(limit, 0) != 0) {
                    events = events.Take(limit.Value);
                }


                if (Utils.ConvertDBNull(paginate.Value, false)) {
                    SessionManager.iPageCount = events.Count();
                    events = events.Paginate(Utils.ConvertDBNull(iPageNum, 0), Utils.ConvertDBNull(iPageSize, 0));
                }

                if (isCurrentWeek) {
                    var eventData = from evt in events.ToList()
                                    where (evt.StartDate.Value.GetWeekNumber() == currentWeekNum ||
                                           evt.EndDate.Value.GetWeekNumber() >= currentWeekNum)
                                    select evt;

                    Utils.ServiceSuccessResultHandler(ref data, eventData, eventData.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, events.ToList().OrderBy(evt => evt.StartDate.Value.Month).ThenBy(evt => evt.StartDate.Value.Day), events.Count());
                }

                return data;
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        #endregion

        #region Gallery
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetGalleries(int categoryId) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                var result = (from gallery in context.Gallery
                              where gallery.CategoryID == categoryId
                              && gallery.DeletedOn == null
                              select gallery).DefaultIfEmpty().ToList().OrderBy(galleryItems => galleryItems.GalleryIndex);


                var cleanedList = new List<Custom.Data.Gallery>();
                foreach (Custom.Data.Gallery gallery in result) {
                    if (File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/gallery_01_{1}.jpg", SessionManager.ClientSiteID, gallery.ID)))) {
                        cleanedList.Add(gallery);
                    }
                }
                data.Add("Data", cleanedList);
                data.Add("Result", true);

            } catch (Exception exception) {
                data.Add("Data", string.Empty);
                data.Add("Result", false);
                data.Add("ErrorMessage", exception.Message);
            }

            return data;
        }

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetGalleriesForCategoriesString(string[] galleryInclude, string[] galleryExclude) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var includeLength = galleryInclude.Where(w => w.Length > 0).Count();
				var result = (from gallery in context.Gallery
							  join category in context.Category on gallery.CategoryID equals category.ID
							  where (galleryInclude.Contains(category.Name.ToLower()) || includeLength == 0) && !galleryExclude.Contains(category.Name.ToLower())
							  && gallery.DeletedOn == null
							  && category.DeletedOn == null
							  && category.ClientSiteID == SessionManager.ClientSiteID
							  orderby gallery.GalleryIndex
							  select gallery).ToList();


				var cleanedList = new List<Custom.Data.Gallery>();
				foreach (Custom.Data.Gallery gallery in result) {
					if (File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/gallery_01_{1}.jpg", SessionManager.ClientSiteID, gallery.ID)))) {
						cleanedList.Add(gallery);
					}
				}
				data.Add("Data", cleanedList);
				data.Add("Result", true);

			} catch (Exception exception) {
				data.Add("Data", string.Empty);
				data.Add("Result", false);
				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetGalleriesForCategories(int [] categories) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			List<object> galleriesList = new List<object>();

			try {

				int currentCategory;
				Dictionary<string, object> galleries;

					for(int i = 0; i < categories.Length; i++) {

						galleries = new Dictionary<string, object>();
						currentCategory = categories[i];

						var result = (from gallery in context.Gallery
									  join categoryJ in context.Category on gallery.CategoryID equals categoryJ.ID
									  where gallery.CategoryID == currentCategory
									  && gallery.DeletedOn == null
									  && categoryJ.DeletedOn == null
									&& categoryJ.ClientSiteID == SessionManager.ClientSiteID
									  orderby gallery.GalleryIndex
									  select gallery).ToList();

						var category = context.Category.Where(c => c.ID == currentCategory).FirstOrDefault();


						var cleanedList = new List<Custom.Data.Gallery>();

						foreach(Custom.Data.Gallery gallery in result) {
							if(File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/gallery_01_{1}.jpg", SessionManager.ClientSiteID, gallery.ID)))) {
								cleanedList.Add(gallery);
							}
						}

						if(category != null){

							galleries.Add("CategoryName", category.Name);
							galleries.Add("CategoryID", category.ID);
							galleries.Add("Gallery", cleanedList);
							galleriesList.Add(galleries);
						}
					}


					data.Add("Data", galleriesList);
				data.Add("Result", true);

			} catch(Exception exception) {
				data.Add("Data", string.Empty);
				data.Add("Result", false);
				data.Add("ErrorMessage", exception.Message);
			}

			return data;
		}

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetGalleryCat(string galleryType, string catName) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Utils.CheckSqlDBConnection();

                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[proc_Gallery_GetCategories]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    cmd.Parameters.AddWithValue("@galleryType", galleryType);
                    cmd.Parameters.AddWithValue("@CategoryName", catName);

                    DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                    foreach (DataRow dataRow in ds.Tables[0].Rows) {

                        if (!File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/gallery_01_{1}.jpg", SessionManager.ClientSiteID, dataRow["ImageID"])))) {
                            long catID = long.Parse(dataRow["ID"].ToString());
                            var result = (from gallery in context.Gallery
                                          where gallery.CategoryID == catID
                                          && gallery.DeletedOn == null
                                          select gallery).DefaultIfEmpty().ToList();

                            int imageID = 0;

                            foreach (Custom.Data.Gallery gallery in result) {
                                if (File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/gallery_01_{1}.jpg", SessionManager.ClientSiteID, gallery.ID)))) {
                                    imageID = gallery.ID;
                                    break;
                                }
                            }
                            dataRow["ImageID"] = imageID;
                        }

                    }
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(ds.GetXml());

                    data.Add("Data", Utils.XmlToJSON(xdoc).ToString());
                    data.Add("Result", true);

                    return data;
                }
            } catch (Exception exception) {
                data.Add("Data", string.Empty);
                data.Add("Result", false);
                data.Add("ErrorMessage", exception);
            }

            return data;
        }

        #endregion

        #region Feedback
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> SendFeedback(CustomerFeedback customerFeedback) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                customerFeedback.InsertedByIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                customerFeedback.ClientSiteID = SessionManager.ClientSiteID;
                context.AddToCustomerFeedback(customerFeedback);
                context.SaveChanges();

                var testimonyData = new List<dynamic>();
                testimonyData.Add(customerFeedback);
                //Testimony to site owner
                if (Utils.isValidEmail(CacheManager.SiteData.Email1)) {
                    Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysNewTestimonyAdded.GetEnumDescriptionAttribute(), string.Empty, null, "New Testimony",
                        "<xmldata><users><user email=\"" + CacheManager.SiteData.Email1 + "\" /></users></xmldata>");
                }

                Utils.ServiceSuccessResultHandler(ref data, string.Empty, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetFeedback(int iPageSize, int iPageNum, int? id, string iFilter) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //Check if request is for a single item or multiple items.
                var feedback = id == 0
                               ?
                               from comments in context.CustomerFeedback
                               where comments.isApproved == true
                               && comments.DeletedOn == null
                               && comments.ClientSiteID == SessionManager.ClientSiteID
                               && comments.Preview != null
                               orderby comments.ApprovedOn descending
                               select comments
                               :
                               from comments in context.CustomerFeedback
                               where comments.isApproved == true
                               && comments.DeletedOn == null
                               && comments.ClientSiteID == SessionManager.ClientSiteID
                               && comments.Preview != null
                               && comments.ID == id
                               select comments
                               ;


                if (iPageSize == 0) {
                    Utils.ServiceSuccessResultHandler(ref data, feedback.ToList(), feedback.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, feedback.Paginate(iPageNum, iPageSize), feedback.Count());
                }

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }
        #endregion

        #region Utility
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> Exists(string path) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {

                data["Data"] = string.Format(path, SessionManager.ClientSiteID);
                data["Result"] = File.Exists(Server.MapPath(string.Format(path, SessionManager.ClientSiteID))); ;

            } catch (Exception ex) {
                data.Add("Data", string.Empty);
                data.Add("Return", false);
                data.Add("ErrorMessage", ex.Message);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> SendMail(string fromAddress, string fromName, string message, string subject, bool sendToClient) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                string retVal = Custom.Utils.SendMail(fromAddress, fromName, message, subject, true, sendToClient);

                if (retVal == "Sent") {
                    data.Add("Result", true);
                    data.Add("Data", string.Empty);
                    data.Add("ErrorMessage", string.Empty);
                } else {
                    data.Add("Result", false);
                    data.Add("Data", string.Empty);
                    data.Add("ErrorMessage", retVal);
                }

            } catch (Exception ex) {
                data.Add("Result", true);
                data.Add("Data", string.Empty);
                data.Add("ErrorMessage", ex.Message);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> SendMail(string fromAddress, string fromName, string message, string subject, bool sendToClient, string customResponse) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                string retVal = Custom.Utils.SendMail(fromAddress, fromName, message, subject, true, sendToClient, customResponse);

                if (retVal == "Sent") {
                    data.Add("Result", true);
                    data.Add("Data", string.Empty);
                    data.Add("ErrorMessage", string.Empty);
                } else {
                    data.Add("Result", false);
                    data.Add("Data", string.Empty);
                    data.Add("ErrorMessage", retVal);
                }

            } catch (Exception ex) {
                data.Add("Result", true);
                data.Add("Data", string.Empty);
                data.Add("ErrorMessage", ex.Message);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public void KeepAlive() {
            Utils.CheckSession();

        }
        #endregion

        #region Products
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductsMenu(int iPageNum, int iPageSize, string iFilter, int productID, bool isFeatured) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            Utils.CheckSession();
            try {
                var productData = productID == 0
                                    ?
                                    (from products in context.Product
                                     where products.ClientSiteID == SessionManager.ClientSiteID
                                        && products.DeletedBy == null
                                        && products.DeletedOn == null
                                        && products.SizeID != null
                                     select products).DefaultIfEmpty()
                                    :
                                    (from products in context.Product
                                     where products.ClientSiteID == SessionManager.ClientSiteID
                                        && products.ID == productID
                                        && products.DeletedBy == null
                                        && products.DeletedOn == null
                                        && products.SizeID != null
                                     select products).DefaultIfEmpty();
                if (iFilter != string.Empty) {
                    int catID = int.Parse(iFilter);
                    productData = productData.Where(t => t.CategoryID == catID);
                }

                if (productData.Count() == 0)
                    Utils.ServiceSuccessResultHandler(ref data, productData.Paginate(iPageNum, iPageSize).ToList(), productData.Count());
                else
                    Utils.ServiceSuccessResultHandler(ref data, productData.ToList(), productData.Count());

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductCategories(int? CategoryID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var prodCategories = CategoryID == null
                                    ? from categories in context.Category
                                      where categories.ClientSiteID == SessionManager.ClientSiteID
                                      select categories
                                     :
                                     from categories in context.Category
                                     where categories.ClientSiteID == SessionManager.ClientSiteID
                                     && categories.ID == CategoryID
                                     select categories
                                     ;

                Utils.ServiceSuccessResultHandler(ref data, prodCategories, prodCategories.Count());

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;

        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetCategoryChildren(int categoryID) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            try {
                Utils.CheckSqlDBConnection();
                using (SqlCommand sqlCmd = new SqlCommand()) {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = "[dbo].[proc_GetProductCategoriesWithImage]";
                    sqlCmd.Parameters.AddWithValue("@ParentCatergoryID", categoryID);

                    DataSet ds = Utils.SqlDb.ExecuteDataSet(sqlCmd);

                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(ds.GetXml());

                    Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);
                }
            } catch (Exception ex) {

                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

		[WebMethod(EnableSession=true)]
		public Dictionary<string, object> GetCategoryChildrenForProduct(int productID) {
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				long categoryID = context.Product.Where(w => w.ID == productID).Select(s => s.CategoryID).FirstOrDefault();
				Utils.CheckSqlDBConnection();
				using (SqlCommand sqlCmd = new SqlCommand()) {
					sqlCmd.CommandType = CommandType.StoredProcedure;
					sqlCmd.CommandText = "[dbo].[proc_GetProductCategoriesWithImage]";
					sqlCmd.Parameters.AddWithValue("@ParentCatergoryID", categoryID);

					DataSet ds = Utils.SqlDb.ExecuteDataSet(sqlCmd);

					XmlDocument xdoc = new XmlDocument();
					xdoc.LoadXml(ds.GetXml());

					Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);
					data.Add("CategoryID", categoryID);
				}
			} catch (Exception ex) {

				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductsPerCategory(int CategoryID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Utils.CheckSqlDBConnection();
                using (SqlCommand sqlCmd = new SqlCommand()) {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = "[dbo].[proc_Product_GetProductsWithImage]";
                    sqlCmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                    sqlCmd.Parameters.AddWithValue("@CategoryID", CategoryID);

                    DataSet ds = Utils.SqlDb.ExecuteDataSet(sqlCmd);

                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(ds.GetXml());

                    Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);
                }


            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetProductsPerBrand(int BrandID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				Utils.CheckSqlDBConnection();
				using(SqlCommand sqlCmd = new SqlCommand()) {
					sqlCmd.CommandType = CommandType.StoredProcedure;
					sqlCmd.CommandText = "[dbo].[proc_Product_GetProductsByBrand]";
					sqlCmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
					sqlCmd.Parameters.AddWithValue("@BrandID", BrandID);

					DataSet ds = Utils.SqlDb.ExecuteDataSet(sqlCmd);

					XmlDocument xdoc = new XmlDocument();
					xdoc.LoadXml(ds.GetXml());

					Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xdoc), ds.Tables[0].Rows.Count);
				}


			} catch(Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductGallery(int productID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                string categoryName = productID.ToString();

                var category = (from categories in context.Category
                                where categories.Name == categoryName
                                && categories.DeletedBy == null
                                && categories.DeletedOn == null
                                select categories).FirstOrDefault();
                if (category != null) {
                    var gallery = (from galleries in context.Gallery
                                   where galleries.CategoryID == category.ID
                                   && galleries.DeletedBy == null
                                    && galleries.DeletedOn == null
                                   select galleries).DefaultIfEmpty();

                    Utils.ServiceSuccessResultHandler(ref data, gallery, gallery.Count());
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, "", 0);
                }

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductColours(int productID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var colours = from colour in context.Colour
                              join colourProduct in context.ColourSizeProductGallery on colour.ID
                                  equals colourProduct.ColourID
                              where colourProduct.ProductID == productID
                              select colour;

                Utils.ServiceSuccessResultHandler(ref data, colours, colours.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> PutOrderEnquiry(string[] products, object contactData) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Dictionary<string, object> personaldata = (Dictionary<string, object>)(contactData);

                Dictionary<string, string> clientData = new Dictionary<string, string>();
                //get client data
                foreach (KeyValuePair<string, object> keyValuePair in personaldata) {
                    clientData.Add(keyValuePair.Key, keyValuePair.Value.ToString());
                }

                //create an Order
                Orders newOrder = new Orders {
                    ClientContactNo = clientData["contact"],
                    ClientEmail = clientData["email"],
                    ClientLastName = clientData["lastName"],
                    ClientSiteID = (int)SessionManager.ClientSiteID,
                    ClientFirsName = clientData["firstName"],
					Enquiry = clientData["Enquiry"],
                    CompanyName = clientData["companyName"],
                    InsertedByIP = HttpContext.Current.Request["REMOTE_ADDR"],
                    InsertedOn = DateTime.Now

                };
				//if enquiry
                context.AddToOrders(newOrder);
                context.SaveChanges();

                //add products
                foreach (string product in products) {

                    if (!String.IsNullOrEmpty(product)) {
                        string[] details = product.Split('|');
                        OrderItems item = new OrderItems {
                            ProductID = int.Parse(details[0]),
                            ColourID = details[3].Length == 0 ? 0 : int.Parse(details[3]),
                            SizeID = int.Parse(details[4]),
                            Quantity = int.Parse(details[1]),
                            OrderID = newOrder.ID

                        };
                        context.AddToOrderItems(item);
                    }


                }
                context.SaveChanges();

                var enquiryData = new List<dynamic>();
                enquiryData.Add(newOrder);
                //send to site owner
                Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysProductEnquiryCreated.GetEnumDescriptionAttribute(),
                                          string.Empty,
                                          null, 
                                          "Product Enquiry",
                                          Utils.GenerateCommunicationXML(enquiryData).Replace("client", string.Empty).Replace(enquiryData[0].ClientEmail, CacheManager.SiteData.Email1));
                //send to visitor
                Utils.SendToMessageCentre(Utils.MessageCentreEvents.ProductEnquiryCreated.GetEnumDescriptionAttribute(),
                                   string.Empty, 
                                   null, 
                                   "Product Enquiry", 
                                   Utils.GenerateCommunicationXML(enquiryData).Replace("client", string.Empty), 
                                   CacheManager.SiteData.Email1, 
                                   SessionManager.ClientSiteID.Value);

                Utils.ServiceSuccessResultHandler(ref data, "", 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductSizes(int productID) {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var sizeResults = from products in context.Product
                                  join size in context.Size on products.SizeID equals size.ID
                                  where products.ClientSiteID == SessionManager.ClientSiteID
                                  && products.DeletedOn == null
                                  && products.DeletedBy == null
                                  && (products.ID == productID || products.ParentID == productID)
								  orderby products.OrderIndex
                                  select size;

                Utils.ServiceSuccessResultHandler(ref data, sizeResults.ToList(), sizeResults.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductBasket(string productCookie) {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //Removes the final comma if the cookie has an extra character.
                if (productCookie.Substring(productCookie.Length - 1) == ",") {
                    productCookie = productCookie.Remove(productCookie.Length - 1);
                }
                string[] productLines = productCookie.Split(',');

                List<BasketLine> basketItems = new List<BasketLine>();
                foreach (var productLine in productLines) {
                    string[] productValues = productLine.Split('|');
                    string id = productValues[0] == string.Empty ? null : productValues[0];
                    string colour = productValues[3] == string.Empty ? null : productValues[3];
                    string quantity = productValues[1] == string.Empty ? null : productValues[1];
                    string size = productValues[4] == string.Empty ? null : productValues[4];

                    basketItems.Add(new BasketLine {
                        ID = Convert.ToInt64(id),
                        Colour = Convert.ToInt32(colour),
                        Quantity = Convert.ToInt64(quantity),
                        Size = Convert.ToInt64(size)
                    });
                }

                var productDetails = (from products in basketItems
                                      join dbProd in context.Product on products.ID equals dbProd.ID
                                      join CS in context.ColourSizeProductGallery on products.ID equals CS.ProductID
                                          into CS_J
                                      from CS in
                                          CS_J.Where(
                                              csj =>
                                              (csj.ColourID == products.Colour && csj.SizeID == products.Size) ||
                                              csj.ColourID == null).DefaultIfEmpty()
                                      select new {
                                          dbProd,
                                          CS,
                                          products,
                                          colours =
                               CS_J.Count() > 0
                                   ? from colours in context.Colour
                                     where colours.ID == CS.ColourID
                                     select colours
                                   : null,
                                          sizeName =
                               (from sizes in context.Size
                                where sizes.ID == dbProd.SizeID
                                select sizes.Description).SingleOrDefault()
                                      });

                Utils.ServiceSuccessResultHandler(ref data, productDetails.ToList(), productDetails.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductImage(string ProductID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                var cat = from category in context.Category
                          join gallery in context.Gallery on category.ID equals gallery.CategoryID
                          where category.Name == ProductID
                          select gallery;

                Utils.ServiceSuccessResultHandler(ref data, cat.ToList(), cat.Count());
            } catch (Exception ex) {

                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetAssociatedProducts(int currentProductID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var currentAssociatedProducts = from assProd in context.AssociatedProduct
                                                where assProd.ProductID == currentProductID
                                                join products in context.Product on assProd.AssociatedProductID equals
                                                    products.ID into leftJoinProducts
                                                from products in leftJoinProducts.DefaultIfEmpty()
                                                join categories in context.Category on products.CategoryID equals
                                                    categories.ID
                                                select new { products, categories };

                List<string> productIDs = currentAssociatedProducts.Select(all => all.products.ID).Cast<string>().ToList();

                var groupedResults = (from imageCats in context.Category
                                      join gallery in context.Gallery on imageCats.ID equals gallery.CategoryID
                                      select new { imageCats, gallery }).Where(t => productIDs.Contains(t.imageCats.Name))
                    //.OrderBy(o => Guid.NewGuid()) Uncomment this to enable random gallery image selection.
                              .GroupBy(i => i.imageCats.ID);
                var images = groupedResults.Select(s => s.FirstOrDefault());

                // performs a left join between the list of product IDs and the Image Galleries to get null for products
                // with no images
                var imageResults = (from products in productIDs
                                    from productImages in images.Where(t => products == (t.imageCats.Name)).DefaultIfEmpty()
                                    select new { products, productImages });

                data.Add("Images", imageResults);

                Utils.ServiceSuccessResultHandler(ref data, currentAssociatedProducts.ToList(), currentAssociatedProducts.Count());
            } catch (Exception ex) {

                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> ValidateProductVoucher(string voucherCode) {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var voucher = context.VoucherItem.Where(v => v.Code == voucherCode).SingleOrDefault();

                if (voucher != null) {
                    if (voucher.Redeemed.Value) {
                        Utils.ServiceSuccessResultHandler(ref data, "Error: Sorry but that voucher code has already been redeemed.", 0);
                        return data;
                    }
                    //perform validation checks
                    //check if voucher is still valid
                    var voucherSetting = (from voucherSettings in context.Voucher
                                          where voucherSettings.ID == voucher.VoucherID
                                          select voucherSettings).SingleOrDefault();

                    XmlDocument settings = new XmlDocument();
                    settings.LoadXml(voucherSetting.CustomXML);

                    bool valid = false;
                    bool isOnline = false;
                    string reason = string.Empty;
                    string voucherTypeText = (from voucherTypes in context.Types
                                              where voucherTypes.ID == voucherSetting.VoucherTypeID
                                              select voucherTypes).SingleOrDefault().Algorithm;

                    StringBuilder voucherConditions = new StringBuilder();
                    //check if this voucher has been assigned to this user
                    if (SessionManager.VistorSecureSessionID != Guid.Empty && SessionManager.MemberData != null) {
                        if (voucher.MemberID != null && voucher.MemberID != SessionManager.MemberData.ID) {
                            Utils.ServiceSuccessResultHandler(ref data, "Error: This voucher has not been assigned to you. It is therefore not redeemable", 0);
                            return data;
                        } else {
                            voucherConditions.Append(String.Format("Congratulations {0} {1} your voucher is valid.<br/>", SessionManager.MemberData.FirstName, SessionManager.MemberData.LastName));
                        }
                    }



                    voucherConditions.Append(String.Format("<div>This voucher gives you a {0} of the total of your purchases</div><br/>",
                                             voucherTypeText.Replace("{X}", voucherSetting.VoucherTypeValue)));
                    voucherConditions.Append("<div>Here are the conditions for this voucher:</div><br/>");
                    voucherConditions.Append("<div><ul>");
                    foreach (XmlNode setting in settings.GetElementsByTagName("field")) {

                        switch (setting.Attributes["fieldname"].Value) {
                            case "Date Range":
                                string[] dates = setting.Attributes["value"].Value.Split('|');
                                DateTime from, to;
                                DateTime.TryParse(dates[0], out from);
                                DateTime.TryParse(dates[1], out to);
                                voucherConditions.Append(String.Format("<li><b>Valid from {0} To the {1}.</b></li>", from.ToString("dd-MMM-yyyy"), to.ToString("dd-MMM-yyyy")));
                                valid = DateTime.Now >= from && DateTime.Now <= to;
                                reason = "This voucher has execeeded it valid time period for redemption.";
                                break;
                            case "Purchase Over X":
                                valid = true;
                                voucherConditions.Append(String.Format("<li><b>This current purchase must exceed R {0} which is required for this voucher to be valid.</b></li>", setting.Attributes["value"].Value));
                                break;
                            case "Online":
                                isOnline = true;
                                valid = true;
                                voucherConditions.Append("<li><b>Voucher is redeemable online only.</b></li>");
                                break;
                            case "InStore":
                                if (isOnline) {
                                    valid = true;
                                }
                                break;


                        }


                    }
                    voucherConditions.Append("<ul></div>");
                    if (!valid) {
                        Utils.ServiceSuccessResultHandler(ref data, "Error: " + reason, 0);
                        return data;
                    } else if (valid && !isOnline) {
                        Utils.ServiceSuccessResultHandler(ref data, "Error: Voucher can only be redeem in a store", 0);
                        return data;
                    } else if (valid && isOnline) {

                        Utils.ServiceSuccessResultHandler(ref data, voucherConditions.ToString(), 1);
                        return data;
                    }

                } else {
                    Utils.ServiceSuccessResultHandler(ref data, "Error: Sorry but that voucher code does not exist within our system. Please ensure the voucher code is typed out exactly as it is displayed to you as it is case sensitive.", 0);
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;

        }
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> AddProductVoucherToTransaction(string voucherCode) {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            double basketTotal = Convert.ToDouble(SessionManager.CurrentTransactions[0].TotalAmount.Value);
            XDocument DataXML = XDocument.Parse(SessionManager.CurrentTransactions[0].DataXML);

            try {
                if (DataXML.Descendants("Fields").Attributes("vouchercode").FirstOrDefault() != null) {
                    Utils.ServiceSuccessResultHandler(ref data, "Error: This transaction already has a voucher attached to it.",
                                                      0);
                    return data;
                }
                // ReSharper disable HeuristicUnreachableCode
                if (!String.IsNullOrEmpty(voucherCode)) {
                    var voucher = (from vouchers in context.VoucherItem
                                   join voucherSettings in context.Voucher on vouchers.VoucherID equals voucherSettings.ID
                                   join voucherTypes in context.Types on voucherSettings.VoucherTypeID equals voucherTypes.ID
                                   where vouchers.Code == voucherCode
                                   select new { vouchers, voucherSettings, voucherTypes }).SingleOrDefault();

                    if (voucher != null) {
                        //first determine if voucher meets the settings criteria
                        XmlDocument settings = new XmlDocument();
                        settings.LoadXml(voucher.voucherSettings.CustomXML);

                        bool valid = false;
                        bool isOnline = false;
                        string reason = string.Empty;

                        foreach (XmlNode setting in settings.GetElementsByTagName("field")) {
                            switch (setting.Attributes["fieldname"].Value) {
                                case "Date Range":
                                    string[] dates = setting.Attributes["value"].Value.Split('|');
                                    DateTime from, to;
                                    DateTime.TryParse(dates[0], out from);
                                    DateTime.TryParse(dates[1], out to);

                                    valid = DateTime.Now >= from && DateTime.Now <= to;
                                    reason = "This voucher has execeeded it valid time period for redemption.";
                                    break;
                                case "Purchase Over X":
                                    valid = basketTotal > Convert.ToDouble((setting.Attributes["value"].Value));
                                    reason = String.Format("This current purchase does not exceed {0} which is required for this voucher to be valid", setting.Attributes["value"].Value);
                                    break;
                                case "Online":
                                    isOnline = true;
                                    valid = true;
                                    break;
                                case "InStore":
                                    if (isOnline) {
                                        valid = true;
                                    }
                                    break;


                            }


                        }

                        //check if voucher is valid
                        if (!valid) {
                            Utils.ServiceSuccessResultHandler(ref data, "Error: " + reason, 0);
                            return data;
                        } else if (valid && !isOnline) {
                            Utils.ServiceSuccessResultHandler(ref data, "Error: Voucher can only be redeem in a store", 0);
                            return data;

                        }

                        //perform voucher discount
                        double voucherAmount = 0;
                        switch (voucher.voucherTypes.Name) {
                            case "Percentage Off":
                                voucherAmount = (basketTotal * (double.Parse(voucher.voucherSettings.VoucherTypeValue) / 100));
                                basketTotal = basketTotal - voucherAmount;
                                break;
                            case "Amount Off":
                                voucherAmount = double.Parse(voucher.voucherSettings.VoucherTypeValue);
                                basketTotal = basketTotal - voucherAmount;
                                break;
                        }

                        DataXML.Descendants("Fields").First().SetAttributeValue("vouchercode", voucherCode);
                        DataXML.Descendants("Fields").First().SetAttributeValue("voucheramount", voucherAmount);
                    }

                    long trnsID = SessionManager.CurrentTransactions[0].ID;
                    var currentTrans = (from trns in context.TransactionLog
                                        where trns.ID == trnsID
                                        select trns).SingleOrDefault();

                    SessionManager.CurrentTransactions[0].TotalAmount = Convert.ToDecimal(basketTotal);
                    SessionManager.CurrentTransactions[0].DataXML = DataXML.ToString(SaveOptions.DisableFormatting);

                    currentTrans.TotalAmount = Convert.ToDecimal(basketTotal);
                    currentTrans.DataXML = DataXML.ToString(SaveOptions.DisableFormatting);
                    currentTrans.SetAllModified(context);
                    context.SaveChanges();


                }
                // ReSharper restore HeuristicUnreachableCode
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;

        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> ProcessBasketOrder(string productCookie, string voucherCode, long paymentSchemaID, int paymentTerm) {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            decimal basketTotal = 0;
			bool requiresShipping = false;
            Dictionary<string, string> productSettings = Utils.GetFieldsFromCustomXML("Products");
            try {
                if (SessionManager.VistorSecureSessionID == Guid.Empty || SessionManager.MemberData == null) {
                    Utils.ServiceSuccessResultHandler(ref data, "login", 0);
                    return data;
                }
				
                //transform the cookie into a product list
                if (productCookie.Substring(productCookie.Length - 1) == ",") {
                    productCookie = productCookie.Remove(productCookie.Length - 1);
                }
                string[] productLines = productCookie.Split(',');

                List<BasketLine> basketItems = new List<BasketLine>();
                foreach (var productLine in productLines) {
                    string[] productValues = productLine.Split('|');
                    string id = productValues[0] == string.Empty ? null : productValues[0];
                    string colour = productValues[3] == string.Empty ? null : productValues[3];
                    string quantity = productValues[1] == string.Empty ? null : productValues[1];
                    string size = productValues[4] == string.Empty ? null : productValues[4];

                    basketItems.Add(new BasketLine {
                        ID = Convert.ToInt64(id),
                        Colour = Convert.ToInt32(colour),
                        Quantity = Convert.ToInt64(quantity),
                        Size = Convert.ToInt64(size)
                    });
                }

                var productDetails = (from products in basketItems
                                      join dbProd in context.Product on products.ID equals dbProd.ID
                                      join CS in context.ColourSizeProductGallery on products.ID equals CS.ProductID into CS_J
                                      from CS in CS_J.Where(csj => (csj.ColourID == products.Colour && csj.SizeID == products.Size)).DefaultIfEmpty()
                                      select new {
                                          dbProd,
                                          CS,
                                          products,
                                          colours = CS_J.Count() > 0 ? from colours in context.Colour where colours.ID == CS.ColourID select colours : null
                                      });

				int numberOfFreeShippingItems = productDetails.Where(w => w.dbProd.isZeroShipping == true || (w.dbProd.isFixedShipping == true && w.dbProd.ShippingPriceInt == 0 && w.dbProd.ShippingPriceLoc == 0)).Count();
				
				if (numberOfFreeShippingItems != productDetails.Count()) {
					requiresShipping = true;
				}

                //create data XML for purchase
                XmlDocument purchaseXML = new XmlDocument();
                XmlElement root = purchaseXML.CreateElement("XmlData");
                XmlElement fields = purchaseXML.CreateElement("Fields");
                double voucherAmount = 0;
                purchaseXML.AppendChild(root);

                foreach (var productDetail in productDetails) {
					// round each item price before adding to the total because that is how to correctly calculate a total (this will help later if we ever want aurora to generate invoices that adhere to financial )
                    basketTotal = Math.Round(Convert.ToDecimal((productDetail.dbProd.Price.Value * productDetail.products.Quantity)),2) + basketTotal;
                    XmlElement product = purchaseXML.CreateElement("field");

                    //add entity properties to XML
                    product.SetAttribute("id", productDetail.dbProd.ID.ToString());
                    product.SetAttribute("colourid", productDetail.products.Colour.ToString());
                    product.SetAttribute("sizeid", productDetail.dbProd.SizeID.ToString());
                    product.SetAttribute("price", productDetail.dbProd.Price.ToString());
                    product.SetAttribute("quantity", productDetail.products.Quantity.ToString());

                    //append to Fields
                    fields.AppendChild(product);
                }

                //add final attributes to xml

                //check if tax is applicable
				int recurringInterest = 0;
                if (productSettings.Count > 0) {
					decimal VAT = 0;

					if(productSettings["Products_VAT"] != "0" || !String.IsNullOrEmpty(productSettings["Products_VAT"])) {
						VAT = decimal.Parse(productSettings["Products_VAT"]) / 100;
					}

					if (productSettings.Keys.Contains("Products_RecurringInterest") && paymentTerm > 0)
					{
						int.TryParse(productSettings["Products_RecurringInterest"], out recurringInterest);
						//we need to calculate the installment and use the installment to calcualte a new total (this is because rounding the installment will result in the calculated total not equating to the installment multiplied by the payment term)
						decimal totalIncludingFinance = basketTotal + ((basketTotal * Convert.ToDecimal((decimal)recurringInterest / 100)) * (paymentTerm / 30));
						//VAT must be calculated on the total and rounded before adding to the total
						decimal calculatedVAT = Math.Round((totalIncludingFinance * VAT),2);
						decimal installment = Math.Round((totalIncludingFinance + calculatedVAT) / (paymentTerm / 30), 2);
						//now calculate the total by multiplying the rounded decimal value for the installment by the payment term
						basketTotal = installment * (paymentTerm / 30);
						fields.SetAttribute("vat", calculatedVAT.ToString());

					}else if (productSettings["Products_VAT"] != "0" || !String.IsNullOrEmpty(productSettings["Products_VAT"])) {

                        fields.SetAttribute("vat", (Math.Round((basketTotal * VAT),2).ToString()));

                        basketTotal = basketTotal + Math.Round((basketTotal * VAT),2);

                    } else {
                        fields.SetAttribute("vat", "0");
                    }
                }


                fields.SetAttribute("total", basketTotal.ToString());
                root.AppendChild(fields);


                //add to transaction
                Utils.ServiceSuccessResultHandler(ref data, CreateTransacation(paymentSchemaID, "Product Purchase", basketTotal, SessionManager.MemberData.ID, purchaseXML.OuterXml, Duration: paymentTerm, IsRecurring: (paymentTerm > 0? true : false), RequiresShipping: requiresShipping), 1);

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetProductFamily(int productID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var family = from products in context.Product
                             join colourSizeProductGallery in context.ColourSizeProductGallery on products.ID equals colourSizeProductGallery.ProductID
                             into leftJoin
                             from colourSizeProductGallery in leftJoin.DefaultIfEmpty()
                             where products.ID == productID
                             select new { products, colourSizeProductGallery };

                var sizes = from products in context.Product
                            join colourSizeProductGallery in context.ColourSizeProductGallery on products.ID equals colourSizeProductGallery.ProductID
                            into leftJoin
                            from colourSizeProductGallery in leftJoin.DefaultIfEmpty()
                            where products.ParentID == productID
                            select new { products, colourSizeProductGallery };

                family = family.Concat(sizes);

                Utils.ServiceSuccessResultHandler(ref data, family.ToList(), family.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        #endregion

        #region Members

        /// <summary>
        /// Checks if the current user has a login session
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> MemberLoggedIn() {
            Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                bool returnValue = false;

                if (SessionManager.VistorSecureSessionID != Guid.Empty && SessionManager.MemberData != null && SessionManager.MemberData.ID > 0) {
                    returnValue = true;
					data.Add("MemberName", SessionManager.MemberData.FirstName);
                }

                Utils.ServiceSuccessResultHandler(ref data, returnValue, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetLicenses() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var licenses = from membersettings in context.MembershipSettings
                               where membersettings.ClientSiteID == SessionManager.ClientSiteID
                               select new {
                                   membersettings,
                                   paymentMethods = (from payment in context.PaymentSchema
                                                     join setting in context.MembershipSettings on payment.ID equals setting.PaymentSchemaID
                                                     group payment by payment.ID into groupResults
                                                     select groupResults

                                                         )
                               };

                Utils.ServiceSuccessResultHandler(ref data, licenses.ToList(), licenses.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

		
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> LoginMemberUser(string userName,string password) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			bool loginSuccess = false;

			try {


				if(Utils.loginMemberUser(userName, password)) {
					data.Add("loggedIn", true);
					data.Add("MemberName", SessionManager.MemberData.FirstName);
					loginSuccess = true;
				} else {
					data.Add("loggedIn", false);
				}

					Utils.ServiceSuccessResultHandler(ref data, loginSuccess, 1);

			}catch(Exception ex){
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}


		/// <summary>
		/// This method is used to obtain the content the user has access to in order to build a navigation menu
		/// </summary>
		/// <returns>returns the users name as well as an object containing hierachy for each member category the user belongs to.</returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMemberMenu() {
			Dictionary<string, object> data = new Dictionary<string, object>();

			try {
				List<object> membershipMenus = new List<object>();
				List<MembershipPages> tmpMembershipPages;

				if(SessionManager.MemberData != null) {
					//send down the members name
					data.Add("MemberName", SessionManager.MemberData.FirstName);
					//get the roles that have been assigned to the member, join on role in order to get role name
					var memberMenus = from memberRoles in context.MemberRole
									  join role in context.Role on memberRoles.RoleID equals role.ID
									  where memberRoles.MemberID == SessionManager.MemberData.ID
									  && memberRoles.IsActivated == true
									  && memberRoles.LapsedOn == null
									  && memberRoles.ClientSiteID == CacheManager.SiteData.ClientSiteID
									  && memberRoles.Replaced == null
									  && (memberRoles.ExpiresOn == null || memberRoles.ExpiresOn > DateTime.Now)
									  select new {
										  name = role.Name,
										  RoleID = role.ID
									  };
					//loop through each role and populate an object with the role name and the page hierarchy
					foreach(var memberRole in memberMenus) {
						//call the stored proc which will get the heirarchy for the role and website
						tmpMembershipPages = context.GetMembershipPages(memberRole.RoleID, CacheManager.SiteData.ClientSiteID).ToList();
						//if empty don't send it down
						if(tmpMembershipPages.Count > 0){

							membershipMenus.Add(new {
								name = memberRole.name,
								menu = tmpMembershipPages
							});
						}
					}
				} else {
					data.Add("MemberName", "N/A");
				}

				data.Add("MemberMenus", membershipMenus);

				Utils.ServiceSuccessResultHandler(ref data, new object { }, null);
			}catch(Exception ex){
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyData"></param>
        /// <param name="paymentSchemaID"></param>
        /// <param name="password"></param>
        /// <param name="settingsID"></param>
        /// <param name="licensecount"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> CreateClient(Company companyData, long paymentSchemaID, string password, long settingsID, int licensecount) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            //Create a transaction
            context.Connection.Open();
            int success = 0;
            using (DbTransaction transaction = context.Connection.BeginTransaction()) {
                try {
                    //create a company
                    companyData.ClientSiteID = SessionManager.ClientSiteID;
                    context.AddToCompany(companyData);
                    success = context.SaveChanges();
                    //get the settings for this license
                    var memberSettings = (from settings in context.MembershipSettings
                                          where settings.ID == settingsID
                                          select settings).SingleOrDefault();

                    //create a default user to login with
                    Member defaultUser = new Member {
                        ClientSiteID = SessionManager.ClientSiteID,
                        Email = companyData.Email1,
                        ExpiresOn = DateTime.Now.AddDays(memberSettings.Expiration),
                        InsertedOn = DateTime.Now,
                        InsertedBy = 0,
                        FirstName = companyData.Name,
                        isDefault = true,
                        isActive = false,
                        OrganisationID = companyData.ID,
                        Password = password,
                        WorkTel = companyData.Tel1

                    };

                    context.AddToMember(defaultUser);
                    success = context.SaveChanges();
                    //start a financial transaction this will only be approved once the user has logged in for the first time.
                    TransactionLog financeTransaction = new TransactionLog();
                    financeTransaction.ClientSiteID = SessionManager.ClientSiteID;
                    financeTransaction.PaymentSchemaID = paymentSchemaID;
                    financeTransaction.PaymentType = "Registration";
                    //calculate total
                    financeTransaction.TotalAmount = (decimal)(memberSettings.Price.Value * licensecount);

                    context.AddToTransactionLog(financeTransaction);

                    success = context.SaveChanges();
                    if (SessionManager.CurrentTransactions == null) {
                        SessionManager.CurrentTransactions = new List<TransactionLog>();
                    }
                    SessionManager.CurrentTransactions.Add(financeTransaction);

                    //Create a License record
                    License newLicense = new License {
                        CompanyID = companyData.ID,
                        MemberSettings = settingsID,
                        Total = licensecount,
                        InsertedBy = 0,
                        InsertedOn = DateTime.Now
                    }
                    ;

                    context.AddToLicense(newLicense);
                    success = context.SaveChanges();


                    if (success == 1) {
                        transaction.Commit();
                        SessionManager.MemberData = defaultUser;
                        string message = "";
                        message += "A New member request has been created for the following individual:<br/>";
                        message += "<br/>Company/Individual: " + companyData.Name;
                        message += "<br/>Tel 1: " + companyData.Tel1;
                        message += "<br/>Tel 2: " + companyData.Tel2;
                        message += "<br/>Email 1: " + companyData.Email1;
                        message += "<br/>Email 2: " + companyData.Email2;

                        SendMail(CacheManager.SiteData.Email1, CacheManager.SiteData.CompanyName, message,
                                 "New Member Request", false);
                    }
                    Utils.ServiceSuccessResultHandler(ref data, financeTransaction, 1);
                } catch (Exception ex) {
                    transaction.Rollback();
                    Utils.ServiceErrorResultHandler(ex, ref data);
                }
            }


            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyData"></param>
        /// <param name="password"></param>
        /// <param name="companyName"></param>
        /// <param name="additionalData"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> CreateClientNoLicense(Company companyData, string password, string companyName, string additionalData) {
            Dictionary<string, object> data = new Dictionary<string, object>();

            //Create a transaction
            context.Connection.Open();
            int success = 0;
            using (DbTransaction transaction = context.Connection.BeginTransaction()) {
                try {
                    //create/find a company
                    var comp = (from companies in context.Company
                                where companies.Name == companyName
                                      && companies.ClientSiteID == SessionManager.ClientSiteID
                                select companies).SingleOrDefault();

                    if (comp == null) {
                        comp = new Company {
                            ClientSiteID = SessionManager.ClientSiteID,
                            Name = companyName,
                            Email1 = CacheManager.SiteData.Email1,
                            Description = "",
                            InsertedBy = 0,
                            InsertedOn = DateTime.Now
                        };
                        context.AddToCompany(comp);
                        success = context.SaveChanges();
                    }


                    //create a default user to login with
                    Member defaultUser = new Member {
                        ClientSiteID = SessionManager.ClientSiteID,
                        Email = companyData.Email1,
                        ExpiresOn = null,
                        LastLogin = null,
                        LastUpdated = DateTime.Now,
                        DeletedOn = null,
                        InsertedOn = DateTime.Now,
                        InsertedBy = 0,
                        FirstName = companyData.Name,
                        isDefault = false,
                        isActive = true,
                        OrganisationID = comp.ID,
                        Password = password,
                        WorkTel = companyData.Tel1

                    };

                    context.AddToMember(defaultUser);

                    success = context.SaveChanges();

                    //add to role
                    var roleid = (from roles in context.Role
                                  where roles.Name == companyName
                                  select roles.ID).SingleOrDefault();

                    MemberRole newRoles = new MemberRole {
                        ClientSiteID = SessionManager.ClientSiteID,
                        MemberID = defaultUser.ID,
                        RoleID = roleid
                    };
                    context.AddToMemberRole(newRoles);
                    success = context.SaveChanges();

                    if (success == 1) {
                        transaction.Commit();
                        //Send Communication to site owner
                        SessionManager.MemberData = defaultUser;
                        var messageXmlData = new List<dynamic>();
                        messageXmlData.Add(SessionManager.MemberData);

                        Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysMemberNewRegistration.GetEnumDescriptionAttribute(), string.Empty, string.Empty, "New Member request", Utils.GenerateCommunicationXML(messageXmlData));

                        //send communication to visitor
                        var hasMessage = from messages in context.Message
                                         join messageEvent in context.MessageEvent on messages.MessageEventID equals
                                             messageEvent.ID
                                         where messages.ClientSiteID == SessionManager.ClientSiteID
                                               && messageEvent.EventType == "New Registration"
                                         select new { messages, messageEvent };
                        if (hasMessage.Count() > 0) {

                            Utils.SendToMessageCentre(Utils.MessageCentreEvents.MemberNewRegistration.GetEnumDescriptionAttribute(), string.Empty, null,
                                                CacheManager.SiteData.CompanyName + " Registration", Utils.GenerateCommunicationXML(messageXmlData),
                                                CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
                        }
                    }
                    Utils.ServiceSuccessResultHandler(ref data, "Login," + defaultUser.ID, 1);
                } catch (Exception ex) {
                    transaction.Rollback();
                    Utils.ServiceErrorResultHandler(ex, ref data);
                }
            }


            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberData"></param>
        /// <param name="companyName"></param>
        /// <param name="additionalData"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> UpdateMember(Member memberData, string companyName, string additionalData) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //save additional data;
                var customData = (from customDatas in context.CustomModuleData
                                  where customDatas.SystemModuleID == memberData.ID
                                  select customDatas).FirstOrDefault();
                if (customData != null) {
                    customData.Data = additionalData;
                    context.AttachTo("CustomModuleData", customData);
                    customData.SetAllModified(context);
                } else {
                    //get schema
                    //  var schema = 

                }
                context.AttachTo("Member", memberData);
                memberData.SetAllModified(context);
                context.SaveChanges();
            } catch (Exception) {

            }

            return data;
        }

        /// <summary>
        /// Creates a new member for a website.
        /// </summary>
        /// <param name="memberData">Entity object</param>
        /// <param name="companyID">Organisation this member will be placed under</param>
        /// <param name="roleID">Role the member will be placed under</param>
        /// <param name="additionalData">Data from custom Forms module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> CreateMember(Member memberData, long categoryID, string additionalData, long? paymentSchemaID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            context.Connection.Open();
            int success = 0;
            string messageXml = "";
            long memberCategoryID = -1;
            using (DbTransaction transaction = context.Connection.BeginTransaction()) {
                try {
                    //get module config
                    Dictionary<string, string> customFields = Utils.GetFieldsFromCustomXML("Members");
                    XmlDocument additionalXML = new XmlDocument();
                    XmlDocument customModuleDataXML = new XmlDocument();
                    
                    #region Preliminary Checks
                    //check if this member exists
                    var memberExists = (from members in context.Member
                                        where members.Email == memberData.Email
                                        && members.ClientSiteID == SessionManager.ClientSiteID
                                        && members.DeletedOn == null
                                        && members.DeletedBy == null
                                        select members).FirstOrDefault();

                    //get any custom module data that might be related
                    var customModuleData = (from customD in context.CustomModuleData
                                            where customD.SystemModuleID == memberData.ID
                                            select customD).FirstOrDefault();
                    //Prompt user that their email is already in the system
                    if (memberExists != null) {
                        memberData.Password = Security.Encryption.Encrypt(memberData.Password);
                        Utils.ServiceSuccessResultHandler(ref data, memberData, -1);
                        return data;
                    }
                    #endregion

                    #region Create a new member
					memberData.ClientSiteID = SessionManager.ClientSiteID;
                    memberData.isActive = true;
					memberData.isPending = false;
                    memberData.Password = Security.Encryption.Encrypt(memberData.Password);
                    context.AddObject("Member", memberData);
                    context.SaveChanges();

					if (!String.IsNullOrEmpty(SessionManager.TempFileName) && File.Exists(SessionManager.TempFileName)) {
						String memberpath = Server.MapPath("\\ClientData\\" + CacheManager.SiteData.ClientSiteID + "\\Uploads\\Members\\" + memberData.ID);
						Directory.CreateDirectory(memberpath);
						String filename = memberData.ID.ToString() + "-1" + SessionManager.TempFileName.Substring(SessionManager.TempFileName.LastIndexOf('.'));
						if (File.Exists(memberpath + "\\" + filename)) {
							File.Delete(memberpath + "\\" + filename);
						}
						File.Move(SessionManager.TempFileName, memberpath + "\\" + filename);
						memberData.PrimaryImageURL = "/ClientData/" + CacheManager.SiteData.ClientSiteID + "/Uploads/Members/" + memberData.ID + "/" + filename;
						SessionManager.TempFileName = "";
						context.SaveChanges();
					}
                    //assign to CACHE
                    SessionManager.MemberData = memberData;
                    #endregion

                    #region Create Category
                    var category = (from cat in context.Role
                                    where cat.ID == categoryID && cat.ClientSiteID == SessionManager.ClientSiteID
                                    select cat).FirstOrDefault();
                    int successCode = 0;
                    if (category == null) {
                        success = 0;
                    } else {
                        string status = "";
                        MemberRole memberRole = new MemberRole();
                        memberRole.ClientSiteID = SessionManager.ClientSiteID;
                        memberRole.MemberID = memberData.ID;
                        memberRole.RoleID = categoryID;
                        memberRole.InsertedOn = DateTime.Now;
						memberRole.IsRecurring = category.IsRecurring;

                        bool isPending = (category.DefaultToPending == true) || (category.Cost > 0);
                        bool isExpiring = (category.RoleDuration > 0);
                        bool isCosted = (category.Cost > 0);

                        if (isPending) {
                            memberRole.IsActivated = false;
                        } else {
                            memberRole.IsActivated = true;
                            memberRole.CommencedOn = memberRole.InsertedOn;
                        }
                        if (!isPending && isExpiring) {
                            memberRole.ExpiresOn = memberRole.InsertedOn.Value.AddDays((double)category.RoleDuration);
                        }
                        memberRole.CostAtPurchase = category.Cost;
                        context.AddToMemberRole(memberRole);
                        success = context.SaveChanges();
                        memberCategoryID = memberRole.ID;
                        if (success == 1) {
                            if (!isPending) {
                                successCode = 1; //Successful Activation, allow user to log in.
                            } else if (!isCosted) {
                                successCode = 2; //Successful capture, requires activation by site owner
                            } else if (isCosted) {
                                successCode = 3; //Successful capture, prompt user for payment options.

                                //Create a transaction log that will be picked up by the process payment page.
                                TransactionLog payment = new TransactionLog();
                                payment.ClientSiteID = SessionManager.ClientSiteID;
                                payment.MemberID = SessionManager.MemberData.ID;
                                payment.PaymentSchemaID = paymentSchemaID;
                                payment.PaymentType = "Registration";
                                payment.MemberCategoryID = memberRole.ID;
                                payment.InsertedOn = DateTime.Now;
                                payment.InsertedBy = SessionManager.ClientSiteID;
                                payment.TotalAmount = (decimal)memberRole.CostAtPurchase;
								payment.IsRecurring = memberRole.IsRecurring ?? false;
								payment.Duration = category.RoleDuration;

                                context.AddToTransactionLog(payment);

                                success = context.SaveChanges();
                                memberRole.TransactionID = payment.ID;
                                if (SessionManager.CurrentTransactions == null) {
                                    SessionManager.CurrentTransactions = new List<TransactionLog>();
                                }
                                SessionManager.CurrentTransactions.Add(payment);
                            }
                        }
                    }

                    #endregion

                    #region Additional Custom XML Saves for member creations
                    //update custom XML

                    if (additionalData != null) {
                        additionalXML.LoadXml(additionalData);
                    }

                    if (customModuleData != null && !String.IsNullOrEmpty(additionalData) && customModuleData.Data != null) {
                        customModuleDataXML.LoadXml(customModuleData.Data);
                        customModuleData.Data =
                            Utils.UpdateXml(ref additionalXML, ref customModuleDataXML, "field").OuterXml;

                    } else if (!String.IsNullOrEmpty(additionalData)) {
                        var moduleID = (from myModule in context.CustomModules
                                        where myModule.FeaturesModuleID == 175
										&& myModule.ClientSiteID == SessionManager.ClientSiteID
                                        select myModule
                                        ).FirstOrDefault();

                        if (moduleID != null) {

                            context.AddToCustomModuleData(new CustomModuleData {
                                ClientSiteID = SessionManager.ClientSiteID.Value,
                                Data = additionalData,
                                DataSchemaXML = moduleID.XMLSchema,
                                SystemModuleID = memberData.ID,
                                ModuleID = moduleID.ID
                            });

                            success = context.SaveChanges();
                        }
                    }
                    #endregion

                    #region Transaction Comit/Rollback
                    if (success >= 1) {

                        //Send Communication to site owner
                        SessionManager.MemberData = memberData;

                        messageXml = string.Empty;
                        messageXml += "<xmldata><users>";
                        messageXml +=
                            String.Format("<user email=\"{0}\" memfirstname=\"{1}\" memlastname=\"{2}\" mememail=\"{3}\"></user>",
                                            Utils.EncodeXML(CacheManager.SiteData.Email1), Utils.EncodeXML(SessionManager.MemberData.FirstName),
                                            Utils.EncodeXML(SessionManager.MemberData.LastName), Utils.EncodeXML(SessionManager.MemberData.Email));
                        messageXml += "</users></xmldata>";

                        //send communication to visitor
                        //var hasMessage = from messages in context.Message
                        //                 join messageEvent in context.MessageEvent on messages.MessageEventID equals
                        //                     messageEvent.ID
                        //                 where messages.ClientSiteID == SessionManager.ClientSiteID
                        //                       && messageEvent.EventType == "New Registration"
                        //                 select new { messages, messageEvent };

                        //if (hasMessage.Count() > 0) {
                        //    messageXml = string.Empty;
                        //    messageXml += "<xmldata><users>";
                        //    messageXml += String.Format("<user email=\"{0}\" firstname=\"{1}\" lastname=\"{2}\" mobile=\"{3}\"></user>", Utils.EncodeXML(SessionManager.MemberData.Email), Utils.EncodeXML(SessionManager.MemberData.FirstName), Utils.EncodeXML(SessionManager.MemberData.LastName), Utils.EncodeXML(Aurora.Security.Encryption.Decrypt(SessionManager.MemberData.Password)), Utils.EncodeXML(SessionManager.MemberData.Mobile ?? ""));
                        //    messageXml += "</users></xmldata>";
                        //    Utils.SendToMessageCentre(Utils.MessageCentreEvents.MemberNewRegistration.GetEnumDescriptionAttribute(), string.Empty, null,
                        //                        CacheManager.SiteData.CompanyName + " Registration", messageXml,
                        //                        CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
                        //}
                        
                        //Commit transaction
                        transaction.Commit();
                        try {
                            Utils.SendToMessageCentre(
                                        Utils.MessageCentreEvents.SysMemberNewRegistration.GetEnumDescriptionAttribute(),
                                        string.Empty, string.Empty, Utils.MessageCentreEvents.SysMemberNewRegistration.GetEnumDescriptionAttribute(), messageXml.Replace("email1", "email"));

                            if (memberCategoryID > -1 && successCode == 1) {
                                Utils.SendMemberCategoryMessageEmail(memberCategoryID, 0, false);
                            }
                            Utils.ServiceSuccessResultHandler(ref data, memberData, successCode);
                        } catch (Exception ex) {
                            Utils.ServiceErrorResultHandler(ex, ref data);
                        }
                        
                    } else {
                        //it was not successful, roll it back
                        transaction.Rollback();
                    }
                    #endregion
                } catch (Exception ex) {
                    //method failed roll back transactions
                    transaction.Rollback();
                    Utils.ServiceErrorResultHandler(ex, ref data);
                }


            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> UpdateMember(Member memberData, string additionalData) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            context.Connection.Open();
            int success = 0;
            string messageXml = "";
            using (DbTransaction transaction = context.Connection.BeginTransaction()) {
                try {
                    //get module config
                    Dictionary<string, string> customFields = Utils.GetFieldsFromCustomXML("Members");
                    XmlDocument additionalXML = new XmlDocument();
                    XmlDocument customModuleDataXML = new XmlDocument();

                    #region Preliminary Checks
                    //check if this member exists
                    var memberExists = (from members in context.Member
                                        where members.ID == SessionManager.MemberData.ID
                                        && members.ClientSiteID == SessionManager.ClientSiteID
                                        && members.DeletedOn == null
                                        && members.DeletedBy == null
                                        select members).FirstOrDefault();

                    var memberEmailExists = (from members in context.Member
                                             where members.Email == memberData.Email
                                                && members.ClientSiteID == SessionManager.ClientSiteID
                                                && members.DeletedOn == null
                                                && members.DeletedBy == null
                                                && members.ID != memberExists.ID
                                             select members).FirstOrDefault();

                    //get any custom module data that might be related
                    var customModuleData = (from customD in context.CustomModuleData
                                            where customD.SystemModuleID == memberData.ID
                                            select customD).FirstOrDefault();
                    //Prompt user that their email is already in the system
                    if (memberExists != null && memberEmailExists == null) {
                        memberExists.FirstName = memberData.FirstName;
                        memberExists.LastName = memberData.LastName;
                        memberExists.Email = memberData.Email;
                        memberExists.LastUpdated = DateTime.Now;
                        memberExists.CustomXML = memberData.CustomXML;

                        #region Additional Custom XML Saves for member creations
						/*
						 * It would appear this code was written to facilitate Baby2Mom's additional data. It is causing problems with the new
						 * membership system, so I am disabling it until it is needed. - Garth
						 * 

                        //update custom XML
                        if (additionalData != null) {
                            additionalXML.LoadXml(additionalData);
                        }

                        if (customModuleData != null && !String.IsNullOrEmpty(additionalData) && customModuleData.Data != null) {
                            customModuleDataXML.LoadXml(customModuleData.Data);
                            customModuleData.Data =
                                Utils.UpdateXml(ref additionalXML, ref customModuleDataXML, "field").OuterXml;

                        } else if (!String.IsNullOrEmpty(additionalData)) {
                            var moduleID = (from myModule in context.CustomModules
                                            where myModule.FeaturesModuleID == 175
                                            select myModule
                                            ).FirstOrDefault();

                            if (moduleID != null) {

                                context.AddToCustomModuleData(new CustomModuleData {
                                    ClientSiteID = SessionManager.ClientSiteID.Value,
                                    Data = additionalData,
                                    DataSchemaXML = moduleID.XMLSchema,
                                    SystemModuleID = memberData.ID,
                                    ModuleID = moduleID.ID
                                });

                                success = context.SaveChanges();
                            }
                        } */
                        #endregion

                        context.SaveChanges();
						if (!String.IsNullOrEmpty(SessionManager.TempFileName) && File.Exists(SessionManager.TempFileName)) {
							String memberpath = Server.MapPath("\\ClientData\\" + CacheManager.SiteData.ClientSiteID + "\\Uploads\\Members\\" + memberData.ID);
							Directory.CreateDirectory(memberpath);
							String filename = memberData.ID.ToString() + "-1" + SessionManager.TempFileName.Substring(SessionManager.TempFileName.LastIndexOf('.'));
							if (File.Exists(memberpath + "\\" + filename)) {
								File.Delete(memberpath + "\\" + filename);
							}
							File.Move(SessionManager.TempFileName, memberpath + "\\" + filename);
							memberExists.PrimaryImageURL = "/ClientData/" + CacheManager.SiteData.ClientSiteID + "/Uploads/Members/" + memberData.ID + "/" + filename;
							SessionManager.TempFileName = "";
							context.SaveChanges();
						}
                        SessionManager.MemberData = memberExists;
                        //Commit transaction
                        transaction.Commit();
                        Utils.ServiceSuccessResultHandler(ref data, memberData, 1);
                    } else {
                        transaction.Rollback();
                    }
                    #endregion

                } catch (Exception ex) {
                    //method failed roll back transactions
                    transaction.Rollback();
                    Utils.ServiceErrorResultHandler(ex, ref data);
                }
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetMemberCategories() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var memberCategories = from memcats in context.MemberRole.Where(w => w.Replaced == null && w.MemberID == SessionManager.MemberData.ID).AsEnumerable()
                                       join cats in context.Role on memcats.RoleID equals cats.ID
                                       from replcats in context.MemberRole.Where(w => w.Replacing == memcats.ID).DefaultIfEmpty()
                                       select new {
                                           MemberCategory = memcats,
                                           Category = cats,
                                           CanRenew = ((memcats.LapsedOn != null || (memcats.ExpiresOn != null && (memcats.ExpiresOn - DateTime.Now).Value.Days < cats.RenewalAllowedAt)) && cats.RenewalAllowedAt != -1),
                                           IsPendingReplace = (replcats != null)
                                       };

                Utils.ServiceSuccessResultHandler(ref data, memberCategories, memberCategories.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> RequestRenewal(long MemberCategoryID, long PaymentSchemaID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var memberCategory = (from memcats in context.MemberRole.Where(w => w.Replaced == null && w.MemberID == SessionManager.MemberData.ID && w.ID == MemberCategoryID).AsEnumerable()
                                      join cats in context.Role on memcats.RoleID equals cats.ID
                                      select new {
                                          MemberCategory = memcats,
                                          Category = cats,
                                          CanRenew = ((memcats.LapsedOn != null || (memcats.ExpiresOn != null && (memcats.ExpiresOn - DateTime.Now).Value.Days < cats.RenewalAllowedAt)) && cats.RenewalAllowedAt != -1)
                                      }).FirstOrDefault();
                if (memberCategory == null) {
                    Utils.ServiceSuccessResultHandler(ref data, "No Category Found", -1);
                } else if (!memberCategory.CanRenew) {
                    Utils.ServiceSuccessResultHandler(ref data, "You cannot renew this category now. How did you get here?", -1);
                } else if (memberCategory.Category.Cost > 0) {
                    //Costed purchase of category
                    TransactionLog tLog = new TransactionLog {
                        ClientSiteID = SessionManager.ClientSiteID,
                        MemberID = SessionManager.MemberData.ID,
                        InsertedOn = DateTime.Now,
                        InsertedBy = SessionManager.ClientSiteID,
                        PaymentSchemaID = PaymentSchemaID,
                        PaymentType = "Renewal",
                        MemberCategoryID = memberCategory.MemberCategory.ID,
                        TotalAmount = (decimal)memberCategory.Category.Cost,
						IsRecurring = memberCategory.MemberCategory.IsRecurring ?? false,
						Duration = memberCategory.Category.RoleDuration ?? 0
                    };
                    context.AddToTransactionLog(tLog);
                    context.SaveChanges();
                    if (SessionManager.CurrentTransactions == null) {
                        SessionManager.CurrentTransactions = new List<TransactionLog>();
                    }
                    SessionManager.CurrentTransactions.Add(tLog);
                    Utils.ServiceSuccessResultHandler(ref data, tLog, 1);
                    
                } else {
                    //Free category renewal
                    MemberRole renewedCategory = new MemberRole {
                        ClientSiteID = SessionManager.ClientSiteID,
                        MemberID = SessionManager.MemberData.ID,
                        RoleID = memberCategory.MemberCategory.RoleID,
                        IsActivated = memberCategory.MemberCategory.IsActivated,
                        InsertedOn = DateTime.Now,
                        CommencedOn = memberCategory.MemberCategory.CommencedOn,
                        ExpiresOn = memberCategory.MemberCategory.ExpiresOn.Value.AddDays((double)memberCategory.Category.RoleDuration),
                        CostAtPurchase = memberCategory.Category.Cost,
                    };
                    context.AddToMemberRole(renewedCategory);
                    renewedCategory.Replacing = memberCategory.MemberCategory.ID;
                    memberCategory.MemberCategory.Replaced = true;
                    context.SaveChanges();
                    Utils.ServiceSuccessResultHandler(ref data, renewedCategory, 2);
                }

                
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> RequestChange(long MemberCategoryID, long newCategoryID, long PaymentSchemaID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            context.Connection.Open();
            using (DbTransaction transaction = context.Connection.BeginTransaction()) {
                try {
                    var memberCategory = (from memcats in context.MemberRole.Where(w => w.Replaced == null && w.MemberID == SessionManager.MemberData.ID && w.ID == MemberCategoryID).AsEnumerable()
                                          join cats in context.Role on memcats.RoleID equals cats.ID
                                          select new {
                                              MemberCategory = memcats,
                                              Category = cats
                                          }).FirstOrDefault();
                    var newCategory = (from cats in context.Role
                                       where cats.ID == newCategoryID
                                       select cats).FirstOrDefault();
                    long memberCategoryID = 0;
                    if (memberCategory == null) {
                        Utils.ServiceSuccessResultHandler(ref data, "No Category Found", -1);
                    } else {
                        var successCode = 0;
                        MemberRole memberRole = new MemberRole();
                        memberRole.ClientSiteID = SessionManager.ClientSiteID;
                        memberRole.MemberID = SessionManager.MemberData.ID;
                        memberRole.RoleID = newCategoryID;
                        memberRole.InsertedOn = DateTime.Now;
                        memberRole.Replacing = MemberCategoryID;
						memberRole.IsRecurring = newCategory.IsRecurring;

                        bool isPending = (newCategory.DefaultToPending == true) || (newCategory.Cost > 0);
                        bool isExpiring = (newCategory.RoleDuration > 0);
                        bool isCosted = (newCategory.Cost > 0);

                        if (isPending) {
                            memberRole.IsActivated = false;
                        } else {
                            memberRole.IsActivated = true;
                            memberRole.CommencedOn = memberRole.InsertedOn;
                        }
                        if (!isPending && isExpiring) {
                            memberRole.ExpiresOn = memberRole.InsertedOn.Value.AddDays((double)newCategory.RoleDuration);
                        }
                        memberRole.CostAtPurchase = newCategory.Cost;
                        context.AddToMemberRole(memberRole);
                        var success = context.SaveChanges();
						memberCategoryID = memberRole.ID;
						
                        string transactionNumber = "No Transaction";
                        if (success == 1) {
                            if (!isPending) {
                                successCode = 1; //Successful Activation, allow user to log in.
                                Utils.markCategoryAsReplaced(MemberCategoryID);
                            } else if (!isCosted) {
                                successCode = 2; //Successful capture, requires activation by site owner
                            } else if (isCosted) {
                                successCode = 3; //Successful capture, prompt user for payment options.

                                //Create a transaction log that will be picked up by the process payment page.
                                TransactionLog payment = new TransactionLog();
                                payment.ClientSiteID = SessionManager.ClientSiteID;
                                payment.PaymentSchemaID = PaymentSchemaID;
                                payment.MemberID = SessionManager.MemberData.ID;
                                payment.PaymentType = "Change Category";
                                payment.MemberCategoryID = memberRole.ID;
                                payment.InsertedOn = DateTime.Now;
                                payment.InsertedBy = SessionManager.ClientSiteID;
                                //calculate total
                                payment.TotalAmount = (decimal)memberRole.CostAtPurchase;
								payment.IsRecurring = newCategory.IsRecurring;
								payment.Duration = newCategory.RoleDuration;

                                context.AddToTransactionLog(payment);
                                context.SaveChanges();
                            
                                memberRole.TransactionID = payment.ID;
                                if (SessionManager.CurrentTransactions == null) {
                                    SessionManager.CurrentTransactions = new List<TransactionLog>();
                                }
                                SessionManager.CurrentTransactions.Add(payment);
                                
                                transactionNumber = payment.ID.ToString();
                            }
                        }
                        success = context.SaveChanges();
                        string messageXml = string.Empty;
                        messageXml += "<xmldata><users>";
                        messageXml +=
                            String.Format("<user email=\"{0}\" memfirstname=\"{1}\" memlastname=\"{2}\" mememail=\"{3}\" transactiontype=\"{4}\" catname=\"{5}\" amount=\"{6}\" transactionnumber=\"{7}\"></user>",
                                            Utils.EncodeXML(CacheManager.SiteData.Email1),
                                            Utils.EncodeXML(SessionManager.MemberData.FirstName),
                                            Utils.EncodeXML(SessionManager.MemberData.LastName),
                                            Utils.EncodeXML(SessionManager.MemberData.Email),
                                            Utils.EncodeXML("Change Category"),
                                            Utils.EncodeXML("From " + memberCategory.Category.Name + " to " + newCategory.Name),
                                            Utils.EncodeXML(newCategory.Cost.Value.ToString("0.00")),
                                            Utils.EncodeXML(transactionNumber));
                        messageXml += "</users></xmldata>";

                        transaction.Commit();
                        try {
                            Utils.SendToMessageCentre(
                                        Utils.MessageCentreEvents.SysMemberNewRegistration.GetEnumDescriptionAttribute(),
                                        string.Empty, string.Empty, Utils.MessageCentreEvents.SysMemberNewRegistration.GetEnumDescriptionAttribute(), messageXml.Replace("email1", "email"));
							if (memberCategoryID > -1 && successCode == 1) {
                                Utils.SendMemberCategoryMessageEmail(memberCategoryID, 0, false);
                            }
                            Utils.ServiceSuccessResultHandler(ref data, "", successCode);
                        } catch (Exception ex) {
                            Utils.ServiceErrorResultHandler(ex, ref data);
                        }
                    } 
                } catch (Exception ex) {
                    transaction.Rollback();
                    Utils.ServiceErrorResultHandler(ex, ref data);
                }
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetSitePaymentPlans() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var plans = from plan in context.PaymentSchemaClientSite
                            join planscheme in context.PaymentSchema on plan.PaymentSchemaID equals planscheme.ID
                            where plan.ClientSiteID == SessionManager.ClientSiteID
                            select new {
                                PlanID = plan.ID,
                                Name = planscheme.Name,
								SupportsRecurring = planscheme.SupportsRecurring
                            };

                Utils.ServiceSuccessResultHandler(ref data, plans.ToList(), plans.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetSecureMenu() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				string menu = Utils.SecureMenu();
				int loggedIn = (SessionManager.VistorSecureSessionID != Guid.Empty && SessionManager.MemberData != null) ? 1 : 0;
				Utils.ServiceSuccessResultHandler(ref data, menu, loggedIn);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

		/// <summary>
		/// Retrieves Member data for Member Showcase
		/// </summary>
		/// <returns></returns>
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetMemberShowcase(int numberOfMembers, string categoryFilter, bool randomise) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				var cats = categoryFilter.Split(',');
				var catIDs = cats.Where(w => w.Length > 0).Select(s=> Convert.ToInt64(s.Trim()));
				var members = from mem in context.Member
							  join memcat in context.MemberRole on mem.ID equals memcat.MemberID
							  where memcat.LapsedOn == null
							  && memcat.IsActivated == true
							  && memcat.ClientSiteID == SessionManager.ClientSiteID
							  && memcat.Replaced == null
							  && (catIDs.Contains(memcat.RoleID) || catIDs.Count() == 0)
							  select mem;
				if (randomise) {
					members = members.OrderBy(o => Guid.NewGuid()).Take(numberOfMembers);
				} else {
					members = members.OrderBy(o => o.InsertedOn).Take(numberOfMembers);
				}
				var returnMembers = members.ToList();
				foreach (var mem in returnMembers) {
					context.Detach(mem);	
					mem.Password = null;
					mem.Email = null;
					mem.PrivateTel = null;
					var checkstring = mem.PrimaryImageURL;
					if (checkstring != null) {
						String memberpath = Server.MapPath(checkstring);
						if (!File.Exists(memberpath)) {
							mem.PrimaryImageURL = null;
						}
					}
				}

				Utils.ServiceSuccessResultHandler(ref data, returnMembers, returnMembers.Count());
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}

			return data;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetClientLicense() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var licenses = from memberSettings in context.MembershipSettings
                               join license in context.License on memberSettings.ID equals license.MemberSettings
                               where license.CompanyID == SessionManager.MemberData.OrganisationID
                               select new { memberSettings, license };

                Utils.ServiceSuccessResultHandler(ref data, licenses.ToList(), licenses.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// Gets all departments within a company
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetDepartments() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var deparments = from deps in context.Departments
                                 where deps.ClientSiteID == SessionManager.ClientSiteID
                                       && deps.CompanyID == SessionManager.MemberData.OrganisationID
                                 select deps;

                Utils.ServiceSuccessResultHandler(ref data, deparments.ToList(), deparments.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        /// <summary>
        /// Gets all members that belong to a company
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetMembersForCompany() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var companyMembers = from members in context.Member
                                     where members.OrganisationID == SessionManager.MemberData.OrganisationID
                                           && members.isActive == true
                                           && members.isDefault == false
                                           && members.DeletedBy == null
                                           && members.DeletedOn == null
                                     select members;

                Utils.ServiceSuccessResultHandler(ref data, companyMembers.ToList(), companyMembers.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;

        }

        /// <summary>
        /// Saves members that pay for membership service
        /// </summary>
        /// <param name="member">Member Entity</param>
        /// <param name="licenseID">Selected License</param>
        /// <param name="isCompany">Company registrar</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> SaveMember(Member member, long licenseID, bool isCompany) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                if (member.ID == 0) {
                    //check if users are allowed to be created
                    var licenses = (from license in context.License
                                    join membershipSettingse in context.MembershipSettings on license.MemberSettings equals membershipSettingse.ID
                                    where license.CompanyID == SessionManager.MemberData.OrganisationID
                                    && license.MemberSettings == licenseID
                                    select license.Total).SingleOrDefault();

                    var members = from membr in context.Member
                                  where membr.OrganisationID == SessionManager.MemberData.OrganisationID
                                  select membr;


                    if (members.Count() >= licenses) {
                        return data;
                    }



                }
                //get deafult user to get the expiry date
                var defaultUser = (from defaultMember in context.Member
                                   where defaultMember.OrganisationID == SessionManager.MemberData.OrganisationID
                                         && defaultMember.isDefault == true
                                   select defaultMember).SingleOrDefault();

                //set up defaults
                member.OrganisationID = SessionManager.MemberData.OrganisationID;
                member.ExpiresOn = defaultUser.ExpiresOn;

                //save
                if (member.ID == 0) {
                    context.AddToMember(member);
                } else {
                    context.Member.Attach(member);
                    member.SetAllModified(context);
                }


                context.SaveChanges();

                //if new user append to the license table
                if (member.ID == 0) {
                    context.AddToMemberLicense(new MemberLicense { MemberID = member.ID, LicenseID = licenseID });
                    context.SaveChanges();
                }

                Utils.ServiceSuccessResultHandler(ref data, member, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// Get the current member's license
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetLicenseForMember(long memberID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var license = from memLicense in context.MemberLicense
                              where memLicense.MemberID == memberID
                              select memLicense;
                Utils.ServiceSuccessResultHandler(ref data, license.ToList(), 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// Gets all available payment plans.
        /// </summary>
        /// <param name="settingsID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetPaymentPlans(long settingsID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                if (settingsID == 0) {
                    var plans = from paymentPlans in context.PaymentSchema
                                where paymentPlans.DeletedBy == null
                                && paymentPlans.DeletedOn == null
                                select paymentPlans;

                    Utils.ServiceSuccessResultHandler(ref data, plans.ToList(), plans.Count());
                } else {
                    var settingPlans = from paymentPlans in context.PaymentSchema
                                       join memberSettingsPaymentSchema in context.MemberSettingsPaymentSchema on paymentPlans.ID equals memberSettingsPaymentSchema.PaymentSchemaID
                                       where paymentPlans.DeletedBy == null
                                       && paymentPlans.DeletedOn == null
                                       && memberSettingsPaymentSchema.DeletedBy == null
                                       && memberSettingsPaymentSchema.DeletedOn == null
                                       && memberSettingsPaymentSchema.MemberSettingsID == settingsID
                                       select paymentPlans;

                    Utils.ServiceSuccessResultHandler(ref data, settingPlans.ToList(), settingPlans.Count());
                }



            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        /// <summary>
        ///  Gets all items linked to a role that the current member belongs to.
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetMemberContent() {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {

                StringBuilder menu = new StringBuilder();
                var roles = (from memberRoles in context.MemberRole
                             where memberRoles.MemberID == SessionManager.MemberData.ID
                             select memberRoles.RoleID).ToList<long>();

                var menuItems = from roleModule in context.RoleModule
                                where roles.Contains(roleModule.RoleID)
                                group roleModule by roleModule.ModuleItemID
                                    into groupedResults
                                    select groupedResults;

                foreach (System.Linq.IGrouping<long, RoleModule> roleModules in menuItems) {
                    string itemTitle = string.Empty;
                    switch (roleModules.First().ModuleName) {
                        case "News":
                            itemTitle = (from news in context.News
                                         where news.ID == roleModules.Key
                                         select news.Title).First();
                            break;
                        case "Events":
                            itemTitle = (from events in context.Event
                                         where events.ID == roleModules.Key
                                         select events.Title).First();
                            break;
                        case "PageContent":
                            itemTitle = (from pages in context.PageContent
                                         where pages.ID == roleModules.Key
                                         select pages.ShortTitle).First();
                            break;
                    }

                    menu.Append(String.Format("<li><a dataid='{0}' datatitle='{1} href=\"/{1}/{0}/{2}\">{2}</a></li>",
                                              roleModules.First().ModuleItemID, roleModules.First().ModuleName,
                                              itemTitle));
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetLicenseForSettings(long settingID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var license = from memPay in context.MemberSettingsPaymentSchema
                              join paymentSchema in context.PaymentSchema on memPay.PaymentSchemaID equals
                                  paymentSchema.ID
                              where memPay.ClientSiteID == SessionManager.ClientSiteID
                              group paymentSchema by memPay.MemberSettingsID into grouped
                              select grouped;

                Utils.ServiceSuccessResultHandler(ref data, license.ToList(), license.Count());

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }

        /// <summary>
        /// Sends comms to a member using their email as an identifier.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> LostPassword(string email) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var member = (from members in context.Member
                              where members.Email.ToLower() == email.Trim().ToLower()
                              && members.ClientSiteID == SessionManager.ClientSiteID
                              && members.DeletedBy == null
                              && members.DeletedOn == null
                              select members).FirstOrDefault();
                
                if (member != null) {
                    member.Password = Security.Encryption.Decrypt(member.Password);
                    var entity = new List<dynamic>();
                    entity.Add(member);
                    Utils.SendToMessageCentre(Utils.MessageCentreEvents.MemberForgottenPassword.GetEnumDescriptionAttribute(), string.Empty, string.Empty,
                                        CacheManager.SiteData.CompanyName + ": Password Retrieval", Utils.GenerateCommunicationXML(entity),
                                        CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);

                    Utils.ServiceSuccessResultHandler(ref data, member.Email, 1);

                } else {
                    Utils.ServiceSuccessResultHandler(ref data, null, 0);
                }



            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        /// <summary>
        /// Allows the updating of a members password
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <param name="confirmPassword"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> ChangeMemberPassword(long memberID, string oldPassword, string newPassword, string confirmPassword) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //get the member from memberID
                var currentMember = context.Member.Where(member => member.ID == memberID).FirstOrDefault();
                int successfulChange = -1;

                //does the member exist?
                if (currentMember != null) {
                    if (oldPassword == currentMember.Password && newPassword == confirmPassword) {
                        currentMember.Password = newPassword;
                        currentMember.SetAllModified(context);
                        context.SaveChanges();
                        successfulChange = 1;

                    } else {
                        //password does not match
                        successfulChange = -2;
                    }
                } else {
                    //member not found
                    successfulChange = 0;
                }

                Utils.ServiceSuccessResultHandler(ref data, currentMember, successfulChange);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
                throw;
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> ChangeLoggedInMemberPassword(string oldPassword, string newPassword, string confirmPassword) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //get the member from memberID
                var currentMember = context.Member.Where(member => member.ID == SessionManager.MemberData.ID).FirstOrDefault();
                int successfulChange = -1;

                //does the member exist?
                if (currentMember != null) {
                    if (Security.Encryption.Encrypt(oldPassword) == currentMember.Password && newPassword == confirmPassword) {
                        currentMember.Password = Security.Encryption.Encrypt(newPassword);
                        context.SaveChanges();
                        successfulChange = 1;

                    } else {
                        //password does not match
                        successfulChange = -2;
                    }
                } else {
                    //member not found
                    successfulChange = 0;
                }

                SessionManager.MemberData = currentMember;

                Utils.ServiceSuccessResultHandler(ref data, "", successfulChange);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
                throw;
            }

            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetMembershipCategoryInformation(long categoryID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var categoryInfo = (from cat in context.Role
                                    where cat.ID == categoryID && cat.ClientSiteID == SessionManager.ClientSiteID
                                    select cat).FirstOrDefault();

                Utils.ServiceSuccessResultHandler(ref data, categoryInfo, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
                throw;
            }

            return data;
        }
        #endregion

        #region Finance

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> CreateTransacation(long PaymentSchemaID, string PaymentType, decimal Amount, long MemberID) {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                TransactionLog myTransaction = new TransactionLog {
                    MemberID = MemberID,
                    PaymentSchemaID = PaymentSchemaID,
                    PaymentType = PaymentType,
                    TotalAmount = Amount,
                    ClientSiteID = SessionManager.ClientSiteID,
                    InsertedBy = SessionManager.MemberData.ID,
                    InsertedOn = DateTime.Now

                };
                context.AddToTransactionLog(myTransaction);
                context.SaveChanges();
                if (SessionManager.CurrentTransactions == null) {
                    SessionManager.CurrentTransactions = new List<TransactionLog>();
                }
                SessionManager.CurrentTransactions.Add(myTransaction);
                Utils.ServiceSuccessResultHandler(ref data, myTransaction, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> CreateTransacation(long PaymentSchemaID, string PaymentType, decimal Amount, long MemberID, string Data, int? Duration = null, bool? IsRecurring = null, bool RequiresShipping = true) {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
                SessionManager.CurrentTransactions.RemoveAll(tr => tr != null);
            }
            try {
                TransactionLog myTransaction = new TransactionLog {
                    MemberID = MemberID,
                    PaymentSchemaID = PaymentSchemaID,
                    PaymentType = PaymentType,
                    TotalAmount = Amount,
                    ClientSiteID = SessionManager.ClientSiteID,
                    InsertedBy = SessionManager.MemberData.ID,
                    InsertedOn = DateTime.Now,
                    DataXML = Data,
					IsRecurring = IsRecurring,
					RequiresShipping = RequiresShipping,
					Duration = Duration

                };
                context.AddToTransactionLog(myTransaction);
                context.SaveChanges();
                if (SessionManager.CurrentTransactions == null) {
                    SessionManager.CurrentTransactions = new List<TransactionLog>();
                }
                SessionManager.CurrentTransactions.Add(myTransaction);
                Utils.ServiceSuccessResultHandler(ref data, myTransaction, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetPaymentSchemas() {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Dictionary<string, string> settings = Utils.GetFieldsFromCustomXML("Products");

                if (settings.Count > 0) {
                    IEnumerable<long> paymentSchemaID = Utils.ConvertDBNull(settings["Products_PaymentSchema"], string.Empty).Split(',').Select(n => long.Parse(n)).ToList();

					var schemas = context.GetValidClientPaymentGateways(SessionManager.ClientSiteID).Where(w => paymentSchemaID.Contains(w.ID)).ToList();

                    Utils.ServiceSuccessResultHandler(ref data, schemas, schemas.Count());
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetPaymentRecurringOptions(long PaymentSchemaId)
		{
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
				//retrieve product configurations settings, these have recurring months and interest per recurring month
                Dictionary<string, string> settings = Utils.GetFieldsFromCustomXML("Products");

                if (settings.Count > 0) {
                    IEnumerable<long> paymentSchemaID = Utils.ConvertDBNull(settings["Products_PaymentSchema"], string.Empty).Split(',').Select(n => long.Parse(n)).ToList();

                    if (settings.ContainsKey("Products_RecurringOption")) {
                        if (!string.IsNullOrEmpty(settings["Products_RecurringOption"])) {
                        IEnumerable<int> recurringOptions = Utils.ConvertDBNull(settings["Products_RecurringOption"], string.Empty).Split(',').Select(n => int.Parse(n)).ToList();
                        int recurringInterest = 0;
                        int.TryParse(settings["Products_RecurringInterest"], out recurringInterest);
                        int minimumBasketTotal = 0;
                        int.TryParse(settings["Products_RecurringCutoffAmount"], out minimumBasketTotal);

                        //load schema if it supports recurring payments and clients has access to it.
                        var schema = context.GetValidClientPaymentGateways(SessionManager.ClientSiteID).Where(w => paymentSchemaID.Contains(w.ID) && w.ID == PaymentSchemaId && w.SupportsRecurring == true && w.RecurringHTML != null).ToList();

                        var recurringOptionsToReturn = recurringOptions.Select(x => new { Month = x + " Month(s)", MonthValue = x, recurringInterest = recurringInterest, minimumBasketTotal = minimumBasketTotal }).ToList();
                        Utils.ServiceSuccessResultHandler(ref data, recurringOptionsToReturn, recurringOptionsToReturn.Count());
                    
                    }
                    }
										
                }
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetTransactionDetails() {
			Utils.CheckSession();
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
				if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
                Utils.ServiceSuccessResultHandler(ref data, SessionManager.CurrentTransactions.First(), 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, null, 0);
				}
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }
        [WebMethod(EnableSession = true)]

        public Dictionary<string, object> CreateDonationTransaction(long PaymentSchemaID, string PaymentType, decimal Amount, string donationData) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                Utils.CheckSession();
                //remove any transactions
                if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
                    SessionManager.CurrentTransactions.RemoveAll(tr => tr != null);
                }
                //Create a log for the transaction
                TransactionLog myTransaction = new TransactionLog {
                    MemberID = null,
                    PaymentSchemaID = PaymentSchemaID,
                    PaymentType = PaymentType,
                    TotalAmount = Amount,
                    ClientSiteID = SessionManager.ClientSiteID,
                    InsertedBy = SessionManager.ClientSiteID,
                    InsertedOn = DateTime.Now,
                    DataXML = donationData

                };
				Dictionary<string,string> _xml = Utils.GetFieldsForXmlData(donationData);
                if (_xml.ContainsKey("isRecurring")) {
                    string isRec;
                    if (_xml.TryGetValue("isRecurring", out isRec)) {
                        if (isRec.ToLower() == "on") {
                            myTransaction.IsRecurring = true;
                        } else {
                            myTransaction.IsRecurring = false;
                        }
                    }
                    string duration;
                    if (_xml.TryGetValue("Duration", out duration)) {
                        int Duration;
                        if (int.TryParse(duration, out Duration)) {
                            myTransaction.Duration = Duration;
                        } else {
                            myTransaction.Duration = 1;
                        }
                    }
                }
                context.AddToTransactionLog(myTransaction);
                //commit
                context.SaveChanges();
                if (SessionManager.CurrentTransactions == null) {
                    SessionManager.CurrentTransactions = new List<TransactionLog>();
                }
                SessionManager.CurrentTransactions.Add(myTransaction);
                Utils.ServiceSuccessResultHandler(ref data, myTransaction, 1);
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }

        #endregion

        #region Utils

        #endregion

        #region Shipping Modules
		#region SAPO
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetQuoteSAPO(int countryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			decimal? totalWeight = 0, totalVolume = 0, shippingCost = 0;
			try {
				//Get products & dimensions from transaction cache
				if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
					XDocument products = XDocument.Parse(SessionManager.CurrentTransactions[0].DataXML);
					var query = from prodTrans in products.Descendants("field")
								join product in context.Product on int.Parse(prodTrans.Attribute("id").Value) equals
									product.ID
								select new {
									Product = product,
									Quantity = Convert.ToInt32(prodTrans.Attribute("quantity").Value)
								};
					bool calcBasedOnWeight = false;
					AuroraShippingService shippingServ = new AuroraShippingService();
					bool isInternational = Convert.ToBoolean(shippingServ.CheckIfInternational(countryID)["Data"]);

					//Determine whether or not to use Weight based calculations
					foreach (var x in query) {
						if (isInternational) {
							// check against International
							bool fixedOrFreeShippingInt = x.Product.isZeroShipping == true || ((!(x.Product.isZeroShipping == true)) && (x.Product.isFixedShipping == true && x.Product.ShippingPriceInt != null));
							if (!fixedOrFreeShippingInt) {
								calcBasedOnWeight = true;
							}
						} else {
							// check against Local
							bool fixedOrFreeShippingLoc = x.Product.isZeroShipping == true || ((!(x.Product.isZeroShipping == true)) && (x.Product.isFixedShipping == true && x.Product.ShippingPriceLoc != null));
							if (!fixedOrFreeShippingLoc) {
								calcBasedOnWeight = true;
							}
						}
					}
					

					if (calcBasedOnWeight) {
						//Calculate shipping by Weight
						foreach (var product in query) {
							if (product.Product.Weight > 0 && product.Product.isZeroShipping != true) {
								totalWeight += product.Product.Weight * product.Quantity;
							}
						}
						shippingCost = Convert.ToDecimal(shippingServ.CalculateShippingCost(countryID, Utils.ConvertDBNull(totalWeight, 0))["Data"]);
					} else {
						//Calculate fixed shipping 
						if (isInternational) {
							foreach (var product in query) {
								if (product.Product.isFixedShipping == true && product.Product.isZeroShipping != true) {
									shippingCost += product.Product.ShippingPriceInt * product.Quantity;
								}
							}
						} else {
							foreach (var product in query) {
								if (product.Product.isFixedShipping == true && product.Product.isZeroShipping != true) {
									shippingCost += product.Product.ShippingPriceLoc * product.Quantity;
								}
							}
						}
						
					}
					ShippingModule SAPO = context.ShippingModule.Where(w => w.Name == "SAPO").FirstOrDefault();

					shippingCost = Convert.ToDecimal(Utils.ConvertCurrency(Convert.ToDouble(shippingCost.Value), SAPO.CurrencyID, null));

					Utils.ServiceSuccessResultHandler(ref data, shippingCost, 1);

				} else {
					Utils.ServiceSuccessResultHandler(ref data, null, 0);
				}
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> AppendSAPOPriceToQuote(int CountryID, string Address1, string Address2, string Address3, string City, string PostCode) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			decimal? totalWeight = 0, totalVolume = 0;
			try {
				
				var productConfig = Utils.GetFieldsFromCustomXML("Products");
				//string currencyname = productConfig["Products_ShippingCurrency"];

				//Get products & dimensions from transaction cache
				if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
					long transactionDBID = SessionManager.CurrentTransactions[0].ID;

					var dbtransactionLog = (from transactions in context.TransactionLog
											where transactions.ID == transactionDBID
											select transactions
										   ).SingleOrDefault();

					XDocument products = XDocument.Parse(dbtransactionLog.DataXML);
					var query = from prodTrans in products.Descendants("field")
								join product in context.Product on int.Parse(prodTrans.Attribute("id").Value) equals
									product.ID
								select product;

					decimal shippingCost = Convert.ToDecimal(GetQuoteSAPO(CountryID)["Data"]);

					////update data xml for transaction log
					
					products.Root.SetAttributeValue("ShippingCost", shippingCost);
					products.Root.SetAttributeValue("AddressLine1", Address1);
					products.Root.SetAttributeValue("AddressLine2", Address2);
					products.Root.SetAttributeValue("AddressLine3", Address3);
					products.Root.SetAttributeValue("City", City);
					products.Root.SetAttributeValue("PostCode", PostCode);

					products.Descendants("Fields").FirstOrDefault().Attribute("total").SetValue(SessionManager.CurrentTransactions[0].TotalAmount + (decimal)shippingCost);

					dbtransactionLog.TotalAmount = SessionManager.CurrentTransactions[0].TotalAmount + (decimal)shippingCost;
					dbtransactionLog.DataXML = products.ToString();
					//Update DB
					dbtransactionLog.SetAllModified(context);
					context.SaveChanges();
					SessionManager.CurrentTransactions[0] = dbtransactionLog;
					//Send data down
					Utils.ServiceSuccessResultHandler(ref data, dbtransactionLog, 1);
				} else {
					Utils.ServiceSuccessResultHandler(ref data, null, 0);
				}


			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		/// <summary>
		/// Returns the data of a passed result converted to decimal
		/// </summary>
		/// <param name="result"></param>
		/// <returns></returns>
		private decimal GetPriceFromSAPOShippingResult(Dictionary<string, object> result) {
			return Convert.ToDecimal(result["data"]);
		}

		#endregion
		#region RAM
		[WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetQuoteRAM(string serviceTypeCode, string recieverZoneID) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            decimal? totalWeight = 0, totalVolume = 0;
            try {
                //get ram module config
				var xml = (from ship in context.ShippingModule
						   join cship in context.ClientShippingModule on ship.ID equals cship.ShippingModuleID
						   where ship.Name == "RAM" && cship.ClientSiteID == SessionManager.ClientSiteID
						   select cship.CustomXML).FirstOrDefault();
				var config = Utils.GetFieldsForXmlData(xml, true);

                //Get products & dimensions from transaction cache
                if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
                    XDocument products = XDocument.Parse(SessionManager.CurrentTransactions[0].DataXML);
                    var query = from prodTrans in products.Descendants("field")
                                join product in context.Product on int.Parse(prodTrans.Attribute("id").Value) equals
                                    product.ID
                                select product;

                    foreach (Product product in query) {
                        if (product.Height > 0 && product.Weight > 0 && product.Length > 0 && product.Width > 0) {
                            totalWeight += product.Weight;
                            totalWeight += (product.Weight * product.Height * product.Length);
                        }
                    }

                    RAMCouriers ramQuote = new RAMCouriers(config["RAM_UserName"], config["RAM_UserPassword"]);
					XDocument result = XDocument.Parse(ramQuote.GetQuoteV3(
                                                   config["RAM_UserName"],
                                                   serviceTypeCode,
                                                   config["RAM_FromAddress"],
                                                   recieverZoneID,
                                                   false,
                                                   false,
                                                   false,
                                                   0.00,
                                                   false,
                                                   false,
                                                   Convert.ToDouble(totalWeight),
                                                   0.00,
                                                   0.00,
                                                   Convert.ToDouble(totalVolume),
                                                   0
                                                   ).ToString());

					var shipAmount = result.Descendants(XName.Get("Bill_GrandTotal", ""));
					var shipVat = result.Descendants(XName.Get("Bill_VAT", ""));

					ShippingModule RAM = context.ShippingModule.Where(w => w.Name == "RAM").FirstOrDefault();

					shipAmount.First().SetValue(Utils.ConvertCurrency(Convert.ToDouble(shipAmount.First().Value), RAM.CurrencyID));
					shipVat.First().SetValue(Utils.ConvertCurrency(Convert.ToDouble(shipVat.First().Value), RAM.CurrencyID));

					var xmlDoc = new XmlDocument();
					xmlDoc.LoadXml(result.ToString());

					Utils.ServiceSuccessResultHandler(ref data, Utils.XmlToJSON(xmlDoc), 1);

                } else {
                    Utils.ServiceSuccessResultHandler(ref data, null, 0);
                }


            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }
        [WebMethod(EnableSession = true)]
		public Dictionary<string, object> AppendPriceToQuote(string serviceTypeCode, string recieverZoneID, string Address1, string Address2, string Address3, string City, string PostCode) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            decimal? totalWeight = 0, totalVolume = 0;
            try {
				var xml = (from ship in context.ShippingModule
						   join cship in context.ClientShippingModule on ship.ID equals cship.ShippingModuleID
						   where ship.Name == "RAM" && cship.ClientSiteID == SessionManager.ClientSiteID
						   select cship.CustomXML).FirstOrDefault();
				var config = Utils.GetFieldsForXmlData(xml, true);
				var productConfig = Utils.GetFieldsFromCustomXML("Products");

                //Get products & dimensions from transaction cache
                if (SessionManager.CurrentTransactions != null && SessionManager.CurrentTransactions.Count() > 0) {
                    long transactionDBID = SessionManager.CurrentTransactions[0].ID;

                    var dbtransactionLog = (from transactions in context.TransactionLog
                                            where transactions.ID == transactionDBID
                                            select transactions
                                           ).SingleOrDefault();

                    XDocument products = XDocument.Parse(dbtransactionLog.DataXML);
                    var query = from prodTrans in products.Descendants("field")
                                join product in context.Product on int.Parse(prodTrans.Attribute("id").Value) equals
                                    product.ID
                                select product;

                    foreach (Product product in query) {
                        if (product.Height > 0 && product.Weight > 0 && product.Length > 0 && product.Width > 0) {
                            totalWeight = totalWeight + product.Weight;
                            totalVolume = totalVolume + (product.Weight * product.Height * product.Length);
                        }
                    }

                    RAMCouriers ramQuote = new RAMCouriers(config["RAM_UserName"], config["RAM_UserPassword"]);
                    XDocument result = ramQuote.GetQuoteV3(
                                                   config["RAM_UserName"],
                                                   serviceTypeCode,
                                                   config["RAM_FromAddress"],
                                                   recieverZoneID,
                                                   false,
                                                   false,
                                                   false,
                                                   0.00,
                                                   false,
                                                   false,
                                                   Convert.ToDouble(totalWeight),
                                                   0.00,
                                                   0.00,
                                                   Convert.ToDouble(totalVolume),
                                                   0
                                                   );

					//get the ram shipping module info so we can convert to the default site currency
					ShippingModule ramShipping = context.ShippingModule.Where(sm => sm.Name == "RAM").FirstOrDefault();
                    //update data xml for transaction log
                    XElement quoteData = result.Descendants(XName.Get("Qoute", "")).ElementAt(1);
					//convert the returned sipping cost values to the sites default currency. We always need to do this because shipping modules have a currency and there may be a different default currency set for the site
					var shipAmount = Convert.ToDecimal(Math.Round(Utils.ConvertCurrency(Convert.ToDouble(quoteData.Descendants(XName.Get("Bill_GrandTotal", "")).First().Value), ramShipping.CurrencyID, null),2));
					var shipVat = Convert.ToDecimal(Math.Round(Utils.ConvertCurrency(Convert.ToDouble(quoteData.Descendants(XName.Get("Bill_VAT", "")).First().Value), ramShipping.CurrencyID, null),2));

					products.Root.SetAttributeValue("ShippingCost", shipAmount);
					products.Root.SetAttributeValue("ShippingVAT", shipVat );
					products.Root.SetAttributeValue("AddressLine1", Address1);
					products.Root.SetAttributeValue("AddressLine2", Address2);
					products.Root.SetAttributeValue("AddressLine3", Address3);
					products.Root.SetAttributeValue("City", City);
					products.Root.SetAttributeValue("PostCode", PostCode);

					products.Descendants("Fields").FirstOrDefault().Attribute("total").SetValue(SessionManager.CurrentTransactions[0].TotalAmount + shipAmount);

                    dbtransactionLog.TotalAmount = SessionManager.CurrentTransactions[0].TotalAmount + shipAmount;
                    dbtransactionLog.DataXML = products.ToString();


                    //Update DB
                    dbtransactionLog.SetAllModified(context);
                    context.SaveChanges();
                    SessionManager.CurrentTransactions[0] = dbtransactionLog;
                    //Send data down
                    Utils.ServiceSuccessResultHandler(ref data, dbtransactionLog, 1);
                } else {
                    Utils.ServiceSuccessResultHandler(ref data, null, 0);
                }


            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetSuburb(string filter, int maxRecords) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                //get ram module config
				var xml = (from ship in context.ShippingModule
						   join cship in context.ClientShippingModule on ship.ID equals cship.ShippingModuleID
						   where ship.Name == "RAM" && cship.ClientSiteID == SessionManager.ClientSiteID
						   select cship.CustomXML).FirstOrDefault();
                var config = Utils.GetFieldsForXmlData(xml, true);
                RAMCouriers ramInterface = new RAMCouriers(config["RAM_UserName"], config["RAM_UserPassword"]);

                Utils.ServiceSuccessResultHandler(ref data, ramInterface.SuburbSearch(filter, maxRecords), maxRecords);

            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }
            return data;
        }
        #endregion
        #endregion

        #region Custom Modules
        /// <summary>
        /// Gets all data for a custom module
        /// </summary>
        /// <param name="moduleName">Name of the custom Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Dictionary<string, object> GetCustomModuleData(string moduleName) {
            Dictionary<string, object> data = new Dictionary<string, object>();
            try {
                var customModuleData = from customMod in context.CustomModules
                                       join moduleData in context.CustomModuleData on customMod.ID equals
                                           moduleData.ModuleID
                                       select moduleData;
                Utils.ServiceSuccessResultHandler(ref data, customModuleData.ToList(), customModuleData.Count());
            } catch (Exception ex) {
                Utils.ServiceErrorResultHandler(ex, ref data);
            }

            return data;
        }


        [WebMethod(EnableSession = true)]
		public string LoadURLXML(string url, int totalToTake)
		{
            Dictionary<string, object> data = new Dictionary<string, object>();
			string result = string.Empty;
            try {

				HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
				myRequest.Method = "GET";
				WebResponse myResponse = myRequest.GetResponse();
				StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
				result = sr.ReadToEnd();
				sr.Close();
				myResponse.Close();

				XmlDocument _x = new XmlDocument();
				_x.LoadXml(result);
				XmlNodeList _e = _x.GetElementsByTagName("PACKAGE");
				XmlDocument _newDoc = new XmlDocument();
				Regex rgx = new Regex("[^a-zA-Z0-9]");
				for (int i = 0; i < _e.Count; i++)
				{
					if (_e[i].Name != null)
					{
						if (i < totalToTake)
						{
							//_e[i].RemoveAll();
							continue;
						}
						for (int r = 0; r < _e[i].ChildNodes.Count; r++)
						{
							_e[i].ChildNodes[r].InnerText = "";
							if ("INCLUDES" == _e[i].ChildNodes[r].Name || "INFO" == _e[i].ChildNodes[r].Name)
							{
								//byte[] bytes = Encoding.Default.GetBytes(_e[i].ChildNodes[r].InnerText.Replace("�", "").Replace("\"","").Replace("\\","").Replace(",",""));
								//_e[i].ChildNodes[r].InnerText = Encoding.UTF8.GetString(bytes);
								//_e[i].ChildNodes[r].InnerText = Server.HtmlEncode(_e[i].ChildNodes[r].InnerText);
								//_e[i].ChildNodes[r].InnerText = Utils.EscapeStringValue(_e[i].ChildNodes[r].InnerText);
								_e[i].ChildNodes[r].InnerText = rgx.Replace(_e[i].ChildNodes[r].InnerText,"");
								string tmp = "v*i|n,c:e'";
								tmp = rgx.Replace(tmp, "");
								_e[i].ChildNodes[r].RemoveAll();
							}
							else if ("IMG" == _e[i].ChildNodes[r].Name)
							{
								_e[i].ChildNodes[r].InnerText = _e[i].ChildNodes[r].InnerText.Replace("\\", "");
							}
						}
					}
				}

				result = Utils.XmlToJSON(_x).Replace("�", "");
				int total = result.Length;
				//result = JsonConvert.SerializeXmlNode(_x);

				Utils.ServiceSuccessResultHandler(ref data, result, 1);
            } catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
            }

			return result;
        }
        #endregion

		[WebMethod(EnableSession = true)]
		public Dictionary<string, object> GetCurrencyConversions() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {

				var currencyConversions = (from curr in context.CurrencyConversions
										   join currencies in context.Currency on curr.CurrencyID equals currencies.CurrencyID
										   where 
											curr.DeletedOn == null && 
											curr.ClientSiteID == SessionManager.ClientSiteID &&
											curr.IsDefault != true
										   orderby curr.InsertedOn
										   select new {
											   Name = currencies.CurrencyPossessive + " " + currencies.CurrencyName + " (" + currencies.CurrencyCode + ")",
											   CurrencyConversionID = curr.CurrencyConversionID,
											   CurrencySymbol = currencies.CurrencySymbol,
											   CurrencyCode = currencies.CurrencyCode,
											   Value = curr.Value
										   }).DefaultIfEmpty();

					Utils.ServiceSuccessResultHandler(ref data, currencyConversions, currencyConversions.Count());

			} catch (Exception exception) {
				Utils.ServiceErrorResultHandler(exception, ref data);
			}

			return data;
		}
    }
}
