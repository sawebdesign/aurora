﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Data.SqlClient;
using System.Globalization;
using Aurora.Custom.Providers.Shipping;
using Aurora.Custom.UI;
using System.Data.Common;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Net;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Aurora.Services {
	/// <summary>
	/// Summary description for Shipping
	/// </summary>
	/// 

	public struct Country {
		public long ID {get; set;}
		public string Name {get; set;}
	}

	[WebService(Namespace = "AuroraShipping")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
	public class AuroraShippingService : WebService {
		public SqlDatabase SqlDb { get; set; }
		public void CheckSqlDBConnection() {
			if (SqlDb == null)
				SqlDb = new SqlDatabase(ConfigurationManager.ConnectionStrings["Shipping"].ConnectionString);
		}

		[WebMethod]
		public Dictionary<string,object> GetCountryList() {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				List<Country> countries = null;
				CheckSqlDBConnection();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "SELECT * FROM dbo.TB_Country";
					DataSet ds = SqlDb.ExecuteDataSet(cmd);
					countries = ds.Tables[0].AsEnumerable().Select(s => new Country {
						ID = s.Field<int>("countryID"),
						Name = s.Field<string>("countryName")
					}).ToList();
				}
				Utils.ServiceSuccessResultHandler(ref data, countries, countries.Count);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod]
		public Dictionary<string, object> CalculateShippingCost(int CountryID, decimal TotalWeightInGrams) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				decimal shippingCost = 0;
				CheckSqlDBConnection();
				string sapoRegion = String.Empty;
				using (SqlCommand cmd = new SqlCommand()) {
					// Select SAPO Country Zone
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "SELECT sapo from dbo.TB_Country tc WHERE tc.countryID = @CountryID";
					cmd.Parameters.AddWithValue("@CountryID", CountryID);
					sapoRegion = (string)SqlDb.ExecuteScalar(cmd);
				}
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.Text;
					//If Local
					if (sapoRegion == "X") {
						cmd.CommandText = "SELECT * from dbo.sapoZoneSA szs where szs.Id = 1";
						DataSet ds = SqlDb.ExecuteDataSet(cmd);

						decimal standardRate = Convert.ToDecimal(ds.Tables[0].Rows[0]["oneKg"]);
						decimal perKgRate = Convert.ToDecimal(ds.Tables[0].Rows[0]["thereOf"]);
						decimal numberKgsExtra = (Math.Ceiling(TotalWeightInGrams / 1000) - 1);
						shippingCost = standardRate + (numberKgsExtra * perKgRate);
					//If International
					} else {
						cmd.CommandText = "SELECT * from dbo.sapoZone sz where sz.zone = @SapoRegion";
						cmd.Parameters.AddWithValue("@SapoRegion", sapoRegion);
						DataSet ds = SqlDb.ExecuteDataSet(cmd);
						decimal standardRate = Convert.ToDecimal(ds.Tables[0].Rows[0]["AirRate"]);
						decimal per100gramsRate = Convert.ToDecimal(ds.Tables[0].Rows[0]["AirRatePlus"]);
						decimal number100Grams = (Math.Ceiling(TotalWeightInGrams / 100));
						shippingCost = standardRate + (number100Grams * per100gramsRate);
					}
					
					

				}
				Utils.ServiceSuccessResultHandler(ref data, shippingCost, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}

		[WebMethod]
		public Dictionary<string, object> CheckIfInternational(int CountryID) {
			Dictionary<string, object> data = new Dictionary<string, object>();
			try {
				List<Country> countries = null;
				bool isInternational = true;
				CheckSqlDBConnection();
				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "SELECT * FROM dbo.TB_Country where countryID = @CountryID AND sapo = 'X'";
					cmd.Parameters.AddWithValue("@CountryID", CountryID);
					DataSet ds = SqlDb.ExecuteDataSet(cmd);
					if (ds.Tables[0].Rows.Count > 0) {
						isInternational = false;
					}
				}
				Utils.ServiceSuccessResultHandler(ref data, isInternational, 1);
			} catch (Exception ex) {
				Utils.ServiceErrorResultHandler(ex, ref data);
			}
			return data;
		}
	}
}
