﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManageLicenses.aspx.cs" Inherits="Aurora.ManageLicenses" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="scripts/aurora.custom.js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <link href="styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <script src="managelicenses.aspx.js" type="text/javascript" />
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
    <div id="Users">
        <table id="functions">
            <tr>
                <td>
                    <a href="javascript:void" onclick="clearForm();$('#contentAddEdit').dialog({modal: true,resizable: false,title: 'Add A Member',width: '800px',buttons: { 'Save': SaveUser, 'Cancel': function () { $('#contentAddEdit').dialog('close'); $('#Messages').html(''); } }});;return false;">
                        Add A User</a>&nbsp;|
                </td>
                <td>
                    <a href="javascript:void" onclick="buyMoreLicenses();">Purchase A License</a>
                </td>
            </tr>
        </table>
        <table cellpadding="10" border="0" cellspacing="0" width="100%" style="border: 1px solid grey;">
            <thead>
                <tr class="userHeader" style="background: Gray;">
                    <th>
                        Name
                    </th>
                    <th>
                        Deparment
                    </th>
                    <th>
                        License
                    </th>
                    <th>
                        Expires On
                    </th>
                    <th>
                        Function
                    </th>
                </tr>
            </thead>
            <tbody class="sys-template" id="userData">
                <tr>
                    <td>
                        {{FirstName+' '+ LastName}}
                    </td>
                    <td>
                        {{findByInArray(departmentsDS,"ID",DepartmentID) != null ? findByInArray(departmentsDS,"ID",DepartmentID).Name : 'None'
                        }}
                    </td>
                    <td>
                    </td>
                    <td>
                        {{formatDateTime(ExpiresOn)}}
                    </td>
                    <td align="right">
                        <a href="javascript:void()" sys:dataid="{{ID}}" onclick="getLicenseForMember($(this).attr('dataid'));return false;">
                            Edit</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" id="ID" />
        <input type="hidden" id="InsertedOn" />
        <input type="hidden" id="InsertedBy" />
        <input type="hidden" id="DeletedOn" />
        <input type="hidden" id="DeletedBy" />
        <input type="hidden" id="ClientSiteID" />
        <input type="hidden" id="ExpiresOn" />
        <input type="hidden" id="OrganisationID" />
        <input type="hidden" id="DepartmentID" />
        <input type="hidden" name="isDefault" value="false" id="isDefault" />
        <table cellpadding="5" cellspacing="5" border="0" width="800" id="contentAddEdit"
            style="display: none;">
            <tr>
                <td colspan="2">
                    <div id="Messages">
                    </div>
                </td>
            </tr>
            <tr style="display:none;">
                <td>
                    Active:
                </td>
                <td>
                    <input type="checkbox" id="isActive" checked="checked" />
                </td>
            </tr>
            <tr>
                <td>
                    First Name:
                </td>
                <td>
                    <input type="text" valtype="required" id="FirstName" />
                </td>
            </tr>
            <tr>
                <td>
                    Last Name:
                </td>
                <td>
                    <input type="text" valtype="required" id="LastName" />
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <input type="text" valtype="required:regex:email" id="Email" />
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <input type="password" value="" valtype="required" id="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    Confirm Password:
                </td>
                <td>
                    <input type="password" value="" valtype="required;mustmatch:Password" id="ConfirmPassword" />
                </td>
            </tr>
            <tr>
                <td>
                    Mobile:
                </td>
                <td>
                    <input type="text" runat="server" id="Mobile" />
                </td>
            </tr>
            <tr>
                <td>
                    Work Telephone:
                </td>
                <td>
                    <input type="text" runat="server" id="WorkTel" />
                </td>
            </tr>
            <tr>
                <td>
                    Private Telephone:
                </td>
                <td>
                    <input type="text" runat="server" id="PrivateTel" />
                </td>
            </tr>
            <tr>
                <td>
                    Organisation:
                </td>
                <td>
                    <input type="text" runat="server" id="Organisation" />
                </td>
            </tr>
            <tr>
                <td>
                    Occupation:
                </td>
                <td>
                    <input type="text" runat="server" id="Occupation" />
                </td>
            </tr>
            <tr>
                <td>
                    Select A License:
                </td>
                <td>
                    <select id="CurrentLicenses" class="sys-template">
                        <option sys:paymentschema="{{memberSettings.PaymentSchemaID}}"  sys:value="{{memberSettings.ID}}">{{memberSettings.Name}} {{(usersDS._data.length - license.Total) + " left"}}</option>
                    </select>
                    <a href="javascript:void(0)" onclick="renewLicense($('#ID').val());">Renew</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                </td>
            </tr>
        </table>
    </div>
    <div style="display:none;" id="Renew"> 
        <table>
            <tr>
                <td>You are about to renew a license for:</td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><label id="renewName"></label></td>
            </tr>
            <tr>
                <td>Price:</td>
                <td>
                    <label id="renewPrice">
                    </label>
                </td>
            </tr>
            
        </table>
    </div>
    <div style="display:none">
        <div id="BuyMore"></div>
    </div>
</asp:Content>
