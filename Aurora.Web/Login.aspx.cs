﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora {
    public partial class Login : Aurora.Common.BasePage {
        private Custom.Data.AuroraEntities entity = new Custom.Data.AuroraEntities();
        
        protected void Page_Load(object sender, EventArgs e) {
            Dictionary<string, string> config = Utils.GetFieldsFromCustomXML("Members");

            if (config.Count() > 0) {
                cmsZoneTop.Attributes.Add("pageid", config["Member_CMSZoneTop"]);
                cmsZoneBottom.Attributes.Add("pageid", config["Member_CMSZoneBottom"]); 
            }

            SiteMemberArea1.Visible = false;
            //user already logged in 
            if (SessionManager.MemberData == null || SessionManager.MemberData.ID <= 0) {
                SessionManager.VistorSecureSessionID = Guid.Empty;
                return;
            };
            SiteMemberArea1.Visible = true;
            SiteMemberArea1.LoadProfile();
        }

    }

}