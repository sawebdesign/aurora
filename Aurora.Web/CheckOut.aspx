﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
	CodeBehind="CheckOut.aspx.cs" Inherits="Aurora.CheckOut" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<input type="hidden" runat="server" id="total" />
	<div class="checkoutHeading">
		<h1>Check Out</h1>
	</div>
	<div id="inputcontent">
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tbody runat="server" clientidmode="Static" id="Coupon">
			<tr>
				<td colspan="2">
					<h2>Coupon</h2>
				</td>
			</tr>
			<tr>
				<td>If you have a promotion code enter it here
				</td>
				<td>
					<input type='text' id='VoucherCode' />&nbsp;<input type="button" value="Validate"
						id="Voucher" onclick="AuroraJS.Modules.validateVoucher($('#VoucherCode').val());" />
				</td>
			</tr>
			<tr>
				<td style="height: 25px;"></td>
			</tr>
		</tbody>
		<tbody runat="server" id="ShippingSection" clientidmode="Static">
			<tr>
				<td colspan="2">
					<h2>Shipping</h2>
				</td>
			</tr>
			<tr>
				<td>Select a Shipping Method:</td>
				<td>
					<select id="ShippingMethod" runat="server" clientidmode="Static"></select>
				</td>
			</tr>
		</tbody>
		<tbody runat="server" id="SAPOShipping" clientidmode="Static" style="display:none;" visible="false" class="shippingModule">
			<tr>
				<td>Country</td>
				<td>
					<select id="SAPOCountry" name="SAPOCountry"  runat="server" clientidmode="Static"></select>
				</td>
			</tr>
		</tbody>
		<tbody runat="server" id="RamShipping" clientidmode="Static" style="display:none;" visible="false" class="shippingModule">

			<tr>
				<td>Select Delivery Type:
				</td>
				<td>
					<select id="DeliveryType">
						<option value="ES">ECONOMY SERVICE (48 - 72 HRS)</option>
						<option value="XR">EXPRESS ROAD (24 - 48 HRS)</option>
						<option value="ND" selected="selected">NEXT DAY (17H00)</option>
						<option value="ND10H30">NEXT DAY (10H30)</option>
						<option value="EB08H30">EARLY BIRD (08H30)</option>
						<option value="VC">VALUABLE CARGO</option>
						<option value="SD">SAME DAY</option>
						<option value="IP">INTERNATIONAL PARCELS</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Enter Suburb name of Delivery Address:
				</td>
				<td>
					<input type="text" id="ShippingSuburb" onkeyup="AuroraJS.Modules.GetRamSuburbs();"
						onfocus="AuroraJS.Modules.GetRamSuburbs();" style="width: 270px" /><img id="suburbload"
							style="display: none; float: right; height: 20px; margin-top: 3px;" src="/styles/images/loader.gif" />
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" value="Get Quote" onclick="getQuote();" /><img id="quoteLoad"
						style="display: none; height: 20px;" src="/styles/images/loader.gif" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="QuoteField">
					</div>
					<br />
				</td>
			</tr>
		</tbody>
		<tbody id="addressDetails" style="display:none;">
			<tr>
				<td>Address Line 1</td>
				<td><input type="text" id="AddressLine1" /></td>
			</tr>
			<tr>
				<td>Address Line 2</td>
				<td><input type="text" id="AddressLine2" /></td>
			</tr>
			<tr>
				<td>Address Line 3</td>
				<td><input type="text" id="AddressLine3" /></td>
			</tr>
			<tr>
				<td>City/Town</td>
				<td><input type="text" id="City" /></td>
			</tr>
			<tr>
				<td>Post Code</td>
				<td><input type="text" id="PostalCode" /></td>
			</tr>
			</tbody>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<tbody id="CheckOutDetails">
						<tr>
							<td colspan="2">
								<h2>Check Out Details:</h2>
							</td>
						</tr>
						<tr>
							<td>Purchases:
							</td>
							<td id="STotal" align="right"></td>
						</tr>
						<tr>
							<td>VAT:
							</td>
							<td align="right" id="VAT"></td>
						</tr>
						<tr>
							<td colspan="2">
								<hr />
							</td>
						</tr>
						<tr>
							<td>Total:
							</td>
							<td align="right" id="Total"></td>
						</tr>
						<tr>
							<td style="height: 25px;"></td>
						</tr>
						<tr class="discountBox" style="display: none">
							<td>Discount:
							</td>
							<td align="right" id="VoucherDiscount"></td>
						</tr>
						<tr class="discountBox" style="display: none">
							<td colspan="2">
								<hr />
							</td>
						</tr>
						<tr class="discountBox" style="display: none">
							<td>Sub-Total:
							</td>
							<td align="right" id="VoucherTotal"></td>
						</tr>
						<tr>
							<td style="height: 25px;"></td>
						</tr>
						<tr class="shippingBox" style="display: none">
							<td>Shipping:
							</td>
							<td align="right" id="ShippingTotal"></td>
						</tr>
						<tr class="shippingBox" style="display: none">
							<td colspan="2">
								<hr />
							</td>
						</tr>
						<tr class="shippingBox" style="display: none">
							<td>Total With Shipping:
							</td>
							<td align="right" id="TShipping"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="height: 25px;"></td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" id="NoShippingPayment" onclick="AuroraJS.Modules.ProceedToPayment();"
					style="display: none;" value="Proceed To Payment" />
				<input type="button" id="IncludeShip" onclick="AuroraJS.Modules.AppendToTotal();"
					style="display: none;" value="Accept Shipping Charges?" />
			</td>
		</tr>
	</table>
	<script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
	<script src="/scripts/utils.js?" type="text/javascript"></script>
	<script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
	<script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
	<script src="/services/auroraws.asmx/js" type="text/javascript"></script>
	<script type="text/javascript" src="/services/shipping.asmx/js"></script>
	<script src="scripts/aurora.custom.js" type="text/javascript"></script>
	<script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/plugins/boltaccordion.js" type="text/javascript"></script>
	<script src="/scripts/modules.js?" type="text/javascript"></script>
	<script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
	<script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
	<script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
	<script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"	defer="defer"></script>
	<link href="styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
	<link href="styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
	<link href="styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
	<script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
	<script src="/scripts/jsrender.js?" type="text/javascript"></script>
	<script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
	<!--superfish-->
	<script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
	<script src="/scripts/supersubs.js?" type="text/javascript"></script>
	<script src="/scripts/superfish.js?" type="text/javascript"></script>
	<script src="/scripts/jquery-cookie.js" type="text/javascript"></script>
	<link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
	<link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
	<script src="checkout.aspx.js" type="text/javascript">
	</script>
	<script type="text/javascript">
		AuroraJS.Modules.Members = true;
	</script>
	
</asp:Content>
