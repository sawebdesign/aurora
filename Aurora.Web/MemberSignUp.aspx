﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MemberSignUp.aspx.cs" Inherits="Aurora.MemberSignUp" %>

<%@ Register Assembly="Aurora.Custom" Namespace="Aurora.Custom.UI" TagPrefix="cui" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" defer="async">
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
   <div id="Member">
       <table width="100%">
           <tbody>
               <tr class="Details">
                   <td colspan="2">
                       <h2>
                           Details</h2>
                   </td>
               </tr>
               <tr>
                   <td colspan="2">
                       <h2>
                           Please enter your details in the form below:</h2>
                   </td>
               </tr>
               <tr>
                   <td>
                       First Name:
                   </td>
                   <td>
                       <input runat="server" id="FirstName" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Last Name:
                   </td>
                   <td>
                       <input runat="server" id="LastName" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Password:
                   </td>
                   <td>
                       <input runat="server" id="Password" type="password" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Confirm Password:
                   </td>
                   <td>
                       <input runat="server" id="Confirm" type="password" valtype="required;mustmatch:MainContent_Password" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Description:
                   </td>
                   <td>
                       <input runat="server" id="Description" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Physical Address 1:
                   </td>
                   <td>
                       <input runat="server" id="PhysicalAddr1" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Physical Address 2:
                   </td>
                   <td>
                       <input runat="server" id="PhysicalAddr2" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Physical Address 3:
                   </td>
                   <td>
                       <input runat="server" id="PhysicalAddr3" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Physical Address 4:
                   </td>
                   <td>
                       <input runat="server" id="PhysicalAddr4" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Postal Address 1:
                   </td>
                   <td>
                       <input runat="server" id="PostalAddr1" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Postal Address 2:
                   </td>
                   <td>
                       <input runat="server" id="PostalAddr2" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Postal Address 3:
                   </td>
                   <td>
                       <input runat="server" id="PostalAddr3" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Postal Address 4:
                   </td>
                   <td>
                       <input runat="server" id="PostalAddr4" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Email1:
                   </td>
                   <td>
                       <input runat="server" id="Email1" type="text" valtype="required;regex:email" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Email2:
                   </td>
                   <td>
                       <input runat="server" id="Email2" type="text" valtype="regex:email" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Telephone 1:
                   </td>
                   <td>
                       <input runat="server" id="Tel1" type="text" valtype="required" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Telephone 2:
                   </td>
                   <td>
                       <input runat="server" id="Tel2" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Telephone 3:
                   </td>
                   <td>
                       <input runat="server" id="Tel3" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Telephone 4:
                   </td>
                   <td>
                       <input runat="server" id="Tel4" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Fax 1:
                   </td>
                   <td>
                       <input runat="server" id="Fax1" type="text" />
                   </td>
               </tr>
               <tr>
                   <td>
                       Fax 2:
                   </td>
                   <td>
                       <input runat="server" id="Fax2" type="text" />
                   </td>
               </tr>
               <tr style="display: none;">
                   <td>
                       Registration Type:
                   </td>
                   <td>
                       <select id="MemberType">
                       </select>
                   </td>
               </tr>
               <tr>
                   <td colspan="2">
                       <cui:objectpanel id="ContentPanel" visible="false" runat="server">
                                <cui:XmlPanel ID="ContentXml" runat="server">
                                </cui:XmlPanel>
                            </cui:objectpanel>
                   </td>
               </tr>
           </tbody>
       </table>
   </div>
</asp:Content>
