﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using System.Text;
using Aurora.Custom.Data;
using System.Web.UI.HtmlControls;

namespace Aurora
{
    
    public partial class Register : Aurora.Common.BasePage
    {
        public string lastModifiedDate = new FileInfo(HttpContext.Current.Server.MapPath("~/Scripts/modules.js")).LastWriteTime.ToString("ddMMyyyyHHss");
        Aurora.Custom.Data.AuroraEntities entities = new Aurora.Custom.Data.AuroraEntities();
        public string cmsZoneID;

        protected void Page_Load(object sender, EventArgs e) {
            var configData = Utils.GetFieldsFromCustomXML("Members");
            cmsZoneID = Utils.ConvertDBNull(configData.Where(cfg => cfg.Key == "Member_CMSZoneTopRegister").Select(cfg => cfg.Value).FirstOrDefault(), "0");
            if (!Page.IsPostBack) {
                MemberType.DataTextField = "Name";
                MemberType.DataValueField = "ID";
                MemberType.DataSource = from categories in entities.Role
										where categories.IsPublic == true && categories.ClientSiteID == SessionManager.ClientSiteID && categories.DeletedOn == null
                                        orderby categories.OrderIndex
                                        select categories;
                MemberType.DataBind();
                PaymentOptionType.DataTextField = "Name";
                PaymentOptionType.DataValueField = "ID";
				var paymentSource = from cschemas in entities.PaymentSchemaClientSite
									join schemas in entities.PaymentSchema on cschemas.PaymentSchemaID equals schemas.ID
									where cschemas.ClientSiteID == SessionManager.ClientSiteID && cschemas.DeletedOn == null
									select new {
										Name = schemas.Name,
										ID = schemas.ID,
										SupportsRecurring = schemas.SupportsRecurring
									};
				PaymentOptionType.DataSource = paymentSource;
                PaymentOptionType.DataBind();
                PaymentOptionType.Items.Insert(0, new ListItem("Select a Payment Option", "-1"));
				// The code below adds an additional attribute to the payment gateway drop down.
				// There may be a more efficient way of doing this.
				foreach (ListItem item in PaymentOptionType.Items) {
					var source = paymentSource.ToList().Where(w =>w.ID.ToString() == item.Value).FirstOrDefault();
					if (source != null) {
						item.Attributes.Add("supportsRecurring", source.SupportsRecurring.ToString());
					}
				}
                
                XmlDocument SchemaXml = new XmlDocument();
                var schema = (from schemas in entities.Schema
                              join featuresModule in entities.Module on schemas.ModuleID equals featuresModule.ID
                              where featuresModule.Name == "MemberManagement"
                                    && schemas.ClientSiteID == SessionManager.ClientSiteID
                              select schemas.SchemaXML).SingleOrDefault();
                if (!string.IsNullOrEmpty(schema)
                    && schema != "<XmlDataSchema><fieldcount>1</fieldcount></XmlDataSchema>"
                    && schema != "<XmlDataSchema><fieldcount>0</fieldcount></XmlDataSchema>") {

                    SchemaXml.LoadXml(schema);
                    //check for if edit

                    //Render the control and add it to the Form
                    ContentXml.XmlPanelSchema = SchemaXml;
                    ContentXml.RenderXmlControls("tbl_CustomFields");
                    ContentPanel.Visible = true;
                    ContentXml.UploaderEventHandler += UploadFile;

                    //Check if the members module has been linked to custom modules
                    long? moduleID =
                        (from modules in entities.Module
                         where modules.Name == "MemberManagement"
                         select modules.ID).FirstOrDefault();

                    var additionalData = (from customData in entities.CustomModules
                                          where customData.FeaturesModuleID == moduleID
                                          && customData.ClientSiteID == SessionManager.ClientSiteID
                                          select customData).SingleOrDefault();

                    //additonal custom data
                    ClientSiteID.Value = SessionManager.ClientSiteID.Value.ToString();
                    XmlDocument XmlAdditionalData = new XmlDocument(); //The xml data
                    XmlDocument SchemaXmlAdditional = new XmlDocument(); //the schema for the extension
                    if (additionalData != null) {
                        if (SessionManager.MemberData != null) {
                            string addData = Utils.ConvertDBNull((from customData in entities.CustomModules
                                                                  join customModuleData in entities.CustomModuleData on customData.ID equals
                                                                      customModuleData.ModuleID
                                                                  where customData.FeaturesModuleID == moduleID
                                                                        && customModuleData.SystemModuleID == SessionManager.MemberData.ID
                                                                  select customModuleData.Data).FirstOrDefault(), "<XmlData></XmlData>");

                            XmlAdditionalData.LoadXml(addData);
                        }


                        //Load the schema and data
                        ContentXmlAdditional.XmlPanelSchema = SchemaXmlAdditional;
                        //ContentXmlAdditional.XmlPanelData = XmlAdditionalData;

                        SchemaXmlAdditional.LoadXml(additionalData.XMLSchema);
                        //Render the control and add it to the Form
                        ContentXmlAdditional.RenderXmlControls("tbl_AdditionalData");
                        ContenPanelAdditional.Visible = true;

                    }
                }
            } else {
                //get member info
                long memID = long.Parse(ID.Value);

                var MemberInfo = (from members in entities.Member
                                  where members.ID == memID
                                  select members).SingleOrDefault();


                var httpContext = HttpContext.Current;
                if (Request.Files.Count > 0 && Request.Files[0].FileName != string.Empty) {
                    if (ID.Value != null) {
                        Request.Files[0].SaveAs(Utils.CreateFileNameFromID(ID.Value, ref httpContext, Request.Files[0].FileName));
                    }

                } else if (Request.InputStream.Length > 0) {
                    //if mozila

                    try {
                        byte[] buffer = new byte[1024];

                        string path = string.Empty;
                        path = Utils.CreateFileNameFromID(ID.Value, ref httpContext, "myfile.jpg");
                        string filedata = RequestInputStreamToString();

                        if (!filedata.ToUpper().Contains("__VIEWSTATE")) {
                            System.IO.File.WriteAllBytes(path, ReadToEnd(Request.InputStream));
                        }

                    } catch (Exception) {
                        //fire fox error cannot be resolved if this fails
                    }

                }
            }
        }

        protected bool retfalse() {
            return false;
        }

        protected void UploadFile(object sender, EventArgs e) {

        }

        public string RegisterMember() {
            string status = "";
            try {
                var category = (from cat in entities.Role
                                 where cat.ID == Convert.ToInt64(MemberType.Value) && cat.ClientSiteID == SessionManager.ClientSiteID
                                 select cat).FirstOrDefault();
                if (category == null) {
                    status = "Failed|No such category";
                } else {
                    var emailAlreadyUsed = ((from mem in entities.Member
                                             where mem.Email == Email.Value.Trim() && mem.ClientSiteID == SessionManager.ClientSiteID
                                             select mem).Count() > 0);
                    if (emailAlreadyUsed == true) {
                        status = "Failed|This email address has already been used to register.";
                    } else {
                        Member newMember = new Member();
                        newMember.ClientSiteID = SessionManager.ClientSiteID;
                        newMember.Email = Email.Value;
                        newMember.FirstName = FirstName.Value;
                        newMember.LastName = LastName.Value;
                        newMember.Password = Aurora.Security.Encryption.Encrypt(Password.Value);

                        //These are legacy fields. This is now handled by the MemberRole table
                        newMember.isActive = true;
                        newMember.isPending = false;
                    }
                }
            } catch (Exception ex) {
                status = "Failed|" + ex.Message;
            }
            
            return status;
        }

        public byte[] ReadToEnd(System.IO.Stream stream) {
            long originalPosition = stream.Position;
            stream.Position = 0;
            try {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length) {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1) {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            System.Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            System.Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp; totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead) {
                    buffer = new byte[totalBytesRead];
                    System.Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            } finally {
                stream.Position = originalPosition;
            }
        }

        private string RequestInputStreamToString() {
            StringBuilder sb = new StringBuilder();
            int streamLength;
            int streamRead;
            Stream s = Request.InputStream;
            streamLength = Convert.ToInt32(s.Length);
            Byte[] streamArray = new Byte[streamLength];

            streamRead = s.Read(streamArray, 0, streamLength);

            for (int i = 0; i < streamLength; i++) {
                sb.Append(Convert.ToChar(streamArray[i]));
            }

            return sb.ToString();
        }
    }
}