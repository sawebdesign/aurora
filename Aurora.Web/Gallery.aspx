﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="Aurora.Gallery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="styles/layout.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="styles/jd.gallery.css" type="text/css" media="screen"
        charset="utf-8" />
    <script src="scripts/mootools-1.2.1-core-yc.js" type="text/javascript"></script>
    <script src="scripts/mootools-1.2-more.js" type="text/javascript"></script>
    <script src="scripts/microsoftajax.debug.js" type="text/javascript"></script>
    <script src="scripts/microsoftajaxtemplates.debug.js" type="text/javascript"></script>
    <script src="services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="scripts/jd.gallery.js" type="text/javascript"></script>
    <script src="scripts/gallery.js" type="text/javascript"></script>
    <script src="scripts/jd.gallery.set.js" type="text/javascript"></script>
    <%if (Request["response"] == "404")
          ClientScript.RegisterStartupScript(this.GetType(),"404"+DateTime.Now.Ticks.ToString(),"<script>alert('The file you have requested was not found.');</script>");     
     %>
    <%if(Request["gallerytype"] =="audio")
    {%>
    <script src="scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
      
    <%}%>
    <script type="text/javascript">
    <%if (Request["gallerytype"] == "image")
    {	%>
    LoadGallery("Images");
    <%  
    } 
    else if(Request["gallerytype"] =="audio")
    {%>

      LoadGallery("Audio");
   <%  
    } 
    else if(Request["gallerytype"] =="video")
    {%>
        LoadGallery("Video");
    <%}%>
       
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%
        if (Request["gallerytype"] == "image" || Request["gallerytype"] == "video")
        {%>	
  <div class="content">
            <div id="myGallerySet">
               
            </div>
        </div>
        <div id="videos"></div>
<%
        }
        else if (Request["gallerytype"] == "audio")
        {
        %>
        <div style="position:absolute;left:0px;top:0px;z-index:-10;" id="audio"></div>
<%
        }
%>
    </form>
</body>
</html>
