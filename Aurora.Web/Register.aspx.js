﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx" />
/// <reference path="scripts/microsoftajax.vsextensions.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="scripts/fancybox/jquery.fancybox-1.3.0.js" />
/// <reference path="/Scripts/modules.js" />


(function ($, window, document, undefined) {

	var regMethods = {
		init: function (categoryID) {
			if ($('#MemberType option').length > 1) {
				$("#MemberType").parent().parent().show();
			}

			$('#MemberType').change(regEvents.changeCategory);

			if (categoryID) {
				regMethods.getCategoryInfo(categoryID);
			};

			if (AuroraJS.Bespoke.AfterRegistrationFormRender) {
				AuroraJS.Bespoke.AfterRegistrationFormRender();
			}

			if (AuroraJS.Bespoke.Register) {
				AuroraJS.Bespoke.Register();
			} else {
				$('#Lock').QapTcha({
            	    buttonLock: "#btnProcessOrder",
            	    buttonFunc: regMethods.RegisterUser
                });

			    if (!$("#RegistrationForm").is(":visible")) {
				    $("#Lock").hide();
			    }

			}

			$('input[type=file]').each(function () {
				var $this = $(this);
				var $container = $("<div class='uploader'></div>");
				$container.fineUploader({
					request: {
						endpoint: '/httphandlers/uploadfile.ashx',
						params: {
							fileID: $this.attr('id')
						}
					},
					multiple: false,
					validation: {
						allowedExtensions: ['jpeg', 'jpg', 'png'],
						sizeLimit: 153600
					}
				}).bind('submit', function (event, id, filename) {
					$(event.currentTarget).fineUploader('setParams', {
						name: filename
					});
				}).bind('complete', function (event, id, filename, response) {

				});
				$(this).replaceWith($container);
			});


		},
		getCategoryInfo: function (categoryID) {
			Aurora.Services.AuroraWebService.GetMembershipCategoryInformation(categoryID, regEvents.getCategoryInfoSuccess, onError);
		},
		renderCategoryInfo: function (category) {
			var $container = $('#CategoryInfo .infopanel')
			var infoTable = "";
			if (AuroraJS.Templates.CategoryInformationTemplate) {
				infoTable = $(AuroraJS.Templates.CategoryInformationTemplate.render(category));
			} else {

				//js session activation for js render methods
				$.views.helpers({ GetJSSession: GetJSSession });

				tmp = $.templates('<table>' +
						'<tr>' +
							'<td>Type:</td>' +
							'<td>{{:Name}}</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Duration:</td>' +
							'<td>{{: (RoleDuration > 0 ? RoleDuration + " days" : "Forever") }}</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Cost:</td>' +
							'<td>{{if Cost > 0}}' +
									'{{:~GetJSSession("Currency_Symbol")}} {{: (Cost.toFixed(2))}} ({{:~GetJSSession("Currency_Code")}})' +
									'{{if IsRecurring}}' +
										' (Monthly)' +
									'{{/if}}' +
									'<div class="conversions" value="{{:Cost}}"></div>' +
								'{{else}}' +
								'Free' +
								'{{/if}}' +
							'</td>' +
						'</tr>' +
					'</table>');
				infoTable = tmp.render(category);
			}
			$container.html(infoTable);
		},
		RegisterUser: function () {
			if (validateForm(undefined, "", "", false, undefined, "#RegistrationForm")) {
				$("<div>Please review the form as some required fields have not been entered.</div>").dialog({
					modal: true,
					resizable: false,
					title: "Error",
					buttons: { "Okay": function () { $(this).dialog("close") } }
				});
				return;
			} else {
				var memberObject = {};//new Aurora.Custom.Data.Member();
				$('#CompanyDetails').mapToObject(memberObject);

				var $customFields = $('#tbl_CustomFields').find(':input');
				var $additionalFields = $('#tbl_AdditionalData').find(':input');
				var additionalXML;

				memberObject.InsertedOn = new Date();
				memberObject.DeletedOn = null;
				memberObject.DeletedBy = null;
				memberObject.LastUpdated = new Date();

				if ($customFields.length > 0) {
					memberObject.CustomXML = buildCustomXMLBySelector($customFields, "value", true);
				} else {
					memberObject.CustomXML = null;
				}
				if ($additionalFields.length > 0) {
					additionalXML = buildCustomXMLBySelector($additionalFields, "value");
				} else {
					additionalXML = "<XmlData><Fields></Fields></XmlData>";
				}
				var paymentSchemaID;
				if ($('#PaymentOptions').is(':visible')) {
					paymentSchemaID = $('#PaymentOptionType').val();
				} else {
					paymentSchemaID = -1;
				}
				$('#RegistrationForm').hide();

				Aurora.Services.AuroraWebService.CreateMember(memberObject, memberObject.MemberType, additionalXML, paymentSchemaID, regEvents.RegisterUserSuccess, regEvents.RegisterError);
			}
		}
	};

	var regEvents = {
		changeCategory: function (event) {
			$this = $(event.currentTarget);
			$('#CategoryInfo').addClass('loading');
			regMethods.getCategoryInfo($this.val());
		},
		getCategoryInfoSuccess: function (response) {
			if (response.Count === 1) {
				$('#CategoryInfo').removeClass('loading');
				regMethods.renderCategoryInfo(response.Data);
				if ($.CurrencyConverter) {
					$.CurrencyConverter($('#CategoryInfo .conversions'));
				}
				if (response.Data.Cost > 0) {
					$('#PaymentOptions').show()
					$('#PaymentOptionType').attr('valtype', 'required');
					if (response.Data.IsRecurring) {
						var $options = $('#PaymentOptions option').filter(':not([supportsrecurring])');
						$options.hide().attr('disabled', true);
					} else {
						$('#PaymentOptions option').show()
					};
				} else {
					$('#PaymentOptions').hide()
					$('#PaymentOptionType').removeAttr('valtype');
				}
			} else {
				onError(response);
			}
		},
		RegisterUserSuccess: function (response) {
			if (response.Result === true) {
				$.auroraRegister.member = response.Data;
				switch (response.Count) {
					case -1:
						$('<div>The email you provided has already been used to register an account. Please log in to update your membership</div>').dialog({
							modal: true,
							resizable: false,
							title: "Error",
							buttons: {
								"Close": function () {
									$(this).dialog('close');
									$('#RegistrationForm').show();
								}
							}
						});
						break;
					case 1:
						$('<div>Your registration was successful.</div>').dialog({
							modal: true,
							title: "Congratulations",
							resizable: false,
							buttons: {
								"Close": function () {
									$(this).dialog('close');
								}
							},
							close: function () {
								document.location = '/login.aspx';
							}
						});
						break;
					case 2:
						$('<div>Your account has been successfully created. A site adminsitrator will activate your account shortly.</div>').dialog({
							modal: true,
							resizable: false,
							title: "Congratulations",
							buttons: {
								"Close": function () {
									$(this).dialog('close');
								}
							},
							close: function () {
								document.location = '/login.aspx';
							}
						}
                            );
						break;
					case 3:
						var $paymentDialog = $('<div>Your account has been created. We will now redirect you to payment processing.</div>').dialog({
							modal: true,
							resizable: false,
							close: function () {
								document.location = "/processpayment.aspx";
							},
							buttons: {
								"Close": function () {
									$(this).dialog('close');
								}
							}
						});

						break;
					default:
						var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
						$(htmlMessage).dialog({
							modal: true,
							resizable: false,
							title: "Error",
							buttons: { "Okay": function () { $(this).dialog("close"); $('#RegistrationForm').show(); } }
						});
				}
			} else {
				var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
				$(htmlMessage).dialog({
					modal: true,
					resizable: false,
					title: "Error",
					buttons: { "Okay": function () { $(this).dialog("close"); $('#RegistrationForm').show(); } }
				});
				console.log(response.ErrorMessage);
			}
		},
		RegisterError: function (response) {
			var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
			$(htmlMessage).dialog({
				modal: true,
				resizable: false,
				title: "Error",
				buttons: { "Okay": function () { $(this).dialog("close"); $('#RegistrationForm').show(); } }
			});
			console.log(response);
		}
	}

	$.auroraRegister = function (method) {
		// Method calling logic
		if (regMethods[method]) {
			return regMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return regMethods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.auroraRegister');
		}
	};

	$.auroraRegister.member = {};

	$(function () {
		$.auroraRegister('init', $("#MemberType").val());
	});

})(jQuery, window, document);