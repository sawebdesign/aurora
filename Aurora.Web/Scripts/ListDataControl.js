﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="modules.js" />


AuroraJS.Modules.ListData = function (objectParams) {
    //create session varibles
    SetJSSession("ListDataSource", null);
    SetJSSession("ListTotalCount", 0);
    SetJSSession("ListCurrentPage", 0);

    if (objectParams) {
        //Load template into page
        if (!objectParams.moduleName || !objectParams.moduleHTML) {
            return;
        }
       
        var data = null;


        for (var item in GetJSSession("Modules")) {
            if (GetJSSession("Modules")[item].featureModules.Name == objectParams.moduleName) {
                data = GetJSSession("Modules")[item];
                break;
            }
        }
        var listTemplate = data.clientModules.HTML == null
                ? data.featureModules.HTML
                : data.clientModules.HTML;

        //$("#inputcontent").html(listTemplate);

        $("#"+objectParams.moduleName).each(
        function () {
            //get the module name from the template attribute
            var moduleName = objectParams.moduleName;
            $("#" + objectParams.moduleName).attr("class", "sys-template");
            $("#" + objectParams.moduleName).attr('sys:attach', 'dataview');
            $("#" + objectParams.moduleName).attr("id", "Lister_Data");
                        //temporarily store the current item in a temp variable.
            var itemStore = this;

            //check which module to load.
            //If module data is stored in JSSession then load from there else make a new call.
            if (moduleName != null) {
                getTemplateData(moduleName);
            }
            else {
                $(itemStore).hide();
            }
        }
        );
    }
}



function getTemplateData(moduleName) {
    var getData = function (result) {
        SetJSSession("ListDataSource", result.Data);
        if (result.Data.indexOf("NewDataSet") > -1) {
            result.Data = $.parseJSON(result.Data);
            result.Data = result.Data.NewDataSet.Table;
            $("#" + objectParams.moduleName + " a").click(
                function () {

                }
            );
        }
        var dataSet = null;

        dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get("Lister_Data"));
        dataSet.set_data(result.Data);
        AuroraJS.Modules.DecodeHTML("Lister_Data");
    }

    switch (moduleName.replace("ListDetail","")) {
        case "News":
            Aurora.Services.AuroraWebService.GetNews(getData, onError);
            break;
        case "Event":
            Aurora.Services.AuroraWebService.GetEvents(0, false, 0, 0, false, "", getData, AuroraJS.onError);
            break;
        case "Testimonies":
            Aurora.Services.AuroraWebService.GetFeedback(10,1,0,"",getData,onError);
            break;

    }
    //make service call

}

function populateList(result) {

}

