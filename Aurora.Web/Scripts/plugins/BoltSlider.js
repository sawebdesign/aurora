﻿/*

	Aurora Slider
	Author: Garth
	2014/01/30

	See Gerard, I'm trying to write more comments :)

*/
var deferal;
(function ($, window, document, undefined) {

	var sources = {
		//Stores arrays of resources to load for each slider
		FlexSlider: ['/Scripts/jquery.flexslider.js', '/Styles/flexslider.css']
	};
	var templates = {
		FlexSlider: {
			base: "<div class=\"flexslider\"><ul class=\"slides\">{{for data}}<li>{{:#data}}</li>{{/for}}</ul></div>",
			galleryimage: "<img src=\"/ClientData/" + clientSiteID + "/Uploads/gallery_01_{{>ID}}.jpg\"/>"
		}

	};

	var methods = {
		init: function (options) {
			// Initialises sliders passed in as jquery objects.
			return this.each(function () {
				var $this = $(this);
				$this.BoltSlider('load');
			});
		},
		load: function () {
			return this.each(function () {
				var $this = $(this);
				if (!$this.attr('init')) {
					$this.attr('init', true);
					//Obtain the html set options
					var htmlOptions = {
						source: $this.attr('data-source'),
						slider: $this.attr('data-slider'),
						animation: $this.attr('data-animation'),
						galleryinclude: ($this.attr('data-galleryinclude') || "").split('|'),
						galleryexclude: ($this.attr('data-galleryexclude') || "").split('|'),
						showcontrols: $this.attr('data-showcontrols')
					};

					var dfdList = [];
					var options = $.extend({}, $.fn.BoltSlider.options, htmlOptions, options);
					$this.data('BoltSliderOptions', options);
					//first confirm that the slider type exists. If not, log and return.
					if (!templates[options.slider]) {
						console.log("No templates were found for " + options.slider);
						return;
					}

					//Check whether scripts have already been loaded or begun loading.
					if ($("#" + options.slider + "Scripts").length > 0) {
						//If they aren't done yet, attach to their deferred object and wait.
						deferal = $("#" + options.slider + "Scripts").data('deferal');
						deferal.then(function () {
							methods._loadData($this, true)
						});
					} else {
						//Scripts have not been loaded, so add the sources to the deferred list
						dfdList = dfdList.concat(methods._loadSources(options.slider));
						dfdList = dfdList.concat(methods._loadData($this));
					}

					//If there are pending ajax loads, wait for the load. Store the deferal on the container
					if (dfdList.length > 0) {
						$("#" + options.slider + "Scripts").data('deferal', $.when.apply($, dfdList).then(function (e) {
							$("#" + options.slider + "Scripts").attr('loaded', true);
							$this.BoltSlider('render');
						}));
					}
				}
			});
		},
		render: function () {
			return this.each(function () {
				var $this = $(this);
				var options = $this.data('BoltSliderOptions');
				if (options.data.length > 0) {
					var $base = $($.templates(templates[options.slider].base).render(options));
					$this.html($base);

					if (options.slider === "FlexSlider") {
						options.controlNav = (options.showcontrols == "true");
						if ($base.flexslider) {
							$base.flexslider(options);
						} else {
							console.log('Boltslider detected the script has not loaded yet. Waiting...');
							var interval = setInterval(function () {
								if ($base.flexslider) {
									console.log('Executing delayed render for bolt-flexslider');
									clearInterval($base.data('boltslider-interval'));
									$base.flexslider(options);
								} else {
									console.log('Waiting more...');
								}
							}, 250);
							$base.data('boltslider-interval', interval);
						}
					} else if (options.slider === "bxSlider") {
						$base.bxSlider(options);
					}

				} else {
					console.log("Nothing to render for slider " + options.slider);
				}
			});
		},
		_loadSources: function (slider) {
			var dfdList = [];
			//Create a script holding div
			var $sourceHolder = $('<div id="' + slider + 'Scripts" style="display:none"></div>');
			$sourceHolder.appendTo('body');

			//for each source, add a tag and load
			for (var i = 0; i < sources[slider].length; i++) {
				var sourceType = sources[slider][i].split('.').pop().toLowerCase();
				var $container;
				switch (sourceType) {
					case 'js':
						// IE and Jquery don't like script tags being inserted, so this bypasses that.
						var script = document.createElement('script');
						script.type = 'text/javascript';
						$sourceHolder.get(0).appendChild(script);
						$container = $(script);
						break;
					case 'css':
						$container = $('<style type="text/css"/>');
						$sourceHolder.append($container);
						break;
					default:
						$container = $('<div/>');
						break;
				}

				dfdList.push($container.load(sources[slider][i]));
			}
			return dfdList;
		},
		_loadData: function ($this, scriptsLoaded) {
			var dfd = $.Deferred();
			var options = $this.data('BoltSliderOptions');
			options.deferal = dfd;
			options.scriptsLoaded = scriptsLoaded;
			$this.data('BoltSliderOptions', options);
			//Locate source
			if (options.source.toLowerCase() === "html") {
				//If source is HTML, grab lis and map them to data, then trigger render.
				options.data = $this.find('li').map(function () {
					return $(this).html();
				});
				$this.data('BoltSliderOptions', options);
				if (scriptsLoaded) {
					$this.BoltSlider('render');
				}
				dfd.resolve();
			} else if (options.source.toLowerCase() === "gallery") {

				//load gallery information from aurora.
				Aurora.Services.AuroraWebService.GetGalleriesForCategoriesString(options.galleryinclude, options.galleryexclude, events.loadDataSuccess, events.loadDataFailed, {tag: $this, options: options});
			}

			return dfd;
		}
	};
	events = {
		loadDataSuccess: function (result, context) {
			var options = context.options;

			if (result && options) {
				var data = [];
				for (var i = 0; i < result.Data.length; i++) {
					data.push($.templates(templates[options.slider].galleryimage).render(result.Data[i]));
				}
				options.data = data;
				context.tag.data('BoltSliderOptions', options);
				if (options.scriptsLoaded) {
					context.tag.BoltSlider('render');
				}
				options.deferal.resolve();
			}

		},
		loadDataFailed: function (e, context) {
			context.fail();
		}
	}
	$.fn.BoltSlider = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.BoltSlider');
		}
	};

	$.fn.BoltSlider.options = {
		slider: "FlexSlider",
		source: "HTML",
		animation: "slide",
		showcontrols: true
	};

	AuroraJS.plugins.BoltSlider = function () {
		$('.BoltSlider').BoltSlider();
	};

})(jQuery, window, document);
