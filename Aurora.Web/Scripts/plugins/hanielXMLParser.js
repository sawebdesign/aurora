﻿(function ($, window, document, undefined) {
	var templates = {
		packages: $.templates('<div style="width:150px;float:left;margin-right: 10px;margin-bottom: 10px;min-height: 200px;{{if TITLE ==""}}display:none;{{/if}}" id="package_{{:(#index)}}">' +
								'<div style="min-height: 300px">'+
									'<div style="min-height: 115px;"><img width="150" src="{{attr:~strClean(IMG)}}" /></div>'+
									'<div style="min-heights: 180px;">'+
									'	<div class="heading" style="padding-top:0;">{{attr:~strClean(TITLE)}}</div>' +
									'	<div class="destination">Destination: {{attr:DESTINATION}}</div>'+
									'	<div class="price">Priced From: {{attr:PRICEDFROM}}</div>'+
									'	<div class="expiryDate">Expiry Date: {{attr:~strClean(EXPDATE)}}</div>' +
									'	<div class="includes" style="display:none">{{attr:INCLUDES}}</div>' +
									'	<div class="info" STYLE="display:none;">{{attr:INFO}}</div>' +
									'</div>' +
								'</div>' +
								'<div>' +
								'	<a class="viewproductFromXML button" data-id="package_{{:#index}}" href="javascript:void(0);">READ MORE</a>' +
								'</div>' +
							'</div>'+
							'{{if (#index+1)%4==0}}<div style="clear:both"></div>{{/if}}'),
		packageDetail : $.templates('<a class="productDetailBack button">BACK</a><BR/>'+
						'<table style="width: 700px" colspacing="0" cellpadding="5">'+
						'<tr><td colspan="2"><div style="width:700px;"><img src="{{attr:IMG}}" /></div></td></tr>' +
						'<tr><td>Title:</td><td>{{attr:~strClean(Title)}}</td></tr>' +
						'<tr><td></td><td>{{attr:Destination}}</td></tr>' +
						'<tr><td valign="top">Includes:</td><td>{{attr:Includes}}</td></tr>' +
						'<tr><td valign="top">Info:</td><td>{{attr:Info}}</td></tr>' +
						'<tr><td valign="top"></td><td>{{attr:ExpiryDate}}</td></tr>' +
						'<tr><td></td><td>{{attr:Price}}</td></tr>' +
						'</table>'),
		airfares: $.templates('<div style="min-height: 95px;width: 150px;float: left;margin-right: 10px;padding-bottom: 10px;margin-bottom: 15px;border-bottom: solid 1px #E5B02B;">'+
					'<div class="heading">City: {{attr:CITY}}</div>'+
					'<div>Johannesburg: {{attr:EXJNB}}</div>'+
					'<div>Cape Town: {{attr:EXCPT}}</div>'+
					'<div>Durban: {{attr:EXDUR}}</div>'+
					'</div>{{if (#index+1)%4==0}}<div style="clear:both"></div>{{/if}}')
	};

	$.views.helpers({
		strClean: function (val) {
			return val.replace(/[\\]/g, "");
		}
	});

	var methods = {
		init: function () {
			var type = $(this).attr("data-type");
			var xml = $(this).attr("data-xml");
			var n = parseInt($(this).attr("data-n"),10);

			if (n <= 0) {
				n = 10;
			}

			var container = this;
			var success = function(data){
				var json = $.parseJSON(data);
				title = "<h2 style='clear:both;'>" + (container).attr("data-title") + "</h2>";
				contentArea = "<div class=\"contentArea\"></div>";
				if (type == "airfares") {
					html = templates.airfares.render(json.AIRFARES.FLIGHT);
				} else if (type == "packages") {
					html = templates.packages.render(json.TRAVELPACKAGES.PACKAGE);
				}
				html = title + contentArea +"<div class=\"dataArea\">"+ html+"</div>";
				$(container).html(html);
				events.bind();
			};

			Aurora.Services.AuroraWebService.LoadURLXML(xml, n, success, function () { alert("unable to load xml"); },$(this));
			
			//allow for chaining
			return $(this);
		},
	};

	var events = {
		bind: function () {
			$(".viewproductFromXML").click(events.viewProductClick);
		},
		viewProductClick: function (e) {
			e.preventDefault();
			var $this = $(this);
			var id = $(this).attr("data-id");
			var $container = $("#" + id);
			var $parent = $container.parent();

			while (!$parent.hasClass("HanielXMLParser")) {
				$parent = $parent.parent();
			}

			//find the contentArea
			var $contentArea = $parent.find(".contentArea");
			var $dataArea = $parent.find(".dataArea");

			//hide the data area
			$dataArea.hide();

			//add the package data to the contentArea and show it()!
			img = $container.find("img");
			imgHTML = img.attr("src");
			var packageDetail = {};
			packageDetail.Title = $container.find(".heading").html();
			packageDetail.IMG = imgHTML;
			packageDetail.Destination = $container.find(".destination").html();
			packageDetail.Includes = $container.find(".includes").html();
			packageDetail.Info = $container.find(".info").html();
			packageDetail.ExpiryDate = $container.find(".expiryDate").html();
			packageDetail.Price = $container.find(".price").html();
			var newHTML =  templates.packageDetail.render(packageDetail);

			$contentArea.html(newHTML).show();

			$('html, body').animate({
				scrollTop: $contentArea.offset().top
			}, 2000);

			$(".productDetailBack").click(function (e) {
				e.preventDefault();

				$contentArea.hide();
				$dataArea.show();
			});

		}
	};
	

	$.fn.HanielXMLParser = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.MemberLogin');
		}
	};

	AuroraJS.plugins.HanielXMLParser = function () {
		$('.HanielXMLParser').each(function (i, o) {
			$(this).HanielXMLParser();
		});
	};
})(jQuery,window,document);