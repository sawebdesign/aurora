﻿/*
	Membership Login
	Aurora Plugin
	Author: Gerard
	Creation Date: 2013/9/13

*/
(function ($, window, document, undefined) {

	var methods = {
		init: function () {
			//check if templates exist, if not render into page
			if ($('.scriptTemplates.MemberLogin').length === 0) {
				$('body').append('<div style="display:none" class="scriptTemplates MemberLogin">' + AuroraJS.Modules.GetTemplate("MemberLogin") + '</div>');
			}
			
			var $this = $(this);

			if ($this.length > 0) {
				//check if it has been initialized already
				if (!$this.attr('init')) {
					$this.attr('init', true);
					//call function to check if the member is logged in
					Aurora.Services.AuroraWebService.MemberLoggedIn(events.isMemberLoggedInSuccess, events.getFailed, $this);
				}
			} else {
				console.log("Please include a container element with and id of \"MemberLoginUI\". Required by jquery.memberlogin auroa plugin.");
			}

			return $this;
		},
		renderLoginForm: function ($container) {
			$container.html($('#memberLoginForm-tmpl').render({}));
			$container.find("input[type=button]").click(function () {
				Aurora.Services.AuroraWebService.LoginMemberUser($container.find("input[name=UserName]").val(), $container.find("input[name=Password]").val(), events.validateMember_Success, events.getFailed, $container);
			});
		},
		renderLoader: function ($container) {
			$container.html($('#memberLoginLoader-tmpl').render({}));
		},
		renderLoggedInWidget: function (data, $container) {
			//get the member menu
			$container.html($('#memberLoginWidget-tmpl').render(data));
			//check if we must render the menu
			if (GetJSSession("MemberLogin_BuildMenu") === "true") {
				Aurora.Services.AuroraWebService.GetMemberMenu(events.getMemberMenu_success, events.getFailed, $container);
			}
		}
	};

	var utils = {
		generateMenuLinks: function (pagesArr, $container) {

			var tmpBuildUL = $("<ul></ul>");

			for (var ii = 0; ii < pagesArr.length; ii++) {
				var tmpBuildLI = $("<li></li>");

				tmpBuildLI.append("<a href='javascript:void(0);' onclick='AuroraJS.Modules.GetContentForModule(\"Page\"," + pagesArr[ii].ID + ",\"" + utils.replaceChars(pagesArr[ii].ShortTitle) + "\"); return false;'>" + pagesArr[ii].ShortTitle + "</a>");
				//get the children, if there are any
				var currentChildren = utils.getSubPages(pagesArr[ii].ID);

				if (currentChildren.length > 0) {
					//if there are children intiate recursive call
					utils.generateMenuLinks(currentChildren, tmpBuildLI);
				}

				//append the resultant menu item to the container ul
				tmpBuildUL.append(tmpBuildLI);
			}
			//append to the container which was passed through
			$container.append(tmpBuildUL);
		},
		getSubPages: function (parentID) {

			var tmpChildArr = [];
			//get all children of the current page
			for (var ci = 0; ci < utils.currentRootMenu.length; ci++) {
				if (parseInt(utils.currentRootMenu[ci].ParentID, 10) === parseInt(parentID, 10)) {
					tmpChildArr.push(utils.currentRootMenu[ci]);
				}
			}

			return tmpChildArr;
		},
		getRootPages: function () {
			var tmpRootPages = [];
			//get all root pages
			for (var i = 0; i < utils.currentRootMenu.length; i++) {
				if (parseInt(utils.currentRootMenu[i].ParentID) === 0) {
					tmpRootPages.push(utils.currentRootMenu[i]);
				}
			}

			return tmpRootPages;
		},
		getCurrentRootMenu: function () {
			return utils.currentRootMenu;
		},
		replaceChars: function (_inString) {
			return _inString.replace(/[^a-zA-Z0-9]/g, "-");
		},
		currentRootMenu: []
	};

	var events = {
		isMemberLoggedInSuccess: function (result, $context) {
			//check if member is logged in, if not render form, otherwise render logged in ui
			if (!result.Data) {
				//not logged in, render form
				methods.renderLoginForm($context);
			} else {
				//logged in render space to attach menu
				methods.renderLoggedInWidget(result, $context);
			}
		},
		validateMember_Success: function (result, $context) {
			if (result.Data) {
				location.reload();
			} else {
				
				$("<p>The username or password you entered is incorrect.</p>").dialog({
					modal: true,
					width: 300,
					resizable:false,
					title: "Login Error",
					buttons: {
						"Close": function () {
							$(this).dialog("close");
						}
					}
				});
			}
		},
		getMemberMenu_success: function (result, $context) {
			//generate the html
			var $menuContainer = $("<div id='memberMenuContainer'></div>");
			
			if (result.MemberMenus !== null) {
				if (result.MemberMenus.length > 0) {

					for (var i = 0; i < result.MemberMenus.length; i++) {
						//store the current menu hierarchy
						utils.currentRootMenu = result.MemberMenus[i].menu;

						var tmpCurrentUL = $("<ul></ul>");
						var tmpCurrentLI = $("<li></li>");

						if (GetJSSession("MemberLogin_ApplySuperfishStyles") === "true") {
							tmpCurrentUL.addClass("sf-menu sf-vertical sf-js-enabled");
						}

						tmpCurrentLI.append("<a href='javascript:void(0);'>" + result.MemberMenus[i].name + "</a>");
						
						//now call recursive function to populate links
						utils.generateMenuLinks(utils.getRootPages(), tmpCurrentLI);

						//append content to containers
						tmpCurrentUL.append(tmpCurrentLI);
						$menuContainer.append(tmpCurrentUL);
					}
				}
			}

			//bind to the element specified in the config
			var $elementToBindTo = $(GetJSSession("MemberLogin_AttachMemberMenuTo"));

			if ($elementToBindTo.length > 0) {
				$elementToBindTo.append($menuContainer);
			} else {
				console.log("Aurora_MemberLogin: Attempting to attach the menu to an element that does not exist. " + GetJSSession("MemberLogin_AttachMemberMenuTo") + " does not match any elements.");
			}
		},
		getFailed: function (ex) {
			console.log(ex);
		}
	};

	$.fn.MemberLogin= function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.MemberLogin');
		}
	};

	AuroraJS.plugins.MemberLogin = function () {
		$('#MemberLoginUI').MemberLogin();
	};

})(jQuery, window, document);





