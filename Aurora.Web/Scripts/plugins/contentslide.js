﻿(function ($, window, document, undefined) {

	var methods = {
		init: function (options) {
			return this.each(function () {
				var $this = $(this);
				if (!$this.data("ContentSliderOptions")) {
					//Obtain the html set options
					var htmlOptions = {
						source: $this.attr('data-source'),
						itemSelector: $this.attr('data-itemselector'),
						itemHeight: $this.attr('data-itemheight'),
						numberToDisplay: $this.attr('data-numbertodisplay'),
						timer: $this.attr('data-timer')
					};
					var localOptions = $.extend({}, $.fn.BoltContentSlider.options, htmlOptions, options);
					localOptions.itemList = $this.find(localOptions.itemSelector);
					if (localOptions.itemList.length > 0) {
						localOptions.numberOfItems = localOptions.itemList.length;
						localOptions.position = 0;
						
						$this.data("ContentSliderOptions", localOptions);

						$this.BoltContentSlider('start');
					} else {
						console.log("No items were found to slide");
					}
				}
			});
		},
		start: function () {
			return this.each(function () {
				var $this = $(this);
				var $viewport = $('<div class=viewport></div>');
				var options = $this.data("ContentSliderOptions");
				$viewport.width($this.width());
				if (options.itemHeight === "auto") {
					var height = 0;
					for (var i = 0; i < options.numberToDisplay; i++) {
						height += options.itemList.eq(i + options.position).outerHeight();
					}
					$viewport.height(height);
				} else {
					$viewport.height(options.itemHeight * options.numberToDisplay);
				}
				$viewport.css({
					"position": "relative",
					"overflow": "hidden"
				});
				$this.wrap($viewport);
				$this.css({
					"position": "absolute",
					"top" : "-" + (options.itemList.eq(0).position().top) + "px"
				});
				options.viewport = $this.parent();
				$this.data("ContentSliderOptions", options);
				setTimeout(function () {
					$this.BoltContentSlider('slide');
				}, options.timer);
				
			});
		},
		slide: function () {
			return this.each(function () {
				var $this = $(this);
				var options = $this.data("ContentSliderOptions");

				options.position = options.position + options.numberToDisplay;
				if (options.position >= options.numberOfItems) {
					options.position = 0;
				}
				
				if (options.itemHeight === "auto") {
					var height = 0;
					for (var i = 0; i < options.numberToDisplay; i++) {
						if (options.itemList.eq(i + options.position).length !== 0) {
							console.log(options.itemList.eq(i + options.position).outerHeight());
							height += options.itemList.eq(i + options.position).outerHeight();
							console.log("total " + height);
						}
					}
					
					options.viewport.animate({ "height": height + "px" }, options.animationSpeed);
				} else {
				}
				
				var offset = "-" + (options.itemList.eq(options.position).position().top) + "px";
				$this.animate({ top: offset }, options.animationSpeed);
				setTimeout(function () {
					$this.BoltContentSlider('slide');
				}, options.timer);
				
			});
		}
	};

	var events = {

	};

	$.fn.BoltContentSlider = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.BoltContentSlider');
		}
	};

	$.fn.BoltContentSlider.options = {
		itemSelector: 'li',
		itemHeight: 'auto',
		numberToDisplay: 1,
		timer: 5000,
		animationSpeed: 2000
	};

})(jQuery, window, document);





