﻿/*
	Bolt Slider
	Author: Garth
	2014/09/05
*/

(function ($, window, document, undefined) {

	var methods = {
		init: function (options) {
			return this.each(function () {
				var $this = $(this),
					data = $this.data('boltaccordion');
				
				if (!data) {
					options = $.extend({}, $.fn.BoltAccordion.options, options);
					options.displayMode = $this.attr('data-displaymode') || options.displayMode;
					options.headerClass = $this.attr('data-headerclass') || options.headerClass;
					var headerSelector = (options.headerClass) ? '.' + options.headerClass : 'h3';
					options.$headers = $this.children(headerSelector);
					options.$headers.addClass('boltaccordion-header');
					var $other = $this.children().not(headerSelector);
					$other.addClass('boltaccordion-content');
					$other.hide();
					$this.data('boltaccordion', options);
					if (options.$headers.has('.boltaccordion-active').length > 0) {
						methods.toggleContainer.call($this, options.$headers.has('.active'));
					} else {
						methods.toggleContainer.call($this, options.$headers.eq(0));
					}

					$this.delegate('.boltaccordion-header', 'click', $this, Events.clickHeader)
				}
			});
		},
		options: function (options) {
			var $this = $(this),
				data = $this.data('boltaccordion');
			if (!data) {
				methods.init.call($this);
			} else {
				$this.data('boltaccordion', $.extend(data, options));
			}
			return data;

		},
		toggleContainer: function ($header, options) {
			var options = methods.options.call($(this))
			var $accordion = $(this);
			if ($header.length > 0 && options.displayMode.toLowerCase() == 'single') {
				$header = $header.eq(0);
			}
			$header.each(function () {
				var $this = $(this),
					$container = $this.next();
				if (!$accordion.hasClass('animating')) {
					$accordion.addClass('animating');
					if (options.displayMode.toLowerCase() == 'single') {
						options.$headers.removeClass('boltaccordion-active');
						$accordion.find('.boltaccordion-content:visible').not($container).slideToggle();
					}
					$header.toggleClass('boltaccordion-active', !$container.is(':visible'));
					$container.slideToggle(function () {
						$accordion.removeClass('animating');
					})
				}
			});
		}
	};


	Events = {
		clickHeader: function (e) {
			e.preventDefault();
			var $this = $(e.currentTarget);
			methods.toggleContainer.call(e.data,$this);
		}
	};

	$.fn.BoltAccordion = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.BoltAccordion');
		}
	};

	$.fn.BoltAccordion.options = {
		displayMode : 'Single'
	};


})(jQuery, window, document);
