﻿if (VSIntellisenseExtensions) {

VSIntellisenseExtensions.enableMicrosoftAjaxLog = function (logLevel) { 
/// <summary>Enables Microsoft Ajax VSIntellisense extension logging</summary>
/// <param name="logLevel" type="String">Acceptable values are "info", "warning", and "error"</param>

    VSIntellisenseExtensions.logMessage("Passed in log level is " + logLevel);
    if(logLevel) {
        switch(logLevel.trim().toLowerCase())
        {
            case "info":
                VSIntellisenseExtensions["MicrosoftAjaxLogLevelInfo"] = true;
                VSIntellisenseExtensions.logMessage("Setting log level info.");
                // Intentional fallthrough
            case "warning":
                VSIntellisenseExtensions["MicrosoftAjaxLogLevelWarning"] = true;
                VSIntellisenseExtensions.logMessage("Setting log level warning.");
                // Intentional fallthrough
            case "error":
                VSIntellisenseExtensions["MicrosoftAjaxLogLevelError"] = true;
                VSIntellisenseExtensions.logMessage("Setting log level error.");
        }
    }
};


(function () {


    function logError(message) {
        VSIntellisenseExtensions.logMessage("MicrosoftAjax.vsextensions error: " + message);
    }

    function logWarning(message) {
        VSIntellisenseExtensions.logMessage("MicrosoftAjax.vsextensions warning: " + message);
    }

    function logInfo(message) {
        VSIntellisenseExtensions.logMessage("MicrosoftAjax.vsextensions info: " + message);
    }

    // Override runtime validation functions to improve perf and avoid unnecessary errors
    Function._validateParams = function Function$_validateParams(params, expectedParams, validateParameterCount) {
        return null;
    }

    String._toFormattedString = function String$_toFormattedString(useLocale, args) {
        return "";
    }


    function getMetaFieldNames(functionObject) {
        /// <summary>Returns the names of the doc-comment fields defined in the passed in function</summary>
        /// <param name="functionObject" type="Function">Function to search for doc-comment field definitions</param>
        /// <returns type="Object">Map of meta-field names to "MetaField" marker string.</returns>

        if(!functionObject) {
            return null;
        }

        var functionDocComments = getFunctionDocComments(functionObject);
        if (!functionDocComments) {
            return null;
        }

        var fieldSet = {};

        var fieldNamesRegExp = /<field\s+[^>]*name=[\'\"]{1}([^\'\"]*)[\'\"]{1}/g;
        var fieldNameCaptureIndex = 1;
        var results;
        while (results = fieldNamesRegExp.exec(functionDocComments)) {
            var metaField = results[fieldNameCaptureIndex];
            if (logInfos) logInfo("Found meta-field: " + metaField); 
            fieldSet[metaField] = "MetaField";
        }

        return fieldSet;
    }

    function getFunctionDocComments(functionObject) {
        /// <summary>Returns XML doc-comment text of function</summary>
        /// <param name="functionObject" type="Function">Function to search for doc-comments</param>
        /// <returns type="String">Doc-comment content after stripping the leading ///</returns>

        var functionCode = functionObject.toString();
        var functionDocComments = "";

        var line;
        var nextLineStartPos = functionCode.indexOf('{');

        if (nextLineStartPos < 0)
            return null;
        else
            nextLineStartPos++;

        while ((line = getLine(nextLineStartPos, functionCode)) && (line.length > 0)) {
            var docCommentLine = getDocCommentLine(line);
            if (docCommentLine == null)
                break;
            else if (docCommentLine.length > 0)
                functionDocComments += docCommentLine;

            nextLineStartPos += line.length;
        }

        if (logInfos) logInfo("Got function doc-comments: " + functionDocComments);

        return functionDocComments;
    }

    function getLine(startPosition, multiLineText) {
        /// <summary>First line starting at the specified position within passed in text</summary>
        /// <param name="startPosition" type="Number">Search start position</param>
        /// <param name="multiLineText" type="String">Multi-line text to search for the next line</param>
        /// <returns type="String">Single line starting at the specified position or null at the end of the text.</returns>
   
        if (startPosition === undefined || multiLineText === undefined)
            return null;

        if(startPosition >= multiLineText.length)
            return null;

        for(var position = startPosition; position < multiLineText.length; position++) {
            if(multiLineText[position] == '\r' || multiLineText[position] == '\n') {
                if (multiLineText[position] == '\r' && position + 1 < multiLineText.length && multiLineText[position + 1] == '\n')
                    position++;
                return multiLineText.substring(startPosition, position + 1);
            }
        }

        return multiLineText.substring(startPosition);
    }

    function getDocCommentLine(line) {
        /// <summary>Returns text of the XML doc-comment one the line</summary>
        /// <param name="line" type="String"></param>
        /// <returns type="String">Doc-comment line content stripping leading ///, or null if the line is not a doc-comment line</returns>

        var docComment = "///";

        line = line.trim();

        // Blank lines are fine
        if (line.length == 0)
            return "";

        // Non-blank line that doesn't start with "///" is not considered a doc-comment line
        if (!line.startsWith(docComment))
            return null;
        
        // Don't allow more than three slashes at the start
        if (line.length > docComment.length && line[docComment.length + 1] == '/')
            return null;

        // Empty doc-comment lines are fine
        if (line.length == docComment.length)
            return "";

        return line.substring(docComment.length);
    }


    VSIntellisenseExtensions.addCompletionHandler(function (context, completions) {

        function addInheritedItems() {
            
            if(VSIntellisenseExtensions["MicrosoftAjaxLogLevelInfo"]) logInfo("In addInheritedItems");

            if (contextMsAjaxType == "class") {
                if (context.target.resolveInheritance) {
                    var childMemberCache = new Object();
                    for (var member in context.target) {
                        childMemberCache[member] = member;
                    }
             
                    context.target.resolveInheritance();
             
                    for (var member in context.target) {
                        if (!childMemberCache[member]) {
                            var kind = isFunction(context.target[member]) ? "method" : "field";
                            completions.items.push({ name: member, kind: kind, value: context.target[member], parentObject: context.target }); 
                        }
                    }
                }
            }

            if (logInfos) logInfo("Exiting addInheritedItems");
        }


        function isFunction(obj) {
            return Object.prototype.toString.call(obj) === "[object Function]";
        }

        function filterItems() {

            if (logInfos) logInfo("Entering filterItems for " + completions.items.length + " items");

            completions.items = completions.items.filter(filterItem);

            if (logInfos) logInfo("Exiting filterItems");

        }

        function filterItem(item) {

            if (logInfos) logInfo("");
            if (logInfos) logInfo("Filtering item: " + item.name);

            var hidden = false;
            var msAjaxType = null;
        
            hidden = item.name[0] == "_";
            hidden |= item.name.indexOf("$") > 0;

            if (hidden) {
                var contextIsThis = false;
                if (context.targetName && context.targetName == "this") { 
                    contextIsThis = true;
                }
                if (logInfos) logInfo("Context is this: " + contextIsThis + " (it is " + context.identifier + ")");
                if (contextIsThis && !isGlobalScope) {
                    // Don"t hide any members when completion is invoked on "this."
                    // and "this" is not global scope
                    hidden = false;
                }
                else if (isGlobalScope) {
                    if (logInfos) logInfo("In global scope item value for item " + item.name + " is " + item.value);
                    msAjaxType = getMicrosoftAjaxTypeForObject(item.value);
                    if (msAjaxType == null) {
                        hidden = false;
                    }
                }
            }

            // For Enums and Flags, we show fields only
            if (!hidden && isEnumContext) {

                if (item.kind != "field") {
                    hidden = true;
                    if (logInfos) logInfo("Filtering out non-field member " + item.name + " on a " + contextMsAjaxType);
                }
                else if (enumMetaFields && (enumMetaFields[item.name] != "MetaField")) {
                    hidden = true;
                    if (logInfos) logInfo("Filtering out non-meta-field member " + item.name + " on a " + contextMsAjaxType);
                }
            }

            return !hidden;
        }

        function setItemsGlyphs() {
            completions.items.forEach(setItemGlyph);
        }

        function setItemGlyph(item) {

            if (logInfos) logInfo("Setting item glyph for: " + item.name);

            if (isEnumContext) {
                item.glyph == "vs:GlyphGroupEnumMember";
            }
            else if (item.name.indexOf("set_") == 0 || item.name.indexOf("get_") == 0) {
                if (item.kind == "method") {
                    item.kind = "property";
                }
            }
            else if (item.name.indexOf("add_") == 0 || item.name.indexOf("remove_") == 0) {
                if (item.kind == "method") {
                    item.glyph = "vs:GlyphGroupEvent";
                }
            }
            else {
                var ajaxType = getMicrosoftAjaxTypeForObject(item.value);
                if (ajaxType != null) {
                    switch (ajaxType) {
                        case "class":
                            item.glyph = "vs:GlyphGroupClass";
                            break;
                        case "namespace":
                            item.glyph = "vs:GlyphGroupNamespace";
                            break;
                        case "interface":
                            item.glyph = "vs:GlyphGroupInterface";
                            break;
                        case "enum":
                        case "flags":
                            item.glyph = "vs:GlyphGroupEnum";
                        default:
                            if (logErrors) logError("Unknown Microsoft Ajax type: " + ajaxType);
                    }
                }
            }
        }

        function isFlagsOrEnumItem(item) {
            if (logInfos) logInfo("Entering isFlagsOrEnumItem for: " + item.name);
            var microsoftAjaxType = getMicrosoftAjaxTypeForObject(item.value);
            return isFlagsOrEnumType(microsoftAjaxType);
        }

        function isFlagsOrEnumType(microsoftAjaxType) {
            return microsoftAjaxType == "enum" || microsoftAjaxType == "flags";
        }

        function getMicrosoftAjaxTypeForObject(object) {
            
            var ajaxType = null;

            if (object) {
                ajaxType = getMicrosoftAjaxType(object);

                if (ajaxType == null) {
                    var objectConstructor = object.constructor;
                    if (objectConstructor) {
                        ajaxType = getMicrosoftAjaxType(objectConstructor);
                        if (ajaxType != null) { 
                            if (logInfos) logInfo("Got MS Ajax type for object constructor: " + ajaxType);
                        }
                    }
                }
                else {
                    if (logInfos) logInfo("Got MS Ajax type for object itself: " + ajaxType);
                }
            }

            return ajaxType;
        }

        function getMicrosoftAjaxType(obj) {

            var ajaxType = null;

            if (obj.__class)
                ajaxType = "class";
            else if (obj.__interface)
                ajaxType = "interface";
            else if (obj.__namespace)
                ajaxType = "namespace";
            else if (obj.__flags)
                ajaxType = "flags";
            else if (obj.__enum)
                ajaxType = "enum";

            return ajaxType;
        }

        function isGlobalScopeCompletionList() {

            var result = false;

            for (var i = 0; i < completions.items.length; i++) {
                if (completions.items[i].name == "break") {
                    result = true;
                    break;
                }
            }

            return result;
        }

        var logErrors = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelError"];
        var logWarnings = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelWarning"];
        var logInfos = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelInfo"];
        
        if (logInfos) logInfo("In addCompletionHandler\n");
//        if (logInfos) logInfo("Context.target is " + context.target);
        if (logInfos) logInfo("Context.targetName is " + context.targetName);
        if (logInfos) logInfo("Context.extent.start is " + context.extent.start);
        if (logInfos) logInfo("Context.extent.length is " + context.extent.length);

        var isGlobalScope = isGlobalScopeCompletionList();
        var contextMsAjaxType = isGlobalScope ? null : getMicrosoftAjaxTypeForObject(context.target);
        var isEnumContext = contextMsAjaxType != null ? isFlagsOrEnumType(contextMsAjaxType) : false;
        var enumMetaFields = isEnumContext ? getMetaFieldNames(context.target) : {};

        if (logInfos) logInfo("isGlobalScope is " + isGlobalScope);
        if (logInfos) logInfo("contextMsAjaxType is " + contextMsAjaxType + "\n");

        addInheritedItems();
        filterItems();
        setItemsGlyphs();
    });


    VSIntellisenseExtensions.addParameterHelpHandler(function (context, functionHelp) {

        function getPropertyType(comments) {
            /// <param name="comments" type="String" />

            var propertyType = "";

            var docCommentStart = comments.indexOf("<value");
            if (docCommentStart == -1) {
                if (logWarnings) logWarning("<value> doc comment is not found.");
                return "";
            }

            var docCommentEnd = comments.indexOf("</value>", docCommentStart);
            if (docCommentEnd == -1) {
                docCommentEnd = comments.indexOf("/>", docCommentStart);
                if (docCommentEnd == -1) {
                    if (logWarnings) logWarning("End of <value> doc comment is not found.");
                    return "";
                }
            }

            var typeStart = comments.indexOf("type", docCommentStart);
            if (typeStart == -1 || typeStart > docCommentEnd)
                return "";

            var typeValueStart = comments.indexOf("\"", typeStart);
            if (typeValueStart == -1 || typeValueStart > docCommentEnd)
                return "";

            var typeValueEnd = comments.indexOf("\"", typeValueStart + 1);
            if (typeValueEnd == -1 || typeValueEnd > docCommentEnd)
                return "";

            propertyType = comments.substring(typeValueStart + 1, typeValueEnd);

            return propertyType;
        }

        function fixupSetterSignature(signature) {
            if (logInfos) logInfo("Fixing signature for type " + getterType);
            if (getterType) {                
                if (signature.params.length > 0) {
                    signature.params[0].type = getterType;
                }
            }
            else {
                if(VSIntellisenseExtensions["MicrosoftAjaxLogLevelError"]) logError("No getter.");
            }
        }

        function fixupSetter() {

            var baseName = functionHelp.functionName.substring("set_".length);
            var getterName = "get_" + baseName;

            if (context.parentObject) {
                var getterFunction = context.parentObject[getterName];
                if (getterFunction) {
                    if (context.getComments) {
                        var comments = context.getComments(getterFunction);
                        if (comments && comments.inside) { 
                            if (logInfos) logInfo("Comments is " + comments.inside);
                            getterType = getPropertyType(comments.inside);
                            if (logInfos) logInfo("Matching getter type is " + getterType);
                            if (functionHelp.signatures) {
                                functionHelp.signatures.forEach(fixupSetterSignature);
                            }
                        }
                        else { 
                            if (logWarnings) logWarning("No doc-comments in matching getter " + getterName);
                        }
                    }
                    else {
                        if(VSIntellisenseExtensions["MicrosoftAjaxLogLevelError"]) logError("UNEXPECTED: No getComments exist on the current context.");
                    }
                }
                else {
                    if (logWarnings) logWarning("No matching getter for seter " + functionHelp.functionName);
                }
            }
        }

        var logErrors = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelError"];
        var logWarnings = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelWarning"];
        var logInfos = !!VSIntellisenseExtensions["MicrosoftAjaxLogLevelInfo"];

        if (logInfos) logInfo("In addParameterHelpHandler");

        var getterType = null;

        if (functionHelp.functionName.indexOf("set_") == 0 && functionHelp.functionName.length > "set_".length) {
            fixupSetter();
        }
    });

})();
}

// SIG // Begin signature block
// SIG // MIIXYAYJKoZIhvcNAQcCoIIXUTCCF00CAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFJN+d9J0hAVO
// SIG // nUSYtuAxtZZpyvQZoIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQGymwAAAAAAFTANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0xMTAyMjEyMDUzMTJa
// SIG // Fw0xMjA1MjEyMDUzMTJaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQClcXQYy9ucL/rxYxmAo47/
// SIG // yHCDDKFHKvr6WrDOTQ+9O9ohzQXVfXw/tZl2JTrcoE8g
// SIG // 5XLTQ3Hm/2wU+Z+EsiWHvRS1Xb43iS7Fq4VgcYWBJ7kz
// SIG // A/mt1pMC7rZVEQLLg4X2wYVjYRmgFJydn5xGYzfYETxX
// SIG // x0ggUxb3zFj+9sAayrZGbA3oOgXOphzjiPDdqsJwg1vR
// SIG // 8vI/uNmknaMAbqqBze46BK3ASpIsE+DpQXYJgAFsZyC0
// SIG // MbA+a9JACIPVcDCNMOe+jNAv7tSGPxpapcBoheiJfJED
// SIG // Sm5ZQyBInIj+yUW36EwEAQtizqLsHJMl/dCxDsb4tiQa
// SIG // e/t81lxO0WfpAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFNly1MsMYl/do3SfXw6Y
// SIG // QahbE0MmMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAYGAn2HwwSRJHQTc8
// SIG // XnW05KIANhFIsCxYm5h87aMSoZjuGLOX9m2O2DAaHK0h
// SIG // Y0FGk9evGihLlzZ9QFRgzGK86fxpp19/xYemhgx8qeJZ
// SIG // fduI1Xd4BaYkymT4TlG3RCiOGixUCChyxR8YPSzPACdv
// SIG // E9GlYfLWYjq9AE4FL5lnfeVjQz87DL6LB3rheEC/33jO
// SIG // hyCiGaG8YPCfrFJcdhT9G3gF12N1a4H9h0atrVlSv3u/
// SIG // rxmkkrLyCyx+MBDbMUu+hFp/95sSeOnefmCrDcYxHvZy
// SIG // mj5nzzjuOM45G3PbO9k034PSnM3hi94a/mQIX6/2Lrgy
// SIG // JXZpJfPVjTUjw9slRDCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggSbMIIElwIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQGymwAA
// SIG // AAAAFTAJBgUrDgMCGgUAoIHGMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBRACLcnIa3C
// SIG // E1Q/EzM9bQMpng92rzBmBgorBgEEAYI3AgEMMVgwVqA8
// SIG // gDoATQBpAGMAcgBvAHMAbwBmAHQAQQBqAGEAeAAuAHYA
// SIG // cwBlAHgAdABlAG4AcwBpAG8AbgBzAC4AagBzoRaAFGh0
// SIG // dHA6Ly9taWNyb3NvZnQuY29tMA0GCSqGSIb3DQEBAQUA
// SIG // BIIBABIMDASYcm1z7BQmYOsuKhRlVVjRByKHUhOwR3TW
// SIG // eOcdNfsMDRHM+YBv1JaOwc2l5POb85tInB2gYkELtylC
// SIG // dWUSj3Mdht8V+/O+OIOOBsMUaZ6loVxUVelfEsXURdGd
// SIG // DtiTKEvMFxf+2PoIO7l3rGatAoHVDXOPt/JtwKbRtT1c
// SIG // SkAGfzgq0amDVLRDLUmOS4pLAeVKoqV2roS/Lv8XXcbX
// SIG // /VGkiOaudc5A6asy/NKndrksKLNXcZZPHKMGn6KpehXX
// SIG // eqBcUsyDaapbfRPx/BdJrj9yxvCQpyYWFSj/LnaPP1CP
// SIG // levr1eabCdyff9NwVn6Ww+sk8TFew731umOCqS+hggIf
// SIG // MIICGwYJKoZIhvcNAQkGMYICDDCCAggCAQEwgYcweTEL
// SIG // MAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0b24x
// SIG // EDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jv
// SIG // c29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWljcm9z
// SIG // b2Z0IFRpbWVzdGFtcGluZyBQQ0ECCmEGlC0AAAAAAAkw
// SIG // BwYFKw4DAhqgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcN
// SIG // AQcBMBwGCSqGSIb3DQEJBTEPFw0xMTA4MjUwNTA1Mjla
// SIG // MCMGCSqGSIb3DQEJBDEWBBQWcSPmS8APd7LM88KxHprp
// SIG // uFUm4jANBgkqhkiG9w0BAQUFAASCAQAmDWM9ZoxGh97E
// SIG // ltRPQ2jnw5W2l8loO4JWYmaKfQQQTksYDsRM9yvPp2jb
// SIG // BjBAUTtB9A9ps3JJ+TOAktosIhlBqidgMXbRW13qqFtf
// SIG // cvZnUqqdCWH7dPtfVC/Wo62aoj5h48hZC94Y8zFSzs9M
// SIG // llWZ+TxcrUCoykAwWJsUF2AkxOdVWM4CXbx06OBYfo+0
// SIG // QeKv6Am14q514BOym+nLvddgIFBKNYqQgUjv6QvLoP3S
// SIG // bsEa3R4ieCmmDzvQmAQPtYEr68HmUuBv9iIX5V1gfz5v
// SIG // n89pCA6tuFnlqJaWIYYN07yks8Z69z3LxRWyZwjpXp0F
// SIG // 9/0VXrc1PiFDEwKMFwtS
// SIG // End signature block
