﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />

(function ($) {

    $.fn.extend({
        //pass the options variable to the function
        PolaroidGallery: function (options) {

            var albumDisplayAmount = Number(GetJSSession("Polaroid_AlbumLimit"));
            var pictureDisplayAmount = Number(GetJSSession("Polaroid_PictureLimit"));
            var exlcudedGalleries = GetJSSession("GalleryImages_Excluded").split(',');
            //Set the default values, use comma to separate the settings, example:
            var defaults = {
                attachToMenuItem: false,
                popUp: Boolean.parse(GetJSSession("Polaroid_PopOut")),
                link: GetJSSession("Polaroid_PictureLink") || null,
                linkName: GetJSSession("Polaroid_PictureLinkName") || null,
                boxWidth: '90%',
                boxHeight: '90%'
            }

            var options = $.extend(defaults, options);
            var o = options;


            //write out the html required
            //check if the container exists
            var container = $("#PoloroidContainer");
            container.remove();
            //add a container to the document
            container = $("<div id='PoloroidContainer' style='width:100%;height:100%;background:#fff;top:0;left:0;position:fixed'><div id='PolaroidLogo'></div></div>");

            container.appendTo(document.body);



            //populate the polaroid container

            var catCount = 0;
            var maxCats = 0;
            AuroraJS.Modules.htmlObjectCateImages = [];
            var galleries = [];


            function createAlbums() {
                if (catCount == maxCats - 1) {
                    //create albums
                    for (var catImage = 0; catImage < galleries.length; catImage++) {
                        if (!findItemInArray(exlcudedGalleries, galleries[catImage].Name)) {
                            var catHTML = "";
                            catHTML += "<div class='album' id=" + galleries[catImage].ID + ">";
                            var imageLen = galleries[catImage].Images.length > pictureDisplayAmount ? pictureDisplayAmount : galleries[catImage].Images.length;

                            for (var image = 0; image < imageLen; image++) {
                                var imgSmall = '/ClientData/' + clientSiteID + '/Uploads/sm_gallery_01_' + galleries[catImage].Images[image].ID;
                                var imgLarge = '/ClientData/' + clientSiteID + '/Uploads/gallery_01_' + galleries[catImage].Images[image].ID + ".jpg";

                                catHTML += '<div class="content">';
                                var imageID = 'polaimg' + new Date().getMilliseconds();
                                catHTML += String.format('<img src="{0}.jpg" alt="{1}" id="{2}"/>', imgSmall, imgLarge, imageID);
                              
                                if (o.link) {
                                    catHTML += String.format('<span>{0}&nbsp;<a class="pp_viewmore" name="{0}" href="{1}" target="_blank">' + o.linkName + '</a><a href="javascript:void(0)"></a></span>', galleries[catImage].Images[image].Name, o.link + '?imgname=' + escape(galleries[catImage].Images[image].Name));
                                }
                                else {
                                    catHTML += String.format('<span>{0}</span>', galleries[catImage].Images[image].Name);
                                }

                                catHTML += '</div>';



                            }
                            catHTML += String.format('<div class="descr">{0}</div>', galleries[catImage].Name);
                            catHTML += "</div>";

                            AuroraJS.Modules.htmlObjectCateImages.push(catHTML);
                        }
                    }

                    AuroraJS.Modules.galleryDS = galleries;
                    AuroraJS.Modules.currentAlbumPage = 1;
                    AuroraJS.Modules.maxAlbumPages = Math.round(((AuroraJS.Modules.htmlObjectCateImages.length + 1) / albumDisplayAmount)).toFixed(0);
                    AuroraJS.Modules.currentPolaroidIndex = 3;
                    AuroraJS.Modules.currentPictureIndex = pictureDisplayAmount - 1;
                    AuroraJS.Modules.maxPictureIndex = 0;
                    AuroraJS.Modules.currentPicturePage = 0;
                    AuroraJS.Modules.hasImagesSpread = false;
                    var currentAlbumset = [];

                    //add default display items
                    for (var album = 0; album < albumDisplayAmount; album++) {
                        if (AuroraJS.Modules.htmlObjectCateImages[album]) {
                            currentAlbumset.push(AuroraJS.Modules.htmlObjectCateImages[album]);
                        }

                    }

                    var polaroidHeader = "<h1>" + GetJSSession("Polaroid_GalleryName") + "</h1><div id='pp_gallery' class='pp_gallery'><div id='pp_Exit' class='pp_Exit' onclick=\"$('#PoloroidContainer').remove();\">Exit Gallery</div><div id='pp_loading' class='pp_loading'></div><div id='pp_next' class='pp_next'></div><div id='pp_prev' class='pp_prev'></div><div id='pp_thumbContainer'>";
                    var polaroidFooter = "<div id='pp_back' class='pp_back'>Back to Albums</div><div id='pp_AlbumMsg' class='pp_AlbumMsg'>Click an Album stack below to view its pictures</div><div id='pp_PicMsg' class='pp_PicMsg'>Click a picture to view from the items below</div><div id='pp_Albums' class='pp_Albums'>Albums Showing (" + AuroraJS.Modules.currentAlbumPage + " of " + AuroraJS.Modules.maxAlbumPages + ")</div><div id='pp_PicPrev' class='pp_PicPrev'>Previous Set of Photos</div><div id='pp_PicNext' class='pp_PicNext'>Next Set of Photos</div><div id='pp_NavNext' class='pp_NavNext'>Next Set of Albums</div><div id='pp_NavPrev' class='pp_NavPrev'>Previous Set of Albums</div></div></div><div></div>";

                    $(polaroidHeader + currentAlbumset.join('') + polaroidFooter).appendTo(container);
                    $(".pp_loading").show();
                    //append fancybox if link available

                    if (o.link) {
                        $(".pp_viewmore").live('click', function (e) {
                            e.preventDefault();
                            var $this = $(this);
                            var windowOpened = window.open($this.attr("href"));
                            if (!windowOpened) {
                                alert('Please enable pop-ups for this site to view the detailed profile');
                            }
                            //                            showFancyBoxPopup($this.attr("href"), 'iframe', $this.attr('name'), o.boxWidth, o.boxHeight);
                        });
                    }


                    ready();
                }
                else {
                    catCount++;
                }
            }

            function showAlbums(startIndex, endIndex) {

            }
            var response = function (result) {
                var galleryData = $.parseJSON(result.Data).NewDataSet.Table;
                maxCats = galleryData.length;
                for (var cate = 0; cate < galleryData.length; cate++) {
                    galleries.push(galleryData[cate]);

                    var resImages = function (resultimages, userContext) {
                        myCat = findByInArray(galleries, "ID", userContext);

                        if (myCat) {
                            var MyImages = [];
                            for (var image = 0; image < resultimages.Data.length; image++) {
                                MyImages.push(resultimages.Data[image]);

                            }
                            myCat.Images = MyImages;
                            createAlbums();
                        }

                    };

                    Aurora.Services.AuroraWebService.GetGalleries(galleryData[cate].ID, resImages, AuroraJS.onError(), galleryData[cate].ID);
                }


            };




            Aurora.Services.AuroraWebService.GetGalleryCat("Images", null, response, AuroraJS.onError);





            function ready() {


                //load final load script
                $.ajax({
                    url: "/Scripts/loadPolaroid.js",
                    dataType: 'script',
                    success: function () {


                        //pop up?
                        if (o.popUp) {
                            $("#galLink").fancybox("#PoloroidContainer", {
                                overlayColor: '#000000',
                                overlayOpacity: 0.5,
                                width: '90%',
                                height: '90%'
                            });
                            $("#galLink").click();
                        }
                        else {
                            $("#inputcontent").append($("#PoloroidContainer"));
                        }

                        loadPolaroids(o.popUp, pictureDisplayAmount, AuroraJS.Modules.maxAlbumPages, o.linkName, o.link);
                        $(".pp_loading").hide();
                    }
                });


            }

            var element = this;
            return this.each(function () {


            });
        }
    });

})(jQuery);