﻿var rhino = document.createElement("script");
var easing = document.createElement("script");
var mousewheel = document.createElement("script");
var head = document.getElementsByTagName('head')[0];

rhino.type = "text/javascript";
rhino.src = "/Scripts/easing.js";

easing.type = "text/javascript";
easing.src = "/Scripts/rhinoslider-1.04.min.js";

mousewheel.type = "text/javascript";
mousewheel.src = "/Scripts/mousewheel.js";

head.appendChild(easing);
head.appendChild(mousewheel);
head.appendChild(rhino);
