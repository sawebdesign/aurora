﻿/*
//Author: Wesley Perumal
//Date : 20/07/2011
//Description: Allows the slow fading of an array of content.
*/
/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="Utils.js" />
/// <reference path="jquery.tools.min.js" />
/// <reference path="common.js" />
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />
/// <reference path="jquery.history.js" />

AuroraJS.Modules.SegueInit = function () {
    var containerWidth = GetJSSession("Segue_Size");
    var insertAfter = GetJSSession("Segue_AppendAfter");

    $('#navLinks')
    .css({ width: containerWidth, 'text-align': 'center', margin: '4px 0' })
    .append("[ <a href='#'><span id='prev'>&lt; Previous</span></a>"
    + " | <a id='pause-play' href='#'>Pause</a>"
    + " | <a href='#'><span id='next'>Next &gt;</span></a> ]"
  );
    // <div> container holding all the news stories
    var $newsStories = $('#Segue_Data');

    if (insertAfter) {
        $("#Segue").insertAfter($("#" + insertAfter));
        $('#segueInfo').insertAfter($newsStories);
        $('#navLinks').insertAfter($newsStories);
    }
    /* dynamically style:
    * [1] container itself
    * [2] each paragraph in the summary 
    */

    var biggest = 0;
    $newsStories.find('div div').each(
    function () {
        if ($(this).height() > biggest) {
            biggest = $(this).height();
        }
    }
    );

    if (biggest == 0) {
        biggest = "280";
    }
    $newsStories.css({
        width: containerWidth
        , height: biggest
        , margin: '10px 0'
        , border: GetJSSession("Segue_BorderStyle")
        , display: 'block'
    }).find('div div').css({ padding: '8px' });

    // call the plugin and set the options  
    $newsStories.cycle({
        timeout: GetJSSession("Segue_TimeOut")
        , cleartype: 1
        , speed: GetJSSession("Segue_Speed")
        , prev: '#prev'
        , next: '#next'
        , after: function (currSlideElement, nextSlideElement, options, forwardFlag) {
            $('#segueInfo').html('<strong>'
        + 'Article ' + (options.currSlide + 1) + ' of ' + options.slideCount
        + '</strong>'
            ////

            ////
      );

            var big = 0;
            $("#Segue").find('div div').each(
                function () {
                    if ($(this).height() > big) {
                        big = $(this).height();
                    }
                }
                );
            $("#Segue_Data").css("height", big + "px");
        }
    });
    $('#pause-play').click(function () {
        var pause = 'Pause';
        var text = $(this).text();
        if (pause == text) {
            $newsStories.cycle('pause');
            $(this).text('Play');
        }
        else {
            $newsStories.cycle('resume', true);
            $(this).text(pause);
        }
        return false;
    });

    $("#Segue").show();
    //    $('img.newsPic').css({ float: 'left', padding: '0 4px', margin: '8px' });
    //    $('a.msgAlert').click(function () {
    //        alert('Hyperlink disabled for this example; News Article does not exist.');
    //        return false;
    //    });
};