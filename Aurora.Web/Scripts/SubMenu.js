﻿
AuroraJS.Modules.generateSubMenu = function () {
    /// <summary>Generates a submenu on the page, using the primary menu control as a source, and 
    /// inserts it into the document within #SubMenu
    ///</summary>

    var currentPage = $("#menu .current");
    var subMenu;
    if (currentPage.length > 0) {
        var currentRoot = currentPage.parents('#menu>ul').clone();
        subMenu = currentRoot;
    }
    else {
        var homePage = $("#menu .homeitem");
        var homeRoot = homePage.parents('#menu>ul').clone();
        subMenu = homeRoot;
    }
    subMenu.find('span.sf-sub-indicator').andSelf().remove();
    subMenu.find('*:not(.current)').andSelf().removeAttr('class').removeAttr('style');
    subMenu.find('.current').andSelf().removeAttr('style');

    //Code to create "more..." links in menus over a certain length.
    //TODO: Add a switch to turn code on and off. Off by default.
    //    subMenu.find('ul').each(function () {
    //        var branch = $(this);
    //        var menuItems = branch.children('li');
    //        if (menuItems.length > 2) {
    //            menuItems.filter(':gt(1)').hide();
    //            var expand = $('<li><a href="javascript:void(0);">more...</a></li>');
    //            expand.click(function () { $(this).hide(); menuItems.filter(':gt(1)').slideDown(); })
    //            menuItems.eq(1).after(expand);

    //        }

    //    });

    var subMenuContainer = $("#SubMenu");
    subMenuContainer.html(subMenu);

    if (AuroraJS.Bespoke.SubMenu) {
        AuroraJS.Bespoke.SubMenu(subMenuContainer);
    }
    return subMenuContainer;

}
AuroraJS.Modules.generateSubMenu.After = function () { };
AuroraJS.Modules.generateSubMenu.Before = function () { };