﻿/*
	Membership Showcase
	Aurora Plugin
	Author: Garth
	Creation Date: 2013/5/21

	N.B. The self referencing plugin calls, i.e. $this.MemberShowCase('Method'), are to preserve scope.
	I would have prefered to use $.proxy but am limited by version 1.5.2 not supporting argument passing.
*/


(function ($, window, document, undefined) {

	var methods = {
		getConvertedValue: function ($locations) {
			if ($('.scriptTemplates.CurrencyConverter').length === 0) {
				$('body').append('<div style="display:none" class="scriptTemplates CurrencyConverter">' + AuroraJS.Modules.GetTemplate("CurrencyConverter") + '</div>');
			}
			if ($.CurrencyConverter.data) {
				methods.renderResults($locations);
			} else {
				$locations.html($('#currencyConversionLoadertmpl').render({}));
			}
			Aurora.Services.AuroraWebService.GetCurrencyConversions(events.storeCurrencyData, AuroraJS.onError, $locations);
		},
		renderResults: function ($locations) {
			if ($.CurrencyConverter.data.length > 0) {
				$locations.each(function () {
					var $currencyContainer = $($('#currencyConversionContainertmpl').render({}));
					var $this = $(this);
					var value = $this.attr('value');

					for (var i = 0; i < $.CurrencyConverter.data.length; i++) {
						$.CurrencyConverter.data[i].CalculatedValue = $.CurrencyConverter.data[i].Value * value;
					}
					$currencyContainer.append($('#currencyConversionDisplaytmpl').render($.CurrencyConverter.data));
					$this.html($currencyContainer);
				});
			} else {
				$locations.html('');
			}
			
		},
		reverseConvertValue: function (value, currency, callback) {
			if ($('.scriptTemplates.CurrencyConverter').length === 0) {
				$('body').append('<div style="display:none" class="scriptTemplates CurrencyConverter">' + AuroraJS.Modules.GetTemplate("CurrencyConverter") + '</div>');
			}
			var options = {
				value : value,
				currency : currency,
				callback : callback
			}

			if ($.CurrencyConverter.data) {
				methods.calculateReturn(options);
			} else {
				Aurora.Services.AuroraWebService.GetCurrencyConversions(events.reverseReturn, AuroraJS.onError, options);
			}
			
		},
		calculateReturn: function (options) {
			var currencyVal = findByInArray($.CurrencyConverter.data, "Name", options.currency);
			if (currencyVal != undefined) {
				options.callback(options.value / currencyVal.Value);
			} else {
				options.callback(options.value);
				console.log("Error: no currency conversion found");
			}
		}
	};

	var events = {
		storeCurrencyData: function (result, $locations) {
			if (result.Result == true) {
				$.CurrencyConverter.data = result.Data;
				methods.renderResults($locations)
			};
			
		},
		reverseReturn: function (result, options) {
			if (result.Result == true) {
				$.CurrencyConverter.data = result.Data;
				methods.calculateReturn(options)
			};
		}
	};

	$.CurrencyConverter = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.getConvertedValue.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.CurrencyConverter');
		}
	};

	AuroraJS.plugins.CurrencyConverter = function () {
		
	};

})(jQuery, window, document);





