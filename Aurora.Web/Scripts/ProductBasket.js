﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="Utils.js" />
/// <reference path="jquery.tools.min.js" />
/// <reference path="common.js" /> 
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />
/// <reference path="jquery.history.js" />
/// <reference path="modules.js" />
/// <reference path="Utils.js" />

var allowProcess = false;
var waitForCallback = false;
var paymentSchemaDS = null;


function getColour(item,product) {
    if (item) {
        var returnItem = "";
        if (item[0].HexValue) {
            returnItem = "background-color:#" + item[0].HexValue + ";";
        }
        if (item[0].ImageUrl) {
            returnItem += " background-image:url('" + item[0].ImageUrl + "');";
        }

       
        return returnItem;
    }

    if (item == null && product != null) {
        var response = function (result, prd) {
            if (result.Data.length > 0) {
                var imageSrc = String.format("/ClientData/{0}/Uploads/sm_product_{1}{2}", clientSiteID, result.Data[0].ID, result.Data[0].MediaType);
                $(String.format("#{0} .productImage", prd.ID)).html(String.format("<img src='{0}' style='width:60px!important;height:60px!important' /> ", imageSrc));
            }
        };

        Aurora.Services.AuroraWebService.GetProductImage(product.ID, response, onError,product);
    }
}
AuroraJS.Modules.calculateBasketTotal = function () {
	var total = 0;
	var interest = ($('#PaymentSchemasRecurringOptions').find("option:selected").attr("data-recurring-interest")) || 0;
	var paymentTerm = ($('#PaymentSchemasRecurringOptions').find("option:selected").val()) || 0;
	console.log("interest: " + interest);
	console.log("payment term: " + paymentTerm);
    //updateprice

    $("#BasketRowTemplate tr").each(function (index, domEle) {
        var $current = $(domEle);
        var $textBox = $current.children('.productQuantity').children('input');

        $current.find('.productTotal').html((Number($textBox.val()) * Number($current.find('.productPrice').html())).toFixed(2));

        if (Number($current.find('.productTotal').html())) {
            total = (total + Number($current.find('.productTotal').html()));
        }

    });

    var VAT = Number(GetJSSession("Products_VAT")) / 100;
    if (VAT > 0) {
        $("#SubTotalVal").html(String.format("{0} {1}", GetJSSession("Currency_Symbol"), total.toFixed(2)));
        $("#VatVal").html(String.format("{0} {1}", GetJSSession("Currency_Symbol"), (total * VAT).toFixed(2)));
    }
    else {
        $("#SubTotalVal").parent().hide();
        $("#VatVal").parent().hide();
    }

    console.log("vat: " + VAT);

    if (paymentTerm == 0) {
    	$("#PaymentSchemasRecurringOptions_Installment").parent().show();
    	$("#PaymentSchemasRecurringOptions_Installment").html(GetJSSession("Currency_Symbol") + " " + (total + roundTo(total * VAT,2)).toFixed(2));
    	$("#TotalVal").html(String.format("{0} {1} ({2})", GetJSSession("Currency_Symbol"), (total + (total * VAT)).toFixed(2), GetJSSession("Currency_Code")));
    	var converter = $('<div class="conversions" value="' + (total + (total * VAT)).toFixed(2) + '"></div>')
    	$("#TotalVal").append(converter);
    } else {
		//calculate the installment and round and then calculate the new total by multiplying the installment amount bby the payment term
    	console.log("total before finance: " + total);

    	var financeCharges = ((total * (interest / 100)) * paymentTerm);
    	var totalIncludingFinance = total + financeCharges;
    	var totalVAT = roundTo(totalIncludingFinance * VAT, 2);
    	var installment = paymentTerm > 0 ? ((totalIncludingFinance + totalVAT) / paymentTerm).toFixed(2) : (total + (total * VAT));
		//recalculate the new total based off of the installment (installment * paymentTerm)
    	total = installment * paymentTerm;

    	console.log("finance charges: " + financeCharges);
    	console.log("total including finance: " + totalIncludingFinance);
    	console.log("installment: " + installment);
    	console.log("new total calculated from installment: " + total);

    	$("#VatVal").html(String.format("{0} {1}", GetJSSession("Currency_Symbol"), totalVAT));
    	$("#PaymentSchemasRecurringOptions_Installment").parent().show();
    	$("#PaymentSchemasRecurringOptions_Installment").html(GetJSSession("Currency_Symbol") + " " + installment + " pm");
    	$("#TotalVal").html(String.format("{0} {1} ({2})", GetJSSession("Currency_Symbol"), total.toFixed(2), GetJSSession("Currency_Code")));
    	var converter = $('<div class="conversions" value="' + total.toFixed(2) + '"></div>')
    	$("#TotalVal").append(converter);
    }

    if ($.CurrencyConverter) {
    	$.CurrencyConverter($('#TotalVal .conversions'));
    }
    var itemCount = $(".productQuantity input");
    if (itemCount.length > 0) {
        var itemTotal = 0;
        for (var item = 0; item < itemCount.length; item++) {
            itemTotal = itemTotal + Number($(itemCount[item]).val());
        }

        $(".basketCount").attr("count", itemTotal);
        AuroraJS.Modules.setBasketLink();
    }

};
AuroraJS.Modules.getProductBasket = function () {
    //get the template for the basket
    var template = fetchTemplate("ProductBasket");

    //check if there is a VAT is available
    if (GetJSSession("Products_VAT")) {
        template = template.replace(/{Amount}/gi, GetJSSession("Products_VAT"));
    }

    var ds = null;
    //add the template to the page
    $("#MainBasket").html(template);
    $("#productBasket").hide();

    var productBasketCall = function (result) {
        if (result.Result) {
            if (!ds) {
                ds = $create(Sys.UI.DataView, {}, {}, {}, $get("BasketRowTemplate"));
            }
            ds.set_data(result.Data);
        }

        $("#Contents").unblock();

        //create delete buttons
        $("<img class=\"deleteItem\" style=\"cursor:hand;\" src=\"/Styles/images/cross.png\">")
        .click(function () {
            var $delete = $(this);
            AuroraJS.Modules.UpdateCookieQuantity(0, $delete.parent().parent().find(".productImage").first().attr("colid"), $delete.parent().parent().attr("sizeid"), $delete.parent().parent().attr("id"), true);
            $delete.parent().parent().remove();
            AuroraJS.Modules.calculateBasketTotal();
        })
        .appendTo("td.productAction");

        //get all text boxes in quantity columns
        var productQuantityInputs = $(".productQuantity input");
        productQuantityInputs.bind('change', AuroraJS.Modules.calculateBasketTotal);
        //get the default symbols for > & <
        var defaultLessSymbol = productQuantityInputs.parent().attr("defaultless") || "";
        var defaultMoreSymbol = productQuantityInputs.parent().attr("defaultmore") || "";
        //force numbers only
        productQuantityInputs.ForceNumericOnly();

        //Add < & >
        if (productQuantityInputs.first().parent().attr("showbuttons") == "true") {
            $(String.format("<a href='javascript:void(0)' class='quanLess'><b>{0}</b></a>", defaultLessSymbol)).insertBefore(productQuantityInputs);
            $(String.format("<a href='javascript:void(0)' class='quanMore'><b>{0}</b></a>", defaultMoreSymbol)).insertAfter(productQuantityInputs);
        }

        //Bind events to the < & > buttons
        productQuantityInputs.parent().find('.quanLess').next().change(this.click);
        productQuantityInputs.parent().find('.quanLess').bind('click', function () {
            var $textBox = $(this).next();

            if (Number($textBox.val()) !== 0) {
                $textBox.val(Number($textBox.val()) - 1);
            }
            AuroraJS.Modules.UpdateCookieQuantity($textBox.val(), $textBox.parent().parent().find(".productImage").first().attr("colid"), $textBox.parent().parent().attr("sizeid"), $textBox.parent().parent().attr("id"));
            AuroraJS.Modules.calculateBasketTotal();
        	//reset the selected payment
            $('input[type=radio]:checked').click();
        }).bind('change', $(this).click);


        productQuantityInputs.parent().find('.quanMore').bind('click', function () {
            var $textBox = $(this).prev();
            $textBox.val(Number($textBox.val()) + 1);
            AuroraJS.Modules.UpdateCookieQuantity($textBox.val(), $textBox.parent().parent().find(".productImage").first().attr("colid"), $textBox.parent().parent().attr("sizeid"), $textBox.parent().parent().attr("id"));
            AuroraJS.Modules.calculateBasketTotal();
        	//reset the selected payment
            $('input[type=radio]:checked').click();
        }).bind('change', function () { $(this).click; });

        //get total price
        AuroraJS.Modules.calculateBasketTotal();

        $("#Voucher").click(AuroraJS.Modules.validateVoucher);
    }

    //check if the product cookie has been populated
    if ($.cookie('productbasket')) {
        //execute basket calculation.
        Aurora.Services.AuroraWebService.GetProductBasket($.cookie('productbasket').toString(), productBasketCall, AuroraJS.onError);
    }
    else {
    	$("#MainBasket").html("<h2 class='NoItemsHeader'>Items in your basket</h2><p class='NoItemsText'>There are no items to process in your basket</p>");
        $(".processCheckout").hide();
        $("#PaymentSchemas").hide();
        $("#PaymentSchemas").prev().hide();
        $("<div>You do not have any items in your basket</div>").dialog({ modal: true,
            resizable: false,
            title: "Error",
            buttons: { "Okay": function () { $(this).dialog("close") } }
        });
        if (AuroraJS.Bespoke.NoProductsBasket) {
        	AuroraJS.Bespoke.NoProductsBasket();
        }
    }
};

AuroraJS.Modules.ProcessPayment = function () {
    var newBasketString = "";
    if ($("#PaymentSchemas").val == "") {
        $("<div>Please select a payment gateway to attempt a checkout.</div>").dialog({ modal: true,
            resizable: false,
            title: "Error",
            buttons: { "Okay": function () { $(this).dialog("close"); } }
        }
    );
        return false;
    }
    var response = function (result) {
        if (result.Data == "login") {
            document.location = "Login.aspx?ReturnUrl=" + escape("/checkout.aspx");
        }
        if (result.Data instanceof Object && result.Result) {
            document.location = "CheckOut.aspx";
        }
        else {

            if (result.Data.toString().indexOf("Error:") > -1) {
                $("<div>" + result.Data.replace("Error:", "") + "</div>").dialog(
            { modal: true,
                resizable: false,
                title: "Error",
                buttons: { "Okay": function () { $(this).dialog("close") } }
            });
            }
            else if (result.Data.indexOf("<ul>") > -1) {
                $("<div>" + result.Data + "</div>").dialog({ modal: true,
                    resizable: false,
                    title: "Success",
                    width: '400',
                    buttons: { "Okay": function () { $(this).dialog("close") } }
                });
            }
        }


    };



    $("<div>Please wait while we attempt a checkout...</div>").dialog({ modal: true,
        resizable: false,
        title: "Processing"
    }
    );
	
	//set recurring months to 0 if invalid value is found
    var recurringMonths = $("#PaymentSchemasRecurringOptions").val();
    if ( /^\d+$/.test(recurringMonths)==false) {
    	recurringMonths = 0;
    }

    $.cookie('basketRecurringMonths', recurringMonths * 30);
    $.cookie('basketSchema', $("#PaymentSchemas").val());

    window.document.location ='/checkout.aspx';
    //Aurora.Services.AuroraWebService.ProcessBasketOrder($.cookie('productbasket'), "", $("#PaymentSchemas").val(), recurringMonths*30, response, onError);
    return false;
};

AuroraJS.Modules.UpdateCookieQuantity = function (quantity, color, size, id, isDelete) {
    var cookie = $.cookie('productbasket').toString().split(",");
    var productItems;
    var newCookie = "";
    color = color || "";
    for (var item = 0; item < cookie.length; item++) {
        if (cookie[item] != "") {
            var productItems = cookie[item].split("|");
            var isFound = productItems[0] == id
                      && productItems[3] == (color == "0" ? "" : color)
                      && productItems[4] == size;

            if (isFound && !isDelete) {
                productItems[1] = quantity;
            }


            if (isFound && isDelete) {
                //do nothing
            }
            else {
                newCookie += productItems.join('|') + ",";
            }


        }
    }

    $.cookie('productbasket', newCookie);

    AuroraJS.Modules.setBasketLink();
    if (newCookie == "") {
        document.location.reload();
    }
};

function GetPaymentRecurringOptions() {
	//Load recurring options this payment option support it and the client also has this enabled in their products management section.
	var schemaID = $('#PaymentSchemas').val(),
		response = function (result) {
			if (result.Data && result.Data.length > 0) {
				if (result.Data.length == 1 && result.Data[0].MonthValue == 0) {
					result.Data[0].Month = "Straight";
					$("#PaymentSchemasRecurringOptions_Container").hide();
				} else {
					var straightHTML = '';
					if (result.Data[0].MonthValue != 0) {
						straightHTML = '<option value="0" data-recurring-interest="0">Straight</option>';
					}
					$("#PaymentSchemasRecurringOptions_Container").show();
				}
				var basketTotal = Number($("#TotalVal").find(".conversions").attr("value"));
				if (result.Data[0].minimumBasketTotal == 0 || result.Data[0].minimumBasketTotal > basketTotal) {
					$("#PaymentSchemasRecurringOptions_Container").hide();
				}

				paymentSchemaDS = $("#PaymentSchemasRecurringOptions").html( straightHTML + $("#PaymentSchemasRecurringOptionsTmpl").render(result.Data));
				$("#PaymentSchemasRecurringOptions").change();
				
			} else {
				$("#PaymentSchemasRecurringOptions_Container").hide();
			}
		};

	Aurora.Services.AuroraWebService.GetPaymentRecurringOptions(schemaID, response, onError);
}
AuroraJS.Modules.GetPaymentSchemas = function () {
    var response = function (result) {
        if (result.Data.length > 0 && $("#PaymentSchemas").attr("class") != "") {
            paymentSchemaDS = $create(Sys.UI.DataView, {}, {}, {}, $get("PaymentSchemas"));
            paymentSchemaDS.set_data(result.Data);
            $(".processCheckout").show();
            GetPaymentRecurringOptions();
        	//set the event handler for the recurring option load
            $("#PaymentSchemasRecurringOptions").change(AuroraJS.Modules.calculateBasketTotal);
            $("#PaymentSchemas").change(GetPaymentRecurringOptions);
        }

        if (result.Data.length <= 0 && $("#PaymentSchemas").attr("class") != "") {
//            $("<div>Sorry but the checkout feature has been disabled as there are no available payment gateways.<br/> To Process your order.</div>").dialog(
//              { modal: true,
//                  resizable: false,
//                  title: "Error"
//              });
            $(".processCheckout").hide();
        }
    };

    Aurora.Services.AuroraWebService.GetPaymentSchemas(response, onError);
};


//get payment gateways
if (document.location.toString().toUpperCase().indexOf("BASKET.ASPX") > -1 && document.location.toString().toUpperCase().indexOf("RETURNURL=/BASKET.ASPX") == -1) {
    AuroraJS.Modules.GetPaymentSchemas();
    if (AuroraJS.Bespoke.ProductBasket) {
        AuroraJS.Bespoke.ProductBasket();
    }
}
