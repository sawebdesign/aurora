﻿/*
	Membership Listing
	Aurora Plugin
	Author: Garth
	Creation Date: 2013/5/23

*/

(function ($, window, document, undefined) {
	$.views.helpers({
		stripCurrencyInfo: function (value) {
			return Number(value.replace(/[ Rr]/gi, ''));
		}
	});

	var methods = {
		init: function (options) {
			if ($('.scriptTemplates.MemberList').length === 0) {
				$('body').append('<div style="display:none" class="scriptTemplates MemberList">' + AuroraJS.Modules.GetTemplate("MemberList") + '</div>');
			}

			return this.each(function () {
				var $this = $(this);
				if (!$this.attr('init')) {
					$this.attr('init', true);
					var htmlOptions = {
						numberToGet: parseInt($this.attr('numbertoget'), 10),
						randomise: $this.attr('randomise'),
						categoryFilter: $this.attr('categoryfilter')
					};
					$this.MemberList('options', $.extend(htmlOptions, options));
					$this.MemberList('getMembers');
				}
			});
		},
		options: function (options) {
			var $this = $(this);
			var settings = $this.data('MemberListOptions') || $.fn.MemberList.options;
			var fullOptions = $.extend({}, settings, options);
			$this.data('MemberListOptions', fullOptions);
			return fullOptions;
		},
		getMembers: function () {
			return this.each(function () {
				var $this = $(this);
				var options = $this.MemberList('options');
				Aurora.Services.AuroraWebService.GetMemberShowcase(options.numberToGet, options.categoryFilter, options.randomise, events.getMembersSuccess, events.getFailed, $this);
			});
		},
		renderMembers: function (data) {
			return this.each(function () {
				var $this = $(this);
				var $container = $this.find('.showcaseContainer');
				if ($container.length === 0) {
					$container = $($('#memberlisttmpl').render({}));
				}
				var convertedData = utils._convertCustomXMLtoObject(data);
				var $members;
				if (convertedData && convertedData.length === 0) {
					$members = $('#memberlistnomemberstmpl').render({});
				} else {
					$members = $($('#memberlistitemtmpl').render(convertedData));
					
				}
				
				$container.append($members);
				$this.html($container);
			});
		}

	};

	var utils = {
		_convertCustomXMLtoObject: function (data) {
			for (var d = 0; d < data.length; d++) {
				if (data[d].CustomXML) {
					var $fields = $($.parseXML(data[d].CustomXML)).find('field');
					var xmlData = data[d].xmlData = {};
					for (var f = 0; f < $fields.length; f++) {
						var $field = $($fields[f]);
						xmlData[$field.attr('fieldname').replace(/ /gi, '')] = $field.attr('value');
					}
				}

			}
			return data;
		}
	};

	var events = {
		getMembersSuccess: function (result, $context) {
			$context.MemberList('renderMembers', result.Data);
		},
		getFailed: function (ex) {
			console.log(ex);
		}
	};

	$.fn.MemberList = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.MemberList');
		}
	};

	$.fn.MemberList.options = {
		numberToGet: 5000,
		randomise: false,
		categoryFilter: "",
		index: 0
	};

	AuroraJS.plugins.MemberList = function () {
		$('.MemberList').MemberList();
	};

})(jQuery, window, document);





