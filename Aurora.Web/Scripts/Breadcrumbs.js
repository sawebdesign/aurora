﻿/*globals AuroraJS Aurora onError confirm jQuery window Sys MSAjaxExtension */

AuroraJS.Modules.generateBreadCrumbs = function () {
    /// <summary>Breadcrumb system for aurora. Works by extracting the menu structure directly from the page and creating a breadcrumb from it.</summary>
    var homePage = "";
    var currentPage = $("#menu .current");
    var breadCrumb = $('<ul class="breadCrumbs"></ul>');
    if (currentPage.length > 0) {
        var currentItem = $('<span></span>').append(currentPage.children('a').clone());
        breadCrumb.append($("<li></li>").append(currentItem));
        if (!currentPage.hasClass('homeitem')) {
            while (currentPage.attr('id') !== "menu") {
                currentPage = currentPage.parent();
                if (currentPage.is('li:not(.homeitem)') && currentPage.is(':not(#menu>ul:not(.sf-menu)>li)')) {
                    breadCrumb.prepend($("<li></li>").append(currentPage.children('a').clone()));
                }
            }
            homePage = $("#menu .homeitem");
            breadCrumb.prepend($("<li></li>").append(homePage.children('a').clone()));
        }

    }
    else {
        homePage = $("#menu .homeitem");
        breadCrumb.prepend($("<li></li>").append(homePage.children('a').clone()));
    }
    breadCrumb.find('span.sf-sub-indicator').remove();
    var crumbContainer = $("#BreadCrumbs");
    crumbContainer.html(breadCrumb);
    if (AuroraJS.Bespoke.BreadCrumbs) {
        AuroraJS.Bespoke.BreadCrumbs(crumbContainer);
    }
    return crumbContainer;

};