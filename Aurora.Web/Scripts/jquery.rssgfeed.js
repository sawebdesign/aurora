﻿(function ($) {
	var feedReady = false;
	function loaded() {
		feedReady = true;
	}

	if (!window.google) {
		alert('You must include the Google AJAX Feed API script');
		return;
	}

	if (!google.feeds) google.load("feeds", "1");

	google.setOnLoadCallback(loaded);

	$.fn.rssgFeed = function (options) {
		var opts = jQuery.extend({
			target: this,
			max: 5,   // max number of items per feed
			feeds: []
		}, options || {});


		if (feedReady) {
			// Create a feed control
			var feedControl = new google.feeds.FeedControl();

			// Add feeds.
			for (var feed = 0; feed < options.feeds.length; feed++) {
				var currentItem = options.feeds[feed];
				feedControl.addFeed(currentItem[1], currentItem[0]);
			}

			// Options
			feedControl.setLinkTarget("google.feeds.LINK_TARGET_BLANK");
			if (options.max > -1) {
				feedControl.setNumEntries(options.max);
			}


			//assign an id if none specified
			if (options.target.id === "") {
				options.target.id = "RSS" + new Date().getMilliseconds();
			}

			//Draw it.
			feedControl.draw(document.getElementById(options.target.id));

			//Bespoke event
			if (AuroraJS.Bespoke.RSSFeedLoadComplete) {
				AuroraJS.Bespoke.RSSFeedLoadComplete();
			}
		}

		return this;
	};

})(jQuery);
