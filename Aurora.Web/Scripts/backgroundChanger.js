﻿/// <reference path="jquery-1.5.2.js" />
(function ($) {

    $.fn.extend({
        //pass the options variable to the function
        backgroundChanger: function (options) {
            var index = 0;
            var intialised = false;

            //Set the default values, use comma to separate the settings, example:
            var defaults = {
                changeTime: 4000,
                transitionTime: 1500,
                fadeInTime:4000,
                data: []
            }
           
            var options = $.extend(defaults, options);
            var o = options;

            $(document.body).css({ background: '' });
            var changeit = function (src, element) {
                if (!intialised) {
                    $(".mybackground").children('div').fadeIn(o.fadeInTime);
                    intialised = true;
                }

                //the element is aasumed to be visible therefore enabling a fade out
                $($(element).children('div')[index]).fadeOut(o.fadeInTime);


            };
            var element = this;
            return this.each(function () {
               
                var deferArray = {};

                for (var item in o.data) {
                    $("<div></div>").css({ width: '100%', height: '100%', display: 'none', position: 'absolute', background: 'url(' + o.data[item] + ')', zIndex: (1000 - item) }).appendTo(".mybackground");

                }


                setInterval(function () {
                    if (o.data[index] !== undefined) {
                        changeit(o.data[index], element);
                        index++;
                    }
                    else {
                        index = 0;
                        $(".mybackground").children('div').fadeIn(o.transitionTime);
                        changeit(o.data[index], element);
                        index++;
                    }

                }, o.changeTime);

            });
        }
    });

})(jQuery);