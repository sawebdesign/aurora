﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="modules.js" />
/// <reference path="jquery.ui.dialog.js" />

//#region Globals
var GlobalVariables = new function () {

    this.GlobalBase = function () {
        var t = this;
    };

    this.GlobalVar = function (wo) {
        var t = this;
        t.type = "Global Variables:#Aurora";
    };

    this.GlobalVar.prototype = new this.GlobalBase();
    this.GlobalVar.prototype.constructor = this.GlobalVar;
    this.GlobalVar.prototype.TimeOutID = 0;
    this.GlobalVar.prototype.simpleSlideTimerID = 0;
    this.GlobalVar.prototype.TimeOutID2 = 0;
    this.GlobalVar.prototype.simpleSlideTimerID2 = 0;
    this.GlobalVar.prototype.reloadMethod = null;
    this.GlobalVar.prototype.totalCount = 0;
    this.GlobalVar.prototype.currentPage = 0;
    this.GlobalVar.prototype.pageSize = 3;
    this.GlobalVar.prototype.globalDataSet = null;

    //This is used to prevent the updating of the "current" class menu item on content load for both product and page content.
    this.GlobalVar.prototype.updateCurrentMenuItem = true;


    this.GlobalVar.prototype.populateEntityFromForm = function (entity, formsource, formfieldprfxlength) {
        //if we've passed in an array of form elements then set the properties to the values in the array if they match
        if (formsource !== undefined) {
            if ((formfieldprfxlength == undefined) || (formfieldprfxlength == null))
                formfieldprfxlength = 0;

            for (var i = 0; i < formsource.length; i++) {
                //if (formsource[i].name.substr(formfieldprfxlength, entname.length + 1) == entname + "_") {
                if (typeof entity[formsource[i].name.substr(formfieldprfxlength)] != "undefined")
                    entity[formsource[i].name.substr(formfieldprfxlength)] = formsource[i].value;
                //}
            }
        }
    };

}
//#endregion

//#region String
String.prototype.isNullOrEmpty = function (replace) {
    /// <summary>Checks if a string is set to empty or null</summary>
    /// <param name="replace" type="String">If specified then returns that value, if not returns T|F</param>
    if (!this || this == "null") {
        if (!replace) {
            return true;
        }
        else {
            return replace;
        }
    }
    if (this == "") {
        if (!replace) {
            return true;
        }
        else {
            return replace;
        }
    }
    return this;
};
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

function shortenString(stringValue, length) {
    if (stringValue.length + 3 > length) {
        stringValue = stringValue.substr(0, length) + "...";
    }

    return stringValue;
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1); });
}

function roundTo(val, decimalPlaces) {
	var multi = Math.pow(10, decimalPlaces);
	var retVal = Math.round(val * multi) / multi;
	if (!isNaN(retVal)) {
		return retVal;
	} else {
		return 0;
	}
}
//#endregion

//#region Url  
function getQueryVariable(variable) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="variable" type="String">
    ///		The name of the query string param that you are looking for
    ///	</param>

    var query = urldecode(window.location.search.substring(1));
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var check = pair[0];
        if (check.toLowerCase() === variable.toLowerCase()) {
            return pair[1];
        }
    }
    return null;
}

function urldecode(str) {
    str = str.replace('+', ' ');
    str = unescape(str);
    return str;
}
//#endregion

//#region JSSession  
function GetJSSession(getVar) {
    /// <summary>Gets the value of the requested getVar from the JSSession</summary>
    /// <param name="getVar" type="String">The name of the variable you require</param>
    return AuroraJS.Modules.JSSession[getVar];
};

function SetJSSession(setVar, value) {
    /// <summary>Set the variable setVar and value into the JSSession</summary>
    /// <param name="setVar" type="String">The name of the variable you want to set</param>
    /// <param name="value" type="String">The value of the variable you are settings</param>
    AuroraJS.Modules.JSSession[setVar] = value;
};
//#endregion


//#region Array Utils 

function findInArray(ary, idfield) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The name of the column that you want to interrogate
    ///	</param>

    for (var i = 0; i < ary.length; i++) {
        if (ary[i].name == idfield) {
            return ary[i].value;
        }
    }
    return null;
};

function findByInArray(ary, idfield, value, returnIndex) {
    ///	<summary>
    ///		Get elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The name of the column that you want to interrogate
    ///	</param>
    ///	<param name="value" type="Variable">
    ///		The value for the idfield column
    ///	</param>
    ///	<param name="returnIndex" type="Boolean">
    ///		The index value for the idfield column
    ///	</param>

    for (var i = 0; i < ary.length; i++) {
        if (value == ary[i][idfield]) {
            if (!returnIndex) {
                return ary[i];
            }
            else {
                return i;
            }

        }
    }
    return null;
};

function findItemInArray(ary, itemValue) {
    ///	<summary>
    ///		Get query string elements by their value
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="ary" type="Array">
    ///		The array that you want to interrogate
    ///	</param>
    ///	<param name="idfield" type="String">
    ///		The data you are looking for
    ///	</param>
    if (!ary) {
        return null;
    }
    for (var i = 0; i < ary.length; i++) {
        if (ary[i] == itemValue) {
            return ary[i];
        }
    }
    return null;
}
//#endregion

//#region DateTime Utils 
/// <summary>Calculates the amount of weeks in a month</summary>
/// <param name="year" type="Number">Year Value</param>
/// <param name="month_number" type="Number">Month Value</param>
function getWeekCount(year, month_number) {
    // month_number is in the range 1..12
    var firstOfMonth = new Date(year, month_number - 1, 1);
    var lastOfMonth = new Date(year, month_number, 0);

    var used = firstOfMonth.getDay() + lastOfMonth.getDate();

    return Math.ceil(used / 7);
}
function getWeekDay(dayNum) {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[dayNum];
}
function formatDateTime(dateTime, format) {
    /// <summary>Formats a UTC date to a short date string</summary>
    /// <param name="dateTime" type="Date">Represents a UTC date time format</param>
    /// <param name="format" type="String">format string when specified will format date</param>
    /// <returns type="String" />
    if (dateTime instanceof Date && dateTime.getFullYear() == 2050) {
        return dateTime.format("HH:mm");
    }
    if (!format) {
        format = "dd MMM yyyy";
    }

    if (!(dateTime instanceof Date)) {
        dateTime = new Date(dateTime);
    }

    return dateTime.format(format);
};

function daysBetween(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)

}

//#endregion

//#region HTML Utils 
function encodeHTML(html) {
    return String(html)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}
(function ($) {
    $.fn.decodeHTML = function () {
        var $this = $(this);
        var str = $this.html();
        str = str.replace(/&quot;/g, '"');
        str = str.replace(/&amp;/g, "&");
        str = str.replace(/&lt;/g, "<");
        str = str.replace(/&gt;/g, ">");

        $this.html(str);
        return this;
    }
})(jQuery);
//#endregion

//#region XML Utilities 

function getCustomXML() {
    /// <summary>Gets all the custom fields on a form and places them inside an XML tree</summary>
    /// <returns type="String" />

    var tmp = $('[name*="fld"]').serializeArray();
    if (tmp.length > 0) {
        var CustomXML = "<XmlData><Fields>";
        tmp.each(
            function (idx, Elme) {
                $item = $(this);
                if ($item.is('input') || $item.is('select')) {
                    CustomXML += "<field fieldname='" + $item.attr("name").replace("fld", "") + "' value='" + encodeHTML($item.val()) + "' />"
                }
                else if ($item.is('textarea')) {
                    CustomXML += "<field fieldname='" + $item.attr("name").replace("fld", "") + "' value='" + encodeHTML($item.html()) + "' />"
                }

            }
        );
        CustomXML += "</Fields></XmlData>";
        return CustomXML;
    }
    return null;
}
function LoadDataFromXML(xmlString, fieldName) {
    /// <summary>Returns a value from a specified field in XML</summary>
    /// <param name="xmlString" type="String">the data that needs to be searched</param>
    /// <param name="fieldName" type="String">The Field that contains the data</param>
    var xmlDoc = $.parseXML(xmlString);
    var item;
    $xml = $(xmlDoc);
    $fields = $xml.find('field');
    if (fieldName) {
        $.each($fields, function (i, field) {

            if ($(field).attr("fieldname") == fieldName) {
                item = $(field).attr('value');
            }
        });
    }
    else {
        return $xml;
    }

    return item;
}
function buildCustomXMLBySelector(selector, valueField, addTitle) {
    /// <summary>Takes any Jquery element selector and converts it into XML Data</summary>
    var tmp = selector;
    if (tmp.length > 0) {
        //xml string container
        var CustomXML = "<XmlData><Fields>";
        //iterate through control list
        for (var i = 0; i < tmp.length; i++) {
            //#region Variables 
            var titleField = addTitle ? $(tmp[i]).attr("title") : "";
            var isCheckBox = $(tmp[i]).is("input[type='checkbox']");
            var itemValue = valueField ? $(tmp[i]).attr(valueField) : $(tmp[i]).val();
            //#endregion
            //#region Exception for check boxes 
            if ($(tmp[i]).is("input[type='checkbox']")) {
                if ($(tmp[i]).is(":checked")) {
                    itemValue = "true";
                }
                else {
                    itemValue = "false";
                }
            }
            //#endregion
            
            //#region Exception for multiple select Drop downs 
            try {
                if (typeof (eval($(tmp[i]).val())) === "object" && $(tmp[i]).is("select[multiple]")) {
                    var tempObj = eval($(tmp[i]).val());
                    itemValue = "";
                    for (var item in tempObj) {
                        itemValue += tempObj[item] + ",";
                    }
                }
            } catch (e) {
                //eval fails just continue
            }
            //#endregion

            //#region Append Value 
            CustomXML += "<field fieldname='" + tmp[i].id.replace("fld", "") + "' value='" + encodeHTML(itemValue) + "' title='" + encodeHTML(titleField) + "' />";

            //#endregion
        }

        //terminate XML
        CustomXML += "</Fields></XmlData>";
        return CustomXML;
    }
    return null;
}
//#endregion

//#region Form Utils 
function postWebForm(url, data, successMethod, dataType) {
    $.post(url, data, successMethod, dataType);
}
function validateForm(formId, displayMessage, messageContainer, clearForm, elementID, instance) {
    /// <summary>Validates all input tags in a container.More work to do here only does text</summary>
    /// <param name="formId" type="String">Optional, searches a form element and validates input elements</param>
    /// <param name="displayMessage" type="Boolean">true to show a message false not to</param>
    /// <param name="name" type="String">ID of the element that will show the message</param>
    /// <param name="clearForm" type="Boolean">Clears the current forms element values</param>
    /// <param name="elementID" type="String">When populated validates a single element</param>
    /// <param name="instance" type="String">Represents a JQuery object/JQuery selector</param>

    var form;
    if (elementID == undefined) {
        if (!instance) {
            form = document.getElementById(formId).getElementsByTagName("input");
        }
        else {
            form = $(instance).find("input,select,textarea");
        }
    } else {
        form = $('#' + elementID + '');
    }
    var hasErrors = false;

    for (var element = 0; element < form.length; element++) {
    	//ignoring all hidden elements
    	if (form[element].getAttribute("valtype") != null && $(form[element]).is(":visible")) {

            var toValidate = form[element].getAttribute("valtype").split(";");
            var isRequired = false;
            if (form[element].getAttribute("valtype").toUpperCase().indexOf("REQUIRED") > -1) {
                isRequired = true;
            }
            for (i = 0; i < toValidate.length; i++) {
                var validate = toValidate[i].split(":");

                switch (validate[0].trim().toUpperCase()) {
                    default:
                        break;
                    case "REQUIRED":
                        var itemValue = form[element].value;
                        var $currentElement = $(form[element]);

                        if ($currentElement.is("textarea")) {
                            if ($.browser["msie"]) {
                                itemValue = $currentElement.html();
                            }
                            else {
                                itemValue = $currentElement[0].value;
                            }

                        }
                        if ($currentElement.is("select")) {
                            itemValue = $currentElement.val() == "NotSelected" ? -1 : $currentElement.val();
                            if (itemValue == "Not Selected") {
                                itemValue = "";
                            }
                        }
                        if ($currentElement.is("input[type='checkbox']")) {
                            itemValue = $currentElement.is(":checked") ? true : "";
                        }

                        if (itemValue == "" || itemValue == -1) {
                            hasErrors = true;
                            validateFail(form, formId, element);
                        }
                        else {
                            validatePass(form, formId, element);
                        }
                        break;
                    case "DATEREQ":
                        break;
                    case "REGEX":
                        if ((isRequired == true) || (form[element].value != "")) {
                            var pattern = validateRegex(validate[1]);
                            var val = form[element].value;

                            test = val.match(new RegExp(pattern, 'gi'));
                            if (test == null) {
                                hasErrors = true;
                                form[element].setAttribute("valmessage", validateRegexMessage(validate[1]));
                                validateFail(form, formId, element, displayMessage);
                            } else {
                                validatePass(form, formId, element, displayMessage);
                            }
                        }
                        break;
                    case "MUSTMATCH":
                        var elemToMatch = $("#" + validate[1]).val()
                        if (form[element].value == elemToMatch && elemToMatch != "") {
                            validatePass(form, formId, element);
                        }
                        else {
                            hasErrors = true;
                            if (elemToMatch == "") {
                                validateFail(form, formId, element, "Please enter a value above, then repeat that sequence in this text box");
                            }
                            else {
                                validateFail(form, formId, element, "Values do not match");
                            }

                        }
                        break;
                }
            }
        }
    }
    if (hasErrors && displayMessage) {
        DisplayMessage(messageContainer, "warn", "items in form have not been entered");
    }
    return hasErrors;
}

function validatePass(form, formId, element) {
    var icon = form[element].parentNode.lastChild;
    if (form[element].parentNode.lastChild.id == "icon_" + form[element].name) {
        form[element].parentNode.removeChild(icon);
    }
}

function validateFail(form, formId, element, displayMessage) {
    var icon = document.createElement("img");
    icon.src = "/Styles/images/exclamation.png";
    icon.id = "icon_" + form[element].name;
    if (form[element].getAttribute("valmessage") != null) {
        icon.title = form[element].getAttribute("valmessage");
    } else {
        icon.title = "Please enter a value for this field";
    }

    if (form[element].parentNode.lastChild.id != icon.id) {
        form[element].parentNode.appendChild(icon);
    }
    else {
        form[element].parentNode.lastChild.title = icon.title;
    }

    if (form[element].nodeName == "SELECT") {
        icon.title = "Please select an option for this field";
    }
}

function validateRegex(regType) {
    switch (regType.trim().toUpperCase()) {
        case "DATE":
            return "^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
        case "STRING":
            return "[a-z][A-Z]";
        case "INT":
            return "^\\d+$";
        case "PHONE":
            return "^[\\d]{10,12}$";
        case "PASSWORD":
            return "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$"; //Password matching expression. Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit
        case "EMAIL":
            return "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        default:
            return regType;
    }
}
function validateRegexMessage(regType) {
    switch (regType.trim().toUpperCase()) {
        case "STRING":
            return "Only text characters are allowed for this field";
        case "INT":
            return "Only numeric characters are allowed for this field";
        case "PHONE":
            return "Only phone numbers are allowed for this field";
        case "EMAIL":
            return "Only email addresses are allowed for this field";
        default:
            return "Please enter a value for this field.";
    }
}

function ClearForm() {
    for (var element = 0; element < form.length; element++) {
        if (form[element].type == "text")
            form[element].value = "";
    }
}

jQuery.fn.ForceNumericOnly =
function () {
    /// <summary>Only allows numeric values in a text field</summary>
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 13 ||
                key == 8 ||
                key == 9 ||
                key == 46 ||
                key == 190 ||
                key == 110 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        })
    })
};

function showHideColumn(colno, doshow, tableId) {
    /// <summary>shows or hides columns inside a specified table</summary>
    /// <param name="colno" type="Number">cell index to hide</param>
    /// <param name="doshow" type="Boolean">hide/show true = show false = hide</param>
    /// <param name="tableId" type="String">the table to which modifications must occur</param>

    var style;
    if (doshow) style = 'block'
    else style = 'none';

    var tbl = document.getElementById(tableId);
    var rows = tbl.getElementsByTagName('tr');

    for (var row = 0; row < rows.length; row++) {
        var cels = rows[row].getElementsByTagName('td')
        cels[colno].style.display = style;
    }
};
//#endregion

//#region UI Utils 
function DisplayMessage(containerId, messageType, message, timeOut) {
    /// <summary>Displays a relative message in a message box </summary>
    /// <param name="containerId" type="String">The unique identifier for the message place holder</param>
    /// <param name="messageType" type="String">This determines how the message looks the types are warn error info success</param>
    /// <param name="message" type="String">The text that will be displayed to the user</param>
    /// <param name="timeOut" type="Number">Sets the interval for the message to disappear in milliseconds.When 0 item does not disappear</param>

    var container = $("#" + containerId)[0];
    var text = $("<p/>")[0];
    var icon = $("<img/>")[0];

    container.innerHTML = "";
    text.style.textTransform = "capitalize";
    clearTimeout(GlobalVariables.GlobalVar.prototype.TimeOutID);

    //type
    switch (messageType.toUpperCase()) {
        case "WARN":
            container.className = "alert_warning";
            icon.src = "/Styles/images/icon_warning.png";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "INFO":
            icon.src = "/Styles/images/icon_info.png";
            container.className = "alert_info";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "ERROR":
            icon.src = "/Styles/images/icon_error.png";
            container.className = "alert_error";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
        case "SUCCESS":
            icon.src = "/Styles/images/icon_accept.png";
            container.className = "alert_success";
            icon.className = "mid_align";
            icon.alt = messageType.toLowerCase();
            text.appendChild(icon);
            text.innerHTML += message;
            container.appendChild(text);
            break;
    }

    $("#" + containerId).show();

    if (timeOut > 0) {

        GlobalVariables.GlobalVar.prototype.TimeOutID = setTimeout(function () {
            if ($("#" + containerId).is(":visible")) {
                $("#" + containerId).slideToggle();
            }
        }, timeOut);
    }

}
function showFancyBoxPopup(url, type, title, width, height) {
    if (!type) {
        type = "image";
    }
    if (!width) {
        width = '800';
    }
    if (!height) {
        height = '600';
    }

    $.fancybox(url, {
        padding: 0,
        titleShow: true,
        title: title,
        overlayColor: '#000000',
        overlayOpacity: 0.5,
        showActivity: true,
        type: type,
        width: width,
        height: height
    }
    );
    return false;
}
function fetchTemplate(moduleName) {
    /// <summary>Finds and returns a HTML template for any module</summary>
    /// <param name="moduleName" type="String">Name of the module you wish to retrieve the HTML template for</param>
    /// <returns type="HTML" />
    var template = null;

    if (GetJSSession("Modules")) {
        var myModules = GetJSSession("Modules");
        for (var module in myModules) {
            if (myModules[module].featureModules.Name.toUpperCase() == moduleName.toUpperCase()) {
                template = myModules[module].clientModules.HTML == null ? myModules[module].featureModules.HTML : myModules[module].clientModules.HTML;
                return template;
            }

        }
    }

    return template;
}
function scrollToElement(elementID, offset, duration) {
    /// <summary>Scrolls the page smoothly to the element ID specifed</summary>
    ///	<param name="elementID" type="String">
    ///		The ID of the element to scroll to.
    ///	</param>
    ///	<param name="offset" type="Int">
    ///		The offset to apply from the element. Negative numbers offset above. Positive numbers offset below.
    ///	</param>
    ///	<param name="duration" type="Int">
    ///		The duration of the animation in milliseconds. 
    ///	</param>
    if ($("#" + elementID).length == 0) {
        return;
    }
    if (offset == null) { offset = 0 };
    if (duration == null) { duration = 2000 };
    if (elementID != null) {
        $('html, body').animate({
            scrollTop: $("#" + elementID).offset().top + parseInt(offset, 10)
        }, duration);
    };
}

function scrollToSelector($selector, offset, duration) {
	/// <summary>Scrolls the page smoothly to the first element specifed in the jquery selector</summary>
	///	<param name="$selector" type="jQuery">
	///		jQuery selector
	///	</param>
	///	<param name="offset" type="Int">
	///		The offset to apply from the element. Negative numbers offset above. Positive numbers offset below.
	///	</param>
	///	<param name="duration" type="Int">
	///		The duration of the animation in milliseconds. 
	///	</param>
	if ($selector.length === 0) {
		return;
	}
	if (offset == null) { offset = 0 };
	if (duration == null) { duration = 2000 };
	if ($selector.length !== 0) {
		$('html, body').animate({
			scrollTop: $selector.offset().top + parseInt(offset, 10)
		}, duration);
	};
}
function DisplayLogin(container) {
    if (!($("#LoginForm").length > 0)) {
        container.html("<div id=\"LoginForm\"><table><tr><td>User Name:</td><td><input id=\"UserName\" valtype=\"required\"></td></tr><tr><td>Password:</td><td><input valtype=\"required\" id=\"Password\" type=\"password\"></td></tr><tr><td colspan='2'><input id=\"Login\" type=\"button\" value=\"Login\"></td></tr><tr><td colspan='2'><div id=\"QapTcha\">&nbsp;</div></td></tr></div>");
        $('#QapTcha').QapTcha({ buttonLock: '#Login', buttonFunc: LoginToPage });
    }

}
function showDefaultDialog(htmlMessage, title, opts) {
    /// <summary>Displays a jquery dialog</summary>
    /// <param name="htmlMessage" type="String">The content for the dialog</param>
    /// <param name="title" type="String">Title of the dialog</param>
    /// <param name="opts" type="Object">Options for JQuery UI, this will replace any other paramter values</param>

    if (opts) {
        $("<div>" + result.Data + "</div>").dialog(opts);
    }
    else {
        $("<div>" + htmlMessage + "</div>").dialog(
        {
            modal: true,
            resizable: false,
            title: title,
            width: '400',
            buttons: { "Okay": function () { $(this).dialog("close") } }
        });
    }
}
//#endregion

//#region Misc
function onError(result) {
	console.log(result);
}
function LoginToPage() {
    if (validateForm("LoginForm", false, '', false, null)) {
        return;
    }

    $("#Contents").block({ message: "Please wait while we log you in" });

    var login = function (result) {
        if (result.Data != "Failed Login") {
            //get the cached page templates from memory
            var myModules = GetJSSession("Modules");
            var pageHTML;
            //loop through the modules and find the page template html, the page module has an id of 12
            for (var module = 0; module < myModules.length; module++) {
                if (myModules[module].clientModules.FeaturesModuleID == 12)
                    pageHTML = myModules[module].clientModules.HTML;
            }

            if (pageHTML == null) {
                for (var module = 0; module < myModules.length; module++) {
                    if (myModules[module].featureModules.ID == 12) {
                        pageHTML = myModules[module].featureModules.HTML.toString();
                    }
                }
            }

            //create page template from client/feature templates
            $("#inputcontent").html('<div class="sys-template" id="spContainer">' + pageHTML.replace(/{{Image}}/g, "<img class='PageDetail_img' style='display:none;' sys:dataid='{{ID}}' sys:src='{{\"ClientData/\"+ClientSiteID+\"/Uploads/page_\"+ID+\".jpg\"}}' alt='Image' sys:id='{{ID + \"_img\"}}' />") + '</div>');
            //create MS AJAX dataset
            var dsSecurePage = $create(Sys.UI.DataView, {}, {}, {}, $get("spContainer"));
            result.Data[0].PageContent = result.Data[0].PageContent1;
            dsSecurePage.set_data(result.Data);
            //Load image 
            var image = $("#spContainer").find("img");
            AuroraJS.Modules.LoadImage(image.attr("src"), image.attr("id"));
            //decode the html
            $("#spContainer").decodeHTML();
            //load any modules that could be included in the content
            AuroraJS.Modules.LoadModulesInContent("#spContainer");
            $("#LoginForm").remove();
            $("#Contents").unblock();
        }
        else {
            $("#Contents").block({ message: "The User name and password did not match our records please try again." });
            setTimeout(function () { $("#Contents").unblock(); }, 5000);
        }
    };
    Aurora.Services.AuroraWebService.PageLogin($("#UserName").val(), $("#Password").val(), GetJSSession("PageID"), login, onError);

}
/// <summary>Keeps the server client connection alive</summary>
function keepAlive() {
    try {
        var request = function () { };
        Aurora.Services.AuroraWebService.KeepAlive(request, request);
    } catch (e) {
        alert("Please check your internet connection as I cannot connect to the server.")
    }
}

var xTemplate, xType
function initPagination(reloadData, TotalCount, CurrentPage, template, type, ds) {
    xTemplate = template;
    xType = type;
    AuroraJS.Modules.globals.reloadMethod = reloadData;
    // Create content inside pagination element
    $("#" + type.Name + "_Pagination").pagination(AuroraJS.Modules.globals.totalCount, {
        callback: pageselectCallback,
        items_per_page: AuroraJS.Modules.globals.pageSize,
        prev_text: '<<',
        next_text: '>>'
    });
}

//callback
function pageselectCallback(page_index, jq) {
    if (page_index > 0) {
        //skip loadList() on the first load or if causes and error
        AuroraJS.Modules.globals.currentPage = page_index;
        AuroraJS.Modules.globals.reloadMethod(xTemplate, xType, true)
    } else if (AuroraJS.Modules.globals.currentPage > 0 && page_index == 0) {
        //make sure you run loadList() if we are going back to the fist page
        AuroraJS.Modules.globals.currentPage = 0;
        AuroraJS.Modules.globals.reloadMethod(xTemplate, xType, true);
    }
    $("#Contents").unblock();
    return false;
}
var scriptsToLoad = [];

function loadScript(url, callback, index) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                script.onreadystatechange = null;
                if (callback) {
                    callback(index + 1);
                }
            }
        };
    } else {  //Others
        script.onload = function () {
            if (callback) {
                callback(index + 1);
            }
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

//#endregion
//#region Members
function ChangeMemberPassword() {
    /// <summary>Allows the updating of a members password</summary>
    var response = function (result, contextObj) {
        $(contextObj).dialog("close");
        var message = "<div>{0}</div>";
        if (result.Data) {
            switch (result.Count) {
                case 0:
                    message = String.format(message, "Your information was found, please ensure your membership is active.");
                    break;
                case 1:
                    message = String.format(message, "Your password has been changed.");
                    break;
                case -1:
                    message = String.format(message, "There was an unexpected error whilst trying to update your password please try again.");
                    break;
                case -2:
                    message = String.format(message, "Your password is invalid, please enter the correct password.");
                    break;

            }

            $(message).dialog(
        {
            modal: true,
            resizable: false,
            title: "Password Change",
            width: '400',
            buttons: { "Okay": function () { $(this).dialog("close"); } }
        });
        }

    };
    var requestChangePassword = function (dialogBox) {
        if (validateForm(undefined, "", "", false, undefined, ".changepassworddialog")) {
            return;
        }
        Aurora.Services.AuroraWebService.ChangeMemberPassword($("#MainContent_ID").val(), $(dialogBox).find("#oldpassword").val(), $(dialogBox).find("#newpassword").val(), $(dialogBox).find("#confirmpassword").val(), response, onError, dialogBox);
    };

    var changePasswordHTML = "<table class='changepassworddialog'><tr><td>Old Password:</td><td><input type='password' id='oldpassword' valtype='required' /></td></tr>";
    changePasswordHTML += "<tr><td>New Password:</td><td><input type='password' id='newpassword' valtype='required' /></td></tr>";
    changePasswordHTML += "<tr><td>Confirm Password:</td><td><input type='password' id='confirmpassword' valtype='required;mustmatch:newpassword' valmessage='Passwords do not match' /></td></tr></table>";
    $(changePasswordHTML).dialog(
        {
            modal: true,
            resizable: false,
            title: "Change Password",
            width: '400',
            buttons: { "Apply": function () { requestChangePassword(this); }, "Cancel": function () { $(this).dialog("close"); } }
        });
}
//#endregion

//#region JQuery Extensions
//#region $.fn.mapInputsToObject - Maps form element values to an object by field.
$.fn.mapToObject = function (object, field) {
    /// <summary>
    /// Maps form element values to an object by field name.
    /// Currently only maps text, hidden, checkbox, selects but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">(optional) The object to map values to / extend.</param>
    /// <param name="field" type="string">(optional) The field name to map values to.</param>
    /// <returns type="String" />
    object = object || {};
    field = field || "id";
    var inputs = this.find(':input');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select, textarea, input[type="password"]')) {
            if (i.hasClass("isDatePicker")) {
                object[i.attr(field)] = i.datepicker("getDate");
            } else {
                object[i.attr(field)] = (i.val() !== "") ? i.val() : null;
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            object[i.attr(field)] = i.is(":checked");
        } else if (i.is("input[type='radio']")) {

            if (i.is(":checked")) {
                object[i.attr(field)] = i.val();
            }
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion
//#region $.fn.applyObjecttoInputs- Maps object values to form elements by field.
$.fn.mapObjectTo = function (object, field, overwrite) {
    /// <summary>
    /// Maps object values to form elements by field.
    /// Currently only maps text, hidden, checkbox but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">The object to draw values from.</param>
    /// <param name="field" type="string">(optional) The field to use as key for value mapping.</param>
    /// <returns type="String" />
    object = object || {};
    overwrite = overwrite || false;
    field = field || "id";
    var inputs = this.find(':input, .field[' + field + ']');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select, textarea')) {
            var value;

            if (object[i.attr(field)] instanceof Date) {
                value = object[i.attr(field)].format("dd/MM/yyyy");
            }
            else {
                value = object[i.attr(field)];
            }


            if (overwrite) {
                i.val(value !== null ? value : "");
            }
            else {
                i.val(value !== null ? value : i.val());
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            if (object[i.attr(field)]) {
                i.attr('checked', 'checked');
            }
            else {
                i.removeAttr('checked');
            }
        }
        else if (i.is('.field')) {
            i.html(object[i.attr(field)] || "&nbsp;");
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion

//#endregion