﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="Utils.js" />
/// <reference path="jquery.tools.min.js" />
/// <reference path="common.js" /> 
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />
/// <reference path="jquery.history.js" />
/// <reference path="modules.js" />
/// <reference path="SubMenu.js" />

AuroraJS.Modules.CustomModuleData = function (dsModule, data) {
    if (data.Count > 0) {
        dsModule.set_data(data);
    }
};