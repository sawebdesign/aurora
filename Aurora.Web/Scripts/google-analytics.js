﻿/// <reference path="Utils.js" />

var _gaq = _gaq || [];

if (!GetJSSession("GAnalytics_AccountNumber")) {
    SetJSSession("GAnalytics_AccountNumber", gaCode);
}
_gaq.push(['_setAccount', '' + GetJSSession("GAnalytics_AccountNumber") + '']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_setAllowHash', false]);
_gaq.push(['_trackPageview']);


