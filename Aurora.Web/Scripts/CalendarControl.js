﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="Utils.js" />
/// <reference path="jquery.tools.min.js" />
/// <reference path="common.js" /> 
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />
/// <reference path="jquery.history.js" />
/// <reference path="modules.js" />

AuroraJS.Modules.currentDay = 0;
AuroraJS.Modules.currentMonth = 0;
AuroraJS.Modules.currentData = [];

AuroraJS.Modules.CreateCalendar = function (calendarId) {
    $(document).ready(function () {
        var getDates = function (result) {
            //Data returned from the server
            var data = result.Data;
            //represents the current day
            var d = new Date();
            //represents the current month
            var curMonth = null;
            //array holds the days that need to be added to the main JSON object that represent Weekly,Monthly and Yearly events
            var datesToAdd = new Array();
            //Gets all the days that have events 
            var Days = GetDays();
            //Represents the last day of the calendar to render
            var calendarEndDate = new Date(d.getFullYear(), d.getMonth() + 7, 0);

            function GetDays() {
                //used to keep track of the days the events occur on.
                var includedDays = new Array();
                var dataLength = data.length;
                for (var day = 0; day < dataLength; day++) {
                    var tmpStartDateArr = data[day].StartDateStr.split("/");
                    var date = new Date(parseInt(tmpStartDateArr[2]), (parseInt(tmpStartDateArr[1])-1), parseInt(tmpStartDateArr[0]));

                    //adds the orignal data returned from the server however if its a mothly occurence 
                    //and has a special filter then we need to do some work on it

                    if (data[day].Occurance == "Monthly" && data[day].CustomXML != null) {
                        var newDate = getNextSpecialOccurance(data[day]);

                        includedDays.push([newDate.getMonth() + 1, newDate.getDate(), newDate.getFullYear()]);
                        //add to lookup
                        if (!AuroraJS.Modules.currentData[newDate.getMonth() + 1 + "" + newDate.getDate() + "" + newDate.getFullYear()]) {
                            AuroraJS.Modules.currentData[newDate.getMonth() + 1 + "" + newDate.getDate() + "" + newDate.getFullYear()] = [data[day]];
                        }
                        else {
                            AuroraJS.Modules.currentData[newDate.getMonth() + 1 + "" + newDate.getDate() + "" + newDate.getFullYear()].push(data[day]);
                        }
                    }
                    else {
                        includedDays.push([date.getMonth() + 1, date.getDate(), date.getFullYear()]);
                        //add to lookup
                        if (!AuroraJS.Modules.currentData[date.getMonth() + 1 + "" + date.getDate() + "" + date.getFullYear()]) {
                            AuroraJS.Modules.currentData[date.getMonth() + 1 + "" + date.getDate() + "" + date.getFullYear()] = [data[day]];
                        }
                        else {
                            AuroraJS.Modules.currentData[date.getMonth() + 1 + "" + date.getDate() + "" + date.getFullYear()].push(data[day]);
                        }
                    }


                    //Checks if the event has an occurance flag and if so applies an occurance filter to 
                    //create fictious data that represent the occurance.
                    if (data[day].Occurance != "None") {
                        if (data.length >= 2) {
                            var incr = day + result.Data.length;
                        }
                        else {
                            var incr = day + 1;
                        }
                        var arrIndex = 0;
                        var occuranceDate = null;
                        switch (data[day].Occurance) {
                            case "Weekly":
                                //appends a week to the start date
                                occuranceDate = new Date(date.getMonth() + 1 + "/" + (date.getDate() + 7) + "/" + date.getFullYear());
                                break;
                            case "Monthly":
                                //appends a month to the start date
                                if (data[day].CustomXML == null) {
                                    occuranceDate = new Date(date.getMonth() + 2 + "/" + (date.getDate()) + "/" + date.getFullYear());
                                }
                                else {
                                    var newData = data[day];
                                    newData.StartDate = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                                    occuranceDate = getNextSpecialOccurance(newData);
                                }

                                break;
                            case "Daily":
                                //appends a day to the start date
                                occuranceDate = new Date(date.getMonth() + 1 + "/" + (date.getDate() + 1) + "/" + date.getFullYear());
                                break;
                            case "Yearly":
                                //appends a year to the start date
                                occuranceDate = new Date(date.getMonth() + 1 + "/" + (date.getDate()) + "/" + date.getFullYear() + 1);
                                break;
                        }

                        //used to store the events expiry date.
                        var tmpEndDateArr = data[day].EndDateStr.split("/");
                        var endDate = new Date(parseInt(tmpEndDateArr[2]), (parseInt(tmpEndDateArr[1]) - 1), parseInt(tmpEndDateArr[0]));

                        //check if event ends more than 7 months away. If so, set end date to 6 months in the future
                        if (daysBetween(new Date(), endDate) > 210) {
                            endDate = new Date(new Date().getFullYear(), date.getMonth() + 7, 0);
                        } else {
                            endDate = new Date(endDate.getMonth() + 1 + "/" + (endDate.getDate()) + "/" + new Date().getFullYear());
                        }


                        //Recursion appends dates to datesToAdd array.
                        while (occuranceDate <= endDate) {

                            //check if an item exists within the array if so find the next availible index.
                            if (includedDays[incr] === undefined) {
                                includedDays.push([occuranceDate.getMonth() + 1, occuranceDate.getDate(), occuranceDate.getFullYear()]);

                                //add to lookup
                                if (!AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()]) {
                                    AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()] = [data[day]];
                                }
                                else {
                                    AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()].push(data[day]);
                                }
                            }
                            else {

                                includedDays.push([occuranceDate.getMonth() + 1, occuranceDate.getDate(), occuranceDate.getFullYear()]);

                                //add to lookup
                                if (!AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()]) {
                                    AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()] = [data[day]];
                                }
                                else {
                                    AuroraJS.Modules.currentData[(occuranceDate.getMonth() + 1) + "" + occuranceDate.getDate() + "" + occuranceDate.getFullYear()].push(data[day]);
                                }

                            }

                            //create a partial event object to be relayed to the calendar.
                            var newData = new Object();
                            var indexCheck = arrIndex;
                            newData.StartDate = occuranceDate;
                            newData.Title = data[day].Title;
                            newData.ID = data[day].ID;

                            //check if an item exists within the array if so find the next availible index.
                            if (datesToAdd[indexCheck] == undefined) {
                                datesToAdd[indexCheck] = newData;
                            }
                            else {

                                datesToAdd.push(newData);

                            }

                            //increment
                            switch (data[day].Occurance) {
                                case "Weekly":
                                    occuranceDate = new Date(occuranceDate.getMonth() + 1 + "/" + (occuranceDate.getDate() + 7) + "/" + occuranceDate.getFullYear());
                                    break;
                                case "Monthly":
                                    //appends a month to the start date
                                    if (data[day].CustomXML == null) {
                                        occuranceDate = new Date(occuranceDate.getMonth() + 2 + "/" + (occuranceDate.getDate()) + "/" + occuranceDate.getFullYear());
                                    }
                                    else {
                                        var newData = data[day];
                                        newData.StartDate = new Date(occuranceDate.getFullYear(), occuranceDate.getMonth() + 1, 1);
                                        occuranceDate = getNextSpecialOccurance(newData);
                                    }
                                    break;
                                case "Daily":
                                    occuranceDate = new Date(occuranceDate.getMonth() + 1 + "/" + (occuranceDate.getDate() + 1) + "/" + occuranceDate.getFullYear());
                                    break;
                                case "Yearly":
                                    occuranceDate = new Date(occuranceDate.getMonth() + 1 + "/" + (occuranceDate.getDate()) + "/" + occuranceDate.getFullYear() + 1);
                                    break;
                            }
                            arrIndex++;
                            incr++;
                        }

                    }
                }

                //Add new occurance items.
                datesToAdd.push(data);


                return includedDays;
            };

            //run exclude to remove highlighted cells from calendar so only events display.
            function excludeDays(date) {
                var m = date.getMonth();
                var d = date.getDate();
                var y = date.getFullYear();

                var daysLength = Days.length;
                for (i = 0; i < daysLength; i++) {
                    try {
                        if ((m == Days[i][0] - 1) && (d == Days[i][1]) && (y == Days[i][2])) {
                            return [true];
                        }
                    }
                    catch (e) {
                        return [false];
                    }
                }
                return [false];
            }

            //create calendar
            $(function () {
                $("#" + calendarId).datepicker({
                    hideIfNoPrevNext: true,
                    beforeShowDay: excludeDays,
                    selectDay: function (year, month, inst) {
                        var x = month;
                    },
                    onChangeMonthYear: function (year, month, inst) {

                        if (year) {
                            curMonth = month;
                        }

                    },
                    onSelect: function (dateText, inst) {
                        //this fixes a bug on the calender when the user has clicked a day which causes the getDateFromData method to return void.
                        inst.selectedDay = parseInt(inst.selectedDay.toString().substr(0, 2).replace("<", ""));
                        AuroraJS.Modules.currentDay = inst.selectedDay;
                        $("#" + calendarId).val(+(inst.selectedMonth + 1 <= 9 ? "0" + (inst.selectedMonth + 1) : inst.selectedMonth + 1) + "/" + inst.selectedDay + "/" + new Date().getFullYear());

                    },
                    maxDate: calendarEndDate
                });

                //add tooltips
                $("#inputcontent").die("mouseover.AuroraEvents");
                $("#inputcontent").live("mouseover.AuroraEvents", this, function () {
                    $("#" + calendarId).find("a").hideTip();
                });

                $(".ui-datepicker-prev").die("mouseover.AuroraEvents");
                $(".ui-datepicker-prev").live("mouseover.AuroraEvents", function () {
                    $("#" + calendarId).find("a").hideTip();
                })
                $(".ui-datepicker-prev").die("mouseup.AuroraEvents");
                $(".ui-datepicker-prev").live("mouseup.AuroraEvents", function () {
                    var cur = new Date($("#" + calendarId).val());
                    var newmonth = cur.getMonth() + 1;
                    var year = cur.getFullYear();
                    newmonth = newmonth - 1;
                    if (newmonth <= 0) {
                        newmonth = newmonth + 12;
                        year = year - 1;
                    }
                    var next = new Date(newmonth + "/" + cur.getDate() + "/" + year);
                    $("#" + calendarId).val(next.format('MM/dd/yyyy'));
                })

                $(".ui-datepicker-next").die("mouseover.AuroraEvents");
                $(".ui-datepicker-next").live("mouseover.AuroraEvents", function () {
                    $("#" + calendarId).find("a").hideTip();
                })
                $(".ui-datepicker-next").die("mouseup.AuroraEvents");
                $(".ui-datepicker-next").live("mouseup.AuroraEvents", function () {
                    var cur = new Date($("#" + calendarId).val());
                    var next = new Date(cur.getMonth() + 2 + "/" + cur.getDate() + "/" + cur.getFullYear());
                    $("#" + calendarId).val(next.format('MM/dd/yyyy'));
                })

                //hide tips
                $("#tips div").live('mouseleave', function () {
                    $("#" + calendarId).find("a").hideTip();
                });

                $("#" + calendarId).blur(function () {
                    $("#" + calendarId).find("a").hideTip();
                });


                //add show event & event data to tooltip
                $("#" + calendarId).die("mouseover");
                $("#" + calendarId).live("mouseover", this, function () {
                    $(this).find("a").each(
                            function () {
                                if (this.parentNode.tagName.toLowerCase() != "div") {
                                    $("#" + calendarId).hideTip();
                                    this.href = "javascript:void";
                                    this.className += " with-children-tip";
                                    this.onclick = function (e) {
                                        $("#" + calendarId).find("a").hideTip();
                                    };
                                    this.onmouseover = function (e) {
                                        $("#" + calendarId).find("a").hideTip();
                                        var events = AuroraJS.Modules.getDateFromData(this, data, calendarId, curMonth);

                                        var tip = document.createElement("input");
                                        tip.type = "hidden";
                                        tip.title = events;
                                        this.appendChild(tip);
                                        $(this).showTip({ onHover: true, delay: 300, position: "right", html: true })

                                    };


                                }
                            }
                        );
                });

            });
        };

        Aurora.Services.AuroraWebService.GetEvents(0, false, 0, 0, false, null, getDates, AuroraJS.onError);
    });
}

//create tooltip content
AuroraJS.Modules.getDateFromData = function (value, data, calendarId, curMonth) {
    var itemNames = "";
    var nowItem = $(value);

    var cur = new Date($("#" + calendarId).val());
    var curData = "";
    if (curMonth == null || curMonth == NaN) {
        curMonth = cur.getMonth() + 1;
        curData = AuroraJS.Modules.currentData[cur.getMonth() + 1 + "" + nowItem.html() + cur.getFullYear()];
    }
    else {
        curData = AuroraJS.Modules.currentData[curMonth + "" + nowItem.first().text() + cur.getFullYear()];
    }


    if (curData == undefined) {
        return "";
    }
    AuroraJS.Modules.globalHoverData = curData;
    for (var elemt = 0; elemt < curData.length; elemt++) {
        itemNames += "<a dataid='" + curMonth + "" + nowItem.html() + cur.getFullYear() + "' mylen='" + curData[elemt].ID + "' onclick=\"AuroraJS.Modules.globalHover(this);\" href=\"javascript:void(0);\">" + curData[elemt].Title + "</a><br/><br/>";
    }

    return itemNames + "<br/>Click the event title to read more";
}

AuroraJS.Modules.globalHoverData = null;
AuroraJS.Modules.globalHoverLength = 0;
AuroraJS.Modules.globalHover = function (anchor) {
    $("#EventCalendar").find('a').hideTip();
    var item = findByInArray(AuroraJS.Modules.currentData[anchor.getAttribute("dataid")], "ID", anchor.getAttribute("mylen"));
    if (!item) {
        return "";
    }
    AuroraJS.Modules.GetContentForModule('Event', item.ID);
};

function getCurrentMonthWeekNumber(currentDate) {
    var remainder = parseInt(currentDate.getDate() % 7, 10);
    var week = parseInt(currentDate.getDate() / 7, 10);
    if (remainder > 0) {
        week = week + 1
    }
    return week;
}

function getNextSpecialOccurance(data) {
    var tmpStartDateArr = data.StartDateStr.split("/");
    var date = new Date(parseInt(tmpStartDateArr[2]), (parseInt(tmpStartDateArr[1]) - 1), parseInt(tmpStartDateArr[0]));
    var weekNumRepeat = Number(LoadDataFromXML(data.CustomXML, "Week"));
    var dayNumRepeat = Number(LoadDataFromXML(data.CustomXML, "Days"));

    //get the specified week
    var lastOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var firstOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
    //get the specified day
    if (firstOfMonth.format("dddd") == getWeekDay(dayNumRepeat - 1) && weekNumRepeat == 1) {
        return firstOfMonth;
    }
    else {
        if (weekNumRepeat < 4) {
            var maxWeeks = weekNumRepeat;
            var nextMonthlyDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var currentWeek = getCurrentMonthWeekNumber(nextMonthlyDate);

            //Loops through the days until it finds the next occurrence of the current event
            while (nextMonthlyDate.format("dddd") != getWeekDay(dayNumRepeat - 1) || maxWeeks != currentWeek) {
                //if the day is correct but the week is wrong, move forward a week
                if (nextMonthlyDate.format("dddd") == getWeekDay(dayNumRepeat - 1)) {
                    nextMonthlyDate.setDate(nextMonthlyDate.getDate() + 7);
                }
                //Day is incorrect so increment by 1
                else {
                    nextMonthlyDate.setDate(nextMonthlyDate.getDate() + 1);
                }
                //get the current week number and loop back
                currentWeek = getCurrentMonthWeekNumber(nextMonthlyDate);
            }

            return nextMonthlyDate;
        }
        else {
            var currentDay = lastOfMonth.getDate();
            var weekCount = getWeekCount(date.getFullYear(), date.getMonth()) - 1;

            while (lastOfMonth.format("dddd") != getWeekDay(dayNumRepeat - 1)) {
                currentDay = lastOfMonth.getDate() - 1;
                lastOfMonth = new Date(lastOfMonth.getFullYear(), lastOfMonth.getMonth(), currentDay);

            }

            return lastOfMonth;
        }
    }

    return firstOfMonth;
}
