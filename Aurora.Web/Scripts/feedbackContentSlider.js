﻿/*Date: 25-02-2014
Author:Vincent Majavu
usage: construct new class object with desired param values, then kick back and enjoy.
*/
function feedbackContentSlider(container,itemsPerSlide,liHeight) {
	this.container = container;
	this.timer = 5000;
	this.index = 0;
	this.itemHeight = parseInt(liHeight,10)>0? parseInt(liHeight,10) : 0;
	this.pos = 0;
	this.totalItems = $(container).find("li").length;
	this.itemsPerSlide = itemsPerSlide;
	this.itemsPerPage = Math.ceil(this.totalItems / itemsPerSlide);

	//run slider
	this.startSlide();
};

feedbackContentSlider.prototype.startSlide = function () {
	if (this.itemHeight > 0) {
		//safe check for the required css
		if ($(this.container).css("position") !== "relative") {
			$(this.container).css("position", "relative");
			$(this.container).css("overflow", "hidden");

			$(this.container).css("height",(this.itemsPerSlide*this.itemHeight));
		}
		if ($(this.container + "_Data").css("position") !== "absolute") {
			$(this.container + "_Data").css("position", "absolute");
		}
		this.slide();
	}
};

feedbackContentSlider.prototype.slide = function () {
	var thiz = this;
	var top = "";
	var scrollHeight = this.itemHeight * this.itemsPerSlide;
	this.index++;

	if (this.index < this.itemsPerPage) {
		top = "-"+(this.pos + scrollHeight) + "px";
		this.pos += scrollHeight; 
	} else {
		this.index = 0;
		this.pos = 0;
		top = "0px"
	}
	setTimeout(function () {
		$(thiz.container + "_Data").animate({ top: top }, 2000, "swing", function () {
			thiz.slide();
		});
	}, this.timer);
};