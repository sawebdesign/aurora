﻿/// <reference path="jquery-1.5.2.js" />
/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="Utils.js" />
/// <reference path="jquery.tools.min.js" />
/// <reference path="common.js" /> 
/// <reference path="jquery.blockUI.js" />
/// <reference path="jquery-ui-1.8.9.custom.min.js" />
/// <reference path="jquery.tip.js" />
/// <reference path="jquery.history.js" />
/// <reference path="modules.js" />
/// <reference path="SubMenu.js" />
/// <reference path="jquery.gfeed.js" />
//#region Init
$("document").ready(function () {
	AuroraJS.Modules.GetModules();
	setInterval(keepAlive, 30000);

}
);
//#endregion

//TODO move namespace into Aurora Modules
try {
	var AuroraJS = new function () {
		var globals = new GlobalVariables.GlobalVar();
		this.Modules = function () {
			var t = this;
		};
		this.Templates = {};
		this.Bespoke = {};
		this.plugins = {};

		//#region Constants 
		this.Modules.Calendar = true;
		this.Modules.ClientModules = {};        //This is a object with all the Module data
		this.Modules.templateDetail;
		this.Modules.templateDetailModule;
		this.Modules.userNavigated = true;
		this.Modules.JSSession = {};            //This is the JavaScript "session" used for global variables
		this.Modules.globals = new GlobalVariables.GlobalVar();
		this.Modules.jsFiles = {};
		this.Modules.PagedData = null;
		this.Modules.FromPage = false;
		this.Modules.bannerInterval = {};
		this.Modules.Members = false;
		//#endregion

		//#region Module Functions 
		this.Modules.GetModules = function () {
			/// <summary>Gets the modules data and applies it to its template</summary>
			/// <param name="moduleList" type="String">comma separated module IDs</param>
			var request = function (result) {
				AuroraJS.Modules.ClientModules = result.Data;

				for (var index = 0; index < result.Data.length; index++) {
					if (result.Data[index].clientModules.Name !== "") {
						var file = null;
						var ccsFile = null;
						//Load all module data into session
						SetJSSession("Modules", result.Data);
						//get the ConfigXML and set the variables into the JSSession
						if (result.Data[index].clientModules.ConfigXML || result.Data[index].featureModules.ConfigXML) {
							file = (result.Data[index].clientModules.ConfigXML !== null) ? result.Data[index].clientModules.ConfigXML : result.Data[index].featureModules.ConfigXML;
							xmlDoc = $.parseXML(file);
							$xml = $(xmlDoc);
							$fields = $xml.find('field');

							$.each($fields, function (i, field) {
								SetJSSession($(field).attr('fieldname'), $(field).attr('value'));
							});
						}
						
						var moduleName = $.trim(result.Data[index].featureModules.Name);
						if (AuroraJS.plugins[moduleName]) {
							AuroraJS.plugins[moduleName].apply(this, arguments);
						};

						//Load the module
						AuroraJS.Modules.BindData(result.Data[index].clientModules, result.Data[index].featureModules);

					}
				}

				//Trigger an event to indicate JSSession is ready to be queried. Any listeners to this event will then fire their code.
				$.event.trigger({ type: "OnJSSessionReady" });

				//Login menu if neccessary
				if (GetJSSession("Member_MenuOnOff") === "false") {
					AuroraJS.Modules.createMemberMenu();
				}

				AuroraJS.Modules.createMemberMenu();
				//Ajax history: Bind a handler for ALL hash/state changes
				if ($.history) {
				    $.history.init(function (url, isClick) {

				        //#8157: Share it Bug
				        if (url.indexOf("sthash") > -1) {
				            return;
				        }
						url = url.replace("/", "");
						var varArray = url.split("/");
						var itemModule = varArray[0];
						var itemID = varArray[1];
						var itemTitle = varArray[2];
						if (!isClick && url !== "") {
							AuroraJS.Modules.GetContentForModule(itemModule, itemID, itemTitle, 'true');

						}
						//check for Google Analytics and track if available
						if (GetJSSession("GAnalytics_AccountNumber")) {
							try {
								_gaq.push(['_trackPageview', url]);
							} catch (e) {

							}
						}
					});
				}
				try {
					$('#QapTcha').QapTcha({});
				} catch (e) {
					$('input[name="SendMail"]').bind("click", function () {
						AuroraJS.Modules.SendMail(this);
					});
				}
				//Super Fish
				var checkSuperFish = document.getElementsByTagName('link');
				var superfishCount = 0;
				for (var styleSheet = 0; styleSheet < checkSuperFish.length; styleSheet++) {
					if (checkSuperFish[styleSheet].href.toString().indexOf("superfish.css") > -1) {
						if ($("#menu").length === 0) {
							document.location.reload(1);
						}
						var menuDOM = $("#menu").html();
						var menuAnimations = GetJSSession("MyMenu_Animation").toString().split(",");
						var animationObj = {};

						for (var item in menuAnimations) {
							animationObj[menuAnimations[item]] = 'show';
						}

						$("#menu").html(menuDOM);
						$("ul.sf-menu").supersubs({
							minWidth: Number(GetJSSession("MyMenu_MinWidth")),
							extraWidth: 0.1
						}).superfish({
							delay: parseInt(GetJSSession("MyMenu_ShowDelay")),
							animation: animationObj,
							speed: Number(GetJSSession("MyMenu_Speed")),
							autoArrows: Boolean.parse(GetJSSession("MyMenu_Arrows")),
							dropShadows: Boolean.parse(GetJSSession("MyMenu_Shadow")),
							disableHI: Boolean.parse(GetJSSession("MyMenu_HoverIntent")),
							onInit: function () {
								$('#Menu').show();
								if (AuroraJS.Bespoke.Menu) {
									AuroraJS.Bespoke.Menu(this);
								}
							}
						});
						//load member menu
						if (AuroraJS.Modules.generateSubMenu) {
							AuroraJS.Modules.generateSubMenu();
						}
						++superfishCount;
					}
				}
				if (superfishCount === 0 && $('#Menu[data-newmenu]').length < 0) {
					//Exception  for non superfish menus like manning road.
					var menuDOM = $("#menu").html();
					$("#menu").html('<ul style=" list-style: none;"><li>' + menuDOM + '</li></ul>');
					$("#Menu").show();
				};
				if ($('#Menu[data-isresponsive]').length > 0) {
					if (AuroraJS.Templates.ResponsiveMenuButtonTemplate) {
						$('#Menu .responsenavbtn').append(AuroraJS.Templates.ResponsiveMenuButtonTemplate.render({}));
					}
					if (AuroraJS.Bespoke.ResponsiveMenuInit) {
						AuroraJS.Bespoke.ResponsiveMenuInit(this);
					}
					$('#Menu .responsenavbtn').click(function () {
						$('.sf-menu').toggleClass("showmenu");
					});
					$('#Menu .responsenavsub').click(function () {
						$(this).parent().toggleClass("showdrop");
					});
				}
			};

			Aurora.Services.AuroraWebService.GetClientModules(request, AuroraJS.onError);
		};
		//used for articles temporary
		this.Modules.myArticle = "";
		this.Modules.BindData = function (template, type, paginated, filter) {
			/// <summary>Creates templates in page and injects their data:: Item only fires on a page request>
			/// <param name="type" type="String">Name of the template to load</param>
			/// <param name="template" type="String">The JSON array of the object to be bound</param>

			try {
				//Varibles
				var dataSet = null;
				var instanceID = null;

				//check for a filter
				if (!filter) {
					filter = null;
				}

				if (!paginated) {
					//remove any unwanted spaces within name
					type.Name = $.trim(type.Name);

					// This switch is to execute functions for modules where there is no template to bind data to.
					switch (type.Name) {
						case "BreadCrumbs":
							AuroraJS.Modules.generateBreadCrumbs();
							break;
						case "Products":
							AuroraJS.Modules.loadBasketFromCookies();
							var url = document.location.toString().replace('http://' + document.domain, "");
							//Doublechecks whether this is a products url
							if ((url.toUpperCase().indexOf("/PRODUCTS/") === 0) || (url.toUpperCase().indexOf("/PRODUCT/") === 0)) {
								//if the url does not contain a hash string, create a new hash. 
								//Otherwise, ignore and continue with existing hash.
								if (url.indexOf("#") < 0) {
									if ($.history) {
										$.history.load(url, true);
									}
								}
							}
							break;
						case "CustomScrollBar":
							if ($.tiny) {
								AuroraJS.Modules.bindCustomScrollBars("#inputcontent .customScrollBar");
							}
							break;

							//#region CMSZone 
						case "CMSZone":
							var response = function (result) {
								if (result.Data.length > 0) {
									for (var i = 0; i < result.Data.length; i++) {
										var page = result.Data[i];
										$('.cmszone[pageid=' + page.ID + ']').each(function () {
											var cmszone = $(this);
											var currentTemplate = template.HTML !== null ? template.HTML : type.HTML;
											if (currentTemplate === null) {
												cmszone.html(page.PageContent1);
											}
											else {
												cmszone.addClass("sys-template");
												cmszone.attr('sys:attach', 'dataview');
												cmszone.html(currentTemplate);
												var cmsdataSet = $create(Sys.UI.DataView, {}, {}, {}, cmszone.get(0));
												cmsdataSet.set_data(page);
												cmszone.decodeHTML();

											}
											if (AuroraJS.Bespoke.CmsZoneAfterContentLoad) {
												AuroraJS.Bespoke.CmsZoneAfterContentLoad();
											}
											AuroraJS.Modules.LoadModulesInContent(cmszone);
										});
									}
								}
							};

							var pageIds = [];

							$('.cmszone').each(function (indx, ele) {

								if ($(ele).attr("pageid") && $(ele).attr("pageid") !== "") {
									pageIds.push($(ele).attr("pageid"));
								}
							});
							Aurora.Services.AuroraWebService.GetBulkContent(pageIds, response, onError);

							break;
							//#endregion

							//#region ExternalForms 
						case "ExternalForms":
							//get all to be form elements
							$('.formext').each(function (indx, ele) {
								var form = $(this);
								$(String.format("<form action='{0}' name = '{3}' method='{1}'>{2}</form>", form.attr("action"), form.attr("method"), form.html(), form.attr("name"))).insertAfter(form);
								form.remove();
							});
							break;
							//#endregion

							//#region Feeds 
						case "RSSFeeds":
							$(".rssfeeds").each(function () {
								var $currentElement = $(this);
								//get feed global limit
								var max = GetJSSession("RSSFeed_Max") ? GetJSSession("RSSFeed_Max") : 3;

								//global limit override
								if ($currentElement.attr("max")) {
									max = $currentElement.attr("max");
								}

								//get feeds in JSON format '{link:'http://www.myfeeds.com/feeds.rss',title:'My Feed'}'
								var feeds = eval($currentElement.attr("feedLinks"));

								//call feed plugin
								$currentElement.rssgFeed({ target: this, max: max, feeds: feeds });
							});
							break;
							//#endregion
					}
					//find the template container if not found ignore.
					var element = $("#" + type.Name)[0];
					var currentTemplate = template.HTML !== null ? template.HTML : type.HTML;
					//update for multiple instance modules
					var classBased = false;
					if (!element && $(String.format(".{0}", type.Name)).length > 0) {
						element = $(String.format(".{0}", type.Name));
						classBased = true;
					}
					if (element === undefined || currentTemplate === null) {
						return;
					}

					currentTemplate = currentTemplate.replace("{{Image1}}", "<img class='" + type.Name + "_img1' style='display:none;' sys:dataid='{{ID}}' sys:id='{{ID + \"_img1\"}}' alt='Image1' />");
					currentTemplate = currentTemplate.replace("{{Image2}}", "<img class='" + type.Name + "_img2' style='display:none;' sys:dataid='{{ID}}' sys:id='{{ID + \"_img2\"}}' alt='Image2' />");
					currentTemplate = currentTemplate.replace("{{Image}}", "<img class='" + type.Name + "_img' style='display:none;' sys:dataid='{{ID}}' alt='Image' sys:id='{{ID + \"_img\"}}' />");

					if (!classBased) {
						if ($("#" + type.Name).children('.placeholder').length != 0) {
							$("#" + type.Name).children('.placeholder').remove();
						}
						//Check if the template container is empty or due to our editor which by default sets an nbsp; as a placeholder
						if ($("#" + type.Name).html().toString().trim() === "" || $("#" + type.Name).html().toString() === "&nbsp;") {
							$("#" + type.Name).html(currentTemplate);
						}

						//append attributes to make item queryable by MSAjax 
						$("#" + type.Name).find(".sys-template").attr('sys:attach', 'dataview');
						$("#" + type.Name).find(".sys-template").attr('id', type.Name + '_Data');
						$("#" + type.Name + '_Data').attr("instanceid", type.Name + new Date().getMilliseconds().toString());
						instanceID = $("#" + type.Name + '_Data').attr("instanceid");


						//Check if the sys-template item was found if so create a global dataset to bind to
						if ($("#" + type.Name + "_Data[instanceid='" + instanceID + "']").length > 0) {
							dataSet = $create(Sys.UI.DataView, {}, {}, {}, $("#" + type.Name + "_Data[instanceid='" + instanceID + "']")[0]);
							globals.globalDataSet = dataSet;
						}
							//this will just break the method so return
						else if (type.Name.indexOf("Banner") < 0) {
							return;
						}
					}
					else {
						//append attributes to make item queryable by MSAjax 
						if (type.AppendTemplatesToBody) {
							if ($('.scriptTemplates.' + type.Name).length === 0) {
								$('body').append('<div style="display:none" class="scriptTemplates ' + type.Name + '">' + currentTemplate + '</div>');
							}
						}
						else {
							$("." + type.Name).html(currentTemplate);
							$("." + type.Name)
							.find(".sys-template")
							.attr('sys:attach', 'dataview')
							.attr("instanceid", type.Name + new Date().getMilliseconds().toString());
							instanceID = $("." + type.Name).find(".sys-template").attr("instanceid");
						}
					}

				} else {
					dataSet = globals.globalDataSet;
				}


				//Calls the web service to obtain module data to bind to. Determined by module name.
				switch (type.Name) {

					//#region Articles                       
					case "Articles":
						var catFilter = $("#Articles").attr('catfilter');
						var request = function (result) {

							AuroraJS.Modules.myArticle = $("#Articles").attr('artcatfilter');
							if (result != null && result.Data !== "" && result.Result) {
								if (result.Data == "{ \"NewDataSet\": }}") {
									$("#" + type.Name).append("No Articles Available");
									return;
								}
								var newsData = $.parseJSON(result.Data);


								SetJSSession("Articles", newsData.NewDataSet.Table);
								dataSet.set_data(newsData.NewDataSet.Table);
								AuroraJS.Modules.DecodeHTML(type.Name + "_Data");

								var categories = $("#Articles [catid]");
								var totalCat = {};



								$("#" + type.Name).find("a").each(function () {
									var dataId = $(this).attr('dataid');
									var dataDesc = $(this).attr('datadesc');
									$(this).attr("href", "/News/" + dataId + "/" + dataDesc.replace(/ /gi, "-"));
									$(this).bind("click", function () {
										AuroraJS.Modules.GetContentForModule("News", dataId, dataDesc.replace(/ /gi, "-"));
										return false;
									});
								});

							}
							else {
								$("#" + type.Name + "_Data").hide();
								$("#" + type.Name).append("No Articles Available");
							}

							//remove whats greater than 5
							$("#Articles_Data").children('li').each(
                                function (index) {
                                	if (index >= 5) {
                                		$(this).remove();
                                	}
                                }
                            );
							$(type.Name).unblock();

						};

						Aurora.Services.AuroraWebService.GetNewsWithFilter(catFilter, request, AuroraJS.onError);
						break;
						//#endregion                                                                                                                                 

						//#region Banners                         
					case "BannerButton": case "BannerMain": case "Banner": case "BannerBottom":
						AuroraJS.Modules.LoadBanners(type);
						break;
						//#endregion                         

						//#region BreadCrumbs                       
					case "BreadCrumbs":
						AuroraJS.Modules.generateBreadCrumbs();

						break;
						//#endregion                                                                                                                                 

						//#region Custom Modules                         
					case "CustomModules":
						var customFormFilter = $("#" + type.Name).attr("catfilter");

						if (!customFormFilter) {
							customFormFilter = $("#" + type.Name).attr("filter");
							$("#" + type.Name).attr("catfilter", customFormFilter);
						}

						var response = function (result) {
							if (result.Data) {
								var $customHTML = $(result.Data),
                                formID = $customHTML.find("table").first().attr("ID");
								//append QapTcha
								//$customHTML.append("<tr><td><div class='QapTcha' id='Qaptcha2'></div></td></tr><tr><td><input type='button' id='sendRequest' value='Submit'/></td></tr>");

								$("#" + type.Name).html($customHTML);

								$("#" + type.Name).append("<div class='QapTcha' id='Qaptcha2'></div><div><input type='button' data-formName='" + result.Name + "' id='sendRequest' value='Submit'/></div>");

								//submit google analytics track event for visiting the form
								if (_gaq) {
									_gaq.push(['_trackEvent', 'CustomForms', 'FormLoad', result.Name]);
								}

								//create form lock
								$("#Qaptcha2").QapTcha({
									buttonLock: "#sendRequest"
                                , buttonFunc: function () {
                                	//success
                                	var request = function (result) {
                                		if (result.Result) {
                                			//submit google analytics track event
                                			if (_gaq) {
                                				_gaq.push(['_trackEvent', 'CustomForms', 'SubmitSuccess', $("#sendRequest").attr("data-formName")]);
                                			}

                                			var htmlMessage = $("#CustomFormMessage[customform=" + customFormFilter + "]").length > 0 ? $("#CustomFormMessage[customform=" + customFormFilter + "]").html() : "<div>Your details were captured successfully.</div>"
                                			$(htmlMessage).dialog({
                                				modal: true,
                                				resizable: false,
                                				title: "Success",
                                				buttons: { "Okay": function () { $(this).dialog("close") } }
                                			});
                                		}
                                		else {

                                			if (_gaq) {
                                				_gaq.push(['_trackEvent', 'CustomForms', 'SubmitFailed', $("#sendRequest").attr("data-formName")]);
                                			}

                                			var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
                                			$(htmlMessage).dialog({
                                				modal: true,
                                				resizable: false,
                                				title: "Error",
                                				buttons: { "Okay": function () { $(this).dialog("close") } }
                                			});
                                		}

                                		//empty function
                                		$("#sendRequest").unbind('click');
                                	};

                                	$("#sendRequest").attr("disabled", "disabled");

                                	if (validateForm(undefined, "", "", false, undefined, "#tbl_Form")) {
                                		$("<div>Please review the form as some required fields have not been entered.</div>").dialog({
                                			modal: true,
                                			resizable: false,
                                			title: "Error",
                                			buttons: { "Okay": function () { $(this).dialog("close") } }
                                		});
                                		$("#sendRequest").removeAttr("disabled");
                                		return;
                                	}

                                	Aurora.Services.AuroraWebService.SaveCustomModuleData(buildCustomXMLBySelector($("#tbl_Form input[type=text],#tbl_Form input[type=checkbox],#tbl_Form select,#tbl_Form textarea"), "value"), $("#" + type.Name).attr("catfilter"), request, onError);
                                }
								});

								//Bespoke event that executes after form load
								if (AuroraJS.Bespoke.CustomModuleLoad) {
									AuroraJS.Bespoke.CustomModuleLoad(customFormFilter);
								}
							}
						};

						if (Number(customFormFilter) && $("#" + type.Name + "[catfilter=" + customFormFilter + "] #CustomModules_Data").length === 1) {
							Aurora.Services.AuroraWebService.GetCustomModuleHTML(Number(customFormFilter), response, onError);

						}
						break;
					case "CustomModuleData":
						var response = function (result) {
							if (AuroraJS.Modules.CustomModuleData) {
								AuroraJS.Modules.CustomModuleData(dataSet, result.Data);
							}
						};
						Aurora.Services.AuroraWebService.GetCustomModuleData(filter, getData, onError);
						break;
						//#endregion                         

						//#region Custom Scroll Bar                         
					case "CustomScrollBar":
						if ($.tiny) {
							AuroraJS.Modules.bindCustomScrollBars("#inputcontent .customScrollBar");
						}
						break;
						//#endregion                           

						//#region Events                       
					case "Events":

						var eventsCall = function (result) {
							if (result.Data.length > 0 && result.Result) {
								var trimmedDS = [result.Data[0], result.Data[1], result.Data[2], result.Data[3], result.Data[4]]
								dataSet.set_data(result.Data);
								//store to session
								SetJSSession("Event", result.Data);
								//remove encoded html
								AuroraJS.Modules.DecodeHTML(type.Name + "_Data");
							} else {
								$("#" + type.Name + "_Data").hide();
								$("#" + type.Name).append("There are no upcoming events.");
							}
							if (AuroraJS.Modules.Calendar) {
								AuroraJS.Modules.CreateCalendar("EventCalendar");
							}
							$(type.Name).unblock();

							$("#" + type.Name).find("a").each(function () {
								var dataId = $(this).attr('dataid');
								var dataDesc = $(this).attr('datadesc');
								if (!dataId && !dataDesc) {
									return;
								}
								$(this).attr("href", "/Event/" + dataId + "/" + dataDesc.replace(/ /gi, "-"));
								$(this).bind("click", function () {
									AuroraJS.Modules.GetContentForModule("Event", dataId, dataDesc.replace(/ /gi, "-"));
									return false;
								});
							});

							var images = $("#Events img");

							for (var items = 0; items < images.length; items++) {
								if (images[items].getAttribute("alt") == "Image1") {
									AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/sm_event_01_" + images[items].getAttribute("dataid") + ".jpg", images[items].id, GetJSSession("News_noImage"));
								} else if (images[items].getAttribute("alt") == "Image2") {
									AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/sm_event_02_" + images[items].getAttribute("dataid") + ".jpg", images[items].id);
								} 
							}

							if (AuroraJS.Bespoke.Events) {
								AuroraJS.Bespoke.Events();
							}

							if ($('#' + type.Name).attr('data-slide') && $('#' + type.Name).attr('data-slide').toLowerCase() == 'true') {
								$('#' + type.Name).BoltContentSlider();
							}
						};

						Aurora.Services.AuroraWebService.GetEvents(GetJSSession("Event_Limit"), false, 0, 0, GetJSSession("Event_CurrentWeek"), GetJSSession("Event_Category"), eventsCall, AuroraJS.onError);
						break;
						//#endregion                                                                                                                              

						//#region Gallerys Audio,Image,Video                      
					case "GalleryImages": case "GalleryVideo": case "GalleryAudio":
						var galleryType = type.Name.replace(/Gallery/gi, "");
						filter = $("#" + type.Name).attr("catfilter") || null;

						var onRequest = function (result) {
							if (result.Data !== "" && result.Result) {
								if (result.Data == "{ \"NewDataSet\": }}") {
									$("#" + type.Name).append("No Galleries Available");
									return;
								}

								if ($("#" + type.Name).attr("isbound")) {
									return;
								}

								var galleryData = $.parseJSON(result.Data);
								SetJSSession("GalleryImages", galleryData.NewDataSet.Table);
								//TODO :exclude galleries here
								dataSet.set_data(galleryData.NewDataSet.Table);
								$("#" + type.Name).attr("isbound", "true");
								AuroraJS.Modules.DecodeHTML(type.Name + "_Data");
							} else {
								$("#" + type.Name + "_Data").hide();
								$("#" + type.Name).append("No Galleries Available");
							}
							$(type.Name).unblock();
							//Used to hide galleries on the page
							var excludedGalleries = GetJSSession("GalleryImages_Excluded");
							if (excludedGalleries) {
								excludedGalleries = excludedGalleries.split(",")
							}
							else {
								excludedGalleries = undefined;
							}

							if (galleryType.toUpperCase() === 'IMAGES') {
								$("#" + type.Name).find(".GalleryImage").each(function () {

									var imageId = $(this).attr('imageid');
									var dataId = $(this).attr('dataid');
									var dataName = $(this).attr('dataname');
									var dataDesc = $(this).attr('datadesc');

									if (findItemInArray(excludedGalleries, dataName)) {
										$(".GalleryImage").each(
                                        function () {
                                        	if ($(this).attr("dataname") == findItemInArray(excludedGalleries, dataName)) {
                                        		$(this).remove();
                                        	}
                                        	else if ($(this).attr("dataid") == $("#GallerySlider").attr("catfilter") || $(this).attr("dataid") == $("#GallerySliderLarge").attr("catfilter")) {
                                        		$(this).remove();
                                        	}
                                        }
                                        );

									} else {


										if (dataName) {
											var img = '/ClientData/' + clientSiteID + '/Uploads/sm_gallery_01_' + imageId + '.jpg';
											$(this).attr('style', 'background-image:url(' + img + '?' + new Date().getMilliseconds() + ');');
											$('<div class="GalleryTitle">' + dataName + '</div>').appendTo(this);
											$(this).wrap('<div class="Gallery" />');
											$(this).attr('dataid', dataId);
											$(this).attr('dataname', dataName.replace(/ /gi, "-"));

											$(this).bind("click", function () {
												AuroraJS.Modules.GetContentForModule("GalleryImages", dataId, dataName.replace(/ /gi, "-"), undefined, true);
												return false;
											});
										}

									}
								});
								$($(".GalleryImage")).append("<div class='GalleryOverlay'></div>");
								// to hold the event bindings
								if (!$(".GalleryImage").attr("onclick")) {
									$(".GalleryImage").live("click", this, function () {
										AuroraJS.Modules.GetContentForModule("GalleryImages", $(this).attr("dataid"), $(this).attr("dataname"), undefined, true);
										return false;
									});
								}

								//}
							} else if (galleryType.toUpperCase() === 'VIDEO') {
								$("#" + type.Name).find(".GalleryVideo").each(function () {
									var imageId = $(this).attr('imageid');
									var dataId = $(this).attr('dataid');
									var dataName = $(this).attr('dataname');
									var dataDesc = $(this).attr('datadesc');
									var img = '/ClientData/' + clientSiteID + '/Uploads/sm_gallery_01_' + imageId + '.jpg';
									$(this).attr('dataid', dataId);
									$(this).attr('dataname', dataName.replace(/ /gi, "-"));
									$(this).attr('style', 'background-image:url(' + img + ')');
									$('<div class="GalleryTitle">' + dataName + '</div>').appendTo(this);
									$(this).wrap('<div class="Gallery" />');

									$(this).bind("click", function () {
										AuroraJS.Modules.GetContentForModule("GalleryVideo", dataId, dataName.replace(/ /gi, "-"), undefined, true);
										return false;
									});
								});
								$($(".GalleryVideo")).append("<div class='GalleryOverlay'></div>");
								if (!$(".GalleryVideo").attr("onclick")) {
									$(".GalleryVideo").live("click", this, function () {
										AuroraJS.Modules.GetContentForModule("GalleryVideo", $(this).attr("dataid"), $(this).attr("dataname"), undefined, true);
										return false;
									});
								}
							} else if (galleryType.toUpperCase() === 'AUDIO') {
								$("#" + type.Name).find(".GalleryAudio").each(function () {
									var imageId = $(this).attr('imageid');
									var dataId = $(this).attr('dataid');
									var dataName = $(this).attr('dataname');
									var dataDesc = $(this).attr('datadesc');
									var img = '/ClientData/' + clientSiteID + '/Uploads/sm_gallery_01_' + imageId + '.jpg';
									$(this).attr('dataid', dataId);
									$(this).attr('dataname', dataName.replace(/ /gi, "-"));
									$(this).attr('style', 'background-image:url(' + img + ')');
									$('<div class="GalleryTitle">' + dataName + '</div>').appendTo(this);
									$(this).wrap('<div class="Gallery" />');

									$(this).fancybox({
										'transitionIn': 'none',
										'transitionOut': 'none',
										'titlePosition': 'outside',
										'overlayColor': '#000000',
										'overlayOpacity': 0.8,
										'showActivity': true,
										'href': '/UserControls/xspf_player.swf?playlist_url=/GalleryAudioPlayList.ashx?categoryid=' + dataId + '',
										'type': 'iframe',
										'titleFormat': function (title) {
											return '<span id="fancybox-title-over">' + (title.length ? ' &nbsp; ' + title : '') + '</span>';
										},
										'title': '' + dataName + ''
									});

								});
								$($(".GalleryAudio")).append("<div class='GalleryOverlay'></div>");
								if (!$(".GalleryAudio").attr("onclick")) {
									$(".GalleryAudio").live("click", this, function () {
										AuroraJS.Modules.GetContentForModule("GalleryAudio", $(this).attr("dataid"), $(this).attr("dataname"), undefined, true);
										return false;
									});
								}
							}
						};

						Aurora.Services.AuroraWebService.GetGalleryCat(galleryType, filter, onRequest, AuroraJS.onError);
						break;
					case "GallerySliderLarge":
						var albumID = $("#" + type.Name.replace("_Details", "")).attr("catfilter");

						var getAlbumItems = function (result) {
							if (result.Data === "") {
								return;
							}

							dataSet.set_data(result.Data);
							var myticker = GetJSSession("GallerySlide_changeCountdown");
							var options = {
								'status_width': GetJSSession("GallerySlide_statusWidth"),
								'status_color_inside': GetJSSession("GallerySlide_statusSliderColour"),
								'status_color_outside': GetJSSession("GallerySlide_statusBackgroundColour"),
								'set_speed': parseInt(GetJSSession("GallerySlide_speed")),
								'fullscreen': 'false',
								'swipe': 'false',
								'callback': null
							};
							//this must be done better

							AuroraJS.Modules.globals.TimeOutID2 = setInterval(function () {
								try {
									simpleSlide(options);
									clearInterval(AuroraJS.Modules.globals.TimeOutID2);
								} catch (e) {

								}
							}, 1000);


							$("GallerySliderLarge").block({ message: null });
							if (AuroraJS.Modules.globals.simpleSlideTimerID2 === 0) {
								AuroraJS.Modules.globals.simpleSlideTimerID2 = setInterval(function () {
									$(".right-button").click();
								}, myticker);
							}

							$("#GallerySliderLarge .right-button").live('mouseover', null, function () {
								clearInterval(AuroraJS.Modules.globals.simpleSlideTimerID2);
							});
							$("#GallerySliderLarge .right-button").live('mouseout', null, function () {
								AuroraJS.Modules.globals.simpleSlideTimerID2 = setInterval(function () { $(".right-button").click(); }, myticker);
							});

							$("#GallerySliderLarge .left-button")[0].onmouseover = function () {
								clearInterval(AuroraJS.Modules.globals.simpleSlideTimerID2);
							};

							$("#GallerySliderLarge .left-button").mouseout(function () {
								AuroraJS.Modules.globals.simpleSlideTimerID = setInterval(function () { $(".right-button").click(); }, myticker);
							});

							$("#GallerySliderLarge").unblock();

						};

						Aurora.Services.AuroraWebService.GetGalleries(albumID, getAlbumItems, onError);
						break;
					case "GallerySlider":

						var albumID = $("#" + type.Name.replace("_Details", "")).attr("catfilter");

						var getAlbumItems = function (result) {
							dataSet.set_data(result.Data);
							var ticker = GetJSSession("GallerySlide_changeCountdown");
							var options = {
								'status_width': GetJSSession("GallerySlide_statusWidth"),
								'status_color_inside': GetJSSession("GallerySlide_statusSliderColour"),
								'status_color_outside': GetJSSession("GallerySlide_statusBackgroundColour"),
								'set_speed': parseInt(GetJSSession("GallerySlide_speed")),
								'fullscreen': 'false',
								'swipe': 'false',
								'callback': null
							};

							//load the slider
							simpleSlide(options);

							$("#GallerySlider").block({ message: null });
							if (AuroraJS.Modules.globals.simpleSlideTimerID === 0) {
								AuroraJS.Modules.globals.simpleSlideTimerID = setInterval(function () {
									$(".right-button").click();
								}, ticker);
							}

							$("#GallerySlider .right-button").live('mouseover', null, function () {
								clearInterval(AuroraJS.Modules.globals.simpleSlideTimerID);
							});
							$("#GallerySlider .right-button").live('mouseout', null, function () {
								AuroraJS.Modules.globals.simpleSlideTimerID = setInterval(function () { $(".right-button").click(); }, ticker);
							});

							$("#GallerySlider .left-button")[0].onmouseover = function () {
								clearInterval(AuroraJS.Modules.globals.simpleSlideTimerID);
							};

							$("#GallerySlider .left-button").mouseout(function () {
								AuroraJS.Modules.globals.simpleSlideTimerID = setInterval(function () { $(".right-button").click(); }, ticker);
							});

							$("#GallerySlider a[href='']")
                            .attr("href", "#")
                            .removeAttr("target");

							$("#GallerySlider").unblock();

						};

						Aurora.Services.AuroraWebService.GetGalleries(albumID, getAlbumItems, onError);
						break;
					case "RhinoSlider":

						var getAlbumItems = function (result) {
							dataSet.set_data(result.Data);
							var options = {
								effect: GetJSSession("RhinoSlide_effect") || 'slide',
								effectTime: GetJSSession("RhinoSlide_effectTime") || 1000,
								showTime: GetJSSession("RhinoSlide_showTime") || 3000,
								slidePrevDirection: GetJSSession("RhinoSlide_slidePrevDirection") || "toLeft",
								slideNextDirection: GetJSSession("RhinoSlide_slideNextDirection") || "toRight",
								easing: GetJSSession("RhinoSlide_easing") || "swing",
								animateActive: GetJSSession("RhinoSlide_animateActive") || true,
								controlsPlayPause: GetJSSession("RhinoSlide_controlsPlayPause") || true,
								controlsPrevNext: GetJSSession("RhinoSlide_controlsPrevNext") || true,
								showControls: GetJSSession("RhinoSlide_showControls") || "hover",
								playText: GetJSSession("RhinoSlide_playText") || "play",
								pauseText: GetJSSession("RhinoSlide_pauseText") || "pause",
								prevText: GetJSSession("RhinoSlide_prevText") || "prev",
								nextText: GetJSSession("RhinoSlide_nextText") || "next",
								controlFadeTime: GetJSSession("RhinoSlide_controlFadeTime") || 650,
								showBullets: GetJSSession("RhinoSlide_showBullets") || "hover",
								changeBullets: GetJSSession("RhinoSlide_changeBullets") || "after",
								controlsMousewheel: GetJSSession("RhinoSlide_controlsMouseWheel") || true,
								randomOrder: GetJSSession("RhinoSlide_randomOrder") == undefined ? false : Boolean.parse(GetJSSession("RhinoSlide_randomOrder")),
								cycled: GetJSSession("RhinoSlide_cycled") || true,
								showCaptions: GetJSSession("RhinoSlide_showCaptions") == undefined ? "never" : GetJSSession("RhinoSlide_showCaptions"),
								captionsFadeTime: GetJSSession("RhinoSlide_captionsFadeTime"),
								captionsOpacity: GetJSSession("RhinoSlide_captionsOpacity"),
								autoPlay: GetJSSession("RhinoSlide_autoPlay") || false
							};

							//load the slider
							$("#" + type.Name + "_Data").rhinoslider(options);

							if (AuroraJS.Bespoke.rhinosliderAfterLoad) {
								//This override allows you to make pre-Redirect Modifications
								AuroraJS.Bespoke.rhinosliderAfterLoad();
							}
						};
						//get category id from name
						var catNames = function (result) {
							var catID = null;
							result.Data = $.parseJSON(result.Data).NewDataSet.Table;

							catID = result.Data.ID;

							if (catID) {
								Aurora.Services.AuroraWebService.GetGalleries(catID, getAlbumItems, onError);
							}
						}
						if (GetJSSession("RhinoSlide_CategoryName")) {
							Aurora.Services.AuroraWebService.GetGalleryCat("Images", GetJSSession("RhinoSlide_CategoryName"), catNames, onError);
						}
						else {
							$("<div>Rhino Slide Gallery has no image album attached to it.</div>").dialog({
								modal: true,
								resizable: false,
								title: "Error",
								width: '400',
								buttons: { "Okay": function () { $(this).dialog("close") } }
							});
						}
						break;
					case "BackgroundChanger":
						//get category id from name
						var catNames = function (result) {
							var catID = null;
							result.Data = $.parseJSON(result.Data).NewDataSet.Table;

							catID = result.Data.ID;

							if (catID) {
								Aurora.Services.AuroraWebService.GetGalleries(catID, request, onError);
							}
						}
						var request = function (result) {
							var changerData = new Array();
							var newIndex = 0;
							var imagesDownloaded = 0;

							for (var item in result.Data) {
								changerData[newIndex] = String.format("/ClientData/{0}/Uploads/gallery_01_{1}.jpg", clientSiteID, result.Data[item].ID);
								$('<img/>').attr('src', changerData[newIndex]).load(function () {
									imagesDownloaded++;
									if (imagesDownloaded == result.Data.length) {
										$(".mybackground").backgroundChanger({
											changeTime: parseInt(GetJSSession("BackgroundChanger_ChangeTime"))
                                                                    , transitionTime: parseInt(GetJSSession("BackgroundChanger_TransitionTime"))
                                                                    , fadeInTime: parseInt(GetJSSession("BackgroundChanger_FadeOutTime"))
                                                                    , data: changerData
										});
									}
								});




								newIndex++;

							}


						};

						Aurora.Services.AuroraWebService.GetGalleryCat("Images", GetJSSession("BackgroundChanger_GalleryID"), catNames, onError);
						break;
					case "SmoothSlider":
						//Go here for documentation : http://www.smoothdivscroll.com
						//get the album we wish to show inside the slider
						var albumID = Number(GetJSSession("SmoothSlider_CategoryID"));

						//no category specified hide the module & stop processing
						if (isNaN(albumID)) {
							$(dataSet._element).html("There are no albums linked to the slider.");
							return;
						}

						var getAlbumImages = function (result) {
							if (result.Data.length > 0) {

								//bind the data
								if (!$(dataSet._element).attr("bound")) {
									dataSet.set_data(result.Data);
									$(dataSet._element).attr("bound", "true");
								}

								//call the slider
								$(dataSet._element).parent().parent().smoothDivScroll({
									autoScroll: GetJSSession("SmoothSlider_AutoScroll"),
									autoScrollDirection: "backandforth",
									autoScrollStep: Number(GetJSSession("SmoothSlider_AutoScrollStep")),
									autoScrollInterval: Number(GetJSSession("SmoothSlider_AutoScrollInterval")),
									visibleHotSpots: GetJSSession("SmoothSlider_VisibleHotSpots")
								});

								// check if the scroller has any links
								if ($(dataSet._element).children('a').length > 0) {
									$(dataSet._element).parent().parent().bind("mouseover", function () {
										$(this).smoothDivScroll("stopAutoScroll");
									}).bind("mouseout", function () {
										$(this).smoothDivScroll("startAutoScroll");
									});
								}
							}

						};


						Aurora.Services.AuroraWebService.GetGalleries(albumID, getAlbumImages, onError);



						break;
						//#endregion                         

						//#region News                       
					case "News":
						var catFilter = $("#News .sys-template").attr('catFilter');
						if (catFilter === undefined) {
							catFilter = $("#News").attr('catFilter');
						}
						var request = function (result) {

							if (result != null && result.Data !== "" && result.Result) {
								if (result.Data == "{ \"NewDataSet\": }}") {
								    $("#" + type.Name).append("There's no current news available");
									if (AuroraJS.Bespoke.NoNews) {
										AuroraJS.Bespoke.NoNews();
									}
									return;
								}
								var newsData = $.parseJSON(result.Data);
								SetJSSession("News", newsData.NewDataSet.Table);
								dataSet.set_data(newsData.NewDataSet.Table);
								AuroraJS.Modules.DecodeHTML(type.Name + "_Data");


								$first = null;
								hasCatHeader = false;
								$(".CatHeader").each(
                                    function () {
                                    	hasCatHeader = true;
                                    	if ($first == null) {
                                    		$first = $(this);
                                    	}
                                    	else {
                                    		if ($(this).parent().parent().attr("catid") == $first.parent().parent().attr("catid")) {
                                    			$(this).parent().parent().find(".CatContent").insertAfter($first.parent().parent().find(".CatContent").first());
                                    			$(this).parent().parent().remove();
                                    		}

                                    	}
                                    }
                                );

								if (!hasCatHeader) {
									var categories = $("#News ^[catid]");
									var totalCat = {};
									$("#News ^[catid]").each(
                                     function () {
                                     	if (totalCat[$(this).attr("catid")] !== true) {
                                     		totalCat[$(this).attr("catid")] = true;
                                     	}
                                     	else {
                                     		$(this).remove();
                                     	}
                                     }
                                );
								}
								$("#News_Data").children('div').each(
                                    function () {
                                    	if ($(this).attr("catid") == AuroraJS.Modules.myArticle) {
                                    		$(this).next().remove();
                                    		$(this).remove();
                                    	}
                                    }
                                );

								$("#" + type.Name).find("a").each(function () {
									var dataId = $(this).attr('dataid');
									var dataDesc = $(this).attr('datadesc');
									if (dataId != undefined && dataDesc != undefined) {
										$(this).attr("href", "/News/" + dataId + "/" + dataDesc.replace(/ /gi, "-"));
										$(this).bind("click", function () {
											AuroraJS.Modules.GetContentForModule("News", dataId, dataDesc.replace(/ /gi, "-"));
											return false;
										});
									}
								});
							} else {
								$("#" + type.Name + "_Data").hide();
								$("#" + type.Name).append("There's no current news available");
							}
							$(type.Name).unblock();

							var images = $("#News img");

							for (var items = 0; items < images.length; items++) {

								if (images[items].getAttribute("alt") == "Image1") {
									AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/sm_News_01_" + images[items].getAttribute("dataid") + ".jpg", images[items].id, GetJSSession("News_noImage"));
								} else if (images[items].getAttribute("alt") == "Image2") {
									AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/sm_News_02_" + images[items].getAttribute("dataid") + ".jpg", images[items].id);
								} else {
									AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/sm_News_01_" + images[items].getAttribute("dataid") + ".jpg", images[items].id, GetJSSession("News_noImage"));
								}
							}

							if (AuroraJS.Bespoke.News) {
								AuroraJS.Bespoke.News();
							}

						};

						if (catFilter) {
							Aurora.Services.AuroraWebService.GetNewsWithFilter(catFilter, request, AuroraJS.onError);
						}
						else {
							Aurora.Services.AuroraWebService.GetNews(request, AuroraJS.onError);
						}
						break;
						//#endregion                                                                                                                             

						//#region MultiInstanceNews 
					case "MultiInstanceNews":
						var response = function (result, dataElement) {
							if (result.Data == "{ \"NewDataSet\": }}") {
							    $(dataElement).parent().append("There's no current news available");
								//run bespoke method
								if (AuroraJS.Bespoke.NoMultiNews) {
									AuroraJS.Bespoke.NoMultiNews(dataElement);
								}
								return;
							}
							var bindElement = $create(Sys.UI.DataView, {}, {}, {}, dataElement);
							var newsData = $.parseJSON(result.Data);
							bindElement.set_data(newsData.NewDataSet.Table);

							if ($(".MultiInstanceNews img").length > 0) {

								if (newsData.NewDataSet.Table.length > 0) {
									for (var i = 0; i < newsData.NewDataSet.Table.length; i++) {
										if ($(".MultiInstanceNews #" + newsData.NewDataSet.Table[i].ID + "_img1").length > 0) {
											AuroraJS.Modules.LoadImage("/ClientData/" + newsData.NewDataSet.Table[i].ClientSiteID + "/Uploads/news_01_" + newsData.NewDataSet.Table[i].ID + ".jpg", newsData.NewDataSet.Table[i].ID + "_img1", "/ClientData/" + newsData.NewDataSet.Table[i].ClientSiteID + "/Styles/images/noimage.gif");
										}
									}
								} else {
									if ($(".MultiInstanceNews #" + newsData.NewDataSet.Table.ID + "_img1").length > 0) {
										AuroraJS.Modules.LoadImage("/ClientData/" + newsData.NewDataSet.Table.ClientSiteID + "/Uploads/news_01_" + newsData.NewDataSet.Table.ID + ".jpg", newsData.NewDataSet.Table.ID + "_img1", "/ClientData/" + newsData.NewDataSet.Table.ClientSiteID + "/Styles/images/noimage.gif");
									}
								}
							}

							//add link    
							$(dataElement).find("a").each(function () {
								var dataId = $(this).attr('dataid');
								var dataDesc = $(this).attr('datadesc');

								if (!isNaN(dataId)) {
									$(this).attr("href", "/News/" + dataId + "/" + dataDesc.replace(/ /gi, "-"));
									$(this).bind("click", function () {
										AuroraJS.Modules.GetContentForModule("News", dataId, dataDesc.replace(/ /gi, "-"));
										return false;
									});
								}
							});

							//run bespoke method
							if (AuroraJS.Bespoke.MultiNews) {
								AuroraJS.Bespoke.MultiNews(dataElement);
							}
						};

						$(".MultiInstanceNews .sys-template").each(function () {
							var $catID = $(this).parent().attr("catfilter");

							if (!$catID) {
								Aurora.Services.AuroraWebService.GetNews(response, AuroraJS.onError, this);
							}
							else {
								Aurora.Services.AuroraWebService.GetNewsWithFilter($catID, response, AuroraJS.onError, this);
							}
						});
						break;
						//#endregion

						//#region Pages                         
					case "ContentCarousel":

						var request = function (result) {
							if (result.Count > 0 && result.ErrorMessage == "") {
								createControl(result);
								$("#ContentCarousel").find(".sys-template").removeClass("sys-template");
							}
						}

						var pages = GetJSSession("ContentCarousel_Pages").split(',');
						var pageIDArray = [];
						var allowRequest = false;

						for (var item in pages) {
							pageIDArray[item] = pages[item];
							allowRequest = true;
						}

						if (allowRequest && pageIDArray[0] != "") {
							Aurora.Services.AuroraWebService.GetPagesByName(pageIDArray, false, request, onError);
						}
						else {
							$("#ContentCarousel").hide();
						}
						break;
						//#endregion                        

						//#region Products                         
					case "Products":
						AuroraJS.Modules.loadBasketFromCookies();
						break;
						//#endregion                         

						//#region Testimonies                         
					case "CustomerFeedbackDisplay":
						var getFeedback = function (result) {
							if (result.Result) {
								if (result.Data.length > 0) {
									dataSet.set_data(result.Data);
									SetJSSession("Testimionies", result.Data);
									//Set Link click bindings
									$("#" + type.Name).find("a").each(function () {
										var dataId = $(this).attr('dataid');
										var dataDesc = $(this).attr('datadesc');
										if (dataDesc && dataId) {
											$(this).attr("href", "/Testimonies/" + dataId + "/" + dataId);
											$(this).bind("click", function () {
												AuroraJS.Modules.GetContentForModule("Testimonies", dataId, dataDesc.replace(/ /gi, "-"));
												return false;
											});
										}

									});
									slider = new feedbackContentSlider("#CustomerFeedbackDisplay", GetJSSession("CustomerFeedback_Limit"),GetJSSession("CustomerFeedback_ItemHeight"));
									
								}
								else {
									var url = GetJSSession("CustomerFeedback_FormURL") || "#";
									var linkText = GetJSSession("CustomerFeedback_LinkText") || "Click here to send us your comments";
									$("#" + type.Name).append("<a href='" + url + "' >" + GetJSSession("CustomerFeedback_LinkText") + "</a>");
								}

							}
							$(type.Name).unblock();
							if (AuroraJS.Bespoke.CustomerFeedbackDisplayAfterLoad) {
								//This override allows you to make pre-Redirect Modifications
								AuroraJS.Bespoke.CustomerFeedbackDisplayAfterLoad();
							}
						};
						var itemsPerPage = parseInt(GetJSSession("CustomerFeedback_ItemHeight"), 10);
						//console.log("Item Height: "+itemsPerPage);
						Aurora.Services.AuroraWebService.GetFeedback((itemsPerPage <= 0 || isNaN(itemsPerPage) ? GetJSSession("CustomerFeedback_Limit") : 0), 1, 0, "", getFeedback, onError);
						break;
					case "EventsList":
					case "ListDetailTestimonies":
					case "ListDetailNews":
						$("#Contents").block({ message: null });
						var catFilter = $("#ListDetailNews").attr('catFilter');

						if ($("#" + type.Name + "_Pagination").length === 0) {
							$("<div style='width:100%;text-align:center;' id=\"" + type.Name + "_Pagination\"></div>").insertAfter($(dataSet._element.parentNode));
							AuroraJS.Modules.globals.totalCount = 0;
						}
						AuroraJS.Modules.globals.pageSize = GetJSSession(type.Name + "_PageSize") !== undefined ? GetJSSession(type.Name + "_PageSize") : 10;
						var getData = function (result) {
							if (result.Data.length === 0) {
							    $("<div>There's no current data available</div>").insertAfter(dataSet._element);
								$("#Contents").unblock();
							}
							else {
								if (result.Data.toString().indexOf("NewDataSet") > -1) {
									result.Data = $.parseJSON(result.Data);
									result.Data = result.Data.NewDataSet.Table;
								}

								if (AuroraJS.Modules.globals.totalCount === 0) {
									AuroraJS.Modules.globals.totalCount = result.Count;
									initPagination(AuroraJS.Modules.BindData, result.Count, AuroraJS.Modules.globals.currentPage, template, type);
									$("#Contents").unblock();
								}

								if (AuroraJS.Modules.PagedData == null || $("#" + AuroraJS.Modules.PagedData._element.id).attr("init") != "1") {
									var templateName = ""
									if (type.Name === "ListDetailTestimonies") {
										templateName = "ListDetailTestimonies_Data";
									}
									else if (type.Name === "EventsList") {
										templateName = "EventsList_Data";
									}
									else {
										templateName = "ListDetailNews_Data";
									}
									AuroraJS.Modules.PagedData = $create(Sys.UI.DataView, {}, {}, {}, $get(templateName));
									$("#" + AuroraJS.Modules.PagedData._element.id).attr("init", "1");
								}


								AuroraJS.Modules.PagedData.set_data(result.Data);

								AuroraJS.Modules.DecodeHTML(AuroraJS.Modules.PagedData._element.id);

							}
						};
						//switch data for display
						switch (type.Name) {
							case "ListDetailNews":
								if (catFilter) {
									Aurora.Services.AuroraWebService.GetDetailNewsWithFilter(catFilter, getData, onError);
								}
								else {
									Aurora.Services.AuroraWebService.GetDetailNews(getData, onError);
								}
								break;
							case "ListDetailTestimonies":
								Aurora.Services.AuroraWebService.GetFeedback(AuroraJS.Modules.globals.pageSize, AuroraJS.Modules.globals.currentPage + 1, 0, "", getData, onError);
								break;
							case "EventsList":
								Aurora.Services.AuroraWebService.GetEvents(0, true, AuroraJS.Modules.globals.currentPage + 1, AuroraJS.Modules.globals.pageSize, false, "", getData, onError);
								break;

						}
						break;
					case "Segue":

						var resulted = function (result) {
							if (!dataSet) {
								dataSet = $create(Sys.UI.DataView, {}, {}, {}, $get(type.Name + "_Data"));
							}
							dataSet.set_data(result.Data);
							AuroraJS.Modules.DecodeHTML('Segue_Data');
							if (AuroraJS.Modules.SegueInit === undefined) {
								loadScript("/Scripts/Segue.js", function () { AuroraJS.Modules.SegueInit(); }, 0);
							}
							else {
								AuroraJS.Modules.SegueInit();
							}
						};
						Aurora.Services.AuroraWebService.GetFeedback(GetJSSession("Segue_Amount"), 1, 0, "", resulted, onError);
						break;
						//#endregion                         

						//#region Top Donors 
					case "B2MTopDonors":
						var response = function (result) {
							var donorData = $.parseJSON(result.Data);
							donorData = donorData.NewDataSet.Table;

							dataSet.set_data([donorData[0], donorData[1], donorData[2], donorData[3]]);
							$("#B2MTopDonors .donorFeedImages").each(
                              function (indx, Elem) {
                              	$this = $(this);
                              	AuroraJS.Modules.LoadImage($this.attr("src"), $this.attr("id"), "/ClientData/10016/Styles/images/no_photo_available.png");
                              }
                            );

							$("#B2MTopDonors").find("a").each(
                              function () {
                              	var $a = $(this);

                              	$a
								.attr("target", "_blank")
								.attr("href", "/DonorView.aspx?single=" + $a.attr("dataid"));

                              }
                            );
							$(type.Name).unblock();
						};
						$(type.Name).block({ message: null });
						Aurora.Services.AuroraWebService.GetTopTenDonors(response, onError);
						break;
						//#endregion

				}
			} catch (e) {
				alert("exception" + e);
			}
		};
		this.Modules.LoadBanners = function (type) {
			if ($("#" + type.Name + "Settings").length !== 0) {
				//clean up before loading fresh banners
				$("#" + type.Name + " div").each(
              function () {
              	if ($(this).attr("id") != type.Name + "Settings") {
              		$(this).remove();
              	}
              });
				//get settings 
				var Farm = $("#" + type.Name + "Settings").attr('Farm');
				var Zone = GetJSSession("PageID") ? GetJSSession("PageID") : 0;
				var Count = GetJSSession(type.Name + "_DisplayNumber") || $("#" + type.Name + "Settings").attr('Count');
				var RotationSpeed = GetJSSession(type.Name + "_RotationSpeed") || 0;
				var Style = $("#" + type.Name + "Settings").attr('Style');
				var TypeName = type.Name;


				if (Count === undefined) {
					Count = 0;
				}

				//Count will be to return multiple banners this is ideally for buttons (Look at changing Count to N)
				//style is vertical is style=padding:2px or horizontal is style=padding:2px;float:left
				if (Zone !== undefined) {
					$.get('/banner.ashx?F=' + Farm + '&Z=' + Zone + '&N=1&Count=' + Count + '&Site={ClientSiteID}&CatId=&style=' + Style + ' &x=' + new Date().getMilliseconds(), function (data) {
						$('#' + TypeName).hide();
						$('#' + TypeName).append(data);
						var banners = $("#BannerButton div a,#Banner div a");

						var temp;
						for (var banner = 0; banner < banners.length; banner++) {
							var count = 0;
							for (var alike = 0; alike < banners.length; alike++) {
								if (banners[alike].getAttribute("dataid") == banners[banner].getAttribute("dataid")) {
									count++;
									if (count > 1) {
										$(banners[alike]).remove();
									}

								}
							}
						}
						if (RotationSpeed > 0) {

							if (AuroraJS.Modules.bannerInterval[TypeName]) {
								if (AuroraJS.Modules.bannerInterval[TypeName].interval) {
									clearInterval(AuroraJS.Modules.bannerInterval[TypeName].interval);
								}
							}
							// Gets all the banners in the container div.
							AuroraJS.Modules.bannerInterval[TypeName] = {
								banners: $('#' + TypeName + ' div:has(a)')
							}

							var rotatorSettings = AuroraJS.Modules.bannerInterval[TypeName];
							rotatorSettings.currentBanner = 0;
							rotatorSettings.bannerCount = rotatorSettings.banners.length;
							rotatorSettings.banners.filter(':gt(0)').hide();
							$('#' + TypeName).css({
								position: 'relative'
							});
							var size = GetJSSession(type.Name + "_Size").split('*');
							rotatorSettings.banners.css({
								position: 'absolute',
								top: '0',
								width: size[0],
								height: size[1]
							})

							//check if more than one banner
							if ($("#" + TypeName + " a").length > 1) {
								// sets up the rotating interval
								rotatorSettings.interval = setInterval(function () {
									var rotatorSettings = AuroraJS.Modules.bannerInterval[TypeName];
									if ($('#' + TypeName).length === 0) {
										//clears an interval if the container div is no longer present on the page
										clearInterval(rotatorSettings.interval);
									}
									$(rotatorSettings.banners[rotatorSettings.currentBanner]).fadeOut(500);
									rotatorSettings.currentBanner += 1;
									rotatorSettings.currentBanner = (rotatorSettings.currentBanner >= rotatorSettings.bannerCount) ? 0 : rotatorSettings.currentBanner;
									$(rotatorSettings.banners[rotatorSettings.currentBanner]).fadeIn(500);

								}, RotationSpeed);
							}

						}


						//loop though all the banners and add the _trackEvent for view and click.
						$("#" + TypeName).find("a[done!=true]").each(function () {
							var dataId = $(this).attr('dataid');
							var dataDesc = $(this).attr('datadesc');
							_gaq.push(['_trackEvent', 'Banner', 'View', '/' + dataId + '/' + dataDesc + '/']);

							$(this).attr("done", "true");
							$(this).bind("click", function () {
								_gaq.push(['_trackEvent', 'Banner', 'Click', '/' + dataId + '/' + dataDesc + '/']);
							});

						});
						if (AuroraJS.Bespoke.BannerLoad) {
							//This override allows you to make pre-Redirect Modifications
							AuroraJS.Bespoke.BannerLoad($('#' + TypeName));
						}
						$('#' + TypeName).fadeIn(500);
					});
				}
			}
		};
		//TODO IN HERE
		this.Modules.GetDetailTemplate = function (templateId) {
			//get the NewsDetail template from var ClientModules
			var DetailTemplate;
			for (var i = 0; i < AuroraJS.Modules.ClientModules.length; i++) {
				if (AuroraJS.Modules.ClientModules[i].featureModules["Name"] == templateId + "Detail") {
					if (AuroraJS.Modules.ClientModules[i].clientModules["HTML"]) {
						DetailTemplate = AuroraJS.Modules.ClientModules[i].clientModules["HTML"];
					} else {
						DetailTemplate = AuroraJS.Modules.ClientModules[i].featureModules["HTML"];
					}
				}
			}

			if ($("#" + templateId + "DetailData").length === 0) {

				var eleId = 'inputcontent';
				//if this is a gallery then load the detail into a different div because we don't want the #inputcontent overridden with the detail
				//TODO This might change for future attached modules
				if (templateId.toUpperCase().indexOf('GALLERY') !== -1) {
					eleId = 'AttachedModulesDetails';
				}

				$("#" + eleId).html("<div></div>");
				if (AuroraJS.Modules.templateDetail !== null) {
					AuroraJS.Modules.templateDetail = null;
				}
				if (DetailTemplate) {
					var id = (new Date().getMilliseconds());
					DetailTemplate = DetailTemplate.replace("{{Image1}}", "<img style='display:none;width:100%;height:auto;' id='" + (id + 1) + "' alt='Image1' />");
					DetailTemplate = DetailTemplate.replace("{{Image2}}", "<img style='display:none;width:100%;height:auto;' id='" + (id + 7) + "' alt='Image2' />");
					DetailTemplate = DetailTemplate.replace("{{Image}}", "<img style='display:none;' id='" + (id + 30) + "' alt='Image' />");
					var detailTemplate = "<div class='sys-template' sys:attach='dataview' id='" + templateId + "DetailData'>" + DetailTemplate + "</div>";

					$(detailTemplate).insertBefore($("#" + eleId + " div"));

					$("#" + id + 1).css("width", $("#" + id + 1).parent().css("width"));


				}
			}
		};
		this.Modules.GetTemplate = function (moduleName) {
			var module = $.grep(AuroraJS.Modules.ClientModules, function (element, index) {
				return (element.featureModules["Name"].toUpperCase() === moduleName.toUpperCase())
			});
			return (module[0].clientModules["HTML"] || module[0].featureModules["HTML"]);
		};
		this.Modules.GetModulesForPage = function (pageModuleID) {
			var getModule = function (result) {
				//clear attached module DIVS
				$("#AttachedModulesDetails").html("");
				$("#AttachedModules").html("");
				if (!result.Result) {
					return;
				}
				if (result.Data.length > 0 && result.Result) {
					if ($("#" + result.Data[0].module.Name).length === 0) {
						$("#AttachedModules").html("<div id='" + result.Data[0].module.Name + "'></div>");
						AuroraJS.Modules.BindData(result.Data[0].clientModule, result.Data[0].module);
					}
				}

			};

			Aurora.Services.AuroraWebService.GetLinkedModuleOnPage(pageModuleID, getModule, AuroraJS.onError);
		};
		this.Modules.LoadImage = function (imageUrl, imageTagID, failOver) {
			/// <summary>Checks if the image requested exists, if so it will create the image element</summary>
			/// <param name="imageUrl" type="String">Path to image on server</param>
			/// <param name="imageTagID" type="String">name of the container to be created to hold the image</param>
			/// <param name="failOver" type="String">Replacement Image</param>

			$("#Contents").block({ message: null });
			$("#Contents").context.className = "Loading";
			var loadImage = function (result) {

				var imageElement = document.getElementById(imageTagID);



				imageElement.src = imageUrl;
				$(imageElement).load(function () {
					$(this).fadeIn(500);
				});

				$("#Contents").unblock();


			};
			var fail = function (result) {
				if ($("#" + imageTagID).length > 0) {
					if (!failOver) {

						$("#" + imageTagID).remove();

					}
					else {
						$("#" + imageTagID).attr("src", failOver);
						$("#" + imageTagID).fadeIn(500);
					}

				}
				$("#Contents").unblock();
			};

			if ($("#" + imageTagID).length > 0) {
				//Go get image from server
				$.ajax({ url: imageUrl, type: 'HEAD', error: fail, success: loadImage });
			}
		};
		this.Modules.GetContentForModule = function (moduleName, itemID, itemTitle, isHistory, isAttachedModule) {
		    moduleName = toTitleCase(moduleName);
			/// <summary>Gets the data for a specified module and displays it within the content area</summary>
			/// <param name="moduleName" type="String">Name of the module to be displayed</param>
			if (AuroraJS.Modules.Members) {
				document.location = String.format("/{0}/{1}/{2}", moduleName, itemID, itemTitle);
				return;
			}
			//Display Loading signal
			$("#Contents").block({ message: null });
			//clear header image
			$("#pageHeaderImage img").fadeAndRemove(1000);
			//fetch detail template for module.

			$("#Segue").css('display', 'none');
			$("#Segue").insertAfter("#Contents");

			//save old content in case of a redirect that executes a function
			var oldContent = $("#inputcontent").html();
			AuroraJS.Modules.GetDetailTemplate(moduleName);

			//execute operations for specified module.
			var getData = function (result) {
				$("#inputcontent").show();
				SetJSSession("PageID", itemID);
				//check if secured item
				if (result.Data == "RoleSecurity") {
					document.location = "/login.aspx";
					return;
				} else if (result.Data == "PageSecurity") {
					DisplayLogin($("#inputcontent"));
					return;
				}
				var dataToBind = null;

				if (!result.Result) {
					//TODO insert global message handler
				}

				//For master page switching do not remove
				if (moduleName.toUpperCase().indexOf("PAGE") > -1
                                && !result.Data[0].isDefault && AuroraJS.Modules.DefaultPage) {
					document.location = String.format("http://{0}/{1}/{2}/{3}", document.domain, moduleName, itemID, itemTitle);
					return true;
				}

				if (moduleName.toUpperCase().indexOf("PAGE") > -1 && result.Data[0].isDefault) {
					document.location = "http://" + document.domain;
					return true;
				}

				//Template error handling removes template from page as errors will occur if bound to more than once.
				if (moduleName.indexOf("Page") > -1) {
					if (result.Data[0].RedirectURL && document.location.toString().indexOf(result.Data[0].RedirectURL) == -1) {
						if (result.Data[0].RedirectURL.indexOf("func:") > -1) {
							$("#inputcontent").stop();
							setTimeout(result.Data[0].RedirectURL.replace("func:", "") + ";", 100);
							$("#inputcontent").html(oldContent);

						}
						else if (result.Data[0].RedirectURL.indexOf("#Products:") === 0) {
							var categoryID = $('#menu a[redirect="' + result.Data[0].RedirectURL + '"]').attr("productid");
							var redirect = "/Products/" + categoryID + "/" + result.Data[0].ShortTitle;
							if ($.history) {
								$.history.load(redirect);
							}
							//document.location = redirect;
						}
						else {
							if (AuroraJS.Bespoke.Redirect) {
								//This override allows you to make pre-Redirect Modifications
								AuroraJS.Bespoke.Redirect(result.Data[0]);
							}
							document.location = result.Data[0].RedirectURL;

						}
						return;
					}
				}
				//create history to enable the back button
				if (!isHistory) {
					//Ajax history: browser back button functionality
					if ($.history) {
						$.history.load('/' + moduleName + "/" + itemID + "/" + itemTitle, 'true');
					}
				}
				var reNew = AuroraJS.Modules.templateDetail === undefined || AuroraJS.Modules.templateDetail === null;
				if (reNew) {
					AuroraJS.Modules.templateDetail = $create(Sys.UI.DataView, {}, {}, {}, $get(moduleName + "DetailData"));
				} else {
					AuroraJS.Modules.templateDetail.dispose();
					AuroraJS.Modules.templateDetail = null;
					$("#" + moduleName + "DetailData").remove();
					AuroraJS.Modules.GetDetailTemplate(moduleName);
					AuroraJS.Modules.templateDetail = $create(Sys.UI.DataView, {}, {}, {}, $get(moduleName + "DetailData"));
				}

				//apply data to template
				if (result.Data[0]) {
					if (result.Data[0].StartDate) {
						formatDateTime(result.Data[0].StartDate);
					}

					if (moduleName.toLocaleUpperCase() == "TESTIMONIES") {
						result.Data[0].Title = result.Data[0].FirstName + " " + result.Data[0].LastName;
						result.Data[0].Details = result.Data[0].Comment;
					}

					if (moduleName.toLocaleUpperCase() == "PAGE") {
						if (result.Data[0].PageContent1 === null) {
							result.Data[0].PageContent1 = "";
						}




						result.Data[0].PageContent = result.Data[0].PageContent1;

						//find header image
						if ($("#pageHeaderImage").length > 0) {
							$("#pageHeaderImage").html("");
							$("#pageHeaderImage").append("<img src='/ClientData/" + clientSiteID + "/Uploads/" + moduleName + "_" + result.Data[0].ID + ".jpg' id='pageHeader_" + result.Data[0].ID + "' onerror='$(this).hide()'/>");
							//Garth: Disabled for ECM Optimisation
							//$("#pageHeaderImage").append("<img style='display:none' id='pageHeader_" + result.Data[0].ID + "' />");
							//AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/" + moduleName + "_" + result.Data[0].ID + ".jpg", "pageHeader_" + result.Data[0].ID);
						}
					}
					if (result.Data.length === 1) {
						dataToBind = result.Data[0];
					}
					else {
						dataToBind = result.Data;
					}

				} else {
					//                if (result.Data.StartDate && $("#EventCalendar").length > 0) {
					//                    result.Data.StartDate = new Date($("#EventCalendar").val() + " " + result.Data.StartDate.getHours() + ":" + result.Data.StartDate.getMinutes());
					//                }
					//                if ((result.Data.Occurrance !== null || result.Data.Occurrance != "None") && $("#EventCalendar").length > 0) {
					//                    result.Data.EndDate = new Date(result.Data.StartDate.getMonth() + 1 + "/" + result.Data.StartDate.getDate() + "/" + result.Data.StartDate.getFullYear() + " " + result.Data.EndDate.getHours() + ":" + result.Data.EndDate.getMinutes());
					//                }
					dataToBind = result.Data;
				}

				//Specific to pages only
				if (moduleName.toUpperCase() === "PAGE") {

					dataToBind.PageContent = dataToBind.PageContent1;

					//#region Banner Loads 
					//Loads Banner Button
					var type = {};
					type.Name = "BannerButton";
					AuroraJS.Modules.LoadBanners(type);

					//Loads Main Banner
					if ($("#BannerSettings").length !== 0) {
						type.Name = "Banner";
						AuroraJS.Modules.LoadBanners(type);

					}
					//Loads Banner Bottom
					if ($("#BannerBottom").length > 0) {
						type.Name = "BannerBottom";
						AuroraJS.Modules.LoadBanners(type);
					}
					//#endregion

					if (dataToBind.ClientModuleID !== null) {
						AuroraJS.Modules.GetModulesForPage(dataToBind.ClientModuleID);
					}

				}

				if (!isAttachedModule) {
					$("#AttachedModulesDetails").html("");
					$("#AttachedModules").html("");
				}


				//bind data to template
				AuroraJS.Modules.templateDetail.set_data(dataToBind);
				//bespoke event when content is loaded
				if (AuroraJS.Bespoke.ContentLoadComplete) {
					AuroraJS.Bespoke.ContentLoadComplete();
				}

				if (moduleName.toLocaleUpperCase().indexOf("EVENT") > -1) {
					$("#EventDetailData table td").each(
                        function () {
                        	var currentEle = $(this);

                        	if (currentEle.html().toUpperCase().indexOf("ENDS") > -1) {
                        		dateval = currentEle.next().next()[0].innerHTML;
                        		if (dateval == "01/01/2050") {
                        			currentEle.parent().remove();
                        		}
                        	}
                        	else if (currentEle.html() == "00:00") {
                        		currentEle.parent().remove();
                        	}
                        	else if (currentEle.html() == "None") {
                        		currentEle.parent().remove();
                        	}
                        }
                    );

					if (dataToBind.GeoLatLong != "") {
						var gpsContainer = $("#gps0");
						if (gpsContainer.length > 0) {
							var gmapsLink = $("<a target='_blank' href='http://maps.google.com/maps?z=12&q=loc:" + escape(dataToBind.GeoLatLong) + "'>View in Google Maps</a>");
							$(gpsContainer).html(gmapsLink);
							$("<td><b>View Map:<b/></td><td style='width:15px;'></td>").insertBefore(gpsContainer);
						}


					}
				}

				//if we are loading a gallery then fill in the href and load fancybox
				if (moduleName.toUpperCase().indexOf('GALLERY') !== -1) {
					var isYouTube;
					$("#" + moduleName + "DetailData").find("a").each(function () {
						var dataId = $(this).attr('dataid');
						var dataDesc = $(this).attr('datadesc');
						isYouTube = ($(this).attr('VideoURL') !== undefined) ? true : false;
						var file = ($(this).attr('VideoURL') !== undefined) ? $(this).attr('VideoURL').replace(new RegExp("watch\\?v=", "i"), 'v/') : '/ClientData/' + clientSiteID + '/Uploads/gallery_01_' + dataId + '.jpg';
						$(this).attr('href', file);
						$(this).attr('title', dataDesc);
					});
					if (isYouTube === true) {
						$("a[rel=gallery_group]").fancybox({
							'transitionIn': 'none',
							'transitionOut': 'none',
							'titlePosition': 'outside',
							'overlayColor': '#000000',
							'overlayOpacity': 0.8,
							'showActivity': true,
							'type': 'swf',
							'swf': {
								'wmode': 'transparent',
								'allowfullscreen': 'true'
							},
							'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
								return '<span id="fancybox-title-over">Video ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
							}
						});
					} else {
						$("a[rel=gallery_group]").fancybox({
							'transitionIn': 'none',
							'transitionOut': 'none',
							'titlePosition': 'over',
							'overlayColor': '#000000',
							'overlayOpacity': 0.8,
							'showActivity': true,
							'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
								return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
							}
						});
					}


					$("a[rel=gallery_group]").first().trigger('click');

				}

				//Check for images
				var images = $("#" + moduleName + "DetailData img");

				for (var items = 0; items < images.length; items++) {
					if (images[items].getAttribute("alt") == "Image1") {
						AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/" + moduleName + "_01_" + dataToBind.ID + ".jpg", images[items].id);
					} else if (images[items].getAttribute("alt") == "Image2") {
						AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/" + moduleName + "_02_" + dataToBind.ID + ".jpg", images[items].id);
					}
					//                else if (images[items].getAttribute("alt") == "Image") {
					//                    AuroraJS.Modules.LoadImage("/ClientData/" + clientSiteID + "/Uploads/" + moduleName + "_" + dataToBind.ID + ".jpg", images[items].id, moduleName + "DetailData");
					//                }
				}

				//decode html that is text
				AuroraJS.Modules.DecodeHTML('inputcontent');
				if (moduleName == "Page" && result.Data[0].isDefault) {
					$("#Segue").css('display', 'block');
					$("#Segue").insertAfter("#" + GetJSSession("Segue_AppendAfter"));

				}
				//#region set the page title
				if (moduleName == "Page") {
					document.title = (dataToBind.PageTitle !== "" && dataToBind.PageTitle !== null) ? dataToBind.PageTitle : dataToBind.ShortTitle;
				} else {
					document.title = dataToBind.Title === undefined ? dataToBind.ShortTitle : dataToBind.Title;

					if (moduleName.indexOf("Gallery") > -1) {
						document.title = itemTitle;
					}
				}
				//#endregion

				//Check for modules within content.
				AuroraJS.Modules.LoadModulesInContent("#" + moduleName + "DetailData");

				if ($.tiny) {
					AuroraJS.Modules.bindCustomScrollBars("#inputcontent .customScrollBar");
				}

				scrollToElement(elementScroll, offSet, 1000);
				//check for the QapTcha and load the relevant events
				try {
					$('#QapTcha').each(
                function () {
                	if ($(this).find("#bgSlider").length == 0) {
                		$('#QapTcha').QapTcha({});
                	}
                }
                );


				}
				catch (e) {
					$('input[name="SendMail"]').bind("click", function () {
						AuroraJS.Modules.SendMail();
					});
				}
			};

			//Determines which web service to call
			switch (moduleName.toLocaleUpperCase()) {
				case "TESTIMONIES":
					Aurora.Services.AuroraWebService.GetFeedback(0, 0, itemID, "", getData, onError);
					break;
				case "NEWS":
					Aurora.Services.AuroraWebService.GetNewsItem(itemID, getData, onError);
					break;
				case "EVENT":
					Aurora.Services.AuroraWebService.GetEvent(itemID, getData, AuroraJS.onError);
					break;
				case "PAGE":
					Aurora.Services.AuroraWebService.GetPageContent(itemID, false, getData, AuroraJS.onError);
					break;
				case "GALLERYIMAGES": case "GALLERYVIDEO":

					Aurora.Services.AuroraWebService.GetGalleries(itemID, getData, AuroraJS.onError);
					break;
				case "PRODUCTS":
					//determine whether the request is by brand or category
					if (itemID.indexOf("b-") === -1) {
						AuroraJS.Modules.getCategoriesWithinCategory(itemID);
					} else {
						itemID = itemID.replace("b-", "");
						//check that we are passing an actual number as the brandID
						if (!isNaN(itemID)) {
							AuroraJS.Modules.getProducts(null, itemID);
						}
					}
					break;
				case "PRODUCT":
					AuroraJS.Modules.productIDToLoad = itemID;
					AuroraJS.Modules.getCategoriesForProduct(itemID);
					break;
				case "EXTERNALFORMS":
					//get all to be form elements
					$('.formext').each(function (indx, ele) {
						var form = $(this);
						$(String.format("<form action='{0}' method='{1}'>{2}</form>", form.attr("action"), form.attr("method"), form.html())).insertAfter(form);
						form.remove();
					});
					break;
				default:
					break;
			}
			if ($[moduleName]) {
				$[moduleName].apply(this, ["load", itemID, itemTitle]);
			};

			//updates menu current class
			if (AuroraJS.Modules.globals.updateCurrentMenuItem) {
				$('#menu li').removeClass('current');
				$('#menu li a[dataid=' + itemID + ']').parent().addClass('current');

			}
			else {
				AuroraJS.Modules.globals.updateCurrentMenuItem = true;
			}

			//Updates BreadCrumbs only if the site has the Breadcrumbs module.
			if (AuroraJS.Modules.generateBreadCrumbs) {
				AuroraJS.Modules.generateBreadCrumbs();
			}
			if (AuroraJS.Modules.generateSubMenu) {
				AuroraJS.Modules.generateSubMenu();
			}
			//Remove Loading signal
			if (moduleName.toUpperCase().indexOf("GALLERY") === -1) {
				if (GetJSSession(moduleName + "_autoScroll") != "false") {
					var elementScroll = "inputcontent";
					var offSet = -300;
					if (GetJSSession(moduleName + "_scrollTo")) {
						elementScroll = GetJSSession(moduleName + "_scrollTo");
					}

					if (GetJSSession(moduleName + "_offSet")) {
						offSet = GetJSSession(moduleName + "_offSet");
					}
					//scrollToElement(elementScroll, offSet, 1000);
				}

			}
			$("#Contents").unblock();

		};
		this.Modules.LoadModulesInContent = function (selector, options) {
			/// <summary>Check for modules within content.</summary>
			/// <param name="selector" type="String">JQuery selector value</param>
			var html = $(selector).find('div');

			$(html).each(function (index, domEle) {
				var myModules = GetJSSession("Modules");

				for (var module in myModules) {
					if (myModules[module].featureModules.Name == $(this).attr('id') && $(this).children('.placeholder').length > 0) {
						if (myModules[module].featureModules.Name == "GalleryImages") {
							AuroraJS.Modules.FromPage = true;
						}
						AuroraJS.Modules.BindData(myModules[module].clientModules, myModules[module].featureModules, false, $(this).attr('filter'), $(this).attr('rel'));
					}
						//special case for external forms
					else if ($(this).hasClass("formext") && myModules[module].featureModules.Name == "ExternalForms") {
						AuroraJS.Modules.BindData(myModules[module].clientModules, myModules[module].featureModules, false, $(this).attr('filter'), $(this).attr('rel'));
					}
					// THIS IS USED TO EXECUTE MODULES WITHIN PAGE CONTENT
					else if ($(this).hasClass("incontent") && $(this).hasClass(myModules[module].featureModules.Name)) {
						if ($.fn[myModules[module].featureModules.Name]) {
							$(this)[myModules[module].featureModules.Name]();
							//$[moduleName].apply(this, ["load", itemID, itemTitle]);
						};
					}

				}

			});

			if ($.fn.BoltAccordion) {
				$(selector).find('.boltaccordion').BoltAccordion();
			}
		}
		this.Modules.bindCustomScrollBars = function (selector, options) {
			/// <summary>
			/// binds a custom scroll bar to each of the elements returned by the specified selector
			/// </summary>
			/// <param name="selector" type="String">JQuery selector used to identify the target elements</param>
			if (!selector) {
				selector = ".customScrollBar";
			}

			var tinyScrollSettings = {
				axis: GetJSSession("CustomScrollBar_Axis"),
				wheel: GetJSSession("CustomScrollBar_Wheel").toUpperCase() == "TRUE" ? true : false,
				scroll: Number(GetJSSession("CustomScrollBar_WheelSpeed")),
				size: GetJSSession("CustomScrollBar_TrackSize"),
				sizethumb: GetJSSession("CustomScrollBar_ThumbSize"),
			};

			var options = $.extend({}, tinyScrollSettings, options);
			var width = "";
			if ((options.width) && (options.axis === 'x')) {
				width = " style='width:" + options.width + ";' ";
			}

			$(selector).each(function () {
				var scrollContainer = $(this);
				if (scrollContainer.find(".scrollbar").length === 0) {
					scrollContainer.wrapInner("<div class='viewport'><div class='overview' " + width + "></div></div>");
					scrollContainer.prepend('<div class="scrollbar"><div class="track"><div class="thumb"></div></div>');
					scrollContainer.tinyscrollbar(options);
				}
				else {
					scrollContainer.tinyscrollbar_update();
				}
			});

		}
		//#endregion

		//#region Utilites Functions 
		this.onError = function (resultError) {

		};
		this.Modules.DecodeHTML = function (id) {
			if (!id) {
				id = "inputcontent";
			}
			var str = "" + $("#" + id).html();
			str = str.replace(/&quot;/g, '"');
			str = str.replace(/&amp;/g, "&");
			str = str.replace(/&lt;/g, "<");
			str = str.replace(/&gt;/g, ">");

			$("#" + id).html(str);
		};
		this.Modules.LoadGalleryPreview = function () {
			var gallery = $("<iframe id='Gallery' marginheight='0' marginwidth='0' frameborder='0' scrolling='no' width='600' height='400' src='Gallery.aspx?gallerytype=" + getQueryVariable('gallerytype') + "'></iframe>");
			if ($("#headerimg")[0]) {
				$("#headerimg")[0].src = "";
				$("#headerimg")[0].style.visiblity = "hidden";
				$("#headerimg")[0].style.position = "absolute";
				$("#headerimg")[0].style.display = "none";
			}
			$("#inputcontent")[0].innerHTML = "";
			$(gallery).appendTo($("#inputcontent"));
		};
		this.Modules.SendMail = function (e) {
			var myparent;
			var getparent = function (current) {
				if ($(current).parent().attr("id") == "EmailDetail") {
					myparent = $(current).parent();
				}
				else {
					getparent($(current).parent());
				}
			};

			getparent($(e));


			if (validateForm("EmailDetail", false, '', false, null, myparent)) {
				return;
			}

			var EmailArray = $(myparent).find(':input[name^="email"]').serializeArray();

			var subject = findInArray(EmailArray, "email-subject");
			var emailFrom = findInArray(EmailArray, "email-FromEmail");
			var emailFromName = findInArray(EmailArray, "email-FromName");
			var gratitudeMessage = $("#email-SuccessMessage input").val();
			var message = "<table>";

			if ($('td[id^="email"][name^="email"]"').length > 0) {
				$('td[id^="email"][name^="email"]"').each(
                    function () {
                    	message += "<tr><td colspan='2'>" + $(this).html() + "</td><tr>";
                    }
                );
			}
			for (var i = 0; i < EmailArray.length; i++) {
				if (EmailArray[i].name != "email-sendToClient" && EmailArray[i].name != "email-subject") {
					message += "<tr><td valign=top>" + EmailArray[i].name.replace("email-From", "").replace("email-", "").replace(/_/gi, " ") + "</td><td valign=top>: " + EmailArray[i].value + "</td><tr>";
				}

			}
			if ($("#email-Extra").length > 0) {
				var inputs = $("#email-Extra input,select").each(
                    function () {
                    	$(this).parent().append($(this).val());
                    	$(this).remove();
                    }
                );
				message += String.format("<tr><td colspan='2'>{0}</td></tr>", $("#email-Extra").html());
			}
			message += "</table>";
			var sendToClient = $("#email-sendToClient").length == 0 ? true : false;
			var customResponse = $("#responsemsg").html();
			//replace any field place holders within custom message
			if (customResponse && customResponse.length) {
				for (var field = 0; field < EmailArray.length; field++) {
					var itemHolder = "{" + EmailArray[field].name.replace("email-", "") + "}";
					var start = "";
					var end = ""

					while (customResponse.indexOf(itemHolder) > -1) {
						start = customResponse.substr(0, customResponse.indexOf(itemHolder)) + EmailArray[field].value;
						end = customResponse.substr(customResponse.indexOf(itemHolder) + itemHolder.length);

						customResponse = start + end;
					}

				}
			}


			Aurora.Services.AuroraWebService.SendMail(emailFrom, emailFromName, message, subject, sendToClient, customResponse, SendMailSuccess, SendMailFail, gratitudeMessage);
			$("#Contents").block({ message: null });
			function SendMailSuccess(result, userContext) {
				if (result.Result === true) {
					if (!userContext) {
						$(myparent).html("<tr><td>Thank you for your response we will contact you shortly.</td></tr>");
					}
					else {
						if (userContext == "") {
							$("#EmailDetail").html("Thank you for your response we will contact you shortly.");
						}
						else {
							$("#EmailDetail").html(userContext);

						}
					}
					var elementScroll = "Menu";
					var offSet = -300;
					if (GetJSSession("Page_scrollTo")) {
						elementScroll = "EmailDetail";
					}

					if (GetJSSession("Page_offSet")) {
						offSet = GetJSSession("Page_offSet");
					}
					scrollToElement(elementScroll, offSet, 1000);
				} else {
					alert(result.ErrorMessage);
				}
				$("#Contents").unblock();
			}
			function SendMailFail(result) {
				if (typeof result.status == "undefined") {
					alert(result);
				} else {
					alert("error status: " + result.status + " statusText: " + statusText);
				}
				$("#Contents").unblock();
			}
		};
		this.Modules.sendFeedback = function () {
			if (!validateForm("CustomerFeedback", false, "", false)) {
				var entity = {};
				var values = $("#CustomerFeedback input,textarea");
				entity.ID = 0;
				entity.FirstName = values[0].value;
				entity.LastName = values[1].value;
				entity.Email = values[2].value;
				entity.ContactNumber = values[3].value;
				entity.Comment = values[4].value;
				entity.isApproved = false;
				entity.InsertedOn = new Date();

				function saveEntity(result) {
					if (result.Result) {
						$("#CustomerFeedback input:text,textarea").val("");
						$("#CustomerFeedback").hide();
						$("<div>Thank you for your comment.</div><br/><br/>").insertAfter("#CustomerFeedback");
						//TODO Send EMail
						//Aurora.Services.AuroraWebService.SendMail(emailFrom, emailFromName, message, subject, sendToClient, SendMailSuccess, SendMailFail, gratitudeMessage);
					}
				}
				Aurora.Services.AuroraWebService.SendFeedback(entity, saveEntity, onError);
			}
		};
		//#endregion

		//#region Product System 
		this.Modules.ProductCatDS = null;
		this.Modules.ProductSchemaXML = null;
		this.Modules.ProductsDS = null;
		this.Modules.basketTimer = 0;
		this.Modules.cookieLoaded = false;
		this.Modules.hasColours = false;
		this.Modules.productDetailDS = null;
		this.Modules.productFamilyDS = null;
		this.Modules.statusMessageTimerID = 0;
		//Initialises the basket on page refresh.
		this.Modules.getCategoriesWithinCategory = function (categoryID, targetElement) {
			/// <summary>This will retrieve all categories that are directly related to this category </summary>
			/// <param name="categoryID" type="Number">The category parent ID</param>
			/// <param name="targetElement" type="String">This is where the resulted data will display in</param>
			if (AuroraJS.Modules.Members || AuroraJS.Modules.DefaultPage) {
				document.location = String.format("/{0}/{1}", "products", categoryID);
				return;
			}

			$("#inputcontent").show();
			$("#Contents").block({ message: null });

			if (AuroraJS.Modules.productDetailDS) {
				AuroraJS.Modules.productDetailDS.dispose();
			}

			AuroraJS.Modules.productDetailDS = null;
			$("#inputcontent").html("");
			AuroraJS.Modules.loadBasketFromCookies();
			$('#ProductCategoryList a').die('click');
			var response = function (result) {
				//convert into JSON
				if (result.Data != "{ \"NewDataSet\": }}" && result.Data != "") {
					result.Data = $.parseJSON(result.Data).NewDataSet.Table;
					//Acquire the template
					var count = !result.Data.length ? 1 : result.Data.length;
					$("#inputcontent").html("<h5 class='aurora-products-subcategories'>Sub-Categories within <span id='Prodname'></span> (" + count + ") <a class='showMoreCat' style='cursor:pointer;'>Show</a></h5>");
					$(fetchTemplate("ProductCategoryList")).attr("id", "ProductCategoryList").appendTo("#inputcontent");
					//Create a data view
					var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("ProductCategoryList"));
					$("#ProductCategoryList").hide();
					ds.set_data(result.Data);

					$('#ProductCategoryList a.viewcategories').die('click');
					$('#ProductCategoryList a.viewcategories').live('click',
                            function () {
                            	AuroraJS.Modules.getCategoriesWithinCategory($(this).attr("dataid")); return false;
                            }
                     );
					$("#inputcontent a.showMoreCat").die();
					$("#inputcontent a.showMoreCat").live('click', function () {
						if ($("#ProductCategoryList").is(":visible")) {
							$("#ProductCategoryList").toggle(500);
							$("#inputcontent a.showMoreCat").html("Show");
						}
						else {
							$("#ProductCategoryList").toggle(500);
							$("#inputcontent a.showMoreCat").html("Hide");
						}
					});


				}
				//updates menu current
				if (AuroraJS.Modules.globals.updateCurrentMenuItem) {
					$('#menu li').removeClass('current');
					$('#menu li a[productid=' + categoryID + ']').parent().addClass('current');


				}
				else {
					AuroraJS.Modules.globals.updateCurrentMenuItem = true;
				}

				//Updates BreadCrumbs only if the site has the Breadcrumbs module.

				if (AuroraJS.Modules.generateBreadCrumbs) {
					AuroraJS.Modules.generateBreadCrumbs();
				}
				if (AuroraJS.Modules.generateSubMenu) {
					AuroraJS.Modules.generateSubMenu();
				}

				AuroraJS.Modules.getProductForCategory(categoryID);
				//check for header image
				if ($("#pageHeaderImage").length > 0) {
					if ($("#pageHeaderImage img").length > 0) {
						$("#pageHeaderImage img").fadeAndRemove(500,
                            function () {
                            	$("#pageHeaderImage").append("<img style='display:none' id='pageHeader_" + categoryID + "' />");
                            	AuroraJS.Modules.LoadImage(String.format("/ClientData/{0}/Uploads/productcat_{1}.jpg", clientSiteID, categoryID), "pageHeader_" + categoryID);
                            }
                            );
					}
					else {
						$("#pageHeaderImage").append("<img style='display:none' id='pageHeader_" + categoryID + "' />");
						AuroraJS.Modules.LoadImage(String.format("/ClientData/{0}/Uploads/productcat_{1}.jpg", clientSiteID, categoryID), "pageHeader_" + categoryID);
					}

				}
			};

			Aurora.Services.AuroraWebService.GetCategoryChildren(categoryID, response, onError);
		};
		this.Modules.getCategoriesForProduct = function (productID, targetElement) {
			/// <summary>This will retrieve all categories that are directly related to this category </summary>
			/// <param name="categoryID" type="Number">The category parent ID</param>
			/// <param name="targetElement" type="String">This is where the resulted data will display in</param>

			$("#inputcontent").show();
			$("#Contents").block({ message: null });

			if (AuroraJS.Modules.productDetailDS) {
				AuroraJS.Modules.productDetailDS.dispose();
			}

			AuroraJS.Modules.productDetailDS = null;
			$("#inputcontent").html("");
			AuroraJS.Modules.loadBasketFromCookies();
			$('#ProductCategoryList a').die('click');
			var response = function (result) {
				//convert into JSON
				if (result.Data != "{ \"NewDataSet\": }}" && result.Data != "") {
					result.Data = $.parseJSON(result.Data).NewDataSet.Table;
					//Acquire the template
					var count = !result.Data.length ? 1 : result.Data.length;
					$("#inputcontent").html("<h5 class='aurora-products-subcategories'>Sub-Categories within <span id='Prodname'></span> (" + count + ") <a class='showMoreCat' style='cursor:pointer;'>Show</a></h5>");
					$(fetchTemplate("ProductCategoryList")).attr("id", "ProductCategoryList").appendTo("#inputcontent");
					//Create a data view
					var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("ProductCategoryList"));
					$("#ProductCategoryList").hide();
					ds.set_data(result.Data);

					$('#ProductCategoryList a.viewcategories').die('click');
					$('#ProductCategoryList a.viewcategories').live('click',
                            function () {
                            	AuroraJS.Modules.getCategoriesWithinCategory($(this).attr("dataid")); return false;
                            }
                     );
					$("#inputcontent a.showMoreCat").die();
					$("#inputcontent a.showMoreCat").live('click', function () {
						if ($("#ProductCategoryList").is(":visible")) {
							$("#ProductCategoryList").toggle(500);
							$("#inputcontent a.showMoreCat").html("Show");
						}
						else {
							$("#ProductCategoryList").toggle(500);
							$("#inputcontent a.showMoreCat").html("Hide");
						}
					});


				}
				//updates menu current
				if (AuroraJS.Modules.globals.updateCurrentMenuItem) {
					$('#menu li').removeClass('current');
					$('#menu li a[productid=' + result.CategoryID + ']').parent().addClass('current');


				}
				else {
					AuroraJS.Modules.globals.updateCurrentMenuItem = true;
				}

				//Updates BreadCrumbs only if the site has the Breadcrumbs module.

				if (AuroraJS.Modules.generateBreadCrumbs) {
					AuroraJS.Modules.generateBreadCrumbs();
				}
				if (AuroraJS.Modules.generateSubMenu) {
					AuroraJS.Modules.generateSubMenu();
				}

				AuroraJS.Modules.getProductForCategory(result.CategoryID);
				//check for header image
				if ($("#pageHeaderImage").length > 0) {
					if ($("#pageHeaderImage img").length > 0) {
						$("#pageHeaderImage img").fadeAndRemove(500,
                            function () {
                            	$("#pageHeaderImage").append("<img style='display:none' id='pageHeader_" + result.CategoryID + "' />");
                            	AuroraJS.Modules.LoadImage(String.format("/ClientData/{0}/Uploads/productcat_{1}.jpg", clientSiteID, result.CategoryID), "pageHeader_" + result.CategoryID);
                            }
                            );
					}
					else {
						$("#pageHeaderImage").append("<img style='display:none' id='pageHeader_" + result.CategoryID + "' />");
						AuroraJS.Modules.LoadImage(String.format("/ClientData/{0}/Uploads/productcat_{1}.jpg", clientSiteID, result.CategoryID), "pageHeader_" + result.CategoryID);
					}

				}
			};

			Aurora.Services.AuroraWebService.GetCategoryChildrenForProduct(productID, response, onError);
		};
		this.Modules.getProductForCategory = function (categoryID, categoryName) {
			/// <summary>Gets</summary>
			var results = function (result) {
				AuroraJS.Modules.ProductCatDS = $create(Sys.UI.DataView, {}, {}, {}, $get("ProductsCategory"));
				AuroraJS.Modules.ProductCatDS.set_data(result.Data);
				AuroraJS.Modules.getProducts(categoryID);



				//Ajax history: browser back button functionality (also checks if a product is being loaded)
				if ($.history && !AuroraJS.Modules.productIDToLoad) {
					$.history.load(String.format('/Products/{0}/{1}', categoryID, categoryName), 'true');
				}

				$("#Contents").unblock();
				document.title = result.Data[0].Name;
				$("#Prodname").html(document.title);
			};
			//get template
			var myModules = GetJSSession("Modules");
			var productCatTemplate = fetchTemplate("Products");



			$("#inputcontent").html($("#inputcontent").html() + "<div style='clear:both;'></div>" + productCatTemplate);
			$(".productThumbs").hide();
			Aurora.Services.AuroraWebService.GetProductCategories(categoryID, results, onError);
		};
		this.Modules.getProducts = function (cat, brandID) {

			var isBrandFilter = false;

			if (typeof brandID !== "undefined") {
				//Gerard: do some set up stuff, adding this here because for categories the below is handled by different functions when doing a product by category render
				isBrandFilter = true;

				$("#inputcontent").html("");

				//NOTE: In future if we include the brands in the menu we will need to set the current menu item and generate the breadcrumbs
				if (AuroraJS.Modules.generateSubMenu) {
					AuroraJS.Modules.generateSubMenu();
				}

				//Ajax history: browser back button functionality (also checks if a product is being loaded):
				if ($.history && !AuroraJS.Modules.productIDToLoad) {
					$.history.load(String.format('/products/b-{0}', brandID), 'true');
				}

			}

			var results = function (result) {
				//No products
				if (result.Data == "{ \"NewDataSet\": }}") {
					$("#ProductsCategory").hide();
					$("#inputcontent a.showMoreCat").click();
				}

				if (result.Data != "{ \"NewDataSet\": }}") {
					//convert into JSON
					AuroraJS.Modules.ProductSchemaXML = $.parseJSON(result.Data).NewDataSet.Table1 === undefined ? "" : $.parseJSON(result.Data).NewDataSet.Table1.SchemaXML;
					result.Data = $.parseJSON(result.Data).NewDataSet.Table;

					//brand specific render code
					if (isBrandFilter && result.Data) {

						var dataToRender;

						if(result.Count === 1){
							dataToRender = result.Data;
						} else if (result.Count > 1) {
							dataToRender = result.Data[0];
						} else {
							dataToRender = null;
						}

						if (dataToRender !== null) {
							//insert a template for the brand heading, allow it to be overridden
							var brandTemplate = typeof AuroraJS.Templates.BrandHeaderTemplate !== "undefined" ? $.templates(AuroraJS.Templates.BrandHeaderTemplate) : $.templates("<h2>{{:BrandName}}</h2>");
							$(brandTemplate.render(dataToRender)).appendTo("#inputcontent");
						}
					}

					//Acquire the template
					$(fetchTemplate("ProductCategoryView")).attr("id", "ProductCategoryView").appendTo("#inputcontent");
					//remove any objects 
					if (result.Data) {
						for (var dataElement = 0; dataElement < result.Data.length; dataElement++) {
							for (var object in result.Data[dataElement]) {
								if (result.Data[dataElement][object] instanceof Object) {
									result.Data[dataElement][object] = "";
								}
							}
						}
						//Create a data view
						var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("ProductCategoryView"));
						//Set global Var
						AuroraJS.Modules.ProductsDS = result.Data;
						//Bind data to template
						ds.set_data(result.Data);
						AuroraJS.Modules.DecodeHTML("inputcontent .products");
					}
					else {
						$(".showMoreCat").click();
						$("#ProductsCategory").hide()
					}

				}
				//Load all random images for a product, if they do not exist then load no image default
				$(".productImage").each(
                                function (index, ele) {
                                	var currentImageElement = $(this);
                                	var image = $(String.format("<img id='{0}' title='{2}' src='/ClientData/{1}/Uploads/sm_product_{0}.jpg'/>", $(this).attr("imageid"), clientSiteID, $(this).attr("imagename")));
                                	currentImageElement.append(image);
                                	image.load(
                                        function (result) {
                                        	var imageID = $(this).parent().attr("imageid");
                                        	var link = $(this).wrap("<a href='javascript:void(0);'/>").parent();
                                        	$(link).attr("url", String.format("/ClientData/{1}/Uploads/product_{0}.jpg", imageID, clientSiteID));
                                        	$(link).attr("title", $(this).attr("title"));
                                        }
                                    ).error(
                                    function (result) {
                                    	image.remove();
                                    	$("<img src='/Styles/images/no-image.jpg'/>").load(
                                        function (result) {
                                        	$(this).appendTo(currentImageElement);
                                        });
                                    });
                                });

				$(".productImage a").live('click',
                                                function () {
                                                	showFancyBoxPopup($(this).attr("url"), null, $(this).attr("title"));
                                                }
                                            );
				//unbind view click event if any 
				$('.viewproduct').die('click');
				//bind click event to view product details
				$('.viewproduct').live('click', function () {
					try {

						AuroraJS.Modules.getProductDetails($(this).attr("dataid"));
						AuroraJS.Modules.getProductGallery($(this).attr("dataid"));
						return false;
					}
					catch (e) {
						return false;
					}
				}).attr('href', 'javascript:void(0); return false;');

				//check for currency conversions and insert if found
				if ($.CurrencyConverter) {
					$.CurrencyConverter($('.products .conversions'));
				}

				if (AuroraJS.Bespoke.getProducts) {
					AuroraJS.Bespoke.getProducts();
				}
				//All work is complete show products
				$(".productThumbs").show();
				//scroll to products
				scrollToElement("inputcontent", 10, 1000);

				if (AuroraJS.Modules.productIDToLoad) {
					AuroraJS.Modules.getProductDetails(AuroraJS.Modules.productIDToLoad);
					AuroraJS.Modules.getProductGallery(AuroraJS.Modules.productIDToLoad);
					//$("#inputcontent a.viewproduct[dataid=" + AuroraJS.Modules.productIDToLoad + "]").click();
					AuroraJS.Modules.productIDToLoad = undefined;
				}
			};
			//call service method
			if (!isBrandFilter) {
				Aurora.Services.AuroraWebService.GetProductsPerCategory(cat, results, onError);
			} else {
				Aurora.Services.AuroraWebService.GetProductsPerBrand(brandID, results, onError);
			}
		};
		this.Modules.getProductDetails = function (productID) {
			/// <summary>Displays a product with all its information such as Gallery and Product Properties.</summary>
			/// <param name="productID" type="Number">The id of the product that has been requested</param>
			var product = findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", productID);

			if (!product) {
				product = AuroraJS.Modules.ProductsDS;
			}

			var productIndex = !AuroraJS.Modules.ProductsDS.length ? 0 : findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", product.ProductID, true);
			var prdTempl = fetchTemplate("ProductDetails");

			$("#inputcontent").append(prdTempl);
			//$("#inputcontent").html($("#inputcontent").html() + prdTempl);

			if ($.history) {
				$.history.load(String.format('/Product/{0}', productID), 'true');
			}

			if (!product) {
				return;
			}

			//get this products full hierachy
			AuroraJS.Modules.getAllProductCombinations(product.ProductID);
			//Get all associated items
			AuroraJS.Modules.getAssociatedProducts(product.ProductID);
			//Add back Button 
			var init = false
			$('.products').fadeOut(0, "", function () {
				if (!init) {
					$("<div><a class='back' href='javascript:' onclick=\"AuroraJS.Modules.productBack(" + product.CategoryID + ");return false;\">Back </a></div>").insertAfter(".products").last();
					//                scrollToElement("Menu", 10, 2000);
				}
			});


			if (AuroraJS.Modules.productDetailDS == null) {
				//Bind template
				AuroraJS.Modules.productDetailDS = $create(Sys.UI.DataView, {}, {}, {}, $(".productinfo").get(0));
			}

			$("#ProductDetails").hide();
			AuroraJS.Modules.productDetailDS.set_data(product);
			AuroraJS.Modules.DecodeHTML("inputcontent .productinfo");
			$("#ProdTable0 tr").children('td').each(function () {
				var domEle = $(this);
				if (domEle.html().toString() == "null" || domEle.html() == null || domEle.html() == "[object Object]" || domEle.html().replace(/\s/gi, '').length == 0 || domEle.html() == "0.00") {
					domEle.parent().remove();
				}
			})

			//get basket button template
			var buttonTemplate = typeof AuroraJS.Templates.AddToBasketButton !== "undefined" ? $.templates(AuroraJS.Templates.AddToBasketButton) : $.templates("<br/><div class='addToBasket' style='float:left;'><input type='button' id='addItems' value='Add' />&nbsp;<input id='quantity' value='1' type='text'/><input type='button' id='viewItems' value='View Basket' /></div><div style='clear:both;' />");
			$(buttonTemplate.render()).appendTo("#ProductInformation0");

			//("#ProductInformation0").append("<br/><div class='addToBasket' style='float:left;'><input type='button' id='addItems' value='Add' />&nbsp;<input id='quantity' value='1' type='text'/><input type='button' id='viewItems' value='View Basket' /></div><div style='clear:both;' />");
			$("#quantity").keypress(function (e) {
				if (e.keyCode == 13) {
					$("#addItems").click();
					return false;
				}
			}).ForceNumericOnly();

			$("#viewItems").click(
                function () {
                	if ($("#productBasket").length == 0) {
                		AuroraJS.Modules.createBasket();
                	}
                	else {
                		$("#productBasket").fadeIn(700);
                		$('.basketBody').show('slow');
                	}
					
                });
			$("#addItems").click(function (e) {
				var $this = $(e.currentTarget);
				if ($("#productBasket").length == 0) {
					AuroraJS.Modules.createBasket();
				}
				else {
					$("#productBasket").fadeIn(700);

				}
				var countQty = 1
				if ($("#quantity").val() != "" && $("#quantity").val() != "0") {
			
					AuroraJS.Modules.addToBasket($this.data("proddata"), false, $("#quantity").val());

				}


			});
			$('#addItems').data("proddata", product);


			//Add Custom Fields And Product Tabs
			if (product.CustomXML && product.CustomXML.toString() != "") {
				var xmlDoc = $.parseXML(product.CustomXML);
				$xml = $(xmlDoc);

				if (AuroraJS.Modules.ProductSchemaXML) {
					var xmlSchema = $($.parseXML(AuroraJS.Modules.ProductSchemaXML));
					var customFieldTpl = fetchTemplate("ProductCustomField");
					var customFields = [];
					customFieldTpl = customFieldTpl || "<!-- Could not find template data -->";
					xmlSchema.find('Fields[type"CustomFields"] field').each(function () {
						var field = $(this);
						var fieldName = field.attr('fieldname');
						var fieldLabel = field.find('label').text();
						var fieldValue = $xml.find('Fields[type="CustomFields"] field[fieldname="' + fieldName + '"]').attr('value');

						var renderedField = customFieldTpl.replace(/{{fieldname}}/gi, fieldLabel);
						renderedField = renderedField.replace(/{{value}}/gi, fieldValue);
						renderedField = renderedField.replace(/{{fieldid}}/gi, fieldName);
						if (fieldValue) {
							customFields.push(renderedField);
						}
					});
					$('#ProductCustomFields0').append(customFields.join(''));
					if (AuroraJS.Bespoke.CustomXML) {
						AuroraJS.Bespoke.CustomXML(product);
					}
				}

				//Add Tabs
				if ($xml.find('Fields[type="Tabs"] field').length > 0) {

					var item = [];

					$fields = $xml.find('Fields[type="Tabs"] field');
					$.each($fields, function (i, field) {
						if ($(field).attr("fieldname") == "TabName") {
							item.push({ name: $(field).attr('value'), order: $(field).next().attr('value'), desc: $(field).next().next().attr('value') });
						}
					});

					$(fetchTemplate("ProductTabs")).attr("id", "ProductTabs").appendTo("#ProductTabContainer0");


					//create buttons & tabs

					$(item).each(function (index, domEle) {
						$(String.format("<div id='tab{1}'>{0}</div>", this.name, index)).appendTo(".tab-buttons");
						$(String.format("<div id='tabcontent_{1}'>{0}</div>", this.desc, index)).appendTo(".tabcontent");

					});

					$(".tabcontent").children('div').hide();
					$(".tab-buttons>div").first().addClass("current");
					$(".tabcontent").children('div').first().show();
					$("<br style='clear:both'/>").insertAfter(".tab-buttons");

					//bind events
					$(".tab-buttons").children('div').click(
						function () {
							$(".tab-buttons>div").removeClass('current');
							$(this).addClass('current');
							$(".tabcontent").children('div').hide();
							$("#" + $(this).attr("id").toString().replace("tab", "tabcontent_")).show();
						}
					);


				}
			}
			AuroraJS.Modules.bindProductFacebookLike(productID, product.CategoryID);

			//check for currency conversions and insert if found
			if ($.CurrencyConverter) {
				$.CurrencyConverter($('.productinfo .conversions'));
			}

			if (AuroraJS.Bespoke.getProductDetails) {
				AuroraJS.Bespoke.getProductDetails(product);
			}
			$("#ProductDetails").fadeIn(500);


		};
		this.Modules.productBack = function () {
			var prodData = $('#addItems').data("proddata")
			$('.products').fadeIn(600);
			$('.back,.productinfo').remove();

			if (AuroraJS.Modules.productDetailDS)
				AuroraJS.Modules.productDetailDS.dispose();

			AuroraJS.Modules.productDetailDS = null;
			if ($.history) {
				$.history.load(String.format('/Products/{0}/{1}', prodData.CategoryID, undefined), 'true');
			}
			if (AuroraJS.Bespoke.productBack) {
				//Runs at the end of the product Back function
				AuroraJS.Bespoke.productBack(prodData);
			}
		};
		//#region bindProductArrows
		this.Modules.bindProductArrows = function (productIndex) {
			/// <summary>
			/// Binds next and back product arrows to a product detail.
			/// </summary>
			/// <param name="productIndex" type="int">the index of the current product in AuroraJS.Modules.ProductDS</param>
			if (productIndex <= 0) {
				$(".lastProduct").hide();
				productIndex = 0;
			}
			else {
				$(".lastProduct").show();
				$(".lastProduct").click(function () {

					$(".lastProduct").unbind('click');
					$('.back,.productinfo,.nextProduct,.lastProduct').remove();
					AuroraJS.Modules.productDetailDS.dispose();
					AuroraJS.Modules.productDetailDS = null;
					try {
						AuroraJS.Modules.getProductDetails(AuroraJS.Modules.ProductsDS[productIndex - 1].ProductID);
						AuroraJS.Modules.getProductGallery(AuroraJS.Modules.ProductsDS[productIndex - 1].ProductID);
						return false;
					}
					catch (e) {
						return false;
					}
				});
			}
			var length = (!AuroraJS.Modules.ProductsDS.length) ? 0 : AuroraJS.Modules.ProductsDS.length;
			if (productIndex >= length - 1) {
				$(".nextProduct").hide();
				productIndex = length - 1;
			}
			else {
				$(".nextProduct").show();
				$(".nextProduct").click(function () {
					$(".nextProduct").unbind('click');
					$('.back,.productinfo,.nextProduct,.lastProduct').remove();
					AuroraJS.Modules.productDetailDS.dispose();
					AuroraJS.Modules.productDetailDS = null;
					try {
						AuroraJS.Modules.getProductDetails(AuroraJS.Modules.ProductsDS[productIndex + 1].ProductID);
						AuroraJS.Modules.getProductGallery(AuroraJS.Modules.ProductsDS[productIndex + 1].ProductID);
						return false;
					}
					catch (e) {
						return false;
					}

				});
			}



		};
		//#endregion
		this.Modules.getProductGallery = function (productID) {
			/// <summary>Gets a gallery of images for a selected product</summary>
			/// <param name="productID" type="Number">ID of the selected Product</param>

			//show loader in gallery
			$("#ProductGallery0").block({ message: null });
			loadImage = function (url) {
				if ($(".mainscreen img").attr("src") == url) {
					return;
				}
				$(".mainscreen").block({ message: null });
				var image = $("<img class='mainscreenimage' />");
				image.attr("src", url + "?" + new Date().getMilliseconds());
				image.hide();
				image.load(function () {
					$(".mainscreen").html("").append(image);


					image.fadeIn(500);
					$(".mainscreen").unblock();

				}).error(
					function () {
						$(".mainscreen").html("").append("<img src='/Styles/images/no-image.jpg' />")
						.height(300 * (200 / $(".mainscreenimage").height())).width(200).fadeIn(500);
						$(".mainscreen").unblock();
					}
				);
			};

			var response = function (result) {
				$("#ProductGallery0").unblock();
				//create main display and components
				var screen = $("<div class='mainscreen' ></div>");
				var colors = $("<tr class='productColours' id='Colours0'><td><b>Select a colour</b></td><td style='padding-right:10px;padding-left:10px;'>:</td><td></td></tr>");
				var thumbs = $("<div class='productThumbnails'></div>");
				//Append to main container
				$(screen).appendTo("#ProductGallery0");
				if ($('.productinfo #ProductColoursPlaceholder0').length > 0) {
					$(colors).appendTo('.productinfo #ProductColoursPlaceholder0');
				}
				else {
					$(colors).appendTo("#ProdTable0");
				}
				$(thumbs).appendTo("#ProductGallery0");
				//
				if (result.Data.length == 0) {
					$(".mainscreen").html("").append("<img src='/Styles/images/no-image.jpg' />");
					var product = findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", productID);
					var productIndex = !AuroraJS.Modules.ProductsDS.length ? 0 : findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", product.ProductID, true);
					$.when(AuroraJS.Modules.getProductSizes(productID), AuroraJS.Modules.getProductTextures(productID)).then(function () {
						if ($(".lastProduct, .nextProduct").length > 0) {
							AuroraJS.Modules.bindProductArrows(productIndex);
						}

						if (AuroraJS.Bespoke.getProductSizes) {
							//implement any bespoke functionality on the size data
							AuroraJS.Bespoke.getProductSizes(productIndex);
						}
					});
					return;
				}
				$(result.Data).each(
					function (index, ele) {
						//display the first image in the result set
						if (index == 0) {
							loadImage(String.format("/ClientData/{0}/Uploads/product_{1}.jpg?{2}", clientSiteID, this.ID, new Date().getMilliseconds()));
						}
						//add subsequent images into the tumbnail view.

						var thumbImage = $("<img  />");
						thumbImage.attr("src", String.format("/ClientData/{0}/Uploads/sm_product_{1}.jpg?{2}", clientSiteID, this.ID, new Date().getMilliseconds()));
						$(thumbImage).hide();
						$(thumbImage).css({ cursor: 'pointer' });
						$(thumbImage).load(
								function () {
									$(this).fadeIn(500);
								}
							).click(
								function () {
									loadImage($(this).attr("src").replace("sm_", ""));
								}
							);
						$(".productThumbnails").append(thumbImage);

					}
				);
				var product = findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", productID);
				var productIndex = !AuroraJS.Modules.ProductsDS.length ? 0 : findByInArray(AuroraJS.Modules.ProductsDS, "ProductID", product.ProductID, true);
				$.when(AuroraJS.Modules.getProductSizes(productID), AuroraJS.Modules.getProductTextures(productID)).then(function () {
					if ($(".lastProduct, .nextProduct").length > 0) {
						AuroraJS.Modules.bindProductArrows(productIndex);
					}
					if (AuroraJS.Bespoke.getProductSizes) {
						//implement any bespoke functionality on the size data
						AuroraJS.Bespoke.getProductSizes(productIndex);
					}
				});
			};

			Aurora.Services.AuroraWebService.GetProductGallery(productID, response, onError);
		};
		this.Modules.getProductTextures = function (productID) {
			/// <summary>Gets the colours for the selected product.</summary>
			/// <param name="productID" type="Number">ID of the selected Product</param>

			var dfd = $.Deferred();
			$(fetchTemplate("ProductColours")).attr("id", "ProductColours").appendTo($("#Colours0 td").last());
			if ($("#ProductColours").length === 0) {
				return;
			}
			var dataset = $create(Sys.UI.DataView, {}, {}, {}, $get("ProductColours"));

			var response = function (result) {
				if (result.Data.length != 0) {
					dataset.set_data(result.Data);
					$(dataset._element).children('div').each(
					function () {

						$(this).css('background', "#" + $(this).attr("colour") + "url('" + $(this).attr('texture') + "')");
						//$(this).css('backgroundImage', $(this).attr('texture'));
						$(this).click(
							function () {
								$(this).parent().children('div').removeClass('colourSelected');
								$(this).addClass("colourSelected");
							}
						);
					}
					);
					AuroraJS.Modules.hasColours = true;

				}
				else {
					AuroraJS.Modules.hasColours = false;
					$("#Colours0").hide();
				}
				dfd.resolve();
			};

			Aurora.Services.AuroraWebService.GetProductColours(productID, response, onError);
			return dfd.promise();
		};
		this.Modules.getProductSizes = function (productID) {
			/// <summary>Gets the relative sizes for a selected product</summary>
			/// <param name="productID" type="Number">ID of the selected Product</param>

			var dfd = $.Deferred();
			var response = function (result) {
				if (result.Data) {
					$(String.format("<tr class='productSizeSelect'><td><b>Select a Size</b></td><td style='padding-right:10px;padding-left:10px;'>:</td><td><select id='Sizes' class='sys-template'>{0}</select></td></tr>", fetchTemplate("ProductSizes"))).appendTo($("#ProdTable0"));
					if (!$get("Sizes")) {
						return;

					}
					var sizeDS = $create(Sys.UI.DataView, {}, {}, {}, $get("Sizes"));

					sizeDS.set_data(result.Data);

				}
				dfd.resolve();
			};

			Aurora.Services.AuroraWebService.GetProductSizes(productID, response, onError);
			return dfd.promise();
		};
		//#region bindProductFacebookLike
		this.Modules.bindProductFacebookLike = function (productID, CategoryID) {
			/// <summary>
			/// Binds a facebook like to the current product. Only binds if a container exists with the class 'facebookLike'
			/// </summary>
			/// <param name="productID" type="int">The ID of the product being liked</param>
			/// <param name="CategoryID" type="int">The ID of the Category the product being liked belongs to</param>
			var container = $(".productinfo .facebookLike");
			//Check whether the template contains a facebook like option
			if (container.length > 0) {
				//Facebook code to load the facebook API
				//            (function (d, s, id) {
				//                var js, fjs = d.getElementsByTagName(s)[0];
				//                if (d.getElementById(id)) return;
				//                js = d.createElement(s); js.id = id;
				//                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				//                fjs.parentNode.insertBefore(js, fjs);
				//            } (document, 'script', 'facebook-jssdk'));
				// end of facebook API Load

				//            var elem = $(document.createElement("fb:like"));
				//            elem.attr("layout", "button_count");
				//            elem.attr("show_faces", "false");
				//            elem.attr("action", "recommend");
				//            elem.attr("href", String.format("http://" + document.domain + "/Product/{0}/{1}", CategoryID, productID));

				var elem = $(document.createElement("div"));
				elem.addClass('fb-like');
				elem.attr("data-layout", "button_count");
				elem.attr("data-show-faces", "false");
				elem.attr("data-action", "recommend");
				elem.attr("data-href", String.format("http://" + document.domain + "/Product/{0}/{1}", CategoryID, productID));

				$(".productinfo .facebookLike").empty().append(elem);
				try {
					if (FB) {
						FB.XFBML.parse($(".productinfo .facebookLike").get(0));
					}
					else {
						$("<div>Facebook XFBML not loaded. Please contact the website administrator</div>").dialog({
							modal: true,
							resizable: false,
							title: "Error",
							width: '400',
							buttons: { "Okay": function () { $(this).dialog("close") } }
						});
					}
				} catch (e) {
					$("<div>Could not contact facebook server please check your internet connectivity or proxy settings.</div>").dialog({
						modal: true,
						resizable: false,
						title: "Error",
						width: '400',
						buttons: { "Okay": function () { $(this).dialog("close") } }
					});
				}
			}
		};
		//#endregion

		//#region Basket 
		this.Modules.createBasketFunctions = function () {
			/// <summary>Creates the controls within the product basket.</summary>

			$("<a class='ClearBasket' href='javascript:'>Clear</a>").click(
				function () {
					if ($(".basketList ul li").length != 0) {
						var ans = confirm("Do you want to remove all the items in your basket?");
						if (ans) {
							$(".basketList ul").remove();
							$.cookie('productbasket', null);
							$(".basketCount").attr("count", 0);
							$(".basketCount").html(String.format("My Basket ({0})", 0));
							AuroraJS.Modules.addToBasket(null, true);
							AuroraJS.Modules.setBasketLink();
						}
					}
				}
			).appendTo(".basketfunctions");


		};
		this.Modules.createBasket = function () {
			/// <summary>Creates the product basket.</summary>

			if ($("#productBasket").length == 0) {

				var template = typeof AuroraJS.Templates.ProductBasketTemplate !== "undefined" ? $.templates(AuroraJS.Templates.ProductBasketTemplate) : $.templates("<div id='productBasket' style=''><div class='basketHead'><div style='float:left;display:block;width: 50%;' class='basketCount' count='0'><b>My Basket</b></div></div><div class='BasketHeader basketBody'>You have added the following product/s to your basket</div><div class='basketList basketBody'></div><div class='buttons basketBody'><div class='checkout basketBody' style='width: 44%; text-align: right; float: right; display: block; padding-right: 20px;'>Checkout</div><div class='basketfunctions basketBody'><div class=''></div></div></div>");

				$(template.render()).insertAfter("#inputcontent");
				//create basket functions buttons
				AuroraJS.Modules.createBasketFunctions();
				$("#productBasket").mouseout(
					function () {
						AuroraJS.Modules.basketTimer = setTimeout(function () {
							$(".basketBody").fadeOut('slow');
						}, 25000);
					}
				).mouseover(
					function () {
						clearTimeout(AuroraJS.Modules.basketTimer);
					}
				);
			}
			$(".basketHead").click(function (e) {

				$('.basketBody').show('slow');
			});
				

			$(".checkout").click(
					function () {
						AuroraJS.Modules.checkOut();
					}
				);

			if (!$(".basketBody").is(":visible")) {
				$(".basketBody").fadeIn('slow');
			}

			$(".basketCount").attr("count", parseInt($(".basketCount").attr("count")) + 0);
			//$(".basketCount").html(String.format("My Basket", parseInt($(".basketCount").attr("count"))));
		};
		this.Modules.renderBasketItem = function (ID, Count, Name, Size, SizeName, Colour, ColourHex, IsZeroShipping, IsFixedShipping, ShippingPriceLoc, ShippingPriceInt) {
			var basketItemString;
			var itemObject = {
				ID: ID,
				Count: Count,
				Name: Name,
				Size: Size,
				SizeName: SizeName,
				Colour: Colour,
				ColourHex: ColourHex,
				IsZeroShipping: IsZeroShipping,
				IsFixedShipping: IsFixedShipping,
				ShippingPriceLoc: ShippingPriceLoc,
				ShippingPriceInt: ShippingPriceInt
			};
			$.views.helpers({ shortenString: shortenString });
			var template = $.templates("<li class='basketItem' count='{{:Count}}' id='{{:ID}}' name='{{:Name}}' size='{{:Size}}' color='{{:Colour}}' colourhex='{{:ColourHex}}' sizename='{{:SizeName}}' iszeroshipping='{{:IsZeroShipping}}' isfixedshipping='{{:IsFixedShipping}}' shippingpriceloc='{{:ShippingPriceLoc}}' shippingpriceint='{{:ShippingPriceInt}}'><span class='productName'>{{:~shortenString(Name, 15)}} ({{:Count}})</span><div style='margin-right:10px;float:left;width:12px;height:12px;background:#{{:ColourHex}}' class='colorswatch'></div><div class='deleteItem'><img class='deleteImage' src='/Styles/images/cross.png'/>&nbspRemove item</div><div class='clear'></div></li>");
			return template.render(itemObject);
		};
		this.Modules.addToBasket = function (productData, isCookieLoad, totalAmount) {
			/// <summary>Adds a selected product to the basket and the cookie</summary>
			/// <param name="productData" type="Object">The data object of the selected product.</param>
			/// <param name="isCookieLoad" type="Boolean">If the method is being called from the load Cookie method.</param>
			/// <param name="totalAmount" type="Number">The total quantity for the selected product</param>

			if (!AuroraJS.Modules.validateAddToBasket()) {
				return;
			}

			if (!totalAmount) {
				totalAmount = 1;
			}

			var appendToCookie = true;
			var itemCount = 0;
			var items = "";
			//check if items have been loaded before
			if ($("#productBasket .basketList ul").length == 0) {
				$("<ul></ul>").appendTo(".basketList");
			}

			//check if load is from a cookie
			if (!isCookieLoad && productData) {
				itemCount = $("#" + productData.ProductID).length > 0 ? Number($("#" + productData.ProductID).attr("count")) + Number(totalAmount) : 1;

				//get colour & size
				var selColour = $("#ProductColours .colourSelected").attr("colourid");
				var selSize = $("#Sizes").val();
				var sizeExists = $("#productBasket .basketList ul li[size=" + selSize + "]");;
				var sizeName = $("#Sizes option[value='" + selSize + "']").html();
				productData = AuroraJS.Modules.getProductForBasket(selColour, selSize);
				//check if the product exists in the list
				if (itemCount == 1) {
					//create a new instance of this item
					$("#productBasket .basketList ul").append(AuroraJS.Modules.renderBasketItem(productData.ID, totalAmount, productData.Name, selSize, sizeName, selColour, $(".colourSelected").attr("colour"),productData.isZeroShipping, productData.isFixedShipping, productData.ShippingPriceLoc, productData.ShippingPriceInt));
				}
				else {
					//check if product exist with the same colour
					if ($("#Colours0").is(":hidden")) {
						var listItem = $("#productBasket .basketList ul li[size='" + selSize + "'][id='" + productData.ID + "']");
					}
					else {
						var listItem = $("#productBasket .basketList ul li[color=" + selColour + "][size='" + selSize + "'][id='" + productData.ID + "']");
					}

					if (listItem.attr("color") != undefined && sizeExists.length > 0) {
						listItem.attr("count", Number((listItem).attr("count")) + Number(totalAmount));
						listItem.replaceWith(AuroraJS.Modules.renderBasketItem(listItem.attr('id'), listItem.attr('count'), listItem.attr('name'), listItem.attr('size'), listItem.attr('sizename'), listItem.attr('color'), listItem.attr('colourhex'), listItem.attr('iszeroshipping'), listItem.attr('isfixedshipping'), listItem.attr('shippingpriceloc'), listItem.attr('shippingpriceint')));
					}
					else {
						$("#productBasket .basketList ul").append(AuroraJS.Modules.renderBasketItem(productData.ID, totalAmount, productData.Name, selSize, sizeName, selColour, $(".colourSelected").attr("colour"), productData.isZeroShipping, productData.isFixedShipping, productData.ShippingPriceLoc, productData.ShippingPriceInt));
					}

				}
				$("#" + productData.ID).data("proddata", productData);
				$(".basketCount").attr("count", Number($(".basketCount").attr("count")) + Number(totalAmount));
			}
			$(".basketCount").html(String.format("My Basket", parseInt($(".basketCount").attr("count"))));
			$("#productBasket .basketList").animate({ scrollTop: $("#productBasket .basketList").attr("scrollHeight") }, 400);
			$('.basketBody').show('slow');

			$("#productBasket .basketList ul li").each(
				function (index, ele) {
					var myItem = $(this);
					//write to cookie
					items += String.format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10},", myItem.attr("id"), myItem.attr("count"), myItem.attr("name"), myItem.attr("color"), myItem.attr("size"), myItem.attr('colourhex'), myItem.attr('sizename'), myItem.attr('iszeroshipping'), myItem.attr('isfixedshipping'), myItem.attr('shippingpriceloc'), myItem.attr('shippingpriceint'));
				}
			);
			$.cookie('productbasket', items, { expires: 2, path: "/" });
			AuroraJS.Modules.bindDeleteFunction();
			AuroraJS.Modules.setBasketLink();
			//Basket Item Added Message
			if ($(".productBasketAddMessage").length > 0) {
				clearTimeout(AuroraJS.Modules.statusMessageTimerID);
				if (!$(".productBasketAddMessage").attr("template")) {
					$(".productBasketAddMessage").attr("template", $(".productBasketAddMessage").html());
				}

				$(".productBasketAddMessage").html($(".productBasketAddMessage").attr("template"));

				for (var property in productData) {
					$(".productBasketAddMessage").html($(".productBasketAddMessage").html().toString().replace("{" + property + "}", productData[property]));
				}
				$(".productBasketAddMessage").html($(".productBasketAddMessage").html().toString().replace(/{TotalAmount}/gi, totalAmount));
				$(".productBasketAddMessage").stop()._fadeIn(500, 'swing', function () {
					AuroraJS.Modules.statusMessageTimerID = setTimeout(function () { $(".productBasketAddMessage").fadeOut(500); }, 2000);
				});
			}
		};
		this.Modules.loadBasketFromCookies = function () {
			/// <summary>
			/// Reads from the product cookie and loads the basket on site visit.
			/// </summary>



			if ($.cookie('productbasket') && !AuroraJS.Modules.cookieLoaded) {
				AuroraJS.Modules.createBasket();
				$(".basketCount").attr("count", 0);
				//check if items have been loaded before
				$("#productBasket .basketList").html("");
				if ($("#productBasket .basketList ul").length == 0) {
					$("<ul></ul>").appendTo(".basketList");
				}
				var products = $.cookie('productbasket').toString().split(',');

				$(products).each(
					function () {
						if (this != "") {

							var currentItem = this.toString().split('|');
							$(".basketCount").attr("count", Number($(".basketCount").attr("count")) + Number(currentItem[1]));
							$(".basketCount").html(String.format("Product Basket ({0})", parseInt($(".basketCount").attr("count"))));
							$("#productBasket .basketList ul").append(AuroraJS.Modules.renderBasketItem(currentItem[0], currentItem[1], currentItem[2], currentItem[4], currentItem[6], currentItem[3], currentItem[5], currentItem[7], currentItem[8], currentItem[9], currentItem[10]));
						}
					}
				);

				AuroraJS.Modules.addToBasket("", true);
				AuroraJS.Modules.cookieLoaded = true;
			}
			AuroraJS.Modules.setBasketLink();
			if (document.location.toString().toLowerCase().indexOf("basket.aspx") > 0 && AuroraJS.Modules.getProductBasket) {
				AuroraJS.Modules.getProductBasket();
			}
		};
		this.Modules.deleteBasketItem = function (anchor) {
			/// <summary>
			/// Deletes an item from the basket and the cookie
			/// </summary>
			/// <param name="anchor" type="HTMLObject">The anchor tag that the event originated from</param>


			var basketItem = $(anchor.target).parents('.basketList li.basketItem');
			$(".basketCount").attr("count", Number($(".basketCount").attr("count")) - Number(basketItem.attr("count")));
			basketItem.remove();
			AuroraJS.Modules.addToBasket("", true);

		};
		this.Modules.bindDeleteFunction = function () {
			$('#productBasket .deleteItem').die('click');
			$('#productBasket .deleteItem').live('click', AuroraJS.Modules.deleteBasketItem);
		}
		this.Modules.cancelOrder = function () {
			/// <summary>Stops an enquiry from being processed, and returns the basket to its normal state </summary>
			$("#productBasket").remove();
			AuroraJS.Modules.createBasket();
			AuroraJS.Modules.cookieLoaded = false;
			AuroraJS.Modules.loadBasketFromCookies();
			AuroraJS.Modules.addToBasket(null, true);
		};
		this.Modules.checkOut = function () {
			/// <summary>
			/// Depending on module setup if the E-Commerce system is available then the user will
			/// be redirected to Basket.aspx, if not then the user will be presented with an enquiry
			/// form.
			//</summary>

			if (AuroraJS.Modules.getProductBasket) {
				document.location = "/Basket.aspx";
				$("#productBasket").block({ message: null });
				return;
			}

			if ($("#productBasket .basketList ul li").length === 0 && !$("#PlaceOrder").is(":visible")) {
				x = true;
				alert("You need to add items to your basket in order to perform a check out.");
				return;
			}

			clearTimeout(AuroraJS.Modules.basketTimer);
			var tmp = "";
			$("#productBasket .basketfunctions").html("<a id='CancelCheckOut' href='javascript:' onclick='AuroraJS.Modules.cancelOrder();'>Cancel</a>");
			
			if (AuroraJS.Templates.EnquiryFormTemplate) {
				tmp = $(AuroraJS.Templates.EnquiryFormTemplate.render({}));
			} else {
			tmp += ("<table id='PlaceOrder'><tr>");
			tmp += ("<td>First Name:</td><td><input valtype='required' type='text' id='firstName' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
			tmp += ("<td>Last Name:</td><td><input valtype='required' type='text' id='lastName' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
			tmp += ("<td>Company Name:</td><td><input  type='text' id='companyName' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
			tmp += ("<td>Email:</td><td><input valtype='required;regex:email' type='text' id='email' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
			tmp += ("<td>Contact No:</td><td><input type='text' id='contact' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
				tmp += ("<td>Comments:</td><td><textarea id='orderEnquiry' style='width:129px' rows=2></textarea></td>");
				tmp += ("</tr>");
				tmp += ("<tr>");
			tmp += ("<td><input id='sendOrder' value='Send' type='button' /></td>");
			tmp += ("</tr>");
			tmp += ("<tr>");
			tmp += ("<td colspan='2'><div id='QapTcha'/></td>");
			tmp += ("</tr>");
			tmp += ("</table>");
			}

			$(".BasketHeader, #productBasket .checkout").hide();
			$(".basketList").html(tmp);
			$(".basketList").css('max-height', '500px');
			$('#QapTcha').QapTcha(
			{
				buttonLock: "#sendOrder",
				buttonFunc: function () {
					$(".basketList").block({ message: null });
					var response = function (result) {
						$(".basketList").unblock();
						$(".basketList").css('max-height', '200px');
						$.cookie('productbasket', null);
						$(".basketCount").attr("count", 0);
						$("#productBasket").show();
						$(".basketList").html("Thank you for your patronage");
						setTimeout(function () { $("#productBasket").remove(); }, 3000);
					};

					if (!validateForm("PlaceOrder", "", "", true, "PlaceOrder input[type='text']")) {
						var personalData = new Object();
						$("#PlaceOrder input[type='text']").each(
							function (index) {
								personalData[$(this).attr("id")] = $(this).val();
							}
						);
						//set comments/enquiry data
						personalData["Enquiry"] = $("#orderEnquiry").val();

						Aurora.Services.AuroraWebService.PutOrderEnquiry($.cookie('productbasket').toString().split(','), personalData, response, onError);
					}
					else {
						$(".basketList").unblock();
					}

				}
			});
		}
		this.Modules.validateAddToBasket = function () {
			/// <summary>
			/// Checks if the selected product can be added to the basket i.e if colours are 
			/// available then system must prompt user to select a colour.
			/// </summary>
			if ($("#Colours0").is(":visible") && $("#ProductColours .colourSelected").attr("colourid") === undefined) {
				if ($(".productinfo").attr("colourwarn") === undefined) {
					$(".productinfo").attr("colourwarn", "yes");
					alert("In order to place this item in your basket you must select a colour first");
				}

				return false;
			}

			return true;
		}
		this.Modules.setBasketLink = function () {
			var basketLink = $('#basketLink');
			var basketLinkTemplate = $('#basketLinkTemplate');
			if ((basketLink.length > 0) && (basketLinkTemplate.length > 0)) {
				var template = $('#basketLinkTemplate').html();
				count = $(".basketCount").attr("count") || 0;

				template = template.replace('{number}', count);
				var identifiers = basketLink.attr('identifiers').split('|');
				if (Number(count) === 1) {
					template = template.replace('{items}', identifiers[0]);
				}
				else {
					template = template.replace('{items}', identifiers[1]);
				}
				basketLink.html(template);
				basketLink.unbind('click');
				basketLink.click(
				function () {
					if ($("#productBasket").length == 0) {
						AuroraJS.Modules.createBasket();
					}
					else {
						$("#productBasket").fadeIn(700);
					}

				}
			);
			}

		}
		this.Modules.getProductForBasket = function (colorID, sizeID) {
			/// <summary>Finds the product within the combination list and that matches the select product to size and colour and returns it to be added to the basker</summary>
			/// <returns type="Object" />
			for (var product = 0; product < AuroraJS.Modules.productFamilyDS.length; product++) {
				if (AuroraJS.Modules.productFamilyDS[product].colourSizeProductGallery) {
					if (AuroraJS.Modules.productFamilyDS[product].colourSizeProductGallery.SizeID == sizeID && AuroraJS.Modules.productFamilyDS[product].colourSizeProductGallery.ColourID == colorID) {
						return AuroraJS.Modules.productFamilyDS[product].products;
					}


				}
				else if (AuroraJS.Modules.productFamilyDS[product].products.SizeID == sizeID) {
					return AuroraJS.Modules.productFamilyDS[product].products;
				}
			}
		}
		//#endregion

		this.Modules.getAllProductCombinations = function (id) {
			/// <summary>Gets all possible combinations for a selected product</summary>
			/// <param name="id" type="Number">The id of the selected product.</param>

			var response = function (result) {
				AuroraJS.Modules.productFamilyDS = result.Data;
			};

			Aurora.Services.AuroraWebService.GetProductFamily(id, response, onError);
		};
		this.Modules.getAssociatedProducts = function (productID) {
			/// <summary>Gets all products that have been linked to the selected product.</summary>
			/// <param name="productID" type="Number"></param>
			var response = function (result) {
				//check if response is valid
				if (result.Result) {
					if (result.Data.length == 0) {
						$(".associatedHeader").hide();
						$(".associatedWrapper").hide();
					}
					else {
						$(".associatedHeader").show();
						$(".associatedWrapper").show();
					}
					//this method will find an image for the product
					var getImageForProduct = function (productID) {
						var image = findByInArray(result.Images, "products", productID);

						if (image.productImages) {
							AuroraJS.Modules.LoadImage(String.format("/ClientData/{0}/Uploads/sm_product_{1}.jpg", clientSiteID, image.productImages.gallery.ID), productID + "_imageAss", "/Styles/Images/no-image.jpg");
						}
						else {
							AuroraJS.Modules.LoadImage("/Styles/Images/no-image.jpg", productID + "_imageAss", "/Styles/Images/no-image.jpg");
						}
						return productID + "_imageAss";
					};

					//append each item to the associated container
					for (var item = 0; item < result.Data.length; item++) {
						//get images from image list
						if (fetchTemplate("ProductAssociated")) {
							$(String.format(fetchTemplate("ProductAssociated"), "Product", result.Data[item].products.CategoryID, result.Data[item].products.ID, result.Data[item].products.Name, getImageForProduct(result.Data[item].products.ID))).appendTo(".associated");
						}
					}
					if (AuroraJS.Bespoke.getAssociatedProducts) {
						AuroraJS.Bespoke.getAssociatedProducts(result.Data);
					}
				}
			};

			if ($(".associated").length > 0) {
				Aurora.Services.AuroraWebService.GetAssociatedProducts(productID, response, onError);
			}

		};
		//#endregion

	};

	//#region Members 
	AuroraJS.Modules.createMemberMenu = function () {
		//check if user has logged in
		var response = function (result) {
			$('.secureMenu').remove();
			var secureMenu = $(result.Data).show();
			var location = GetJSSession("Member_AttachMemberMenuTo") || "#menu";
			if (GetJSSession("Member_ApplySuperfishStyles") == true || GetJSSession("Member_ApplySuperfishStyles") == "true" || GetJSSession("Member_ApplySuperfishStyles") == undefined) {
				secureMenu.addClass("sf-menu sf-vertical");
			}

			$(location).append(secureMenu);
			if (AuroraJS.Bespoke.MemberMenu) {
				AuroraJS.Bespoke.MemberMenu(result);
			}
		};

		Aurora.Services.AuroraWebService.GetSecureMenu(response, onError);

	};

	//#endregion
} catch (e) {

}

function dateTimeUpdatedText(dateTime) {
	var date = new Date(dateTime.toString().substr(0, 4) + "/" + dateTime.toString().substr(5, 2) + "/" + dateTime.toString().substr(8, 2) + " " + dateTime.toString().substr(11, 8));
	var hoursAgo = 0;
	var returnVal = 0;
	//Get 1 day in milliseconds
	var one_day = 1000 * 60 * 60 * 24;
	var one_hour = 1000 * 60 * 60;
	var one_minute = 1000 * 60;

	// Convert both dates to milliseconds
	var date1_ms = date.getTime();
	var date2_ms = new Date().getTime();

	// Calculate the difference in milliseconds
	var difference_ms = date2_ms - date1_ms;

	// Convert back to days and return
	var hoursAgo = (difference_ms);

	if (hoursAgo >= one_day) {
		return Math.round((difference_ms / one_day), 0) + " day(s) ago";
	}
	else if (hoursAgo < one_day && hoursAgo >= one_hour) {
		return Number(difference_ms / one_hour).toFixed(0) + " hour(s) ago"
	}
	else if (hoursAgo >= one_minute && hoursAgo < one_hour) {
		return Number((difference_ms / one_minute), 0).toFixed(0) + " minute(s) ago";
	}
	else if (hoursAgo < one_minute) {
		return "less than a minute ago";
	}
}

//There is no bug here