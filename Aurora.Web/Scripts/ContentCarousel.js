function createControl(result) {
    var divElement = $("#Content").html();
    var ulElement = $("#Nav ul").html();

    $("#Content").html("");
    $("#Nav ul").html("");
    $(result.Data).each(function (index, domEle) {
        //content
        var divEle = divElement.toString().replace(/{{ID}}/gi, domEle["ID"]);
        divEle = divEle.toString().replace(/{{PageContent1}}/gi, domEle["PageContent1"]);
        divEle = divEle.toString().replace(/{{ShortTitle}}/gi, domEle["ShortTitle"]);
        divEle = divEle.toString().replace(/{{LongTitle}}/gi, domEle["LongTitle"]);
        divEle = divEle.toString().replace(/{{ID}}/gi, domEle["ID"]);

        $(divEle).appendTo("#Content");
        //nav
        var ulEle = ulElement.replace("{{ID}}", domEle["ID"]);
        ulEle = ulEle.replace("{{ShortTitle}}", domEle["ShortTitle"]);
        $(ulEle).appendTo("#Nav ul");

    });

    //declare fadeFunc
    function fadeItem() {
        $("#Content .pages").hide();
        $("#Content .select").fadeIn('fast', function () { $(".pages").css("display", "none"); });
    }

    //add only to parent elements
    $("#Content div").each(
        function () {
            if ($(this).parent().attr("id") == "Content") {
                $(this).addClass("pages").hide();
            }

        }
    );

    fadeItem();


    //hover over effect
    $("#Nav li").bind('mouseover', this, function () {
        var contentID = $(this).attr("id").replace("List", "Content");
        $(".select")[0].className = "pages";
        if ($(".select").attr("id") != contentID) {
            $("#" + contentID).addClass("select").removeClass("pages");
            fadeItem();
        }

    }).mouseout(function () { $(".pages").css("display", "none"); }); ;

    $("#Content div:first").show().addClass("select").remove("pages");
}