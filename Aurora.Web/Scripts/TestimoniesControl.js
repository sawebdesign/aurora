﻿/*globals AuroraJS $create $get Aurora onError confirm jQuery window Sys MSAjaxExtension */

AuroraJS.Modules.GetTestmonies = function (id) {
    if ($("#headerimg")[0]) {
        $("#headerimg")[0].src = "";
        $("#headerimg")[0].style.visiblity = "hidden";
        $("#headerimg")[0].style.position = "absolute";
        $("#headerimg")[0].style.display = "none";
    }

    $("#Contents").block({ message: null });
    AuroraJS.Modules.GetDetailTemplate("Testimonies");

    var getData = function (result) {
        if (AuroraJS.Modules.templateDetail === null) {
            AuroraJS.Modules.templateDetail = $create(Sys.UI.DataView, {}, {}, {}, $get("TestimoniesDetailData"));
        }
        AuroraJS.Modules.templateDetail.set_data(result.Data[0]);
        document.title = "Testmony By : " + result.Data.Title;
    };
    $("#Contents").unblock();
    Aurora.Services.AuroraWebService.GetFeedback(10, id, "", getData, onError);
};