﻿
AuroraJS.Modules.GetFullNewStory = function (newsId) {
    if ($("#headerimg")[0]) {
        $("#headerimg")[0].src = ""
        $("#headerimg")[0].style.visiblity = "hidden";
        $("#headerimg")[0].style.position = "absolute";
        $("#headerimg")[0].style.display = "none";
    }

    AuroraJS.Modules.GetDetailTemplate("News");

    $("#Contents").block({ message: null });
    var newsCall = function (result) {

        if (AuroraJS.Modules.templateDetail == null) {
            AuroraJS.Modules.templateDetail = $create(Sys.UI.DataView, {}, {}, {}, $get("NewsDetailData"));
        }
        AuroraJS.Modules.templateDetail.set_data(result.Data[0]);
        AuroraJS.Modules.DecodeHTML('inputcontent');

        document.title = "News : " + result.Data[0].Title;
        $("#Contents").unblock();
    }
    Aurora.Services.AuroraWebService.GetNewsItem(newsId, newsCall, AuroraJS.onError);

};