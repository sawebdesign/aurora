﻿/// <reference path="MicrosoftAjax.debug.js" />
/// <reference path="MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="/Scripts/jquery-1.5.2.js" />

function LoadGallery(galleryType) {
    /// <summary>Creates a gallery collection and allows the browsing of such galleries.</summary>
    function getcats(catResult) {
        
        //get categories
        var length = catResult.Data.length;

        if(galleryType == "Images")
        length = length - 1;
        for (var cat = 0; cat < length; cat++) {
            switch (galleryType) {
                case "Images":
                    GetImages(catResult, cat);
                    break;
                case "Audio":
                    GetAudio(catResult, cat);
                case "Video":
                    GetVideoGallery(catResult,cat);
                    break;
            }
            
        }//end get cats call

        
    }
    Aurora.Services.AuroraWebService.GetGalleryCat(galleryType, getcats, onError);
}

function GetImages(catResult, cat) {
    function ongetImageSuccess(result) {
        //get gallery container
        var group = document.getElementById("myGallerySet");
        //create gallery group element
        var gallery = document.createElement("div");
        var galleryTitle = document.createElement("h2");
        galleryTitle.innerHTML = catResult.Data[cat].Name;
        gallery.id = cat;
        gallery.className = "galleryElement";
        gallery.appendChild(galleryTitle);
        if (result.Data.length > 0) {
            //Create images,Title and description elements for gallery
            for (var image = 0; image < result.Data.length; image++) {
                var container = document.createElement("div");
                container.className = "imageElement";
                var title = document.createElement("h3")
                title.innerHTML = result.Data[image].Name;

                var description = document.createElement("p");
                description.innerHTML = result.Data[image].Description;

                var openImage = document.createElement("a");
                openImage.id = result.Data[image].ID;
                openImage.className = "open";
                openImage.title = "enlarge";
                openImage.href = "#";

                var largeImage = document.createElement("img");
                largeImage.src = String.format("/ClientData/{0}/Uploads/gallery_01_{1}.jpg", catResult.Data[cat].ClientSiteID, result.Data[image].ID);
                largeImage.className = "full";

                var smallImage = document.createElement("img");
                smallImage.src = String.format("/ClientData/{0}/Uploads/sm_gallery_01_{1}.jpg", catResult.Data[cat].ClientSiteID, result.Data[image].ID);
                smallImage.className = "thumbnail";

                //add elements to container
                container.appendChild(title);
                container.appendChild(description);
                container.appendChild(openImage);
                container.appendChild(largeImage);
                container.appendChild(smallImage);
                gallery.appendChild(container);
            } //end images for

        } //end images if
        if (result.Data.length != 0) {
            group.appendChild(gallery);
        }
        
       //specific to mootools component
        if (cat + 1 == catResult.Data.length - 1) {
            window.addEvent('domready', function () {
                document.myGallerySet = new gallerySet($('myGallerySet'), {
                    timed: false
                });

            });
        }
    }
    Aurora.Services.AuroraWebService.GetGalleries("Images", catResult.Data[cat].ID, ongetImageSuccess, onError);
}

function GetAudio(catResult, cat) {


    function getAudioGallery(result) {

        $("<div class='Category'><b>" + catResult.Data[cat].Name + "</b></div>").appendTo("#audio");
        var html = "";
        //create container
        html += "<div class='Category_body'>"
        for (var item = 0; item < result.Data.length; item++) {
            
            //create table layout
            html += "<table><tbody><tr>"
            //create cell 1
            html += "<td valign='top'><a href='/Download.aspx?id=" + result.Data[item].MediaName + "'><img alt='Download' src='/Styles/images/audio.png' border='0' /></a></td>";
            //create cell 2
            html += "<td valign='bottom' style='color:#0f3178;'><a title='Download' href='/Download.ashx?id=" + result.Data[item].MediaName + "'><b>" + result.Data[item].Name + "</b></a>:</td>";
            //create cell 3
            html += "<td valign='bottom'><i>" + result.Data[item].Description + "</i></td>";
            //Close table
            html += "</tr></tbody></table>";
           
        }
        //close Container
        html += "</div>";
        $(html).appendTo($("#audio"));

        if (cat + 1 > catResult.Data.length -1 ) {
            $(".Category_body").hide();
            //toggle the component with class Category_body  
            $(".Category").click(function () {
                $(this).next(".Category_body").toggle(100)
            });
        }
    }
    Aurora.Services.AuroraWebService.GetGalleries("Audio", catResult.Data[cat].ID, getAudioGallery, onError);
}


function GetVideoGallery(catResult, cat) {
    function ongetVideoSuccess(result) {
        //get gallery container
        var group = document.getElementById("myGallerySet");
        //create gallery group element
        var gallery = document.createElement("div");
        var galleryTitle = document.createElement("h2");
        galleryTitle.innerHTML = catResult.Data[cat].Name;
        gallery.id = cat;
        gallery.className = "galleryElement";
        gallery.appendChild(galleryTitle);
        if (result.Data.length > 0) {
            //Create images,Title and description elements for gallery
            for (var image = 0; image < result.Data.length; image++) {
                var container = document.createElement("div");
                container.className = "imageElement";
                var title = document.createElement("h3");
                title.innerHTML = result.Data[image].Name;

                var description = document.createElement("p");
                description.innerHTML = result.Data[image].Description;

                var openImage = document.createElement("a");
                openImage.id = result.Data[image].ID;
                openImage.className = "open";
                openImage.title = "Play";
                openImage.href = "javascript:this.parent.Load('" + result.Data[image].VideoURL + "');";
                

                var largeImage = document.createElement("img");
                largeImage.src = String.format("/ClientData/{0}/Uploads/gallery_01_{1}.jpg", catResult.Data[cat].ClientSiteID, result.Data[image].ID);
                largeImage.className = "full";
                

                var smallImage = document.createElement("img");
                smallImage.src = String.format("/ClientData/{0}/Uploads/sm_gallery_01_{1}.jpg", catResult.Data[cat].ClientSiteID, result.Data[image].ID);
                smallImage.className = "thumbnail";

                //add elements to container
                container.appendChild(title);
                container.appendChild(description);
                container.appendChild(openImage);
                container.appendChild(largeImage);
                container.appendChild(smallImage);
                gallery.appendChild(container);
            } //end images for

        } //end images if
        if (result.Data.length != 0) {
            group.appendChild(gallery);
        }

        //specific to mootools component
        if (cat + 1 == catResult.Data.length) {
            window.addEvent('domready', function () {
                document.myGallerySet = new gallerySet($('myGallerySet'), {
                    timed: false
                });

            });
        }
    }
    Aurora.Services.AuroraWebService.GetGalleries("Video", catResult.Data[cat].ID, ongetVideoSuccess, onError);
}

function onError(result) {
    alert(result.ErrorMessage);
}