/************************************************************************
*************************************************************************
@Name :       	QapTcha - jQuery Plugin
@Revison :    	1.0
@Date : 		26/01/2011
@Author:     	 Surrel Mickael (www.myjqueryplugins.com - www.msconcept.fr) 
@License :		 Open Source - MIT License : http://www.opensource.org/licenses/mit-license.php
 
**************************************************************************
*************************************************************************/
/// <reference path="jquery-1.5.2.js" />
/// <reference path="jquery-ui.js" />

jQuery.QapTcha = {
    build: function (options) {
        var defaults = {
            txtLock: 'Locked : Drag bar right to unlock.',
            txtUnlock: 'Unlocked : Form can be submitted.',
            buttonLock: 'input[name=\"SendMail\"]',
            buttonFunc: ""
        };

        if (this.length > 0)
            return jQuery(this).each(function (i) {
                /** Vars **/
                var 
				opts = $.extend(defaults, options),
				$this = $(this),
				form = $('form').has($this),
				Clr = jQuery('<div>', { 'class': 'clr' }),
				bgSlider = jQuery('<div>', { id: 'bgSlider' }),
				Slider = jQuery('<div>', { id: 'Slider' }),
				Icons = jQuery('<div>', { id: 'Icons' }),
				TxtStatus = jQuery('<div>', { id: 'TxtStatus', text: opts.txtLock }),
				inputQapTcha = jQuery('<input>', { name: 'iQapTcha', value: generatePass(), type: 'hidden' });

                /** Disabled submit button **/
                //form.find('input[type=\'submit\']').attr('disabled','disabled');
                $(opts.buttonLock).attr('disabled', 'disabled');

                /** Construct DOM **/
                bgSlider.appendTo($this);
                Icons.insertAfter(bgSlider);
                Clr.insertAfter(Icons);
                TxtStatus.insertAfter(Clr);
                inputQapTcha.appendTo($this);
                Slider.appendTo(bgSlider);
                $this.show();

                Slider.draggable({
                    containment: bgSlider,
                    axis: 'x',
                    stop: function (event, ui) {
                        if (ui.position.left > 123) {
                            Slider.draggable('disable').css('cursor', 'default');
                            inputQapTcha.val("");
                            TxtStatus.css({ color: '#307F1F' }).text(opts.txtUnlock);
                            Icons.css('background-position', '-16px 0');
                            //form.find('input[type=\'submit\']').removeAttr('disabled');
                            $(opts.buttonLock).removeAttr('disabled');
                            ///Show a fornm
                            if ($(opts.buttonLock).attr("show")) {
                                $($(opts.buttonLock).attr("show")).css("display", "block");
                                $("#QapTcha").fadeOut(500);
                            }
                            else if (opts.buttonLock == 'input[name="SendMail"]') {
                                $(opts.buttonLock).bind("click", function () {
                                    AuroraJS.Modules.SendMail(this);
                                });
                            }
                            else if (opts.buttonFunc != "") {
                                $(opts.buttonLock).bind("click", function () {
                                    opts.buttonFunc();
                                });
                            }
                        }
                    }
                });

                function generatePass() {
                    var chars = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
                    var pass = '';
                    for (i = 0; i < 10; i++) {
                        var wpos = Math.round(Math.random() * chars.length);
                        pass += chars.substring(wpos, wpos + 1);
                    }
                    return pass;
                }

            });
    }
};      jQuery.fn.QapTcha = jQuery.QapTcha.build;