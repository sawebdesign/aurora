﻿/*
	Membership Showcase
	Aurora Plugin
	Author: Garth
	Creation Date: 2013/5/21

	N.B. The self referencing plugin calls, i.e. $this.MemberShowCase('Method'), are to preserve scope.
	I would have prefered to use $.proxy but am limited by version 1.5.2 not supporting argument passing.
*/

(function ($, window, document, undefined) {

	var methods = {
		init: function (options) {
			return this.each(function () {
				var $this = $(this);
				if (!$this.attr('init')) {
					$this.attr('init', true);
					var htmlOptions = {
						numberToGet: parseInt($this.attr('numbertoget'), 10),
						randomise: $this.attr('randomise'),
						categoryFilter: $this.attr('categoryfilter'),
						rotationSpeed: $this.attr('rotationSpeed'),
						thumbnailSize: $this.attr('thumbnailSize'),
						stretchOldthumb: $this.attr('stretchOldthumb')

					};
					$this.MemberShowcase('options', $.extend(htmlOptions, options));
					$this.MemberShowcase('getMembers');
				}
			});
		},
		options: function (options) {
			var $this = $(this);
			var settings = $this.data('MemberShowcaseOptions') || $.fn.MemberShowcase.options;
			var fullOptions = $.extend({}, settings, options);
			$this.data('MemberShowcaseOptions', fullOptions);
			return fullOptions;
		},
		getMembers: function () {
			return this.each(function () {
				var $this = $(this);
				var options = $this.MemberShowcase('options');
				Aurora.Services.AuroraWebService.GetMemberShowcase(options.numberToGet, options.categoryFilter, options.randomise, events.getMembersSuccess, events.getFailed, $this);
			});
		},
		renderMembers: function (data) {
			return this.each(function () {
				var $this = $(this);
				var $container = $this.find('.showcaseContainer');
				if ($container.length === 0) {
					$container = $($('#membershowcasetmpl').render({}));
					$this.append($container);
				}
				var convertedData = utils._convertCustomXMLtoObject(data);
				var $members;
				if (convertedData && convertedData.length === 0) {
					$members = $('#membershowcasenomemberstmpl').render({});
				} else {
					$members = $($('#membershowcaseitemtmpl').render(convertedData));
					$members.css({
						position: "absolute",
						top : 0
					});
					
				}
				
				$container.append($members);
				$this.MemberShowcase('normaliseImages');
				//$this.html($container);
				//var memberHeights = $members.map(function() {
				//	return $(this).height();
				//});
				//var maxHeight = Math.max.apply(null, memberHeights);
				//$this.height(maxHeight);
			});
		},
		rotateImages: function () {
			return this.each(function () {
				var $this = $(this),
					$items = $this.find('.memberShowcaseItem'),
					options = $this.MemberShowcase('options');
				if ($items.length === 0) {
					clearInterval(options.interval);
				}
				$items.eq(options.index).fadeOut(500);
				options.index++
				if (options.index > $items.length - 1) {
					options.index = 0;
				}
				$items.eq(options.index).fadeIn(500);
				$this.MemberShowcase('options', options);
			});
		},
		normaliseImages: function () {
			var $this = $(this);
			var $images = $this.find('.showcaseImage');
			var options = $this.MemberShowcase('options');
			$images.each(function (i, img) {
				if (img.naturalHeight && img.naturalWidth) {
					var ratio = img.naturalHeight / img.naturalWidth;
					if (ratio >= 1) {
						$(img).css({
							width: "auto",
							height: options.thumbnailSize + "px"
						});
					} else if (ratio < 1) {
						$(img).css({
							width: options.thumbnailSize + "px",
							height: "auto"
						});
					}
				} else if (options.stretchOldthumb) {
					$(img).css({
						width: options.thumbnailSize + "px",
						height: options.thumbnailSize + "px"
					});
				} else {
					$(img).css({
						width: options.thumbnailSize + "px"
					});
				}

			});
			

		},
		startRotation: function (data) {
			return this.each(function () {
				var $this = $(this),
					options = $this.MemberShowcase('options');
				$this.MemberShowcase('renderMembers', data);
				$items = $this.find('.memberShowcaseItem');
				$items.stop(true, true).hide();
				$items.eq(options.index).fadeIn(500);
				options.interval = setInterval(function () {
					$this.MemberShowcase('rotateImages');
				}, options.rotationSpeed);
				$this.MemberShowcase('options', options);

			});
		}

	};

	var utils = {
		_convertCustomXMLtoObject: function (data) {
			for (var d = 0; d < data.length; d++) {
				if (data[d].CustomXML) {
					var $fields = $($.parseXML(data[d].CustomXML)).find('field');
					var xmlData = data[d].xmlData = {};
					for (var f = 0; f < $fields.length; f++) {
						var $field = $($fields[f]);
						xmlData[$field.attr('fieldname').replace(/ /gi, '')] = $field.attr('value');
					}
				}

			}
			return data;
		}
	};

	var events = {
		getMembersSuccess: function (result, $context) {
			$context.MemberShowcase('startRotation', result.Data);
		},
		getFailed: function (ex) {
			console.log(ex);
		}
	};

	$.fn.MemberShowcase = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.MemberShowcase');
		}
	};

	$.fn.MemberShowcase.options = {
		numberToGet : 5,
		randomise: false,
		categoryFilter: "",
		rotationSpeed: 5000,
		index: 0,
		thumbnailSize: 200,
		stretchOldthumb: true
	};

	AuroraJS.plugins.MemberShowcase = function () {
		$('.MemberShowcase').MemberShowcase();
	};

})(jQuery, window, document);





