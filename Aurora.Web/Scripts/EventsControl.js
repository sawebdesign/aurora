﻿
AuroraJS.Modules.GetFullEvent = function (eventId) {
    if ($("#headerimg")[0]) {
        $("#headerimg")[0].src = ""
        $("#headerimg")[0].style.visiblity = "hidden";
        $("#headerimg")[0].style.position = "absolute";
        $("#headerimg")[0].style.display = "none";
    }
    $("#Contents").block({ message: null });
    AuroraJS.Modules.GetDetailTemplate("Event");
    var newsCall = function (result) {
        if (AuroraJS.Modules.templateDetail == null) {
            AuroraJS.Modules.templateDetail = $create(Sys.UI.DataView, {}, {}, {}, $get("EventDetailData"));
        }
        formatDateTime(result.Data.StartDate);
        AuroraJS.Modules.templateDetail.set_data(result.Data);
        AuroraJS.Modules.DecodeHTML('inputcontent');
        document.title = "Events : " + result.Data.Title;

        $("#Contents").unblock();
    }

    Aurora.Services.AuroraWebService.GetEvent(eventId, newsCall, AuroraJS.onError);
};