﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Text;

namespace Aurora {
    public partial class CompletedPayment : Common.BasePage {
		protected void Page_Load(object sender, EventArgs e) {
			Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();

			//variables
			long transactionID;
			long.TryParse(Server.UrlDecode(Request["trns"]), out transactionID);
			if (transactionID == 0) {
				long.TryParse(Server.UrlDecode(Request["li_0_product_id"]), out transactionID);
			}
            if (transactionID == 0) {
                long.TryParse(Server.UrlDecode(Request["p2"]), out transactionID);
            }
			bool successful = false;
			String statusMessage = "";
			//Create a html container for result messaging
			ContentPlaceHolder content = (ContentPlaceHolder)Master.FindControl("MainContent");
			Literal html = new Literal();
			bool overideMessage = false;
			var transAction = entities.TransactionLog.Where(trns => trns.ID == transactionID).SingleOrDefault();

			if (transAction != null) {
				// Process Transaction
				PaymentSchema schema = entities.PaymentSchema.Where(w => w.ID == transAction.PaymentSchemaID).FirstOrDefault();
				if (transAction.Completed != true) {
					if (schema != null) {
						switch (schema.ID) {
							case 10000: // EFT

								//we need to handle membership renewals here because otherwise a members role would not be replaced
								if(transAction.PaymentType ==  "Renewal") {

										var currentMember = (from mr in entities.MemberRole
																join t in entities.TransactionLog on mr.ID equals t.MemberCategoryID
																join r in entities.Role on mr.RoleID equals r.ID
																where t.ID == transactionID
																select new {
																	MemberRole = mr,
																	Category = r
																}).FirstOrDefault();

										MemberRole renewedCategory = new MemberRole {
											ClientSiteID = SessionManager.ClientSiteID,
											MemberID = SessionManager.MemberData.ID,
											RoleID = currentMember.MemberRole.RoleID,
											IsActivated = false,
											InsertedOn = DateTime.Now,
											CommencedOn = null,
											ExpiresOn = currentMember.MemberRole.ExpiresOn.Value.AddDays((double)currentMember.Category.RoleDuration),
											CostAtPurchase = currentMember.Category.Cost,
											Replacing = currentMember.MemberRole.ID
										};
										
										//set the current one as replaced
										Utils.markCategoryAsReplaced(currentMember.MemberRole.ID);

										entities.AddToMemberRole(renewedCategory);
										entities.SaveChanges();
								}

								break;
							case 10001: // PayGate
								long result;
								long.TryParse(Request.Form["RESULT_CODE"], out result);

								var resultInfo = (from errorCode in entities.ErrorCodes
												  where errorCode.ID == result && errorCode.PaymentSchemaID == 10001
												  select errorCode).SingleOrDefault();
								//transaction failure
								if (resultInfo == null && Request["eft"] != "1") {
									html.Text += String.Format("<p>{0}</p>", "Your transaction failed for an unknown reason");
									content.Controls.Add(html);
								}
								if (Request["TRANSACTION_STATUS"] == "1") {
									successful = true;
									transAction.Completed = true;
									entities.SaveChanges();
								} else if (resultInfo != null) {
									statusMessage = resultInfo.Reason;
								}

								break;
							case 10002: // PayFast
								string paymentResult = Request.Form["payment_status"];
								if (paymentResult == "COMPLETE") {
									successful = true;
									transAction.Completed = true;
									entities.SaveChanges();
								} else {
									statusMessage = "Your transaction failed.";
								}
								break;
							case 10003: // 2Checkout
								string compResult = Request["credit_card_processed"];
								if (compResult == "Y") {
									successful = true;
									transAction.Completed = true;
									entities.SaveChanges();
								} else {
									statusMessage = "Your transaction failed.";
								}
								break;
							case 10004: // Paypal
								//Paypal should only ever load this page if transaction was successful.
								successful = true;
								transAction.Completed = true;
								entities.SaveChanges();
								break;
                            case 10005: // VCS
                                statusMessage = Request["p3"];
                                if (statusMessage.ToUpper().Contains("APPROVED")) {
                                    successful = true;
                                    transAction.Completed = true;
                                    entities.SaveChanges();
                                }
                                break;
						}
						if (successful == true) {
							//check payment type
							switch (transAction.PaymentType) {

								#region Donations
								case "Donation":
									var donationData = Utils.GetFieldsFromCustomXML("Donations");
									if (donationData.Count > 0) {
										if (!String.IsNullOrEmpty(donationData["Donation_TextComplete"])) {
											statusMessage = donationData["Donation_TextComplete"];
										}

										if (!String.IsNullOrEmpty(donationData["Donation_CMSZoneAfter"])) {
											long pageID;
											long.TryParse(donationData["Donation_CMSZoneAfter"], out pageID);

											var pageText = (from pages in entities.PageContent
															where pages.ID == pageID
															select pages).SingleOrDefault();

											if (pageText != null) {
												html.Text = string.Empty;
												statusMessage = pageText.PageContent1;
												overideMessage = true;
											}
										}

										//send comms to site owner
										var messageData = (from siteInfo in entities.ClientSite
														   where siteInfo.ClientSiteID == SessionManager.ClientSiteID
														   select new { siteInfo.Email1, siteInfo.CompanyName, TransactionID = transAction.ID }).SingleOrDefault();

										var xmlData = new List<dynamic> { messageData };

										Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysNewDonation.GetEnumDescriptionAttribute(), string.Empty, null, "New Donation Made to your Organisation", Utils.GenerateCommunicationXML(xmlData));

										//send comms to donor
										XmlDocument tranactionXML = new XmlDocument();
										tranactionXML.LoadXml(transAction.DataXML);
										var donorEmailAddress = (from xmldata in tranactionXML.GetElementsByTagName("field").Cast<XmlNode>()
																 select xmldata.Attributes["value"].Value).FirstOrDefault();

										if (donorEmailAddress == null) {
											donorEmailAddress = CacheManager.SiteData.Email1;
										}

										var donorData = (from siteInfo in entities.ClientSite
														 where siteInfo.ClientSiteID == SessionManager.ClientSiteID
														 select new { email = donorEmailAddress, siteInfo.CompanyName, TransactionID = transAction.ID }).SingleOrDefault();

										xmlData = new List<dynamic> { donorData };

										Utils.SendToMessageCentre(
											Utils.MessageCentreEvents.NewDonation.GetEnumDescriptionAttribute(), null, null,
											"Thank you for your support", Utils.GenerateCommunicationXML(xmlData),
											CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
									}
									break;
								#endregion

								#region Registration
								case "Registration":
									//activate new member
									var activeMember = (from mr in entities.MemberRole
														join t in entities.TransactionLog on mr.ID equals t.MemberCategoryID
														join r in entities.Role on mr.RoleID equals r.ID
														where t.ID == transactionID
														select new {
															MemberRole = mr,
															Category = r
														}).FirstOrDefault();

									activeMember.MemberRole.PaidOn = DateTime.Now;

									// If at a later stage we want to have manual activations on purchases, wrap the below code in an if statement.
									// ** Start Wrap **
									activeMember.MemberRole.CommencedOn = DateTime.Now;
									activeMember.MemberRole.IsActivated = true;
									if (activeMember.Category.RoleDuration > 0) {
										activeMember.MemberRole.ExpiresOn = DateTime.Now.AddDays((double)activeMember.Category.RoleDuration);
									}
									// ** End Wrap **
									entities.SaveChanges();
									Utils.SendMemberCategoryMessageEmail(activeMember.MemberRole.ID, 0, false);
									
									statusMessage = "Your payment was successful. Thank you for your patronage.";
									overideMessage = true;
									break;
								#endregion

								#region Member Renewals
								case "Renewal":

									var currentMember = (from mr in entities.MemberRole
														 join t in entities.TransactionLog on mr.ID equals t.MemberCategoryID
														 join r in entities.Role on mr.RoleID equals r.ID
														 where t.ID == transactionID
														 select new {
															 MemberRole = mr,
															 Category = r
														 }).FirstOrDefault();

									MemberRole renewedCategory = new MemberRole {
										ClientSiteID = SessionManager.ClientSiteID,
										MemberID = SessionManager.MemberData.ID,
										RoleID = currentMember.MemberRole.RoleID,
										IsActivated = currentMember.MemberRole.IsActivated,
										InsertedOn = DateTime.Now,
										CommencedOn = currentMember.MemberRole.CommencedOn,
										ExpiresOn = currentMember.MemberRole.ExpiresOn.Value.AddDays((double)currentMember.Category.RoleDuration),
										CostAtPurchase = currentMember.Category.Cost,
										Replacing = currentMember.MemberRole.ID
									};

									currentMember.MemberRole.Replaced = true;
									entities.AddToMemberRole(renewedCategory);
									entities.SaveChanges();
									statusMessage = "Your payment was successful. Thank you for your patronage.";
									overideMessage = true;
									break;
								#endregion

								#region Change Category
								case "Change Category":
									var changeMember = (from mr in entities.MemberRole
														join t in entities.TransactionLog on mr.ID equals t.MemberCategoryID
														join r in entities.Role on mr.RoleID equals r.ID
														where t.ID == transactionID
														select new {
															MemberRole = mr,
															Category = r
														}).FirstOrDefault();
									Utils.markCategoryAsReplaced(changeMember.MemberRole.Replacing.Value);

									changeMember.MemberRole.PaidOn = DateTime.Now;

									// If at a later stage we want to have manual activations on purchases, wrap the below code in an if statement.
									// ** Start Wrap **
									changeMember.MemberRole.CommencedOn = DateTime.Now;
									changeMember.MemberRole.IsActivated = true;
									if (changeMember.Category.RoleDuration > 0) {
										changeMember.MemberRole.ExpiresOn = DateTime.Now.AddDays((double)changeMember.Category.RoleDuration);
									}
									// ** End Wrap **
									entities.SaveChanges();
									Utils.SendMemberCategoryMessageEmail(changeMember.MemberRole.ID, 0, false);

									statusMessage = "Your payment was successful. Thank you for your patronage.";
									overideMessage = true;
									break;
								#endregion

								#region Product Purchases

								case "Product Purchase":

                                    //send email to client & remove stock if you change this then change it on Manage.AuroraWS.asmx.cs.SetTransActionCompleted
                                    StringBuilder additionalEmail = new StringBuilder();
                                    
									if (transAction.DataXML != null) {
                                        additionalEmail.Append("</br></br>Below is a list of products that you purchased. If any are digital products then you can click on the link and download them.</br></br>");

										XmlDocument products = new XmlDocument();
										products.LoadXml(transAction.DataXML);
										XmlNode rootElement = products.GetElementsByTagName("Fields").Item(0);
										List<dynamic> receiptItems = new List<dynamic>();

                                        additionalEmail.Append("<table style=\"width: 100%; border-collapse: collapse;\"><tr><th style=\"border: 1px solid #333; text-align: left; padding: 8px 8px;\">Product</th><th style=\"border: 1px solid #333; text-align: left; padding: 8px 8px;\">Quantity</th></tr>");

										//update product totals in stock
										foreach (XmlElement product in rootElement) {
											long productID = long.Parse(product.GetAttribute("id"));
											int quantity = int.Parse(product.GetAttribute("quantity"));
											decimal basketPrice = decimal.Parse(product.GetAttribute("price"));

											var productEntity = (from prodTable in entities.Product
																 where prodTable.ID == productID
																 select prodTable
																).SingleOrDefault();

											if (productEntity != null && productEntity.Total > quantity) {
												productEntity.Total = productEntity.Total - quantity;
												productEntity.SetAllModified(entities);
												entities.SaveChanges();
											}

                                            if (String.IsNullOrEmpty(productEntity.DownloadURL)) {
                                                additionalEmail.AppendFormat("<tr><td style=\"border: 1px solid #333; padding: 0px 8px;\">{0}</td><td style=\"border: 1px solid #333; padding: 0px 8px;\">{1}</td></tr>", productEntity.Name, quantity);
                                            } else {
                                                additionalEmail.AppendFormat("<tr><td style=\"border: 1px solid #333; padding: 0px 8px;\">{0} (<a href='{2}'>Download</a>)</td><td style=\"border: 1px solid #333; padding: 0px 8px;\">{1}</td></tr>", productEntity.Name, quantity, productEntity.DownloadURL);
                                            }
											receiptItems.Add(new { productEntity.Name, quantity, productEntity.Price, basketPrice });
										}
                                        additionalEmail.Append("</table>");

										//mark voucher as used
										if (products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"] != null) {

											if (products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"].Value != null) {
												string voucherCode =
												 products.GetElementsByTagName("Fields").Item(0).Attributes["vouchercode"].Value;
												if (!String.IsNullOrEmpty(voucherCode)) {
													var voucher =
														(from voucherItems in entities.VoucherItem
														 where voucherItems.Code == voucherCode
														 select voucherItems).SingleOrDefault();

													voucher.Redeemed = true;

													voucher.SetAllModified(entities);
													entities.SaveChanges();
												}
											}

										}
										Page.ClientScript.RegisterStartupScript(GetType(), "cleanBasket", "$.cookie('productbasket', '');", true);
										//send comms to Site Owner
										var xmlData = new List<dynamic> { CacheManager.SiteData };
										Utils.SendToMessageCentre(Utils.MessageCentreEvents.SysProductPurchaseCompleted.GetEnumDescriptionAttribute(), string.Empty, null, "Product Purchase from your website", Utils.GenerateCommunicationXML(xmlData));

										//Send Comms to Member
										xmlData = new List<dynamic> { SessionManager.MemberData };
                                        Utils.SendToMessageCentre(Utils.MessageCentreEvents.ProductPurchaseCompleted.GetEnumDescriptionAttribute(), additionalEmail.ToString(), null, "Purchase from: " + CacheManager.SiteData.CompanyName, Utils.GenerateCommunicationXML(xmlData), CacheManager.SiteData.Email1, SessionManager.ClientSiteID.Value);
									}
									break;
								#endregion
							}
						}

					} else {
						html.Text = "<h3>Payment Failed</h3>";
						html.Text += String.Format("<p>{0}</p>", "Site Configuration error, please contact your site administrator");
					}
				} else {
					successful = true;
				}

				if (schema.isEFT) {
					var bankingDetails = (from bank in entities.PaymentSchemaClientSite
										  where
											  bank.PaymentSchemaID == schema.ID
											  && bank.ClientSiteID == SessionManager.ClientSiteID
										  select bank.FormHTML).SingleOrDefault();
					html.Text = "<h3>Please make a payment to the following bank account</h3>";
					html.Text += String.Format("<p>Your reference code is {0}<br/> {1}</p>", Server.UrlDecode(Request["trns"]), bankingDetails);
					switch (transAction.PaymentType) {
						case "Registration":
						case "Renewal":
						case "Change Category":
							var category = (from memcat in entities.MemberRole
											join cat in entities.Role on memcat.RoleID equals cat.ID
											where memcat.ID == transAction.MemberCategoryID
											select cat).FirstOrDefault();
							string messageXml = string.Empty;
							messageXml += "<xmldata><users>";
							messageXml +=
								String.Format("<user email=\"{0}\" memfirstname=\"{1}\" memlastname=\"{2}\" mememail=\"{3}\" transactiontype=\"{4}\" catname=\"{5}\" amount=\"{6}\" transactionnumber=\"{7}\"></user>",
												Utils.EncodeXML(CacheManager.SiteData.Email1),
												Utils.EncodeXML(SessionManager.MemberData.FirstName),
												Utils.EncodeXML(SessionManager.MemberData.LastName),
												Utils.EncodeXML(SessionManager.MemberData.Email),
												Utils.EncodeXML(transAction.PaymentType),
												Utils.EncodeXML(category.Name),
												Utils.EncodeXML(category.Cost.Value.ToString("0.00")),
												Utils.EncodeXML(transAction.ID.ToString()));
							messageXml += "</users></xmldata>";

							Utils.SendToMessageCentre(
								Utils.MessageCentreEvents.SysEFTRequest.GetEnumDescriptionAttribute(),
								string.Empty, string.Empty, Utils.MessageCentreEvents.SysEFTRequest.GetEnumDescriptionAttribute(), messageXml);

							break;
					}
				} else if (successful == true) {
					html.Text = "<h2>Success</h2>";
					if (overideMessage) {
						html.Text += String.Format("<p>{0}</p>", statusMessage);
					} else {
						html.Text += "<p>Your payment was successful. Thank you for your patronage.</p>";
					}
				} else {
					html.Text = "<h3>Payment Failed</h3>";
					html.Text += String.Format("<p>{0}</p>", statusMessage);
				}

			} else {
				html.Text = "<h3>Payment Failed</h3>";
				html.Text += String.Format("<p>{0}</p>", "Transaction does not exist");
			}

			//Append Message Container to Page
			content.Controls.Add(html);

		}
        
    }
}