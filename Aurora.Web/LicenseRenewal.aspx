﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LicenseRenewal.aspx.cs" Inherits="Aurora.LicenseRenewal" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <link href="styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <script src="managelicenses.aspx.js" type="text/javascript" />
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
    <table width="100%">
        <tr>
            <td>
                <h3>
                    License Selection</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Please choose the license you would like to purchase.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="radioBtnGroupLicense" class="sys-template">
                    <input sys:value="{{membersettings.ID}}" name="License" sys:membername="{{membersettings.Name}}"
                        sys:price="{{membersettings.Price == 0 ? 0 :membersettings.Price}}" type="radio" />{{membersettings.Name}}
                    - {{membersettings.Price == 0 ? "Free!" :"R "+ membersettings.Price }}<br />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>
                    Payment Method</h3>
            </td>
        </tr>
        <tr>
            <td>
                Please choose your preferred payment method
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="radioBtnGroupPayment" class="sys-template">
                    <input sys:value="{{ID}}" sys:pay="{{Name}}" onclick="$(this).attr('checked','checked');"
                        name="Payment" type="radio" /><label>{{Name}}</label><br />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
