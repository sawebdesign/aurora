﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" /> 
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="/Scripts/modules.js" />
/// <reference path="/Scripts/SubMenu.js" />

/*globals AuroraJS Aurora onError confirm jQuery window Sys MSAjaxExtension */

$(function () {
    $("#MainBasket").hide();
    if (AuroraJS.Bespoke.Login) {
        AuroraJS.Bespoke.Login();
    }
});

function lostPassword() {
    var response = function (result) {
        if (!result.Data) {
            $("<div>The email provided does not exist within our records.</div>").dialog({ modal: true,
                resizable: false,
                title: "Error",
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
        }
        else {
            $(String.format("<div>An email has been sent to {0}.</div>", result.Data)).dialog({ modal: true,
                resizable: false,
                title: "Email Sent",
                buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
        }
    };

    if ($("#forgotten").val() === "") {
        $("<div>Please provide us with the email address you registered with.</div>").dialog({ modal: true,
            resizable: false,
            title: "Error",
            buttons: { "Okay": function () { $(this).dialog("close"); } }
        });
    }
    else {
        Aurora.Services.AuroraWebService.LostPassword($("#forgotten").val(), response, onError);
    }
}
