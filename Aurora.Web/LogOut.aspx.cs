﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora {
    public partial class LogOut : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionManager.VistorSecureSessionID = Guid.Empty;
            SessionManager.CurrentTransactions = new List<Custom.Data.TransactionLog>();
            SessionManager.MemberData = new Custom.Data.Member();
            CacheManager.SecureMenu = string.Empty;
            Response.Redirect("http://"+Request.Url.Host);
        }
    }
}