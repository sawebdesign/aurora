<%@ Page Language="C#" %>
<%@ Import Namespace="Aurora.Custom" %>
<%@ Import Namespace="Aurora.Custom.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.ComponentModel" %>
<script runat=server>
	protected void Page_Load(object sender, EventArgs e) {
		using (AuroraEntities context = new AuroraEntities()) {
			var sites = context.ClientSite.OrderByDescending(o => o.ClientSiteID);
			sitenamesRPT.DataSource = sites;
			sitenamesRPT.DataBind();
		}
	}
</script>
<html>
	<head>
		<title>Aurora Sites</title>
		<style>
			ul, li {
				list-style: none;
			}
			a {
				text-decoration: none;
				transition: color 0.5s;
				transition: font-size 0.2s;
				color: #000;
				font-family: Tahoma;
			}

			a:hover {
				color: #f00;
				font-size: 30px;
			}
		</style>
	</head>
<body>
    <form id="form1" runat="server">
		<ul>
		<asp:Repeater runat="server" ID="sitenamesRPT">
			<ItemTemplate>
				<li><a href="http://auroraweb/default.aspx?overide=<%# Eval("ClientSiteID") %>"><%# Eval("CompanyName") %> - <%# Eval("ClientSiteID") %></a></li>
			</ItemTemplate>
		</asp:Repeater>
			</ul>
    </form>
</body>
</html>