﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" /> 
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="/Scripts/modules.js" />
/// <reference path="Scripts/Aurora.Custom.js" />

var usersDS = null;
var departmentsDS = [];
var licenseDS = null; 
$(function () {
    AuroraJS.Modules.Members = true;
    getDepartments();
    
});
function LoadUsers() {
    var response = function (result) {
        if (result.Result) {
            if (result.Data.length > 0) {
                if (!usersDS) {
                    usersDS = $create(Sys.UI.DataView, {}, {}, {}, $get("userData"));
                }

                usersDS.set_data(result.Data);
                getLicenses();
            }
        }
    };

    Aurora.Services.AuroraWebService.GetMembersForCompany(response, onError);
}
function SaveUser() {
    $("#Messages").html("");
    var response = function (result) {
        if (result.Result) {
            $("<label>User has been Saved</label>").dialog({ modal: true,
                resizable: false,
                title: "Save Completed"
            });
            $("#contentAddEdit").dialog("close");
            LoadUsers();
        }
        else if (!result.Data) {
            $("<label>User could not be saved, as you have reached the limit of your purchased license.Please purchase more licenses to add more users</label>").dialog({ modal: true,
                resizable: false,
                title: "Save Failed"
            });
        }
        else {
            $("<label>User could not be Saved,please try again.If the problem persists please contact support.</label>").dialog({ modal: true,
                resizable: false,
                title: "Save Failed"
            });
        }
    };



    if (validateForm("contentAddEdit", "Please Fill in the marked fields", "Messages")) {
        return;
    }
    var newEntity = {};
    var entity = findByInArray(usersDS._data, "ID", $("#ID").val());
    var newMember = false;
    if (!entity) {
        newMember = true;
        entity = usersDS._data[0];
    }

    for (var property in entity) {
        var $control = $("#" + property);
        if ($control.length > 0) {
            if (!newMember)
                entity[property] = $control.val();

            newEntity[property] = $control.val();
        }
    }
    if (newMember) {
        newEntity.ID = 0;
        newEntity.ClientSiteID = clientSiteID;
    }

    newEntity.InsertedOn = new Date(entity.InsertedOn || new Date());
    newEntity.ExpiresOn = new Date(entity.ExpiresOn || new Date());
    newEntity.isActive = $("#isActive").is(":checked");
    Aurora.Services.AuroraWebService.SaveMember(newEntity, $("#CurrentLicenses").val(),false, response, onError);
}
function clearForm() {
    $("#InsertedOn").val(new Date());
    $("#InsertedBy").val("0");
    $("#DeletedOn").val("");
    $("#DeletedBy").val("");
    $("#ClientSiteID").val(clientSiteID);
    $("#ExpiresOn").val("");
    $("#OrganisationID").val("0");
    $("#DepartmentID").val("0");
    $("#ID").val("0");
    $("#FirstName").val("");
    $("#LastName").val("");
    $("#Email").val("");
    $("#Mobile").val("");
    $("#WorkTel").val("");
    $("#PrivateTel").val("");
    $("#Organisation").val("");
    $("#Occupation").val("");
    $("#Password").val("");
    $("#ConfirmPassword").val("");
}
function getLicenseForMember(id) {
    var response = function (result, memberID) { 
        if (result.Result) {
            $("#CurrentLicenses option").each(
                function (indx, Elem) {
                    if (Number(Elem.value) == result.Data[0].LicenseID) {
                        Elem.setAttribute("selected", "selected");
                    }
                    else {
                        Elem.removeAttribute("selected");
                    }
                }
            );
            Edit(memberID);
        }

    };

    Aurora.Services.AuroraWebService.GetLicenseForMember(id, response, onError, id);
}
function Edit(id) {
    var item = findByInArray(usersDS._data, "ID", id);
    if (item) {

        for (var property in item) {
            var $control = $("#" + property);
            if ($control.length > 0) {
                $control.val(item[property]);
            }
        }

        $("#contentAddEdit").dialog({
            modal: true,
            resizable: false,
            title: "Editing : " + item.FirstName + " " + item.LastName,
            width: '800px',
            buttons: { "Save": SaveUser, "Cancel": function () { $("#contentAddEdit").dialog("close"); $("#Messages").html(""); } }

        });
    }
}
function getLicenses() {
    var response = function (result) {
        if (result.Data.length > 0) {
            var ds = $create(Sys.UI.DataView, {}, {}, {}, $get("CurrentLicenses"));
            ds.set_data(result.Data);
            licenseDS = result.Data;
        }
    };

    Aurora.Services.AuroraWebService.GetClientLicense(response, onError);
}

function getDepartments() {
    var response = function (result) {
        if (result.Data.length > 0) {
            departmentsDS = result.Data;

        }
        LoadUsers();
    };

    Aurora.Services.AuroraWebService.GetDepartments(response, onError);

}

function renewLicense(id) {
    var item = findByInArray(usersDS._data, "ID", id);
    var currentLicense;
    for (var member = 0; member < licenseDS.length; member++) {
        if (licenseDS[member].memberSettings.ID == Number($("#CurrentLicenses option[selected='selected']").val())) {
            currentLicense = licenseDS[member].memberSettings;
            break;
        }
        else {
            $('<div>This license has not been assigned to this user, and therefore cannot be renewed</div>').dialog({
                modal: true,
                resizable: false,
                title: "Invalid User selection",
                width: '800px',
                buttons: { "Okay": function () { $("#Renew").dialog("close"); $("#Messages").html(""); } }

            }) ;
            return;
        }
    }

    $("#renewName").html(item.FirstName + " " + item.LastName);
    $("#renewPrice").html(currentLicense.Price);
    $("#Renew").dialog({
        modal: true,
        resizable: false,
        title: "Do you want to renew " + item.FirstName + " " + item.LastName + "'s License?",
        width: '800px',
        buttons: { "Process": processLicense, "Cancel": function () { $("#Renew").dialog("close"); $("#Messages").html(""); } }

    });


}

function processLicense() {
    var response = function (result) {
        if (result.Result) {
            document.location = "http://" + document.domain + "/processpayment.aspx";
        }
    };
    var transaction = new AuroraEntities.TransactionLog();
    transaction.ID = 0;
    transaction.ClientSiteID = Number(clientSiteID);
    transaction.PaymentSchemaID = Number($("#CurrentLicenses option[selected='selected']").attr("paymentschema"));
    transaction.MemberID = 0;
    transaction.PaymentType = "Renewal";
    transaction.TotalAmount = Number($("#renewPrice").html());
    transaction.Completed = false;
    transaction.InsertedOn = new Date();
    transaction.InsertedBy = 0;
    transaction.DeletedOn = null;
    transaction.DeletedBy = null;
    transaction.DataXML = null;
    transaction.CustomXML = String.format("<XmlData><license id='{0}'></license</XmlData>", $("#CurrentLicenses option[selected='selected']").val());
   
    Aurora.Services.AuroraWebService.CreateTransacation(transaction.PaymentSchemaID,transaction.PaymentType,transaction.TotalAmount,0,transaction.CustomXML,response,onError);
}

function buyMoreLicenses() {
    var loadSuccess = function (x, h, r) {
        if (h === "success") {
            $("#BuyMore").dialog({
                modal: true,
                resizable: false,
                title: "Do you want to buy another License?",
                width: '800px',
                buttons: { "Process": processBuyLicense, "Cancel": function () { $("#BuyMore").dialog("close"); $("#Messages").html(""); } }

            });

            getAllLicenses();
        }
    };

    $("#BuyMore").load("AddLicense.aspx?x="+new Date().getMilliseconds(), null, loadSuccess);
}

function getAllLicenses() {
    var response = function (result) {
        if (result.Data.length > 0) {
            dsLicenses = $create(Sys.UI.DataView, {}, {}, {}, $get("radioBtnGroupLicense"));
            dsLicenses.set_data(result.Data);
            getPaymentMethods(dsLicenses._data[0].ID);


        }
    };
    Aurora.Services.AuroraWebService.GetLicenses(response, onError);
}
var dsPayment = null;
function getPaymentMethods(id) {
    var response = function (result) {

        if (!dsPayment) {
            dsPayment = $create(Sys.UI.DataView, {}, {}, {}, $get("radioBtnGroupPayment"));
        }
        dsPayment.set_data(result.Data);


    };

    Aurora.Services.AuroraWebService.GetPaymentMethodsPerLicense(id, response, onError);


}

function processBuyLicense() {
    var response = function (result) {
        if (result.Result) {
            document.location = "http://" + document.domain + "/processpayment.aspx";
        }
    };
    var transaction = new AuroraEntities.TransactionLog();
    transaction.ID = 0;
    transaction.ClientSiteID = Number(clientSiteID);
    transaction.PaymentSchemaID = Number($("#radioBtnGroupPayment input:checked").val());
    transaction.MemberID = 0;
    transaction.PaymentType = "NewLicense";
    transaction.TotalAmount = Number($("#radioBtnGroupLicense input:checked").attr("price"));
    transaction.Completed = false;
    transaction.InsertedOn = new Date();
    transaction.InsertedBy = 0;
    transaction.DeletedOn = null;
    transaction.DeletedBy = null;
    transaction.DataXML = null;
    transaction.CustomXML = String.format("<XmlData><license id='{0}'></license</XmlData>", $("#CurrentLicenses option[selected='selected']").val());

    Aurora.Services.AuroraWebService.CreateTransacation(transaction.PaymentSchemaID, transaction.PaymentType, transaction.TotalAmount, 0, transaction.CustomXML, response, onError);
}