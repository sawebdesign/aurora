﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eezyAds;
using System.Web.SessionState;

namespace Aurora {
    /// <summary>
    /// Summary description for banner
    /// </summary>
    public class banner : IHttpHandler, IRequiresSessionState {

        public void ProcessRequest(HttpContext context) {
            eezyAds.eezyAds ads = new eezyAds.eezyAds();
            string ReturnVal = "<script src=\"/Scripts/google-banners.js\" type=\"text/javascript\"></script>";
            string Style = Aurora.Custom.Utils.ConvertDBNull<string>(context.Request.QueryString["Style"], "padding:2px");
            int BannerCount = Aurora.Custom.Utils.ConvertDBNull<int>(context.Request.QueryString["Count"], 0);
            string qString = HttpUtility.UrlDecode(context.Request.QueryString.ToString().ToUpper()).Replace("{CLIENTSITEID}", Custom.Utils.ConvertDBNull<string>(Custom.SessionManager.ClientSiteID, ""));
            if (BannerCount > 0) {
                for (int I = 1; I <= BannerCount; I++) {
                    ReturnVal += "<div style='" + Style + "'>" + ads.eezyads_GetAdASP(qString) + "</div>";
                }
            } else {
                ReturnVal = ads.eezyads_GetAdASP(qString);
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(ReturnVal);
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}