﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.SessionState;

namespace Aurora {
    /// <summary>
    /// Summary description for GalleryAudioPlayList
    /// </summary>
    public class GalleryAudioPlayList : IHttpHandler, IRequiresSessionState {

        public void ProcessRequest(HttpContext context) {
            Custom.Data.AuroraEntities service = new Custom.Data.AuroraEntities();

            long siteid = Aurora.Custom.Utils.ConvertDBNull<long>(Aurora.Custom.SessionManager.ClientSiteID, 0);
            int categoryId = Aurora.Custom.Utils.ConvertDBNull<int>(context.Request.QueryString["categoryId"], 0);
            var result = (from gallery in service.Gallery
                          where gallery.CategoryID == categoryId
                          select gallery).DefaultIfEmpty().ToList();

            context.Response.ContentType = "text/plain";
            StringBuilder output = new StringBuilder();
            output.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            output.Append("<playlist version=\"0\" xmlns=\"http://xspf.org/ns/0\">");
            output.Append("<trackList>");

            if (result.Count > 0) {
                foreach (var item in result) {
                    output.Append("<track>");
                    output.Append("<location>/ClientData/" + siteid + "/uploads/gallery_01_" + item.ID + ".mp3</location>");
                    output.Append("<image>/ClientData/" + siteid + "/uploads/sm_gallery_01_" + item.ID + ".jpg</image>");
                    output.Append("<annotation>" + item.Name + "</annotation>");
                    output.Append("</track>");
                }
            }

            output.Append("</playlist>");
            context.Response.Write(output.ToString());
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}