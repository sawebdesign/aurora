﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="/Scripts/modules.js" />
/// <reference path="/Scripts/SubMenu.js" />

AuroraJS.plugins.Donations = function () {
	/// <summary>loads form for donations capture</summary>
	var template = AuroraJS.Modules.GetTemplate("Donations");
    $(template).insertAfter("#MainContent_DonateContent");
    $("#captcha").QapTcha({
        buttonFunc: sendDonation,
        buttonLock: "#sendDonation"
    });
    $(".recurringInputs").hide();

	//set the recurring payment events
    $("#isRecurring").click(function (e) {
    	if ($(this).attr("checked") === true) {
    		$(".recurringInputs").show();
    	} else {
    		$(".recurringInputs").hide();
    		console.log();
    	}
    });

    $("#donationAmount").ForceNumericOnly();
};

var sendDonation = function () {
    var amount = Number($("#donationAmount").val()).toFixed(2);

    //#region Validation 
    if (validateForm(undefined, "", "", false, undefined, "#DonationForm")) {
        return false;
    }
    if (amount <= 0) {
        showDefaultDialog("Please enter an amount greater than zero.", "Error");
        return;
    }

    //Min Donate value
    if (GetJSSession("Donation_MinValue") && Number(GetJSSession("Donation_MinValue")) > amount) {
        showDefaultDialog(String.format("Please enter an amount greater than R {0}.", GetJSSession("Donation_MinValue")), "Error");
        return;
    }
    //#endregion

    var response = function (result) {
        if (result.Result && result.Count > 0) {
            document.location = "ProcessPayment.aspx";
        }
    };

    if (GetJSSession("Donation_PaymentSchema")) {
    	showDefaultDialog("Please wait while process your request", "Processing...");
    	var extraXML = "";
    	$('#DonationForm input.extraXML').each(function () {
    		extraXML += String.format("<field fieldname=\"{0}\" title=\"{1}\" value=\"{2}\" />", $(this).attr('id'), $(this).attr('id'), $(this).val());
    	});

    	//check for recurring payment then set xml field for it
    	if ($("#isRecurring").attr("checked") === true) {
    		var $obj = $("#isRecurring");
    		extraXML += String.format("<field fieldname=\"{0}\" title=\"{1}\" value=\"{2}\" />", $obj.attr('id'), $obj.attr('id'), $obj.val());

    		$('.recurringInputs input, select, textarea').each(function () {
    		    extraXML += String.format("<field fieldname=\"{0}\" title=\"{1}\" value=\"{2}\" />", $(this).attr('id'), $(this).attr('id'), $(this).val());
    		});

    	}

        var xmlData = String.format("<XmlData><Fields><field fieldname=\"donationEmailAddress\" title=\"donationEmailAddress\" value=\"{0}\"/>{1}</Fields></XmlData>", $("#emailAddress").val(),extraXML)
		
        Aurora.Services.AuroraWebService.CreateDonationTransaction(GetJSSession("Donation_PaymentSchema"), "Donation", amount, xmlData, response, AuroraJS.onError);
    }
    else {
        showDefaultDialog("Payment Gateway has not been setup", "Error");
    }
};