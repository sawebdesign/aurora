﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.IO.Compression;
using System.Web.UI;
using Aurora.Custom;

namespace Aurora {
    public class Global : System.Web.HttpApplication {

        void Application_Start(object sender, EventArgs e) {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e) {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e) {
            // Get the exception object.
            Exception exc = Server.GetLastError();
            string userName = Utils.GetAppPoolUserName();
            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpUnhandledException) || exc.GetType() == typeof(HttpRuntime)) {
                // The Complete Error Handling Example generates
                // some errors using URLs with "NoCatch" in them;
                // ignore these here to simulate what would happen
                // if a global.asax handler were not implemented.
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength")) {
                    return;
                }
                Utils.SendExceptionMail(exc, "Global.asax Unhandled Exception");


                //Redirect HTTP errors to HttpError page
                Response.Redirect("~/Errors/_errors/503.html");
            }
        }

        void Session_Start(object sender, EventArgs e) {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e) {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        void Application_PreRequestHandlerExecute(object sender, EventArgs e) {
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState) {
                if (SessionManager.ClientSiteID == 0) {
                    string domain = Request.ServerVariables["SERVER_NAME"];
                    //Try to find clients for that domain
                    using (Aurora.Custom.Data.AuroraEntities auroraEntities = new Custom.Data.AuroraEntities()) {
                        var siteData = (from entites in auroraEntities.Domain
                                        join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals clientSite.ClientSiteID
                                        where entites.DomainName == domain
                                        select clientSite).SingleOrDefault();

                        if (Request["overide"] != null && Request["overide"] != "5455") {

                            int ClientSiteID = int.Parse(Request["overide"]);
                            SessionManager.ImpersonateID = ClientSiteID;
                            siteData = (from entites in auroraEntities.Domain
                                        join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals
                                            clientSite.ClientSiteID
                                        where entites.ClientSiteID == ClientSiteID
                                        select clientSite).First();
                        }
                            //kills current override
                        else if (Request["overide"] == "5455") {
                            SessionManager.ImpersonateID = 0;
                            SessionManager.ClientSiteID = 0;
                            siteData = (from entites in auroraEntities.Domain
                                        join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals clientSite.ClientSiteID
                                        where entites.DomainName == domain
                                        select clientSite).SingleOrDefault();
                        } else if (SessionManager.ImpersonateID > 0 && SessionManager.ImpersonateID != 5455) {
                            siteData = (from entites in auroraEntities.Domain
                                        join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals
                                            clientSite.ClientSiteID
                                        where entites.ClientSiteID == SessionManager.ImpersonateID
                                        select clientSite).First();
                        }


                        //if nothing return 404
						if (siteData == null && Request.RawUrl != "/errors/_errors/404.html") {
							Response.Redirect("~/Errors/_errors/404.html", true);
							return;
						} else if (siteData != null) {
							//Load Client Details and Template
							Custom.SessionManager.ClientSiteID = siteData.ClientSiteID;
						}

                        

                    };
                }

                if (CacheManager.SiteData == null) {
                    using (Aurora.Custom.Data.AuroraEntities auroraEntities = new Custom.Data.AuroraEntities()) {
                        var siteData = (from clientSite in auroraEntities.ClientSite
                                        where clientSite.ClientSiteID == SessionManager.ClientSiteID.Value
                                        select clientSite).SingleOrDefault();

                        CacheManager.SiteData = siteData;
                    }
                }
            }
        }

        void Application_BeginRequest(object sender, EventArgs e) {

            try {

                //check for 301
                Redir(Request);

                //Removes any special chars within the Query String
                if (Request.QueryString["title"] != null) {
                    if (Regex.IsMatch(Request.QueryString["title"], @"[\\\~!@#$%^*()_+{}:|""?`;',./[\]]+")) {
                        Response.Redirect(Utils.RemoveSpecialCharacters(Request.QueryString["title"]));
                    }
                }
                HttpApplication app = (HttpApplication)sender;
                string acceptEncoding = app.Request.Headers["Accept-Encoding"];
                acceptEncoding = acceptEncoding.ToLower();
                Stream prevUncompressedStream = app.Response.Filter;
                long size = prevUncompressedStream.CanRead ? Utils.ConvertDBNull(prevUncompressedStream.Length, 0) : 0;


				//if (app.Request.ContentType != "application/json; charset=utf-8" && app.Request.ContentType != "text/css") {
				//	return;
				//}

                if ((acceptEncoding.Contains("gzip,") || acceptEncoding.Contains("gzip"))) {
                    app.Response.Filter = new GZipStream(prevUncompressedStream, CompressionMode.Compress);
                    app.Response.AppendHeader("Content-Encoding", "gzip");
                } else {
                    if (acceptEncoding.Contains("deflate")) {
                        app.Response.Filter = new DeflateStream(prevUncompressedStream, CompressionMode.Compress);
                        app.Response.AppendHeader("Content-Encoding", "deflate");
                    }
                }
            } catch (Exception ex) {
                //this is an attempt to find out which file is causing an error on Aurora with the Run time exception
                HttpApplication app = (HttpApplication)sender;
                string message = string.Empty;

                if (app == null) {
                    message = "application is null";
                } else {
                    message += "App Headers: " + app.Request.Headers + "<br/>";
                    message += "App Request: " + app.Request.FilePath + "<br/>";
                    message += "App Response: " + app.Request.QueryString + "<br/>";
                }

                Utils.SendErrorMail(ConfigurationManager.AppSettings["ErrorMailListFrom"], "Aurora Global ASAX", message, "Global Error");
            }

        }

        /// <summary>
        /// 301 redirects this is were it happens
        /// </summary>
        /// <param name="req"></param>
        private void Redir(HttpRequest req) {

            long ClientSiteID = 0;
            ClientSiteID = CacheManager.Domains.Where(r => r.DomainName == req.Url.Host).Select(s => s.ClientSiteID).FirstOrDefault();
            if (ClientSiteID != 0) {
                //look for static 301 redirects setup by the client
                string redir = CacheManager.Redirect301.Where(r => r.OldURL.ToLower() == req.Url.Scheme.ToLower() + "://" + req.Url.Host.ToLower() + req.RawUrl.ToLower() && r.ClientSiteID == ClientSiteID).Select(s => s.NewURL).FirstOrDefault();
                if (!String.IsNullOrEmpty(redir)) {
                    if (!string.IsNullOrEmpty(redir)) {
                        Response.Clear();
                        Response.StatusCode = 301;
                        Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", redir);
                        Response.End();
                    }
                }

                //check for a root domain and if exists then 301 redirect to the root
                Aurora.Custom.Data.Domain root = CacheManager.Domains.Where(w => w.ClientSiteID == ClientSiteID && w.isRoot == true).FirstOrDefault();
                if (root != null) {
                    if (root.DomainName != req.Url.Host) {
                        Response.Clear();
                        Response.StatusCode = 301;
                        Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", req.Url.Scheme.ToLower() + "://" + root.DomainName + req.RawUrl.ToLower());
                        Response.End();
                    }
                }

            }
        
        }

    }
}
