﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aurora {
    /// <summary>
    /// Summary description for eezyadsredir
    /// </summary>
    public class eezyadsredir : IHttpHandler {

        public void ProcessRequest(HttpContext context) {
            eezyAds.eezyAds ads = new eezyAds.eezyAds();
            string sRedir = null;

            sRedir = ads.eezyads_ClickAd(context.Request.QueryString["id"], context.Request.QueryString["way"]);
            if (string.IsNullOrEmpty(sRedir)) {
                sRedir = context.Request.ServerVariables["HTTP_REFERER"];
            }
            context.Response.Redirect(sRedir);
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}