﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" ClientIDMode="Predictable" Inherits="Aurora.Login" %>

<%@ Import Namespace="Aurora.Custom" %>
<%@ Register src="usercontrols/sitememberarea.ascx" TagName="SiteMemberArea" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<link rel="Stylesheet" href="styles/global.css" type="text/css" />
    <div class="cmszone" runat="server" id="cmsZoneTop"></div>
    <div id="DoLogin" runat="server">
        <%if (SessionManager.VistorSecureSessionID == Guid.Empty) {	
        %>
        <table width="100%" cellpadding="0" cellspacing="0" class="memberLogin">
            <tbody>
                <tr>
                    <td>
                        Email Address:
                    </td>
                    <td id="email">
                        <input type="text" runat="server" name="UserName" id="UserName" style="width:150px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Password:
                    </td>
                    <td id="passwordTD">
                        <input type="password" runat="server" name="Password" id="Password" style="width:150px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="buttoncontainer">
                        <input type="submit" style="display: none;" id="btnLogin" value="Login" />
                        <asp:Button runat="server" ID="testLogin" Text="Login" OnClick="ExecuteLogin" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" class="errormessage">
                        <asp:Label runat="server" ID="ErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="forgotten">
                            <tr>
                                <td>
                                    <div class="forgottenPass">Forgotten Your Password?</div>
                                    Please enter your email address to request your password.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="float: left;">
                                        Email:&nbsp;<input type="text" id="forgotten" /></div>
                                    &nbsp;<div style="float: left;">
                                        <input type="button" value="Request" onclick="lostPassword();" /></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="ntReg">
                    <td colspan="2">
                        <div class="notRegistered">Not Registered?</div>
                    </td>
                </tr>
                <tr class="ntReg2">
                    <td>
                        <input type="button" id="register" value="Register" onclick="document.location='Register.aspx'" />
                    </td>
                </tr>
            </tbody>
        </table>
        <%} %>
        <uc1:SiteMemberArea ID="SiteMemberArea1" runat="server" />
    </div>
    <div class="cmszone" runat="server" id="cmsZoneBottom">
    </div>
    <div id="MainBasket">
    </div>
    <div id="inputcontent">
    </div>
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="scripts/aurora.custom.js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/plugins/boltaccordion.js" type="text/javascript"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <link href="/styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jsrender.js?" type="text/javascript" ></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <script src="login.aspx.js" type="text/javascript"></script>
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
       
    </script>
</asp:Content>
