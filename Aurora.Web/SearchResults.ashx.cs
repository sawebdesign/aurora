﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Xml;

namespace Aurora
{
    /// <summary>
    /// Summary description for SearchResults
    /// </summary>
    public class SearchResults : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
            StringBuilder results = new StringBuilder();
            string keywords = context.Request["s"];
            //results template 
            const string headerTemplate = "<h2>{0}</h2>";
            const string resultsCountTemplate = "<p class=\"results-count\"><strong>{0}</strong> match</p>";
            const string resultsTemplate = "<li><a href=\"{0}\">{1}</a></li>";
            const string allSearchResults = "<div id='results'></div><span class='allSearchResults'>{0}</span>";
            Dictionary<string, string> settings = Utils.GetFieldsFromCustomXML("Search");

            List<int> modules = settings["Search_Modules"].Split(',').Select(t => int.Parse(t)).ToList();
            bool searchSecured = false;
			int iPageSize = 20;//int.Parse(context.Request["pageSize"]);
            int iPageNumber = 5;


            if (modules != null)
            {
                //Pages
                if (modules.Contains(12))
                {
                    var pages = (from pagecontent in entities.PageContent
                                 where (pagecontent.ShortTitle.Contains(keywords)
                                        || pagecontent.LongTitle.Contains(keywords)
										|| pagecontent.Description.Contains(keywords)
                                        || pagecontent.PageContent1.Contains(keywords))
                                       && pagecontent.ClientSiteID == SessionManager.ClientSiteID
                                       && pagecontent.isArchived == false
                                       && pagecontent.DeletedBy == null
                                       && pagecontent.DeletedOn == null
                                       && pagecontent.IsSecured == searchSecured
                                 orderby pagecontent.ShortTitle
                                 select pagecontent);

                    if (pages.Count() > 0)
                    {
                        pages.Paginate(iPageNumber, iPageSize);
                        //href=" + context.Request["page"] + 
                        //results.Append(string.Format(allSearchResults, "<a id='linkToSearchPage' class='searchHeader' >click here for all results</a>"));
                        results.Append(string.Format(headerTemplate, "Pages"));
                        results.Append(string.Format(resultsCountTemplate, pages.Count()));
						results.Append("<ul class=\"small-files-list icon-img\">");
                        foreach (PageContent pageContent in pages.Take(iPageSize))
                        {
                            results.Append(string.Format(resultsTemplate, "#" + context.Server.UrlEncode(string.Format("/Page/{0}/{1}", pageContent.ID, pageContent.Description.Replace(" ", "-"))).Replace("+", "-"), pageContent.ShortTitle));
                        }
						results.Append("</ul>");
						results.Append("<hr>");
                    }
					//must we search the news module
                    if (modules.Contains(3))
                    {
                        var news = (from newsContent in entities.News
                                    where (newsContent.Title.Contains(keywords)
                                           || newsContent.Preview.Contains(keywords)
                                           || newsContent.Details.Contains(keywords))
                                          && newsContent.ClientSiteID == SessionManager.ClientSiteID
                                          && newsContent.DeletedBy == null
                                          && newsContent.DeletedOn == null
                                    //&& newsContent.EndDate >= DateTime.Now
                                    select newsContent);

                        if (news.Count() > 0)
                        {
                            results.Append(string.Format(headerTemplate, "News"));
                            results.Append(string.Format(resultsCountTemplate, news.Count()));
							results.Append("<ul class=\"small-files-list icon-img\">");
                            foreach (News newsItems in news.Take(iPageSize))
                            {
                                results.Append(string.Format(resultsTemplate, "#" + context.Server.UrlEncode(string.Format("/News/{0}/{1}", newsItems.ID, newsItems.Title).Replace("+", "-")), newsItems.Title));
                            }
							results.Append("</ul>");
							results.Append("<hr>");
                        }
                    }
					//must we search the products module
                    if (modules.Contains(158))
                    {
                        //Product Search
                        var products = from product in entities.Product
                                       join categories in entities.Category on product.CategoryID equals categories.ID
                                       where (product.Name.Contains(keywords)
                                             || product.Description.Contains(keywords)
                                             || product.Short_Description.Contains(keywords))
                                             && product.DeletedOn == null
                                             && product.DeletedOn == null
                                             && categories.ClientSiteID == SessionManager.ClientSiteID
                                             && categories.Description == "Product"
                                       select new { product, categories };

                        if (products.Count() > 0)
                        {
                            results.Append(string.Format(headerTemplate, "Products"));
                            results.Append(string.Format(resultsCountTemplate, products.Count()));
							results.Append("<ul class=\"small-files-list icon-img\">");
                            foreach (var productItem in products.Take(iPageSize))
                            {
                                results.Append(string.Format(resultsTemplate, "#" + context.Server.UrlEncode(string.Format("/Products/{0}/{1}", productItem.categories.ID, productItem.categories.Name)).Replace("+", "="), productItem.categories.Name));
                            }
							results.Append("</ul>");
                        }


                    }

                    if (modules.Contains(162))
                    {
                        //Custom Modules

                    }
                }

                if (results.Length == 0)
                {
                    results.Append(string.Format(headerTemplate, "No Items Found"));
                    results.Append(string.Format(resultsCountTemplate, "0"));
                    results.Append(string.Format(resultsTemplate, "#", "Please try another search"));
                }
                context.Response.Write(results.ToString());

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}