﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="Aurora.UserControls.Menu" %>
<%@ Import Namespace="Aurora.Custom" %>
<div id="menu">
    <asp:Literal ID="mainMenu" runat="server"></asp:Literal>
    <asp:Literal ID="secureMenu" runat="server"></asp:Literal>

</div>
