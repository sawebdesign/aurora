﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.UserControls
{
    public partial class SiteMemberArea : System.Web.UI.UserControl{
        public Custom.Data.AuroraEntities entity = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

           
        }

        public void LoadProfile()
        {
            if (SessionManager.VistorSecureSessionID != Guid.Empty)
            {
               
                if (SessionManager.MemberData == null)
                {
                    Response.Redirect("~/Login.aspx");
                }
                else
                {
					String returnURL = Request["ReturnUrl"];
					
					String redirect = (from memberRoles in entity.MemberRole
									   join category in entity.Role on memberRoles.RoleID equals category.ID
									   where memberRoles.MemberID == SessionManager.MemberData.ID
									   && memberRoles.LapsedOn == null
									   && memberRoles.IsActivated == true
									   && memberRoles.ClientSiteID == SessionManager.ClientSiteID
									   && memberRoles.Replaced == null
									   orderby category.OrderIndex descending
									   select category.LandingPageURL).FirstOrDefault();
					if (returnURL != null && (returnURL.ToLower().Contains("checkout.aspx") || returnURL.ToLower().Contains("basket.aspx"))) {
						redirect = returnURL;
					}
					if (String.IsNullOrEmpty(redirect)) {
						redirect = "/Memberscontent.aspx";
					}
					Response.Redirect(redirect);
					//Dictionary<string, string> memberarea = Utils.GetFieldsFromCustomXML("Members");

					//if (!string.IsNullOrEmpty(memberarea["Member_Redirect"])) {
					//	Response.Redirect(memberarea["Member_Redirect"]);
					//}
                }
               
            }
        }
    }
}