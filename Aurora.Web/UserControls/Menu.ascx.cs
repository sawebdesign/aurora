﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using Aurora.Services;
using System.Text;

namespace Aurora.UserControls {
    public partial class Menu : System.Web.UI.UserControl {
		public bool newMenu = false;
		public bool isResponsive = false;
        public StringBuilder menu = new StringBuilder();
        readonly AuroraEntities entities = new AuroraEntities();
        private XDocument productConfig;
        private int OverrideCurrent = 0;

        protected void Page_Load(object sender, EventArgs e) {
            //Check if product Menu available for Site menu
            var module = from moduleConfig in entities.ClientModule
                         join features in entities.Module on moduleConfig.FeaturesModuleID equals features.ID
                         where moduleConfig.ClientSiteID == SessionManager.ClientSiteID
                         && features.Name == "Products"
                         select moduleConfig.ConfigXML;

            if (module.Count() > 0 && module.First() != null) {
                productConfig = XDocument.Parse(module.First());
            }



            var includeProductMenu = productConfig != null ? from prd in productConfig.Descendants("field")
                                                             select new {
                                                                 Name = prd.Attribute("fieldname"),
                                                                 Value = prd.Attribute("value")
                                                             }

                                     : null;

            Utils.CheckSqlDBConnection();
            using (SqlCommand cmd = new SqlCommand()) {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[Features].[proc_PageContent_GetPagesWithChildStatus]";
                cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                var pageList = from tb in ds.Tables[0].AsEnumerable()
                               select new {
                                   ID = tb.Field<int?>("ID"),
                                   Category = tb.Field<long?>("CategoryID"),
                                   ParentId = tb.Field<long?>("ParentID"),
                                   ClientSiteID = tb.Field<long?>("ClientSiteID"),
                                   ShortTitle = tb.Field<string>("ShortTitle"),
                                   LongTitle = tb.Field<string>("LongTitle"),
                                   Description = tb.Field<string>("Description"),
                                   PageContent = tb.Field<string>("PageContent"),
                                   PageIndex = tb.Field<int?>("PageIndex"),
                                   isArchived = tb.Field<bool?>("isArchived"),
                                   isDefault = tb.Field<bool?>("isDefault"),
                                   MetaKeywords = tb.Field<string>("MetaKeywords"),
                                   MetaDescription = tb.Field<string>("MetaDescription"),
                                   CustomXML = tb.Field<string>("CustomXML"),
                                   InsertedOn = tb.Field<DateTime?>("InsertedOn"),
                                   InsertedBy = tb.Field<long?>("InsertedBy"),
                                   DeletedOn = tb.Field<DateTime?>("DeletedOn"),
                                   DeletedBy = tb.Field<long?>("DeletedBy"),
                                   HasChildren = tb.Field<int?>("HasChildren"),
                                   MenuClass = tb.Field<string>("MenuClass"),
                                   RedirectURL = tb.Field<string>("RedirectURL"),
                                   PageTitle = tb.Field<string>("PageTitle")
                               };

				if (isResponsive) {
					menu.Append("<div class=\"responsenavbtn\"></div>");
				}
				if (newMenu) {
					menu.Append("<ul class=\"sf-menu sf-vertical\">");
				}
                foreach (var page in pageList) {
					string href = "/page/" + page.Description.Replace(" ", "-").Replace("?", "").ToLower();

                    string childMenu = string.Empty;
                    string productMenu = string.Empty;
                    string descendants = string.Empty;
                    string pageToAttach = includeProductMenu != null ? includeProductMenu.Where(t => t.Name.Value == "Products_MenuItemID").First().Value.Value : "";

                    childMenu = GetChildren(page.ID.Value, string.Empty, page.ID.Value);

                    if (includeProductMenu != null && includeProductMenu.First().Value.Value == "1" && !String.IsNullOrEmpty(pageToAttach) && int.Parse(pageToAttach) == page.ID.Value) {
                        productMenu = GetProductsMenu();

                    }

                    if (childMenu.Length > 10) {
                        childMenu = childMenu.Trim().Substring(4, childMenu.Length - 9);
                    }
                    descendants = string.Format("<ul style=\"white-space:nowrap;\">{0}{1}</ul>", productMenu, childMenu);

                    if (descendants == "<ul style=\"white-space:nowrap;\"></ul>") {
                        descendants = string.Empty;
					} else if (isResponsive) {
						descendants = "<div class=\"responsenavsub\"></div>" + descendants;
					}

					if (!newMenu) {
						menu.Append("<ul class=\"sf-menu sf-vertical\">");
					}
                   

                    string redirectAttribute = string.Empty;
                    string IDAttribute = string.Empty;
                    Dictionary<string, string> redirect = GetRedirects(page.RedirectURL);
                    if (redirect["IsAuroraRedirect"] == "True" && redirect["Valid"] == "True") {
                        redirectAttribute = " redirect='" + page.RedirectURL + "' ";
                        IDAttribute = redirect["IDAttribute"];
                    }

                    //store page title for use later
                    string pageTitle = !string.IsNullOrEmpty(page.PageTitle) ? page.PageTitle : page.ShortTitle;
					pageTitle = string.IsNullOrEmpty(pageTitle)? "":pageTitle;

					if (Utils.ConvertDBNull(Request["ID"], "") == page.Description.Replace(" ", "-").Replace("?", "").ToLower() && Utils.ConvertDBNull(Request["Type"], "Page").ToUpper() == "PAGE") {
                        if (page.isDefault.Value) {
							menu.Append("<li class='homeitem current " + page.MenuClass + "'><a dataid='" + page.ID + "'  title='" + Server.HtmlEncode(page.LongTitle) + "' href='/'>" + Server.HtmlEncode(page.ShortTitle) + "</a>" + descendants + "</li>");
                            //menu.Append("<li class='homeitem current " + page.MenuClass + "'><a dataid='" + page.ID + "' onclick='AuroraJS.Modules.GetContentForModule(\"Page\"," + page.ID + ",\"" + Server.HtmlEncode(page.LongTitle.Replace(" ", "-")) + "\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" + href + "'>" + Server.HtmlEncode(page.ShortTitle) + "</a>" + descendants + "</li>");
                        }
                        else {
                            menu.Append("<li class='current " + page.MenuClass + "'><a dataid='" + page.ID +
                                        "' onclick='AuroraJS.Modules.GetContentForModule(\"page\"," + page.ID + ",\"" +
                                        Server.HtmlEncode(page.Description.Replace(" ", "-")).ToLower() +
										"\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" +
                                        href + "' " + IDAttribute + redirectAttribute + ">" + Server.HtmlEncode(page.ShortTitle) + "</a>" + descendants +
                                        "</li>");
                        }
                    }
                    else {
                        if (page.isDefault.Value) {
                            menu.Append("<li class='" + (Request["ID"] == null ? "homeitem current" : "homeitem") + " " + page.MenuClass + "'><a  dataid='" + page.ID +
										"'  title='" + Server.HtmlEncode(pageTitle) + "' href='/' " + IDAttribute + redirectAttribute + ">" + page.ShortTitle + "</a>" + descendants + "</li>");
                            /*
                             menu.Append("<li class='" + (Request["ID"] == null ? "homeitem current" : "homeitem") + " " + page.MenuClass + "'><a  dataid='" + page.ID +
                                        "' onclick='AuroraJS.Modules.GetContentForModule(\"Page\"," + page.ID + ",\"" +
                                        Server.HtmlEncode(page.LongTitle.Replace(" ", "-")) +
                                        "\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" +
                                        href + "' " + IDAttribute + redirectAttribute + ">" + page.ShortTitle + "</a>" + descendants + "</li>");
                             */
                        }
                        else {
                            menu.Append("<li class=' " + page.MenuClass + "'><a dataid='" + page.ID +
                                       "' onclick='AuroraJS.Modules.GetContentForModule(\"page\"," + page.ID + ",\"" +
                                       Server.HtmlEncode(page.Description.Replace(" ", "-")).ToLower() +
									   "\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" +
                                       href + "' " + IDAttribute + redirectAttribute + ">" + page.ShortTitle + "</a>" + descendants + "</li>");
                        }
                    }

					if (!newMenu) {
						menu.Append("</ul>");
					}

                    
                }
				if (newMenu) {
					menu.Append("</ul>");
				}
				

            }
			mainMenu.Text = menu.ToString();
			secureMenu.Text = Utils.SecureMenu();


        }

        private string GetChildren(int pageId, string childStyleClass, int bannerBaseId) {
            string childObjects = string.Empty;
            using (SqlCommand cmd = new SqlCommand()) {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[Features].[proc_PageContent_GetPagesWithChildStatus]";
                cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                cmd.Parameters.AddWithValue("@ParentId", pageId);

                DataSet ds = Utils.SqlDb.ExecuteDataSet(cmd);

                var pageList = from tb in ds.Tables[0].AsEnumerable()
                               select new {
                                   ID = tb.Field<int?>("ID"),
                                   Category = tb.Field<long?>("CategoryID"),
                                   ParentId = tb.Field<long?>("ParentID"),
                                   ClientSiteID = tb.Field<long?>("ClientSiteID"),
                                   ShortTitle = tb.Field<string>("ShortTitle"),
                                   LongTitle = tb.Field<string>("LongTitle"),
                                   Description = tb.Field<string>("Description"),
                                   PageContent = tb.Field<string>("PageContent"),
                                   PageIndex = tb.Field<int>("PageIndex"),
                                   isArchived = tb.Field<bool?>("isArchived"),
                                   isDefault = tb.Field<bool?>("isDefault"),
                                   MetaKeywords = tb.Field<string>("MetaKeywords"),
                                   MetaDescription = tb.Field<string>("MetaDescription"),
                                   CustomXML = tb.Field<string>("CustomXML"),
                                   InsertedOn = tb.Field<DateTime?>("InsertedOn"),
                                   InsertedBy = tb.Field<long?>("InsertedBy"),
                                   DeletedOn = tb.Field<DateTime?>("DeletedOn"),
                                   DeletedBy = tb.Field<long?>("DeletedBy"),
                                   HasChildren = tb.Field<int?>("HasChildren"),
                                   MenuClass = tb.Field<string>("MenuClass"),
                                   RedirectURL = tb.Field<string>("RedirectURL"),
                                   PageTitle = tb.Field<string>("PageTitle")
                               };


                if (pageList.Count() > 0) {
                    childObjects += "<ul>";
                    foreach (var page in pageList) {

                        //store page title for use later
                        string pageTitle = !string.IsNullOrEmpty(page.PageTitle) ? page.PageTitle : page.ShortTitle;

                        string redirectAttribute = string.Empty;
                        string IDAttribute = string.Empty;
                        Dictionary<string, string> redirect = GetRedirects(page.RedirectURL);
                        if (redirect["IsAuroraRedirect"] == "True" && redirect["Valid"] == "True") {
                            redirectAttribute = " redirect='" + page.RedirectURL + "' ";
                            IDAttribute = redirect["IDAttribute"];
                        }

						string href = "/page/" + Server.HtmlEncode(page.Description.Replace(" ", "-")).ToLower();
						if (Utils.ConvertDBNull(Request["ID"], "") == page.Description.Replace(" ", "-").Replace("?", "").ToLower()) {
                            childObjects +=
                                string.Format(
                                    "<li  class='current " + page.MenuClass + "'><a dataid='" + page.ID + "' bannerbase='" + bannerBaseId +
                                    "' onclick='AuroraJS.Modules.GetContentForModule(\"page\"," + page.ID + ",\"" +
                                    Server.HtmlEncode(page.Description.Replace(" ", "-").Replace("'", "&#8216")).ToLower() +
									"\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" +
                                    href + "'  " + IDAttribute + redirectAttribute + ">{0}</a>" + GetChildren(page.ID.Value, "id='submenu'", bannerBaseId) +
                                    "</li>", Server.HtmlEncode(page.ShortTitle));
                        }
                        else {
                            childObjects += string.Format(
                                  "<li class=' " + page.MenuClass + "'><a dataid='" + page.ID + "' bannerbase='" + bannerBaseId +
                                  "' onclick='AuroraJS.Modules.GetContentForModule(\"page\"," + page.ID + ",\"" +
                                  Server.HtmlEncode(page.Description.Replace(" ", "-").Replace("'", "&#8216")).ToLower() +
								  "\"); return false;' title='" + Server.HtmlEncode(page.LongTitle) + "' href='" +
                                  href + "'  " + IDAttribute + redirectAttribute + ">{0}</a>" + GetChildren(page.ID.Value, "id='submenu'", bannerBaseId) +
                                  "</li>", Server.HtmlEncode(page.ShortTitle));
                        }
                    }
                    childObjects += "</ul>";

                }
                return childObjects;
            }
        }

        private string GetProductsMenu() {
            StringBuilder catList = new StringBuilder();
            //catList.Append("<ul>");

            var prodCategories = from categories in entities.Category
                                 where categories.ClientSiteID == SessionManager.ClientSiteID
                                 && categories.ParentID == 0
                                 && categories.Description == "Product"
                                 && categories.DeletedBy == null
                                 && categories.DeletedOn == null
                                 orderby categories.OrderIndex
                                 select categories;

            foreach (Category prodCategory in prodCategories) {
				string href = "/products/" + prodCategory.ID;
				long requestID;
				if (!long.TryParse(Request["ID"], out requestID)) {
					requestID = 0;
				}

                if (Utils.ConvertDBNull(Request["Type"], "Page").ToUpper() == "PRODUCTS" && requestID == prodCategory.ID) {
                    catList.Append("<li class='current'><a productid='" + prodCategory.ID + "' onclick='AuroraJS.Modules.getCategoriesWithinCategory(" + prodCategory.ID + ",\"" + Server.HtmlEncode(prodCategory.Name) + "\"," + prodCategory.ID + "); return false;' title='" + Server.HtmlEncode(prodCategory.Name) + "' href='" + href + "'>" + Server.HtmlEncode(prodCategory.Name) + "</a>" + GetProductChildren(prodCategory.ID, string.Empty, prodCategory.ID) + "</li>");
                }
                else {
                    catList.Append("<li><a productid='" + prodCategory.ID + "' onclick='AuroraJS.Modules.getCategoriesWithinCategory(" + prodCategory.ID +
                                   ",\"" + Server.HtmlEncode(prodCategory.Name) + "\"," + prodCategory.ID +
                                   "); return false;' title='" + Server.HtmlEncode(prodCategory.Name) + "' href='" +
                                   href + "'>" + Server.HtmlEncode(prodCategory.Name) + "</a>" +
                                   GetProductChildren(prodCategory.ID, string.Empty, prodCategory.ID) + "</li>");
                }


            }
            // catList.Append("</ul>");
            return catList.ToString();
        }

        private string GetProductChildren(int parentID, string childStyleClass, int bannerBaseId) {
            string childObjects = string.Empty;

            var prodCategories = from categories in entities.Category
                                 where categories.ClientSiteID == SessionManager.ClientSiteID
                                 && categories.ParentID == parentID
                                 && categories.DeletedBy == null
                                 && categories.DeletedOn == null
                                 orderby categories.Name
                                 select categories;
            if (prodCategories.Count() > 0) {
                childObjects += "<ul style=\"white-space:nowrap;\">";
                foreach (var cat in prodCategories) {
                    string href = "/Products/" + cat.ID;
					long prodID;
					if (!long.TryParse(Request["ID"], out prodID)) {
						prodID = 0;
					}

					if (Utils.ConvertDBNull(Request["Type"], "Page").ToUpper() == "PRODUCTS" && Utils.ConvertDBNull(prodID, 0) == cat.ID) {
                        childObjects +=
                            string.Format(
                                "<li class='current'><a productid='" + cat.ID + "' bannerbase='" + bannerBaseId +
                                "' onclick='AuroraJS.Modules.getCategoriesWithinCategory(" + cat.ID + ",\"" +
                                Server.HtmlEncode(cat.Name) + "\"," + cat.ID + "); return false;' title='" +
                                cat.Name + "' href='" + href + "'>{0}</a>" +
                                GetProductChildren(cat.ID, "id='submenu'", bannerBaseId) + "</li>",
                                Server.HtmlEncode(cat.Name));
                    }
                    else {
                        childObjects +=
                           string.Format(
                               "<li><a productid='" + cat.ID + "' bannerbase='" + bannerBaseId +
                               "' onclick='AuroraJS.Modules.getCategoriesWithinCategory(" + cat.ID + ",\"" +
                               Server.HtmlEncode(cat.Name) + "\"," + cat.ID + "); return false;' title='" +
                               cat.Name + "' href='" + href + "'>{0}</a>" +
                               GetProductChildren(cat.ID, "id='submenu'", bannerBaseId) + "</li>",
                               Server.HtmlEncode(cat.Name));
                    }
                }
                childObjects += "</ul>";

            }
            return childObjects;
        }

        /// <summary>
        /// Checks a redirectURL string to determine whether it is an aurora redirect
        /// </summary>
        /// <param name="redirectURL"></param>
        /// <returns>Dictionary of variables</returns>
        private Dictionary<string, string> GetRedirects(string redirectURL) {

            Dictionary<string, string> redirectVars = new Dictionary<string, string>();
            try {
                redirectURL = Utils.ConvertDBNull(redirectURL, string.Empty);
                if (redirectURL.StartsWith("#Products:")) {
                    redirectVars.Add("IsAuroraRedirect", "True");
                    string categoryName = redirectURL.Substring(redirectURL.IndexOf(":") + 1).Trim();
                    Custom.Data.AuroraEntities context = new Custom.Data.AuroraEntities();
                    var categories = (from cat in context.Category
                                      where cat.ClientSiteID == SessionManager.ClientSiteID &&
                                      cat.Name == categoryName &&
                                      cat.DeletedOn == null &&
                                      cat.DeletedBy == null

                                      select cat);
                    if (categories.Count() > 0) {
                        redirectVars.Add("Valid", "True");
                        redirectVars.Add("IDAttribute", " productID='" + categories.First().ID.ToString() + "' ");
                    }
                    else {
                        redirectVars.Add("Valid", "False");
                    }
                }
                else {
                    redirectVars.Add("IsAuroraRedirect", "False");
                }
            }
            catch (Exception ex) {

                throw;
            }
            return redirectVars;
        }
    }
}