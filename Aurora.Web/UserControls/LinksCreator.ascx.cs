﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora
{
    /// <summary>
    /// Author: Wesley Perumal
    /// Date:05 Apr 2011
    /// Description: Creates commented links to all the module that are available for display.
    /// This basically helps with SEO crawlers to find links from our modules,
    /// which allows all content on the site to be indexable.
    /// </summary>
    public partial class LinksCreator : System.Web.UI.UserControl
    {
        AuroraEntities entities = new AuroraEntities();
        private string linkTags = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //disabled method as it was not couture with google.
            return;
            using (SqlCommand cmd = new SqlCommand())
            {
                //Get all modules that are allowed to display on front end
                Utils.CheckSqlDBConnection();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[Clients].[proc_Module_GetConfigPerClient]";
                cmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                DataSet dsModuleLinks = Utils.SqlDb.ExecuteDataSet(cmd);

                if (dsModuleLinks.Tables.Count > 0) {

                    if (dsModuleLinks.Tables[0].Columns.Contains("DisplayOnFrontEnd")) {
                        using (SqlCommand cmdData = new SqlCommand()) {
                            //Get all module data
                            cmdData.CommandType = CommandType.StoredProcedure;
                            cmdData.CommandText = "[dbo].[proc_Modules_GetAllModuleData]";
                            cmdData.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                            cmdData.Parameters.AddWithValue("@CurrentDate", DateTime.Now);
                            cmdData.Parameters.AddWithValue("@ModuleFilter", Request["type"]);
                            DataSet dsModuleData = Utils.SqlDb.ExecuteDataSet(cmdData);

                            var pageList = from tb in dsModuleLinks.Tables[0].AsEnumerable()
                                           select new {
                                               Name = tb.Field<string>("Name"),
                                               Display = tb.Field<string>("DisplayOnFrontEnd")
                                           };

                            foreach (DataRow newsItem in dsModuleData.Tables[0].Rows) {
                                //Check if data is allowed for display
                                if (pageList.Where(tb => tb.Name == newsItem["Module"].ToString() && tb.Display == "true").Count() > 0)
                                    //append to hidden string
                                    linkTags += string.Format("<!--<a href='/{0}/{1}/{2}'>Link</a>-->", newsItem["Module"], newsItem["ID"], newsItem["Title"]);
                            }


                        }
                    }
                }

            }

         
        }
    }
}