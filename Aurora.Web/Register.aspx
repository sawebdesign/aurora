﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Aurora.Register" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Aurora.Custom" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register Assembly="Aurora.Custom" Namespace="Aurora.Custom.UI" TagPrefix="cui" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <link href="/styles/global.css" rel="stylesheet" type="text/css" />
    <div id="RegistrationForm">
        <div id="CompanyDetails">
            <input runat="server" id="ID" type="hidden" value="0" />
            <input runat="server" id="IsUpdate" type="hidden" value="" />
            <input runat="server" id="ClientSiteID" value="0" type="hidden" />
            <input runat="server" id="CustomXML" value="" type="hidden" />
            <input runat="server" id="InsertedOn" value="" type="hidden" />
            <input runat="server" id="InsertedBy" value="0" type="hidden" />
            <input runat="server" id="DeletedOn" value="" type="hidden" />
            <input runat="server" id="DeletedBy" value="" type="hidden" />
            <input runat="server" id="OrganisationID" value="" type="hidden" />
            <input runat="server" id="IsMember" value="false" type="hidden" />
            <input runat="server" id="MemberStatus" value="" type="hidden" />
            <table class="MemberDetailTable" width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr class="Details">
                        <td colspan="2">
                            <h1 class="pageHeader">Registration</h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
							<div class="cmszone" pageid="<%=cmsZoneID %>"></div>
						</td>
					</tr>
                    <tr>
                        <td colspan="2">
                            <h2>Please enter your details in the form below:</h2>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:250px;">
                            First Name:
                        </td>
                        <td>
                            <input runat="server" id="FirstName" type="text" valtype="required" maxlength="150"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <input type="text" id="LastName" runat="server" valtype="required" maxlength="150" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            <input runat="server" id="Email" type="text" valtype="required;regex:email" maxlength="350"/>
                        </td>
                    </tr>
                    <tr class="pwd">
                        <td>
                            Password:
                        </td>
                        <td>
                            <input runat="server" id="Password" type="password" valtype="required" maxlength="150"/>
                        </td>
                    </tr>
                    <tr class="pwd">
                        <td>Confirm Password:</td>
                        <td><input runat="server" id="Confirm" type="password" valtype="required;mustmatch:Password" maxlength="150" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cui:ObjectPanel ID="ContentPanel" Visible="false" runat="server">
                                <cui:XmlPanel ID="ContentXml" runat="server">
                                </cui:XmlPanel>
                            </cui:ObjectPanel>
                            <cui:ObjectPanel ID="ContenPanelAdditional" Visible="false" runat="server">
                                <cui:XmlPanel ID="ContentXmlAdditional" runat="server">
                                </cui:XmlPanel>
                            </cui:ObjectPanel>
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td>
                            Registration Type:
                        </td>
                        <td>
                            <select id="MemberType" runat="server"></select>
                        </td>
                    </tr>
                    <tr id="CategoryInfo" class="loading">
                        <td>
                            Registration Information:
                        </td>
                        <td>
                            <div class="loading"></div>
                            <div class="infopanel"></div>
                        </td>
                    </tr>
                    <tr id="PaymentOptions" style="display:none;">
                        <td>Payment Method:</td>
                        <td>
                            <select id="PaymentOptionType" runat="server"></select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br />
    <div class="lockContainer">
        <div id="Lock" class="QapTcha"></div>
        <input id="btnProcessOrder" type="button" name="ProcessOrder" onclick="confirmed=false;" value="Submit" />
    </div>
    <div style="clear:both;"></div>
    <div id="inputcontent">
    </div>
    <script type="text/javascript" defer="async">
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <script src="/scripts/jquery-1.5.2.js" type="text/javascript"></script>
    <script src="/scripts/utils.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="/scripts/aurora.custom.js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js" type="text/javascript" defer="defer"></script>
	<script src="/scripts/plugins/boltaccordion.js" type="text/javascript"></script>
    <script src="/scripts/modules.js?<%=lastModifiedDate%>" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js" type="text/javascript" defer="defer"></script>
    <link href="/styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="/styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fileupload/combined.js" type="text/javascript"></script>
    <link href="/scripts/fileupload/fineuploader.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jsrender.js" type="text/javascript"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js" type="text/javascript"></script>
    <script src="/scripts/superfish.js" type="text/javascript"></script>
    <script src="/scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <script src="/register.aspx.js" type="text/javascript"></script>
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>

</asp:Content>
