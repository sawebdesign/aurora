﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aurora.Custom.Data;
using Aurora.Services;
using System.Data;
using Aurora.Custom;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Aurora {
    public partial class _Default : Aurora.Common.BasePage {
        private AuroraWebService _auroraWebService = new AuroraWebService();
        public string pageHeaderImage = string.Empty;
        //used to control caching
        public string lastModifiedDate = new FileInfo(HttpContext.Current.Server.MapPath("~/Scripts/modules.js")).LastWriteTime.ToString("ddMMyyyyHHss");
        protected void Page_Load(object sender, EventArgs e) {
            Page.Title = CacheManager.SiteData.CompanyName;
            Literal noScript = new Literal();
            noScript.Text = "<noscript><div style='top: 0px; left: 0px; position: fixed; width: 100%; height: 70px; float: left;background: #000;'><div style=\"float: left;\"><img src=\"/Styles/images/boltMan.png\" width=\"80\" height=\"70\" /></div><div style=\"float: left;font-weight:bold;font-size:20px;margin-top:15px;color:#fff700;\">Um...Something's missing, click <a style=\"color:red;\" href=\"Default.aspx?nojs=yes\">here</a> to find out why?</div></div></noscript>";

            if (Request["nojs"] != null && Request["nojs"] == "yes") {
                var x = this.Master.FindControl("MainContent");

            }


            if (Request["type"] == null) {
                LoadPage(true);
            }
            else {
                switch (Request["type"].ToUpper()) {
                    default:
                        LoadErrorMessage();
                        break;
                    case "PAGE":
                        LoadPage(false);
                        break;
                    case "NEWS":
                    case "EVENT":
                    case "TESTIMONIES":
                        LoadTemplatedData(Request["type"]);
                        break;
                    case "PRODUCTS":
                        LoadProducts();
                        break;
                    case "PRODUCT":
                        LoadProduct();
                        break;
                }
            }
        }

        private void LoadProducts() {
            InsertMetaTags(false);
            // ClientScript.RegisterStartupScript(this.GetType(), "initProducts", string.Format("AuroraJS.Modules.getProductForCategory({0},'{1}')",Request["ID"],Request["title"]), true);
        }

        private void LoadProduct() {
            InsertMetaTags(true);
        }


        /// <summary>
        /// Returns the data for the selected module which is then bound to the template
        /// </summary>
        /// <param name="type"></param>
        private void LoadTemplatedData(string type) {
            DataSet dsDetail;
            Utils.CheckSqlDBConnection();
			try {

				using (SqlCommand cmd = new SqlCommand()) {
					cmd.CommandType = CommandType.StoredProcedure;
					switch (type.ToUpper()) {
						case "NEWS":
							cmd.CommandText = "[dbo].[proc_News_GetNewsByID]";
							break;
						case "EVENT":
							cmd.CommandText = "[dbo].[proc_Event_GetEventsByID]";
							break;
						case "TESTIMONIES":
							cmd.CommandText = "[dbo].[proc_Modules_GetTestmoniesByID]";
							break;
					}

					cmd.Parameters.AddWithValue("@ID", Request["title"].Replace('-', ' '));

					AuroraEntities entities = new AuroraEntities();
					long currentID;
					long.TryParse(Request["ID"], out currentID);
					bool isSecure = (from roleModule in entities.RoleModule
									 where roleModule.ModuleName == type
										   && roleModule.ModuleItemID == currentID
									 select roleModule
								 ).Count() > 0;
					if (isSecure) {
						isSecure = SessionManager.VistorSecureSessionID == Guid.Empty;
					}

					if (isSecure) {
						Response.Redirect("Login.aspx");
					}

					dsDetail = Utils.SqlDb.ExecuteDataSet(cmd);
				}
				if (dsDetail.Tables[0] != null && dsDetail.Tables[0].Rows.Count > 0) {
					string html = BindTemplateData(dsDetail.Tables[0], type + "Detail");

					inputcontent.InnerHtml = html;
					if (dsDetail.Tables[0].Columns.Contains("Title"))
					{
						Page.Title = Utils.ConvertDBNull(dsDetail.Tables[0].Rows[0]["Title"], string.Empty);    
					}

					if (type.ToUpper() == "TESTIMONIES")
					{
						Page.Title = Utils.ConvertDBNull(dsDetail.Tables[0].Rows[0]["FirstName"], "") + " " + Utils.ConvertDBNull(dsDetail.Tables[0].Rows[0]["LastName"],string.Empty);
					}
                

				}
				else {
					LoadErrorMessage();
				}
			} catch (Exception) {

				LoadErrorMessage();
			}
        }

        /// <summary>
        /// Default message when an error or no items are found
        /// </summary>
		private void LoadErrorMessage() {
			HttpContext context = HttpContext.Current;
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetNoStore();
			context.Response.Cache.SetExpires(DateTime.MinValue);

			//Internal Server Error
			context.Response.StatusCode = 404;
			context.Response.End();
			return;

			//Title = "Page Not Found";
			//inputcontent.InnerHtml = string.Format("<h3>{0}</h3>{1}", "Page not found", "The resource you are looking for has been removed, had its name changed, or is temporarily unavailable.");
		}

        /// <summary>
        /// Returns data for specified page which is then bound to a template
        /// </summary>
        /// <param name="isDefault"></param>
        private void LoadPage(bool isDefault) {
            //Get Content for page
            DataSet dsDetail;
            Utils.CheckSqlDBConnection();
            using (SqlCommand pagecmd = new SqlCommand()) {
                pagecmd.CommandType = CommandType.StoredProcedure;
                pagecmd.CommandText = "[dbo].[proc_Modules_GetPages]";
                pagecmd.Parameters.AddWithValue("@ClientSiteID", SessionManager.ClientSiteID);
                if (!isDefault) {
					pagecmd.Parameters.AddWithValue("@ID", Request["title"].Replace('-', ' '));
                }
                pagecmd.Parameters.AddWithValue("@isDefault", isDefault);
                dsDetail = Utils.SqlDb.ExecuteDataSet(pagecmd);

               

                if (dsDetail.Tables[0] != null && dsDetail.Tables[0].Rows.Count > 0) {
                    //check if page is secure
                    AuroraEntities entities = new AuroraEntities();
                    long currentID;
                    long.TryParse(dsDetail.Tables[0].Rows[0]["ID"].ToString(), out currentID);

                    IEnumerable<RoleModule> secureRoles = Utils.CheckItemSecurity("PageDetail", currentID);
                    bool authenticated = true;
                    if (secureRoles.Count() > 0) {
                        authenticated = false;
                        if (SessionManager.MemberData != null) {
                            var roleMatches = (from memRoles in entities.GetActiveMemberCategories(SessionManager.MemberData.ID, SessionManager.ClientSiteID)
                                               join s in secureRoles on memRoles equals s.RoleID
                                               select memRoles);
                            if (roleMatches.Count() > 0)
                                authenticated = true;
                        }
                    }

					//its a secured page hide or request login
					if ((bool)dsDetail.Tables[0].Rows[0]["IsSecured"] && !(bool)dsDetail.Tables[0].Rows[0]["IsDefault"] )
					{
						Response.Redirect("~/Default.aspx");
						return;
					}

                    if (!authenticated) {
                        Response.Redirect("~/Login.aspx");
                    }
                   
                    //bool isSecure = (from roleModule in entities.RoleModule
                    //                 where roleModule.ModuleName == "PageDetail"
                    //                       && roleModule.ModuleItemID == currentID
                    //                 select roleModule
                    //             ).Count() > 0;

                    //if (isSecure) {
                    //    isSecure = SessionManager.VistorSecureSessionID == Guid.Empty;
                    //}

                    //if (isSecure) {
                    //    Response.Redirect("~/Login.aspx");
                    //}

                    //check for redirect
                    if (Utils.ConvertDBNull(dsDetail.Tables[0].Rows[0]["RedirectURL"], string.Empty) != string.Empty) {
						if (HandleAuroraRedirect(dsDetail.Tables[0].Rows[0]["RedirectURL"].ToString(), Utils.ConvertDBNull(dsDetail.Tables[0].Rows[0]["ID"], 0))) {
                            return;
                        }
						if ((bool)dsDetail.Tables[0].Rows[0]["PermanentRedirect"] == true) {
							Response.RedirectPermanent(dsDetail.Tables[0].Rows[0]["RedirectURL"].ToString());
						} else {
							Response.Redirect(dsDetail.Tables[0].Rows[0]["RedirectURL"].ToString());
						}
                    }
                    //check if page has header image template
                    HtmlGenericControl pageImage = (HtmlGenericControl)Master.FindControl("pageHeaderImage");

                    if (pageImage != null) {
                        if (File.Exists(Server.MapPath(string.Format("~/ClientData/{0}/Uploads/page_{1}.jpg", SessionManager.ClientSiteID, dsDetail.Tables[0].Rows[0]["ID"])))) {
                            pageImage.InnerHtml = string.Format("<img id=\"headerimg\" src=\"{0}/Uploads/{1}_{2}.jpg\"  />",
                                                            "/ClientData/" + Custom.SessionManager.ClientSiteID, "page",
                                                             dsDetail.Tables[0].Rows[0]["ID"]);
                        }
                    }
                    string html = BindTemplateData(dsDetail.Tables[0], "PageDetail");
                    //inject page ID to be accessible via javascript
                    ClientScript.RegisterStartupScript(this.GetType(), "PageID", string.Format("SetJSSession('PageID','{0}');", dsDetail.Tables[0].Rows[0]["ID"]), true);
                    inputcontent.InnerHtml = html;

                    Page.Title = Utils.ConvertDBNull<string>(dsDetail.Tables[0].Rows[0]["PageTitle"], "");

                    if(Page.Title == "") {
                    Page.Title = Utils.ConvertDBNull<string>(dsDetail.Tables[0].Rows[0]["ShortTitle"], "");
                    }

                    InsertMetaTags(dsDetail);

                    //set var to determine default page or not
                    ClientScript.RegisterStartupScript(GetType(), "defaultpage", string.Format("AuroraJS.Modules.DefaultPage={0};", dsDetail.Tables[0].Rows[0]["isDefault"].ToString().ToLower()), true);
                    ClientScript.RegisterStartupScript(GetType(), "modules",
                                                       "AuroraJS.Modules.LoadModulesInContent('#inputcontent');", true);
					var pageSettings = Utils.GetFieldsFromCustomXML("PageDetail");
					if ((bool)dsDetail.Tables[0].Rows[0]["isDefault"] != true && pageSettings["Page_autoScroll"].ToLower() == "true") {
						ClientScript.RegisterStartupScript(GetType(), "scroll", string.Format("scrollToElement('{0}', {1}, 1000)",pageSettings["Page_scrollTo"], pageSettings["Page_offSet"]), true);
					}
                }
                else {
                    LoadErrorMessage();
                }


            }

        }

        /// <summary>
        /// Accepts a Data table which it then uses against a template found in the DB. It replaces column names found within the
        /// template html with the values within the row of that column.
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="TemplateName"></param>
        /// <returns></returns>
        private string BindTemplateData(DataTable tbl, string TemplateName) {
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            DataSet dsTemplate;
            using (SqlCommand cmd = new SqlCommand()) {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[Clients].[proc_Module_GetByClientSiteIDName]";
                cmd.Parameters.AddWithValue("@ClientSiteID", Custom.Utils.ConvertDBNull<int>(SessionManager.ClientSiteID, 0));
                cmd.Parameters.AddWithValue("@Name", TemplateName);
                dsTemplate = Utils.SqlDb.ExecuteDataSet(cmd);
            }

            string Template = "No template was found for " + TemplateName;
            if (dsTemplate.Tables[0] != null && dsTemplate.Tables[0].Rows.Count > 0) {
                Template = dsTemplate.Tables[0].Rows[0]["HTML"] == DBNull.Value
                            ? dsTemplate.Tables[0].Rows[0]["DefaultHTML"].ToString()
                            : dsTemplate.Tables[0].Rows[0]["HTML"].ToString();

                foreach (DataRow row in tbl.Rows) {

                    foreach (DataColumn col in tbl.Columns) {
                        if (col.ColumnName.Contains("Date")) {
                            if (TemplateName.ToUpper().Contains("EVENT")) {
                                Template = Template.Replace("<tr><td><b>Ends On:</b> </td><td style='width:15px;'></td><td>1/1/2050</td></tr>", string.Empty);
                                Template = Template.Replace("+ID", row["ID"].ToString());
                                if (col.ColumnName == "EndDate" &&
                                    (row["Occurance"] != null && row["Occurance"] != "None")) {
                                    Template = Template.Replace("{{formatDateTime(EndDate,\"dd/MM/yyyy\")}}",
                                                                "" + DateTime.Now.ToShortDateString() + "");
                                    Template = Template.Replace("{{formatDateTime(EndDate,\"dd/MM/yyyy HH:mm\")}}",
                                                                DateTime.Now.ToShortDateString() + " " +
                                                                DateTime.Parse(row["EndDate"].ToString()).
                                                                    ToShortTimeString());
                                }
                                else {
                                    Template = Template.Replace("{{formatDateTime(EndDate,\"dd/MM/yyyy\")}}",
                                                                "" + DateTime.Parse(row["EndDate"].ToString()).ToShortDateString() + "");
                                    Template = Template.Replace("{{formatDateTime(EndDate,\"dd/MM/yyyy HH:mm\")}}",
                                                                "" + DateTime.Parse(row["EndDate"].ToString()).ToShortDateString() + " " +
                                                                DateTime.Parse(row["EndDate"].ToString()).
                                                                    ToShortTimeString());
                                }

                                //last check
                                Template = Template.Replace("{{formatDateTime(EndDate,\"HH:mm\")}}", "" + DateTime.Parse(row["EndDate"].ToString()).ToString("HH:mm") + "");
                            }
                            if (TemplateName.ToUpper().Contains("EVENT")) {
                                if (col.ColumnName == "StartDate" && (row["Occurance"] != null && row["Occurance"].ToString().ToUpper() != "NONE")) {
                                    Template = Template.Replace("{{formatDateTime(StartDate,\"dd/MM/yyyy\")}}", "" + DateTime.Now.ToString("dd/MM/yyyy") + "");
                                    Template = Template.Replace("{{formatDateTime(StartDate,\"dd/MM/yyyy HH:mm\")}}", DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " " + DateTime.Parse(row["StartDate"].ToString()).ToShortTimeString());
                                }
                                else {
                                    Template = Template.Replace("{{formatDateTime(StartDate,\"dd/MM/yyyy\")}}", "" + DateTime.Parse(row["StartDate"].ToString()).ToString("dd/MM/yyyy") + "");
                                    Template = Template.Replace("{{formatDateTime(StartDate,\"dd/MM/yyyy HH:mm\")}}", "" + DateTime.Parse(row["StartDate"].ToString()).ToString("dd/MM/yyyy HH:mm") + " " + DateTime.Parse(row["StartDate"].ToString()).ToShortTimeString());
                                }

                                //last check
                                Template = Template.Replace("{{formatDateTime(StartDate,\"HH:mm\")}}", "" + DateTime.Parse(row["StartDate"].ToString()).ToString("HH:mm") + "");

                            }

                            //ical Check
                            Template = Template.Replace("sys:href=\"{{'/Ical.ashx?EventID='+ID}}\"", String.Format("href=\"/Ical.ashx?EventID={0}\"", row["ID"].ToString()));
                            Template = Template.Replace("sys:", string.Empty);
                        }
                        else {
                            Template = Template.Replace("{{" + col.ColumnName + "}}", "" + row[col.ColumnName] + "");
                        }
                        //Image replacers follows predefined file name conventions

                    }

                    //Page Images
                    if (File.Exists(Server.MapPath("~/ClientData/" + Custom.SessionManager.ClientSiteID + "/Uploads/page_" + row["ID"] + ".jpg"))) {
                        Template = Template.Replace("{{Image}}",
                                                    string.Format(
                                                        "<img id=\"headerimg\" src=\"{0}/Uploads/page_{1}.jpg\"  /></br>",
                                                        "/ClientData/" + Custom.SessionManager.ClientSiteID,
                                                        row["ID"]));
                    } else {
                        Template = Template.Replace("{{Image}}", string.Empty);
                    }

                    if (!containsABadCharacter.IsMatch(Request["type"] + Request["id"])) {
                        //Module images 1
                        if (File.Exists(Server.MapPath("~/ClientData/" + Custom.SessionManager.ClientSiteID + "/Uploads/" + Request["type"] + "_01_" + Request["id"] + ".jpg"))) {
                            Template = Template.Replace("{{Image1}}",
                                                        string.Format(
                                                            "<img id=\"headerimg\" src=\"{0}/Uploads/{1}_01_{2}.jpg\"  /></br>",
                                                           "/ClientData/" + Custom.SessionManager.ClientSiteID, Request["type"],
                                                            row["ID"]));
                        } else {
                            Template = Template.Replace("{{Image1}}", string.Empty);
                        }

                        //Module images 2
                        if (File.Exists(Server.MapPath("~/ClientData/" + Custom.SessionManager.ClientSiteID + "/Uploads/" + Request["type"] + "_02_" + Request["id"] + ".jpg"))) {
                            Template = Template.Replace("{{Image2}}",
                                                        string.Format(
                                                            "<img id=\"headerimg\" src=\"{0}/Uploads/{1}_02_{2}.jpg\"  /></br>",
                                                           "/ClientData/" + Custom.SessionManager.ClientSiteID, Request["type"],
                                                            row["ID"]));
                        } else {
                            Template = Template.Replace("{{Image2}}", string.Empty);
                        }
                    } else {
                        Template = Template.Replace("{{Image1}}", string.Empty);
                        Template = Template.Replace("{{Image2}}", string.Empty);
                    }

                    //custom code I know but unavoidable
                    if(TemplateName.ToUpper() == "EVENTDETAIL") { 
                        //create an event entity from the row
                        var eventEntity = new Object();
                        
                        var tmpEvent = new Aurora.Custom.Data.Event() {
                            Details = row ["Details"].ToString(),
                            EndDate = DateTime.Parse(row ["EndDate"].ToString()),
                            GeoLatLong = row ["GeoLatLong"].ToString(),
                            ID = Int64.Parse(row ["ID"].ToString()),
                            Location = row ["Location"].ToString(),
                            Occurance = row ["Occurance"].ToString(),
                            Preview = row ["Preview"].ToString(),
                            StartDate = DateTime.Parse(row ["StartDate"].ToString()),
                            Title = row ["Title"].ToString()
                        };

                        eventEntity = Utils.getAnonEvent(tmpEvent);
                        
                        Template = Template.ReplaceObjectTokens(eventEntity);
                    }
                }
            }

            return Template;
        }

        /// <summary>
        /// Injects description and keyword meta tags on page load
        /// </summary>
        /// <param name="requestedPage">Page Content Entity</param>
        private void InsertMetaTags(DataSet requestedPage) {
            HtmlMeta meta = new HtmlMeta();
			meta.Name = "Title";
			meta.Content = requestedPage.Tables[0].Rows[0]["PageTitle"].ToString();
			Page.Header.Controls.Add(meta);

			meta = new HtmlMeta();
            meta.Name = "description";
            meta.Content = requestedPage.Tables[0].Rows[0]["MetaDescription"].ToString();
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.Name = "keywords";
            meta.Content = requestedPage.Tables[0].Rows[0]["MetaKeyWords"].ToString();
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.HttpEquiv = "CACHE-CONTROL";
            meta.Content = DateTime.Now.AddDays(1).ToString("ddd, dd MMM yyyy HH:mm:ss") + " GMT";
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.HttpEquiv = "CONTENT-TYPE";
            meta.Content = "text/html; charset=UTF-8";
            Page.Header.Controls.Add(meta);

			DateTime contentModified = (DateTime)requestedPage.Tables[0].Rows[0]["InsertedOn"];

            meta = new HtmlMeta();
            meta.HttpEquiv = "LAST-MODIFIED";
            meta.Content = contentModified.ToString("ddd, dd MMM yyyy HH:mm:ss") + " GMT";
            Page.Header.Controls.Add(meta);

        }

        /// <summary>
        /// Injects meta tags on page load for the product gallery and product pages
        /// </summary>
        /// <param name="isProduct">If true, creates tags for a specific product</param>
        private void InsertMetaTags(Boolean isProduct) {
            HtmlMeta meta = new HtmlMeta();
            int productID;
			DateTime contentModified = DateTime.Now;
            if (isProduct && Int32.TryParse(Request["title"], out productID)) {
                try {
                    meta = new HtmlMeta();
                    meta.Attributes["property"] = "og:image";
                    Aurora.Services.AuroraWebService auroraWS = new AuroraWebService();
                    string domain = Request.ServerVariables["SERVER_NAME"];
                    
                    Dictionary<string, object> galleryDictionary = auroraWS.GetProductGallery(productID);
                    if ((int)galleryDictionary["Count"] != 0) {
                        Aurora.Custom.Data.Gallery galleryObject = ((System.Data.Objects.ObjectQuery<Custom.Data.Gallery>)galleryDictionary["Data"]).First();

                        meta.Content = String.Format("http://" + domain + "/ClientData/{0}/Uploads/product_{1}{2}", SessionManager.ClientSiteID, galleryObject.ID, galleryObject.MediaType);
                    }
                    else {
                        meta.Content = String.Format("http://" + domain + "/Styles/images/no-image.jpg");
                    }
                    Page.Header.Controls.Add(meta);

                    Custom.Data.AuroraEntities context = new Custom.Data.AuroraEntities();
                    var product = (from prod in context.Product
                                   where prod.ID == productID
                                   select prod).First();
                    meta = new HtmlMeta();
                    meta.Attributes["property"] = "og:description";
                    meta.Content = product.Name;
                    Page.Header.Controls.Add(meta);

					contentModified = product.InsertedOn;
                }
                catch (Exception error) {
                    Utils.SendErrorMail(ConfigurationManager.AppSettings["ErrorMailList"], "Aurora", string.Format("<b>Message:</b>{0}<br/><br/> <b>Stacktrace:</b> {1} <br/><br/> <b>InnerException:</b>{2} <br/><br/> <b>Target Site:</b>{3}", error.Message, error.StackTrace, error.InnerException, error.TargetSite), "Error occured on Aurora");
                }

            }

            meta = new HtmlMeta();
            meta.HttpEquiv = "CACHE-CONTROL";
            meta.Content = DateTime.Now.AddDays(1).ToString("ddd, dd MMM yyyy HH:mm:ss") + " GMT";
            Page.Header.Controls.Add(meta);


            meta = new HtmlMeta();
            meta.HttpEquiv = "CONTENT-TYPE";
            meta.Content = "text/html; charset=UTF-8";
            Page.Header.Controls.Add(meta);

            

            meta = new HtmlMeta();
            meta.HttpEquiv = "LAST-MODIFIED";
            meta.Content = contentModified.ToString("ddd, dd MMM yyyy HH:mm:ss") + " GMT";
            Page.Header.Controls.Add(meta);

            //SEO
            meta = new HtmlMeta();
            meta.HttpEquiv = "Description";
            meta.Content = CacheManager.SiteData.CompanyDescription;
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.HttpEquiv = "Robots";
            meta.Content = "index,follow";
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.HttpEquiv = "Author";
            meta.Content = "Prepeeled";
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta();
            meta.HttpEquiv = "Copyright";
            meta.Content = CacheManager.SiteData.CompanyName;
            Page.Header.Controls.Add(meta);

        }

		private bool HandleAuroraRedirect(string redirectURL, long pageID) {
            Boolean isValid = false;
            if (redirectURL.StartsWith("#Products:")) {
                string categoryName = redirectURL.Substring(redirectURL.IndexOf(":") + 1).Trim();
                Custom.Data.AuroraEntities context = new Custom.Data.AuroraEntities();
                var categories = (from cat in context.Category
                                  where cat.ClientSiteID == SessionManager.ClientSiteID &&
                                  cat.Name == categoryName &&
                                  cat.DeletedBy == null && 
                                  cat.DeletedOn == null
                                  select cat);
                if (categories.Count() > 0) {
                    Response.Redirect(String.Format("/Products/{0}/{1}", categories.First().ID.ToString(), categoryName));
                    isValid = true;
                }
            }
            else if(redirectURL.Contains("func:"))
            {
				ClientScript.RegisterStartupScript(GetType(), "ZX", String.Format("setTimeout(function(){2}AuroraJS.Modules.GetContentForModule('Page',{0},'{1}');{3},2500);", pageID, Request["title"], "{", "}"), true);
                return true;
            }

            return isValid;

        }
    }
}
