﻿using Aurora.Custom;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Aurora.httphandlers {
	/// <summary>
	/// Summary description for Uploadfile
	/// </summary>
	public class UploadFile : IHttpHandler, IRequiresSessionState {
        
        public void ProcessRequest(HttpContext context) {
            context.Response.ContentType = "text/plain";
            context.Response.Write(Uploader(context));
            return;
        }

        private string Uploader(HttpContext contextUpload) {
            string uploadMessage = string.Empty;
            try {

                HttpPostedFile file = contextUpload.Request.Files["qqfile"];
               
                string uploadType = (contextUpload.Request.QueryString["UploadType"] == "") ? null : contextUpload.Request.QueryString["UploadType"];

				string path = HttpContext.Current.Server.MapPath("\\ClientData\\" + CacheManager.SiteData.ClientSiteID + "\\Uploads\\temp\\");
				string filename = contextUpload.Request["Name"];
				SessionManager.TempFileName = path + Guid.NewGuid().ToString() + "." +filename.Substring(filename.LastIndexOf('.') + 1);
				Directory.CreateDirectory(path);
				if (file == null) {
					Stream fileStream = contextUpload.Request.InputStream;
					System.IO.File.WriteAllBytes(SessionManager.TempFileName, ReadToEnd(contextUpload.Request.InputStream));
				} else {
					file.SaveAs(SessionManager.TempFileName);
				}

				//TODO: Garth - Add a try/catch wrapped cleanup script here to remove any temporary files older than a certain time period.

                uploadMessage = "{\"success\":true,\"isImage\": \"" + true + "\",\"fileid\":\"" + "" + "\"}";
                    
            } catch (Exception exception) {

                uploadMessage = "{\"error\":\"" + exception.Message + " - " + exception.InnerException.Message + "\"}";                
            }

            return uploadMessage;
        }

        public bool IsReusable {
            get {
                return false;
            }
        }

		public static byte[] ReadToEnd(System.IO.Stream stream) {
			long originalPosition = stream.Position;
			stream.Position = 0;
			try {
				byte[] readBuffer = new byte[4096];
				int totalBytesRead = 0;
				int bytesRead;
				while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
					totalBytesRead += bytesRead;
					if (totalBytesRead == readBuffer.Length) {
						int nextByte = stream.ReadByte();
						if (nextByte != -1) {
							byte[] temp = new byte[readBuffer.Length * 2];
							Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
							Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
							readBuffer = temp; totalBytesRead++;
						}
					}
				}
				byte[] buffer = readBuffer;
				if (readBuffer.Length != totalBytesRead) {
					buffer = new byte[totalBytesRead];
					Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
				}
				return buffer;
			} finally {
				stream.Position = originalPosition;
			}
		}
	}
}