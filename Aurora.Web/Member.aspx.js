﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx" />
/// <reference path="scripts/microsoftajax.vsextensions.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="scripts/fancybox/jquery.fancybox-1.3.0.js" />
/// <reference path="/Scripts/modules.js" />
/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx" />
/// <reference path="scripts/microsoftajax.vsextensions.js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" />
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="scripts/fancybox/jquery.fancybox-1.3.0.js" />
/// <reference path="/Scripts/modules.js" />


(function ($, window, document, undefined) {

    var memMethods = {
        init: function (categoryID) {
            $('#Lock').QapTcha({
                buttonLock: "#btnProcessOrder",
                buttonFunc: memMethods.RegisterUser
            });
            if ($('#CategoryInfo').length > 0) {
                memMethods.getCategories();
            }

            //if (categoryID) {
            //    memMethods.getCategoryInfo(categoryID);
            //};

            if ($('#MemberType option').length > 1) {
                $("#MemberType").parent().parent().show();
            }

            $('#MemberType').change(memEvents.changeCategory);

            if (AuroraJS.Bespoke.MemberProfile) {
            	AuroraJS.Bespoke.MemberProfile();
            }
            $('#ChangePassword').click(memEvents.changePasswordClick);
            $('input[type=file]').each(function () {
            	var $this = $(this);
            	var $container = $("<div class='uploader'></div>");
            	$container.fineUploader({
            		request: {
            			endpoint: '/httphandlers/uploadfile.ashx',
            			params: {
            				fileID: $this.attr('id')
            			}
            		},
            		multiple: false,
            		validation: {
            			allowedExtensions: ['jpeg', 'jpg', 'png'],
            			sizeLimit: 153600
            		}
            	}).bind('submit', function (event, id, filename) {
            		$(event.currentTarget).fineUploader('setParams', {
            			name: filename
            		});
            	}).bind('complete', function (event, id, filename, response) {

            	});
            	$(this).replaceWith($container);
            });
        },
        RegisterUser: function (memberCategory) {
            if (validateForm(undefined, "", "", false, undefined, "#MemberProfileForm")) {
                $("<div>Please review the form as some required fields have not been entered.</div>").dialog({
                    modal: true,
                    resizable: false,
                    title: "Error",
                    buttons: { "Okay": function () { $(this).dialog("close") } }
                });
                return;
            } else {

                var memberObject = {};//new Aurora.Custom.Data.Member();
                $('#CompanyDetails').mapToObject(memberObject);

                var $customFields = $('#tbl_CustomFields').find(':input');
                var $additionalFields = $('#tbl_AdditionalData').find(':input');
                var additionalXML;

                memberObject.InsertedOn = new Date();
                memberObject.DeletedOn = null;
                memberObject.DeletedBy = null;
                memberObject.LastUpdated = new Date();

                if ($customFields.length > 0) {
                    memberObject.CustomXML = buildCustomXMLBySelector($customFields, "value", true);
                } else {
                    memberObject.CustomXML = null;
                }
                if ($additionalFields.length > 0) {
                    additionalXML = buildCustomXMLBySelector($additionalFields, "value");
                } else {
                    additionalXML = "<XmlData><Fields></Fields></XmlData>";
                }
                var paymentSchemaID;
                if ($('#PaymentOptions').is(':visible')) {
                    paymentSchemaID = $('#PaymentOptionType').val();
                } else {
                    paymentSchemaID = -1;
                }
                $('#MemberProfileForm').hide();

                Aurora.Services.AuroraWebService.UpdateMember(memberObject, additionalXML, memEvents.UpdateUserSuccess, memEvents.RegisterError);
                //Aurora.Services.AuroraWebService.CreateMember(memberObject, memberObject.MemberType, additionalXML, paymentSchemaID, memEvents.RegisterUserSuccess, memEvents.RegisterError);
            }
        },
        getCategories: function () {
            Aurora.Services.AuroraWebService.GetMemberCategories(memEvents.CategorySuccess, onError);
        },
        requestRenewal: function (memberCategoryID) {
            $('#MemberProfileForm').hide();
            Aurora.Services.AuroraWebService.RequestRenewal(memberCategoryID, $('#PaymentOptionType').val(), memEvents.renewalResponse, onError);
        },
        showCategorySelection: function (memberCategoryID) {
            $('#CompanyDetails, .lockContainer').hide();
            $('.categoryContainer').hide();
            var $container = $('<div class="categorySelection"><h1 class="changeCategoryHeader">Change your registration type</h1><h3>Select a category</h3></div>');
            $container.append($('#MemberType'));
            $container.delegate('#MemberType', 'change.category', memEvents.changeCategory);
            $('#CategoryInfo').before($container);
            var $button = $('<button class="button changesub" type="button" dataid="'+memberCategoryID+'">Change</button>');
            $button.click(memEvents.submitChange);
            $('#PaymentOptions').append($('#PaymentOptionType')).after($button);
            memMethods.getCategoryInfo($('#MemberType').val());
        },
        getCategoryInfo: function (categoryID) {
            Aurora.Services.AuroraWebService.GetMembershipCategoryInformation(categoryID, memEvents.getCategoryInfoSuccess, onError);
        },
        renderCategoryInfo: function (category) {
        	var $container = $('#CategoryInfo .infopanel')
        	var infoTable = "";
        	if (AuroraJS.Templates.CategoryInformationTemplate) {
        		infoTable = $(AuroraJS.Templates.CategoryInformationTemplate.render(category));
        	} else {

        		//js session activation for js render methods
        		$.views.helpers({ GetJSSession: GetJSSession });

        		tmp = $.templates('<table>' +
						'<tr>' +
							'<td>Type:</td>' +
							'<td>{{:Name}}</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Duration:</td>' +
							'<td>{{: (RoleDuration > 0 ? RoleDuration + " days" : "Forever") }}</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Cost:</td>' +
							'<td>{{if Cost > 0}}' +
									'{{:~GetJSSession("Currency_Symbol")}} {{: (Cost.toFixed(2))}} ({{:~GetJSSession("Currency_Code")}})' +
									'{{if IsRecurring}}' +
										' (Monthly)' +
									'{{/if}}' +
									'<div class="conversions" value="{{:Cost}}"></div>' +
								'{{else}}' +
								'Free' +
								'{{/if}}' +
							'</td>' +
						'</tr>' +
					'</table>');
        		infoTable = tmp.render(category);
        	}
        	$container.html(infoTable);
        	
        	$('#CategoryInfo').show();
        	$('#CurrentCategoryInfo').hide();
        },
        ChangePassword: function ($dialog) {
            if (validateForm(undefined, "", "", false, undefined, ".newPasswordTable")) {

            } else {
                var oldPass = $dialog.find('input#OldPass').val();
                var newPass = $dialog.find('input#NewPass').val();
                var confirmPass = $dialog.find('input#ConfirmPass').val();
                if (newPass === confirmPass) {
                    Aurora.Services.AuroraWebService.ChangeLoggedInMemberPassword(oldPass, newPass, confirmPass, memEvents.ChangePasswordSuccess, onError, $dialog);
                    $dialog.dialog('close');
                    $dialog.remove();
                    $('#MemberProfileForm').block({ message: null });
                }
                $dialog.dialog('close');
                $dialog.remove();
            }

        }
    };

    var memEvents = {
        //#region Changes
        changeCategory: function (event) {
            var $this = $(event.currentTarget);
            $('#CategoryInfo').addClass('loading');
            memMethods.getCategoryInfo($this.val());
        },
        //#endregion
        //#region XHR Responses
        getCategoryInfoSuccess: function (response) {
            if (response.Count === 1) {
                $('#CategoryInfo').removeClass('loading');
                memMethods.renderCategoryInfo(response.Data);
                if ($.CurrencyConverter) {
                	$.CurrencyConverter($('#CategoryInfo .conversions'));
                }
                if (response.Data.Cost > 0) {
                    $('#PaymentOptions').show()
                    $('#PaymentOptionType').attr('valtype', 'required');
                    if (response.Data.IsRecurring) {
                    	var $options = $('#PaymentOptions option').filter(':not([supportsrecurring])');
                    	$options.hide().attr('disabled', true);
                    } else {
                    	$('#PaymentOptions option').show()
                    };
                } else {
                    $('#PaymentOptions').hide()
                    $('#PaymentOptionType').removeAttr('valtype');
                }
            } else {
                onError(response);
            }
        },
        UpdateUserSuccess: function (response) {
            if (response.Result === true) {
                $.auroraMember.member = response.Data;
                switch (response.Count) {
                    case -1:
                        $('<div>The email you provided has already been used to register an account. Please log in to update your membership</div>').dialog({
                            modal: true,
                            resizable: false,
                            title: "Error",
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                    $('#RegistrationForm').show();
                                }
                            }
                        });
                        break;
                    case 1:

                        //check if the category has changed
                        memberCategory = $.auroraMember.memberCategories[0];
                        var tmp = $('#MemberType').val();
                        if (tmp != memberCategory.MemberCategory.RoleID) {
                            var $dialog = $('<div>Are you sure you want to change registration type?</div>');
                            if (memberCategory.Category.Duration > 0) {
                                $dialog.append('You will lose any remaining time on this registration type');
                            };
                            $dialog.dialog({
                                modal: true,
                                title: "Change Registration Type",
                                buttons: {
                                    "Yes": function () {
                                        $(this).dialog('close');
                                        if ($('#PaymentOptionType').is(':visible') && $('#PaymentOptionType').val() < 0) {
                                            $('#PaymentOptionType').after('<div class="error">Please select a payment method</div');
                                        } else {
                                            Aurora.Services.AuroraWebService.RequestChange($.auroraMember.memberCategories[0].MemberCategory.ID, $('#MemberType').val(), $('#PaymentOptionType').val(), memEvents.ChangeResponse, onError);
                                        };
                                    },
                                    "No": function () { $(this).dialog('close'); }
                                }
                            });
                            break;
                        }


                        $('<div>Your member profile has been successfully updated.</div>').dialog({
                        	modal: true,
							title: "Successful",
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                }
                            },
                            close: function () {
                                document.location = '/login.aspx';
                            }
                        });
                        break;
                    case 2:
                        $('<div>Your account has been successfully created. A site administrator will activate your account shortly.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                }
                            },
                            close: function () {
                                document.location = '/login.aspx';
                            }
                        }
                            );
                        break;
                    case 3:
                        var $paymentDialog = $('<div>Your account has been created. We will now redirect you to payment processing.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            close: function () {
                                document.location = "/processpayment.aspx";
                            },
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                }
                            }
                        });

                        break;
                    default:
                        var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
                        $(htmlMessage).dialog({
                            modal: true,
                            resizable: false,
                            title: "Error",
                            buttons: { "Okay": function () { $(this).dialog("close"); $('#RegistrationForm').show(); } }
                        });
                }
            }
        },
        renewalResponse: function (response) {
            if (response.Result === true) {
                switch (response.Count) {
                    case -1:
                        $('<div>' + response.Data + '</div>').dialog({
                            modal: true,
                            buttons: { "Ok": function () { $(this).dialog('close'); } },
                            close: function () { $('#RegistrationForm').show() }
                        });
                        break;
                    case 1:
                        var $paymentDialog = $('<div>We will now redirect you to payment processing.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            close: function () { document.location = "/processpayment.aspx"; },
                            buttons: { "Ok": function () { $(this).dialog('close'); }}
                        });
                        break;
                    case 2:
                        var $paymentDialog = $('<div>Congratulations, your subscription has been extended.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            buttons: { "Ok": function () { $(this).dialog('close'); $('#MemberProfileForm').show(); memMethods.getCategories(); } }
                        });
                        break;
                    default:
                        var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
                        $(htmlMessage).dialog({
                            modal: true,
                            resizable: false,
                            title: "Error",
                            buttons: { "Okay": function () { $(this).dialog("close"); $('#MemberProfileForm').show(); } }
                        });
                }
            }
        },
        ChangeResponse : function (response) {
            if (response.Result === true) {
                switch (response.Count) {
                    case -1:
                        $('<div>' + response.Data + '</div>').dialog({
                            modal: true,
                            buttons: { "Ok": function () { $(this).dialog('close'); } },
                            close: function () { $('#RegistrationForm').show() }
                        });
                        break;
                    case 1:
                        $('<div>Your category change was successful.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            buttons: {
                            	"Ok": function () {
                            		$(this).dialog('close');
                            		location.reload();
                                }
                            }
                        });
                        break;
                    case 2:
                        $('<div>Your category change was successful. A site adminsitrator will activate your account shortly.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                }
                            }
                        }
                            );
                        break;
                    case 3:
                        var $paymentDialog = $('<div>Your account was updated. We will now redirect you to payment processing.</div>').dialog({
                        	modal: true,
                        	title: "Successful",
                            close: function () {
                                document.location = "/processpayment.aspx";
                            },
                            buttons: {
                                "Ok": function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                        break;
                    default:
                        var htmlMessage = $("<div>Unfortunately the information you have provided cannot be processed. This may be due to your personal browser and/or corporate security settings. If you continue to encounter difficulties please contact us directly for assistance.</div>");
                        $(htmlMessage).dialog({
                            modal: true,
                            resizable: false,
                            title: "Error",
                            buttons: { "Okay": function () { $(this).dialog("close"); $('#MemberProfileForm').show(); } }
                        });
                }
            }
        },
        CategorySuccess: function (response) {
            if (response.Result) {
                var categories = $.auroraMember.memberCategories = response.Data;
                $('.categoryContainer').remove();
                for (var i = 0; i < categories.length; i++) {
                    $('#MemberType').val(categories[i].MemberCategory.RoleID);
                    var $container = $('<table class="categoryContainer"></table>');
                    $container.append('<tr><td class="name">' + categories[i].Category.Name + '</td><td class="label">Is Activated? <input type="checkbox" disabled="disabled" ' + (categories[i].MemberCategory.IsActivated ? "checked=true" : "") + '"/></td></tr>');
                    $container.append('<tr><td class="label">Registered On</td><td>' + (categories[i].MemberCategory.InsertedOn ? formatDateTime(categories[i].MemberCategory.InsertedOn, 'yyyy/MM/dd HH:mm') : "Never") + '</td></tr>');
                    if (categories[i].MemberCategory.CostAtPurchase > 0) {
                    	$container.append('<tr><td class="label">Paid On</td><td>' + (categories[i].MemberCategory.PaidOn ? formatDateTime(categories[i].MemberCategory.PaidOn, 'yyyy/MM/dd HH:mm') : "Never") + '</td></tr>');
                    }
                    if (categories[i].MemberCategory.CommencedOn) {
                    	$container.append('<tr><td class="label">Started On</td><td>' + (categories[i].MemberCategory.CommencedOn ? formatDateTime(categories[i].MemberCategory.CommencedOn, 'yyyy/MM/dd HH:mm') : "Never") + '</td></tr>');
                    	$container.append('<tr><td class="label">Expires On</td><td>' + (categories[i].MemberCategory.ExpiresOn ? formatDateTime(categories[i].MemberCategory.ExpiresOn, 'yyyy/MM/dd HH:mm') : "Never") + '</td></tr>');
                    }
                    if (categories[i].MemberCategory.LapsedOn) {
                    	$container.append('<tr><td class="label">Lasped On</td><td>' + (categories[i].MemberCategory.LapsedOn ? formatDateTime(categories[i].MemberCategory.LapsedOn, 'yyyy/MM/dd HH:mm') : "Never") + '</td></tr>');
                    }

                    if (!categories[i].IsPendingReplace) {
                        if (categories[i].CanRenew) {
                            $container.append('<tr><td colspan="2"><button class="Renew" type="button" dataid="' + i + '">Renew this registration</button></td></tr>');
                            $container.delegate('button.Renew', 'click.renewal', memEvents.renewalClick);
                        };
                        //$container.append('<tr><td colspan="2"><button class="Change" type="button" dataid="' + i + '">Change Category</button></td></tr>');
                        //$container.delegate('button.Change', 'click.changeCat', memEvents.changeClick);
                    } else {
                        $container.addClass('replacing');
                    }
                    if (categories[i].MemberCategory.LapsedOn) {
                        $container.addClass('lapsed');
                    } else if (categories[i].MemberCategory.IsActivated) {
                        $container.addClass('active');
                    } else {
                        $container.addClass('pending');
                    }
                    $('#CurrentCategoryInfo .infopanel').html($container);
                }
                $('#CurrentCategoryInfo').removeClass('loading');
                $('#CategoryInfo').hide();
            }
        },
        ChangePasswordSuccess: function (response) {
            if (response.Result) {
                $('#MemberProfileForm').unblock();
                if (response.Count === 0) {
                    var $dialog = $('<div>We were unable to change your password. Please log in and try again.</div>');
                    $dialog.dialog({
                        modal: true,
                        title: "Change Password",
                        buttons: {
                            Ok: function () { $(this).dialog('close');}
                        },
                        close: function () {
                            document.location = '/login.aspx';
                        }
                    })
                } else if (response.Count === -2) {
                    var $dialog = $('<div>The supplied old password does not match our records. Try again.</div>');
                    $dialog.dialog({
                        modal: true,
                        title: "Change Password",
                        buttons: {
                            Ok: function () { $(this).dialog('close'); }
                        },
                        close: function () {
                            memEvents.changePasswordClick();
                        }
                    })
                } else if (response.Count === 1) {
                    var $dialog = $('<div>Password changed successfully.</div>');
                    $dialog.dialog({
                        modal: true,
                        title: "Change Password",
                        buttons: {
                            Ok: function () { $(this).dialog('close'); }
                        }
                    })
                }
            }
        },
        //#endregion
        //#region Clicks
        renewalClick : function (event) {
            var $this = $(event.currentTarget),
                index = $this.attr('dataid'),
                memberCategory = $.auroraMember.memberCategories[index];

            var $dialog = $('<div></div>');

            if (memberCategory.Category.Cost > 0) {
                $dialog.append('Please select a payment type');
                $dialog.append($('#PaymentOptionType'));
                $dialog.dialog({
                    modal: true,
                    title: "Renew Category",
                    buttons: { "Ok": function () { $(this).dialog('close'); memMethods.requestRenewal(memberCategory.MemberCategory.ID); }}
                });
            } else {
                memMethods.requestRenewal(memberCategory.MemberCategory.ID);
            }
            
        },
        changeClick : function (event) {
            var $this = $(event.currentTarget),
                index = $this.attr('dataid'),
                memberCategory = $.auroraMember.memberCategories[index];
            
            var $dialog = $('<div>Are you sure you want to change registration type?</div>');
            if (memberCategory.Category.Duration > 0) {
                $dialog.append('You will lose any remaining time on this registration type');
            };
            $dialog.dialog({
                modal: true,
                title: "Change Registration Type",
                buttons: {
                    "Yes": function () { $(this).dialog('close'); memMethods.showCategorySelection(memberCategory.MemberCategory.ID); },
                    "No": function () { $(this).dialog('close'); }}
            });

        },
        submitChange: function (event) {
            if ($('#PaymentOptionType').is(':visible') && $('#PaymentOptionType').val() < 0) {
                $('#PaymentOptionType').after('<div class="error">Please select a payment method</div');
                return;
            } else {
                $('#MemberProfileForm').hide();
                Aurora.Services.AuroraWebService.RequestChange($(event.currentTarget).attr('dataid'), $('#MemberType').val(), $('#PaymentOptionType').val(), memEvents.ChangeResponse, onError);
            }
        },
        changePasswordClick: function (event) {
            var $dialog = $('<div>Please enter your new Password in the fields below</div>');
            var $table = $('<table class="newPasswordTable"></table>');
            $table.append('<tr><td>Old Password</td><td><input type="password" name="OldPass" id="OldPass" valtype="required"></input></td></tr>');
            $table.append('<tr><td>New Password</td><td><input type="password" name="NewPass" id="NewPass" valtype="required"></input></td></tr>');
            $table.append('<tr><td>Confirm Password</td><td><input type="password" name="ConfirmPass" id="ConfirmPass" valtype="required;mustmatch:NewPass"></input></td></tr>');
            $dialog.append($table);
            $dialog.dialog({
                modal: true,
                title: "Change Password",
                resizable: false,
                width: '500px',
                buttons: {
                    "Confirm": function () { memMethods.ChangePassword($(this)); },
                    "Cancel": function () { $(this).dialog('close'); $(this).remove(); }
                }
            });
            return false;
        },
        //#endregion
        RegisterError: function (response) {
            $('<div>There was an error trying to update you account: ' + response + '</div>').dialog({
                modal: true,
                resizable: false,
                title: "Error",
                buttons: {
                    "Ok": function () {
                        $(this).dialog('close');
                        $('#RegistrationForm').show();
                    }
                }
            });
        }
    }

    $.auroraMember = function (method) {
        // Method calling logic
        if (memMethods[method]) {
            return memMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return memMethods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.auroraRegister');
        }
    };

    $.auroraMember.member = {};
    $.auroraMember.memberCategories = [];

    $(function () {
        $.auroraMember();
    });

})(jQuery, window, document);