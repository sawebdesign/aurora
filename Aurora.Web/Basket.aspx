﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Basket.aspx.cs" Inherits="Aurora.Basket" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <style type="text/css">
        .processCheckout
        {
            float: right;
        }
        
        
        
        #MainBasket table
        {
            padding-top: 0px;
            border: none;
            border-collapse: collapse;
        }
        #MainBasket table tbody tr
        {
            border-top: 5px solid #fff;
        }
        #MainBasket table td
        {
            padding: 5px;
            margin-top: 5px;
        }
        .productQuantity input 
        {
            width: 30px;
            text-align: center;
        }
    </style>
    <div id="MainBasket">
    </div>
    <div id="inputcontent" style="display: none;" runat="server" clientidmode="Static"
        enableviewstate="True">
    </div>
    <br style="clear: both" />
    <div id="Gateways">
        <div id="GTitle">
            Please select a payment gateway:</div>
        <select id="PaymentSchemas" class="sys-template">
            <option sys:value="{{ID}}" class="paymentSchemaOption" sys:data-payment-schema-id="{{ID}}" />{{Description}}<br />
        </select>
		<!-- Recurring payment Template -->
		<script id="PaymentSchemasRecurringOptionsTmpl" type="text/x-jsrender">
			<option value="{{:MonthValue}}" data-recurring-interest="{{:recurringInterest}}" >{{:Month}}</option>
		</script>
		<!-- Temlate End -->
        <div id="PaymentSchemasRecurringOptions_Container" style="display: none">
			<span>Payment Term:</span>
			<select id="PaymentSchemasRecurringOptions" name="recurringOption"></select>
			<div style="padding-top: 5px;">You pay <span style="font-weight: bold;" id="PaymentSchemasRecurringOptions_Installment"></span></div>
        </div>
    </div>
    <div class="processCheckoutContainer">
		<input type="button" value="Continue Shopping" class="continueShopping" onclick="javascript: history.back();"/>
        <input type="button" value="Process Payment" class="processCheckout" onclick="AuroraJS.Modules.ProcessPayment();" />
    </div>
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="scripts/aurora.custom.js" type="text/javascript"></script> 

    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
	<script src="/scripts/plugins/boltaccordion.js" type="text/javascript"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
       
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <link href="styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
	<script src="/scripts/jsrender.js?" type="text/javascript"></script>
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
  
</asp:Content>
