﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora {
    public partial class CheckOut : Common.BasePage{
        private Services.AuroraWebService service = new Services.AuroraWebService();
		private AuroraEntities dbContext = new AuroraEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //when no member is detected redirect to login
			if (SessionManager.VistorSecureSessionID == Guid.Empty || SessionManager.MemberData == null) {
				Response.Redirect("~/login.aspx?ReturnUrl=/checkout.aspx");
            }
            else {
				List<dynamic> clientModules = (List<dynamic>)service.GetClientModules()["Data"];
				//if (clientModules.Where(mods => mods.featureModules.Name == "Shipping").Count() == 0) {

				//}

				if (clientModules.Where(mods => mods.featureModules.Name == "Voucher").Count() == 0) {
					Coupon.Visible = false;
				}


				IQueryable<ShippingModule> shippingM = from cshipm in dbContext.ClientShippingModule
													   join shipm in dbContext.ShippingModule on cshipm.ShippingModuleID equals shipm.ID
													   where cshipm.ClientSiteID == SessionManager.ClientSiteID
													   select shipm;
				ShippingMethod.Items.Clear();
				ShippingMethod.DataSource = shippingM;
				ShippingMethod.DataValueField = "ID";
				ShippingMethod.DataTextField = "Name";
				ShippingMethod.DataBind();
				ShippingMethod.Items.Insert(0, new ListItem("Select a Shipping Method", "0"));

				foreach (ShippingModule sm in shippingM) {
					switch (sm.Name) {
						case "RAM":
							RamShipping.Visible = true;
							break;
						case "SAPO":
							SAPOShipping.Visible = true;
							RenderSAPO();
							break;
					}
				}
            }


            
        }

		protected void RenderSAPO() {
			Aurora.Services.AuroraShippingService service = new Aurora.Services.AuroraShippingService();
			var countries = service.GetCountryList()["Data"];
			SAPOCountry.DataSource = countries;
			SAPOCountry.DataTextField = "Name";
			SAPOCountry.DataValueField = "ID";
			SAPOCountry.DataBind();
			SAPOCountry.Items.Insert(0, new ListItem("Select destination Country", "0"));
		}


    }
}