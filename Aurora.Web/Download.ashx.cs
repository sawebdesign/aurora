﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Aurora.Custom;

namespace Aurora
{
    /// <summary>
    /// Summary description for Download1
    /// </summary>
    public class Download1 : IHttpHandler,IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request["id"] != null)
            {
                bool encryptedUrl;
                bool.TryParse(context.Request["noenc"], out encryptedUrl);
                Utils.DownloadFile(context.Request["id"],encryptedUrl);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}