﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;
using System.Linq;

namespace Aurora {
    public partial class Donate : Aurora.Common.BasePage {
        private Custom.Data.AuroraEntities entities = new Custom.Data.AuroraEntities();
        protected void Page_Load(object sender, EventArgs e) {
            //get settings for this module
            var donateSettings = Utils.GetFieldsFromCustomXML("Donations");

            if (donateSettings.Count > 0) {
                if (!String.IsNullOrEmpty(donateSettings["Donation_TextBefore"])) {
                    DonateContent.InnerHtml = donateSettings["Donation_TextBefore"];
                }

                if (!String.IsNullOrEmpty(donateSettings["Donation_CMSZoneBefore"]) && donateSettings["Donation_CMSZoneBefore"] != "0") {
                    DonateContent.InnerHtml = donateSettings["Donation_TextBefore"];
                }

                long pageID;
                long.TryParse(donateSettings["Donation_CMSZoneBefore"], out pageID);

                var pageText = (from pages in entities.PageContent
                                where pages.ID == pageID
                                select pages).SingleOrDefault();

                if (pageText != null) {
                    var pageTemplate = (from modules in entities.Module
                                        join clientModules in entities.ClientModule on modules.ID equals clientModules.FeaturesModuleID
                                        where modules.Name == "PageDetail"
                                        && clientModules.ClientSiteID == SessionManager.ClientSiteID
                                        select new { modules, clientModules }).SingleOrDefault();

                    var template = !String.IsNullOrEmpty(pageTemplate.clientModules.HTML) ? pageTemplate.clientModules.HTML : pageTemplate.modules.HTML;
                    template= template.Replace("{{LongTitle}}", pageText.LongTitle);
                    template = template.Replace("{{PageContent}}",pageText.PageContent1);
                    template = template.Replace("{{Image}}", string.Empty);

                    DonateContent.InnerHtml = template;
                }

                // apply template

                // call js init
                //ClientScript.RegisterStartupScript(GetType(), "init", "initialisePage();", true);
            }
        }
    }
}