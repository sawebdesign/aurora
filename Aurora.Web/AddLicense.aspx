﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLicense.aspx.cs" Inherits="Aurora.AddLicense" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
    <table width="100%">
        <tr>
            <td>
                <h3>
                    License Selection</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Please choose the license you would like to purchase.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="radioBtnGroupLicense" class="sys-template">
                    <input sys:value="{{ID}}" name="License" sys:membername="{{Name}}" sys:price="{{Price == 0 ? 0 :Price}}"
                        type="radio" />{{Name}} - {{Price == 0 ? "Free!" :"R "+ Price }}<br />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>
                    Payment Method</h3>
            </td>
        </tr>
        <tr>
            <td>
                Please choose your preferred payment method
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="radioBtnGroupPayment" class="sys-template">
                    <input sys:value="{{payschema.PaymentSchemaID}}" sys:pay="{{schema.Name}}" onclick="$(this).attr('checked','checked');"
                        name="Payment" type="radio" /><label>{{schema.Name}}</label><br />
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
