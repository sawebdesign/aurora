﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Aurora.Custom;
using Aurora.Custom.Data;
using System.Linq;
using Aurora.Services;
using Aurora.UserControls;

namespace Aurora.Common {
    /// <summary>
    /// A custom base page class that sets the page Title, if needed.
    /// </summary>
    public class BasePage : System.Web.UI.Page {
        AuroraEntities auroraEntities = new AuroraEntities();
        protected override void OnPreInit(EventArgs e) {
            SetMasterPageFile();
            base.OnPreInit(e);
        }

        protected override void OnLoadComplete(EventArgs e) {
            //Dynamically insert user controls into page
            Aurora.UserControls.Menu menuControl= (Aurora.UserControls.Menu)LoadControl("~/UserControls/Menu.ascx");
			var menu = (HtmlGenericControl)Master.FindControl("Menu");
			if (menu.Attributes["data-newmenu"] != null) {
				menuControl.newMenu = (menu.Attributes["data-newmenu"].ToLower() == "true");
			}
			if (menu.Attributes["data-isresponsive"] != null) {
				menuControl.isResponsive = (menu.Attributes["data-isresponsive"].ToLower() == "true");
			}
			menu.Controls.Add(menuControl);
            var userControl = LoadControl("~/UserControls/LinksCreator.ascx");
            //Bind to page
            Master.Controls.Add(userControl);
            GetModulesJS(CacheManager.SiteData);

            base.OnLoadComplete(e);

        }

        /// <summary>
        /// Executes master page change
        /// </summary>
        protected virtual void SetMasterPageFile() {
            MasterPageFile = GetMasterPageFileFromSession();
            Page.Title = CacheManager.SiteData.CompanyName;
        }

        /// <summary>
        /// Determines what masterpage to load,which is determined by the requested domain.
        /// </summary>
        /// <returns>string File Path</returns>
        protected string GetMasterPageFileFromSession() {

            string domain = Request.ServerVariables["SERVER_NAME"];
            //Try to find clients for that domain
            var siteData = (from entites in auroraEntities.Domain
                            join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals clientSite.ClientSiteID
                            where entites.DomainName == domain
                            select clientSite).SingleOrDefault();

            if (Request["overide"] != null && Request["overide"] != "5455") {

                int ClientSiteID = int.Parse(Request["overide"]);
                SessionManager.ImpersonateID = ClientSiteID;
                siteData = (from entites in auroraEntities.Domain
                            join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals
                                clientSite.ClientSiteID
                            where entites.ClientSiteID == ClientSiteID
                            select clientSite).First();
            }
                //kills current override
            else if (Request["overide"] == "5455") {
                SessionManager.ImpersonateID = 0;
                SessionManager.ClientSiteID = 0;
                siteData = (from entites in auroraEntities.Domain
                            join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals clientSite.ClientSiteID
                            where entites.DomainName == domain
                            select clientSite).SingleOrDefault();
            } else if (SessionManager.ImpersonateID > 0 && SessionManager.ImpersonateID != 5455) {
                siteData = (from entites in auroraEntities.Domain
                            join clientSite in auroraEntities.ClientSite on entites.ClientSiteID equals
                                clientSite.ClientSiteID
                            where entites.ClientSiteID == SessionManager.ImpersonateID
                            select clientSite).First();
            }


            //if nothing return 404
            if (siteData == null)
                Response.Redirect("~/Errors/_errors/404.html");

            //Load Client Details and Template
            Custom.SessionManager.ClientSiteID = siteData.ClientSiteID;


            var templates = from sitetemplate in auroraEntities.SiteTemplates
                            where sitetemplate.ClientSiteID == Custom.SessionManager.ClientSiteID
                            select sitetemplate;

            //if template not found throw 404
            if (templates.Count() == 0) {
                Response.Redirect("~/Errors/_errors/404.html");
            }

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Unique", "var clientSiteID = " + SessionManager.ClientSiteID + ";", true);

            //set site data cache object 
            var clientData = (from clients in auroraEntities.ClientSite
                              where clients.ClientSiteID == SessionManager.ClientSiteID
                              select clients).SingleOrDefault();

            if (CacheManager.SiteData == null) {
                CacheManager.SiteData = clientData;
            }

            if (Request.Url.Query == string.Empty && Request.Path.ToUpper().Contains("DEFAULT.ASPX")) {
                return templates.First().MasterPageFilePath;
            }
            if (templates.Count() >= 2 && (Request.Url.Query != string.Empty || !Request.Path.ToUpper().Contains("DEFAULT.ASPX"))) {
                //if the template does not exists return the default
                var secondaryMaster = Utils.ConvertDBNull(templates.Where(master => master.ParentID != 0).First().MasterPageFilePath, templates.First().MasterPageFilePath);
                return secondaryMaster;
            }

            return templates.First().MasterPageFilePath;

        }

        protected void GetModulesJS(ClientSite clientData) {
            //vars
            string jsFilePath = string.Empty;
            string cssFilePath = string.Empty;

            if (clientData != null) {
                var clientModules = from clientmodule in auroraEntities.ClientModule
                                    join modules in auroraEntities.Module on clientmodule.FeaturesModuleID equals
                                        modules.ID
                                    where clientmodule.ClientSiteID == clientData.ClientSiteID
                                    && clientmodule.DeletedOn == null
                                    && clientmodule.DeletedBy == null
                                    select new { clientmodule, modules };
                bool analyticsOn = false;
                foreach (var clientModule in clientModules) {
                    //Load JS 
                    jsFilePath = string.IsNullOrEmpty(clientModule.clientmodule.JScriptFilePath)
                                     ? clientModule.modules.JScriptFilePath
                                     : clientModule.clientmodule.JScriptFilePath;
                    //if GA

                    if (clientModule.modules.Description == "Goolge Analytics" && clientModule.clientmodule.ConfigXML != null) {
                        if (XDocument.Parse((clientModule.clientmodule.ConfigXML)).Descendants("field").Count() > 0) {
                            analyticsOn = true;
                            ClientScript.RegisterClientScriptBlock(GetType(), "Analytics", "var gaCode='" + XDocument.Parse((clientModule.clientmodule.ConfigXML)).Descendants("field").First().Attribute("value").Value + "';", true);
                        }
                    }

                    //Load CSS
                    cssFilePath = string.IsNullOrEmpty(clientModule.clientmodule.StylesFilePath)
                                     ? clientModule.modules.StylesFilePath
                                     : clientModule.clientmodule.StylesFilePath;

                    if (!string.IsNullOrEmpty(jsFilePath)) {
                        HtmlGenericControl scriptFile = new HtmlGenericControl("script");
                        scriptFile.Attributes.Add("type", "text/javascript");
                        scriptFile.Attributes.Add("src", jsFilePath.Replace("~", string.Empty));
                        Controls.Add(scriptFile);
                    }

                    if (!string.IsNullOrEmpty(cssFilePath)) {
                        HtmlGenericControl cssFile = new HtmlGenericControl("link");
                        cssFile.Attributes.Add("rel", "stylesheet");
                        cssFile.Attributes.Add("type", "text/css");
                        cssFile.Attributes.Add("href", cssFilePath.Replace("~", string.Empty));
                        Controls.Add(cssFile);
                    }
                }

                if (!analyticsOn) {
                    ClientScript.RegisterClientScriptBlock(GetType(), "Analytics", "var gaCode='';", true);
                }
            }
        }

        #region Click Events overides
        protected void ExecuteLogin(object sender, EventArgs e) {
            AuroraEntities entity = new AuroraEntities();
            var pageControls = Master.FindControl("MainContent").FindControl("DoLogin").Controls;
            HtmlInputText userName = null;
            HtmlInputPassword password = null;
            Label ErrorMessage = null;
            SiteMemberArea SiteMemberArea1 = (SiteMemberArea)Master.FindControl("MainContent").FindControl("SiteMemberArea1");

            foreach (var pageControl in pageControls) {
                if (pageControl.GetType() == typeof(HtmlInputText)) {
                    if (((HtmlInputText)pageControl).ID == "UserName") {
                        userName = (HtmlInputText)pageControl;
                    }

                } else if (pageControl.GetType() == typeof(HtmlInputPassword)) {
                    password = (HtmlInputPassword)pageControl;
                } else if (pageControl.GetType() == typeof(Label)) {
                    ErrorMessage = (Label)pageControl;
                } else if (pageControl.GetType() == typeof(SiteMemberArea)) {
                    SiteMemberArea1 = (SiteMemberArea)pageControl;
                }
            }

            ErrorMessage.Text = "";

            var encryptedPass = Aurora.Security.Encryption.Encrypt(password.Value);
            var memberInfo = from members in entity.Member
                             where members.Email == userName.Value
                                   && members.Password == encryptedPass
                                   && members.isActive == true
                                   && members.ClientSiteID == SessionManager.ClientSiteID
                                   && members.DeletedOn == null
                                   && members.DeletedBy == null
                                   && members.isPending == false
                             select members;

            if (memberInfo.Count() > 0 && !string.IsNullOrEmpty(userName.Value) && !string.IsNullOrEmpty(password.Value)) {
                SiteMemberArea1.Visible = true;
                SessionManager.VistorSecureSessionID = Guid.NewGuid();
                SessionManager.MemberData = memberInfo.First();
                SiteMemberArea1.LoadProfile();
                if (Request["ReturnUrl"] != null) {
                    Response.Redirect(Server.UrlDecode(Request["ReturnUrl"]));
                } else {
                    Response.Redirect("~/login.aspx");
                }


            } else {
                SiteMemberArea1.Visible = false;
                ErrorMessage.Style.Add("color", "red");
                ErrorMessage.Text = "The user name & password combination do not match our records, please try again.";
            }
        }
        #endregion
    }
}
