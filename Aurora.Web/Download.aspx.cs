﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aurora.Custom;

namespace Aurora
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["id"] != null)
            {
                if (!Utils.DownloadFile(Request["id"]))
                    ClientScript.RegisterStartupScript(this.GetType(), "nofile", "<script>document.history(-1);</script>");
            }
            
        }

    }
}