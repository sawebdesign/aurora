﻿/// <reference path="/Scripts/jquery-1.5.2.js" />
/// <reference path="/Scripts/MicrosoftAjax.debug.js" />
/// <reference path="/Scripts/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Services/AuroraWS.asmx/js" />
/// <reference path="/Scripts/Utils.js" />
/// <reference path="/Scripts/jquery.tools.min.js" />
/// <reference path="/Scripts/common.js" /> 
/// <reference path="/Scripts/jquery.blockUI.js" />
/// <reference path="/Scripts/jquery-ui-1.8.9.custom.min.js" />
/// <reference path="/Scripts/jquery.tip.js" />
/// <reference path="/Scripts/jquery.history.js" />
/// <reference path="/Scripts/modules.js" />
/// <reference path="Scripts/Aurora.Custom.js" />

/*globals AuroraJS Aurora onError confirm jQuery window Sys MSAjaxExtension */

var timerID = 0, VAT, Total, STotal, Voucher;

$(function () {
	//Run this on page ready rather than JS ready to block interface earlier.
	$('.leftContent').block({ message: null });
});

$(document).bind("OnJSSessionReady", function () {
	//append click events
	$(document).click(function () {
		if ($("#SuburbsList").is(":visible")) {
			$("#SuburbsList").slideUp(550);
		}
	});

	$('#ShippingMethod').change(function (e) {
		var $this = $(e.currentTarget);
		$('.shippingModule').hide();
		$('#addressDetails').show();
		switch ($this.val()) {
			case "0":
				$('#RamShipping,#SAPOShipping,#addressDetails').hide();
				break;
			case "1":
				$('#RamShipping').show();
				break;
			case "2":
				$('#SAPOShipping').show();
				break;
		}
		
	});
	$('#SAPOCountry').change(function (e) {
		var countryID = $(e.currentTarget).val();
		if (countryID !== 0) {
			SAPO.GetQuote(countryID);
		}
	});


	Transaction.ProcessTransaction();
	
	if (AuroraJS.Bespoke.CheckOut) {
		AuroraJS.Bespoke.CheckOut();
	}

	var SAPO = {
		GetQuote: function (CountryID) {
			var response = function (result) {
				$("#IncludeShip").show();
				$("#QuoteField").show();
				$("#QuoteField").html("");

				//var quoteData = $.parseJSON(result.Data).DataSet["diffgr:diffgram"].Qoute.Qoute;
				if (result.Data > 0) {
					var shippingCurrency = GetJSSession("Currency_Symbol");
					renderQuote(result.Data);
				}
			};

			Aurora.Services.AuroraWebService.GetQuoteSAPO(CountryID, response, onError);
		}
	};

	
});


AuroraJS.Modules.GetTransactionDetails = function () {
	var response = function (result) {
		if (result.Data) {
			Total = Number(result.Data.TotalAmount);
			var xmlDoc = $.parseXML(result.Data.DataXML);
			var $xml = $(xmlDoc);
			var $fields = $xml.find('XmlData');

			$.each($fields, function (i, field) {
				VAT = Number($(field.childNodes[0]).attr("vat"));
				//console.log(VAT);
				Voucher = Number($(field.childNodes[0]).attr("voucheramount")).toFixed(2);
			});

			var currency = GetJSSession('Currency_Symbol') + ' ';
			//console.log(Total + '-' + VAT+ '-' + Voucher);
			$("#Total").html(currency + Total.toFixed(2));
			$("#STotal").html(currency + (Total - VAT).toFixed(2));
			$("#VAT").html(currency + VAT.toFixed(2));

			if (Voucher > 0) {
				$(".discountBox").show();
				$("#Total").html(currency + (Total + Number(Voucher)).toFixed(2));
				$("#STotal").html(currency + ((Total + Number(Voucher)) - VAT).toFixed(2));
				$("#VoucherDiscount").html(currency + Voucher);
				$("#VoucherTotal").html(String.format(currency + "{0}", (Total).toFixed(2)));
			}

		}
	};

	Aurora.Services.AuroraWebService.GetTransactionDetails(response, onError);
};
AuroraJS.Modules.validateVoucher = function (value, isCheckout) {STotal
	var response = function (result) {
		if (result.Data.toString().indexOf("Error:") > -1 && $("#VoucherCode").val() !== "") {
			$("<div>" + result.Data.replace("Error:", "") + "</div>").dialog(
            {
            	modal: true,
            	resizable: false,
            	title: "Error",
            	buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
		}
		else if (result.Data.indexOf("<ul>") > -1) {
			$("<div>" + result.Data + "</div>").dialog({
				modal: true,
				resizable: false,
				title: "Success",
				width: '400',
				buttons: {
					"Apply Voucher": function () {
						var returnData = function (voucherResult, oldDialog) {
							if (voucherResult.Data) {
								$(oldDialog).dialog("close");
								$("<div>" + voucherResult.Data.replace("Error:", "") + "</div>").dialog(
                                    {
                                    	modal: true,
                                    	resizable: false,
                                    	title: "Error",
                                    	buttons: { "Okay": function () { $(this).dialog("close"); } }
                                    });
							}
							else {
								$(oldDialog).dialog("close");
								$("<div>Voucher has been applied to your total amount.</div>").dialog(
                                    {
                                    	modal: true,
                                    	resizable: false,
                                    	title: "Success",
                                    	buttons: { "Okay": function () { $(this).dialog("close"); } }
                                    });
								AuroraJS.Modules.GetTransactionDetails();
							}
						};
						Aurora.Services.AuroraWebService.AddProductVoucherToTransaction($("#VoucherCode").val(), returnData, onError, this);
					},
					"Cancel": function () {
						$(this).dialog("close");
					}
				}
			});
		}

	};

	if ($("#VoucherCode").val().toString().length > 0) {
		Aurora.Services.AuroraWebService.ValidateProductVoucher($("#VoucherCode").val(), response, onError);
	}
	else {
		$("<div>Please enter the voucher code that you have been provided.</div>").dialog({
			modal: true,
			resizable: false,
			title: "Error",
			buttons: { "Okay": function () { $(this).dialog("close"); } }
		});
	}

};
AuroraJS.Modules.GetRamSuburbs = function () {

	var response = function (result) {
		if (!$("#SuburbsList").is(":visible")) {
			$("#SuburbsList").slideDown(550);
		}
		if ($("#SuburbsList").length === 0) {
			$("<div id='SuburbsList' style='position:absolute;background:#fff'></div>").appendTo(document.body);
		}

		$("#SuburbsList").offset({ top: 25 + $("#ShippingSuburb").offset().top, left: $("#ShippingSuburb").offset().left });
		$("#SuburbsList").css({ width: $("#ShippingSuburb").css("width"), border: "1px solid black" });

		if (result.Data.length === 0) {
			$("#SuburbsList").html("No items found");
			$("#suburbload").hide();
			return;
		}
		$("#SuburbsList").html("");

		for (var suburb = 0; suburb < result.Data.length; suburb++) {
			var item = result.Data[suburb].split("|");
			$(String.format("<div  id='{0}' code='{4}'>{1}{2}{3}</div>", item[0], item[1], " : " + item[2] || "", "(" + item[3] + ")", item[3])).appendTo("#SuburbsList");
		}
		$("#SuburbsList div").bind('click', function (e) {
			$("#IncludeShip").hide();
			$("#QuoteField").hide();
			if ($(e.currentTarget).attr("code") === "0") {
				$("#DeliveryType")[0].selectedIndex = 7;
			}
			else {
				$("#DeliveryType")[0].selectedIndex = 2;
			}

			$("#ShippingSuburb").attr("selected", $(e.currentTarget).attr("id"));
			$("#ShippingSuburb").val($(e.currentTarget).html());
		});

		$("#SuburbsList").slideDown(500, 'swing');
		$("#suburbload").hide();
	};

	if ($("#ShippingSuburb").val().toString().length >= 3 && $("#ShippingSuburb").val().toString().indexOf(":") === -1) {
		$("#suburbload").show();
		clearTimeout(timerID);
		timerID = setTimeout(function () {
			Aurora.Services.AuroraWebService.GetSuburb($("#ShippingSuburb").val(), 10, response, onError);
		}, 800);
	}
};
function getQuote() {

	var response = function (result) {
		$("#IncludeShip").show();
		$("#QuoteField").show();
		$("#QuoteField").html("");

		var quoteData = $.parseJSON(result.Data).DataSet["diffgr:diffgram"].Qoute.Qoute;
		if (quoteData.Bill_GrandTotal > 0) {
			var shippingCurrency = GetJSSession("Products_ShippingCurrency");
			if (shippingCurrency != undefined && shippingCurrency.length > 0 && $.CurrencyConverter) {

				$.CurrencyConverter('reverseConvertValue', quoteData.Bill_GrandTotal, shippingCurrency, renderQuote);
			}
			else {
				renderQuote(quoteData.Bill_GrandTotal);
			}
		}

	};

	if ($("#ShippingSuburb").attr("selected")) {
		$("#quoteLoad").show();
		Aurora.Services.AuroraWebService.GetQuoteRAM($("#DeliveryType").val(), $("#ShippingSuburb").attr("selected"), response, onError);
	}
	else {
		$("<div>Please enter the name of the suburb you would like the package to be delivered to.</div>").dialog(
            {
            	modal: true,
            	resizable: false,
            	title: "Error",
            	buttons: { "Okay": function () { $(this).dialog("close"); } }
            });
	}
}

function renderQuote(grandTotal) {
	$("#quoteLoad").hide();
	var currency = GetJSSession('Currency_Symbol') + ' ';
	$(".shippingBox").show();
	$("#ShippingTotal").html(currency + Number(grandTotal).toFixed(2));

	$("#TShipping").html((Number(grandTotal) + Total).toFixed(2));

	$("#TShipping").html(currency + $("#TShipping").html());
}

AuroraJS.Modules.AppendToTotal = function () {
	var response = function (result) {
		if (result.Data !== null && result.Count > 0) {
			document.location = "/ProcessPayment.aspx";
		}
	};
	switch ($('#ShippingMethod').val()) {
		case '1':
			Aurora.Services.AuroraWebService.AppendPriceToQuote(
				$("#DeliveryType").val(),
				$("#ShippingSuburb").attr("selected"),
				$('#AddressLine1').val(),
				$('#AddressLine2').val(),
				$('#AddressLine3').val(),
				$('#City').val(),
				$('#PostalCode').val(),
				response,
				onError);
			break;
		case '2':
			Aurora.Services.AuroraWebService.AppendSAPOPriceToQuote(
				$('#SAPOCountry').val(),
				$('#AddressLine1').val(),
				$('#AddressLine2').val(),
				$('#AddressLine3').val(),
				$('#City').val(),
				$('#PostalCode').val(),
				response,
				onError);
	}

};

AuroraJS.Modules.ProceedToPayment = function () {
	document.location = "/ProcessPayment.aspx";
};

var Transaction = {
	ProcessTransaction: function () {
		var basketData = $.cookie('productbasket');
		var basketRecurring = $.cookie('basketRecurringMonths');
		var basketSchema = $.cookie('basketSchema');
		if (basketData && basketSchema) {
			$('.leftContent').block({message:null});
			Aurora.Services.AuroraWebService.ProcessBasketOrder(basketData, "", basketSchema, basketRecurring, Transaction.ProcessTransactionSuccess, onError);
		} else {
			document.localName = "/basket.aspx";
		}
	},
	ProcessTransactionSuccess: function (result) {
		if (result.Count > 0) {
			if (result.Data && result.Data.Data) {
				$('.leftContent').unblock();
				if (!result.Data.Data.RequiresShipping) {
					$('#ShippingSection').html("No Shipping is required for your order<br/><br/>");
					//document.location = "/ProcessPayment.aspx";
					$('#NoShippingPayment').show();
				}
				
				Total = Number(result.Data.Data.TotalAmount);
				var xmlDoc = $.parseXML(result.Data.Data.DataXML);
				var $xml = $(xmlDoc);
				var $fields = $xml.find('XmlData');

				$.each($fields, function (i, field) {
					VAT = Number($(field.childNodes[0]).attr("vat"));
					//console.log(VAT);
					Voucher = Number($(field.childNodes[0]).attr("voucheramount")).toFixed(2);
				});

				var currency = GetJSSession('Currency_Symbol') + ' ';
				//console.log(Total + '-' + VAT+ '-' + Voucher);
				$("#Total").html(currency + Total.toFixed(2));
				$("#STotal").html(currency + (Total - VAT).toFixed(2));
				$("#VAT").html(currency + VAT.toFixed(2));

				if (Voucher > 0) {
					$(".discountBox").show();
					$("#Total").html(currency + (Total + Number(Voucher)).toFixed(2));
					$("#STotal").html(currency + ((Total + Number(Voucher)) - VAT).toFixed(2));
					$("#VoucherDiscount").html(currency + Voucher);
					$("#VoucherTotal").html(String.format(currency + "{0}", (Total).toFixed(2)));
				}

			}
		}

	}
}

var CheckoutError = function () {
	$('<div>An Error has occurred. Please refresh the page and try again</div>').dialog({ buttons: { 'OK': function () { } } });
};

