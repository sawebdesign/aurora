﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnSubscribe.aspx.cs" Inherits="Aurora.UnSubscribe" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1 class="unsubscribe-header" runat="server" id="headerPage"></h1>
    <div runat="server" id="divMessages"></div>
    <br />
    <div class="cmszone" runat="server" id="divCMSZone"></div>
    <table cellpadding="5" border="0" cellspacing="0" class="unsubscribe-table" runat="server" id="tblUnsubscribe">
        <tr>
            <td>Email:</td>
            <td><input type="text" id="inpEmail" runat="server" /></td>
        </tr>
        <tr>
            <td colspan="2">Please tell us why you wish to unsubscribe:</td>
        </tr>
        <tr class="reasons">
            <td></td>
            <td><asp:RadioButtonList ID="lstReasons" runat="server">
                </asp:RadioButtonList></td>
        </tr>
          <tr>
            <td valign="top">Other:</td>
            <td><asp:TextBox ID="txtOtherReason" runat="server" TextMode="MultiLine" Height="150" Width="100%" /></td>
        </tr>
        <tr>
            <td valign="top">Remove me from the following communication(s):</td>
            <td>
                <asp:CheckBoxList runat="server" ID="lstNewsletters">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button runat="server" ID="btnUnSubscribe" Text="UnSubscribe" OnClick="btnUnSubscribe_Click" /></td>
        </tr>
    </table>
    
    <script src="/scripts/jquery-1.5.2.js?" type="text/javascript"></script>
    <script src="/scripts/utils.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajax.js?" type="text/javascript"></script>
    <script src="/scripts/microsoftajaxtemplates.js?" type="text/javascript"></script>
    <script src="/services/auroraws.asmx/js" type="text/javascript"></script>
    <script src="scripts/aurora.custom.js" type="text/javascript"></script>
    <script src="/scripts/jquery.cycle.all.min.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.history.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.pagination.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/modules.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.11.custom.min.js?" type="text/javascript"></script>
    <script src="/scripts/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="/scripts/jquery.tip.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/common.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/jquery.blockui.js?" type="text/javascript" defer="defer"></script>
    <script src="/scripts/fancybox/jquery.fancybox-1.3.0.pack.js?" type="text/javascript"></script>
    <script src="/scripts/fancybox/jquery.mousewheel-3.0.2.pack.js?" type="text/javascript"
        defer="defer"></script>
    <link href="styles/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/calendarcontrol.js?" type="text/javascript"></script>
    <script src="/scripts/qaptcha.jquery.js?" type="text/javascript" defer="defer"></script>
    <!--superfish-->
    <script src="/scripts/hoverintent.js?" defer="defer" type="text/javascript"></script>
    <script src="/scripts/supersubs.js?" type="text/javascript"></script>
    <script src="/scripts/superfish.js?" type="text/javascript"></script>
    <script src="/scripts/jquery-cookie.js" type="text/javascript"></script>
    <link href="/styles/qaptcha.jquery.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/productbasket.js?" type="text/javascript"></script>
    <script type="text/javascript">
        AuroraJS.Modules.Members = true;
    </script>
</asp:Content>
