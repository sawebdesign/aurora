﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.ComponentModel;

namespace Aurora.Custom
{
    public class ImageFX
    {
        private Image originalImage;
        private Bitmap tempImage;
        string tempFileName, originalImagePath;
        public ImageFX(string imagePath)
        {
            if (!string.IsNullOrEmpty(imagePath))
            {
                //load image from file
                originalImagePath = imagePath;
                originalImage = Image.FromFile(imagePath);
                tempFileName = String.Format("{0}tmp_{1}{2}", imagePath.Substring(imagePath.LastIndexOf("\\")), Guid.NewGuid(), imagePath.Substring(imagePath.LastIndexOf(".")));
                File.Copy(imagePath, tempFileName);
                tempImage = new Bitmap(tempFileName);
            }
        }

        public bool Save()
        {
            originalImage.Dispose();            
            tempImage.Save(originalImagePath);
            return true;
        }
        public void Dispose()
        {
            originalImage.Dispose();
            tempImage.Dispose();

        }

        /// <summary>
        /// Applies an rgb color filter to an image
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        [Description("Colour Filter")]
        public void ApplyColorFilter(double r, double g, double b)
        {
            byte A, R, G, B;
            Color pixelColor;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;
                    R = (byte)(pixelColor.R * r);
                    G = (byte)(pixelColor.G * g);
                    B = (byte)(pixelColor.B * b);
                    tempImage.SetPixel(x, y, Color.FromArgb((int)A, (int)R, (int)G, (int)B));
                }
            }
        }

        /// <summary>
        /// Applies sepia effect to image
        /// </summary>
        /// <param name="depth"></param>
        [Description("Sepia")]
        public void ApplySepia(int depth)
        {
            int A, R, G, B;
            Color pixelColor;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;
                    R = (int)((0.299 * pixelColor.R) + (0.587 * pixelColor.G) + (0.114 * pixelColor.B));
                    G = B = R;

                    R += (depth * 2);
                    if (R > 255)
                    {
                        R = 255;
                    }
                    G += depth;
                    if (G > 255)
                    {
                        G = 255;
                    }

                    tempImage.SetPixel(x, y, Color.FromArgb(A, R, G, B));
                }
            }
        }

        /// <summary>
        /// Apply gray scale effect
        /// </summary>
        [Description("Gray Scale")]
        public void ApplyGreyscale()
        {
            byte A, R, G, B;
            Color pixelColor;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;
                    R = (byte)((0.299 * pixelColor.R) + (0.587 * pixelColor.G) + (0.114 * pixelColor.B));
                    G = B = R;

                    tempImage.SetPixel(x, y, Color.FromArgb((int)A, (int)R, (int)G, (int)B));
                }
            }

        }

        /// <summary>
        /// Decrease colour saturation
        /// </summary>
        /// <param name="offset"></param>
        [Description("Colour Depth")]
        public void ApplyDecreaseColourDepth(int offset)
        {
            int A, R, G, B;
            Color pixelColor;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;
                    R = ((pixelColor.R + (offset / 2)) - ((pixelColor.R + (offset / 2)) % offset) - 1);
                    if (R < 0)
                    {
                        R = 0;
                    }
                    G = ((pixelColor.G + (offset / 2)) - ((pixelColor.G + (offset / 2)) % offset) - 1);
                    if (G < 0)
                    {
                        G = 0;
                    }
                    B = ((pixelColor.B + (offset / 2)) - ((pixelColor.B + (offset / 2)) % offset) - 1);
                    if (B < 0)
                    {
                        B = 0;
                    }
                    tempImage.SetPixel(x, y, Color.FromArgb(A, R, G, B));
                }
            }

        }

        /// <summary>
        /// increase image brightness
        /// </summary>
        /// <param name="brightness"></param>
        [Description("Colour Brightness")]
        public void ApplyBrightness(int brightness)
        {
            int A, R, G, B;
            Color pixelColor;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;
                    R = pixelColor.R + brightness;
                    if (R > 255)
                    {
                        R = 255;
                    }
                    else if (R < 0)
                    {
                        R = 0;
                    }

                    G = pixelColor.G + brightness;
                    if (G > 255)
                    {
                        G = 255;
                    }
                    else if (G < 0)
                    {
                        G = 0;
                    }

                    B = pixelColor.B + brightness;
                    if (B > 255)
                    {
                        B = 255;
                    }
                    else if (B < 0)
                    {
                        B = 0;
                    }

                    tempImage.SetPixel(x, y, Color.FromArgb(A, R, G, B));
                }
            }

        }

        /// <summary>
        /// Apply Contrast
        /// </summary>
        /// <param name="contrast"></param>
        [Description("Contrast")]
        public void ApplyContrast(double contrast)
        {
            double A, R, G, B;

            Color pixelColor;

            contrast = (100.0 + contrast) / 100.0;
            contrast *= contrast;

            for (int y = 0; y < tempImage.Height; y++)
            {
                for (int x = 0; x < tempImage.Width; x++)
                {
                    pixelColor = tempImage.GetPixel(x, y);
                    A = pixelColor.A;

                    R = pixelColor.R / 255.0;
                    R -= 0.5;
                    R *= contrast;
                    R += 0.5;
                    R *= 255;

                    if (R > 255)
                    {
                        R = 255;
                    }
                    else if (R < 0)
                    {
                        R = 0;
                    }

                    G = pixelColor.G / 255.0;
                    G -= 0.5;
                    G *= contrast;
                    G += 0.5;
                    G *= 255;
                    if (G > 255)
                    {
                        G = 255;
                    }
                    else if (G < 0)
                    {
                        G = 0;
                    }

                    B = pixelColor.B / 255.0;
                    B -= 0.5;
                    B *= contrast;
                    B += 0.5;
                    B *= 255;
                    if (B > 255)
                    {
                        B = 255;
                    }
                    else if (B < 0)
                    {
                        B = 0;
                    }

                    tempImage.SetPixel(x, y, Color.FromArgb((int)A, (int)R, (int)G, (int)B));
                }
            }

        }

    }
}
