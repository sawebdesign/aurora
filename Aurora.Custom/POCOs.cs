﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aurora.Custom;
using Aurora.Custom.Data;

namespace Aurora.Custom {
    public class BasketLine {
        public long ID { get; set; }
        public long? Colour { get; set; }
        public long? Quantity { get; set; }
        public long? Size { get; set; }
    }
    public class TIDelegate {
        public string firstName { get; set; }
        public string surname { get; set; }
        public string emailAddress { get; set; }
        public string position { get; set; }
        public string dietaryRequirements { get; set; }
        public string mainContactNo { get; set; }
    };

    public class SiteMapWithRole: Aurora.Custom.Data.SiteMap {
        //public SiteMap sites { get; set; }
        public virtual List<Roles> roles { get; set; }
    }

    public class Link {
        public string Title { get; set; }
        public string URL { get; set; }
    }

}
