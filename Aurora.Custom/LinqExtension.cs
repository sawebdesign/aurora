﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Dynamic;
using System.Data;

namespace Aurora.Custom {

#region PageQuery
    public static class PageQuery {
        public static IQueryable<T> Paginate<T>(
            this IQueryable<T> query, int page, int pageSize) {
            int skip = Math.Max(pageSize * (page - 1), 0);
            query.Count();
            return query.Skip(skip).Take(pageSize);
        }
        public static IEnumerable<T> Paginate<T>(
            this IEnumerable<T> query, int page, int pageSize) {
            int skip = Math.Max(pageSize * (page - 1), 0);
            query.Count();
            return query.Skip(skip).Take(pageSize);
        }
    }
#endregion

#region SetModified
    public static class SetModified {
        public static void SetAllModified<T>(this T entity, System.Data.Objects.ObjectContext context) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);
            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);
            foreach (var propName in propertyNameList) {
                stateEntry.SetModifiedProperty(propName);
            }
        }

        /// <summary>
        /// Updates an existing entity's properties in the given object context manager.
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entity">Entity that the changes will be applied to</param>
        /// <param name="otherEntity">Entity that the values will be drawn from </param>
        /// <param name="context">Current object context</param>
        /// <param name="PropertyList">An array of property names that will be included(default) or excluded if Exclude is set to true</param>
        /// <param name="Exclude">If true, excludes properties from modification</param>
        public static void SetAllModified<T>(this T entity, T otherEntity,System.Data.Objects.ObjectContext context, string[] PropertyList, bool Exclude = false) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);
            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);
            if (Exclude) {
                propertyNameList = propertyNameList.Where(w => !PropertyList.Contains(w));
            }
            else {
                propertyNameList = propertyNameList.Where(w => propertyNameList.Contains(w));
            }
            foreach (var propName in propertyNameList) {
                PropertyInfo propInfo = entity.GetType().GetProperty(propName);
                if (propInfo.GetValue(entity, null) != propInfo.GetValue(otherEntity, null)) {
                    propInfo.SetValue(entity, propInfo.GetValue(otherEntity, null), null);
                }
                stateEntry.SetModifiedProperty(propName);
            }
        }
    }
#endregion

#region DynamicDataRow
    public class DynamicDataRow : DynamicObject
    {
        /// <summary>
        /// Creates a dynamic wrapper for datarows, allowing convenient access to data members.
        /// </summary>
        private DataRow _dataRow;

        public DynamicDataRow(DataRow dataRow)
        {
            if (dataRow == null)
                throw new ArgumentNullException("dataRow");
            this._dataRow = dataRow;
        }

        public DataRow DataRow
        {
            get { return _dataRow; }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            if (_dataRow.Table.Columns.Contains(binder.Name))
            {
                result = _dataRow[binder.Name];
                return true;
            }
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (_dataRow.Table.Columns.Contains(binder.Name))
            {
                _dataRow[binder.Name] = value;
                return true;
            }
            return false;
        }
    }

    public static class DynamicDataRowExtensions
    {
        public static dynamic AsDynamic(this DataRow dataRow)
        {
            return new DynamicDataRow(dataRow);
        }
    }
#endregion

#region String Comparer
    public static class ContainsStringComparer {
        public static bool Contains(this string source, string toCheck, StringComparison comp) {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
#endregion
}

