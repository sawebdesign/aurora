﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Aurora.Custom.Providers.Shipping {
    public class RAMCouriers {
        #region Properties
        private string APIUserName { get; set; }
        private string APIPassword { get; set; }
        private string AuthToken { get; set; }
        AuroraREST RestRequest = new AuroraREST();

        #endregion

        #region Constructor
        /// <summary>
        /// Performs a login to the API and retrieves a secure token
        /// </summary>
        /// <param name="userName">User name provided by RAM Couriers</param>
        /// <param name="password">Password provided by RAM Couriers</param>
        public RAMCouriers(string userName, string password) {
            APIUserName = userName;
            APIPassword = password;
            AuthToken = PerformLogin();
        }

        /// <summary>
        /// Logons onto the API
        /// </summary>
        /// <returns>Api token</returns>
        private string PerformLogin() {

            RestRequest.ServiceURL = new Uri("http://services.ramgroup.co.za/ramconnectv2/Tracking/TrackingWS.asmx/Logon");
            RestRequest.AddParameter("userID", APIUserName);
            RestRequest.AddParameter("password", APIPassword);

            return RestRequest.InvokeMethod()
                .Descendants(XName.Get("string", "http://tempuri.org/")).First().Value;

        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a list of possible suburbs.
        /// </summary>
        /// <param name="filter">text to search for</param>
        /// <param name="recordCount">limits the amount of records returned</param>
        /// <returns></returns>
        public List<string> SuburbSearch(string filter, int recordCount) {

            RestRequest.ServiceURL = new Uri("http://services.ramgroup.co.za/ramconnectv2/Tracking/TrackingWS.asmx/SuburbSearch");
            RestRequest.AddParameter("userID", APIUserName);
            RestRequest.AddParameter("logonToken", AuthToken);
            RestRequest.AddParameter("filter", filter);
            RestRequest.AddParameter("maxRecords", recordCount.ToString());

            return RestRequest.InvokeMethod()
                   .Descendants(XName.Get("string", "http://tempuri.org/"))
                   .Select(token => token.Value).ToList<string>();

        }

        /// <summary>
        /// Gets a quote on an item
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="logonToken"></param>
        /// <param name="billedTo"></param>
        /// <param name="serviceTypeCode"></param>
        /// <param name="senderZoneID"></param>
        /// <param name="receiverZoneID"></param>
        /// <param name="saturdaySurcharge"></param>
        /// <param name="afterHoursSurcharge"></param>
        /// <param name="insured"></param>
        /// <param name="insuredValue"></param>
        /// <param name="armouredVehicle"></param>
        /// <param name="faceToFaceSurcharge"></param>
        /// <param name="weight"></param>
        /// <param name="length"></param>
        /// <param name="breadth"></param>
        /// <param name="height"></param>
        /// <param name="noOfCards"></param>
        /// <returns></returns>
        public XDocument GetQuoteV3(string billedTo,
                                        string serviceTypeCode,
                                        string senderZoneID,
                                        string receiverZoneID,
                                        bool saturdaySurcharge,
                                        bool afterHoursSurcharge,
                                        bool insured,
                                        double insuredValue,
                                        bool armouredVehicle,
                                        bool faceToFaceSurcharge,
                                        double weight,
                                        double length,
                                        double breadth,
                                        double height,
                                        int noOfCards) {

            RestRequest.AddParameter("userID", APIUserName);
            RestRequest.AddParameter("logonToken", AuthToken);
            RestRequest.AddParameter("billedTo", billedTo);
            RestRequest.AddParameter("serviceTypeCode", serviceTypeCode);
            RestRequest.AddParameter("senderZoneID", senderZoneID);
            RestRequest.AddParameter("receiverZoneID", receiverZoneID);
            RestRequest.AddParameter("saturdaySurcharge", saturdaySurcharge.ToString());
            RestRequest.AddParameter("afterHoursSurcharge", afterHoursSurcharge.ToString());
            RestRequest.AddParameter("insured", insured.ToString());
            RestRequest.AddParameter("insuredValue", insuredValue.ToString());
            RestRequest.AddParameter("armouredVehicle", armouredVehicle.ToString());
            RestRequest.AddParameter("faceToFaceSurcharge", faceToFaceSurcharge.ToString());
            RestRequest.AddParameter("weight", weight.ToString());
            RestRequest.AddParameter("length", length.ToString());
            RestRequest.AddParameter("breadth", breadth.ToString());
            RestRequest.AddParameter("height", height.ToString());
            RestRequest.AddParameter("noOfCards", noOfCards.ToString());

            RestRequest.ServiceURL = new Uri("http://services.ramgroup.co.za/ramconnectv2/Tracking/TrackingWS.asmx/GetQuoteV3");

            return RestRequest.InvokeMethod();

        }

        #endregion
    }
}
