﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Provider;
using System.Web.Security;
using System.Web;

namespace Aurora.Security {
    class AuroraMembershipUser : MembershipUser {
        private int? _LoginAttemptCounter;

        public int? LoginAttemptCounter {
            get {
                return _LoginAttemptCounter;
            }
            set {
                _LoginAttemptCounter = value;
            }
        }

        public AuroraMembershipUser(string providername, 
                                    string username, 
                                    object providerUserKey, 
                                    string email, 
                                    string passwordQuestion, 
                                    string comment,
                                    bool isApproved, 
                                    bool isLockedOut, 
                                    DateTime creationDate, 
                                    DateTime lastLoginDate, 
                                    DateTime lastActivityDate, 
                                    DateTime lastPasswordChangedDate,
                                    DateTime lastLockedOutDate, 
                                    int loginAttemptCounter) : base(providername, username, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockedOutDate) {

                    this.LoginAttemptCounter = loginAttemptCounter;
        }
    }
}
