﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Provider;
using System.Web.Security;
using System.Web;
using Aurora.Custom.Data;

namespace Aurora.Security {
    class AuroraRoleProvider : RoleProvider {

        public override string ApplicationName {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames) {

            foreach (string username in usernames) {
                long userID = 0;
                long roleID = 0;

                AuroraEntities service = new AuroraEntities();
                
                var resultUser = (from user in service.Users
                                  where user.UserName == username
                                  select user).SingleOrDefault();
                userID = resultUser.ID;

                foreach (string userrole in roleNames) {
                    var resultRole = (from role in service.Roles
                                      where role.RoleName == userrole
                                      select role).SingleOrDefault();
                    roleID = resultRole.ID;

                    UserRole userRole = new UserRole();
                    userRole.RoleID = roleID;
                    userRole.UserID = userID;
                    service.AddToUserRole(userRole);
                    service.SaveChanges();
                }
            }
           
        }

        public override void CreateRole(string roleName) {
            AuroraEntities service = new AuroraEntities();
            Aurora.Custom.Data.Roles role = new Custom.Data.Roles();

            role.RoleName = roleName;
            role.InsertedOn = DateTime.Now;
            role.InsertedBy = Custom.SessionManager.UserID;

            service.AddToRoles(role);
            service.SaveChanges();

        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole) {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch) {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles() {
            AuroraEntities service = new AuroraEntities();
            var results = from roles in service.Roles
                          select roles.RoleName;

            return results.ToArray();
        }

        public override string[] GetRolesForUser(string username) {
            if (Aurora.Custom.SessionManager.UserRoles == null) {
                AuroraEntities service = new AuroraEntities();

                var results = from roles in service.Roles
                              join userrole in service.UserRole on roles.ID equals userrole.RoleID
                              join user in service.Users on userrole.UserID equals user.ID
                              where user.UserName == username
                              select roles;
                Aurora.Custom.SessionManager.UserRoles = results.ToList();
                return results.Select(p => p.RoleName).ToArray();
            } else {
                return Aurora.Custom.SessionManager.UserRoles.Select(p => p.RoleName).ToArray();
            }
        }

        public override string[] GetUsersInRole(string roleName) {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName) {
            if (Aurora.Custom.SessionManager.UserRoles != null) {

                var isUser = from ur in Aurora.Custom.SessionManager.UserRoles
                             where ur.RoleName == roleName
                             select ur;

                if (isUser != null) {
                    return true;
                }
            }
            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames) {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName) {
            AuroraEntities service = new AuroraEntities();
            var results = from roles in service.Roles
                          select roles.RoleName;
            
            if (results == null) {
                return false;
            }
            
            return true;
        }
    }
}
