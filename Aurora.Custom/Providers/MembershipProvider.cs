﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Provider;
using System.Web.Security;
using System.Web;
using Aurora.Custom.Data;

namespace Aurora.Security {
    public class AuroraMembershipProvider : MembershipProvider {

        #region Fields
        private string _applicationName;
        private int _maxInvalidPasswordAttempts;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _enablePasswordRetrieval;
        private bool _enablePasswordReset;
        private MembershipPasswordFormat _passwordFormat;
        #endregion

        /// <summary>
        /// Performs initialization of the membership provider based on customized requirements from the web.config membership settings
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
            base.Initialize(name, config);
            _applicationName = GetValueFromConfigFile(config["applicationName"], "");
            _maxInvalidPasswordAttempts = Convert.ToInt32(GetValueFromConfigFile(config["maxInvalidPasswordAttempts"], "3"));
            _minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetValueFromConfigFile(config["minRequiredNonAlphanumericCharacters"], "1"));
            _minRequiredPasswordLength = Convert.ToInt32(GetValueFromConfigFile(config["minRequiredPasswordLength"], "6"));
            _passwordStrengthRegularExpression = GetValueFromConfigFile(config["passwordStrengthRegularExpression"], "");
            _requiresQuestionAndAnswer = Convert.ToBoolean(GetValueFromConfigFile(config["requiresQuestionAndAnswer"], "true"));
            _enablePasswordRetrieval = Convert.ToBoolean(GetValueFromConfigFile(config["enablePasswordRetrieval"], "true"));
            _enablePasswordReset = Convert.ToBoolean(GetValueFromConfigFile(config["enablePasswordReset"], "true"));
            _passwordFormat = MembershipPasswordFormat.Encrypted;
        }

        /// <summary>
        /// Checks if configuration file value is null, if it is, then this method will return the default value
        /// </summary>
        /// <param name="configValue">Value from config file</param>
        /// <param name="defaultValue">Default value to return if config value is null</param>
        /// <returns>Returns string containing either config or default value</returns>
        private string GetValueFromConfigFile(string configValue, string defaultValue) {
            if (string.IsNullOrEmpty(configValue)) {
                return defaultValue;
            }
            return configValue;
        }

        /// <summary>
        /// Get/Set Application Name
        /// </summary>
        public override string ApplicationName {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword) {
            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.UserName == username
                          select user).SingleOrDefault();

            if (Encryption.Decrypt(result.Password) == oldPassword) {

                result.Password = Encryption.Encrypt(newPassword);
                result.LastPasswordChangedDate = DateTime.Now;
                service.SaveChanges();

                return true;
            }
            return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer) {
            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.UserName == username
                          select user).SingleOrDefault();

            if (Encryption.Decrypt(result.Password) == password) {

                result.PasswordQuestion = newPasswordQuestion;
                result.PasswordAnwser = newPasswordAnswer;
                service.SaveChanges();

                return true;
            }
            return false;
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status) {
            throw new NotImplementedException();
        }

        public bool CreateNewUser(int clientsiteid, string firstname, string lastname, string username, string password, string email, string phone, string fax, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey) {

            Users user = new Users { ClientSiteID = clientsiteid, FirstName = firstname, LastName = lastname, UserName = username, Password = Encryption.Encrypt(password), Email = email, Phone = phone, Fax = fax, PasswordQuestion = passwordQuestion, IsLocked = isApproved };

            AuroraEntities service = new AuroraEntities();
            service.AddToUsers(user);
            service.SaveChanges();

            //TODO: Add roles

            return true;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData) {
            
            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.UserName == username
                          select user).SingleOrDefault();

            result.DeletedBy = (long)Custom.SessionManager.UserID;
            result.DeletedOn = DateTime.Now;
            service.SaveChanges();

            //TODO: Delete roles

            return true;

        }

        public override bool EnablePasswordReset {
            get { return _enablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval {
            get { return _enablePasswordRetrieval; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline() {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer) {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline) {
            
            AuroraMembershipUser memebershipUser = null;
            if (string.IsNullOrEmpty(username)) {
                return memebershipUser;
            }
            
            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.UserName == username
                          select user).SingleOrDefault();

            if (result == null)
                return memebershipUser;

            return new AuroraMembershipUser("Aurora.Security.AuroraMembershipProvider", result.UserName, null, result.Email, "", "", true, (bool)result.IsLocked, (DateTime)result.InsertedOn, (DateTime)result.LastLoginDate.GetValueOrDefault(), (DateTime)result.LastActivityDate.GetValueOrDefault(), (DateTime)result.LastPasswordChangedDate.GetValueOrDefault(), (DateTime)result.LastLockedOutDate.GetValueOrDefault(), 0);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline) {
            AuroraMembershipUser memebershipUser = null;
            if (providerUserKey == null) {
                return memebershipUser;
            }

            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.ID == (int)providerUserKey
                          select user).SingleOrDefault();

            if (result == null)
                return memebershipUser;

            return new AuroraMembershipUser("Aurora.Security.AuroraMembershipProvider", result.UserName, null, result.Email, "", "", true, (bool)result.IsLocked, (DateTime)result.InsertedOn, (DateTime)result.LastLoginDate, (DateTime)result.LastActivityDate, (DateTime)result.LastPasswordChangedDate, (DateTime)result.LastLockedOutDate, 0);
        }

        public override string GetUserNameByEmail(string email) {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts {
            get { return _maxInvalidPasswordAttempts; }
        }

        public override int MinRequiredNonAlphanumericCharacters {
            get { return _minRequiredNonAlphanumericCharacters; }
        }

        public override int MinRequiredPasswordLength {
            get { return _minRequiredPasswordLength; }
        }

        public override int PasswordAttemptWindow {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat {
            get { return _passwordFormat; }
        }

        public override string PasswordStrengthRegularExpression {
            get { return _passwordStrengthRegularExpression; }
        }

        public override bool RequiresQuestionAndAnswer {
            get { return _requiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer) {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName) {

            AuroraEntities service = new AuroraEntities();

            var results = (from user in service.Users
                          where user.UserName == userName
                           select user).SingleOrDefault();

            results.IsLocked = false;
            service.SaveChanges();
            return true;
        }

        public override void UpdateUser(MembershipUser user) {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password) {
            bool isValid = false;

            MembershipUser membershipUser = GetUser(username, false);
            if (membershipUser == null)
                return false;

            if (membershipUser.IsLockedOut) {
                return false;
            }

            AuroraEntities service = new AuroraEntities();
            var result = (from user in service.Users
                          where user.UserName == username
                          select user).SingleOrDefault();

            if (result == null)
                return false;

            if (result == null) {

            } else {
                if (result.IsLocked.GetValueOrDefault(false)) {
                    return false;
                }
                if (Encryption.Decrypt(result.Password) != password) {
                    isValid = false;
                    int loginAttempts = Aurora.Custom.SessionManager.LoginAttemptCounter == null ? Aurora.Custom.SessionManager.LoginAttemptCounter : 0;
                    loginAttempts += 1;
                    Aurora.Custom.SessionManager.LoginAttemptCounter = loginAttempts;
                    if (loginAttempts >= MaxInvalidPasswordAttempts) {
                        result.IsLocked = true;
                    }
                } else {
                    isValid = true;
                    result.LastLoginDate = DateTime.Now;
                    Custom.SessionManager.LoginAttemptCounter = 0;
                    Custom.SessionManager.isloggedIn = true;
                    Custom.SessionManager.UserID = result.ID;
                    Custom.SessionManager.UserName = username;
                    Custom.SessionManager.FirstName = result.FirstName;
                    Custom.SessionManager.LastName = result.LastName;
                    Custom.SessionManager.ClientSiteID = result.ClientSiteID;
                    Custom.SessionManager.LoggedInClientSiteID = result.ClientSiteID;
                    Custom.SessionManager.ClientBasePath = string.Format("~/ClientData/{0}",result.ClientSiteID);
                    HttpContext.Current.Session.Timeout = 60;
                    AuroraRoleProvider roles = new Security.AuroraRoleProvider();
                    roles.GetRolesForUser(username);
                    Custom.SessionManager.SiteExcludedRoles = service.GetUserSiteExcludedRoles(result.ClientSiteID, result.ID).ToList();
                }
                service.SaveChanges();
            }
            return isValid;
        }
    }
}
