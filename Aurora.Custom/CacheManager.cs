﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Aurora.Custom.Data;

namespace Aurora.Custom {
    public class CacheManager : SAWD.WebManagers.CacheManagerBase {
        private static AuroraEntities service = new AuroraEntities();

        /// <summary>
        /// Stores the current clients information
        /// </summary>        
        public static Data.ClientSite SiteData {
            get {
                Data.ClientSite retval = (Data.ClientSite)(GetCache("SiteData" + SessionManager.ClientSiteID));
                return retval;
            }
            set {
                SetCache("SiteData" + SessionManager.ClientSiteID, value, new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        ///  stores menu items for secure menu
        /// </summary>
        public static string SecureMenu {
            get {
                string retVal = (string)(GetCache("SecureMenu" + SessionManager.VistorSecureSessionID));
                return retVal;
            }
            set {

                SetCache("SecureMenu" + SessionManager.VistorSecureSessionID, value, new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        ///  all 301 redirects for SEO
        /// </summary>
        public static List<Data.C301Redirect> Redirect301 {
            get {
				List<Data.C301Redirect> redirlist = (List<Data.C301Redirect>)(GetCache("Redirect301"));
                if (redirlist == null) {
					using (Aurora.Custom.Data.AuroraEntities dataContext = new AuroraEntities()) {
						redirlist = dataContext.C301Redirect.ToList();
					}
                }
                SetCache("Redirect301", redirlist, new TimeSpan(0, 1, 0));
                return redirlist;
            }
            set {
                SetCache("Redirect301", value, new TimeSpan(0, 1, 0));
            }
        }

        /// <summary>
        ///  all domains in the system used for 301 redirects
        /// </summary>
        public static List<Data.Domain> Domains {
            get {
				List<Data.Domain> dlist = (List<Data.Domain>)(GetCache("Domain"));
                if (dlist == null) {
                    dlist = (from r in service.Domain
                                 select r).ToList();
                }
				SetCache("Domain", dlist, new TimeSpan(0, 5, 0));
                return dlist;
            }
            set {
				SetCache("Domain", value, new TimeSpan(0, 5, 0));
            }
        }

        ///// <summary>
        ///// Stores the site vistors information if they have logged in.
        ///// </summary>
        //public static Data.Member MemberData {
        //    get {
        //        Data.Member retval = (Data.Member)(GetCache("MemberData" + HttpContext.Current.Session.SessionID));
        //        return retval;
        //    }
        //    set {
        //        SetCache("MemberData" + HttpContext.Current.Session.SessionID, value, new TimeSpan(0, 30, 0));
        //    }
        //}

        ///// <summary>
        ///// Temporarily stores site visitors purchases for processing.
        ///// </summary>
        //public static List<Data.TransactionLog> CurrentTransactions {
        //    get {
        //        List<Data.TransactionLog> returnVal = (List<Data.TransactionLog>)(GetCache("CurrentTransactions" + SessionManager.VistorSecureSessionID));
        //        return returnVal;
        //    }
        //    set {
        //        SetCache("CurrentTransactions" + SessionManager.VistorSecureSessionID, value, new TimeSpan(0, 30, 0));
        //    }
        //}


    }
}
