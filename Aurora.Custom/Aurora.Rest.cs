﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Net;

namespace Aurora.Custom {
    /// <summary>
    /// Author      : Wesley Perumal
    /// Date        : 07-03-2012
    /// Description : Basic interrogation of any restful web service and returns response a querable Xdocument
    /// </summary>
    public class AuroraREST {

        #region Properties
        /// <summary>
        /// the full service url eg. http://www.service.com/Services/asmx
        /// </summary>
        public Uri ServiceURL { get; set; }
        /// <summary>
        /// holds the current parameters that are needing to be passed
        /// </summary>
        private Dictionary<string, string> Params = new Dictionary<string, string>();
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes a method call for the current instance
        /// </summary>
        /// <returns></returns>
        public XDocument InvokeMethod() {

            string url = String.Format("{0}?{1}", ServiceURL, string.Join("&", Params.Select(t => t.Key + "=" + t.Value)));

            WebClient serviceRequest = new WebClient();
            string response = serviceRequest.DownloadString(new Uri(url));
            //Refresh
            Params.Clear();
            ServiceURL = null;
            return XDocument.Parse(response);
        }


        /// <summary>
        /// Appends items to the parameter list
        /// </summary>
        /// <param name="name">The expected parameter name</param>
        /// <param name="value">The expected parameter value</param>
        public void AddParameter(string name, string value) {
            Params.Add(name, value);
        }
        #endregion
    }
}
