﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System {
    public static class GetDescription {
        /// <summary>
        /// Gets the value of an enums description attribute
        /// </summary>
        /// <param name="value">Current enum</param>
        /// <returns></returns>
        public static string GetEnumDescriptionAttribute(this Enum value) {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0) {
                return attributes[0].Description;
            }
            else {
                return value.ToString();
            }
        }

        /// <summary>
        /// Gets an enums value from its description.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="enumType"></param>
        /// <returns>Enum</returns>
        public static object GetEnumValueFromDescription(string value, Type enumType) {
            string[] names = Enum.GetNames(enumType);
            foreach (string name in names) {
                if (GetEnumDescriptionAttribute((Enum)Enum.Parse(enumType, name)).Equals(value)) {
                    return Enum.Parse(enumType, name);
                }
            }

            throw new ArgumentException("The string is not a description or value of the specified enum.");
        }
    }
}
