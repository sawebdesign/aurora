﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Net;
using System.IO;

namespace Aurora.Custom {
    /// <summary>
    /// Author      : Wesley Perumal
    /// Date        : 07-03-2012
    /// Description : Basic intergration into smarter stats
    /// </summary>
    public class SmarterStats {
        #region Structs
        /// <summary>
        /// These structs mimic the XML structure of the returning data from the service.
        /// </summary>
        public struct ServerInfo {
            public string ServerIP { get; set; }
            public string ServerPort { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }
        public struct SiteSettingsInfo {
            public string DomainName { get; set; }
            public int ServerID { get; set; }
            public int SiteID { get; set; }
        }
        public struct ActivityResult {
            public long BandwidthBytes { get; set; }
            public long Hits { get; set; }
            public int SiteID { get; set; }
            public string SiteName { get; set; }
            public int Views { get; set; }
            public int Visits { get; set; }
        }
        public struct VisitsItem {
            public DateTime Date { get; set; }
            public int NewVisits { get; set; }
            public int ReturningVisits { get; set; }
            public string SiteName { get; set; }
            public int UniqueVisits { get; set; }
            public int Visits { get; set; }
        }
        #endregion

        #region Constructor
        private ServerInfo settings = new ServerInfo();
        public SmarterStats(ServerInfo serverSettings) {
            settings = serverSettings;
        }
        #endregion

        #region SiteAdmin
        /// <summary>
        /// Gets a list of sites/ particular site
        /// </summary>
        /// <param name="siteName">(Optional) Specify a domain name to retrieve a specific site</param>
        /// <returns></returns>
        public List<SiteSettingsInfo> GetSites(string siteName = null) {
            AuroraREST newWebRequest = new AuroraREST();
            newWebRequest.ServiceURL = new Uri(String.Format("http://{0}{1}/Services/SiteAdmin.asmx/GetSitesForServer2", settings.ServerIP, settings.ServerPort));
            newWebRequest.AddParameter("authUserName", settings.UserName);
            newWebRequest.AddParameter("authPassword", settings.Password);
            newWebRequest.AddParameter("ServerID", "1");
            newWebRequest.AddParameter("IncludeDetails", "true");
            string xmlNamespace = "http://www.smartertools.com/smarterstats/SiteAdmin.asmx";

            XDocument response = newWebRequest.InvokeMethod();

            var items = (from res in response.Descendants(XName.Get("SiteSettingInfo", xmlNamespace))
                         select new SiteSettingsInfo {
                             DomainName = (string)res.Element(XName.Get("DomainName", xmlNamespace)),
                             ServerID = (int)res.Element(XName.Get("ServerID", xmlNamespace)),
                             SiteID = (int)res.Element(XName.Get("SiteID", xmlNamespace))
                         }).ToList();

            if (siteName != null) {
                items = items.Where(site => site.DomainName == siteName).ToList();
            }
            return items;

        }
        #endregion

        #region Statistics
        /// <summary>
        /// Gets site bandwidth,hits,views & visits
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<ActivityResult> GetBandwidthForSite(int siteID, DateTime startDate, DateTime endDate) {
            AuroraREST newWebRequest = new AuroraREST();
            newWebRequest.ServiceURL = new Uri(String.Format("http://{0}{1}/Services/Statistics.asmx/GetActivityForSite", settings.ServerIP, settings.ServerPort));
            newWebRequest.AddParameter("authUserName", settings.UserName);
            newWebRequest.AddParameter("authPassword", settings.Password);
            newWebRequest.AddParameter("SiteID", siteID.ToString());
            newWebRequest.AddParameter("StartDate", startDate.ToString("MM-dd-yyyy HH:mm"));
            newWebRequest.AddParameter("EndDate", endDate.ToString("MM-dd-yyyy HH:mm"));
            string xmlNamespace = "http://www.smartertools.com/smarterstats/Statistics.asmx";
            XDocument response = newWebRequest.InvokeMethod();


            var items = (from res in response.Descendants(XName.Get("ActivityResult", xmlNamespace))
                         select new ActivityResult {
                             BandwidthBytes = (long)res.Element(XName.Get("BandwidthBytes", xmlNamespace)),
                             Hits = (long)res.Element(XName.Get("Hits", xmlNamespace)),
                             SiteID = (int)res.Element(XName.Get("SiteID", xmlNamespace)),
                             SiteName = (string)res.Element(XName.Get("SiteName", xmlNamespace)),
                             Views = (int)res.Element(XName.Get("Views", xmlNamespace)),
                             Visits = (int)res.Element(XName.Get("Visits", xmlNamespace))
                         }).ToList();

            return items;
        }

        /// <summary>
        /// Gets all sites bandwidth,hits,views & visits
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="serverID"></param>
        /// <returns></returns>
        public List<ActivityResult> GetBandwidthForAllSites(DateTime startDate, DateTime endDate, int serverID = 1) {
            AuroraREST newWebRequest = new AuroraREST();
            newWebRequest.ServiceURL = new Uri(String.Format("http://{0}{1}/Services/Statistics.asmx/GetActivityForSite", settings.ServerIP, settings.ServerPort));
            newWebRequest.AddParameter("authUserName", settings.UserName);
            newWebRequest.AddParameter("authPassword", settings.Password);
            newWebRequest.AddParameter("ServerID", serverID.ToString());
            newWebRequest.AddParameter("StartDate", startDate.ToString("MM-dd-yyyy HH:mm"));
            newWebRequest.AddParameter("EndDate", endDate.ToString("MM-dd-yyyy HH:mm"));
            string xmlNamespace = "http://www.smartertools.com/smarterstats/Statistics.asmx";
            XDocument response = newWebRequest.InvokeMethod();


            var items = (from res in response.Descendants(XName.Get("ActivityResult", xmlNamespace))
                         select new ActivityResult {
                             BandwidthBytes = (long)res.Element(XName.Get("BandwidthBytes", xmlNamespace)),
                             Hits = (long)res.Element(XName.Get("Hits", xmlNamespace)),
                             SiteID = (int)res.Element(XName.Get("SiteID", xmlNamespace)),
                             SiteName = (string)res.Element(XName.Get("SiteName", xmlNamespace)),
                             Views = (int)res.Element(XName.Get("Views", xmlNamespace)),
                             Visits = (int)res.Element(XName.Get("Visits", xmlNamespace))
                         }).ToList();

            return items;
        }
        #endregion
    }
}
