﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Aurora.Custom
{

    public class SitelutionsAPI
    {

        private string location { get; set; }
        private string uri { get; set; }
        private string user { get; set; }
        private string pass { get; set; }

        public SitelutionsAPI(string location, string uri, string username, string password)
        {

            this.location = location ?? "https://api.sitelutions.com/soap-api";
            this.uri = uri ?? "https://api.sitelutions.com/API";
            this.user = username;
            this.pass = password;

        }

        /// <summary>
        /// Get a list of all domains
        /// </summary>
        /// <returns></returns>
        public string listDomains()
        {

            List<Param> myParams = new List<Param>();
            myParams.Add(new Param("user", "string", user));
            myParams.Add(new Param("password", "string", pass));

            return MakeWebServiceCall(methodName.listDomains.ToString(), myParams);
        }

        /// <summary>
        /// Get a list of all resource records for a domain
        /// </summary>
        /// <param name="domainid">123456</param>
        /// <returns></returns>
        public string listRRsByDomain(long domainid)
        {

            List<Param> myParams = new List<Param>();
            myParams.Add(new Param("user", "string", user));
            myParams.Add(new Param("password", "string", pass));
            myParams.Add(new Param("domainid", "int", domainid.ToString()));

            return MakeWebServiceCall(methodName.listRRsByDomain.ToString(), myParams);
        }

        /// <summary>
        /// Get domain details by domain name
        /// </summary>m>
        /// <param name="domain">Name of the domain</param>
        /// <returns></returns>
        public string getDomainByName(string domain)
        {

            List<Param> myParams = new List<Param>();
            myParams.Add(new Param("user", "string", user));
            myParams.Add(new Param("password", "string", pass));
            myParams.Add(new Param("name", "string", domain));

            return MakeWebServiceCall(methodName.getDomainByName.ToString(), myParams);
        }

        /// <summary>
        /// This will add a sub-domain to an existing site.
        /// </summary>
        /// <param name="domainid">123456</param>
        /// <param name="data">127.0.0.1</param>
        /// <param name="type">a</param>
        /// <param name="hostname">test</param>
        /// <returns></returns>
        public string addRR(long domainid, string data, string type, string hostname, int? ttl)
        {

            ttl = ttl ?? 10;
            List<Param> myParams = new List<Param>();
            myParams.Add(new Param("user", "string", user));
            myParams.Add(new Param("password", "string", pass));
            myParams.Add(new Param("domainid", "int", domainid.ToString()));
            myParams.Add(new Param("data", "string", data));
            myParams.Add(new Param("type", "string", type));
            myParams.Add(new Param("hostname", "string", hostname));
            myParams.Add(new Param("ttl", "int", ttl.ToString()));

            return MakeWebServiceCall(methodName.addRR.ToString(), myParams);
        }

        private string MakeWebServiceCall(string methodName, List<Param> callParams)
        {
            WebRequest webRequest = WebRequest.Create(location);

            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml";
            Stream requestStream = httpRequest.GetRequestStream();

            //Create Stream and Complete Request 
            StreamWriter streamWriter = new StreamWriter(requestStream);
            streamWriter.Write(GetSoapString(methodName, callParams));
            streamWriter.Flush();
            streamWriter.Close();

            //Get the Response 
            WebResponse webResponse = httpRequest.GetResponse();
            Stream responseStream = webResponse.GetResponseStream();
            StreamReader streamReader = new StreamReader(responseStream);

            //Read the response into an xml document 
            System.Xml.XmlDocument soapResonseXMLDocument = new System.Xml.XmlDocument();
            soapResonseXMLDocument.LoadXml(streamReader.ReadToEnd());

            //return only the xml representing the response details (inner request) 
            return soapResonseXMLDocument.InnerXml;
        }

        private string GetSoapString(string methodName, List<Param> callParams)
        {

            StringBuilder soapRequest = new StringBuilder("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"");
            soapRequest.Append(" xmlns:ns1=\"" + uri + "\"");
            soapRequest.Append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
            soapRequest.Append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
            soapRequest.Append(" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"");
            soapRequest.Append(" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
            soapRequest.Append("<SOAP-ENV:Body>");
            soapRequest.Append("<ns1:" + methodName + ">");
            foreach (Param obj in callParams)
            {
                soapRequest.Append("<" + obj.name + " xsi:type=\"xsd:" + obj.type + "\">" + obj.value + "</" + obj.name + ">");
            }
            soapRequest.Append("</ns1:" + methodName + ">");
            soapRequest.Append("</SOAP-ENV:Body>");
            soapRequest.Append("</SOAP-ENV:Envelope>");
            return soapRequest.ToString();
        }

        private class Param
        {
            public string name { get; set; }
            public string type { get; set; }
            public string value { get; set; }

            public Param(string name, string type, string value)
            {
                this.name = name;
                this.type = type;
                this.value = value;
            }
        }

        private enum methodName
        {
            /// <summary>
            /// getDomainByName - Parameters(name)
            /// </summary>
            getDomainByName,

            /// <summary>
            /// listDomains - Parameters()
            /// </summary>
            listDomains,

            // <summary>
            /// addRR - Parameters(domainid, data, type, hostname) 
            /// Optional Parameters (ttl)
            /// </summary>
            addRR,

            // <summary>
            /// listRRsByDomain - Parameters(domainid) 
            /// </summary>
            listRRsByDomain
        }

    }

}
