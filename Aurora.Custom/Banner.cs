﻿//using Microsoft.VisualBasic;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Diagnostics;
//public class eezyAds
//{
//    bool g_eezyads_Demo = false;
//        //Connect string
//     string g_eezyads_strConnect;
//    int g_eezyads_MaxRecords = 50;
//        //HTTP path to adserve.asp and Clients.AdClick.asp, like http://www.sqlexperts.com/ads
//    string g_eezyads_PathToAdServe = "";
//     string g_eezyads_DatabaseType = "SQLServer";
//        //Internal - you should not set it yourself
//     bool g_eezyads_strAlreadyOnPage;
//     string g_eezyads_eezyadsRedirPath = g_eezyads_PathToAdServe + "eezyadsredir.ashx";
//        // Virtually forever, max for a long integer in Access
//    public long  g_MaxLongInt = 2147483647;
//        // This date means forever...
//    public string g_MaxEndDate = "1 Jan 2020";

//    public string eezyads_GetAdASP(string strQueryString)
//    {
//        dynamic sArr = null;
//        dynamic n = null;
//        dynamic sArr2 = null;

//        //Setable parameters
//        dynamic sZones = null;
//        dynamic nFarm = null;
//        dynamic nBannerId = null;
//        dynamic nBannerOnPage = null;
//        dynamic SearchCatId = null;
//        dynamic SiteId = null;

//        strQueryString = strQueryString.Replace("}", "");
//        strQueryString = strQueryString.Replace("{", "");

//        sArr = Strings.Split(strQueryString, "&");

//        for (n = Information.LBound(sArr); n <= Information.UBound(sArr); n++) {
//            sArr2 = Strings.Split(sArr(n), "=");

//            switch (sArr2(0)) {
//                case "Z":
//                    sZones = sArr2(1) * 1;
//                    break;
//                case "F":
//                    nFarm = sArr2(1) * 1;
//                    break;
//                case "B":
//                    nBannerId = sArr2(1) * 1;
//                    break;
//                case "N":
//                    nBannerOnPage = sArr2(1) * 1;
//                    break;
//                case "CATID":
//                    if (Information.IsNumeric(sArr2(1)) == true) {
//                        SearchCatId = sArr2(1) * 1;
//                    } else {
//                        SearchCatId = 0;
//                    }
//                    break;
//                case "SITE":
//                    if (Information.IsNumeric(sArr2(1)) == true) {
//                        SiteId = sArr2(1) * 1;
//                    } else {
//                        SiteId = 0;
//                    }
//                    break;
//            }
//        }

//        //--if there is no banner that matches the CatId for the zone then
//        //--change the zone to the main zone and re-run
//        dynamic tmpStr = null;
//        tmpStr = eezyads_GetAdEx(true, sZones, nFarm, nBannerId, nBannerOnPage, SearchCatId, true, SiteId);
//        if (string.IsNullOrEmpty(tmpStr)) {
//            sZones = 0;
//            SearchCatId = "";
//            tmpStr = eezyads_GetAdEx(true, sZones, nFarm, nBannerId, nBannerOnPage, SearchCatId, true, SiteId);
//            return tmpStr;
//        } else {
//            return tmpStr;
//        }
//    }


//    // If ASP then it returns the HTML
//    // else it simply returns the bannerid
//    // fASP = true or false
//    private object eezyads_GetAdEx(fASP, strZone, nFarm, nBannerId, nBannerOnPage, nCatId, fCanUseHTML, SiteID)
//    {
//        object functionReturnValue = null;
//        DataSet oRS = null;
//        dynamic nSumWeight = null;
//        dynamic nTempIndex = null;
//        dynamic nWeight = null;
//        dynamic nTempIndex2 = null;
//        dynamic nBanner = null;
//        dynamic nCurRow = null;
//        dynamic nMax = null;

//        if (strZone * 1 == 0) {
//            strZone = 0;
//        }

//        if (nFarm * 1 == 0) {
//            nFarm = 0;
//        }

//        //--Get Total Weight
//        oRS = eezyads_DBGetAvailBanners(fASP, strZone, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID);
//        if (oRS.Tables[0].Rows.Count == 0) {
//            //There is no banner in this banner farm
//            oRS.Dispose();
//            return "";
//            return functionReturnValue;
//        }

//        //Now lets get the total weight
//        nSumWeight = 0;
//        for (int ix = 0; ix <= oRS.Tables[0].Rows.Count - 1; ix++) {
//            nSumWeight = nSumWeight + oRS.Tables[0].Rows[ix]["weight"];
//        }

//        //Lets get a random banner
//        VBMath.Randomize();
//        nBanner = Conversion.Int((nSumWeight * VBMath.Rnd()) + 1);

//        int nCurVal = 0;
//        while (nCurVal + oRS.Tables[0].Rows[nCurVal]["weight"] < nBanner) {
//            nCurVal = nCurVal + oRS.Tables[0].Rows[nCurVal]["weight"];
//        }

//        nBanner = oRS.Tables[0].Rows[nCurVal]["bannerid"];

//        eezyads_AddToUsedList(nBanner);

//        if (!fASP) {
//            functionReturnValue = nBanner + "---" + oRS.Tables[0].Rows[nCurVal]["BannerURL"].Value;
//            eezyads_DBAddShowCount(nBanner);
//            return functionReturnValue;
//        }


//        if (fCanUseHTML & oRS.Tables[0].Rows[nCurVal]["ishtml"] == true) {
//            dynamic sHTMCode = null;
//            sHTMCode = eezyads_GetHTMLCode(nBanner);
//            functionReturnValue = FixupSpecialVariables(sHTMCode);
//            eezyads_DBAddShowCount(nBanner);
//            return functionReturnValue;
//        }

//        //Now we have the banner id, lets create the actual HTML
//        //Move into temp variables only to make it more readable
//        dynamic sRedirUrl = null;
//        dynamic sBannerURL = null;
//        dynamic sAltText = null;
//        dynamic sUnderText = null;
//        dynamic sUnderUrl = null;
//        dynamic sRet = null;
//        dynamic nXSize = null;
//        dynamic nYSize = null;
//        dynamic MyTarget = null;

//        sRedirUrl = g_eezyads_eezyadsRedirPath + "?id=" + nBanner + "&way=ban";
//        if (Information.IsDBNull(oRS.Tables[0].Rows[nCurVal]["BannerURL"])) {
//            sBannerURL = "";
//        } else {
//            sBannerURL = oRS.Tables[0].Rows[nCurVal]["BannerURL"];
//        }
//        if (Information.IsDBNull(oRS.Tables[0].Rows[nCurVal]["AltText"])) {
//            sAltText = "";
//        } else {
//            sAltText = oRS.Tables[0].Rows[nCurVal]["AltText"];
//        }
//        if (Information.IsDBNull(oRS.Tables[0].Rows[nCurVal]["UnderText"])) {
//            sUnderText = "";
//        } else {
//            sUnderText = oRS.Tables[0].Rows[nCurVal]["UnderText"];
//        }
//        sUnderUrl = g_eezyads_eezyadsRedirPath + "?id=" + nBanner + "&way=txt";

//        nXSize = oRS.Tables[0].Rows[nCurVal]["xsize"];
//        nYSize = oRS.Tables[0].Rows[nCurVal]["ysize"];
//        if (Strings.InStr(oRS.Tables[0].Rows[nCurVal]["redirurl"], "http://") | Strings.InStr(oRS.Tables[0].Rows[nCurVal]["underurl"], "http://")) {
//            MyTarget = " target=\"_new\"";
//        }
//        if (Strings.Right(sBannerURL, 3) == "swf") {
//            sRet = "<embed src=\"" + sBannerURL + "\" alt=\"" + sAltText + "\"" + " border=0 width=\"" + nXSize + "\"" + " height=\"" + nYSize + "\"" + "  datadesc=\"" + sAltText + "\" dataid=\"" + nBanner + "\" />";
//        } else {
//            sRet = "<a href=\"" + sRedirUrl + "\" " + MyTarget + " datadesc=\"" + sAltText + "\" dataid=\"" + nBanner + "\">" + "<img src=\"" + sBannerURL + "\"" + " alt=\"" + sAltText + "\"" + " border=0 width=\"" + nXSize + "\"" + " height=\"" + nYSize + "\"" + ">" + "</a>";
//        }
//        if (!string.IsNullOrEmpty(sUnderText)) {
//            sRet = sRet + "</br><a class=\"inlink\" href=\"" + sUnderUrl + "\"" + " " + MyTarget + " datadesc=\"" + sUnderText + "\" dataid=\"" + nBanner + "\">" + sUnderText + "</a></font>";
//        } else {
//        }
//        sRet = sRet + "";



//        //Lets update impression for it
//        eezyads_DBAddShowCount(nBanner);
//        oRS.Dispose();

//        return sRet;
//        return functionReturnValue;
//    }


//    public object eezyads_GetHTMLCode(nBannerId)
//    {
//        object functionReturnValue = null;
//        DataSet oRS = null;
//        functions x = new functions();
//        oRS = x.getData("select htmlcode from Clients.AdBanner where bannerid=" + nBannerId);
//        functionReturnValue = oRS.Tables[0].Rows[0]["htmlcode"];
//        oRS.Dispose();
//        x.Dispose();
//        return functionReturnValue;
//    }

//    public DataSet eezyads_DBGetAvailBanners(fASP, strZones, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID)
//    {
//        functions x = new functions();
//        return x.getData(GetAdSQL(fASP, strZones, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID));
//    }


//    public object GetAdSQL(fASP, sZone, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID)
//    {
//        dynamic strSQL = null;
//        dynamic strWhere = null;

//        strSQL = "select distinct Clients.AdBanner.bannerid, Clients.AdBanner.BannerURL, Clients.AdBanner.weight ";
//        if (fASP) {
//            strSQL = strSQL + ", Clients.AdBanner.AltText, Clients.AdBanner.UnderText, Clients.AdBanner.xsize, Clients.AdBanner.ysize, Clients.AdBanner.redirurl, Clients.AdBanner.underurl";
//        }
//        if (fCanUseHTML) {
//            strSQL = strSQL + ",ishtml";
//        }

//        if (!string.IsNullOrEmpty(g_eezyads_strAlreadyOnPage)) {
//            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid not in ( " + g_eezyads_strAlreadyOnPage + ")");
//        }

//        strSQL = strSQL + " from Clients.AdBanner ";
//        if (sZone != 0) {
//            strSQL = strSQL + ",Clients.AdBannerZone ";
//            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid=Clients.AdBannerZone.bannerid");
//            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBannerZone.zoneid in ( " + sZone + ")");
//        }

//        strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.ClientSiteID=" + SiteID);

//        if (Information.IsNumeric(nCatId) == false)
//            nCatId = 0;
//        if (nCatId * 1 != 0) {
//            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.CategoryId='" + nCatId + "'");
//        }
//        if (nBannerId != 0) {
//            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid=" + nBannerId);
//        } else {
//            if (nFarm != 0) {
//                strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.farmid=" + nFarm);
//            }
//            strWhere = AdSQL_AddAndWhere(strWhere, "weight > 0 and showcount < maximpressions AND validtodate >= " + Internal_eezyadsdb_GetDateFunction() + " AND validfromdate <= " + Internal_eezyadsdb_GetDateFunction());
//            if (fCanUseHTML == true) {
//                strWhere = AdSQL_AddAndWhere(strWhere, " ( ( clickcount+underclickcount < maxclicks ) OR ishtml=" + Internal_eezyadsdb_GetBoolValue(true) + " )");
//            } else {
//                strWhere = AdSQL_AddAndWhere(strWhere, "ishtml <> " + Internal_eezyadsdb_GetBoolValue(true));
//            }
//        }

//        strSQL = strSQL + strWhere;

//        // If you want banners with no zoning to mean all zones then add these 
//        // lines

//        //	If sZone <> "0" Then
//        //			strSQL = strSQL & "union select  distinct Clients.AdBanner.bannerid, Clients.AdBanner.BannerURL, Clients.AdBanner.weight "
//        //		If fASP Then
//        //			strSQL = strSQL & ", Clients.AdBanner.AltText, Clients.AdBanner.UnderText, Clients.AdBanner.xsize, Clients.AdBanner.ysize "
//        //		End If
//        //		If fCanUseHTML Then
//        //			strSQL = strSQL & ",ishtml"
//        //		End If
//        //		strSQL = strSQL & " from Clients.AdBanner "
//        //		strSQL = strSQL & " where bannerid not in (select bannerid from Clients.AdBannerZone)"
//        //		If 	g_eezyads_strAlreadyOnPage <> "" Then
//        //			strSQL = strSQL & " AND Clients.AdBanner.bannerid not in ( " & 	g_eezyads_strAlreadyOnPage & ")" 
//        //		End If
//        //	End If	

//        return strSQL;

//    }

//    public void eezyads_DBAddShowCount(nBanner)
//    {
//        functions x = new functions();
//        x.upDateData("update Clients.AdBanner set showcount=showcount+1 where bannerid=" + nBanner);
//        x.Dispose();
//    }

//    public void eezyads_DBUpdateClickCount(nBanner, fUnderText)
//    {
//        dynamic sSQL = null;
//        functions x = new functions();

//        if (fUnderText == true) {
//            sSQL = "update Clients.AdBanner set underclickcount = underclickcount +1 where bannerid = " + nBanner;
//        } else {
//            sSQL = "update Clients.AdBanner set clickcount = clickcount +1 where bannerid = " + nBanner;
//        }
//        x.upDateData(sSQL);

//        sSQL = "Insert Into Clients.AdClick (bannerid,page,ClickDate,ip,isUnderText) Values (" + nBanner + ", '" + System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"] + "', '" + System.DateTime.Now() + "', '" + System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] + "', " + Internal_eezyadsdb_GetBoolValue(fUnderText) + ")";
//        x.upDateData(sSQL);
//        x.Dispose();
//    }

//    public object eezyads_DBGetUrl(nBanner, fUnderText)
//    {
//        object functionReturnValue = null;
//        DataSet oRS = null;
//        dynamic sSQL2 = null;
//        functions x = new functions();
//        if (fUnderText == true) {
//            sSQL2 = "select underurl as url from Clients.AdBanner where bannerid= " + nBanner;
//        } else {
//            sSQL2 = "select redirurl as url from Clients.AdBanner where bannerid= " + nBanner;
//        }

//        oRS = x.getData(sSQL2);
//        functionReturnValue = oRS.Tables[0].Rows[0]["url"];
//        oRS.Dispose();
//        x.Dispose();
//        return functionReturnValue;
//    }

//    public object AdSQL_AddAndWhere(strWhere, strWhat)
//    {
//        if (string.IsNullOrEmpty(strWhere)) {
//            strWhere = " WHERE ";
//        } else {
//            strWhere = strWhere + " AND ";
//        }
//        strWhere = strWhere + " " + strWhat;
//        return strWhere;
//    }

//    private object eezyads_AddToUsedList(nBannerId)
//    {
//        if (!string.IsNullOrEmpty(g_eezyads_strAlreadyOnPage)) {
//            g_eezyads_strAlreadyOnPage = g_eezyads_strAlreadyOnPage + ",";
//        }
//        g_eezyads_strAlreadyOnPage = g_eezyads_strAlreadyOnPage + Convert.ToString(nBannerId);
//    }

//    public object Internal_eezyadsdb_GetDateFunction()
//    {
//        object functionReturnValue = null;
//        if (g_eezyads_DatabaseType == "SQLServer") {
//            functionReturnValue = "getdate()";
//        } else {
//            functionReturnValue = "date()";
//        }
//        return functionReturnValue;
//    }

//    public object Internal_eezyadsdb_GetBoolValue(fTrue)
//    {
//        object functionReturnValue = null;
//        if (g_eezyads_DatabaseType == "SQLServer") {
//            if (fTrue == true) {
//                functionReturnValue = "1";
//            } else {
//                functionReturnValue = "0";
//            }
//        } else {
//            if (fTrue == true) {
//                functionReturnValue = "true";
//            } else {
//                functionReturnValue = "false";
//            }
//        }
//        return functionReturnValue;

//    }

//    private object FixupSpecialVariables(sHTML)
//    {
//        //Now check for '<ADM_RANDOM
//        dynamic fCont = null;
//        fCont = true;
//        while (fCont == true) {
//            dynamic nIndStart = null;
//            dynamic nIndEnd = null;
//            dynamic sSubStr = null;
//            dynamic vData = null;
//            dynamic nLow = null;
//            dynamic nHigh = null;
//            dynamic nNumber = null;
//            dynamic sLeftHTML = null;
//            dynamic sRightHTML = null;

//            nIndStart = Strings.InStr(1, Convert.ToString(sHTML), "<ADM_RANDOM");
//            if (nIndStart > 0) {
//                sLeftHTML = Strings.Left(sHTML, nIndStart - 1);

//                nIndEnd = Strings.InStr(nIndStart, sHTML, ">");

//                sRightHTML = Strings.Mid(sHTML, nIndEnd + 1);

//                sSubStr = Strings.Mid(sHTML, nIndStart, nIndEnd - nIndStart);

//                vData = Strings.Split(sSubStr, "-");
//                if (vData(1) == "LAST") {
//                    nNumber = System.Web.HttpContext.Current.Session["eezyads_RndNumber"];
//                } else {
//                    nLow = Convert.ToInt64(vData(1));
//                    nHigh = Convert.ToInt64(vData(2));
//                    VBMath.Randomize();
//                    nNumber = Convert.ToInt64((nHigh * VBMath.Rnd()) + nLow);
//                    System.Web.HttpContext.Current.Session["eezyads_RndNumber"] = nNumber;
//                }
//                sHTML = sLeftHTML + Convert.ToString(nNumber) + sRightHTML;
//            }
//            if (Strings.InStr(1, Convert.ToString(sHTML), "<ADM_RANDOM") > 0) {
//                fCont = true;
//            } else {
//                fCont = false;
//            }
//        }
//        return sHTML;

//    }

//    public string eezyads_ClickAd(nBannerId, sWay)
//    {
//        bool fIsUnderText = false;
//        if (sWay == "txt") {
//            fIsUnderText = true;
//        } else {
//            fIsUnderText = false;
//            //--Clicked on actual banner
//        }

//        eezyads_DBUpdateClickCount(nBannerId, fIsUnderText);
//        return eezyads_DBGetUrl(nBannerId, fIsUnderText);

//    }

//    public string GetFarmName(int Id)
//    {
//        try {
//            functions x = new functions();
//            DataSet ds = x.getData("select name from Features.AdFarm where farmid = " + Id + "");
//            if (ds.Tables[0].Rows.Count >= 1) {
//                return ds.Tables[0].Rows[0]["name"];
//            } else {
//                return "No farm selected! THIS BANNER WILL NEVER ROTATE!";
//            }
//            ds.Dispose();
//        } catch (Exception ex) {
//            return "Unknown";
//        }
//    }

//    public string GetZoneName(int Id)
//    {
//        try {
//            functions x = new functions();
//            DataSet ds = x.getData("select zonename from Features.AdZone, Clients.AdBannerZone where Clients.AdBannerZone.zoneid=Features.AdZone.zoneid AND Clients.AdBannerZone.bannerid= " + Id + "");
//            if (ds.Tables[0].Rows.Count >= 1) {
//                return ds.Tables[0].Rows[0]["zonename"];
//            } else {
//                return "No zones selected";
//            }
//            ds.Dispose();
//        } catch (Exception ex) {
//            return "Unknown";
//        }
//    }

//}
