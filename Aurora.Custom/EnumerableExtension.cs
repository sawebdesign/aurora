﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Aurora.Custom {
    public static class ExportToCSV {
        /// <summary>
        /// Converts an entity to a csv based string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="context"></param>
        public static string ExportEntityToCSV<T>(this T entity, bool includeHeaders, string[] fieldsToinclude = null) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            return Export(entity, includeHeaders, fieldsToinclude);
        }

        /// <summary>
        /// Converts an entity to a CSV string
        /// </summary>
        /// <typeparam name="T">entity type</typeparam>
        /// <param name="Objects">entiy</param>
        /// <param name="includeHeaderLine">Write entity names</param>
        /// <param name="fields">Fields to include</param>
        /// <returns>CSV string</returns>
        private static string Export<T>(T Objects, bool includeHeaderLine, string[] fields = null) where T : System.Data.Objects.DataClasses.IEntityWithKey {

            StringBuilder sb = new StringBuilder();
            //Get properties using reflection.
            IList<PropertyInfo> propertyInfos = typeof(T).GetProperties();

            if (includeHeaderLine) {
                //add header line.
                foreach (PropertyInfo propertyInfo in propertyInfos) {
                    if (fields.Length > 0) {
                        if (fields.Contains(propertyInfo.Name)) {
                            sb.Append(propertyInfo.Name).Append(",");
                        }
                    }
                    else {
                        sb.Append(propertyInfo.Name).Append(",");
                    }

                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            //add value for each property.
            foreach (PropertyInfo propertyInfo in propertyInfos) {
                sb.Append(MakeValueCsvFriendly(propertyInfo.GetValue(Objects, null))).Append(",");

                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Cleans an entity value
        /// </summary>
        /// <param name="value">Entity Value</param>
        /// <returns></returns>
        private static string MakeValueCsvFriendly(object value) {
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";

            if (value is DateTime) {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }
            string output = value.ToString();

            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';

            return output;

        }
    }
}
