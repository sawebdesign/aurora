﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace System
{
   public static class GetWeek
    {
        /// <summary>
        /// Returns the week number of a date. 
        /// </summary>
        /// <param name="dtPassed"></param>
        /// <returns></returns>
        public static int GetWeekNumber(this DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            DateTimeFormatInfo fiCurr = DateTimeFormatInfo.CurrentInfo;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, fiCurr.CalendarWeekRule, fiCurr.FirstDayOfWeek);
            return weekNum;
        }
       /// <summary>
       /// Gets the week number of the current date in a month i.e the 1st week,2nd week so on..
       /// </summary>
       /// <returns></returns>
       public static int GetCurrentMonthWeekNumber(this DateTime currentMonthDay)
       {
           //calculate our current week
           int remainder = currentMonthDay.Day % 7;
           int week = currentMonthDay.Day / 7;
           if (remainder > 0)
           {
               week = week + 1;
           }

           return week;
       }

      public static DateTime FirstDateOfWeek(this DateTime selectedDate) {
           DateTime jan1 = new DateTime(selectedDate.Year, 1, 1);
           int weekOfYear = selectedDate.GetWeekNumber();
           int daysOffset = (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;

           DateTime firstMonday = jan1.AddDays(daysOffset);

           int firstWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(jan1, CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule, CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);

           if (firstWeek <= 1) {
               weekOfYear -= 1;
           }

           return firstMonday.AddDays(weekOfYear * 7);
       }
    }
}
