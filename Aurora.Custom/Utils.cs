#region Directives
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.Text;
using System.Data;
using System.Configuration;
using System.Reflection;
using System.Collections;
using System.Data.OleDb;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Linq;
using Aurora.Custom.Data;
using HtmlAgilityPack;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Web.Administration;
using System.Data.Common;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Color = MigraDoc.DocumentObjectModel.Color;
using Image = MigraDoc.DocumentObjectModel.Shapes.Image;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Dynamic;
using System.Web.Security;

#endregion

namespace Aurora.Custom {

	#region Utils
	/// <summary>
	/// Author:         Mark 
	/// Date:           2008-01-13
	/// Description:    This class has common function, object and enums
	/// </summary>
	public class Utils {
		public static SqlDatabase SqlDb { get; set; }
		#region Enums
		/// <summary>
		/// Contains all message events in the message centre system
		/// </summary>
		public enum MessageCentreEvents {
			/// <summary>
			/// Occurs when a client has completed a payment
			/// </summary>
			[DescriptionAttribute("Purchase Completed")]
			ProductPurchaseCompleted,
			/// <summary>
			/// Occurs when a client has completed a payment
			/// </summary>
			[DescriptionAttribute("Sys: Purchase Completed")]
			SysProductPurchaseCompleted,
			/// <summary>
			/// Occurs when a client has completed an EFT payment
			/// </summary>
			[DescriptionAttribute("EFT Completed")]
			ProductEftCompleted,
			/// <summary>
			/// Occurs when enquiry is created
			/// </summary>
			[DescriptionAttribute("Enquiry Created")]
			ProductEnquiryCreated,
			/// <summary>
			/// Occurs when a testimony is added to a site
			/// </summary>
			[DescriptionAttribute("New Testimony Added")]
			NewTestimonyAdded,
			/// <summary>
			/// Occurs when the system alerts the client that a vistor has added a testimony
			/// </summary>
			[DescriptionAttribute("Sys: New Testimony Added")]
			SysNewTestimonyAdded,
			/// <summary>
			/// Occurs when a vistor signs up to  a clients web site
			/// </summary>
			[DescriptionAttribute("New Registration")]
			MemberNewRegistration,
			/// <summary>
			/// Occurs when a vistor request their password
			/// </summary>
			[DescriptionAttribute("Forgotten Password")]
			MemberForgottenPassword,
			/// <summary>
			/// Occurs when a client activates a visitors membership
			/// </summary>
			[DescriptionAttribute("Member Status to Active")]
			MemberStatusChangedtoActive,
			/// <summary>
			/// Occurs when the system alerts the client that a vistor has requested a login
			/// </summary>
			[DescriptionAttribute("Sys: New Registration")]
			SysMemberNewRegistration,
			/// <summary>
			/// Occurs when the system alerts the client that a vistor has requested
			/// </summary>
			[DescriptionAttribute("Sys: Enquiry Created")]
			SysProductEnquiryCreated,
			/// <summary>
			/// Occurs when the system alerts the client that a vistor has updated their details
			/// </summary>
			[DescriptionAttribute("Sys: Member Details Updated")]
			SysMemberUpdatedMemberDetails,
			/// <summary>
			/// Occurs when a product quotation has been send to a requester
			/// </summary>
			[DescriptionAttribute("Send Product Quotation")]
			SendProductQuotation,
			/// <summary>
			/// Occurs when a custom form is saved on the front-end
			/// </summary>
			[DescriptionAttribute("Sys: Custom Form Saved")]
			SysCustomFormSave,
			/// <summary>
			/// Occurs when a custom form is saved on the front-end
			/// </summary>
			[DescriptionAttribute("Custom Form Saved")]
			CustomFormSave,
			/// <summary>
			/// Occurs when a donation purchase has been successfull
			/// </summary>
			[DescriptionAttribute("Sys: New Donation")]
			SysNewDonation,
			/// <summary>
			/// Occurs when a donation purchase has been successfull
			/// </summary>
			[DescriptionAttribute("New Donation")]
			NewDonation,
			/// <summary>
			/// Occurs when a Membership category renewal or purchase requests EFT
			/// </summary>
			[DescriptionAttribute("Sys: EFT Request")]
			SysEFTRequest,
			/// <summary>
			/// Occurs when a member changes their membership category
			/// </summary>
			[DescriptionAttribute("Sys: Change Membership Category")]
			SysChangeMembership


		}
		#endregion

		#region util to create an Anonymous typse from an event entity

		public static dynamic getAnonEvent(Event myEventEntity) {
			var anonEventType = new {
				Title = myEventEntity.Title,
				Preview = myEventEntity.Preview,
				Occurance = myEventEntity.Occurance,
				Location = myEventEntity.Location,
				ID = myEventEntity.ID,
				Details = myEventEntity.Details,
				StartTimeStr = myEventEntity.StartDate.Value.ToString("HH:mm"),
				EndTimeStr = myEventEntity.EndDate.Value.ToString("HH:mm"),
				StartDateStr = myEventEntity.StartDate.Value.ToString("dd/MM/yyyy"),
				EndDateStr = myEventEntity.EndDate.Value.ToString("dd/MM/yyyy"),
				StartDateShortMonth = myEventEntity.StartDate.Value.ToString("MMM"),
				StartDateShortMonth1 = myEventEntity.StartDate.Value.ToString("MMMM"),
				StartDateShortDay = myEventEntity.StartDate.Value.ToString("dd"),
				GeoLatLong = myEventEntity.GeoLatLong
			};

			return anonEventType;
		}

		public static List<dynamic> getAnonEntityList(dynamic eventEntities) {
			List<dynamic> eventEntityList = new List<dynamic>();

			foreach (var eventItem in eventEntities) {
				eventEntityList.Add(getAnonEvent((Event)eventItem));
			}

			return eventEntityList;
		}

		#endregion

		#region Email
		/// <summary>
		/// Send generic email
		/// </summary>
		/// <param name="fromAddress">From address: name@sawebdesign.co.za</param>
		/// <param name="fromName">From name</param>
		/// <param name="toAddress">To address: client@domain.co.za</param>
		/// <param name="toName">To name: John Dow</param>
		/// <param name="msgBody">In plain text or HTML</param>
		/// <param name="msgSubject">The subject of the email</param>
		/// <param name="SMTPServer">IP address of domain E.G. smtp.sawebdeisng.co.za</param>
		public static string SendMail(string fromAddress, string fromName, string toAddress, string toName, string msgBody, string msgSubject, bool IsHtml, string SMTPServer) {
			try {

				System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(SMTPServer);
				System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress(fromAddress, fromName);
				System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(toAddress, toName);
				System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to);
				message.Subject = msgSubject;
				message.Body = msgBody;
				message.IsBodyHtml = IsHtml;
				client.Send(message);

				return "Sent";
			} catch (Exception ex) {
				return ("Error: Sending email - " + SMTPServer + " - " + ex.Message);
			}
		}

		/// <summary>
		/// Send generic email
		/// </summary>
		/// <param name="fromAddress">From address: name@sawebdesign.co.za</param>
		/// <param name="fromName">From name</param>
		/// <param name="toAddress">To address: client@domain.co.za</param>
		/// <param name="toName">To name: John Dow</param>
		/// <param name="msgBody">In plain text or HTML</param>
		/// <param name="msgSubject">The subject of the email</param>
		/// <param name="IsHtml">The contents mark up of the email</param>
		/// <param name="sendToClient">send a mail to a client</param>
		public static string SendMail(string fromAddress, string fromName, string msgBody, string msgSubject, bool IsHtml, bool sendToClient, string responseMessage = "") {
			string SMTPServer = ConvertDBNull<string>(ConfigurationManager.AppSettings["SMTP"], "localhost");
			try {

				Aurora.Custom.Data.ClientSite clientSite = CacheManager.SiteData;

				if (string.IsNullOrEmpty(fromAddress) && !Utils.isValidEmail(fromAddress)) {
					fromAddress = clientSite.Email1;
					fromName = clientSite.CompanyName;
				}

				System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(SMTPServer);
				System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress(clientSite.Email1, clientSite.CompanyName);
				System.Net.Mail.MailAddress fromSecondary = null;
				if (ConvertDBNull(clientSite.Email2, string.Empty) != string.Empty) {
					fromSecondary = new System.Net.Mail.MailAddress(clientSite.Email2, clientSite.CompanyName);
				}

				System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(fromAddress, fromName);

				//does not sent to client
				if (!sendToClient && string.IsNullOrEmpty(responseMessage)) {
					to = new System.Net.Mail.MailAddress(clientSite.Email1, clientSite.CompanyName);
				} else if (sendToClient && !string.IsNullOrEmpty(responseMessage)) {
					to = new System.Net.Mail.MailAddress(clientSite.Email1, clientSite.CompanyName);
				}




				System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to);
				message.Subject = msgSubject;
				message.Bcc.Add(from);
				if (fromSecondary != null) {
					message.Bcc.Add(fromSecondary);
				}
				message.Body = msgBody;
				message.IsBodyHtml = IsHtml;
				client.Send(message);

				if (!string.IsNullOrEmpty(responseMessage)) {
					message.Body = responseMessage;
					message.To.RemoveAt(0);
					message.To.Add(fromAddress);
					message.Bcc.RemoveAt(0);
					if (fromSecondary != null) {
						message.Bcc.RemoveAt(0);
					}
					client.Send(message);
				}



				return "Sent";
			} catch (Exception ex) {
				SendExceptionMail(ex);
				//SendErrorMail("support@sawebdesign.co.za", "Aurora", ex.Message + "<br/>" + ex.StackTrace + "<br/>" + ex.TargetSite + "<br/>" + ex.InnerException,
				//              "Error on Aurora");
				return ("Error: Sending email - " + SMTPServer + " - " + ex.Message);
			}
		}

		/// <summary>
		/// Send generic email
		/// </summary>
		/// <param name="fromAddress">From address: name@sawebdesign.co.za</param>
		/// <param name="fromName">From name</param>
		/// <param name="toAddress">To address: client@domain.co.za</param>
		/// <param name="toName">To name: John Dow</param>
		/// <param name="msgBody">In plain text or HTML</param>
		/// <param name="msgSubject">The subject of the email</param>
		public static string SendErrorMail(string fromAddress, string fromName, string msgBody, string msgSubject) {
			string SMTPServer = ConvertDBNull<string>(ConfigurationManager.AppSettings["SMTP"], "127.0.0.1");
			try {
				string userName = GetAppPoolUserName();
				Aurora.Custom.Data.ClientSite clientSite = CacheManager.SiteData;

				System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(SMTPServer);
				System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress(fromAddress, fromName);
				System.Net.Mail.MailAddress to = null;

				if (userName != string.Empty && userName.ToUpper().Contains("SAWEBDESIGN")) {
					to = new System.Net.Mail.MailAddress(userName.ToUpper().Replace("SAWEBDESIGN\\", string.Empty) + "@sawebdesign.co.za");
				} else {
					to = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["ErrorMailList"]);
				}

				System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to);
				message.Subject = msgSubject;
				message.Body = msgBody;
				message.Headers.Add("Precedence", "bulk");

				message.IsBodyHtml = true;
				if (msgBody != string.Empty) {
					client.Send(message);
				}

				return "Sent";
			} catch (Exception ex) {
				return ("Error: Sending email - " + SMTPServer + " - " + ex.Message);
			}
		}


		/// <summary>
		/// Sends an exception email when an error occurs
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public static string SendExceptionMail(Exception ex, String message = "") {
			try {
				string userDetails = "";
				string page = "";
				if (SessionManager.isloggedIn == true) {
					userDetails = SessionManager.FirstName + " " + SessionManager.LastName + ", " + SessionManager.UserName + ", " + SessionManager.ClientSiteID + "<br/>";
				} else {
					userDetails = SessionManager.ClientSiteID.ToString() + "<br/>";
				}
				userDetails += string.Format("{0}<br/>{1}<br/>",
											 HttpContext.Current.Request.UserAgent,
											 HttpContext.Current.Request.UserHostAddress);

				page = HttpContext.Current.Request.Url.OriginalString;
				string request = string.Empty;
				using (StreamReader r = new StreamReader(HttpContext.Current.Request.InputStream)) {
					r.BaseStream.Position = 0;
					request = r.ReadToEnd();
				}

				SendErrorMail(ConfigurationManager.AppSettings["ErrorMailListFrom"] ?? "Errors@BoltCMS.com",
								"Aurora",
								string.Format("An exception occurred on: {0}<br/>{8}<br/><br/><b>Message:</b><br/>{6}<br/>{1}<br/><br/><b>Page:</b><br/>{7}<br/><br/> <b>Stacktrace:</b><br/> {2} <br/><br/> <b>InnerException:<br/></b>{3} <br/><br/><b>Request Body:</b><br/>{4}<br/><br/><br/><b>User Details:</b><br/> {5}",
									System.Environment.MachineName,
									ex.Message,
									ex.StackTrace,
									ex.InnerException,
									HttpContext.Current.Server.HtmlEncode(request),
									userDetails,
									message,
									page,
									DateTime.Now.ToString()), "Error occured on Aurora - " + SessionManager.ClientSiteID + " - " + System.Environment.MachineName);

			} catch (Exception exc) {
				return ("Error: Sending email - " + exc.Message);

			}
			return "Sent";

		}
		#endregion

		#region File
		public static void WriteFile(string text, string path) {
			try {
				path = path + "\\Log.txt";

				System.IO.StreamWriter sw = System.IO.File.AppendText(path);
				sw.WriteLine(text);
				sw.Close();
			} catch { }
		}
		public static string CreateFileNameFromID(string idValue, ref HttpContext context, string currentFileName) {
			int instances = 0;
			if (!Directory.Exists(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, "Members")))) {
				Directory.CreateDirectory(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads", SessionManager.ClientSiteID)) + "/Members");
			}

			if (!Directory.Exists(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}", SessionManager.ClientSiteID, "Members", idValue)))) {
				Directory.CreateDirectory(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}", SessionManager.ClientSiteID, "Members")) + "/" + idValue);
			}

			//create file name
			int freeFileName = 1;
			bool filesFound = false;

			//find the next available file name
			while (!filesFound) {
				if (!File.Exists(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/{2}_0{4}{3}", SessionManager.ClientSiteID, "Members", idValue, currentFileName.Substring(currentFileName.LastIndexOf(".")), freeFileName))) && freeFileName < 10) {
					filesFound = true;
					instances = freeFileName;
				} else if (!File.Exists(context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/{2}_{4}{3}", SessionManager.ClientSiteID, "Members", idValue, currentFileName.Substring(currentFileName.LastIndexOf(".")), freeFileName))) && freeFileName > 10) {
					filesFound = true;
					instances = freeFileName;
				}
				freeFileName++;
			}

			if (instances < 10) {
				return context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/{2}_0{4}{3}", SessionManager.ClientSiteID, "Members", idValue, currentFileName.Substring(currentFileName.LastIndexOf(".")), instances));
			} else {
				return context.Server.MapPath(String.Format("~/ClientData/{0}/Uploads/{1}/{2}/{2}_{4}{3}", SessionManager.ClientSiteID, "Members", idValue, currentFileName.Substring(currentFileName.LastIndexOf(".")), instances));
			}
		}

		#endregion

		#region Database and Data methods
		/// <summary>
		/// Checks if the SqlDatabase property is initialized. If not, it will be.
		/// </summary>
		/// <remarks>
		/// Date:       2010-02-08
		/// </remarks>
		public static void CheckSqlDBConnection() {
			if (SqlDb == null)
				SqlDb = new SqlDatabase(ConfigurationManager.AppSettings["DBConnectionString"].ToString());
		}
		/// <summary>
		/// Converts an object to the specified type or returns the specified 
		/// null replacement if it is null.
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <typeparam name="T">Type of object to be returned</typeparam>
		/// <param name="value">Value to be checked</param>
		/// <param name="replacement">Replacement if value is null</param>
		/// <returns>Value as T or replacement value if null</returns>
		public static T ConvertDBNull<T>(object value, T replacement) {
			if ((value == null) || (value == DBNull.Value))
				return replacement;
			else
				return (T)Convert.ChangeType(value, typeof(T));
		}

		//public static List<T> ConvertDataTableToObject<T>(DataTable table) {
		//    List<T> list = new List<T>();
		//    foreach (DataRow row in table.Rows) {
		//        TObject target = Activator.CreateInstance<TObject>();
		//        DataColumnAttribute.Bind(row, target);
		//       list.Add(target);
		//    }

		//  return list.ToArray();
		//}

		/// <summary>
		/// Extract the specified object from the datarow provided
		/// </summary>
		/// <remarks>
		/// Date:   2010-02-08
		/// </remarks>
		/// <typeparam name="T">Type of object to be returned</typeparam>
		/// <param name="row">DataRow to extract values from</param>
		/// <returns>Values extracted from DataRow as object type specified</returns>
		/// <sample>LocalObject = Utils.ConvertDataRowToObject<Object>(ds.Tables[0].Rows[0]);</sample>
		public static T ConvertDataRowToObject<T>(DataRow row) {
			#region Method variables

			Type resultType = typeof(T);
			Type colT = null;
			PropertyInfo[] props = resultType.GetProperties();

			object result = Activator.CreateInstance(resultType);
			object colObj = null;
			string tmpColName = string.Empty;

			#endregion

			//  Run through each property on the object and try match it up to
			//  a column in the data row.
			foreach (PropertyInfo item in props) {
				tmpColName = item.Name;
				colT = item.PropertyType;

				//  Make sure we have a column name before continuing...
				if (!string.IsNullOrEmpty(tmpColName)) {
					//  Now lets check if the row contains a column by that property name.
					if (row.Table.Columns.Contains(tmpColName)) {
						//  Extract the value from the datarow...
						colObj = Convert.ChangeType(ConvertDBNull<object>(row[tmpColName],
								InitializeProperty(item.PropertyType)), item.PropertyType);

						//  Set the value of the object to the value in the datarow.
						item.SetValue(result, colObj, null);
					}
				}
			}

			//  Return the result. Even if no properties matched up to columns, 
			//  an instance has been created for the result.
			return (T)result;
		}

		/// <summary>
		/// Initializes the specified type.
		/// This needs work...
		/// </summary>        
		/// <remarks>
		/// Date:   2010-02-08
		/// </remarks>
		/// <param name="propType">Property type to be initialized</param>
		/// <returns>Initialized property</returns>
		private static object InitializeProperty(Type propType) {
			/* ================================================================
			 * I know this is messy, unfortunately I could not find a way
			 * to initialize values types and reference types in a generic
			 * way. I also had problems when using generics and the result
			 * was a double or decimal and I tried to assign the default 
			 * value of an integer (0). Strange huh? If you can find a better
			 * way to do this, please let me know!
			 * ================================================================
			*/

			if (propType == typeof(string))
				return string.Empty;
			else if ((propType == typeof(int)) || (propType == typeof(long)))
				return 0;
			else if (propType == typeof(DateTime))
				return DateTime.MinValue;
			else if (propType == typeof(bool))
				return false;
			else if (propType == typeof(Guid))
				return new Guid();
			else if (propType == typeof(decimal))
				return decimal.Parse("0");
			else if (propType == typeof(double))
				return double.Parse("0");
			else if (propType.IsGenericType &&
					 propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
				return null;

			return new object();
		}

		#endregion

		#region Serializers
		/// <summary>
		/// Methods converts XML document to a JSON string
		/// </summary>
		/// <param name="xmlDoc">Document that contains data to be converted</param>
		/// <returns></returns>
		public static string XmlToJSON(XmlDocument xmlDoc) {
			StringBuilder sbJSON = new StringBuilder();
			sbJSON.Append("{ ");
			XmlToJSONnode(sbJSON, xmlDoc.DocumentElement, true);
			sbJSON.Append("}");
			return sbJSON.ToString();
		}

		/// <summary>
		///  Output an XmlElement, possibly as part of a higher array
		/// </summary>
		/// <param name="sbJSON"></param>
		/// <param name="node"></param>
		/// <param name="showNodeName"></param>
		private static void XmlToJSONnode(StringBuilder sbJSON, XmlElement node, bool showNodeName) {
			if (showNodeName)
				sbJSON.Append("\"" + SafeJSON(node.Name) + "\": ");
			sbJSON.Append("{");
			// Build a sorted list of key-value pairs
			//  where   key is case-sensitive nodeName
			//          value is an ArrayList of string or XmlElement
			//  so that we know whether the nodeName is an array or not.
			SortedList childNodeNames = new SortedList();

			//  Add in all node attributes
			if (node.Attributes != null)
				foreach (XmlAttribute attr in node.Attributes)
					StoreChildNode(childNodeNames, attr.Name, attr.InnerText);

			//  Add in all nodes
			foreach (XmlNode cnode in node.ChildNodes) {
				if (cnode is XmlText)
					StoreChildNode(childNodeNames, "value", cnode.InnerText);
				else if (cnode is XmlElement)
					StoreChildNode(childNodeNames, cnode.Name, cnode);
			}

			// Now output all stored info
			foreach (string childname in childNodeNames.Keys) {
				ArrayList alChild = (ArrayList)childNodeNames[childname];
				if (alChild.Count == 1)
					OutputNode(childname, alChild[0], sbJSON, true);
				else {
					sbJSON.Append(" \"" + SafeJSON(childname) + "\": [ ");
					foreach (object Child in alChild)
						OutputNode(childname, Child, sbJSON, false);
					sbJSON.Remove(sbJSON.Length - 2, 2);
					sbJSON.Append(" ], ");
				}
			}
			sbJSON.Remove(sbJSON.Length - 2, 2);
			sbJSON.Append(" }");
		}

		/// <summary>
		/// Store data associated with each nodeName
		/// so that we know whether the nodeName is an array or not.
		/// </summary>
		/// <param name="childNodeNames"></param>
		/// <param name="nodeName"></param>
		/// <param name="nodeValue"></param>

		private static void StoreChildNode(SortedList childNodeNames, string nodeName, object nodeValue) {
			// Pre-process contraction of XmlElement-s
			if (nodeValue is XmlElement) {
				// Convert  <aa></aa> into "aa":null
				//          <aa>xx</aa> into "aa":"xx"
				XmlNode cnode = (XmlNode)nodeValue;
				if (cnode.Attributes.Count == 0) {
					XmlNodeList children = cnode.ChildNodes;
					if (children.Count == 0)
						nodeValue = null;
					else if (children.Count == 1 && (children[0] is XmlText))
						nodeValue = ((XmlText)(children[0])).InnerText;
				}
			}
			// Add nodeValue to ArrayList associated with each nodeName
			// If nodeName doesn't exist then add it
			object oValuesAL = childNodeNames[nodeName];
			ArrayList ValuesAL;
			if (oValuesAL == null) {
				ValuesAL = new ArrayList();
				childNodeNames[nodeName] = ValuesAL;
			} else
				ValuesAL = (ArrayList)oValuesAL;
			ValuesAL.Add(nodeValue);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="childname"></param>
		/// <param name="alChild"></param>
		/// <param name="sbJSON"></param>
		/// <param name="showNodeName"></param>
		private static void OutputNode(string childname, object alChild, StringBuilder sbJSON, bool showNodeName) {
			if (alChild == null) {
				if (showNodeName)
					sbJSON.Append("\"" + SafeJSON(childname) + "\": ");
				sbJSON.Append("null");
			} else if (alChild is string) {
				if (showNodeName)
					sbJSON.Append("\"" + SafeJSON(childname) + "\": ");
				string sChild = (string)alChild;
				sChild = sChild.Trim();
				sbJSON.Append("\"" + SafeJSON(sChild) + "\"");
			} else
				XmlToJSONnode(sbJSON, (XmlElement)alChild, showNodeName);
			sbJSON.Append(", ");
		}

		/// <summary>
		/// removes any characters that are not compatible with JSON.
		/// </summary>
		/// <param name="sIn"></param>
		/// <returns></returns>
		public static string SafeJSON(string sIn) {
			StringBuilder sbOut = new StringBuilder(sIn.Length);
			foreach (char ch in sIn) {
				if (Char.IsControl(ch) || ch == '\'') {
					int ich = (int)ch;
					sbOut.Append(@"\u" + ich.ToString("x4"));
					continue;
				} else if (ch == '\"' || ch == '\\' || ch == '/') {
					sbOut.Append('\\');
				}
				sbOut.Append(ch);
			}
			return sbOut.ToString();
		}

		public static string EscapeStringValue(string value)
		{
			const char BACK_SLASH = '\\';
			const char SLASH = '/';
			const char DBL_QUOTE = '"';

			var output = new StringBuilder(value.Length);
			foreach (char c in value)
			{
				switch (c)
				{
					case SLASH:
						output.AppendFormat("{0}{1}", BACK_SLASH, SLASH);
						break;

					case BACK_SLASH:
						output.AppendFormat("{0}{0}", BACK_SLASH);
						break;

					case DBL_QUOTE:
						output.AppendFormat("{0}{1}", BACK_SLASH, DBL_QUOTE);
						break;

					default:
						output.Append(c);
						break;
				}
			}

			return output.ToString();
		}

		#endregion

		#region File Operations
		/// <summary>
		/// Requests a file within the applications directory and returns it to the user in a download dialog box.
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="noEncryption">Whether the file path coming in is encrypted or not encrypted</param>
		public static bool DownloadFile(string filePath, bool noEncryption = false) {

			if (!string.IsNullOrEmpty(filePath)) {
				if (!noEncryption) {
					filePath = Security.Encryption.Decrypt(filePath);
				}
				//check if file is safe to download 
				string[] fileExcludeList = ConfigurationManager.AppSettings["FileExcludeList"].Split(',');
				string[] directoryIncludeList = ConfigurationManager.AppSettings["DirectoryIncludeList"].Split(',');

				//validate file requested
				foreach (string extension in fileExcludeList) {
					if (filePath.Contains(extension)) {
						return false;
					}
				}

				//Mark Botts paranoia
				//Check if directory is within specified bounds ie directory include list
				foreach (string directory in directoryIncludeList) {
					if (HttpContext.Current.Server.MapPath(filePath).Contains(directory)) {
						return false;
					}
				}


				if (File.Exists(HttpContext.Current.Server.MapPath(filePath))) {
					HttpContext.Current.Response.ClearContent();
					HttpContext.Current.Response.ContentType = GetMimeType(filePath);
					HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath.Substring(filePath.LastIndexOf("/")));
					HttpContext.Current.Response.TransmitFile(filePath);

					HttpContext.Current.Response.End();
				} else {
					HttpContext.Current.Response.Redirect(HttpContext.Current.Request.UrlReferrer.PathAndQuery + "&response=404");
					return false;
				}
			}

			return true;
		}

		public static void LoadScripts() {
			//TODO: Decide wether to deprecate this method
			//if (CacheManager.Resources == null)
			//{

			//    string[] scriptList = ConfigurationManager.AppSettings["ScriptSources"].Split(',');

			//    foreach (string scriptFile in scriptList)
			//    {

			//        if (File.Exists(HttpContext.Current.Server.MapPath(scriptFile)))
			//        {
			//            CacheManager.Resources += File.ReadAllText(HttpContext.Current.Server.MapPath(scriptFile)) + Environment.NewLine;
			//        }
			//    }

			//}
			//HttpContext.Current.Response.Write(CacheManager.Resources);

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static string GetMimeType(string fileName) {
			string mimeType = "application/unknown";
			string ext = System.IO.Path.GetExtension(fileName).ToLower();
			Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
			if (regKey != null && regKey.GetValue("Content Type") != null)
				mimeType = regKey.GetValue("Content Type").ToString();
			return mimeType;
		}
		#endregion

		#region Service
		/// <summary>
		/// Handles an error on a service
		/// </summary>
		/// <param name="error">The exception that was thrown</param>
		/// <param name="item">the dictionary object expected</param>
		public static void ServiceErrorResultHandler(Exception error, ref Dictionary<string, object> item) {
			if (error.Message != "SessionExpired") {
				SendExceptionMail(error);
			}
			item.Add("ErrorMessage", error.Message);
			object hasData;


			if (!item.TryGetValue("Data", out hasData)) {
				item.Add("Data", string.Empty);
			} else {
				item["Data"] = string.Empty;
			}
			item.Add("Result", false);


		}

		/// <summary>
		/// Handles a successful service call
		/// </summary>
		/// <param name="item">the dictionary expected</param>
		/// <param name="data">the object to be returned by request</param>
		/// <param name="count">items in list</param>
		public static void ServiceSuccessResultHandler(ref Dictionary<string, object> item, object data, int? count) {
			item.Add("Data", data);
			item.Add("Result", true);
			item.Add("Count", count);
			item.Add("ErrorMessage", string.Empty);
		}

		/// <summary>
		/// Throws an exception so that the returning ajax call can tell the client their session has expired.
		/// </summary>
		/// <returns></returns>
		public static void CheckSession() {
			if (SessionManager.ClientSiteID == 0) {
				throw new Exception("SessionExpired");
			}
		}

		/// <summary>
		/// Rearranges pages for the CMS page system
		/// http://manage.boltcms.com/PageAED.aspx, click on re-order pages
		/// </summary>
		/// <param name="service"></param>
		/// <param name="obj"></param>
		/// <param name="parentID"></param>
		public static void RearrangePages(ref Custom.Data.AuroraEntities service, dynamic obj, long parentID) {
			for (int i = 0; i < obj.Length; i++) {
				long ID = long.Parse(obj[i]["id"]);
				if (obj[i].ContainsKey("children")) {
					Utils.RearrangePages(ref service, obj[i]["children"], ID);
				}
				Aurora.Custom.Data.PageContent page = (from p in service.PageContent
													   where p.ID == ID && p.ClientSiteID == SessionManager.ClientSiteID
													   select p).First();
				page.ParentId = parentID;
				page.PageIndex = (page.PageIndex > 0) ? (i + 1) : -(i + 1);
			}
		}

		#endregion

		#region IIS
		/// <summary>
		/// Creates an IIS virtual directory
		/// </summary>
		/// <param name="siteName">The name of the site as it would appear in IIS</param>
		/// <param name="directoryName">The new directory's name </param>
		/// <param name="physicalPath">Where to find the new directory</param>
		/// <returns></returns>
		public static bool CreateIISVirtualDirectory(string siteName, string directoryName, string physicalPath) {
			bool successful;
			try {
				using (DirectoryEntry parent = new DirectoryEntry(string.Format("IIS://{0}/W3SVC/1/Root", siteName))) {
					using (DirectoryEntry newFolder = (DirectoryEntry)parent.Invoke("Create", "IIsWebVirtualDir", directoryName)) {
						newFolder.InvokeSet("Path", physicalPath);  // Does not seem to show up in IIS manager without this.
						newFolder.Invoke("AppCreate", true);
						newFolder.InvokeSet("AppFriendlyName", directoryName);
						newFolder.CommitChanges();
						successful = true;
					}
				}
			} catch (Exception ex) {
				throw ex;
			}

			return successful;
		}

		/// <summary>
		/// Creates an IIS Web site
		/// </summary>
		/// <param name="siteName">the name of the site that will appear in IIS</param>
		/// <param name="protocol">http/https/ftp</param>
		/// <param name="serverIPDomainNameHostHeader">Must be in this format serverIP:Port:HostHeader</param>
		/// <param name="path">Physical path of the new website</param>
		/// <param name="appPool">Which application pool will the site run off</param>
		/// <returns></returns>
		public static bool CreateIISWebsite(string siteName, string protocol, string serverIPDomainNameHostHeader, string path, string appPool) {
			bool successful;
			try {
				using (ServerManager iisManager = new ServerManager()) {
					Site mySite = iisManager.Sites.Add(siteName, protocol, serverIPDomainNameHostHeader, path);
					mySite.Applications[0].ApplicationPoolName = appPool;
					if (serverIPDomainNameHostHeader.Contains("www.")) {
						mySite.Bindings.Add(serverIPDomainNameHostHeader.Replace("www.", string.Empty), protocol);
					}

					iisManager.CommitChanges();
					successful = true;
				}
			} catch (Exception ex) {

				throw ex;
			}

			return successful;
		}

		/// <summary>
		/// Gets the current username of the app pool
		/// </summary>
		/// <returns></returns>
		public static string GetAppPoolUserName() {
			using (ServerManager iisManager = ServerManager.OpenRemote(Environment.MachineName.ToLower())) {

				ApplicationPool appool = null;
				foreach (ApplicationPool applicationPool in iisManager.ApplicationPools) {
					if (applicationPool.Name.Contains("Aurora")) {
						appool = applicationPool;
					}
				}

				if (appool != null && appool.ProcessModel.UserName.ToUpper() != "SAWDSQL") {
					return appool.ProcessModel.UserName;
				}

				return string.Empty;
			}
		}
		/// <summary>
		/// Adds domains to an existing web site
		/// </summary>
		/// <param name="Domains"></param>
		/// <param name="SiteName"></param>
		public static void AddDomainToSite(string[] Domains, string SiteName) {

			foreach (string domain in Domains) {
				//This must be declared here as if a domain has been added, this object becomes void;
				ServerManager iisManager = new ServerManager();
				Site mySite = iisManager.Sites[SiteName];
				bool exists = false;
				foreach (Binding binding in mySite.Bindings) {
					if (binding.Host == domain) {
						exists = true;
					}
				}

				if (!exists) {
					mySite.Bindings.Add(ConfigurationManager.AppSettings["ServerIP"] + ":80:" + domain, "http");
					iisManager.CommitChanges();
					iisManager.Dispose();
				}

			}

		}
		/// <summary>
		/// Removes a website from iis
		/// </summary>
		/// <param name="SiteName"></param>
		public static void DeleteSite(string SiteName) {
			ServerManager iisManager = new ServerManager();
			Site mySite = iisManager.Sites[SiteName];
			if (mySite == null) {
				return;
			}
			iisManager.Sites.Remove(mySite);
			iisManager.CommitChanges();
		}
		#endregion

		#region Secure Pages
		public static string SetGetSecureMenu() {
			if (SessionManager.VistorSecureSessionID != Guid.Empty) {
				if (String.IsNullOrEmpty(CacheManager.SecureMenu)) {


					AuroraEntities entity = new AuroraEntities();
					StringBuilder menu = new StringBuilder();
					var roles = (from memberRoles in entity.MemberRole
								 where memberRoles.MemberID == SessionManager.MemberData.ID
								 select memberRoles.RoleID).ToList<long>();

					var menuItems = from roleModule in entity.RoleModule
									where roles.Contains(roleModule.RoleID)
									group roleModule by roleModule.ModuleItemID
										into groupedResults
										select groupedResults;

					foreach (System.Linq.IGrouping<long, RoleModule> roleModules in menuItems) {
						string itemTitle = string.Empty;
						switch (roleModules.First().ModuleName) {
							case "News":
								itemTitle = (from news in entity.News
											 where news.ID == roleModules.Key
											 select news.Title).First();
								break;
							case "Events":
								itemTitle = (from events in entity.Event
											 where events.ID == roleModules.Key
											 select events.Title).First();
								break;
							case "PageContent":
								break;
						}
						menu.Append(String.Format("<li><a dataid='{0}' datatitle='{1} href=\"/{1}/{0}/{2}\">{2}</a></li>",
												  roleModules.First().ModuleItemID, roleModules.First().ModuleName,
												  itemTitle));
					}

					CacheManager.SecureMenu = menu.ToString();
				}
				return CacheManager.SecureMenu;
			}

			return string.Empty;

		}

		public static string GetMD5Hash(string input) {
			System.Security.Cryptography.MD5CryptoServiceProvider crypto = new System.Security.Cryptography.MD5CryptoServiceProvider();
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
			bytes = crypto.ComputeHash(bytes);
			System.Text.StringBuilder newString = new System.Text.StringBuilder();
			foreach (byte b in bytes) {
				newString.Append(b.ToString("x2").ToLower());
			}
			string encyptedString = newString.ToString();
			return encyptedString;
		}
		static public string EncodeTo64(string toEncode) {
			byte[] toEncodeAsBytes
				  = ASCIIEncoding.ASCII.GetBytes(toEncode);
			string returnValue
				  = Convert.ToBase64String(toEncodeAsBytes);
			return returnValue;
		}
		static public string DecodeFrom64(string encodedData) {
			byte[] encodedDataAsBytes
				= Convert.FromBase64String(encodedData);
			string returnValue =
			   ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
			return returnValue;
		}
		public static Dictionary<string, string> GetVoucherHash(long voucherID) {
			string batchNumber = voucherID.ToString() + "|";
			string encryptedBatchNumber = Utils.EncodeTo64(batchNumber);
			string toEncrypt = batchNumber + Guid.NewGuid().ToString().Substring(0, 8);
			string encrypted = EncodeTo64(toEncrypt);

			Dictionary<string, string> voucherCode = new Dictionary<string, string>();
			voucherCode.Add("VoucherBatchID", encryptedBatchNumber);
			voucherCode.Add("VoucherCode", encrypted.Replace("=", String.Empty).Replace(encryptedBatchNumber, String.Empty));

			return voucherCode;
		}
		#endregion

		#region XML
		public static Dictionary<string, string> GetFieldsFromCustomXML(string ModuleName) {
			Dictionary<string, string> data = new Dictionary<string, string>();
			AuroraEntities entities = new AuroraEntities();

			var moduleData = (from featureModules in entities.Module
							  join clientModule in entities.ClientModule on featureModules.ID equals
								  clientModule.FeaturesModuleID
							  where featureModules.Name == ModuleName
							  where clientModule.ClientSiteID == SessionManager.ClientSiteID && clientModule.DeletedOn == null
							  select new { clientModule, featureModules }).FirstOrDefault();


			XmlDocument xmlConfigData = new XmlDocument();
			if (moduleData != null && (moduleData.clientModule.ConfigXML ?? moduleData.featureModules.ConfigXML) != null) {
				xmlConfigData.LoadXml(moduleData.clientModule.ConfigXML ?? moduleData.featureModules.ConfigXML);

				XmlNodeList nodes = xmlConfigData.GetElementsByTagName("field");

				foreach (XmlNode node in nodes) {
					data.Add(node.Attributes["fieldname"].Value, node.Attributes["value"].Value);
				}

			}

			return data;
		}
		public static Dictionary<string, string> GetFieldsForXmlData(string XData, bool UseFieldNameInsteadOfTitle = false) {
			Dictionary<string, string> data = new Dictionary<string, string>();

			XmlDocument xmlConfigData = new XmlDocument();
			if (XData != null) {
				xmlConfigData.LoadXml(XData);

				XmlNodeList nodes = xmlConfigData.GetElementsByTagName("field");

				foreach (XmlNode node in nodes) {
					if (node.Attributes["hide"] == null) {
						if (!UseFieldNameInsteadOfTitle) {
							data.Add(node.Attributes["title"].Value, node.Attributes["value"].Value);
						} else {
							data.Add(node.Attributes["fieldname"].Value, node.Attributes["value"].Value);
						}
					}
				}

			}

			return data;
		}

		/// <summary>
		/// Generates an xmlDocument object that can be passed to the communication engine.
		/// </summary>
		/// <param name="dataList">A list of objects containing user information and custom fields.</param>
		/// <returns></returns>
		public static string GenerateCommunicationXML<T>(List<T> dataList) {
			XmlDocument xmlData = new XmlDocument();
			XmlElement root = xmlData.CreateElement("xmldata");
			XmlElement users = xmlData.CreateElement("users");
			xmlData.AppendChild(root);
			root.AppendChild(users);
			foreach (T obj in dataList) {
				XmlElement user = xmlData.CreateElement("user");
				Type resultType = obj.GetType();
				PropertyInfo[] props = resultType.GetProperties();

				foreach (PropertyInfo item in props) {
					user.SetAttribute(item.Name.ToLower(), Convert.ToString(item.GetValue(obj, null)));
				}
				users.AppendChild(user);
			}
			return xmlData.OuterXml;
		}

		/// <summary>
		/// Updates an xml document from another xml source
		/// </summary>
		/// <param name="sourceXML">the xml document to obtain data</param>
		/// <param name="destinationXML">the xml document to update</param>
		/// <param name="nodeName">the xml node name to search for</param>
		/// <returns>XML destination doc</returns>
		public static XmlDocument UpdateXml(ref XmlDocument sourceXML, ref XmlDocument destinationXML, string nodeName) {

			foreach (XmlNode xmlNode in destinationXML.GetElementsByTagName(nodeName)) {
				XmlNodeList nodes = sourceXML.SelectNodes("//Fields//field[(@fieldname='" + xmlNode.Attributes["fieldname"].Value + "')]");
				foreach (XmlAttribute xmlAttribute in xmlNode.Attributes) {
					if (nodes.Count > 0) {
						string nodeValue = string.Empty;

						foreach (XmlAttribute nodeItemData in nodes.Item(0).Attributes) {
							if (xmlAttribute.Name.ToString().ToLower() == "value") {
								nodeValue = nodes.Item(0).Attributes[xmlAttribute.Name].Value;
							}
						}

						if (nodeValue != string.Empty && nodeValue != xmlAttribute.Value) {
							xmlAttribute.Value = nodes.Item(0).Attributes[xmlAttribute.Name].Value;
						}
					}

				}
			}

			return destinationXML;
		}

		public static string EncodeXML(string content) {
			content = content.Replace("&", "&amp;")
							 .Replace("<", "&lt;")
							 .Replace(">", "&gt;")
							 .Replace("'", "&#39;")
							 .Replace("\"", "&quot;");
			return content;
		}
		#endregion

		#region String
		/// <summary>
		/// Removes any special chars within a string
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string RemoveSpecialCharacters(string str) {
			StringBuilder sb = new StringBuilder();
			foreach (char c in str) {
				if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == ' ') {
					sb.Append(c);
				}
			}
			return sb.ToString();
		}
		/// <summary>
		/// used to pad an  int 1 - 01
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		public static string PadZero(object val) {
			if (val.ToString().Length == 1) {
				return "0" + val.ToString();
			} else {
				return val.ToString();
			}
		}
		#endregion

		#region PDF
		/// <summary>
		/// Sets up PDF document styles
		/// </summary>
		/// <param name="document"></param>
		public static void DefineStyles(Document document) {
			// Get the predefined style Normal.
			Style style = document.Styles["Normal"];
			// Because all styles are derived from Normal, the next line changes the 
			// font of the whole document. Or, more exactly, it changes the font of
			// all styles and paragraphs that do not redefine the font.
			style.Font.Name = "Arial";

			// Heading1 to Heading9 are predefined styles with an outline level. An outline level
			// other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
			// in PDF.

			style = document.Styles["Heading1"];
			style.Font.Name = "Tahoma";
			style.Font.Size = 14;
			style.Font.Bold = true;
			style.Font.Color = Colors.DarkBlue;
			style.ParagraphFormat.PageBreakBefore = true;
			style.ParagraphFormat.SpaceAfter = 10;
			style.ParagraphFormat.Font.Color = Colors.Red;

			style = document.Styles[StyleNames.Header];
			style.ParagraphFormat.Alignment = ParagraphAlignment.Center;

			style = document.Styles["Heading2"];
			style.Font.Size = 12;
			style.Font.Bold = true;
			style.ParagraphFormat.PageBreakBefore = false;
			style.ParagraphFormat.SpaceBefore = 6;
			style.ParagraphFormat.SpaceAfter = 6;
			style.ParagraphFormat.Font.Color = Colors.Red;

			style = document.Styles["Heading3"];
			style.Font.Size = 10;
			style.Font.Bold = true;
			style.Font.Italic = true;
			style.ParagraphFormat.SpaceBefore = 6;
			style.ParagraphFormat.SpaceAfter = 3;
			style.ParagraphFormat.Font.Color = Colors.Red;

			style = document.Styles[StyleNames.Header];
			style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

			style = document.Styles[StyleNames.Footer];
			style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

			// Create a new style called TextBox based on style Normal
			style = document.Styles.AddStyle("TextBox", "Normal");
			style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
			style.ParagraphFormat.Borders.Width = 1.0;
			style.ParagraphFormat.Borders.Distance = "3pt";
			//TODO: Colors
			style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

			// Create a new style called TOC based on style Normal
			style = document.Styles.AddStyle("TOC", "Normal");
			style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
			style.ParagraphFormat.Font.Color = Colors.Red;

			//Create styles for header
			style = document.Styles[StyleNames.Header];
			style.ParagraphFormat.Alignment = ParagraphAlignment.Right;
			style.ParagraphFormat.Font.Bold = true;
			style.ParagraphFormat.Font.Color = Colors.Red;
			style.ParagraphFormat.Font.Size = 15;



			//create styles for footer
			style = document.Styles[StyleNames.Footer];
			style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
			style.ParagraphFormat.Font.Color = Colors.Red;


			//create styles for hyper link 
			style = document.Styles[StyleNames.Hyperlink];
			style.Font.Color = Colors.Red;
			style.Font.Bold = true;

		}
		/// <summary>
		/// For future use
		/// </summary>
		/// <param name="Html"></param>
		public static void CreateFromHtmlPDF(string Html) {
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(Html);
			Document document = new Document();
			document.Info.Title = "Test";
			DefineStyles(document);

			foreach (HtmlNode selectNode in doc.DocumentNode.SelectNodes("//tr")) {
				// Each MigraDoc document needs at least one section.
				Section section = document.AddSection();
				// Create footer
				Paragraph paragraph = section.Footers.Primary.AddParagraph();
				paragraph.AddText(CacheManager.SiteData.CompanyName);
				paragraph.Format.Font.Size = 9;
				paragraph.Format.Alignment = ParagraphAlignment.Center;
				// Create the item table
				Table table = new Table();
				table.Style = "Table";
				table.Borders.Color = new Color(0, 0, 0);
				table.Borders.Width = 0.25;
				table.Borders.Left.Width = 0.5;
				table.Borders.Right.Width = 0.5;
				table.Rows.LeftIndent = 0;


				// Before you can add a row, you must define the columns
				Column column = table.AddColumn("4cm");
				column.Format.Alignment = ParagraphAlignment.Center;

				column = table.AddColumn("2.5cm");
				column.Format.Alignment = ParagraphAlignment.Right;

				column = table.AddColumn("10cm");
				column.Format.Alignment = ParagraphAlignment.Right;
				Row newRow = table.AddRow();

				foreach (HtmlNode htmlTableCell in selectNode.Descendants("td")) {
					var imageRow = htmlTableCell.Descendants("img").FirstOrDefault();
					var heading = htmlTableCell.Descendants("b").FirstOrDefault();
					var text = htmlTableCell.InnerText;

					if (heading != null) {
						newRow.Cells[1].AddParagraph(heading.InnerText);
						newRow.Format.Font.Bold = true;
						newRow.Cells[1].Format.Alignment = ParagraphAlignment.Left;
						newRow.Cells[1].VerticalAlignment = VerticalAlignment.Bottom;
					} else if (imageRow != null) {
						Image image =
							new Image("C:\\" + "Development\\Aurora\\Aurora.Web\\" +
									  imageRow.Attributes["src"].Value.Replace("/", "\\"));
						image.Height = "2.5cm";
						image.LockAspectRatio = true;
						image.RelativeVertical = RelativeVertical.Line;
						image.RelativeHorizontal = RelativeHorizontal.Margin;
						image.Top = ShapePosition.Top;
						image.Left = ShapePosition.Right;
						image.WrapFormat.Style = WrapStyle.Through;

						newRow.Cells[0].Add(image);
						newRow.Cells[0].Format.Font.Bold = false;
						newRow.Cells[0].Format.Alignment = ParagraphAlignment.Left;
						newRow.Cells[0].VerticalAlignment = VerticalAlignment.Top;
						newRow.Cells[0].MergeDown = 9;
					} else if (imageRow == null && heading == null) {
						newRow.Cells[2].AddParagraph(text);
						newRow.Cells[2].Format.Alignment = ParagraphAlignment.Left;
						newRow.Cells[2].VerticalAlignment = VerticalAlignment.Bottom;

					}

				}
				table.SetEdge(0, 0, table.Columns.Count, table.Rows.Count, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
				document.LastSection.Add(table);

			}





			// Create a renderer for the MigraDoc document.
			const bool unicode = false;
			const PdfFontEmbedding embedding = PdfFontEmbedding.Always;

			PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding) { Document = document };
			pdfRenderer.RenderDocument();
			pdfRenderer.PdfDocument.Save(String.Format("C:\\PDF\\B2M_{0}.pdf", DateTime.Now.ToString("HH-mm-ss")));
		}
		/// <summary>
		/// Creates a 4 column pdf document from  a dataset parsed in as a dynamic array
		/// </summary>
		/// <param name="data"></param>
		/// <param name="documentTitle"></param>
		public static string CreatePDFDocument(List<dynamic> data, string documentTitle) {
			//Create document that will be saved
			Document document = new Document();
			document.Info.Title = documentTitle;
			document.Info.Author = CacheManager.SiteData.CompanyName;
			//Every document must have a section
			Section defaultSection = document.AddSection();
			HeaderFooter documentHeader = defaultSection.Headers.Primary;
			HeaderFooter documentFooter = defaultSection.Footers.Primary;

			Paragraph paragraphFooter = documentFooter.AddParagraph();
			paragraphFooter.AddText(CacheManager.SiteData.CompanyName + " - " + CacheManager.SiteData.Email1 + " - " +
									CacheManager.SiteData.Tel1);
			paragraphFooter.AddHyperlink("http://www.baby2mom.co.za", HyperlinkType.Web).AddText(" - www.baby2mom.co.za");


			//Define font styles,colors etc.
			DefineStyles(document);
			//Take the data and form a table
			CreateSimpleTableForPdf(document, data);
			//Render the document into a PDF
			PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always) {
				Document = document
			};


			renderer.RenderDocument();

			//check for save directory
			string directory = HttpContext.Current.Server.MapPath(String.Format("/ClientData/{0}/Uploads/PDF", SessionManager.ClientSiteID));
			if (!Directory.Exists(directory)) {
				Directory.CreateDirectory(directory);
			}

			// Save the document...
			string filename = String.Format("/ClientData/{0}/Uploads/PDF/{1}.pdf", SessionManager.ClientSiteID, Guid.NewGuid());
			renderer.PdfDocument.Save(HttpContext.Current.Server.MapPath(filename));
			return filename;
		}
		/// <summary>
		/// Creates a table on the PDF document
		/// </summary>
		/// <param name="document"></param>
		/// <param name="data"></param>
		public static void CreateSimpleTableForPdf(Document document, List<dynamic> data) {
			//Regions
			var regions = new Dictionary<int, string> {
                {1	,"Johannesburg"},
                {3	,"Pretoria"},
                {6	,"Cape Town"},  
                {8	,"Bloemfontein"},
                {17	,"Durban"},
                {22	,"Mpumalanga"},
                {23	,"Port Elizabeth"},
                {24	,"Grahamstown"},
                {25	,"East London"},  
                {26	,"George"}        
            };
			foreach (var item in data) {
				//bookmark heading
				Image headerImage =
				document.LastSection.AddImage(HttpContext.Current.Server.MapPath("~/ClientData/10016/Styles/images/header_wishlist.jpg"));
				headerImage.Width = 500;
				document.LastSection.AddParagraph(item[8], "Heading2");

				Table table = new Table();
				table.Borders.Width = 0;

				Column column = table.AddColumn(Unit.FromCentimeter(4));
				column.Format.Alignment = ParagraphAlignment.Center;

				table.AddColumn(Unit.FromCentimeter(6));
				table.AddColumn(Unit.FromCentimeter(8));


				//Add image
				string imageFilePath = HttpContext.Current.Server.MapPath("~/ClientData/10016/Uploads/Members/" +
																		  item.ItemArray[0] + "/" + item.ItemArray[0] +
																		  "_01.jpg");
				if (File.Exists(imageFilePath)) {

				} else {
					imageFilePath = HttpContext.Current.Server.MapPath(
										String.Format("~/ClientData/{0}/Styles/images/no_photo_available.png",
													  SessionManager.ClientSiteID));
				}
				Image image =
					new Image(imageFilePath);

				image.Width = "3cm";
				image.LockAspectRatio = true;
				image.RelativeVertical = RelativeVertical.Line;
				image.RelativeHorizontal = RelativeHorizontal.Margin;
				image.Top = ShapePosition.Top;
				image.Left = ShapePosition.Right;
				image.WrapFormat.Style = WrapStyle.Through;


				int counter = 0;
				foreach (dynamic itemColumns in item.ItemArray) {
					if (counter > 0) {
						if (item.Table.Columns[counter].ColumnName != "Egg Reference #:"
							&& item.Table.Columns[counter].ColumnName != "Old Reference#:"
							&& item.Table.Columns[counter].ColumnName != "Region:"
							&& item.Table.Columns[counter].ColumnName != "User ID:"
							) {


							Row row = table.AddRow();

							//Field Label
							Cell cell = row.Cells[1];
							cell.AddParagraph(item.Table.Columns[counter].ColumnName);
							cell.Format.Font.Bold = true;

							//add Cell Content
							cell = row.Cells[2];
							if (item.Table.Columns[counter].ColumnName == "Face Shape:") {
								string faceFileName =
									HttpContext.Current.Server.MapPath(
										String.Format("~/ClientData/{0}/Styles/images/faceshapes/{1}.jpg",
													  SessionManager.ClientSiteID, itemColumns));

								if (File.Exists(faceFileName)) {
									cell.AddImage(faceFileName);
								} else {
									cell.AddParagraph(itemColumns.ToString().Replace("<br/>", Environment.NewLine));
								}


							} else if (item.Table.Columns[counter].ColumnName == "Region:") {
								//int regionID;
								//int.TryParse(itemColumns.ToString(), out regionID);
								//cell.AddParagraph(regions[regionID]);
							} else {

								cell.AddParagraph(itemColumns.ToString().Replace("<br/>", Environment.NewLine));
							}



							if (counter == 1) {
								Cell imageCell = row.Cells[0];
								imageCell.Add(image);
								imageCell.Format.Font.Bold = false;
								imageCell.Format.Alignment = ParagraphAlignment.Left;
								imageCell.VerticalAlignment = VerticalAlignment.Top;
								row.Cells[0].MergeDown = 9;
							}
						}

					}


					counter++;
				}

				//add link to website 
				Row linkRow = table.AddRow();

				//Field Label
				Cell linkCell = linkRow.Cells[1];
				linkCell.AddParagraph("Click to view Donor Profile:");
				linkCell.Format.Font.Bold = true;

				//add Cell Content
				linkCell = linkRow.Cells[2];
				Paragraph hyperLnkParagraph = linkCell.AddParagraph();
				Hyperlink hyperLnk;
				if (string.IsNullOrEmpty(item.ItemArray[15])) {
					hyperLnk = hyperLnkParagraph.AddHyperlink(String.Format("http://baby2mom.co.za/ClientData/10016/Pages/DonorView.aspx?single={0}", item.ItemArray[0]),
																	HyperlinkType.Web);
				} else {
					hyperLnk = hyperLnkParagraph.AddHyperlink(String.Format("http://baby2mom.co.za/ClientData/10016/Pages/DonorView.aspx?single={0}", item.ItemArray[15]),
																	HyperlinkType.Web);
				}

				Text hypText = hyperLnk.AddText("View More");


				table.SetEdge(0, 0, table.Columns.Count, table.Rows.Count, Edge.Box, BorderStyle.None, 0, Colors.Transparent);
				document.LastSection.Add(table);
				if (item != data[data.Count() - 1]) {
					document.LastSection.AddPageBreak();
				}

			}


		}
		#endregion

		#region Password
		/// <summary>
		/// Creates a random password
		/// </summary>
		/// <param name="passwordLength"></param>
		/// <returns></returns>
		public static string CreateRandomPassword(int passwordLength) {
			string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
			char[] chars = new char[passwordLength];
			Random rd = new Random();

			for (int i = 0; i < passwordLength; i++) {
				chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
			}

			return new string(chars);
		}
		#endregion

		#region Image
		private Bitmap Resize(System.Drawing.Image imgPhoto, int imgWidth, int imgHeight) {
			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			const int sourceX = 0;
			const int sourceY = 0;
			int destX = 0;
			int destY = 0;

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)imgWidth / (float)sourceWidth);
			nPercentH = ((float)imgHeight / (float)sourceHeight);
			if (nPercentH < nPercentW) {
				nPercent = nPercentH;
				destX = System.Convert.ToInt16((imgWidth -
							  (sourceWidth * nPercent)) / 2);
			} else {
				nPercent = nPercentW;
				destY = System.Convert.ToInt16((imgHeight -
							  (sourceHeight * nPercent)) / 2);
			}

			int destWidth = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			Bitmap bmPhoto = new Bitmap(imgWidth, imgHeight,
							  PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
							 imgPhoto.VerticalResolution);
			using (bmPhoto) {
				Graphics grPhoto = Graphics.FromImage(bmPhoto);

				grPhoto.Clear(System.Drawing.Color.White);
				grPhoto.InterpolationMode =
						InterpolationMode.HighQualityBicubic;

				grPhoto.DrawImage(imgPhoto,
					new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
					new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
					GraphicsUnit.Pixel);


				imgPhoto.Dispose();
				grPhoto.Dispose();
				return bmPhoto;
			}

		}
		#endregion

		#region Message Centre
		/// <summary>
		/// Sends communication out of the system in form of email
		/// </summary>
		/// <param name="eventName">The name of the event type</param>
		/// <param name="additionalMessages">any additional messages that will appear underneath the specified event message</param>
		/// <param name="attachment">files to add to the email</param>
		/// <param name="subject">subject of the email</param>
		/// <param name="userXML">Comm engine XML</param>
		/// <param name="fromEmail">the email address that will appear in the from field</param>
		/// <param name="clientSiteID">the client requesting this email to be sent</param>
		/// <returns></returns>
		public static Guid SendToMessageCentre(string messageEventName, string additionalMessages, string attachment, string subject, string userXML, string fromEmail = "support@sawebdesign.co.za", long clientSiteID = 10000) {
            Guid returnVal = Guid.Empty;
            AuroraEntities entities = new AuroraEntities();
            try {
				long? messageEventID;
				
                //get the id for the event
				messageEventID = (from messageEvents in entities.MessageEvent
									where messageEvents.EventType == messageEventName
									&& (messageEvents.ClientSiteID == clientSiteID || messageEvents.ClientSiteID == null)
									select messageEvents.ID).FirstOrDefault();

                if (messageEventID == null || messageEventID == 0) {
                    return returnVal;
                }

				if (clientSiteID == 10000 && SessionManager.ClientSiteID != 10000) {
					bool hasOveride = entities.Message.Where(w => w.ClientSiteID == SessionManager.ClientSiteID && w.MessageEventID == messageEventID && w.DeletedOn == null).Count() > 0;
					if (hasOveride) {
						clientSiteID = SessionManager.ClientSiteID.Value;
					}
				}

                Utils.CheckSqlDBConnection();
                if (attachment == string.Empty) {
                    attachment = null;
                }

                if (CacheManager.SiteData.CommEngineSiteID == null) {
                    using (SqlCommand commCmd = new SqlCommand()) {
                        commCmd.CommandType = CommandType.StoredProcedure;
                        commCmd.CommandText = "[CommEngine].[dbo].[proc_Site_Insert]";
                        commCmd.Parameters.AddWithValue("@SiteName", CacheManager.SiteData.CompanyName);
                        commCmd.Parameters.AddWithValue("@URL", HttpContext.Current.Request.ServerVariables["SERVER_NAME"]);
                        Guid commEngineID = new Guid(Utils.SqlDb.ExecuteScalar(commCmd).ToString());
                        AuroraEntities context = new AuroraEntities();
                        ClientSite site = (from sites in context.ClientSite
                                           where sites.ClientSiteID == CacheManager.SiteData.ClientSiteID
                                           select sites).FirstOrDefault();
                        site.CommEngineSiteID = commEngineID;
                        context.SaveChanges();
                        CacheManager.SiteData.CommEngineSiteID = site.CommEngineSiteID;
                    }
                }

                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[Features].[proc_MessageCentre_InsertIntoCommEngine]";
                    cmd.Parameters.AddWithValue("@ClientSiteID", clientSiteID);//not being used yet
                    cmd.Parameters.AddWithValue("@AdditionMessage", additionalMessages);
                    cmd.Parameters.AddWithValue("@UserXML", userXML.Replace("email1", "email"));
                    cmd.Parameters.AddWithValue("@SiteID", CacheManager.SiteData.CommEngineSiteID.Value.ToString());
                    cmd.Parameters.AddWithValue("@EventID", messageEventID);
                    cmd.Parameters.AddWithValue("@Attachments", attachment);
                    cmd.Parameters.AddWithValue("@FromEmail", fromEmail);
                    cmd.Parameters.AddWithValue("@Subject", subject);
                    returnVal = new Guid(Utils.ConvertDBNull(Utils.SqlDb.ExecuteScalar(cmd), Guid.Empty).ToString());

                }
            } catch (Exception ex) {
                throw ex;
            }

            return returnVal;
        }

		public static Guid SendMemberCategoryMessageEmail(long memberCategoryID, int day, bool isReversed) {
			Guid returnVal = Guid.Empty;
			try {
				Utils.CheckSqlDBConnection();
				Utils.CheckSession();

				using (AuroraEntities context = new AuroraEntities()) {
					long? categoryID = context.MemberRole.Where(w => w.ID == memberCategoryID).Select(s => s.RoleID).FirstOrDefault();
					var commsData = from mr in context.MemberRole
									join mem in context.Member on mr.MemberID equals mem.ID
									join cat in context.Role on mr.RoleID equals cat.ID
									join site in context.ClientSite on mr.ClientSiteID equals site.ClientSiteID
									where mr.ClientSiteID == SessionManager.ClientSiteID
									&& mr.ID == memberCategoryID
									select new {
										CategoryName = cat.Name,
										SiteName = site.CompanyName,
										FirstName = mem.FirstName,
										LastName = mem.LastName,
										Email = mem.Email
									};
					if (commsData.Count() == 0) {
						throw new Exception("No Member found to send message to");
					}
					MemberCategoryMessage message = (from msgs in context.MemberCategoryMessage
													 where msgs.ClientSiteID == SessionManager.ClientSiteID
													 && msgs.Day == day
													 && msgs.IsReversed == isReversed
													 && msgs.MemberCategoryID == categoryID
													 select msgs).FirstOrDefault();
					if (message == null) {
						// Retrieve the default message (if any) in the event that one doesn't exist for this client;
						message = context.MemberCategoryMessage.Where(w => w.ClientSiteID == null && w.Day == day && w.IsReversed == isReversed).FirstOrDefault();
					}
					if (message == null) {
						throw new Exception("No Client Message or Default Message exists");
					}

					string commsXML = GenerateCommunicationXML(commsData.ToList());
					if (CacheManager.SiteData.CommEngineSiteID == null) {
						using (SqlCommand commCmd = new SqlCommand()) {
							commCmd.CommandType = CommandType.StoredProcedure;
							commCmd.CommandText = "[CommEngine].[dbo].[proc_Site_Insert]";
							commCmd.Parameters.AddWithValue("@SiteName", CacheManager.SiteData.CompanyName);
							commCmd.Parameters.AddWithValue("@URL", HttpContext.Current.Request.ServerVariables["SERVER_NAME"]);
							Guid commEngineID = new Guid(Utils.SqlDb.ExecuteScalar(commCmd).ToString());
							ClientSite site = (from sites in context.ClientSite
											   where sites.ClientSiteID == CacheManager.SiteData.ClientSiteID
											   select sites).FirstOrDefault();
							site.CommEngineSiteID = commEngineID;
							context.SaveChanges();
							CacheManager.SiteData.CommEngineSiteID = site.CommEngineSiteID;
						}
					}
					using (SqlCommand cmd = new SqlCommand()) {
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = "[CommEngine].[dbo].[proc_Message_Insert]";
						cmd.Parameters.AddWithValue("@SiteID", CacheManager.SiteData.CommEngineSiteID);
						cmd.Parameters.AddWithValue("@Site", CacheManager.SiteData.CompanyName);
						cmd.Parameters.AddWithValue("@MessageType", "Email");
						cmd.Parameters.AddWithValue("@Subject", message.Subject);
						cmd.Parameters.AddWithValue("@Message", message.HTML);
						cmd.Parameters.AddWithValue("@FromEmail", CacheManager.SiteData.Email1);
						cmd.Parameters.AddWithValue("@XMLData", commsXML);
						cmd.Parameters.AddWithValue("@InsertedBy", SessionManager.UserName ?? "System");
						cmd.Parameters.AddWithValue("@IP", "127.0.0.1");
						returnVal = new Guid(Utils.ConvertDBNull(Utils.SqlDb.ExecuteScalar(cmd), Guid.Empty).ToString());
					}
				}
			} catch (Exception ex) {
				throw ex;
			}

			return returnVal;
		}

		#endregion

		#region Image Manipulation
		/// <summary>
		/// Resizes an image of any type to a desired width/ a desired height.
		/// Aspect ratios are sustained during this process
		/// </summary>
		/// <param name="absolutefilePath">The path of the original file to be manipulated</param>
		/// <param name="desiredSize">Width/Height desired</param>
		/// <param name="newFileName">Manipulated image's file path</param>
		/// <param name="useHeight">true for height restriction only</param>
		/// <returns>Boolean</returns>
		public static bool ResizeImageFromFile(string absolutefilePath, int desiredSize, string newFileName, bool useHeight = false) {
			System.Drawing.Image srcImage = System.Drawing.Image.FromFile(absolutefilePath);
			int newHeight, newWidth, currentHeight = srcImage.Height, currentWidth = srcImage.Width;
			decimal convertedHeight, convertedWidth;
			ImageCodecInfo myImageCodecInfo;
			System.Drawing.Imaging.Encoder myEncoder;
			EncoderParameter myEncoderParameter;
			EncoderParameters myEncoderParameters;

			// Get an ImageCodecInfo object that represents the JPEG codec.
			myImageCodecInfo = GetEncoderInfo("image/jpeg");

			// Create an Encoder object based on the GUID

			// for the Quality parameter category.
			myEncoder = System.Drawing.Imaging.Encoder.Quality;

			// Create an EncoderParameters object.

			// An EncoderParameters object has an array of EncoderParameter

			// objects. In this case, there is only one

			// EncoderParameter object in the array.
			myEncoderParameters = new EncoderParameters(1);

			// Save the bitmap as a JPEG file with quality level 75.
			myEncoderParameter = new EncoderParameter(myEncoder, 75L);
			myEncoderParameters.Param[0] = myEncoderParameter;

			if (useHeight) {
				convertedWidth = (decimal)desiredSize / currentHeight;
				convertedWidth = convertedWidth * currentWidth;
				newHeight = desiredSize;
				newWidth = (int)convertedWidth;
			} else {

				convertedHeight = (decimal)desiredSize / currentWidth;
				convertedHeight = convertedHeight * currentHeight;
				newHeight = (int)convertedHeight;
				newWidth = (int)desiredSize;
			}

			Bitmap newImage = new Bitmap(newWidth, newHeight);
			using (Graphics gr = Graphics.FromImage(newImage)) {
				gr.SmoothingMode = SmoothingMode.AntiAlias;
				gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
				gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
				gr.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
				newImage.SetResolution(72, 72);
				if (newFileName == absolutefilePath) {
					srcImage.Dispose();
					File.Delete(absolutefilePath);
					newImage.Save(absolutefilePath, myImageCodecInfo, myEncoderParameters);
				} else {
					newImage.Save(newFileName, myImageCodecInfo, myEncoderParameters);
				}
			}

			return true;
		}

		private static ImageCodecInfo GetEncoderInfo(String mimeType) {
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for (j = 0; j < encoders.Length; ++j) {
				if (encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}

		#endregion

		#region  Validation

		public static bool isValidEmail(string emailStr) {
			if (!string.IsNullOrEmpty(emailStr)) {
				Regex emailReg = new Regex("[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
				return emailReg.IsMatch(emailStr);
			} else {
				return false;
			}
		}

		#endregion

		/// <summary>
		/// Recursively marks categories as replaced.
		/// </summary>
		/// <param name="memberCategoryID"></param>
		public static void markCategoryAsReplaced(long memberCategoryID) {
			using (AuroraEntities entities = new AuroraEntities()) {
				long? memcat = memberCategoryID;
				while (memcat != null) {
					var memberCategory = (from cats in entities.MemberRole
										  where cats.ID == memcat
										   && cats.Replaced == null
										  select cats).FirstOrDefault();
					if (memberCategory != null) {
						memberCategory.Replaced = true;
						memcat = memberCategory.Replacing;
					} else {
						memcat = null;
					}
				}
				entities.SaveChanges();
			}
		}

		public static IEnumerable<RoleModule> CheckItemSecurity(string moduleName, long moduleItemID) {
			IEnumerable<RoleModule> roleList;
			using (AuroraEntities entities = new AuroraEntities()) {
				roleList = (from roleModule in entities.RoleModule
							where roleModule.ModuleName == moduleName
								  && roleModule.ModuleItemID == moduleItemID
							select roleModule
								).ToList();
				return roleList;
			}

		}

		public static string SecureMenu() {
			//get config 
			Dictionary<string, string> memberarea = Utils.GetFieldsFromCustomXML("Members");

			if (memberarea.Count == 0) {
				return "";
			}

			bool menuOn = false;
			var mnu = from mem in memberarea
					  where mem.Key == "Member_MenuOnOff"
					  select mem;
			string menuName;

			if (SessionManager.VistorSecureSessionID != Guid.Empty && SessionManager.MemberData != null) {

				AuroraEntities entity = new AuroraEntities();
				StringBuilder membersMenu = new StringBuilder();
				menuName = !string.IsNullOrEmpty(memberarea["Member_NameOverride"])
									  ? memberarea["Member_NameOverride"]
									  : "Members";
				var roles = (from memberRoles in entity.MemberRole
							 where memberRoles.MemberID == SessionManager.MemberData.ID
							 select memberRoles.RoleID).ToList<long>();

				var menuItems = from roleModule in entity.RoleModule
								where roles.Contains(roleModule.RoleID)
								group roleModule by roleModule.ModuleItemID
									into groupedResults
									select groupedResults;

				foreach (System.Linq.IGrouping<long, RoleModule> roleModules in menuItems) {
					string itemTitle = string.Empty;
					switch (roleModules.First().ModuleName) {
						case "News":
							itemTitle = (from news in entity.News
										 where news.ID == roleModules.Key
										 select news.Title).SingleOrDefault();
							break;
						case "Events":
							itemTitle = (from events in entity.Event
										 where events.ID == roleModules.Key
										 select events.Title).SingleOrDefault();
							break;
						case "PageContent":
							break;
					}
					membersMenu.Append(
						String.Format("<li><a dataid='{0}' datatitle='{1} href=\"/{1}/{0}/{2}\">{2}</a></li>",
									  roleModules.First().ModuleItemID, roleModules.First().ModuleName,
									  itemTitle));
				}


				return "<ul class='secureMenu' style='display:none'><li><a href='/Login.aspx'>" + menuName + "</a><ul><li><a href='/MembersContent.aspx'>" + menuName + " Content</a></li><li><a href='/Member.aspx'>My Details</a></li><li><a href='/LogOut.aspx'>Log Out</a></li></ul></li></ul>";
			} else {

				menuName = memberarea.ContainsKey("Member_LogInText") && !string.IsNullOrEmpty(memberarea["Member_LogInText"])
									  ? memberarea["Member_LogInText"]
									  : "Log In";
				return "<ul class='secureMenu' style='display:none;'><li><a href='/Login.aspx'>" + menuName + "</a></li></ul>";
			}
		}

		#region membership users
		public static bool loginMemberUser(string username, string password) {
			bool isValid = false;

			using (var entity = new AuroraEntities()) {

				var encryptedPass = Aurora.Security.Encryption.Encrypt(password);
				var memberInfo = from members in entity.Member
								 where members.Email == username
									   && members.Password == encryptedPass
									   && members.isActive == true
									   && members.ClientSiteID == SessionManager.ClientSiteID
									   && members.DeletedOn == null
									   && members.DeletedBy == null
									   && members.isPending == false
								 select members;

				if (memberInfo.Count() > 0 && !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) {

					SessionManager.VistorSecureSessionID = Guid.NewGuid();
					SessionManager.MemberData = memberInfo.First();

					isValid = true;

				}
			}

			return isValid;
		}
		#endregion

		#region Currency Conversion
		public static double ConvertCurrency(double Value, int? SourceCurrencyID = null, int? DestinationCurrencyID = null) {
			using (var entity = new AuroraEntities()) {
				var currencyConversions = (from curr in entity.CurrencyConversions
										   where
											curr.DeletedOn == null &&
											curr.ClientSiteID == SessionManager.ClientSiteID
										   select curr).ToList();
				CurrencyConversions SourceCurrency = null, DestinationCurrency = null;

				// Retrieve source currency from the in memory object
				if (SourceCurrencyID != null) {
					SourceCurrency = currencyConversions.Where(w => w.CurrencyID == SourceCurrencyID).FirstOrDefault();
					if (SourceCurrency == null) {
						throw new Exception("Missing Source Currency Conversion");
					}
				}
				// Retrieve Destination currency from the in memory object
				if (DestinationCurrencyID != null) {
					DestinationCurrency = currencyConversions.Where(w => w.CurrencyID == DestinationCurrencyID).FirstOrDefault();
					if (DestinationCurrency == null) {
						throw new Exception("Missing Destination Currency Conversion");
					}
				}

				
				if (SourceCurrency == null && DestinationCurrency == null) {
					// No data to effect a change
					return Value;
				} else if (SourceCurrency != null && DestinationCurrency == null) {
					// Convert Source to Home

					if (SourceCurrency.IsDefault == true) {
						return Value;
					} else {
						return (Value / SourceCurrency.Value.Value);
					}
				} else if (SourceCurrency == null && DestinationCurrency != null) {
					// Convert Home to Destination
					return (Value * DestinationCurrency.Value.Value);
				} else {
					return ((Value / SourceCurrency.Value.Value) * DestinationCurrency.Value.Value);
				}
												
			}
		}
		#endregion


	}



	#endregion

	#region string extention class
	public static class StringExtention {
		/// <summary>
		/// Replaces all occuraences of object properties that are encased in curly braces with that object property's value. EG: {FirstName}
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="str">The string that is invoking the method</param>
		/// <param name="SourceObject">The object/entity that contains the properties you would like to populate in the string.</param>
		/// <param name="fields">A string array of property names to populate in the string.</param>
		/// <returns>The modified string</returns>
		public static string ReplaceObjectTokens<T>(this string str, T SourceObject, string[] fields = null) {
			var propertyNameList = SourceObject.GetType().GetProperties().Select(s => s.Name);

			//change the list to include only the fields that are in the provided array of strings
			if (fields != null) {
				propertyNameList = propertyNameList.Where(p => fields.Contains(p));
			}

			//use string builder for performance
			StringBuilder sb = new StringBuilder(str);
			//loop through all the properties of the object and replace the tokens with values

			foreach (var prop in propertyNameList) {

				PropertyInfo sourceProp = SourceObject.GetType().GetProperty(prop);
				//if the value is null leave it alone
				if (sourceProp.GetValue(SourceObject, null) != null && str.Contains(prop)) {
					sb.Replace("{{" + prop + "}}", sourceProp.GetValue(SourceObject, null).ToString());
				}
			}

			return sb.ToString();
		}

	}
	#endregion


}

