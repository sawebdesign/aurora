using System; 
using System.IO; 
using System.Text; 
using System.Security.Cryptography;


namespace Aurora.Security
{ 
	/// <summary> 
	/// Summary description for Encryption. 
	/// </summary> 
    public class Encryption
	{

        static byte[] Key = new byte[32] { 21, 11, 78, 213, 76, 2, 83, 121, 17, 169, 56, 89, 62, 241, 145, 122, 2, 94, 11, 214, 119, 35, 114, 188, 1, 12, 16, 87, 99, 55, 43, 76 };
        static byte[] IV = new byte[16] { 21, 121, 9, 129, 77, 81, 123, 95, 51, 217, 143, 89, 11, 100, 222, 3 };
        static byte[] inputByteArray;
        static RijndaelManaged myR = new RijndaelManaged(); 


		public static string Encrypt(string stringToEncrypt) 
		{ 
			inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt); 
			ICryptoTransform encryptor = myR.CreateEncryptor(Key, IV); 
			try 
			{ 
				MemoryStream ms = new MemoryStream(); 
				CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write); 
				cs.Write(inputByteArray, 0, inputByteArray.Length); 
				cs.FlushFinalBlock(); 

				return Convert.ToBase64String(ms.ToArray()); 
			} 
			catch(System.Exception ex) 
			{ 
				throw ex; 
			} 
		}


        public static string Decrypt(string stringToDecrypt)
        {
            stringToDecrypt.Replace(" ", "+");
			inputByteArray = new byte[stringToDecrypt.Length]; 
			ICryptoTransform decryptor = myR.CreateDecryptor(Key, IV); 
			try 
			{ 
				inputByteArray = Convert.FromBase64String(stringToDecrypt); 
				MemoryStream ms = new MemoryStream(); 
				CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write); 
				cs.Write(inputByteArray, 0, inputByteArray.Length); 
				cs.FlushFinalBlock(); 
				ASCIIEncoding encoding = new ASCIIEncoding(); 
				return encoding.GetString(ms.ToArray()); 
			} 
			catch(System.Exception ex) 
			{ 
				throw ex; 

			} 
		} 


	}

}
