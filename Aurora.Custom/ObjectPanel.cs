﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Aurora.Custom.UI {

    [DefaultProperty("Entity")]
    [ToolboxData("<{0}:ObjectPanel runat=server></{0}:ObjectPanel>")]
    
    public class ObjectPanel : Panel {

        public ObjectPanel() : base() {
        }

        #region PROPERTIES

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text {
            get {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set { ViewState["Text"] = value; }
        }

        #region Entity
        [Bindable(true)]
        [Category("Methods")]
        [Localizable(true)]
        private Data.AuroraEntities _Entity = null;
        public Data.AuroraEntities Entity {
            set { _Entity = value; }
            get { return _Entity; }
        }
        #endregion
        #endregion

        #region METHOD: WriteObjectToPanel
        public void WriteObjectToPanel(Data.AuroraEntities Entity) {
            this.Entity = Entity;
            WriteObjectToPanel(this);
        }

        public void WriteObjectToPanel() {
            if (this.Entity == null)
                throw new Exception("WriteObjectToPanel: No Entity to populate - set the Entity property");
            WriteObjectToPanel(this);
        }

        private void WriteObjectToPanel(Control control) {

            if (control.HasControls()) {
                foreach (Control ctl in control.Controls) {
                    if (ctl is WebControl || ctl is System.Web.UI.HtmlControls.HtmlForm)
                        WriteObjectToPanel(ctl);
                }
            }

            if (control is WebControl) {
                WebControl ctl = (WebControl)control;
                if (ctl.Attributes["fieldname"] != null) {

                    //Get the Value in the object
                    object oValue = GetObjectProperty(_Entity, ctl.Attributes["fieldname"]);

                    //Set Control Value
                    SetControlValue(ctl, oValue);

                }
            }
        }
        #endregion

        #region METHOD: WriteObjectToXml
        public XmlDocument WriteObjectToXml() {
            return WriteObjectToXml(this);
        }

        /// <summary>
        /// Read the values from a panel into an XML document
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        private XmlDocument WriteObjectToXml(Control control) {
            /*
             SAMPLE:
             -------
             <?xml version="1.0" encoding="utf-8" ?>
                <PanelName>
                    <Fields>
                        <field fieldname="PartnerSurname" value="" />
                        <field fieldname="PartnerBirthDay"  value="1 Jan 2006" />
                    </Fields>
                </PanelName>
             */

            XmlDocument xValues = new XmlDocument();

            //Create the Base XML
            xValues.LoadXml("<" + this.ID + ">\n<Fields>\n</Fields>\n</" + this.ID + ">");
            XmlNode xFields = xValues.SelectSingleNode("/" + this.ID + "/Fields");

            GetControls(control, ref xValues);

            return xValues;

        }

        private void GetControls(Control control, ref XmlDocument xValues) {

            if (control.HasControls()) {
                foreach (Control ctl in control.Controls) {
                    if (ctl is WebControl)
                        GetControls(ctl, ref xValues);
                }
            }

            if (control is WebControl) {
                WebControl ctl = (WebControl)control;
                if (ctl.Attributes["fieldname"] != null) {
                    //Create a 'field' node
                    XmlNode xField = xValues.CreateElement("field");

                    //Get the attribute values
                    XmlAttribute xattrFieldName = xValues.CreateAttribute("fieldname");
                    xattrFieldName.Value = ctl.Attributes["fieldname"];

                    XmlAttribute xattrValue = xValues.CreateAttribute("value");
                    xattrValue.Value = ObjectPanel.GetValue(ctl).ToString();

                    xField.Attributes.Append(xattrFieldName);
                    xField.Attributes.Append(xattrValue);

                    //add the field to the document in the Fields 
                    XmlNode xFields = xValues.SelectSingleNode("" + this.ID + "/Fields");
                    xFields.AppendChild(xField);
                }

                #region Checkbox is different...values are on InputAttribute NOT Attribute - Bruce 16 Oct 2007
                if (ctl is CheckBox && ((CheckBox)ctl).InputAttributes["fieldname"] != null) {
                    XmlNode xField = xValues.CreateElement("field");

                    //Get the attribute values
                    XmlAttribute xattrFieldName = xValues.CreateAttribute("fieldname");
                    xattrFieldName.Value = ((CheckBox)ctl).InputAttributes["fieldname"];

                    XmlAttribute xattrValue = xValues.CreateAttribute("value");
                    xattrValue.Value = ObjectPanel.GetValue(ctl).ToString();

                    XmlAttribute xattrAdditional = xValues.CreateAttribute("additionalpriceid");
                    if (((CheckBox)ctl).InputAttributes["additionalpriceid"] != null) {
                        xattrAdditional.Value = ((CheckBox)ctl).InputAttributes["additionalpriceid"];
                        xField.Attributes.Append(xattrAdditional);
                    }

                    xField.Attributes.Append(xattrFieldName);
                    xField.Attributes.Append(xattrValue);

                    //add the field to the document in the Fields 
                    XmlNode xFields = xValues.SelectSingleNode("" + this.ID + "/Fields");
                    xFields.AppendChild(xField);
                }
                #endregion
            }
        }
        #endregion

        #region METHOD: ReadObjectFromPanel
        /// <summary>
        /// Read the values from a panel into an Entity Object
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public Aurora.Custom.Data.AuroraEntities ReadObjectFromPanel(Aurora.Custom.Data.AuroraEntities Entity) {
            this.Entity = Entity;
            ReadObjectFromPanel(this);

            return this.Entity;
        }

        /// <summary>
        /// Reads the Parameters from a panel into an object
        /// </summary>
        /// <returns></returns>
        public Aurora.Custom.Data.AuroraEntities ReadObjectFromPanel() {
            if (this.Entity == null)
                throw new Exception("ReadObjectFromPanel: No Entity to populate - set the Entity property");

            ReadObjectFromPanel(this);

            return this.Entity;
        }

        private void ReadObjectFromPanel(Control control) {

            if (control.HasControls()) {
                foreach (Control ctl in control.Controls) {
                    if (ctl is WebControl || ctl is System.Web.UI.HtmlControls.HtmlForm)
                        ReadObjectFromPanel(ctl);
                }
            }

            if (control is WebControl) {
                WebControl ctl = (WebControl)control;
                if (ctl.Attributes["fieldname"] != null) {
                    //Get Control Value
                    object oValue = GetValue(ctl);

                    //Set the Value in the object
                    SetObjectProperty(_Entity, ctl.Attributes["fieldname"], oValue);
                }
            }
        }
        #endregion

        #region METHOD: SetObjectProperty
        public bool SetObjectProperty(object obj, string propertyName, object Value) {
            try {
                //get a reference to the PropertyInfo, exit if no property with that name
                System.Reflection.PropertyInfo pi = obj.GetType().GetProperty(propertyName);
                if (pi == null) return false;

                //convert the value to the expected type
                //Value = Convert.ChangeType(Value, pi.PropertyType);
                System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(pi.PropertyType);
                if (converter != null && converter.CanConvertFrom(Value.GetType())) {
                    Value = converter.ConvertFrom(Value);
                } else {
                    Value = Convert.ChangeType(Value, pi.PropertyType);
                }

                //attempt the assignment
                pi.SetValue(obj, Value, null);
                return true;
            } catch {
                return false;
            }

        }
        #endregion

        #region METHOD: GetObjectProperty
        public object GetObjectProperty(object obj, string propertyName) {
            try {
                //get a reference to the PropertyInfo, exit if no property with that name
                System.Reflection.PropertyInfo pi = obj.GetType().GetProperty(propertyName);
                if (pi == null)
                    return null;
                //convert the value to the expected type
                //Value = Convert.ChangeType(Value, pi.PropertyType);
                //attempt the assignment
                return pi.GetValue(obj, null);

            } catch {
                return false;
            }

        }
        #endregion

        #region METHOD: SetControlValue
        //Sets the value of a control based on its type
        public static void SetControlValue(WebControl control, object oValue) {

            if (oValue == null)
                return;

            if (control is CheckBox) {
                if (oValue is int)
                    ((CheckBox)control).Checked = ((int)oValue > 0);
                if (oValue is bool)
                    ((CheckBox)control).Checked = (bool)oValue;
                if (oValue.ToString().ToLower() == "true")
                    ((CheckBox)control).Checked = true;
                if (oValue.ToString().ToLower() == "false")
                    ((CheckBox)control).Checked = false;
            }

            if (control is DropDownList) {
                try {
                    ((DropDownList)control).SelectedValue = oValue.ToString();
                } catch { };
            }

            if (control is TextBox) {
                ((TextBox)control).Text = oValue.ToString();
            }

        }
        #endregion

        #region METHOD: ClearValue
        //Clears the value of a control based on its type
        public static void ClearValue(WebControl control) {

            if (control is DropDownList)
                ((DropDownList)control).SelectedIndex = -1;

        }
        #endregion

        #region METHOD: GetValue
        //Gets the value of a control based on its type
        public static object GetValue(System.Web.UI.Control control) {
            if (control is TextBox)
                return ((TextBox)control).Text;

            if (control is System.Web.UI.WebControls.CheckBox)
                return ((System.Web.UI.WebControls.CheckBox)control).Checked;

            if (control is DropDownList)
                return ((DropDownList)control).SelectedValue;

            if (control is ListBox)
                return ((ListBox)control).SelectedValue;

            return "";

        }
        #endregion

    }
}
