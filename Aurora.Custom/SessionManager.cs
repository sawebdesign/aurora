﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aurora.Custom {
    public class SessionManager : SAWD.WebManagers.SessionManagerBase {


        /// <summary>
        /// This has replaced the CacheManager.MemberData
        /// </summary>
        public static Data.Member MemberData {
            get { return (Data.Member)(GetSession("MemberData", null)); }
            set { SetSession("MemberData", value); }
        }

        /// <summary>
        /// This has replaced the CacheManager.CurrentTransactions
        /// </summary>
        public static List<Data.TransactionLog> CurrentTransactions {
            get { return (List<Data.TransactionLog>)(GetSession("CurrentTransactions", null)); }
            set { SetSession("CurrentTransactions", value); }
        }


        /// <summary>
        /// The number of failed login attempts 
        /// </summary>        
        public static int LoginAttemptCounter {
            get { return (int)(GetSession("LoginAttemptCounter", 0)); }
            set { SetSession("LoginAttemptCounter", value); }
        }

        /// <summary>
        /// Check to if the user is logged in
        /// </summary>        
        public static bool isloggedIn {
            get { return (bool)(GetSession("isLogedIn", false)); }
            set { SetSession("isLogedIn", value); }
        }

        /// <summary>
        /// The username of the current logged in user
        /// </summary>        
        public static string UserName {
            get { return (string)(GetSession("UserName")); }
            set { SetSession("UserName", value); }
        }

        /// <summary>
        /// The ID of the current logged in user
        /// </summary>        
        public static long UserID {
            get { return (long)(GetSession("UserID")); }
            set { SetSession("UserID", value); }
        }
                
        /// <summary>
        /// The FirstName of the current logged in user
        /// </summary>        
        public static string FirstName {
            get { return (string)(GetSession("FirstName")); }
            set { SetSession("FirstName", value); }
        }

        /// <summary>
        /// The LastName of the current logged in user
        /// </summary>        
        public static string LastName {
            get { return (string)(GetSession("LastName")); }
            set { SetSession("LastName", value); }
        }
                
        /// <summary>
        /// The roles of the current logged in user
        /// </summary>        
        public static List<Aurora.Custom.Data.Roles> UserRoles {
            get { return (List<Aurora.Custom.Data.Roles>)(GetSession("UserRoles")); }
            set { SetSession("UserRoles", value); }
        }

        public static List<long?> SiteExcludedRoles {
            get { return (List<long?>)(GetSession("SiteExcludedRoles", new List<long?>{0})); }
            set { SetSession("SiteExcludedRoles", value); }
        }

        /// <summary>
        /// The record count count for the current grid that the user is looking at
        /// </summary>        
        public static int iPageCount {
            get { return (int)(GetSession("iPageCount")); }
            set { SetSession("iPageCount", value); }
        }
                
                
        //Aurora.Web
        /// <summary>
        /// The client id that we can identify them by
        /// </summary>        
        public static long? ClientSiteID
        {
            get { return (long)(GetSession("ClientSiteID", long.Parse("0"))); }
            set { SetSession("ClientSiteID", value); }
        }

        public static string ClientName {
            get { return (string)(GetSession("ClientName", string.Empty)); }
            set { SetSession("ClientName", value); }
        }

        public static long? LoggedInClientSiteID {
            get { return (long)(GetSession("LoggedInClientSiteID", long.Parse("0"))); }
            set { SetSession("LoggedInClientSiteID", value); }
        }

        public static Guid? VistorSecureSessionID
        {
            get { return (Guid)(GetSession("VistorSecureSessionID", new Guid())); }
            set { SetSession("VistorSecureSessionID", value); }
        }

        public static string ClientBasePath
        {
            get { return (string) (GetSession("ClientBasePath")); }
            set { SetSession("ClientBasePath",value);}
        }

        public static int ClientSiteRedirectCount
        {
            get { return (int)(GetSession("ClientSiteRedirectCount",int.Parse("0"))); }
            set { SetSession("ClientSiteRedirectCount", value); }
        }

        public static string ClientSiteRedirectURL {
            get { return (string)(GetSession("ClientSiteRedirectURL")); }
            set { SetSession("ClientSiteRedirectURL", value); }
        }

        /// <summary>
        /// The impersonateID to persist
        /// </summary>        
        public static long? ImpersonateID {
            get { return (long)(Utils.ConvertDBNull(GetSession("ImpersonateID"), 0)); }
            set { SetSession("ImpersonateID", value); }
        }

		/// <summary>
		/// Temporary File Upload Store
		/// </summary>        
		public static string TempFileName {
			get { return (string)(Utils.ConvertDBNull(GetSession("TempFileName"), "")); }
			set { SetSession("TempFileName", value); }
		}
    }
}
