﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;

namespace Aurora.Custom.UI {

    [DefaultProperty("XmlPanelSchema")]
    [ToolboxData("<{0}:XmlPanel runat=server></{0}:XmlPanel>")]

    public class XmlPanel : Table {
        int iWidthControlDefault = 0;
        int iWidthLabelDefault = 0;

        #region PROPERTIES

        #region PROPERTY: XmlPanelSchema
        /// <summary>
        /// Schema used to Generate the layout of the form
        /// </summary>
        XmlDocument _XmlPanelSchema;

        [Bindable(true)]
        [Category("Data")]
        [Localizable(true)]
        public XmlDocument XmlPanelSchema {
            get { return _XmlPanelSchema; }
            set { _XmlPanelSchema = value; }
        }
        #endregion

        #region PROPERTY: XmlPanelData
        /// <summary>
        /// Xml Data Record
        /// </summary>
        XmlDocument _XmlPanelData;
        [Bindable(true)]
        [Category("Data")]
        [Localizable(true)]
        public XmlDocument XmlPanelData {
            get { return _XmlPanelData; }
            set { _XmlPanelData = value; }
        }
        #endregion

        #region PROPERTY: Language
        /// <summary>
        /// Xml Data Record
        /// </summary>
        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("en")]
        [Localizable(true)]
        string _Language = "en"; //default to English
        /// <summary>
        /// The Language settings to use to render the control
        /// </summary>
        public string XmlLanguage {
            get { return _Language; }
            set { _Language = value; }
        }
        #endregion

        #region PROPERTY: Event Handler for the uploader.
        public EventHandler UploaderEventHandler { get; set; }
        public string UploaderDirectory { get; set; }
        #endregion

        #endregion

        #region METHODS

        /// <summary>
        /// This method builds a set of controls in a UI table based on the xml Schema passed in
        /// </summary>
        /// <param name="TableID">The ID of the Table</param>
        public void RenderXmlControls(string TableID) {
            int iWidthControl = -1;
            int iWidthLabel = -1;

            bool bControlsExist = false;
            System.Web.UI.Control ctl;

            if (this.Controls.Count > 0)
                bControlsExist = true;

            if (this.XmlPanelSchema == null)
                throw new Exception("Cannot Generate Fields, No Schema is Bound");

            //get the fields
            XmlNodeList xFields = this.XmlPanelSchema.SelectNodes("XmlDataSchema/Fields/field");
            if (xFields.Count == 0)
                throw new Exception("Invalid Schema is Bound - must comply to XmlDataSchema/Fields/field");

            //Read the Default Properties at Schema level
            if (this.XmlPanelSchema.SelectSingleNode("XmlDataSchema").Attributes["widthcontrol"] != null)
                this.iWidthControlDefault = int.Parse(this.XmlPanelSchema.SelectSingleNode("XmlDataSchema").Attributes["widthcontrol"].InnerText);

            if (this.XmlPanelSchema.SelectSingleNode("XmlDataSchema").Attributes["widthlabel"] != null)
                this.iWidthLabelDefault = int.Parse(this.XmlPanelSchema.SelectSingleNode("XmlDataSchema").Attributes["widthlabel"].InnerText);

            Table table = this; // new System.Web.UI.WebControls.Table();

            int RowCount = 0, ColCount = 0; //Table dimensions

            //table.BorderWidth = 1;
            //table.Width = Unit.Percentage(100);
            //table.BorderColor = System.Drawing.Color.Black;
            table.ID = TableID;

            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            TableCell tc2 = new TableCell();
            if (!bControlsExist) {
                #region Build an empty table to the right size based on all X and Y values
                foreach (XmlNode xField in xFields) {

                    if (xField.Attributes["x"] == null || xField.Attributes["y"] == null)
                        continue;
                    if (int.Parse(xField.Attributes["y"].InnerText) > RowCount) {
                        RowCount = int.Parse(xField.Attributes["y"].InnerText);
                    }
                    if (int.Parse(xField.Attributes["x"].InnerText) > ColCount) {
                        ColCount = int.Parse(xField.Attributes["x"].InnerText);
                    }
                }
                #region Build the table
                //Loop through the rows
                for (int y = 1; y <= RowCount + 1; y++) {
                    //add a new row to the table
                    tr = new TableRow();

                    //loop through the columns
                    for (int x = 1; x <= ColCount + 2; x++) {   //Allow a column for labels
                        tc = new TableCell();
                        tc2 = new TableCell();
                        tr.Cells.Add(tc);
                        tr.Cells.Add(tc2);
                    }
                    //Add the empty row
                    table.Rows.Add(tr);
                }
                #endregion
                #endregion
            }
            #region Populate the controls
            //Loop through the rows
            foreach (XmlNode xField in xFields) {
                //Check the Control width - or default vaue
                if (xField.Attributes["widthcontrol"] != null)
                    iWidthControl = int.Parse(xField.Attributes["widthcontrol"].InnerText);
                else
                    iWidthControl = iWidthControlDefault;

                if (xField.Attributes["widthlabel"] != null)
                    iWidthLabel = int.Parse(xField.Attributes["widthlabel"].InnerText);
                else
                    iWidthLabel = iWidthLabelDefault;

                //Instantiate the control and return an array - this may be the control + a label
                XmlControlBuilder cb = new XmlControlBuilder(xField, XmlLanguage, UploaderEventHandler, UploaderDirectory);
                WebControl[] controls = new WebControl[4];// { new Label() };//Create a dummy control set

                //see if we have any values to set on the created control
                if (this.XmlPanelData == null) {
                    if (!bControlsExist)
                        controls = cb.CreateControl(true);
                }
                else {
                    //try find the value
                    XmlNode xValue = this.XmlPanelData.SelectSingleNode("XmlData/Fields/field[@fieldname='" + cb.fieldname + "']");
                    if (xValue != null && xValue.Attributes["value"] != null) {
                        if (bControlsExist) {
                            ctl = this.FindControl("fld" + cb.fieldname);
                            //Try and match the selected value if we have one;
                            if (xValue.Attributes["value"].Value.Contains(",")) {

                                //lkpx.Attributes.Add("multipleselectvalues", lookupxmlselecteditems);
                                //lkpx.Attributes.Add("multiple", "multiple");

                            }
                            ObjectPanel.SetControlValue((WebControl)ctl, xValue.Attributes["value"].Value);
                        }
                        else
                            //Create the control with the value passed in
                            controls = cb.CreateControl(xValue.Attributes["value"].Value, true);

                    }
                    else //No Value
                    {
                        if (!bControlsExist)
                            controls = cb.CreateControl(true);
                    }

                }
                if (!bControlsExist) {

                    #region Label /HTML
                    if (controls[0] != null) //label
                    {

                        tc = table.Rows[cb.y].Cells[cb.x * 2];
                        table.Rows[cb.y].VerticalAlign = VerticalAlign.Top;

                        if (iWidthLabel > 0)
                            tc.Width = iWidthLabel;


                        tc.Controls.Add(controls[0]);

                        if (controls[1] == null) //html control was rendered
                        {
                            #region HTML

                            tc.ColumnSpan = xField.Attributes["htmlcolspan"] != null ? int.Parse(xField.Attributes["htmlcolspan"].InnerText) : 0;

                            tc.Attributes.Add("align", "left");

                            //Remove the rest of the columns - assume not other data on this row
                            //for (int i = 1; i < table.Rows[cb.y].Cells.Count - cb.x * 2 + 1; i++) {
                            //    table.Rows[cb.y].Cells.RemoveAt(cb.x * 2 + 1);
                            //}
                            #endregion

                        }
                        else {
                            tc.Attributes.Add("align", "left");

                        }

                    }
                    else {
                        tc = table.Rows[cb.y].Cells[cb.x * 2];
                        tc.Style.Value = xField.Attributes["spanstyle"] != null ? xField.Attributes["spanstyle"].InnerText : string.Empty;
                    }

                    #endregion

                    #region Control
                    //Control
                    if (controls[1] != null) {
                        tc = table.Rows[cb.y].Cells[cb.x * 2 + 1];

                        if (iWidthLabel > 0)
                            tc.Width = iWidthLabel;

                        tc.Attributes.Add("align", "left");
                        tc.Controls.Add(controls[1]);

                        //Mandatory
                        if (controls[2] != null) {
                            tc.Controls.Add(controls[2]);
                        }

                        //Regex
                        if (controls[3] != null) {
                            tc.Controls.Add(controls[3]);
                        }



                    }

                    #endregion

                }

            }

            #endregion

        }

        /// <summary>
        /// Read the values from a panel into an XML document
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public XmlDocument ReadXmlControls() {
            /*
             SAMPLE:
             -------
             <?xml version="1.0" encoding="utf-8" ?>
                <XmlData>
                    <Fields>
                        <field fieldname="PartnerSurname" value="" />
                        <field fieldname="PartnerBirthDay"  value="1 Jan 2006" />
                    </Fields>
                </XmlData>
             */

            XmlDocument xValues = new XmlDocument();

            //Create the Base XML
            xValues.LoadXml("<XmlData>\n<Fields>\n</Fields>\n</XmlData>");
            XmlNode xFields = xValues.SelectSingleNode("/XmlData/Fields");

            Control control = (Control)this;
            GetControls(control, ref xValues);

            return xValues;
        }

        private void GetControls(Control control, ref XmlDocument xValues) {
            if (control.HasControls()) {
                foreach (Control ctl in control.Controls) {
                    if (ctl is WebControl)
                        GetControls(ctl, ref xValues);
                }
            }

            if (control is WebControl) {
                WebControl ctl = (WebControl)control;
                if (ctl.Attributes["fieldname"] != null) {
                    //Create a 'field' node
                    XmlNode xField = xValues.CreateElement("field");

                    //Get the attribute values
                    XmlAttribute xattrFieldName = xValues.CreateAttribute("fieldname");
                    xattrFieldName.Value = ctl.Attributes["fieldname"];

                    XmlAttribute xattrValue = xValues.CreateAttribute("value");
                    xattrValue.Value = ObjectPanel.GetValue(ctl).ToString();

                    XmlAttribute xattrTitle = xValues.CreateAttribute("title");
                    xattrTitle.Value = ObjectPanel.GetValue(ctl).ToString();

                    xField.Attributes.Append(xattrFieldName);
                    xField.Attributes.Append(xattrValue);
                    xField.Attributes.Append(xattrTitle);

                    //add the field to the document in the Fields 
                    XmlNode xFields = xValues.SelectSingleNode("XmlData/Fields");
                    xFields.AppendChild(xField);
                }

                #region Checkbox is different...values are on InputAttribute NOT Attribute - Bruce 16 Oct 2007
                if (ctl is CheckBox && ((CheckBox)ctl).InputAttributes["fieldname"] != null) {
                    XmlNode xField = xValues.CreateElement("field");

                    //Get the attribute values
                    XmlAttribute xattrFieldName = xValues.CreateAttribute("fieldname");
                    xattrFieldName.Value = ((CheckBox)ctl).InputAttributes["fieldname"];

                    XmlAttribute xattrValue = xValues.CreateAttribute("value");
                    xattrValue.Value = ObjectPanel.GetValue(ctl).ToString();

                    xField.Attributes.Append(xattrFieldName);
                    xField.Attributes.Append(xattrValue);

                    //add the field to the document in the Fields 
                    XmlNode xFields = xValues.SelectSingleNode("XmlData/Fields");
                    xFields.AppendChild(xField);
                }
                #endregion
            }
        }
        #endregion

    }

    #region CLASS: XmlControlBuilder
    public class XmlControlBuilder {
        public string fieldname, datatype, inputmask;
        public string stylekey, styleval;
        public string regex, regexerrormessage, regexerrortext; //Regex Validation
        public string datavaluefield, datatextfield, datasrc, lookuptype;
        public string defaultvalue;
        public string html, htmlcolspan;
        public string jscriptmethodname, jscriptmethod; //Allows us to add scripts to the control
        public int x, y, width, rows, maxlength;
        public string label, tooltip;
        public short taborder;
        public bool visible, enabled, mandatory;
        public XmlNodeList xDropDownList; //An xml dropdown contained within the control schema
        public string validationgroup;
        public string controlTitle; //title from XML
        //textarea only
        public int multilinerows;
        public int multilinecolumns;
        public EventHandler uploaderEvent;
        public string uploadDir;
        //Front End Functions+
        public bool hideOnFrontEnd;
        public string validation;
        //used to enable multiple select for dropdowns 
        public bool allowmultipleselection;
        public string lookupxmlselecteditems;
        public XmlControlBuilder(XmlNode xField, string Language, EventHandler myEvent = null, string uploadDirectory = null) {

            #region Read Control Settings
            this.fieldname = xField.Attributes["fieldname"] != null ? xField.Attributes["fieldname"].InnerText : "";
            this.datatype = xField.Attributes["datatype"] != null ? xField.Attributes["datatype"].InnerText : "string";
            this.validationgroup = xField.Attributes["validationgroup"] != null ? xField.Attributes["validationgroup"].InnerText : "string";
            if (xField.Attributes["style"] != null) {
                string[] tmpStyle = xField.Attributes["style"].Value.Split(':');
                this.stylekey = tmpStyle[0];
                this.styleval = tmpStyle[1];
            }

            //Text Area
            if (xField.Attributes["multilinerows"] != null && xField.Attributes["multilinerows"].InnerText == "undefined") {
                multilinecolumns = 0;
            }
            else {
                this.multilinecolumns = xField.Attributes["multilinerows"] != null
                                            ? int.Parse(xField.Attributes["multilinerows"].InnerText)
                                            : 0;
            }

            if (xField.Attributes["multilinerows"] != null && xField.Attributes["multilinerows"].InnerText == "undefined") {
                multilinecolumns = 0;
            }
            else {
                this.multilinecolumns = xField.Attributes["multilinerows"] != null
                                            ? int.Parse(xField.Attributes["multilinerows"].InnerText)
                                            : 0;
            }
            //Add title to control
            this.controlTitle = xField.Attributes["title"] != null ? xField.Attributes["title"].InnerText : string.Empty;
            //JavaScript Support
            this.jscriptmethodname = xField.Attributes["jscriptmethodname"] != null ? xField.Attributes["jscriptmethodname"].InnerText : "";
            this.jscriptmethod = xField.Attributes["jscriptmethod"] != null ? xField.Attributes["jscriptmethod"].InnerText : "";

            this.x = xField.Attributes["x"] != null ? int.Parse(xField.Attributes["x"].InnerText) : 0;
            this.y = xField.Attributes["y"] != null ? int.Parse(xField.Attributes["y"].InnerText) : 0;
            this.width = xField.Attributes["width"] != null ? int.Parse(xField.Attributes["width"].InnerText != "" ? xField.Attributes["width"].InnerText : "-1") : -1;
            this.rows = xField.Attributes["rows"] != null ? int.Parse(xField.Attributes["rows"].InnerText != "" ? xField.Attributes["rows"].InnerText : "1") : 1;
            this.taborder = xField.Attributes["taborder"] != null ? short.Parse(xField.Attributes["taborder"].InnerText != "" ? xField.Attributes["taborder"].InnerText : "0") : (short)0;
            this.maxlength = xField.Attributes["maxlength"] != null ? int.Parse(xField.Attributes["maxlength"].InnerText != "" ? xField.Attributes["maxlength"].InnerText : "-1") : -1;

            this.visible = xField.Attributes["visible"] != null ? bool.Parse(xField.Attributes["visible"].InnerText != "" ? xField.Attributes["visible"].InnerText : "true") : true;
            this.enabled = xField.Attributes["enabled"] != null ? bool.Parse(xField.Attributes["enabled"].InnerText != "" ? xField.Attributes["enabled"].InnerText : "true") : true;
            this.mandatory = xField.Attributes["mandatory"] != null ? bool.Parse(xField.Attributes["mandatory"].InnerText != "" ? xField.Attributes["mandatory"].InnerText : "true") : false;

            //Default Value
            this.defaultvalue = xField.Attributes["defaultvalue"] != null ? xField.Attributes["defaultvalue"].InnerText : "";
            //hideable attr
            this.hideOnFrontEnd = xField.Attributes["hide"] != null
                                      ? bool.Parse(xField.Attributes["hide"].InnerText)
                                      : false;

            this.validation = xField.Attributes["validation"] != null
                                      ? xField.Attributes["validation"].InnerText
                                      : string.Empty;

            //Handle special reuirements of a drop down list
            if (this.datatype == "dropdownlist") {
                this.datasrc = xField.Attributes["datasrc"] != null ? xField.Attributes["datasrc"].InnerText : "";
                this.datavaluefield = xField.Attributes["datavaluefield"] != null ? xField.Attributes["datavaluefield"].InnerText : "";
                this.datatextfield = xField.Attributes["datatextfield"] != null ? xField.Attributes["datatextfield"].InnerText : "";
            }

            //Handle a Lookup Type
            if (this.datatype == "lookup") {
                this.lookuptype = xField.Attributes["lookuptype"] != null ? xField.Attributes["lookuptype"].InnerText : "";
            }

            //Handle a Lookup Type
            if (this.datatype == "lookupxml") {
                //get the fields
                xDropDownList = xField.SelectNodes("valuelist/listitem");
            }

            //Handle lookupxml multiple select
            if (this.datatype == "lookupxml" && xField.Attributes["multipleselection"] != null)
            {
                this.allowmultipleselection = bool.Parse(xField.Attributes["multipleselection"].Value);
            }
            //Add custom eventhander for uploader
            this.uploaderEvent += myEvent;
            this.uploadDir = uploadDirectory;

            //colspan
            this.htmlcolspan = xField.Attributes["htmlcolspan"] != null ? xField.Attributes["htmlcolspan"].InnerText : "0";
            #endregion //Read Control Settings

            #region Language settings
            //Handle Language Settings
            //this.label = xField.Attributes["fieldname"] != null ? xField.Attributes["fieldname"].InnerText : "";
            this.label = xField.SelectSingleNode("labels/label").InnerText;
            this.tooltip = "";

            //read the tooltips and labels
            XmlNode xLabel = xField.SelectSingleNode("labels/label[@lang = '" + Language + "']");
            if (xLabel != null)
                this.label = xLabel.InnerText;

            XmlNode xTooltip = xField.SelectSingleNode("tooltips/tooltip[@lang = '" + Language + "']");
            if (xTooltip != null)
                this.tooltip = xTooltip.InnerText;

            #endregion // Language settings

            //Regex Validation
            this.regex = xField.Attributes["regex"] != null ? xField.Attributes["regex"].InnerText : "";
            this.regexerrormessage = xField.Attributes["regexerrormessage"] != null ? xField.Attributes["regexerrormessage"].InnerText : "Error on " + this.label;
            this.regexerrortext = xField.Attributes["regexerrortext"] != null ? xField.Attributes["regexerrortext"].InnerText : "*";

        }

        /// <summary>
        /// Thiis method creates an instance of the control based on the values set
        /// and will set the value if it is passed in
        /// </summary>
        /// <returns></returns>
        public System.Web.UI.WebControls.WebControl[] CreateControl(bool SetAnchors) {
            return CreateControl(null, SetAnchors);
        }
        public System.Web.UI.WebControls.WebControl[] CreateControl(object oValue, bool SetAnchors) {
            //Check for Defaults
            if (oValue == null)
                oValue = this.defaultvalue;

            //Allow us to store preset values
            string MandatoryColour = ConfigurationManager.AppSettings["MandatoryColour"] == null ? "LightYellow" : ConfigurationManager.AppSettings["MandatoryColour"];
            string ValidatedColour = ConfigurationManager.AppSettings["ValidatedColour"] == null ? "LightYellow" : ConfigurationManager.AppSettings["ValidatedColour"];

            //Validation controls
            System.Web.UI.WebControls.RegularExpressionValidator ctlRegex = new RegularExpressionValidator();
            System.Web.UI.WebControls.RequiredFieldValidator ctlMandatory = new RequiredFieldValidator();
            System.Web.UI.WebControls.WebControl ctl = new Label();

            //Allow 4 controls [0] = label [1] = control [2] = Mandatory [3] = Regex
            WebControl[] Result = new WebControl[4];

            //Allow for the label and HTML control
            if (this.label.Length > 0) {
                System.Web.UI.WebControls.Label lbl = new Label();
                lbl.ID = "lbl" + this.fieldname;
                lbl.Visible = this.visible;

                if (this.datatype == "html") {
                    //HTML Text
                    lbl.CssClass = "fldhtmlContent";
                    lbl.Text = this.label.Replace("&gt;", ">").Replace("&lt;", "<");
                }
                else {
                    //Normal Label
                    lbl.Text = this.label;
                }

                Result[0] = lbl;
            }

            //Create the control
            switch (this.datatype) {

                #region Normal Masked Control
                case "string":
                case "decimal":
                case "int":
                case "double": {
                        TextBox txt = new TextBox();
                        //txt.ID = "fld";
                        txt.MaxLength = this.maxlength == -1 ? 150 : this.maxlength;
                        ObjectPanel.SetControlValue(txt, oValue);
                        ctl = txt;
                        break;
                    }
                #endregion //TextBox

                #region TextArea
                case "textarea":
                    TextBox txtArea = new TextBox();
                    txtArea.Rows = multilinerows;
                    txtArea.Columns = multilinecolumns;
                    txtArea.TextMode = TextBoxMode.MultiLine;
                    oValue = ((string)oValue).Replace("<br/>", Environment.NewLine);
                    ObjectPanel.SetControlValue(txtArea, oValue);
                    ctl = txtArea;
                    break;
                #endregion

                #region File Upload
                case "fileupload":
                    FileUpload uploader = new FileUpload();
                    uploader.ID = this.fieldname;
                    uploader.Unload += uploaderEvent;
                    ObjectPanel.SetControlValue(uploader, oValue);
                    ctl = uploader;
                    break;
                #endregion

                #region DropDownList - Normal DataBound
                //case "dropdownlist":
                //    DropDownList cbo = new DropDownList();
                //    cbo.ID = "cbo";
                //    //see if we can bind this control
                //    if (this.datasrc != "")
                //    {
                //        SqlCommand cmd = new SqlCommand(this.datasrc);
                //        Premier.Data.FormBinding.SetDropDownList(ref cbo, cmd, this.datavaluefield, this.datatextfield);
                //        cbo.DataBind();

                //        //Try and match the selected value if we have one;
                //        SetControlValue(cbo, oValue);
                //    }

                //ctl = cbo;
                //break;
                #endregion //DropDownList - Normal DataBound

                #region DropDownList - Lookup DataBound
                case "lookup":
                    DropDownList lkp = new DropDownList();
                    if (this.lookuptype != "") {
                        //TODO : Add SQL Command Interface
                        //Services.LookUpCodesService lus = new Services.LookUpCodesService();
                        //int iTotalCount = 0;
                        //List<Entities.LookUpCodes> lul = lus.GetPaged("LUCLookupType='" + this.lookuptype + "'", "SequenceNo", 0, 1000, out iTotalCount);
                        //lkp.DataSource = lul;
                        //lkp.DataTextField = "Description";
                        //lkp.DataValueField = "LookupCode";
                        //lkp.DataBind();

                        ////Try and match the selected value if we have one;
                        //ObjectPanel.SetControlValue(lkp, oValue);

                    }

                    ctl = lkp;
                    break;
                #endregion //DropDownList - Lookup DataBound

                #region DropDownList - Xml Bound
                case "lookupxml":
                    DropDownList lkpx = new DropDownList();
                    foreach (XmlNode xValue in this.xDropDownList) {
                        lkpx.Items.Add(new ListItem(
                            xValue.Attributes["description"] != null ? xValue.Attributes["description"].InnerText : "",
                            xValue.Attributes["value"] != null ? xValue.Attributes["value"].InnerText : ""));

                       
                    }


                    //Try and match the selected value if we have one;
                    if (this.xDropDownList.Count > 0)
                        ObjectPanel.SetControlValue(lkpx, oValue);

                    ctl = lkpx;
                    break;
                #endregion //DropDownList - Xml Bound

                #region RadioButtonList - Xml Bound
                case "lookupxmlradio":
                    //RadioButtonList lkpxradio = new RadioButtonList();
                    //foreach (XmlNode xValue in this.xDropDownList) {
                    //    lkpxradio.Items.Add(new ListItem(
                    //        xValue.Attributes["description"] != null ? xValue.Attributes["description"].InnerText : "",
                    //        xValue.Attributes["value"] != null ? xValue.Attributes["value"].InnerText : ""));

                    //}

                    ////Try and match the selected value if we have one;
                    //if (this.xDropDownList.Count > 0)
                    //    ObjectPanel.SetControlValue(lkpxradio, oValue);

                    //ctl = lkpxradio;
                    break;
                #endregion //RadioButtonList - Xml Bound

                #region DateTime
                case "datetime": {
                        //TODO: Add Ajax DateTime picker
                        break;
                    }
                #endregion //DateTime

                #region Boolean
                case "bool": {
                        CheckBox chk = new CheckBox();
                        //chk.ID = "chk";
                        ObjectPanel.SetControlValue(chk, oValue);
                        ctl = chk; //Setting the ctl(typeof Label) to chk(typeof CheckBox) results in a CheckBox with Text="" and Checked=false
                        break;
                    }
                #endregion //Boolean

                #region Html
                case "html":
                    Label ltl = new Label();
                    ltl.Text = this.label.Replace("&gt;", ">").Replace("&lt;", "<");
                    ObjectPanel.SetControlValue(ltl, oValue);
                    ctl = ltl;
                    break;

                #endregion //Html
            }

            #region General properties
            ctl.ID = "fld" + this.fieldname;
            ctl.Visible = this.visible;
            ctl.Enabled = this.enabled;
            ctl.ToolTip = this.tooltip;
            ctl.TabIndex = this.taborder;

            if (!string.IsNullOrEmpty(this.stylekey)) {
                ctl.Style.Add(Utils.ConvertDBNull(this.stylekey, "display"), Utils.ConvertDBNull(this.styleval, "block"));
            }
            //adds an attribute that can be used to identify which controls to hide
            if (hideOnFrontEnd) {
                ctl.Attributes.Add("hideme", "true");
            }

            if (oValue.ToString().Contains(",") && ctl is DropDownList) {
                ctl.Attributes.Add("multipleselectvalues", oValue.ToString());
                ctl.Attributes.Add("multiple", "multiple");
            }

            if (ctl is DropDownList && allowmultipleselection)
            {
                ctl.Attributes.Add("multiple", "multiple");
            }
            //Adds an attribute to allow global validation to be performed on this control
            if (!string.IsNullOrEmpty(validation) && ctl.GetType() != typeof(CheckBox)) {
                ctl.Attributes.Add("valtype", validation);
            }
            if (this.width > -1)
                ctl.Width = this.width;

            if (ctl.GetType() == typeof(CheckBox)) {
                ((CheckBox)ctl).InputAttributes.Add("fieldname", this.fieldname);
                ((CheckBox)ctl).InputAttributes.Add("valtype", validation);
            }
            else {
                ctl.Attributes.Add("fieldname", this.fieldname);
            }

            ctl.EnableViewState = true;

            #region Mandatory Field
            if (this.mandatory) {
                //Set mandatory field's colour
                ctl.BackColor = System.Drawing.Color.FromName(MandatoryColour);

                //set the required field control
                ctlMandatory.ControlToValidate = "fld" + this.fieldname;
                ctlMandatory.ID = "vld" + this.fieldname;
                ctlMandatory.ErrorMessage = this.fieldname + " required";
                ctlMandatory.Text = "*";
                ctlMandatory.Display = ValidatorDisplay.Dynamic;

            }
            #endregion

            #region Regex Validation
            if (this.regex != "") {
                ctl.BackColor = System.Drawing.Color.FromName(ValidatedColour);

                //set the required field control
                ctlRegex.ControlToValidate = "fld" + this.fieldname;
                ctlRegex.ID = "regx" + this.fieldname;
                ctlRegex.Text = this.regexerrortext;
                ctlRegex.ErrorMessage = this.regexerrormessage;
                ctlRegex.Display = ValidatorDisplay.Dynamic;
                ctlRegex.ValidationExpression = this.regex;
                ctlRegex.ValidationGroup = this.validationgroup;
            }
            #endregion

            #region Javascript/Custom Attributes
            if (this.jscriptmethodname != "") {
                if (ctl.GetType() == typeof(CheckBox))
                    ((CheckBox)ctl).InputAttributes.Add(this.jscriptmethodname, this.jscriptmethod);
                else
                    ctl.Attributes.Add(this.jscriptmethodname, this.jscriptmethod);
            }
            #endregion

            //remove the control for Html - the label is set
            if (this.datatype == "html") {
                //ctl.ID = "html" + this.fieldname;
                ctl = null;
            }

            //Add Title Attr
            if (ctl != null) {
                ctl.Attributes.Add("title", controlTitle);
            }

            //Add the controls to the collection we are returning
            Result[1] = ctl;

            if (ctlMandatory.ControlToValidate != "")
                Result[2] = ctlMandatory;

            if (ctlRegex.ControlToValidate != "")
                Result[3] = ctlRegex;

            #endregion

            return Result;

        }

        //void uploader_Unload(object sender, EventArgs e)
        //{


        //}


    }
    #endregion
}
