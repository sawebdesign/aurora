﻿//using System.Configuration;
//using Microsoft.VisualBasic;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Diagnostics;
//using System.Data.SqlClient;
//using System.Web.SessionState;
//using System.Web.Mail;


//public class functions : System.ComponentModel.Component
//{

//    #region " Component Designer generated code "

//    public functions(System.ComponentModel.IContainer Container) : this()
//    {

//        //Required for Windows.Forms Class Composition Designer support
//        Container.Add(this);
//    }

//    public functions() : base()
//    {

//        //This call is required by the Component Designer.
//        InitializeComponent();

//        //Add any initialization after the InitializeComponent() call

//    }

//    //Component overrides dispose to clean up the component list.
//    protected override void Dispose(bool disposing)
//    {
//        if (disposing) {
//            if ((components != null)) {
//                components.Dispose();
//            }
//        }
//        base.Dispose(disposing);
//    }

//    //Required by the Component Designer

//    private System.ComponentModel.IContainer components;
//    //NOTE: The following procedure is required by the Component Designer
//    //It can be modified using the Component Designer.
//    //Do not modify it using the code editor.
//    [System.Diagnostics.DebuggerStepThrough()]
//    private void InitializeComponent()
//    {
//        components = new System.ComponentModel.Container();
//    }

//    #endregion

//    public SqlConnection dbConn;
//    public string dbConnect()
//    {
//        try {
//            dbConn = new SqlConnection(ConfigurationManager.AppSettings("DBConnectionString"));
//            dbConn.Open();
//            return "True";
//        } catch (Exception ex) {
//            return "False: " + ex.Message;
//        }
//    }

//    public bool dbDisConnect()
//    {
//        try {
//            dbConn.Close();
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }

//    public DataSet getData(string mStr)
//    {
//        dbConnect();
//        dynamic dbCon = new System.Data.SqlClient.SqlDataAdapter(mStr, dbConn);
//        dynamic ds = new DataSet();
//        try {
//            dbCon.Fill(ds, "myData");
//            return ds;
//        } catch (Exception ex) {
//            dynamic erro = "Error: Updating Data, " + ex.Message;
//        } finally {
//            dbDisConnect();
//            dbConn.Close();
//            dbCon.Dispose();
//            ds.Dispose();
//        }
//    }

//    public string upDateData(string mStr)
//    {
//        try {
//            dbConnect();
//            SqlCommand objCommand = new SqlCommand(mStr, dbConn);
//            objCommand.ExecuteNonQuery();
//            return "True";
//        } catch (Exception ex) {
//            return ("Error: Updating Data, " + ex.Message);
//        } finally {
//            dbDisConnect();
//            dbConn.Close();
//        }
//    }

//    //--Name: lDate
//    //--Description: convert date to South African format
//    public object sDate(DateTime tmpDate)
//    {
//        return PadZero(DateAndTime.Day(Convert.ToDateTime(tmpDate))) + "/" + PadZero(DateAndTime.Month(Convert.ToDateTime(tmpDate))) + "/" + DateAndTime.Year(Convert.ToDateTime(tmpDate));
//    }

//    //--Name: lDate
//    //--Description: convert "27/06/2004" to long date "27 June 2004" for displaying perposes only
//    public System.DateTime lDate(tmpDate)
//    {
//        return DateAndTime.Day(tmpDate) + " " + DateAndTime.MonthName(DateAndTime.Month(tmpDate)) + " " + DateAndTime.Year(tmpDate);
//    }

//    //--Name: GetMySQLDate
//    //--Description: convert "27/06/2004" to long date "yyyy-mm-dd" for input purposes
//    public object GetMySQLDate(sDate)
//    {
//        dynamic strDate = null;
//        strDate = DateAndTime.Year(Convert.ToDateTime(sDate)) + "-" + PadZero(DateAndTime.Month(Convert.ToDateTime(sDate))) + "-" + PadZero(DateAndTime.Day(Convert.ToDateTime(sDate)));
//        return strDate;
//    }

//    //--Name: PadZero
//    //--Description: used with GetMySQLDate to make 1/1/2004 - 2004-01-01
//    public string PadZero(val)
//    {
//        string functionReturnValue = null;
//        if (Strings.Len(Convert.ToString(val)) == 1) {
//            functionReturnValue = "0" + Convert.ToString(val);
//        } else {
//            functionReturnValue = val;
//        }
//        return functionReturnValue;
//    }


//    public string makeCode(maxLen)
//    {
//        dynamic strNewCode = null;
//        dynamic whatsNext = null;
//        dynamic upper = null;
//        dynamic lower = null;
//        dynamic intCounter = null;
//        VBMath.Randomize();
//        for (intCounter = 1; intCounter <= maxLen; intCounter++) {
//            whatsNext = Conversion.Int((1 - 0 + 1) * VBMath.Rnd() + 0);
//            if (whatsNext == 0) {
//                //--character
//                upper = 90;
//                lower = 65;
//            } else {
//                upper = 57;
//                lower = 48;
//            }
//            strNewCode = strNewCode + Strings.Chr(Conversion.Int((upper - lower + 1) * VBMath.Rnd() + lower));
//        }
//        return strNewCode;
//    }

//    public string MySQLStringFormat(strIn)
//    {
//        dynamic tmpString = null;
//        tmpString = Strings.Replace(strIn, "'", "`");
//        tmpString = Strings.Replace(tmpString, "\\", "\\\\");
//        tmpString = Strings.Replace(tmpString, "§", "&amp;Sect");
//        return tmpString;
//    }

//    //Public Function sendMail(ByVal fMail As String, ByVal tMail As String, ByVal mMessage As String, ByVal mSubject As String, ByVal smtp As String) As String

//    //    Try
//    //        '--Creating ActiveX object for SMTP (Jmail) 
//    //        Dim JMail
//    //        JMail=CreateObject("JMail.SMTPMail")

//    //        '--Setting mail parameters 
//    //        JMail.ServerAddress="" & smtp & ""
//    //        JMail.Subject="" & mSubject & ""
//    //        JMail.Sender="" & fMail & ""
//    //        JMail.AddRecipient("" & tMail & ")

//    //        JMail.HTMLBody=mMessage
//    //        'JMail.appendText(mMessage)
//    //        JMail.Execute()
//    //        JMail.Close()

//    //        Return "sent"
//    //    Catch ex As Exception
//    //        Return "Error: Sending email -" & smtp & "- " & ex.Message
//    //    End Try

//    //End Function

//    public string sendMail(string fMail, string tMail, string mMessage, string mSubject, string smtp)
//    {
//        try {
//            System.Web.Mail.MailMessage m = new System.Web.Mail.MailMessage();
//            string strEmail = null;
//            strEmail = mMessage;

//            var _with1 = m;

//            _with1.From = fMail;
//            _with1.To = tMail;
//            _with1.Subject = mSubject;
//            _with1.BodyFormat = MailFormat.Html;
//            _with1.Body = strEmail;


//            SmtpMail.SmtpServer = smtp;
//            SmtpMail.Send(m);

//            return "sent";
//        } catch (Exception ex) {
//            return "Error: Sending email, '" + smtp + "' " + ex.Message;
//        }
//    }

//    //--Function written by mk_bt
//    //--Name: getPage
//    //--Description: Get the page details for specified Id
//    //--IdNum is the Id of the page in the database
//    public object getPage(IdNum, linkId)
//    {
//        object functionReturnValue = null;

//        int theId = 0;
//        if (!string.IsNullOrEmpty(linkId)) {
//            theId = linkId;
//        } else {
//            theId = IdNum;
//        }

//        bool isErr = false;
//        System.Data.SqlClient.SqlDataReader dr = null;
//        try {
//            string gSQl = null;
//            gSQl = "SELECT Title_S,Title_L,Content,ReDir,title,description,keywords,pCount FROM TB_Content WHERE ConId=" + theId + " ";

//            dynamic dsCon = new DataSet();
//            dsCon = getData(gSQl);
//            if (dsCon.Tables(0).Rows.Count == 0) {
//                functionReturnValue = false;
//            } else {
//                System.Web.HttpContext.Current.Session["Title_S"] = dsCon.Tables(0).Rows(0)("Title_S");
//                System.Web.HttpContext.Current.Session["Title_L"] = dsCon.Tables(0).Rows(0)("Title_L");
//                System.Web.HttpContext.Current.Session["Content"] = Strings.Replace(dsCon.Tables(0).Rows(0)("Content"), "../", "");
//                System.Web.HttpContext.Current.Session["ReDir"] = dsCon.Tables(0).Rows(0)("ReDir");
//                if (string.IsNullOrEmpty(Strings.Trim(dsCon.Tables(0).Rows(0)("title")))) {
//                    System.Web.HttpContext.Current.Session["title"] = dsCon.Tables(0).Rows(0)("Title_S");
//                } else {
//                    System.Web.HttpContext.Current.Session["title"] = dsCon.Tables(0).Rows(0)("title");
//                }
//                System.Web.HttpContext.Current.Session["description"] = dsCon.Tables(0).Rows(0)("description");
//                System.Web.HttpContext.Current.Session["keywords"] = dsCon.Tables(0).Rows(0)("keywords");

//                pageCount(IdNum, "TB_Content", "pCount", "ConId", dsCon.Tables(0).Rows(0)("pCount"));
//                functionReturnValue = true;
//            }
//            dsCon.Dispose();
//        } catch (Exception ex) {
//            isErr = true;
//            System.Web.HttpContext.Current.Session["eSource"] = ex.Source;
//            System.Web.HttpContext.Current.Session["eMessage"] = ex.Message;
//            System.Web.HttpContext.Current.Session["eStackTrace"] = ex.StackTrace;
//        }
//        if (isErr == true) {
//            System.Web.HttpContext.Current.Response.Redirect("error.aspx");
//        }
//        return functionReturnValue;
//    }

//    //--Function written by mk_bt
//    //--Name: pageCount
//    //--Description: count the number of page views
//    //--theId is the page id
//    public void pageCount(theId, table, fld, fldId, theCount)
//    {
//        bool isErr = false;
//        try {
//            dynamic dbUpdate = upDateData("Update " + table + " SET " + fld + "=" + theCount + 1 + " where " + fldId + "=" + theId);
//        } catch (Exception ex) {
//            isErr = true;
//            System.Web.HttpContext.Current.Session["eSource"] = ex.Source;
//            System.Web.HttpContext.Current.Session["eMessage"] = ex.Message;
//            System.Web.HttpContext.Current.Session["eStackTrace"] = ex.StackTrace;
//        }
//        if (isErr == true) {
//            System.Web.HttpContext.Current.Response.Redirect("error.aspx");
//        }
//    }

//    public int getrCount(string msql)
//    {
//        functions x = new functions();
//        x.dbConnect();
//        DataSet ds = x.getData(msql);
//        if (ds.Tables[0].Rows.Count >= 1) {
//            return ds.Tables[0].Rows.Count;
//        } else {
//            return 0;
//        }
//        x.dbDisConnect();
//        ds.Dispose();
//    }

//    public string randImg(int ccount)
//    {
//        VBMath.Randomize();
//        int intChoice = 0;
//        intChoice = Conversion.Int(VBMath.Rnd() * ccount);
//        return intChoice;
//    }

//    public string fileType(fname)
//    {
//        if (Strings.UCase(Strings.Right(fname, 4)) == ".GIF" | Strings.UCase(Strings.Right(fname, 4)) == ".BMP") {
//            return ("iconGIF.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".JPG") {
//            return ("iconJPG.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".TIF") {
//            return ("iconTIF.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".TTF") {
//            return ("iconTTF.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".PSD") {
//            return ("iconPSD.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".HTM" | Strings.UCase(Strings.Right(fname, 4)) == "HTML") {
//            return ("iconHTM.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".PDF") {
//            return ("iconPDF.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".TXT") {
//            return ("iconTXT.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".ZIP" | Strings.UCase(Strings.Right(fname, 4)) == ".TAR" | Strings.UCase(Strings.Right(fname, 4)) == ".TGZ") {
//            return ("iconZIP.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".DOC") {
//            return ("iconDOC.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".XLS") {
//            return ("iconXLS.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".PPT" | Strings.UCase(Strings.Right(fname, 4)) == ".PPS") {
//            return ("iconPPT.gif");
//        } else if (Strings.UCase(Strings.Right(fname, 4)) == ".EXE") {
//            return ("iconEXE.gif");
//        } else {
//            return ("iconUN.gif");
//        }
//    }
//}