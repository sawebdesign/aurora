Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.SessionState
Imports System.Web.Mail


Public Class functions
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Public dbConn As SqlConnection
    Public Function dbConnect() As String
        Try
            dbConn = New SqlConnection(ConfigurationManager.AppSettings("DBConnectionString"))
            dbConn.Open()
            Return "True"
        Catch ex As Exception
            Return "False: " & ex.Message
        End Try
    End Function

    Public Function dbDisConnect() As Boolean
        Try
            dbConn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function getData(ByVal mStr As String) As DataSet
        dbConnect()
        Dim dbCon = New SqlClient.SqlDataAdapter(mStr, dbConn)
        Dim ds = New DataSet
        Try
            dbCon.Fill(ds, "myData")
            Return ds
        Catch ex As Exception
            Dim erro = "Error: Updating Data, " & ex.Message
        Finally
            dbDisConnect()
            dbConn.Close()
            dbCon.Dispose()
            ds.Dispose()
        End Try
    End Function

    Public Function upDateData(ByVal mStr As String) As String
        Try
            dbConnect()
            Dim objCommand As New SqlCommand(mStr, dbConn)
            objCommand.ExecuteNonQuery()
            Return "True"
        Catch ex As Exception
            Return ("Error: Updating Data, " & ex.Message)
        Finally
            dbDisConnect()
            dbConn.Close()
        End Try
    End Function

    '--Name: lDate
    '--Description: convert date to South African format
    Public Function sDate(ByVal tmpDate)
        sDate = PadZero(Day(CDate(tmpDate))) & "/" & PadZero(Month(CDate(tmpDate))) & "/" & Year(CDate(tmpDate))
    End Function

    '--Name: lDate
    '--Description: convert "27/06/2004" to long date "27 June 2004" for displaying perposes only
    Public Function lDate(ByVal tmpDate) As Date
        lDate = Day(tmpDate) & " " & MonthName(Month(tmpDate)) & " " & Year(tmpDate)
    End Function

    '--Name: GetMySQLDate
    '--Description: convert "27/06/2004" to long date "yyyy-mm-dd" for input purposes
    Public Function GetMySQLDate(ByVal sDate)
        Dim strDate
        strDate = Year(CDate(sDate)) & "-" & PadZero(Month(CDate(sDate))) & "-" & PadZero(Day(CDate(sDate)))
        GetMySQLDate = strDate
    End Function

    '--Name: PadZero
    '--Description: used with GetMySQLDate to make 1/1/2004 - 2004-01-01
    Public Function PadZero(ByVal val) As String
        If Len(CStr(val)) = 1 Then
            PadZero = "0" & CStr(val)
        Else
            PadZero = val
        End If
    End Function


    Public Function makeCode(ByVal maxLen) As String
        Dim strNewCode
        Dim whatsNext, upper, lower, intCounter
        Randomize()
        For intCounter = 1 To maxLen
            whatsNext = Int((1 - 0 + 1) * Rnd() + 0)
            If whatsNext = 0 Then
                '--character
                upper = 90
                lower = 65
            Else
                upper = 57
                lower = 48
            End If
            strNewCode = strNewCode & Chr(Int((upper - lower + 1) * Rnd() + lower))
        Next
        makeCode = strNewCode
    End Function

    Public Function MySQLStringFormat(ByVal strIn) As String
        Dim tmpString
        tmpString = Replace(strIn, "'", "`")
        tmpString = Replace(tmpString, "\", "\\")
        tmpString = Replace(tmpString, "�", "&amp;Sect")
        MySQLStringFormat = tmpString
    End Function

    'Public Function sendMail(ByVal fMail As String, ByVal tMail As String, ByVal mMessage As String, ByVal mSubject As String, ByVal smtp As String) As String

    '    Try
    '        '--Creating ActiveX object for SMTP (Jmail) 
    '        Dim JMail
    '        JMail=CreateObject("JMail.SMTPMail")

    '        '--Setting mail parameters 
    '        JMail.ServerAddress="" & smtp & ""
    '        JMail.Subject="" & mSubject & ""
    '        JMail.Sender="" & fMail & ""
    '        JMail.AddRecipient("" & tMail & ")

    '        JMail.HTMLBody=mMessage
    '        'JMail.appendText(mMessage)
    '        JMail.Execute()
    '        JMail.Close()

    '        Return "sent"
    '    Catch ex As Exception
    '        Return "Error: Sending email -" & smtp & "- " & ex.Message
    '    End Try

    'End Function

    Public Function sendMail(ByVal fMail As String, ByVal tMail As String, ByVal mMessage As String, ByVal mSubject As String, ByVal smtp As String) As String
        Try
            Dim m As New System.Web.Mail.MailMessage
            Dim strEmail As String
            strEmail = mMessage

            With m

                .From = fMail
                .To = tMail
                .Subject = mSubject
                .BodyFormat = MailFormat.Html
                .Body = strEmail

            End With

            SmtpMail.SmtpServer = smtp
            SmtpMail.Send(m)

            Return "sent"
        Catch ex As Exception
            Return "Error: Sending email, '" & smtp & "' " & ex.Message
        End Try
    End Function

    '--Function written by mk_bt
    '--Name: getPage
    '--Description: Get the page details for specified Id
    '--IdNum is the Id of the page in the database
    Function getPage(ByVal IdNum, ByVal linkId)

        Dim theId As Integer
        If linkId <> "" Then
            theId = linkId
        Else
            theId = IdNum
        End If

        Dim isErr As Boolean = False
        Dim dr As SqlClient.SqlDataReader
        Try
            Dim gSQl As String
            gSQl = "SELECT Title_S,Title_L,Content,ReDir,title,description,keywords,pCount FROM TB_Content WHERE ConId=" & theId & " "

            Dim dsCon = New DataSet
            dsCon = getData(gSQl)
            If dsCon.Tables(0).Rows.Count = 0 Then
                getPage = False
            Else
                System.Web.HttpContext.Current.Session("Title_S") = dsCon.Tables(0).Rows(0)("Title_S")
                System.Web.HttpContext.Current.Session("Title_L") = dsCon.Tables(0).Rows(0)("Title_L")
                System.Web.HttpContext.Current.Session("Content") = Replace(dsCon.Tables(0).Rows(0)("Content"), "../", "")
                System.Web.HttpContext.Current.Session("ReDir") = dsCon.Tables(0).Rows(0)("ReDir")
                If Trim(dsCon.Tables(0).Rows(0)("title")) = "" Then
                    System.Web.HttpContext.Current.Session("title") = dsCon.Tables(0).Rows(0)("Title_S")
                Else
                    System.Web.HttpContext.Current.Session("title") = dsCon.Tables(0).Rows(0)("title")
                End If
                System.Web.HttpContext.Current.Session("description") = dsCon.Tables(0).Rows(0)("description")
                System.Web.HttpContext.Current.Session("keywords") = dsCon.Tables(0).Rows(0)("keywords")

                pageCount(IdNum, "TB_Content", "pCount", "ConId", dsCon.Tables(0).Rows(0)("pCount"))
                getPage = True
            End If
            dsCon.Dispose()
        Catch ex As Exception
            isErr = True
            System.Web.HttpContext.Current.Session("eSource") = ex.Source
            System.Web.HttpContext.Current.Session("eMessage") = ex.Message
            System.Web.HttpContext.Current.Session("eStackTrace") = ex.StackTrace
        End Try
        If isErr = True Then
            System.Web.HttpContext.Current.Response.Redirect("error.aspx")
        End If
    End Function

    '--Function written by mk_bt
    '--Name: pageCount
    '--Description: count the number of page views
    '--theId is the page id
    Sub pageCount(ByVal theId, ByVal table, ByVal fld, ByVal fldId, ByVal theCount)
        Dim isErr As Boolean = False
        Try
            Dim dbUpdate = upDateData("Update " & table & " SET " & fld & "=" & theCount + 1 & " where " & fldId & "=" & theId)
        Catch ex As Exception
            isErr = True
            System.Web.HttpContext.Current.Session("eSource") = ex.Source
            System.Web.HttpContext.Current.Session("eMessage") = ex.Message
            System.Web.HttpContext.Current.Session("eStackTrace") = ex.StackTrace
        End Try
        If isErr = True Then
            System.Web.HttpContext.Current.Response.Redirect("error.aspx")
        End If
    End Sub

    Public Function getrCount(ByVal msql As String) As Integer
        Dim x As New functions
        x.dbConnect()
        Dim ds As DataSet = x.getData(msql)
        If ds.Tables(0).Rows.Count >= 1 Then
            Return ds.Tables(0).Rows.Count
        Else
            Return 0
        End If
        x.dbDisConnect()
        ds.Dispose()
    End Function

    Public Function randImg(ByVal ccount As Integer) As String
        Randomize()
        Dim intChoice As Integer
        intChoice = Int(Rnd() * ccount)
        Return intChoice
    End Function

    Function fileType(ByVal fname) As String
        If UCase(Right(fname, 4)) = ".GIF" Or UCase(Right(fname, 4)) = ".BMP" Then
            Return ("iconGIF.gif")
        ElseIf UCase(Right(fname, 4)) = ".JPG" Then
            Return ("iconJPG.gif")
        ElseIf UCase(Right(fname, 4)) = ".TIF" Then
            Return ("iconTIF.gif")
        ElseIf UCase(Right(fname, 4)) = ".TTF" Then
            Return ("iconTTF.gif")
        ElseIf UCase(Right(fname, 4)) = ".PSD" Then
            Return ("iconPSD.gif")
        ElseIf UCase(Right(fname, 4)) = ".HTM" Or UCase(Right(fname, 4)) = "HTML" Then
            Return ("iconHTM.gif")
        ElseIf UCase(Right(fname, 4)) = ".PDF" Then
            Return ("iconPDF.gif")
        ElseIf UCase(Right(fname, 4)) = ".TXT" Then
            Return ("iconTXT.gif")
        ElseIf UCase(Right(fname, 4)) = ".ZIP" Or UCase(Right(fname, 4)) = ".TAR" Or UCase(Right(fname, 4)) = ".TGZ" Then
            Return ("iconZIP.gif")
        ElseIf UCase(Right(fname, 4)) = ".DOC" Then
            Return ("iconDOC.gif")
        ElseIf UCase(Right(fname, 4)) = ".XLS" Then
            Return ("iconXLS.gif")
        ElseIf UCase(Right(fname, 4)) = ".PPT" Or UCase(Right(fname, 4)) = ".PPS" Then
            Return ("iconPPT.gif")
        ElseIf UCase(Right(fname, 4)) = ".EXE" Then
            Return ("iconEXE.gif")
        Else
            Return ("iconUN.gif")
        End If
    End Function
End Class
