Public Class eezyAds
    Dim g_eezyads_Demo = False
    Dim g_eezyads_strConnect 'Connect string
    Dim g_eezyads_MaxRecords = 50
    Dim g_eezyads_PathToAdServe = "" 'HTTP path to adserve.asp and Clients.AdClick.asp, like http://www.sqlexperts.com/ads
    Dim g_eezyads_DatabaseType = "SQLServer"
    Dim g_eezyads_strAlreadyOnPage 'Internal - you should not set it yourself
    Dim g_eezyads_eezyadsRedirPath = g_eezyads_PathToAdServe & "/eezyadsredir.ashx"
    Public g_MaxLongInt = 2147483647 ' Virtually forever, max for a long integer in Access
    Public g_MaxEndDate = "1 Jan 2020" ' This date means forever...

    Public Function eezyads_GetAdASP(ByVal strQueryString) As String
        Dim sArr, n
        Dim sArr2

        'Setable parameters
        Dim sZones, nBannerId, nBannerOnPage, SearchCatId, SiteId
        Dim nFarm As Double
        strQueryString = Replace(strQueryString, "}", "")
        strQueryString = Replace(strQueryString, "{", "")

        sArr = Split(strQueryString, "&")

        For n = LBound(sArr) To UBound(sArr)
            sArr2 = Split(sArr(n), "=")

            Select Case sArr2(0)
                Case "Z"
                    sZones = sArr2(1) * 1
                Case "F"
                    nFarm = sArr2(1) * 1
                Case "B"
                    nBannerId = sArr2(1) * 1
                Case "N"
                    nBannerOnPage = sArr2(1) * 1
                Case "CATID"
                    If IsNumeric(sArr2(1)) = True Then
                        SearchCatId = sArr2(1) * 1
                    Else
                        SearchCatId = 0
                    End If
                Case "SITE"
                    If IsNumeric(sArr2(1)) = True Then
                        SiteId = sArr2(1) * 1
                    Else
                        SiteId = 0
                    End If
            End Select
        Next

        '--if there is no banner that matches the CatId for the zone then
        '--change the zone to the main zone and re-run
        Dim tmpStr
        tmpStr = eezyads_GetAdEx(True, sZones, nFarm, nBannerId, nBannerOnPage, SearchCatId, True, SiteId)
        If tmpStr = "" Then
            sZones = 0
            SearchCatId = ""
            tmpStr = eezyads_GetAdEx(True, sZones, nFarm, nBannerId, nBannerOnPage, SearchCatId, True, SiteId)
            Return tmpStr
        Else
            Return tmpStr
        End If
    End Function


    ' If ASP then it returns the HTML
    ' else it simply returns the bannerid
    ' fASP = true or false
    Private Function eezyads_GetAdEx(ByVal fASP, ByVal strZone, ByVal nFarm, ByVal nBannerId, ByVal nBannerOnPage, ByVal nCatId, ByVal fCanUseHTML, ByVal SiteID)
        Dim oRS As DataSet
        Dim nSumWeight
        Dim nTempIndex
        Dim nWeight
        Dim nTempIndex2
        Dim nRandom As Integer
        Dim nBanner As Integer
        Dim nCurRow
        Dim nMax

        If strZone * 1 = 0 Then
            strZone = 0
        End If

        If nFarm * 1 = 0 Then
            nFarm = 0
        End If

        '--Get Total Weight
        oRS = eezyads_DBGetAvailBanners(fASP, strZone, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID)
        If oRS.Tables(0).Rows.Count = 0 Then
            'There is no banner in this banner farm
            oRS.Dispose()
            Return ""
            Exit Function
        End If

        '1. Sum the weights of the banners
        nSumWeight = 0
        For ix As Integer = 0 To oRS.Tables(0).Rows.Count - 1
            nSumWeight = nSumWeight + oRS.Tables(0).Rows(ix)("weight")
        Next


        '2. Pick a number at random between 1 and the sum of the weights. 
        Randomize()
        nRandom = Convert.ToInt32((nSumWeight * Rnd()))

        '3. Iterate over the items, decrementing the random number by the weight of the current selection.
        Dim nCurVal As Integer = 0
        For nCurVal = 0 To oRS.Tables(0).Rows.Count - 1
            nRandom -= oRS.Tables(0).Rows(nCurVal)("weight")
            '4. Compare the result to zero, if less than or equal to break otherwise keep iterating.
            If nRandom <= 0 Then
                Exit For
            End If
        Next

        nBanner = oRS.Tables(0).Rows(nCurVal)("bannerid")

        eezyads_AddToUsedList(nBanner)

        If Not fASP Then
            eezyads_GetAdEx = nBanner & "---" & oRS.Tables(0).Rows(nCurVal)("BannerURL").Value
            eezyads_DBAddShowCount(nBanner)
            Exit Function
        End If


        If fCanUseHTML And oRS.Tables(0).Rows(nCurVal)("ishtml") = True Then
            Dim sHTMCode
            sHTMCode = eezyads_GetHTMLCode(nBanner)
            eezyads_GetAdEx = FixupSpecialVariables(sHTMCode)
            eezyads_DBAddShowCount(nBanner)
            Exit Function
        End If

        'Now we have the banner id, lets create the actual HTML
        'Move into temp variables only to make it more readable
        Dim sRedirUrl
        Dim sBannerURL
        Dim sAltText
        Dim sUnderText
        Dim sUnderUrl
        Dim sRet
        Dim nXSize
        Dim nYSize
        Dim MyTarget

        sRedirUrl = g_eezyads_eezyadsRedirPath & "?id=" & nBanner & "&way=ban"
        If IsDBNull(oRS.Tables(0).Rows(nCurVal)("BannerURL")) Then
            sBannerURL = ""
        Else
            sBannerURL = oRS.Tables(0).Rows(nCurVal)("BannerURL")
        End If
        If IsDBNull(oRS.Tables(0).Rows(nCurVal)("AltText")) Then
            sAltText = ""
        Else
            sAltText = oRS.Tables(0).Rows(nCurVal)("AltText")
        End If
        If IsDBNull(oRS.Tables(0).Rows(nCurVal)("UnderText")) Then
            sUnderText = ""
        Else
            sUnderText = oRS.Tables(0).Rows(nCurVal)("UnderText")
        End If
        sUnderUrl = g_eezyads_eezyadsRedirPath & "?id=" & nBanner & "&way=txt"

        nXSize = oRS.Tables(0).Rows(nCurVal)("xsize")
        nYSize = oRS.Tables(0).Rows(nCurVal)("ysize")
        If InStr(oRS.Tables(0).Rows(nCurVal)("redirurl"), "http://") Or InStr(oRS.Tables(0).Rows(nCurVal)("underurl"), "http://") Then
            MyTarget = " target=""_new"""
        End If
        If Right(sBannerURL, 3) = "swf" Then
            sRet = "<embed src=""" & sBannerURL & """ title=""" & sAltText & """ alt=""" & sAltText & """" & " border=0 width=""" & nXSize & """" & " height=""" & nYSize & """" & "  datadesc=""" & sAltText & """ dataid=""" & nBanner & """ />"
        Else
            sRet = "<a href=""" & sRedirUrl & """ " & MyTarget & " datadesc=""" & sAltText & """ dataid=""" & nBanner & """>" & "<img src=""" & sBannerURL & """" & " title=""" & sAltText & """ alt=""" & sAltText & """" & " border=0 width=""" & nXSize & """" & " height=""" & nYSize & """" & ">" & "</a>"
        End If
        If sUnderText <> "" Then
            sRet = sRet & "</br><a class=""inlink"" href=""" & sUnderUrl & """" & " " & MyTarget & " datadesc=""" & sUnderText & """ dataid=""" & nBanner & """>" & sUnderText & "</a></font>"
        Else
        End If
        sRet = sRet & ""



        'Lets update impression for it
        eezyads_DBAddShowCount(nBanner)
        oRS.Dispose()

        Return sRet
    End Function


    Function eezyads_GetHTMLCode(ByVal nBannerId)
        Dim oRS As DataSet
        Dim x As New functions
        oRS = x.getData("select htmlcode from Clients.AdBanner where bannerid=" & nBannerId)
        eezyads_GetHTMLCode = oRS.Tables(0).Rows(0)("htmlcode")
        oRS.Dispose()
        x.Dispose()
    End Function

    Function eezyads_DBGetAvailBanners(ByVal fASP, ByVal strZones, ByVal nFarm, ByVal nBannerId, ByVal nCatId, ByVal fCanUseHTML, ByVal SiteID) As DataSet
        Dim x As New functions
        Return x.getData(GetAdSQL(fASP, strZones, nFarm, nBannerId, nCatId, fCanUseHTML, SiteID))
    End Function


    Function GetAdSQL(ByVal fASP, ByVal sZone, ByVal nFarm, ByVal nBannerId, ByVal nCatId, ByVal fCanUseHTML, ByVal SiteID)
        Dim strSQL
        Dim strWhere

        strSQL = "select distinct Clients.AdBanner.bannerid, Clients.AdBanner.BannerURL, Clients.AdBanner.weight "
        If fASP Then
            strSQL = strSQL & ", Clients.AdBanner.AltText, Clients.AdBanner.UnderText, Clients.AdBanner.xsize, Clients.AdBanner.ysize, Clients.AdBanner.redirurl, Clients.AdBanner.underurl"
        End If
        If fCanUseHTML Then
            strSQL = strSQL & ",ishtml"
        End If

        If g_eezyads_strAlreadyOnPage <> "" Then
            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid not in ( " & g_eezyads_strAlreadyOnPage & ")")
        End If

        strSQL = strSQL & " from Clients.AdBanner "
        If sZone >= 0 Then
            strSQL = strSQL & ",Clients.AdBannerZone "
            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid=Clients.AdBannerZone.bannerid")
            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBannerZone.zoneid in ( " & sZone & ")")
        End If

        strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.ClientSiteID=" & SiteID)

        If IsNumeric(nCatId) = False Then nCatId = 0
        If nCatId * 1 <> 0 Then
            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.CategoryId='" & nCatId & "'")
        End If
        If nBannerId <> 0 Then
            strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.bannerid=" & nBannerId)
        Else
            If nFarm <> 0 Then
                strWhere = AdSQL_AddAndWhere(strWhere, "Clients.AdBanner.farmid=" & nFarm)
            End If
            strWhere = AdSQL_AddAndWhere(strWhere, "weight > 0 and showcount < maximpressions AND validtodate >= " & Internal_eezyadsdb_GetDateFunction() & " AND validfromdate <= " & Internal_eezyadsdb_GetDateFunction())
            If fCanUseHTML = True Then
                strWhere = AdSQL_AddAndWhere(strWhere, " ( ( clickcount+underclickcount < maxclicks ) OR ishtml=" & Internal_eezyadsdb_GetBoolValue(True) & " )")
            Else
                strWhere = AdSQL_AddAndWhere(strWhere, "ishtml <> " & Internal_eezyadsdb_GetBoolValue(True))
            End If
        End If

        strSQL = strSQL & strWhere

        ' If you want banners with no zoning to mean all zones then add these 
        ' lines

        '	If sZone <> "0" Then
        '			strSQL = strSQL & "union select  distinct Clients.AdBanner.bannerid, Clients.AdBanner.BannerURL, Clients.AdBanner.weight "
        '		If fASP Then
        '			strSQL = strSQL & ", Clients.AdBanner.AltText, Clients.AdBanner.UnderText, Clients.AdBanner.xsize, Clients.AdBanner.ysize "
        '		End If
        '		If fCanUseHTML Then
        '			strSQL = strSQL & ",ishtml"
        '		End If
        '		strSQL = strSQL & " from Clients.AdBanner "
        '		strSQL = strSQL & " where bannerid not in (select bannerid from Clients.AdBannerZone)"
        '		If 	g_eezyads_strAlreadyOnPage <> "" Then
        '			strSQL = strSQL & " AND Clients.AdBanner.bannerid not in ( " & 	g_eezyads_strAlreadyOnPage & ")" 
        '		End If
        '	End If	

        GetAdSQL = strSQL

    End Function

    Sub eezyads_DBAddShowCount(ByVal nBanner)
        Dim x As New functions
        x.upDateData("update Clients.AdBanner set showcount=showcount+1 where bannerid=" & nBanner)
        x.Dispose()
    End Sub

    Sub eezyads_DBUpdateClickCount(ByVal nBanner, ByVal fUnderText)
        Dim sSQL
        Dim x As New functions

        If fUnderText = True Then
            sSQL = "update Clients.AdBanner set underclickcount = underclickcount +1 where bannerid = " & nBanner
        Else
            sSQL = "update Clients.AdBanner set clickcount = clickcount +1 where bannerid = " & nBanner
        End If
        x.upDateData(sSQL)

        sSQL = "Insert Into Clients.AdClick (bannerid,page,ClickDate,ip,isUnderText) Values (" & nBanner & ", '" & System.Web.HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "', '" & Date.Now() & "', '" & System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & "', " & Internal_eezyadsdb_GetBoolValue(fUnderText) & ")"
        x.upDateData(sSQL)
        x.Dispose()
    End Sub

    Function eezyads_DBGetUrl(ByVal nBanner, ByVal fUnderText)
        Dim oRS As DataSet
        Dim sSQL2
        Dim x As New functions
        If fUnderText = True Then
            sSQL2 = "select underurl as url from Clients.AdBanner where bannerid= " & nBanner
        Else
            sSQL2 = "select redirurl as url from Clients.AdBanner where bannerid= " & nBanner
        End If

        oRS = x.getData(sSQL2)
        eezyads_DBGetUrl = oRS.Tables(0).Rows(0)("url")
        oRS.Dispose()
        x.Dispose()
    End Function

    Function AdSQL_AddAndWhere(ByVal strWhere, ByVal strWhat)
        If strWhere = "" Then
            strWhere = " WHERE "
        Else
            strWhere = strWhere & " AND "
        End If
        strWhere = strWhere & " " & strWhat
        AdSQL_AddAndWhere = strWhere
    End Function

    Private Function eezyads_AddToUsedList(ByVal nBannerId)

        If g_eezyads_strAlreadyOnPage <> "" Then
            g_eezyads_strAlreadyOnPage = g_eezyads_strAlreadyOnPage & ","
        End If
        g_eezyads_strAlreadyOnPage = g_eezyads_strAlreadyOnPage & CStr(nBannerId)
    End Function

    Function Internal_eezyadsdb_GetDateFunction()
        If g_eezyads_DatabaseType = "SQLServer" Then
            Internal_eezyadsdb_GetDateFunction = "getdate()"
        Else
            Internal_eezyadsdb_GetDateFunction = "date()"
        End If
    End Function

    Function Internal_eezyadsdb_GetBoolValue(ByVal fTrue)
        If g_eezyads_DatabaseType = "SQLServer" Then
            If fTrue = True Then
                Internal_eezyadsdb_GetBoolValue = "1"
            Else
                Internal_eezyadsdb_GetBoolValue = "0"
            End If
        Else
            If fTrue = True Then
                Internal_eezyadsdb_GetBoolValue = "true"
            Else
                Internal_eezyadsdb_GetBoolValue = "false"
            End If
        End If

    End Function

    Private Function FixupSpecialVariables(ByVal sHTML)
        'Now check for '<ADM_RANDOM
        Dim fCont
        fCont = True
        While fCont = True
            Dim nIndStart, nIndEnd, sSubStr, vData, nLow, nHigh, nNumber
            Dim sLeftHTML, sRightHTML

            nIndStart = InStr(1, CStr(sHTML), "<ADM_RANDOM")
            If nIndStart > 0 Then
                sLeftHTML = Left(sHTML, nIndStart - 1)

                nIndEnd = InStr(nIndStart, sHTML, ">")

                sRightHTML = Mid(sHTML, nIndEnd + 1)

                sSubStr = Mid(sHTML, nIndStart, nIndEnd - nIndStart)

                vData = Split(sSubStr, "-")
                If vData(1) = "LAST" Then
                    nNumber = System.Web.HttpContext.Current.Session("eezyads_RndNumber")
                Else
                    nLow = CLng(vData(1))
                    nHigh = CLng(vData(2))
                    Randomize()
                    nNumber = CLng((nHigh * Rnd()) + nLow)
                    System.Web.HttpContext.Current.Session("eezyads_RndNumber") = nNumber
                End If
                sHTML = sLeftHTML & CStr(nNumber) & sRightHTML
            End If
            If InStr(1, CStr(sHTML), "<ADM_RANDOM") > 0 Then
                fCont = True
            Else
                fCont = False
            End If
        End While
        FixupSpecialVariables = sHTML

    End Function

    Public Function eezyads_ClickAd(ByVal nBannerId, ByVal sWay) As String
        Dim fIsUnderText As Boolean
        If sWay = "txt" Then
            fIsUnderText = True
        Else
            fIsUnderText = False '--Clicked on actual banner
        End If

        eezyads_DBUpdateClickCount(nBannerId, fIsUnderText)
        Return eezyads_DBGetUrl(nBannerId, fIsUnderText)

    End Function

    Public Function GetFarmName(ByVal Id As Integer) As String
        Try
            Dim x As New functions
            Dim ds As DataSet = x.getData("select name from Features.AdFarm where farmid = " & Id & "")
            If ds.Tables(0).Rows.Count >= 1 Then
                Return ds.Tables(0).Rows(0)("name")
            Else
                Return "No farm selected! THIS BANNER WILL NEVER ROTATE!"
            End If
            ds.Dispose()
        Catch ex As Exception
            Return "Unknown"
        End Try
    End Function

    Public Function GetZoneName(ByVal Id As Integer) As String
        Try
            Dim x As New functions
            Dim ds As DataSet = x.getData("select zonename from Features.AdZone, Clients.AdBannerZone where Clients.AdBannerZone.zoneid=Features.AdZone.zoneid AND Clients.AdBannerZone.bannerid= " & Id & "")
            If ds.Tables(0).Rows.Count >= 1 Then
                Return ds.Tables(0).Rows(0)("zonename")
            Else
                Return "No zones selected"
            End If
            ds.Dispose()
        Catch ex As Exception
            Return "Unknown"
        End Try
    End Function

End Class
