Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing

Partial Class imagecode
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '--get the code and seperate it
        Dim Code, strA, strB, strC, strD, strE As String

        Code = GetRot39(Server.HtmlDecode(Request("code")))
        Dim CodeArr As Array = Split(Code, ",")
        For i As Integer = 0 To UBound(CodeArr)
            If i = 0 Then
                strA = CodeArr(i)
            ElseIf i = 1 Then
                strB = CodeArr(i)
            ElseIf i = 2 Then
                strC = CodeArr(i)
            ElseIf i = 3 Then
                strD = CodeArr(i)
            ElseIf i = 4 Then
                strE = CodeArr(i)
            End If
        Next

        Dim bmp As New Bitmap(Server.MapPath("images/base.jpg"))
        Dim g As Graphics = Graphics.FromImage(bmp)
        g.SmoothingMode = SmoothingMode.AntiAlias
        g.DrawString(strA, New Font("verdana", 18, FontStyle.Underline), New SolidBrush(Color.Silver), 1, 2)
        g.DrawString(strB, New Font("Arial", 15, FontStyle.Italic), New SolidBrush(Color.Gray), 34, 0)
        g.DrawString(strC, New Font("verdana", 16, FontStyle.Regular), New SolidBrush(Color.SlateGray), 44, 5)
        g.DrawString(strD, New Font("Arial", 22, FontStyle.Italic), New SolidBrush(Color.SlateGray), 70, -5)
        g.DrawString(strE, New Font("verdana", 26, FontStyle.Italic), New SolidBrush(Color.LightGray), 105, -2)

        Response.ContentType = "image/jpeg"
        bmp.Save(Response.OutputStream, bmp.RawFormat)

        bmp.Dispose()
        g.Dispose()

    End Sub

    Public Function GetRot39(ByVal str) As String
        Dim obj
        Try
            obj = CreateObject("Rotation39Encryptor.Rot39")
            GetRot39 = obj.Rot39(CStr(str))
        Catch ex As Exception
            GetRot39 = ex.Message
        Finally
            obj = Nothing
        End Try
        Return GetRot39
    End Function

End Class
