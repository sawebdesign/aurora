﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="WebImage.ascx.vb" Inherits="sawd.WebImage.WebImage" %>
<script src="scripts/jquery-1.5.2.js" type="text/javascript"></script>
<script src="WebImage.js" type="text/javascript"></script>
<link href="WebImage.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#fffff;">
    <tr>
        <td valign="top" class="fileleft">
            <div class="fileinputdiv">
                <img src="images/preload.gif" class="filepreloader" alt="loading" width="20" height="20" />
                <input type="button" class="fileinputbutton" id="btnFakeUpload" value="Upload " />
                <input type="file" class="fileinputhidden" id="txtUpload" name="txtUpload" runat="server" />
            </div>
            <input type="submit" value="Upload!" class="button" id="btnUpload" name="btnUpload" runat="server" />
            <br /><b>File:</b><br /><% =Request("iName")%><br />
            <% If Not String.IsNullOrEmpty(Request("lgWidth")) Then%>
                <br /><b>Max Width:</b><br /><% =Request("lgWidth")%>px<br />
            <% End If %>
            <br /><b>Current Dimensions:</b><br /><label id="currentx"></label>px&nbsp;x&nbsp;<label id="currenty"></label>px<br />
        </td>
        <td valign="top" width="99%" class="filetdmain">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1">
	            <tr>
		            <td class="filetdbg">
                        <div id="imageMenu">
						    <input type="image" src="images/ico_save_off.gif" value="Save" name="save" id="save" runat="server" />
						    <input type="image" src="images/ico_ImageSize_off.gif" value="Resize" name="resize" id="resize"	runat="server" title="Resize" />
						    <input type="image" src="images/ico_delete_off.gif" value="Delete" name="delete" id="delete" runat="server" title="Delete" />
                        </div>
		            </td>
	            </tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" id="tbresize" style="position:absolute">
	                        <tr>
		                        <td style="width:25; height:21">&nbsp;</td>
		                        <td class="filetdbg">&nbsp;Width&nbsp;:&nbsp;<input name="newWidth" size="3" class="text" value='<% =iWidth %>' />&nbsp;&nbsp;</td>
	                        </tr>
                        </table>
                        <div id="message" class="filemessage">File loaded</div>
                    </td>
	            <tr>
		            <td>
                        <div id="mylayerDiv"><img src="" alt="image" id="editimg" /></div>
		            </td>
	            </tr>
            </table>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
