﻿/// <reference path="scripts/jquery-1.5.2.js" />

var usermessage;
var imageUrl = getQueryVariable('ipath') + 'tmp_' + getQueryVariable('iname') + '?' + new Date();
$(function () {

    $("#tbresize").hide();
    $("#resize").mouseover(function () {
        $("#tbresize").slideToggle();
    })
    $("#tbresize").mouseleave(function () {
        $("#tbresize").slideToggle();
    })

    $("#btnUpload").hide();
    $('#txtUpload').change(function () {
        $("#btnFakeUpload").hide();
        $("#txtUpload").hide();
        $('#btnUpload').click();
    });

    $("#message").hide();
    $('#message').click(function () {
        $("#message").slideToggle();
    });
    imageUrl = getQueryVariable('ipath') + 'tmp_' + getQueryVariable('iname') + '?' + new Date();
    loadimage();

    if (usermessage !== undefined) {
        message(usermessage);
    }
});
function loadImage(result) {
    alert(imageUrl);
    imageUrl = getQueryVariable('ipath') + 'tmp_' + getQueryVariable('iname') + '?' + new Date();
    var imageElement = document.getElementById('editimg');
    imageElement.src = imageUrl;
    alert(im
    $(imageElement).load(function () {
        $(this).fadeIn(500);
        setWH();
    });
};
function fail(result) {
    if ($("#editimg").length > 0) {
        $("#editimg").remove();
        message('Select an Image to upload');
    }
};
function loadimage() {
    //load the image
    


    $.ajax({ url: imageUrl, type: 'HEAD', error: fail, success: loadImage });
}

function message(lable) {
    $("#message").html(lable);
    $("#message").slideToggle();
}

function setWH() {
    $("#currenty").text($("#editimg").width());
    $("#currentx").text($("#editimg").height());
}

function getQueryVariable(variable) {
    ///	<summary>
    ///		Get query string elements by their name
    ///	</summary>
    ///	<returns type="string" />
    ///	<param name="variable" type="String">
    ///		The name of the query string param that you are looking for
    ///	</param>

    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return null;
}