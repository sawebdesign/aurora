﻿Imports System.Drawing.Imaging
Imports System.Drawing.Image
Imports System.Drawing
Imports System.IO
Imports System.Text
Imports System.Drawing.Drawing2D

Public Class WebImage
    Inherits System.Web.UI.UserControl
    Public iWidth As Integer = 0            'A local variable for width
    Public redir As String                  'The location that we must redirect the user to
    Public smWidth, lgWidth As String       'The size that the images need to be when saved
    Public iPath As String                  'The path the images will be saved to

    Public lgPrefix As String = ""          'The prefix for the full size image
    Public smPrefix As String = "sm_"       'The prefix for the thumbnails

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '--Check the prefixes of the page
        If Not IsPostBack Then
            If Request("lgPrefix") <> "" Then
                lgPrefix = Request("lgPrefix")
            End If
            If Request("smPrefix") <> "" Then
                smPrefix = Request("smPrefix")
            End If
            If Not String.IsNullOrEmpty(Request("ipath")) Then
                Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iname")

                If File.Exists(Server.MapPath(Request("ipath")) & lgPrefix & Request("iName")) = True Then
                    makeTmp(lgPrefix & Request("iname"), "tmp_" & Request("iname"), Server.MapPath(Request("ipath")))
                    'If Request("lgWidth") > 0 Then
                    '    doResize(fPath, Request("lgWidth"))
                    'End If
                End If

            End If
        End If

    End Sub

    Private Sub makeTmp(ByVal oname, ByVal fname, ByVal sFullPath)
        File.Copy(sFullPath & oname, Server.MapPath(Request("ipath")) & fname, True)
    End Sub

    Public Sub doUpload(ByVal Sender As Object, ByVal e As System.EventArgs, ByVal fPath As String, ByVal fFile As String)
        Dim sPath As String    '--Default path
        Dim sFile As String    '--original name
        Dim sFullPath As String
        Dim sSplit() As String
        Dim sPathFriendly As String
        Dim sMessage As String

        '--Upload to same path as script
        '--Internet Anonymous User must have write permissions
        sPath = fPath
        If Right(sPath, 1) <> "\" Then
            sPathFriendly = sPath    '--Friendly path name for display
            sPath = sPath & "\"
        Else
            sPathFriendly = Left(sPath, Len(sPath) - 1)
        End If

        '--Save as same file name being posted
        '--The code below resolves the file name
        '--(removes path info)
        sFile = txtUpload.PostedFile.FileName
        sSplit = Split(sFile, "\")
        sFile = sSplit(UBound(sSplit))

        sFullPath = fPath & fFile
        Try
            txtUpload.PostedFile.SaveAs(sFullPath)
            sMessage = "Upload of File " & sFile & " succeeded."

            If Request("lgWidth") > 0 Then
                Dim fullSizeImg As System.Drawing.Image
                fullSizeImg = System.Drawing.Image.FromFile(sFullPath)
                Dim imageWidth As Integer = fullSizeImg.Width
                fullSizeImg.Dispose()
                If (imageWidth > Request("lgWidth")) Then
                    doResize(sFullPath, Request("lgWidth"))
                    sMessage += "This image was larger than " & Request("lgWidth") & "px and was reduced in size.<br /> If the quality is low then please resize the image as close to " & Request("lgWidth") & " and re-upload.Please click save to commit changes."
                ElseIf (imageWidth < Request("lgWidth")) Then
                    sMessage += "This image is smaller than " & Request("lgWidth") & "px."
                ElseIf (imageWidth = Request("lgWidth")) Then
                    sMessage += "This Image is the correct size."
                End If

            End If
        Catch Ex As Exception
            sMessage = "Upload of File " & sFile & " to failed for the following reason: " & Ex.Message
        End Try

        'register script to call return function
        If (Not Page.ClientScript.IsStartupScriptRegistered("UploadMessage")) Then
            Dim script As String = "<script language='javascript'> var usermessage = '" & sMessage & "'; loadimage();</script>"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "UploadMessage", script, False)
        End If

    End Sub


    Public Function doLG(ByVal sFullPath As String, ByVal oldfName As String, ByVal fName As String, ByVal maxLengh As Integer)
        Dim fullSizeImg As System.Drawing.Image
        fullSizeImg = System.Drawing.Image.FromFile(sFullPath & oldfName)
        '--Read in the width and height
        Dim imageHeight As Integer = fullSizeImg.Height
        Dim imageWidth As Integer = fullSizeImg.Width

        If maxLengh > 0 Then
            If imageHeight < imageWidth Then
                If imageWidth > maxLengh Then
                    '--use the height as a base
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim thumbNailImg As System.Drawing.Image
                    thumbNailImg = fullSizeImg.GetThumbnailImage(maxLengh, ResizeHeight(imageHeight, imageWidth, maxLengh), dummyCallBack, IntPtr.Zero)
                    '--close fullSizeImg so you can resave it
                    fullSizeImg.Dispose()
                    thumbNailImg.Save(sFullPath & fName, ImageFormat.Jpeg)
                    thumbNailImg.Dispose()
                Else
                    fullSizeImg.Dispose()
                    File.Delete(Server.MapPath(Request("ipath")) & fName)
                    File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
                End If
            Else
                If imageHeight > maxLengh Then
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim thumbNailImg As System.Drawing.Image
                    thumbNailImg = fullSizeImg.GetThumbnailImage(ResizeWidth(imageHeight, imageWidth, maxLengh), maxLengh, dummyCallBack, IntPtr.Zero)
                    fullSizeImg.Dispose()
                    thumbNailImg.Save(sFullPath & fName, ImageFormat.Jpeg)
                    thumbNailImg.Dispose()
                Else
                    fullSizeImg.Dispose()
                    File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
                End If
            End If
        Else
            fullSizeImg.Dispose()
            File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
        End If

    End Function


	Public Function doSM(ByVal sFullPath As String, ByVal oldfName As String, ByVal fName As String, ByVal maxLengh As Integer)
		Dim fullSizeImg As System.Drawing.Image
		fullSizeImg = System.Drawing.Image.FromFile(sFullPath & oldfName)
		'--Read in the width and height
		Dim imageHeight As Integer = fullSizeImg.Height
		Dim imageWidth As Integer = fullSizeImg.Width

		If imageWidth < maxLengh Then
			File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
			fullSizeImg.Dispose()
		Else
			SizeImage(fullSizeImg, maxLengh, ResizeHeight(imageHeight, imageWidth, maxLengh), (sFullPath & fName))
			fullSizeImg.Dispose()
		End If

		'-- OLD LOGIC BELOW
		'--If imageHeight < imageWidth Then
		'--If imageWidth > maxLengh Then
			'--use the height as a base
			'--Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
			'--dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
			'--Dim thumbNailImg As System.Drawing.Image
			'--thumbNailImg = fullSizeImg.GetThumbnailImage(maxLengh, ResizeHeight(imageHeight, imageWidth, maxLengh), dummyCallBack, IntPtr.Zero)
			'--close fullSizeImg so you can resave it
			'--fullSizeImg.Dispose()
			'--thumbNailImg.Save(sFullPath & fName, ImageFormat.Jpeg)
			'--thumbNailImg.Dispose()
			'--SizeImage(fullSizeImg, maxLengh, ResizeHeight(imageHeight, imageWidth, maxLengh), (sFullPath & fName))
		'--fullSizeImg.Dispose()
		'--Else
		'fullSizeImg.Dispose()
		'File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
		'End If
		'Else
		'If imageHeight > maxLengh Then
		'            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
		'            dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
		'            Dim thumbNailImg As System.Drawing.Image
		'            thumbNailImg = fullSizeImg.GetThumbnailImage(ResizeWidth(imageHeight, imageWidth, maxLengh), maxLengh, dummyCallBack, IntPtr.Zero)
		'            fullSizeImg.Dispose()
		'            thumbNailImg.Save(sFullPath & fName, ImageFormat.Jpeg)
		'thumbNailImg.Dispose()
		'SizeImage(fullSizeImg, ResizeWidth(imageHeight, imageWidth, maxLengh), maxLengh, (sFullPath & fName))
		'fullSizeImg.Dispose()
		'Else
		'fullSizeImg.Dispose()
		'File.Copy(sFullPath & oldfName, Server.MapPath(Request("ipath")) & fName, True)
		'End If
		'End If
	End Function

	Public Function SizeImage(ByVal img As Image, ByVal width As Integer, ByVal height As Integer, ByVal fName As String) As Bitmap
		Dim newBit As New Bitmap(width, height)	'new blank bitmap
		Dim jgpEncoder As ImageCodecInfo = GetEncoder(ImageFormat.Jpeg)
		Dim myEncoder As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Quality
		' Create an EncoderParameters object. 
		' An EncoderParameters object has an array of EncoderParameter 
		' objects. In this case, there is only one 
		' EncoderParameter object in the array. 
		Dim myEncoderParameters As New EncoderParameters(1)
		Dim myEncoderParameter As New EncoderParameter(myEncoder, 75&)
		myEncoderParameters.Param(0) = myEncoderParameter


		Dim bmp As New System.Drawing.Bitmap(img)
		Dim g As Graphics = Graphics.FromImage(newBit)

		'change interpolation for reduction quality
		g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
		g.PixelOffsetMode = PixelOffsetMode.HighQuality

		g.DrawImage(bmp, 0, 0, width, height)
		newBit.Save(fName, jgpEncoder, myEncoderParameters)
		img.Dispose()
		Return newBit
	End Function

	Private Function GetEncoder(ByVal format As ImageFormat) As ImageCodecInfo

		Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageDecoders()

		Dim codec As ImageCodecInfo
		For Each codec In codecs
			If codec.FormatID = format.Guid Then
				Return codec
			End If
		Next codec
		Return Nothing

	End Function


    Public Function doResize(ByVal sFullPath As String, ByVal maxLengh As Integer)
        Dim fullSizeImg As System.Drawing.Image
        fullSizeImg = System.Drawing.Image.FromFile(sFullPath)
        '--Read in the width and height
        Dim imageHeight As Integer = fullSizeImg.Height
        Dim imageWidth As Integer = fullSizeImg.Width

        If imageHeight > imageWidth Then

            ''--use the height as a base
            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
            dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
            Dim thumbNailImg As System.Drawing.Image = fullSizeImg
            thumbNailImg = fullSizeImg
            thumbNailImg = fullSizeImg.GetThumbnailImage(ResizeWidth(imageHeight, imageWidth, maxLengh), maxLengh, dummyCallBack, IntPtr.Zero)
            '--close fullSizeImg so you can resave it
            fullSizeImg.Dispose()
            thumbNailImg.Save(sFullPath, ImageFormat.Jpeg)
            thumbNailImg.Dispose()

            'Dim myBit As Bitmap = New Bitmap(fullSizeImg)
            'Dim gr As Graphics = Graphics.FromImage(myBit)
            'If (True) Then
            '    gr.SmoothingMode = SmoothingMode.AntiAlias
            '    gr.InterpolationMode = InterpolationMode.HighQualityBicubic
            '    gr.PixelOffsetMode = PixelOffsetMode.HighQuality
            '    gr.DrawImage(thumbNailImg, New Rectangle(0, 0, imageWidth, imageHeight))
            'End If

            'thumbNailImg = thumbNailImg.GetThumbnailImage(ResizeWidth(imageHeight, imageWidth, maxLengh), maxLengh, dummyCallBack, IntPtr.Zero)
            'fullSizeImg.Dispose()
            'thumbNailImg.Save(sFullPath, ImageFormat.Jpeg)
            'thumbNailImg.Dispose()

        Else
            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
            dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
            Dim thumbNailImg As System.Drawing.Image
            Dim resized As Bitmap

            resized = Bitmap.FromFile(sFullPath)

            Dim newMap As New Bitmap(resized)
            newMap.SetResolution(72, 72)


            newMap.Save(sFullPath.Replace(".jpg", "_res.jpg"), ImageFormat.Jpeg)
            Dim sevtyTwo As Image
            sevtyTwo = Image.FromFile(sFullPath.Replace(".jpg", "_res.jpg"))

            thumbNailImg = sevtyTwo.GetThumbnailImage(maxLengh, ResizeHeight(imageHeight, imageWidth, maxLengh), dummyCallBack, IntPtr.Zero)
            resized.Dispose()
            sevtyTwo.Dispose()
            File.Delete(sFullPath.Replace(".jpg", "_res.jpg"))
            fullSizeImg.Dispose()
            thumbNailImg.Save(sFullPath, ImageFormat.Jpeg)
            thumbNailImg.Dispose()
        End If

        'register script to call return function
        If (Not Page.ClientScript.IsStartupScriptRegistered("ResizeMessage")) Then
            Dim script As String = "<script language='javascript'> var usermessage = 'The file was resized';  loadimage();</script>"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ResizeMessage", script, False)
        End If
    End Function

    Function ResizeWidth(ByVal intHSize As Integer, ByVal intWSize As Integer, ByVal newHSize As Integer) As Integer
        Dim myWidth As Integer
        myWidth = (newHSize / intHSize) * intWSize
        Return myWidth
    End Function

    Function ResizeHeight(ByVal intHSize As Integer, ByVal intWSize As Integer, ByVal newWSize As Integer) As Integer
        Dim myHeight As Integer
        myHeight = (newWSize / intWSize) * intHSize
        Return myHeight
    End Function

    Function ThumbnailCallback() As Boolean
        Return False
    End Function


    Public Function doCrop(ByVal sFullPath As String, ByVal xDown As Integer, ByVal yDown As Integer, ByVal xUp As Integer, ByVal yUp As Integer)
        Dim bmpImage As Bitmap
        Dim recCrop As Rectangle
        Dim bmpCrop As Bitmap
        Dim gphCrop As Graphics
        Dim recDest As Rectangle
        Dim sMessage As String
        Dim width, height, xxDown, yyDown As Integer
        If xDown < xUp Then
            width = xUp - xDown
            xxDown = xDown
            yyDown = yDown
        Else
            width = xDown - xUp
            xxDown = xUp
            yyDown = yUp
        End If
        If yDown < yUp Then
            height = yUp - yDown
        Else
            height = yDown - yUp
        End If

        Try
            bmpImage = New Bitmap(sFullPath)
            recCrop = New Rectangle(xxDown, yyDown, width, height)
            bmpCrop = New Bitmap(recCrop.Width, recCrop.Height, bmpImage.PixelFormat)
            gphCrop = Graphics.FromImage(bmpCrop)
            recDest = New Rectangle(0, 0, width, height)
            gphCrop.DrawImage(bmpImage, recDest, recCrop.X, recCrop.Y, recCrop.Width, recCrop.Height, GraphicsUnit.Pixel)
            sMessage = "The image was cropped"
        Catch er As Exception
            sMessage = "CROP ERR: " & er.ToString
        Finally
            bmpImage.Dispose()
            bmpCrop.Save(sFullPath, ImageFormat.Jpeg)
            iWidth = bmpCrop.Width
        End Try

        'register script to call return function
        If (Not Page.ClientScript.IsStartupScriptRegistered("CropMessage")) Then
            Dim script As String = "<script language='javascript'> var usermessage = '" & sMessage & "'; loadimage();</script>"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "CropMessage", script, False)
        End If
    End Function

    Private Sub btnUpload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.ServerClick
        Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
        doUpload(sender, e, Server.MapPath(Request("ipath")), "tmp_" & Request("iName"))
        'Me.tmpImg.Visible = True
        'Me.tmpImg.ImageUrl = fPath
        'Server.Transfer(Request.ServerVariables("SCRIPT_NAME"))
    End Sub

    Private Sub btnSkip()

    End Sub

    Private Sub resize_ServerClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles resize.ServerClick
        Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
        doResize(fPath, Request("newWidth"))
        iWidth = Request("newWidth")
        'Me.tmpImg.ImageUrl = iPath
        'Server.Transfer(Request.ServerVariables("SCRIPT_NAME"))
    End Sub

    Private Sub crop_ServerClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
        doCrop(fPath, Request("xD"), Request("yD"), Request("xU"), Request("yU"))
        'Me.tmpImg.ImageUrl = iPath
        'Server.Transfer(Request.ServerVariables("SCRIPT_NAME"))
    End Sub

    Private Sub delete_ServerClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles delete.ServerClick
        Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
        If File.Exists(fPath) = True Then
            File.Delete(fPath)
        End If

        fPath = Server.MapPath(Request("ipath")) & smPrefix & Request("iName")
        If File.Exists(fPath) = True Then
            File.Delete(fPath)
        End If

        fPath = Server.MapPath(Request("ipath")) & lgPrefix & Request("iName")
        If File.Exists(fPath) = True Then
            File.Delete(fPath)
        End If

        'register script to call return function
        If (Not Page.ClientScript.IsStartupScriptRegistered("DeleteMessage")) Then
            Dim script As String = "<script language='javascript'> var usermessage = 'The file was deleted'; loadimage();</script>"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "DeleteMessage", script, False)
        End If
    End Sub

    Private Sub save_ServerClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles save.ServerClick
        Dim didLGSM As Boolean = False
        If File.Exists(Server.MapPath(Request("ipath") & "tmp_" & Request("iName"))) Then

            '--resize and save the small image
            If Request("smWidth") * 1 <> 0 And Request("smWidth") <> "" Then
                doSM(Server.MapPath(Request("ipath")), "tmp_" & Request("iName"), smPrefix & Request("iname"), Request("smWidth"))
                didLGSM = True
                'doCrop(Server.MapPath(Request("ipath") & "sm_" & Request("iname")), 0, 0, Request("smWidth"), Request("smWidth"))
            End If

            '--resize and save the large image
            If Request("lgWidth") * 1 <> 0 Then
                doLG(Server.MapPath(Request("ipath")), "tmp_" & Request("iName"), lgPrefix & Request("iname"), Request("lgWidth"))
                didLGSM = True
            End If

            Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
            If File.Exists(fPath) = True Then
                '--if there was a sm or lg image then dont rename file just delete it
                If didLGSM = False Then
                    File.Move(fPath, Server.MapPath(Request("ipath")) & Request("iname"))
                End If
                File.Delete(fPath)
            End If

            'register script to call return function
            If (Not Page.ClientScript.IsClientScriptBlockRegistered("SaveMessage")) Then
                Dim script As String = "<script language='javascript'> var usermessage = 'The file was saved'; </script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SaveMessage", script, False)
            End If
        End If
    End Sub

    'Private Sub close_ServerClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles close.ServerClick
    '    Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
    '    If File.Exists(fPath) = True Then
    '        File.Delete(fPath)
    '        Me.tmpImg.Visible = False
    '    End If
    '    Response.Redirect(Replace(Request("redir"), "|", "&"))
    'End Sub


    '--Convert a physical file path to a URL for hypertext links.
    Private Function MapURL(ByVal path) As String
        Dim rootPath, url
        rootPath = Server.MapPath("/")
        'Response.Write("rootPath:" & rootPath & "<br>")
        url = Right(path, Len(path) - Len(rootPath))
        'Response.Write("url:" & url & "<br>")
        MapURL = Replace("../" & url, "\", "/")
    End Function

    'Private Sub skip_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles skip.ServerClick
    '    Dim fPath As String = Server.MapPath(Request("ipath")) & "tmp_" & Request("iName")
    '    If File.Exists(fPath) = True Then
    '        File.Delete(fPath)
    '        Me.tmpImg.Visible = False
    '    End If
    '    Response.Redirect(Replace(Request("redir"), "|", "&"))
    'End Sub
End Class